-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-05-2017 a las 05:30:12
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP DATABASE db_appbosa;

CREATE DATABASE db_appbosa;

USE db_appbosa;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `app_bosa_soft`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anio`
--

CREATE TABLE `anio` (
  `id` int(11) NOT NULL,
  `anio` year(4) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `anio`
--

INSERT INTO `anio` (`id`, `anio`, `activo`) VALUES
(1, 2017, 1),
(2, 2018, 1),
(3, 2019, 1),
(4, 2020, 1),
(5, 2021, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_caja`
--

CREATE TABLE `audit_caja` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_cargo`
--

CREATE TABLE `audit_cargo` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_ciudad`
--

CREATE TABLE `audit_ciudad` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_cliente`
--

CREATE TABLE `audit_cliente` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(255) NOT NULL,
  `registro` varchar(255) NOT NULL,
  `operacion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_contenedor`
--

CREATE TABLE `audit_contenedor` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(255) NOT NULL,
  `semana` varchar(255) NOT NULL,
  `numero_contenedor` varchar(255) NOT NULL,
  `referencia` varchar(255) NOT NULL,
  `operacion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time DEFAULT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_estado`
--

CREATE TABLE `audit_estado` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_linea_naviera`
--

CREATE TABLE `audit_linea_naviera` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_nave`
--

CREATE TABLE `audit_nave` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_operador`
--

CREATE TABLE `audit_operador` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_pais`
--

CREATE TABLE `audit_pais` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_puerto_destino`
--

CREATE TABLE `audit_puerto_destino` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_semana`
--

CREATE TABLE `audit_semana` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_usuario`
--

CREATE TABLE `audit_usuario` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(255) NOT NULL,
  `usuario_afectado` varchar(255) NOT NULL,
  `operacion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `booking`
--

INSERT INTO `booking` (`id`, `codigo`, `activo`) VALUES
(1, '572123443', 1),
(2, '087LIM227108 ', 1),
(3, '087LIM227108 ', 1),
(4, '98171199', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id` int(11) NOT NULL,
  `marca` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`id`, `marca`, `activo`) VALUES
(1, 'A. HEIJN', 1),
(2, 'ALTOMERCATO', 1),
(3, 'BIO BIO FAIRTRADE', 1),
(4, 'CLUSTERBAGS', 1),
(5, 'EDEKA BIO FAIRTRADE', 1),
(6, 'EDEKA BIO ORGANICO', 1),
(7, 'EKOOKE FAIRTRADE', 1),
(8, 'EQUIFRUIT FLO', 1),
(9, 'EKOOKE VERDE', 1),
(10, 'SAMAN FT', 1),
(11, 'SAMAN ORGANIC SPP', 1),
(12, 'SPAR', 1),
(13, 'YC. BIO GERMANY FLO', 1),
(14, 'YC. SPAR  FLO', 1),
(15, 'KOREA 13.5 KG', 1),
(16, 'DOLE  JAPON 13 KG', 1),
(17, 'CHIQUITA NORMAL', 1),
(18, 'CHIQUITA AL VACIO FLO', 1),
(20, 'SAMAN FLO ORGANIC', 1),
(21, 'WHOLE TRADE', 1),
(22, 'ALBERTH HEIJING', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_contenedor`
--

CREATE TABLE `caja_contenedor` (
  `id` int(11) NOT NULL,
  `contenedor_id` int(11) NOT NULL,
  `caja_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `nombre`, `activo`) VALUES
(1, 'Gerente', 1),
(2, 'Administrador', 1),
(8, 'Cliente', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificado`
--

CREATE TABLE `certificado` (
  `id` int(11) NOT NULL,
  `id_contenedor` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id`, `id_pais`, `nombre`, `activo`) VALUES
(1, 1, 'HOLANDA MERI...', 1),
(2, 2, 'HAMBURGO', 1),
(3, 3, 'CANADA', 1),
(4, 1, 'HOLANDA', 1),
(5, 1, 'DOVER', 1),
(6, 3, 'LOUSIANA', 1),
(7, 3, 'CALIFORNIA', 1),
(8, 3, 'RODMAN', 1),
(9, 4, 'PANAMA', 1),
(10, 4, 'Paita', 0),
(11, 3, 'DOVER USA', 1),
(12, 3, 'FLORIDA', 1),
(13, 5, 'BELGICA', 1),
(14, 3, 'NEW YOURK', 1),
(15, 6, 'JAPON', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `razon_social` varchar(50) NOT NULL,
  `slug` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `razon_social`, `slug`, `email`, `telefono`, `activo`) VALUES
(1, 'AGROFAIR', 'agrofair', '', '', 1),
(2, 'BIODYNAMISKA', 'biodynamiska', '', '', 1),
(3, 'DOLE', 'dole', '', '', 1),
(4, 'TRANSASTRA', 'transastra', '', '', 1),
(5, 'EQUIFRUIT', 'equifruit', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenedor`
--

CREATE TABLE `contenedor` (
  `id` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `referencia` varchar(20) NOT NULL,
  `booking` varchar(20) NOT NULL,
  `numero_contenedor` varchar(50) NOT NULL,
  `id_lineanaviera` int(11) NOT NULL,
  `id_nave` int(11) NOT NULL,
  `viaje` varchar(20) NOT NULL,
  `id_puertodestino` int(11) NOT NULL,
  `id_operador` int(11) NOT NULL,
  `fecha_proceso_inicio` date NOT NULL,
  `fecha_proceso_fin` date NOT NULL,
  `fecha_zarpe` date DEFAULT NULL,
  `fecha_llegada` date DEFAULT NULL,
  `peso_bruto` double DEFAULT NULL,
  `peso_neto` double DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `color` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id`, `nombre`, `color`, `activo`) VALUES
(1, 'Pendiente', '#ff0013', 1),
(2, 'En Proceso', '#D35400', 1),
(3, 'Enviado', '#27AE60', 1),
(4, 'En Puerto Paita', '#2980B9', 1),
(5, 'En la nave', '#004040', 1),
(6, 'Llegó a destino', '#b516e9', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id` int(11) NOT NULL,
  `id_contenedor` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `icono`
--

CREATE TABLE `icono` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linea_naviera`
--

CREATE TABLE `linea_naviera` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `linea_naviera`
--

INSERT INTO `linea_naviera` (`id`, `nombre`, `activo`) VALUES
(1, 'APL', 1),
(2, 'CMA - CGM', 1),
(3, 'HAMBURG SUD - HSD', 1),
(4, 'HAPAG LLOYD - HLE', 1),
(5, 'MAERSK MSK', 1),
(6, 'MOL', 1),
(7, 'MSC', 1),
(8, 'SEATRADE - STR', 1),
(9, 'DOLE', 1),
(18, 'YOKOHAMA-JAPON', 1),
(19, 'JENIFFERee', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `nombre`, `activo`) VALUES
(14, 'Super Administrador', 1),
(15, 'Administrador', 1),
(16, 'Cliente', 1),
(20, 'Gerencia', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_opcion`
--

CREATE TABLE `menu_opcion` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `opcion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_opcion`
--

INSERT INTO `menu_opcion` (`id`, `menu_id`, `opcion_id`) VALUES
(1, 14, 7),
(2, 14, 8),
(3, 14, 9),
(4, 14, 10),
(6, 14, 12),
(7, 15, 7),
(8, 15, 8),
(9, 15, 9),
(10, 15, 10),
(11, 15, 11),
(19, 14, 11),
(24, 20, 7),
(25, 20, 8),
(26, 20, 9),
(27, 20, 10),
(28, 20, 11),
(29, 16, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `id` int(11) NOT NULL,
  `id_contenedor` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `id_estado` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nave`
--

CREATE TABLE `nave` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nave`
--

INSERT INTO `nave` (`id`, `nombre`, `activo`) VALUES
(1, 'MARINER', 1),
(3, 'LOMBOK STRAIT', 1),
(21, 'NORTHERN DEXTERITY', 1),
(22, 'HANSA MEERS BURG', 1),
(24, 'AEREO', 1),
(25, 'ALGOL', 1),
(26, 'ALIOTH', 1),
(27, 'ALM ZURICH', 1),
(28, 'AOTEA MAERSK', 1),
(29, 'ARKADIA', 1),
(30, 'AS FIORELLA', 1),
(31, 'ATLANTIC KLIPPER', 1),
(32, 'ATLANTIC REEFER', 1),
(33, 'AVELONA STAR', 1),
(34, 'AVILA STAR', 1),
(35, 'BALTHASAR SHULTE', 1),
(36, 'BALTIC KLIPPER', 1),
(37, 'BARBARA', 1),
(38, 'BELLA SHULTE', 1),
(39, 'BIRK', 1),
(40, 'BODO SHULTE', 1),
(41, 'BOMAR RESOLUTE', 1),
(42, 'CALLAO EXPRESS', 1),
(43, 'CAP DOUKATO', 1),
(44, 'CAP INES', 1),
(45, 'CAP ISABEL', 1),
(46, 'CAP PALLISER', 1),
(47, 'CAP PALMESTOR', 1),
(48, 'CAP PASLEY', 1),
(49, 'CAP PATTON', 1),
(50, 'CAP PORTLAND', 1),
(51, 'CAP SAN SOUNIO', 1),
(52, 'CAP SAN TAINARO', 1),
(53, 'CAP TALBOT', 1),
(54, 'CAPA', 1),
(55, 'CARDIFF TRADER', 1),
(56, 'CAROLINA STAR', 1),
(57, 'CARSTEN MAERSK', 1),
(58, 'CATHARINA SHULTER', 1),
(59, 'CERINTHUS', 1),
(60, 'CHOAPA TRADER', 1),
(61, 'CMA CGM AMERICA', 1),
(62, 'CMA CGM MISSISSIPPI', 1),
(63, 'CMA CGM SAMBHAR', 1),
(64, 'CNP ILO', 1),
(65, 'CNP PAITA', 1),
(66, 'CONTI ARABELLA', 1),
(67, 'CONTI SOLOME', 1),
(68, 'COSCO KOREA', 1),
(69, 'CREDO', 1),
(70, 'CSCL LONG BEACH', 1),
(71, 'DOLE CALIFORNIA', 1),
(72, 'DOLE COSTA RICA', 1),
(73, 'DOLE ECUADOR', 1),
(74, 'DOLE PACIFIC', 1),
(75, 'DOMINGO', 1),
(76, 'DUBLIAN EXPRESS', 1),
(77, 'DUNCAN ISLAND', 1),
(78, 'ELISABETH-S', 1),
(79, 'EM HYDRA', 1),
(80, 'EVER EXCEL', 1),
(81, 'EVER SAFETY', 1),
(82, 'EVER UBERTY', 1),
(83, 'EVER UNION', 1),
(84, 'EVER URANUS', 1),
(85, 'EVERGREEN LINE', 1),
(86, 'FIONA', 1),
(87, 'FRISIA ROTTERDAM', 1),
(88, 'GLASGLOW EXPRESS', 1),
(89, 'GLORIA', 1),
(90, 'HAMBURG SUDD', 1),
(91, 'HAMMONIA AMERICA', 1),
(92, 'HANNAH SHULTE', 1),
(93, 'HANSA ASIA', 1),
(94, 'HANSA AUSTRALIA', 1),
(95, 'HANSA BREMEN', 1),
(96, 'HANSA CLOPPENBURG', 1),
(97, 'HANSA EUROPE', 1),
(98, 'HANSA FLENSBURG', 1),
(99, 'HANSA FREYBURG', 1),
(100, 'HANSA MEERSBURG', 1),
(101, 'HANSA OLDERBUNG', 1),
(102, 'HELENE 5', 1),
(103, 'HELLAS REEFER', 1),
(104, 'HYUNDAI SPLENDOR', 1),
(105, 'ITAL LIRICA', 1),
(106, 'ITALIA REEFER', 1),
(107, 'JACK LONDON', 1),
(108, 'JAMILA', 1),
(109, 'JPO VELA', 1),
(110, 'JULES VERNE', 1),
(111, 'JULIANA', 1),
(112, 'KIEL TRADER', 1),
(113, 'KMARIN AQUA', 1),
(114, 'KMARIN AZUR', 1),
(115, 'KOTA LAHIR', 1),
(116, 'KOTA LATIF', 1),
(117, 'KOTA LAYANG', 1),
(118, 'KOTA LEGIT', 1),
(119, 'KOTA LUKIS', 1),
(120, 'LADY KORCULA', 1),
(121, 'LADY REEFER', 1),
(122, 'LIVERPOOL EXPRESS', 1),
(123, 'LUNA MAERSK', 1),
(124, 'LUZON STRAIT', 1),
(125, 'MAERSK ALFIRK', 1),
(126, 'MAERSK ANTARES', 1),
(127, 'MAERSK BINTAN', 1),
(128, 'MAERSK BOGOR', 1),
(129, 'MAERSK GAIRLOCH', 1),
(130, 'MAERSK GIRONDE', 1),
(131, 'MAERSK LA PAZ', 1),
(132, 'MAERSK LAUNCESTON', 1),
(133, 'MAERSK LOME', 1),
(134, 'MAERSK NEWHAVEN', 1),
(135, 'MAGARI', 1),
(136, 'MARIA - KATHARINE S', 1),
(137, 'MAX WONDER', 1),
(138, 'MEDOCEAN', 1),
(139, 'MERIDIAN', 1),
(140, 'MOL GRANDEUR', 1),
(141, 'MOL PARADISE', 1),
(142, 'MOL PRESTIGE', 1),
(143, 'MSC AGRIGENTO', 1),
(144, 'MSC ALGECIRAS', 1),
(145, 'MSC ANISHA R', 1),
(146, 'MSC ANZU', 1),
(147, 'MSC ARUSHI R', 1),
(148, 'MSC ATHENS', 1),
(149, 'MSC AZOV', 1),
(150, 'MSC BRANKA', 1),
(151, 'MSC BRUNELLA', 1),
(152, 'MSC CARMEN', 1),
(153, 'MSC CHLOE', 1),
(154, 'MSC DOMITELLE', 1),
(155, 'MSC DONATA', 1),
(156, 'MSC FLAVIA', 1),
(157, 'MSC JULIA R', 1),
(158, 'MSC KATRINA', 1),
(159, 'MSC KATYA R', 1),
(160, 'MSC KIM', 1),
(161, 'MSC LEANNE', 1),
(162, 'MSC LEIGHT', 1),
(163, 'MSC LORENA', 1),
(164, 'MSC LOS ANGELES', 1),
(165, 'MSC MANU', 1),
(166, 'MSC PALAK', 1),
(167, 'MSC ROSARIA', 1),
(168, 'MSC SARAH', 1),
(169, 'MSC SARISKA', 1),
(170, 'MSC SASHA', 1),
(171, 'MSC SILVA', 1),
(172, 'MSC SOFIA CELESTE', 1),
(173, 'MSC VAISHNAVI R', 1),
(174, 'MSC VIDISHA R.', 1),
(175, 'MSC ZLATA R', 1),
(176, 'NEDERLAND REFFER', 1),
(177, 'NORASIA ALYA', 1),
(178, 'NORDAMELIA', 1),
(179, 'NORDSERENA', 1),
(180, 'NYK LIBRA', 1),
(181, 'NYL LYRA', 1),
(182, 'PACIFIC REEFER', 1),
(183, 'PIL', 1),
(184, 'POLARLIGHT', 1),
(185, 'RT ODIN', 1),
(186, 'SAFMARINE BENGUELA', 1),
(187, 'SAFMARINE MULANJE', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opcion`
--

CREATE TABLE `opcion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT 'home.',
  `icono` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `opcion`
--

INSERT INTO `opcion` (`id`, `nombre`, `url`, `icono`, `activo`) VALUES
(7, 'Container', 'home.container', 'truck', 1),
(8, 'Semana', 'home.semana', 'calendar', 1),
(9, 'Mantenimientos', 'home.mantenimiento', 'cog', 1),
(10, 'Reportes', 'home.reporte', 'file-excel-o', 1),
(11, 'Datos Estadisticos', 'home.grafico', 'area-chart', 1),
(12, 'Gestión de Usuarios', 'home.usuario', 'users', 1),
(13, 'Cliente', 'home.containers.cliente', 'users', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opcion_subopcion`
--

CREATE TABLE `opcion_subopcion` (
  `id` int(11) NOT NULL,
  `opcion_id` int(11) NOT NULL,
  `subopcion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `opcion_subopcion`
--

INSERT INTO `opcion_subopcion` (`id`, `opcion_id`, `subopcion_id`) VALUES
(8, 7, 9),
(9, 8, 10),
(10, 9, 11),
(12, 9, 13),
(13, 9, 14),
(14, 9, 15),
(15, 12, 16),
(16, 12, 17),
(17, 12, 18),
(19, 9, 19),
(20, 9, 20),
(21, 9, 21),
(22, 10, 22),
(23, 10, 23),
(24, 10, 24),
(25, 11, 25),
(26, 11, 26),
(27, 11, 27),
(28, 9, 28),
(29, 13, 29),
(30, 9, 30),
(31, 9, 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operador`
--

CREATE TABLE `operador` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `operador`
--

INSERT INTO `operador` (`id`, `nombre`, `activo`) VALUES
(1, 'SAN MIGUEL SERVICIOS LOGISTICOS S.C.R.L', 1),
(2, 'TRANSLOGISTCS SAC', 1),
(3, 'NEPTUNIA S.A.', 1),
(5, 'TPSAC', 1),
(6, 'AFE TRANSPORTATION S.A.C.', 1),
(7, 'GRUPO ADUAN', 1),
(8, 'YACZ CARGO', 1),
(9, 'TAN CARGO PERU SAC', 1),
(10, 'BLUE EXPRESS', 1),
(11, 'TRANSPORTES GIRASOLES', 1),
(12, 'RANSA', 1),
(13, 'SMP SERVICIOS POSTALES Y LOGÍSTICOS', 1),
(14, 'SEATRADE', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packing`
--

CREATE TABLE `packing` (
  `id` int(11) NOT NULL,
  `id_contenedor` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `nombre`, `activo`) VALUES
(1, 'HOLANDA', 1),
(2, 'ALEMANIA', 1),
(3, 'ESTADOS UNIDOS', 1),
(4, 'PANAMA', 1),
(5, 'BELGICA', 1),
(6, 'JAPON', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`id`, `nombre`, `slug`, `activo`) VALUES
(1, 'Registrar Contenedores', 'contenedor.registrar', 1),
(2, 'Registrar Facturas', 'facturas.registrar', 1),
(3, 'Registrar Packing', 'packings.registrar', 1),
(4, 'Registrar Certificados', 'certificados.registrar', 1),
(8, 'Aperturar Semana', 'semana.aperturar', 1),
(9, 'Eliminar Semana', 'semana.eliminar', 1),
(10, 'Actualizar Contenedor', 'contenedor.actualizar', 1),
(11, 'Eliminar Contenedor', 'contenedor.eliminar', 1),
(13, 'Agregar Registro Maestro', 'mantenimiento.registrar', 1),
(14, 'Eliminar Registro Maestro', 'mantenimiento.eliminar', 1),
(15, 'Cambiar estado de contenedor', 'contenedor.cambiar_estado', 1),
(16, 'Actualizar Registro Maestro', 'mantenimiento.actualizar', 1),
(17, 'Eliminar Ceritificados', 'certificados.eliminar', 1),
(18, 'Eliminar Facturas', 'facturas.eliminar', 1),
(19, 'Eliminar Packings', 'packings.eliminar', 1),
(20, 'Actualizar Password', 'usuario.actualizar_password', 1),
(21, 'Actualizar Nombre Usuario', 'usuario.actualizar_usuario', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso_rol`
--

CREATE TABLE `permiso_rol` (
  `id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `permiso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso_rol`
--

INSERT INTO `permiso_rol` (`id`, `rol_id`, `permiso_id`) VALUES
(8, 1, 2),
(9, 1, 3),
(18, 1, 4),
(23, 1, 8),
(24, 1, 9),
(26, 1, 11),
(27, 1, 13),
(28, 1, 14),
(30, 2, 1),
(31, 2, 2),
(32, 2, 3),
(33, 2, 4),
(34, 2, 8),
(35, 2, 9),
(36, 2, 10),
(37, 2, 11),
(39, 2, 13),
(40, 2, 14),
(41, 1, 1),
(42, 1, 10),
(43, 1, 15),
(44, 1, 16),
(45, 2, 15),
(46, 2, 16),
(47, 1, 20),
(48, 2, 20),
(49, 6, 20),
(50, 1, 17),
(51, 1, 18),
(52, 1, 19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `dni` char(8) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_paterno` varchar(50) NOT NULL,
  `apellido_materno` varchar(50) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(9) NOT NULL,
  `id_cargo` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `dni`, `nombre`, `apellido_paterno`, `apellido_materno`, `fullname`, `email`, `telefono`, `id_cargo`, `activo`) VALUES
(6, '56789098', 'Luis Guillermo', 'Ramirez', 'Coronado', 'Luis Guillermo Ramirez Coronado', 'luisramirezcoronado10@gmail.com', '948648043', 2, 1),
(8, '48102462', 'Darwin Joel', 'Andrade', 'Silva', 'Darwin Joel Andrade Silva', 'darwin@hotmail.com', '969543928', 2, 1),
(21, '90879876', 'AGROFAIR', 'AGROFAIR', 'AGROFAIR', 'AGROFAIR AGROFAIR AGROFAIR', 'agrofair@hotmail.com', '987634589', 8, 1),
(22, '45645678', 'BIODYNAMISKA', 'BIODYNAMISKA', 'BIODYNAMISKA', 'BIODYNAMISKA BIODYNAMISKA BIODYNAMISKA', 'BIODYNAMISKA@hotmail.com', '986798907', 8, 1),
(23, '89897867', 'DOLE', 'DOLE', 'DOLE', 'DOLEDOLE DOLE DOLE', 'dole@hotmail.com', '890767345', 8, 1),
(24, '98779845', 'TRANSASTRA', 'TRANSASTRA', 'TRANSASTRA', 'TRANSASTRA TRANSASTRA TRANSASTRA', 'transastra@hotmail.com', '908745345', 8, 1),
(25, '98767678', 'EQUIFRUIT', 'EQUIFRUIT', 'EQUIFRUIT', 'EQUIFRUIT EQUIFRUIT EQUIFRUIT', 'equifruit@hotmail.com', '987876789', 8, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerto_destino`
--

CREATE TABLE `puerto_destino` (
  `id` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puerto_destino`
--

INSERT INTO `puerto_destino` (`id`, `id_ciudad`, `nombre`, `activo`) VALUES
(1, 2, 'HAMBURGO\r\n', 1),
(2, 3, 'MONTREAL', 1),
(3, 1, 'ROTTERDAM', 1),
(4, 7, 'SAN DIEGO', 1),
(5, 1, 'WILMINGTON', 1),
(6, 1, 'ANTWERP', 1),
(7, 1, 'KOREA', 1),
(11, 4, 'VLISSINGEN', 1),
(12, 5, 'DOVER', 1),
(13, 6, 'NEW ORLEANS', 1),
(14, 7, 'PORT HUENEME', 1),
(15, 8, 'RODMAN', 1),
(16, 9, 'PORT FREEPORT', 1),
(17, 11, 'DELAWARE', 1),
(18, 12, 'EVERGALDES', 1),
(19, 13, 'PORT ANTWERP', 1),
(20, 14, 'PHILADELPHIA', 1),
(21, 12, 'MIAMI', 1),
(22, 7, 'WILMINGTON, US', 1),
(23, 15, 'ALIOTH', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `id_menu`, `activo`) VALUES
(1, 'Super Administrador', 14, 1),
(2, 'Administrador', 15, 1),
(3, 'Cliente', 16, 1),
(5, 'paletizador', 16, 0),
(6, 'Gerencia', 20, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semana`
--

CREATE TABLE `semana` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `id_anio` int(4) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subopcion`
--

CREATE TABLE `subopcion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `icono` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subopcion`
--

INSERT INTO `subopcion` (`id`, `nombre`, `icono`, `url`, `activo`) VALUES
(9, 'Lista', 'cog', 'home.container.lista', 1),
(10, 'Semanas', 'calendar', 'home.semana.lista_semana', 1),
(11, 'Clientes', 'users', 'home.mantenimiento.cliente', 1),
(13, 'Linea Naviera', 'ship', 'home.mantenimiento.linea_naviera', 1),
(14, 'Puerto Destino', 'anchor', 'home.mantenimiento.puerto_destino', 1),
(15, 'Operadores', 'user', 'home.mantenimiento.operador', 1),
(16, 'Usuario', 'user', 'home.usuario.usuario', 1),
(17, 'Roles', 'sitemap', 'home.usuario.rol', 1),
(18, 'Menú', 'list-ul', 'home.usuario.menu', 1),
(19, 'Cargo', 'vcard-o', 'home.mantenimiento.cargo', 1),
(20, 'Nave', 'train', 'home.mantenimiento.nave', 1),
(21, 'Estados', 'tags', 'home.mantenimiento.estado', 1),
(22, 'Contenedores Por Semana', 'file-excel-o', 'home.reporte.reporte_semana', 1),
(23, 'Contenedores Por Cliente', 'file-excel-o', 'home.reporte.contenedores_cliente', 1),
(24, 'Movimiento de Contenedores', 'file-excel-o', 'home.reporte.movimiento_contenedores', 1),
(25, 'Contenedores Por Cliente', 'bar-chart', 'home.grafico.grafico_cliente', 1),
(26, 'Contenedores Por Semana', 'bar-chart', 'home.grafico.grafico_semana', 1),
(27, 'Contenedores Por Año', 'bar-chart', 'home.grafico.grafico_anio', 1),
(28, 'Caja', 'inbox', 'home.mantenimiento.caja', 1),
(29, 'Mis Contenedores', 'list', 'home.container.cliente', 1),
(30, 'Pais', 'flag', 'home.mantenimiento.pais', 1),
(31, 'Ciudad', 'institution', 'home.mantenimiento.ciudad', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nick`, `password`, `id_persona`, `id_rol`, `remember_token`, `activo`) VALUES
(2, 'superadmin', '$2y$10$W2ozIxQB.qbmbmFdWxN.q.zT5L8272CVpxmOAIK3JtR0NytaUCD4W', 6, 1, 'S4rmkyeWmAG7d7VDNsu0NFDjIGJzMeqxnTZYuoOVQrG1ctxB4rdZ6wcKwNw0', 1),
(4, 'admin', '$2y$10$ZgvOMHe.tA.rmWv6dGNbEuiO3Ff9v8gZHBFAAzkWysuLsvHNwy9AK', 8, 2, 'baQFuy6DwuKn90JcmP6FaoWugZWEa07wOAbzCbZXxG6YC8W7vNz22CyIlunk', 1),
(10, 'agrofair', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 21, 3, 'sgQIpBucvhvQCCpwRhUNnhP8Cy3iDjtcXp95zQKTOQHDPBLiG0DVvZ3zY3B4', 1),
(11, 'biodynamiska', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 22, 3, 'MOsOg5bAZAwG4LesiqaFRBV3I3jHfHxRVfRV1RzxCdLCKvdj0hX04DoAoypC', 1),
(12, 'dole', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 23, 3, 'fouUVNJgV5R3YI3O5VnSNmg3MsCou23spW4dBHtKXu9tLPM3lqbkFajeGLbo', 1),
(13, 'transastra', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 24, 3, NULL, 1),
(14, 'equifruit', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 25, 3, NULL, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anio`
--
ALTER TABLE `anio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_caja`
--
ALTER TABLE `audit_caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_cargo`
--
ALTER TABLE `audit_cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_ciudad`
--
ALTER TABLE `audit_ciudad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_cliente`
--
ALTER TABLE `audit_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_contenedor`
--
ALTER TABLE `audit_contenedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_estado`
--
ALTER TABLE `audit_estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_linea_naviera`
--
ALTER TABLE `audit_linea_naviera`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_nave`
--
ALTER TABLE `audit_nave`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_operador`
--
ALTER TABLE `audit_operador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_pais`
--
ALTER TABLE `audit_pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_puerto_destino`
--
ALTER TABLE `audit_puerto_destino`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_semana`
--
ALTER TABLE `audit_semana`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_usuario`
--
ALTER TABLE `audit_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caja_contenedor`
--
ALTER TABLE `caja_contenedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contendor_id` (`contenedor_id`),
  ADD KEY `caja_id` (`caja_id`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `certificado`
--
ALTER TABLE `certificado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_contendor_certificado` (`id_contenedor`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pais_id` (`id_pais`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenedor`
--
ALTER TABLE `contenedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_booking` (`booking`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_lineanaviera` (`id_lineanaviera`),
  ADD KEY `id_puertodestino` (`id_puertodestino`),
  ADD KEY `id_operador` (`id_operador`),
  ADD KEY `id_semana` (`id_semana`),
  ADD KEY `id_nave` (`id_nave`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_contenedor_factura` (`id_contenedor`);

--
-- Indices de la tabla `icono`
--
ALTER TABLE `icono`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `linea_naviera`
--
ALTER TABLE `linea_naviera`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu_opcion`
--
ALTER TABLE `menu_opcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `opcionm_id` (`opcion_id`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_contenedor` (`id_contenedor`),
  ADD KEY `id_estado` (`id_estado`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `nave`
--
ALTER TABLE `nave`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `opcion`
--
ALTER TABLE `opcion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `opcion_subopcion`
--
ALTER TABLE `opcion_subopcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `opcion_id` (`opcion_id`),
  ADD KEY `subopcion_id` (`subopcion_id`);

--
-- Indices de la tabla `operador`
--
ALTER TABLE `operador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `packing`
--
ALTER TABLE `packing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_contenedor_packing` (`id_contenedor`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permiso_rol`
--
ALTER TABLE `permiso_rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rol_id` (`rol_id`),
  ADD KEY `permiso_id` (`permiso_id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cargo` (`id_cargo`);

--
-- Indices de la tabla `puerto_destino`
--
ALTER TABLE `puerto_destino`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ciudad_id` (`id_ciudad`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_menu` (`id_menu`);

--
-- Indices de la tabla `semana`
--
ALTER TABLE `semana`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_anio` (`id_anio`);

--
-- Indices de la tabla `subopcion`
--
ALTER TABLE `subopcion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_persona` (`id_persona`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anio`
--
ALTER TABLE `anio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `audit_caja`
--
ALTER TABLE `audit_caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `audit_cargo`
--
ALTER TABLE `audit_cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_ciudad`
--
ALTER TABLE `audit_ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `audit_cliente`
--
ALTER TABLE `audit_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_contenedor`
--
ALTER TABLE `audit_contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `audit_estado`
--
ALTER TABLE `audit_estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `audit_linea_naviera`
--
ALTER TABLE `audit_linea_naviera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `audit_nave`
--
ALTER TABLE `audit_nave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;
--
-- AUTO_INCREMENT de la tabla `audit_operador`
--
ALTER TABLE `audit_operador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `audit_pais`
--
ALTER TABLE `audit_pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `audit_puerto_destino`
--
ALTER TABLE `audit_puerto_destino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `audit_semana`
--
ALTER TABLE `audit_semana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `audit_usuario`
--
ALTER TABLE `audit_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `caja_contenedor`
--
ALTER TABLE `caja_contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `certificado`
--
ALTER TABLE `certificado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `contenedor`
--
ALTER TABLE `contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `linea_naviera`
--
ALTER TABLE `linea_naviera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `menu_opcion`
--
ALTER TABLE `menu_opcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT de la tabla `nave`
--
ALTER TABLE `nave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT de la tabla `opcion`
--
ALTER TABLE `opcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `opcion_subopcion`
--
ALTER TABLE `opcion_subopcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `operador`
--
ALTER TABLE `operador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `packing`
--
ALTER TABLE `packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `permiso_rol`
--
ALTER TABLE `permiso_rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `puerto_destino`
--
ALTER TABLE `puerto_destino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `semana`
--
ALTER TABLE `semana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `subopcion`
--
ALTER TABLE `subopcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `caja_contenedor`
--
ALTER TABLE `caja_contenedor`
  ADD CONSTRAINT `caja_id` FOREIGN KEY (`caja_id`) REFERENCES `caja` (`id`),
  ADD CONSTRAINT `contendor_id` FOREIGN KEY (`contenedor_id`) REFERENCES `contenedor` (`id`);

--
-- Filtros para la tabla `certificado`
--
ALTER TABLE `certificado`
  ADD CONSTRAINT `id_contendor_certificado` FOREIGN KEY (`id_contenedor`) REFERENCES `contenedor` (`id`);

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `pais_id` FOREIGN KEY (`id_pais`) REFERENCES `pais` (`id`);

--
-- Filtros para la tabla `contenedor`
--
ALTER TABLE `contenedor`
  ADD CONSTRAINT `id_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_lineanaviera` FOREIGN KEY (`id_lineanaviera`) REFERENCES `linea_naviera` (`id`),
  ADD CONSTRAINT `id_nave` FOREIGN KEY (`id_nave`) REFERENCES `nave` (`id`),
  ADD CONSTRAINT `id_operador` FOREIGN KEY (`id_operador`) REFERENCES `operador` (`id`),
  ADD CONSTRAINT `id_puertodestino` FOREIGN KEY (`id_puertodestino`) REFERENCES `puerto_destino` (`id`),
  ADD CONSTRAINT `id_semana` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `id_contenedor_factura` FOREIGN KEY (`id_contenedor`) REFERENCES `contenedor` (`id`);

--
-- Filtros para la tabla `menu_opcion`
--
ALTER TABLE `menu_opcion`
  ADD CONSTRAINT `menu_id` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `opcionm_id` FOREIGN KEY (`opcion_id`) REFERENCES `opcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD CONSTRAINT `id_contenedor` FOREIGN KEY (`id_contenedor`) REFERENCES `contenedor` (`id`),
  ADD CONSTRAINT `id_estado` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id`),
  ADD CONSTRAINT `id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `opcion_subopcion`
--
ALTER TABLE `opcion_subopcion`
  ADD CONSTRAINT `opcion_id` FOREIGN KEY (`opcion_id`) REFERENCES `opcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subopcion_id` FOREIGN KEY (`subopcion_id`) REFERENCES `subopcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `packing`
--
ALTER TABLE `packing`
  ADD CONSTRAINT `id_contenedor_packing` FOREIGN KEY (`id_contenedor`) REFERENCES `contenedor` (`id`);

--
-- Filtros para la tabla `permiso_rol`
--
ALTER TABLE `permiso_rol`
  ADD CONSTRAINT `permiso_id` FOREIGN KEY (`permiso_id`) REFERENCES `permiso` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rol_id` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `id_cargo` FOREIGN KEY (`id_cargo`) REFERENCES `cargo` (`id`);

--
-- Filtros para la tabla `puerto_destino`
--
ALTER TABLE `puerto_destino`
  ADD CONSTRAINT `ciudad_id` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudad` (`id`);

--
-- Filtros para la tabla `rol`
--
ALTER TABLE `rol`
  ADD CONSTRAINT `id_menu` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `semana`
--
ALTER TABLE `semana`
  ADD CONSTRAINT `id_anio` FOREIGN KEY (`id_anio`) REFERENCES `anio` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `id_persona` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
