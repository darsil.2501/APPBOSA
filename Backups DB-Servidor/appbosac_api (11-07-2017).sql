-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-07-2017 a las 19:01:54
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `appbosac_api`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anio`
--

CREATE TABLE `anio` (
  `id` int(11) NOT NULL,
  `anio` year(4) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `anio`
--

INSERT INTO `anio` (`id`, `anio`, `activo`) VALUES
(1, 2017, 1),
(2, 2018, 1),
(3, 2019, 1),
(4, 2020, 1),
(5, 2021, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_caja`
--

CREATE TABLE `audit_caja` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_cargo`
--

CREATE TABLE `audit_cargo` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_cargo`
--

INSERT INTO `audit_cargo` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(1, 'APPBOSA SAMAN SAMAN', 'Jefe Trazabilidad', 'INSERTAR', '2017-05-23', '15:37:19', '179.7.139.187', '179.7.139.187'),
(2, 'APPBOSA SAMAN SAMAN', 'Auxiliar Trazabilidad', 'INSERTAR', '2017-05-23', '15:38:05', '179.7.139.187', '179.7.139.187'),
(3, 'Juan Calderón More', 'Jefe Trazabilidad', 'ACTUALIZAR', '2017-05-30', '08:16:10', '179.7.132.187', '179.7.132.187'),
(4, 'APPBOSA SAMAN SAMAN', 'Operador', 'INSERTAR', '2017-07-08', '10:23:39', '::1', 'Luis'),
(5, 'APPBOSA SAMAN SAMAN', 'Operador Logístico', 'ACTUALIZAR', '2017-07-08', '10:35:34', '::1', 'Luis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_ciudad`
--

CREATE TABLE `audit_ciudad` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_cliente`
--

CREATE TABLE `audit_cliente` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(255) NOT NULL,
  `registro` varchar(255) NOT NULL,
  `operacion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_cliente`
--

INSERT INTO `audit_cliente` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(1, 'APPBOSA SAMAN SAMAN', 'Luis', 'INSERTAR', '2017-07-08', '11:05:26', '::1', 'Luis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_contenedor`
--

CREATE TABLE `audit_contenedor` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(255) NOT NULL,
  `semana` varchar(255) NOT NULL,
  `numero_contenedor` varchar(255) NOT NULL,
  `referencia` varchar(255) NOT NULL,
  `operacion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time DEFAULT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_contenedor`
--

INSERT INTO `audit_contenedor` (`id`, `usuario_responsable`, `semana`, `numero_contenedor`, `referencia`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(28, 'Juan Calderón More', '22', 'PONU 481624-0', '22513', 'INSERTAR', '2017-05-29', '10:15:54', '179.7.132.187', '179.7.132.187'),
(29, 'Juan Calderón More', '22', 'PONU 481624-0', '22513', 'ACTUALIZAR', '2017-05-29', '10:17:27', '179.7.132.187', '179.7.132.187'),
(30, 'Juan Calderón More', '22', 'PONU 481624-0', '22513', 'ACTUALIZAR', '2017-05-29', '12:18:16', '179.7.132.187', '179.7.132.187'),
(31, 'Juan Calderón More', '22', 'PONU 481624-0', '22513', 'ACTUALIZAR', '2017-05-29', '12:20:17', '179.7.132.187', '179.7.132.187'),
(32, 'Juan Calderón More', '22', 'PONU 481624-0', '22513', 'ELIMINAR', '2017-05-29', '12:20:17', '179.7.132.187', '179.7.132.187'),
(33, 'Juan Calderón More', '21', 'PONU 497834-4', '22595', 'INSERTAR', '2017-05-29', '12:33:50', '179.7.132.187', '179.7.132.187'),
(34, 'Juan Calderón More', '21', 'MNBU 315895-0', '22595', 'INSERTAR', '2017-05-29', '12:42:51', '179.7.132.187', '179.7.132.187'),
(35, 'Juan Calderón More', '21', 'MNBU 315895-0', '22595', 'ACTUALIZAR', '2017-05-29', '12:43:07', '179.7.132.187', '179.7.132.187'),
(36, 'Juan Calderón More', '21', 'PONU 497834-4', '22595', 'ACTUALIZAR', '2017-05-29', '12:43:37', '179.7.132.187', '179.7.132.187'),
(37, 'Juan Calderón More', '21', 'PONU 480767-6', '22597', 'INSERTAR', '2017-05-29', '12:49:33', '179.7.132.187', '179.7.132.187'),
(38, 'Juan Calderón More', '21', 'PONU 482052-8', '22598', 'INSERTAR', '2017-05-29', '12:52:50', '179.7.132.187', '179.7.132.187'),
(39, 'Juan Calderón More', '21', 'MMAU 107222-6', '22599', 'INSERTAR', '2017-05-29', '12:56:27', '179.7.132.187', '179.7.132.187'),
(40, 'Juan Calderón More', '21', 'BMOU 965137-1', '22588', 'INSERTAR', '2017-05-29', '15:11:35', '179.7.132.187', '179.7.132.187'),
(41, 'Juan Calderón More', '21', 'TTNU 896246-2', '22589', 'INSERTAR', '2017-05-29', '15:17:08', '179.7.132.187', '179.7.132.187'),
(42, 'Juan Calderón More', '21', 'BMOU 961635-0', '22590', 'INSERTAR', '2017-05-29', '15:19:48', '179.7.132.187', '179.7.132.187'),
(43, 'Juan Calderón More', '21', 'SUDU 605016-3', '22591', 'INSERTAR', '2017-05-29', '16:13:40', '179.7.132.187', '179.7.132.187'),
(44, 'Juan Calderón More', '21', 'SUDU 605417-4', '22592', 'INSERTAR', '2017-05-29', '16:15:15', '179.7.132.187', '179.7.132.187'),
(45, 'Juan Calderón More', '21', 'SUDU 604948-1', '22593', 'INSERTAR', '2017-05-29', '16:17:18', '179.7.132.187', '179.7.132.187'),
(46, 'Juan Calderón More', '21', 'SUDU 529208-0', '22594', 'INSERTAR', '2017-05-29', '16:20:48', '179.7.132.187', '179.7.132.187'),
(47, 'Juan Calderón More', '21', 'HLXU 876009-0', '1', 'INSERTAR', '2017-05-29', '16:24:37', '179.7.132.187', '179.7.132.187'),
(48, 'Juan Calderón More', '21', 'HLBU 900451-0', '2', 'INSERTAR', '2017-05-29', '16:27:25', '179.7.132.187', '179.7.132.187'),
(49, 'Juan Calderón More', '21', 'TCLU 102844-3', '1', 'INSERTAR', '2017-05-29', '16:29:35', '179.7.132.187', '179.7.132.187'),
(50, 'Juan Calderón More', '21', 'CRLU 137750-9', '2', 'INSERTAR', '2017-05-29', '16:31:34', '179.7.132.187', '179.7.132.187'),
(51, 'Juan Calderón More', '21', 'TLLU 106897-4', '1', 'INSERTAR', '2017-05-29', '16:40:33', '179.7.132.187', '179.7.132.187'),
(52, 'Juan Calderón More', '21', 'SUDU 823613-6', '1', 'INSERTAR', '2017-05-29', '16:42:35', '179.7.132.187', '179.7.132.187'),
(53, 'Juan Calderón More', '21', 'SUDU  523335-0', '2', 'INSERTAR', '2017-05-29', '16:44:06', '179.7.132.187', '179.7.132.187'),
(54, 'Juan Calderón More', '21', 'CXRU 161009-2', '2', 'INSERTAR', '2017-05-29', '16:48:31', '179.7.132.187', '179.7.132.187'),
(55, 'Juan Calderón More', '20', 'PONU 481624-0', '22513', 'INSERTAR', '2017-05-30', '10:52:17', '179.7.132.187', '179.7.132.187'),
(56, 'Juan Calderón More', '21', 'MNBU 315895-0', '22596', 'ACTUALIZAR', '2017-06-02', '09:16:42', '190.237.186.168', '190.237.186.168'),
(57, 'Juan Calderón More', '22', 'SUDU 616397-7', '22692', 'ACTUALIZAR', '2017-06-06', '10:58:47', '190.237.186.168', '190.237.186.168'),
(58, 'Juan Calderón More', '22', 'CSVU 750614-3', '2', 'ACTUALIZAR', '2017-06-06', '10:59:53', '190.237.186.168', '190.237.186.168'),
(59, 'Juan Calderón More', '22', 'SUDU 621885-3', '1', 'ACTUALIZAR', '2017-06-06', '11:00:37', '190.237.186.168', '190.237.186.168'),
(60, 'Juan Calderón More', '22', 'DFIU 215125-2', '1', 'ACTUALIZAR', '2017-06-06', '11:01:55', '190.237.186.168', '190.237.186.168'),
(61, 'Juan Calderón More', '22', 'MSCU 7433652', '1', 'ACTUALIZAR', '2017-06-06', '11:02:25', '190.237.186.168', '190.237.186.168'),
(62, 'Juan Calderón More', '22', 'HXLU 877232-1', '1', 'ACTUALIZAR', '2017-06-06', '11:02:53', '190.237.186.168', '190.237.186.168'),
(63, 'Juan Calderón More', '22', 'TEMU 906017-7', '22693', 'ACTUALIZAR', '2017-06-06', '11:03:16', '190.237.186.168', '190.237.186.168'),
(64, 'Juan Calderón More', '22', 'SUDU 600809-7', '22691', 'ACTUALIZAR', '2017-06-06', '11:03:51', '190.237.186.168', '190.237.186.168'),
(65, 'Juan Calderón More', '22', 'CNIU 222198-8', '22690', 'ACTUALIZAR', '2017-06-06', '11:04:21', '190.237.186.168', '190.237.186.168'),
(66, 'Juan Calderón More', '22', 'RRSU 100351-4', '22689', 'ACTUALIZAR', '2017-06-06', '11:04:47', '190.237.186.168', '190.237.186.168'),
(67, 'Juan Calderón More', '22', 'SZLU 980331-0', '22688', 'ACTUALIZAR', '2017-06-06', '11:05:29', '190.237.186.168', '190.237.186.168'),
(68, 'Juan Calderón More', '22', 'TCLU 138551-7', '22687', 'ACTUALIZAR', '2017-06-06', '11:05:52', '190.237.186.168', '190.237.186.168'),
(69, 'Juan Calderón More', '22', 'MSWU 102357-9', '22696', 'ACTUALIZAR', '2017-06-06', '11:06:22', '190.237.186.168', '190.237.186.168'),
(70, 'Juan Calderón More', '22', 'MMAU 107723-3', '22698', 'ACTUALIZAR', '2017-06-06', '11:06:47', '190.237.186.168', '190.237.186.168'),
(71, 'Juan Calderón More', '22', 'DAYU 670586-6', '22695', 'ACTUALIZAR', '2017-06-06', '11:07:03', '190.237.186.168', '190.237.186.168'),
(72, 'Juan Calderón More', '22', 'MWCU 657259-1', '22694', 'ACTUALIZAR', '2017-06-06', '11:07:32', '190.237.186.168', '190.237.186.168'),
(73, 'Juan Calderón More', '23', 'MNBU 308760-4', '22796', 'ACTUALIZAR', '2017-06-13', '07:19:10', '190.237.245.136', '190.237.245.136'),
(74, 'Juan Calderón More', '23', 'MMAU 124524-0', '22798', 'ACTUALIZAR', '2017-06-13', '07:46:42', '190.237.245.136', '190.237.245.136'),
(75, 'Juan Calderón More', '23', 'MNBU 041418-0', '22797', 'ACTUALIZAR', '2017-06-13', '07:48:40', '190.237.245.136', '190.237.245.136'),
(76, 'Juan Calderón More', '23', 'SUDU 823377-5', '22794', 'ACTUALIZAR', '2017-06-13', '07:51:51', '190.237.245.136', '190.237.245.136'),
(77, 'Juan Calderón More', '23', 'SUDU 801296-4', '22792', 'ACTUALIZAR', '2017-06-13', '07:53:25', '190.237.245.136', '190.237.245.136'),
(78, 'Juan Calderón More', '23', 'SUDU 800455-2', '22791', 'ACTUALIZAR', '2017-06-13', '07:55:12', '190.237.245.136', '190.237.245.136'),
(79, 'Juan Calderón More', '23', 'SUDU 625143-0', '22790', 'ACTUALIZAR', '2017-06-13', '07:56:41', '190.237.245.136', '190.237.245.136'),
(80, 'Juan Calderón More', '23', 'SUDU 823377-5', '22794', 'ACTUALIZAR', '2017-06-13', '07:58:03', '190.237.245.136', '190.237.245.136'),
(81, 'Juan Calderón More', '23', 'CAIU 554907-9', '22789', 'ACTUALIZAR', '2017-06-13', '08:00:01', '190.237.245.136', '190.237.245.136'),
(82, 'Juan Calderón More', '23', 'CAIU 554907-9', '22789', 'ACTUALIZAR', '2017-06-13', '08:01:20', '190.237.245.136', '190.237.245.136'),
(83, 'Juan Calderón More', '23', 'TRIU 811487-2', '22788', 'ACTUALIZAR', '2017-06-13', '08:02:16', '190.237.245.136', '190.237.245.136'),
(84, 'Juan Calderón More', '23', 'TRIU 835537-6', '22787', 'ACTUALIZAR', '2017-06-13', '08:03:50', '190.237.245.136', '190.237.245.136'),
(85, 'Juan Calderón More', '23', 'TRIU 835537-6', '22787', 'ACTUALIZAR', '2017-06-13', '08:03:51', '190.237.245.136', '190.237.245.136'),
(86, 'Juan Calderón More', '23', 'HLXU 877275-9', '1', 'ACTUALIZAR', '2017-06-13', '08:06:41', '190.237.245.136', '190.237.245.136'),
(87, 'Juan Calderón More', '23', 'HLXU 877275-9', '1', 'ACTUALIZAR', '2017-06-13', '08:06:41', '190.237.245.136', '190.237.245.136'),
(88, 'Juan Calderón More', '23', 'HLXU 877275-9', '1', 'ACTUALIZAR', '2017-06-13', '08:07:43', '190.237.245.136', '190.237.245.136'),
(89, 'Juan Calderón More', '23', 'HLXU 876189-9', '2', 'INSERTAR', '2017-06-13', '08:10:42', '190.237.245.136', '190.237.245.136'),
(90, 'Juan Calderón More', '23', 'HLXU 876189-9', '2', 'ACTUALIZAR', '2017-06-13', '08:12:31', '190.237.245.136', '190.237.245.136'),
(91, 'Juan Calderón More', '23', 'CPSU 515501-3', '1', 'ACTUALIZAR', '2017-06-13', '08:16:19', '190.237.245.136', '190.237.245.136'),
(92, 'Juan Calderón More', '23', 'HLXU 874770-9', '2', 'INSERTAR', '2017-06-13', '08:19:20', '190.237.245.136', '190.237.245.136'),
(93, 'Juan Calderón More', '23', 'HLXU 874770-9', '2', 'ACTUALIZAR', '2017-06-13', '08:19:48', '190.237.245.136', '190.237.245.136'),
(94, 'Juan Calderón More', '23', 'HLXU 876189-9', '1', 'ACTUALIZAR', '2017-06-13', '08:20:42', '190.237.245.136', '190.237.245.136'),
(95, 'Juan Calderón More', '23', 'HLXU 876189-9', '2', 'ACTUALIZAR', '2017-06-13', '08:22:01', '190.237.245.136', '190.237.245.136'),
(96, 'Juan Calderón More', '23', 'CPSU 515501-3', '1', 'INSERTAR', '2017-06-13', '08:24:20', '190.237.245.136', '190.237.245.136'),
(97, 'Juan Calderón More', '23', 'TLLU 104789-0', '1', 'INSERTAR', '2017-06-13', '08:27:25', '190.237.245.136', '190.237.245.136'),
(98, 'Juan Calderón More', '23', 'TLLU 104789-0', '1', 'INSERTAR', '2017-06-13', '08:27:25', '190.237.245.136', '190.237.245.136'),
(99, 'Juan Calderón More', '23', 'DFIU 811104-6', '2', 'ACTUALIZAR', '2017-06-13', '08:29:56', '190.237.245.136', '190.237.245.136'),
(100, 'Juan Calderón More', '23', 'SUDU 623810-3', '1', 'INSERTAR', '2017-06-13', '08:32:49', '190.237.245.136', '190.237.245.136'),
(101, 'Juan Calderón More', '23', 'SUDU 623810-3', '1', 'INSERTAR', '2017-06-13', '08:32:49', '190.237.245.136', '190.237.245.136'),
(102, 'Juan Calderón More', '23', 'SUDU 627365-5', '2', 'ACTUALIZAR', '2017-06-13', '08:34:10', '190.237.245.136', '190.237.245.136'),
(103, 'Juan Calderón More', '23', 'CPSU 515501-3', '1', 'ACTUALIZAR', '2017-06-13', '08:34:51', '190.237.245.136', '190.237.245.136'),
(104, 'Juan Calderón More', '23', 'HLXU 874770-9', '2', 'ACTUALIZAR', '2017-06-13', '08:35:12', '190.237.245.136', '190.237.245.136'),
(105, 'Juan Calderón More', '23', 'SUDU 625143-0', '22790', 'ACTUALIZAR', '2017-06-13', '08:36:22', '190.237.245.136', '190.237.245.136'),
(106, 'Juan Calderón More', '23', 'TRIU 835537-6', '22787', 'ACTUALIZAR', '2017-06-13', '09:22:37', '190.237.245.136', '190.237.245.136'),
(107, 'Juan Calderón More', '23', 'CPSU 515501-3', '1', 'ACTUALIZAR', '2017-06-13', '09:24:12', '190.237.245.136', '190.237.245.136'),
(108, 'Juan Calderón More', '23', 'CPSU 515501-3', '1', 'ELIMINAR', '2017-06-13', '09:24:15', '190.237.245.136', '190.237.245.136'),
(109, 'Juan Calderón More', '23', 'MMAU 124524-0', '22798', 'ACTUALIZAR', '2017-06-13', '09:25:22', '190.237.245.136', '190.237.245.136'),
(110, 'Juan Calderón More', '24', 'MWCU 678940-0', '22904', 'ACTUALIZAR', '2017-06-19', '11:09:33', '190.239.128.226', '190.239.128.226'),
(111, 'Juan Calderón More', '24', 'MNBU 365704-0', '22903', 'ACTUALIZAR', '2017-06-19', '11:11:04', '190.239.128.226', '190.239.128.226'),
(112, 'Juan Calderón More', '24', 'MMAU 111927-8', '22905', 'ACTUALIZAR', '2017-06-19', '11:15:27', '190.239.128.226', '190.239.128.226'),
(113, 'Juan Calderón More', '24', 'SUDU 803505-0', '22898', 'ACTUALIZAR', '2017-06-19', '11:17:19', '190.239.128.226', '190.239.128.226'),
(114, 'Juan Calderón More', '24', 'SUDU 801152-5', '22899', 'ACTUALIZAR', '2017-06-19', '11:19:12', '190.239.128.226', '190.239.128.226'),
(115, 'Juan Calderón More', '24', 'SUDU 628780-7', '22900', 'ACTUALIZAR', '2017-06-19', '11:20:36', '190.239.128.226', '190.239.128.226'),
(116, 'Juan Calderón More', '24', 'SUDU 629428-3', '22901', 'ACTUALIZAR', '2017-06-19', '11:22:16', '190.239.128.226', '190.239.128.226'),
(117, 'Juan Calderón More', '24', 'SUDU 801981-9', '22902', 'ACTUALIZAR', '2017-06-19', '11:24:49', '190.239.128.226', '190.239.128.226'),
(118, 'Juan Calderón More', '24', 'CAIU 556413-4', '22896', 'ACTUALIZAR', '2017-06-19', '11:26:41', '190.239.128.226', '190.239.128.226'),
(119, 'Juan Calderón More', '24', 'CAIU 556259-5', '22895', 'ACTUALIZAR', '2017-06-19', '11:27:55', '190.239.128.226', '190.239.128.226'),
(120, 'Juan Calderón More', '24', 'SZLU 980178-6', '22897', 'ACTUALIZAR', '2017-06-19', '11:29:50', '190.239.128.226', '190.239.128.226'),
(121, 'Juan Calderón More', '24', 'HLXU 875946-4', '1', 'INSERTAR', '2017-06-19', '11:32:44', '190.239.128.226', '190.239.128.226'),
(122, 'Juan Calderón More', '24', 'HLXU 875946-4', '1', 'INSERTAR', '2017-06-19', '11:32:44', '190.239.128.226', '190.239.128.226'),
(123, 'Juan Calderón More', '24', 'HLXU 875946-4', '2', 'ACTUALIZAR', '2017-06-19', '11:33:38', '190.239.128.226', '190.239.128.226'),
(124, 'Juan Calderón More', '24', 'TCLU 117750-8', '1', 'INSERTAR', '2017-06-19', '11:35:56', '190.239.128.226', '190.239.128.226'),
(125, 'Juan Calderón More', '24', 'TCLU 117750-8', '1', 'INSERTAR', '2017-06-19', '11:35:57', '190.239.128.226', '190.239.128.226'),
(126, 'Juan Calderón More', '24', 'HLBU 900823-9', '2', 'ACTUALIZAR', '2017-06-19', '11:36:24', '190.239.128.226', '190.239.128.226'),
(127, 'Juan Calderón More', '24', 'HLBU 902370-0', '2', 'ACTUALIZAR', '2017-06-19', '11:37:19', '190.239.128.226', '190.239.128.226'),
(128, 'Juan Calderón More', '24', 'SUDU 810704-1', '2', 'INSERTAR', '2017-06-19', '11:39:47', '190.239.128.226', '190.239.128.226'),
(129, 'Juan Calderón More', '24', 'SUDU 803122-3', '2', 'INSERTAR', '2017-06-19', '11:41:29', '190.239.128.226', '190.239.128.226'),
(130, 'Juan Calderón More', '24', 'TLLU 105911-8', '1', 'INSERTAR', '2017-06-19', '11:44:01', '190.239.128.226', '190.239.128.226'),
(131, 'Juan Calderón More', '24', 'TLLU 105911-8', '1', 'INSERTAR', '2017-06-19', '11:44:02', '190.239.128.226', '190.239.128.226'),
(132, 'Juan Calderón More', '24', 'BMOU 964121-8', '1', 'ACTUALIZAR', '2017-06-19', '11:44:48', '190.239.128.226', '190.239.128.226'),
(133, 'Juan Calderón More', '24', 'BMOU 964121-8', '2', 'ACTUALIZAR', '2017-06-19', '11:45:15', '190.239.128.226', '190.239.128.226'),
(134, 'Juan Calderón More', '25', 'PONU 483173-3', '23004', 'ACTUALIZAR', '2017-06-26', '10:09:55', '190.232.101.51', '190.232.101.51'),
(135, 'Juan Calderón More', '25', 'PONU 484117-7', '23005', 'ACTUALIZAR', '2017-06-26', '10:11:55', '190.232.101.51', '190.232.101.51'),
(136, 'Juan Calderón More', '25', 'MMAU 101782-5', '23006', 'ACTUALIZAR', '2017-06-26', '10:13:23', '190.232.101.51', '190.232.101.51'),
(137, 'Juan Calderón More', '25', 'SUDU 817060-9', '23003', 'ACTUALIZAR', '2017-06-26', '10:15:16', '190.232.101.51', '190.232.101.51'),
(138, 'Juan Calderón More', '25', 'SUDU 811441-5', '23000', 'ACTUALIZAR', '2017-06-26', '10:17:36', '190.232.101.51', '190.232.101.51'),
(139, 'Juan Calderón More', '25', 'SUDU 821029-7', '23001', 'ACTUALIZAR', '2017-06-26', '10:23:03', '190.232.101.51', '190.232.101.51'),
(140, 'Juan Calderón More', '25', 'SUDU 805116-9', '23002', 'ACTUALIZAR', '2017-06-26', '10:25:04', '190.232.101.51', '190.232.101.51'),
(141, 'Juan Calderón More', '25', 'TRIU 859069-4', '22998', 'ACTUALIZAR', '2017-06-26', '10:26:55', '190.232.101.51', '190.232.101.51'),
(142, 'Juan Calderón More', '25', 'CXRU 152387-1', '22999', 'ACTUALIZAR', '2017-06-26', '10:29:43', '190.232.101.51', '190.232.101.51'),
(143, 'Juan Calderón More', '25', 'CXRU 111677-3', '22997', 'ACTUALIZAR', '2017-06-26', '10:31:08', '190.232.101.51', '190.232.101.51'),
(144, 'Juan Calderón More', '25', 'HLBU 904294-8', '1', 'INSERTAR', '2017-06-26', '10:33:31', '190.232.101.51', '190.232.101.51'),
(145, 'Juan Calderón More', '25', 'HLBU 904294-8', '1', 'INSERTAR', '2017-06-26', '10:33:32', '190.232.101.51', '190.232.101.51'),
(146, 'Juan Calderón More', '25', 'HLBU 904186-0', '2', 'ACTUALIZAR', '2017-06-26', '10:35:25', '190.232.101.51', '190.232.101.51'),
(147, 'Juan Calderón More', '25', 'HLBU 904294-8', '1', 'ACTUALIZAR', '2017-06-26', '10:35:46', '190.232.101.51', '190.232.101.51'),
(148, 'Juan Calderón More', '25', 'HLBU 904294-8', '1', 'ACTUALIZAR', '2017-06-26', '10:36:07', '190.232.101.51', '190.232.101.51'),
(149, 'Juan Calderón More', '25', 'HLBU 904186-0', '2', 'ACTUALIZAR', '2017-06-26', '10:36:37', '190.232.101.51', '190.232.101.51'),
(150, 'Juan Calderón More', '25', 'HLBU 904186-0', '2', 'ELIMINAR', '2017-06-26', '10:36:38', '190.232.101.51', '190.232.101.51'),
(151, 'Juan Calderón More', '25', 'HLBU 904186-0', '2', 'INSERTAR', '2017-06-26', '10:38:43', '190.232.101.51', '190.232.101.51'),
(152, 'Juan Calderón More', '25', 'LNXU 845859-8', '1', 'INSERTAR', '2017-06-26', '11:02:02', '190.232.101.51', '190.232.101.51'),
(153, 'Juan Calderón More', '25', 'LNXU 845859-8', '1', 'INSERTAR', '2017-06-26', '11:02:02', '190.232.101.51', '190.232.101.51'),
(154, 'Juan Calderón More', '25', 'CPSU 511286-0', '2', 'ACTUALIZAR', '2017-06-26', '11:02:56', '190.232.101.51', '190.232.101.51'),
(155, 'Juan Calderón More', '25', 'CAIU 558762-8', '1', 'INSERTAR', '2017-06-26', '11:05:59', '190.232.101.51', '190.232.101.51'),
(156, 'Juan Calderón More', '25', 'CAIU 558762-8', '1', 'INSERTAR', '2017-06-26', '11:05:59', '190.232.101.51', '190.232.101.51'),
(157, 'Juan Calderón More', '25', 'DFIU 723082-1', '2', 'ACTUALIZAR', '2017-06-26', '11:08:38', '190.232.101.51', '190.232.101.51'),
(158, 'Juan Calderón More', '25', 'SUDU 803934-8', '1', 'INSERTAR', '2017-06-26', '11:10:53', '190.232.101.51', '190.232.101.51'),
(159, 'Juan Calderón More', '25', 'SUDU 803934-8', '1', 'INSERTAR', '2017-06-26', '11:10:54', '190.232.101.51', '190.232.101.51'),
(160, 'Juan Calderón More', '25', 'SUDU 810355-5', '2', 'ACTUALIZAR', '2017-06-26', '11:12:05', '190.232.101.51', '190.232.101.51'),
(161, 'Juan Calderón More', '25', 'HLBU 904186-0', '2', 'ACTUALIZAR', '2017-06-26', '11:12:34', '190.232.101.51', '190.232.101.51'),
(162, 'Juan Calderón More', '25', 'HLBU 904294-8', '1', 'ACTUALIZAR', '2017-06-26', '11:12:45', '190.232.101.51', '190.232.101.51'),
(163, 'Juan Calderón More', '25', 'SUDU 817060-9', '23003', 'ACTUALIZAR', '2017-06-26', '11:22:34', '190.232.101.51', '190.232.101.51'),
(164, 'Juan Calderón More', '25', 'SUDU 811441-5', '23000', 'ACTUALIZAR', '2017-06-26', '11:22:44', '190.232.101.51', '190.232.101.51'),
(165, 'Juan Calderón More', '25', 'PONU 484117-7', '23005', 'ACTUALIZAR', '2017-06-27', '09:41:46', '190.232.101.51', '190.232.101.51'),
(166, 'Juan Calderón More', '25', 'MMAU 101782-5', '23006', 'ACTUALIZAR', '2017-06-27', '09:42:26', '190.232.101.51', '190.232.101.51'),
(167, 'Juan Calderón More', '25', 'DFIU 723082-1', '2', 'ACTUALIZAR', '2017-06-27', '09:47:49', '190.232.101.51', '190.232.101.51'),
(168, 'Juan Calderón More', '25', 'CAIU 558762-8', '1', 'ACTUALIZAR', '2017-06-27', '09:48:02', '190.232.101.51', '190.232.101.51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_estado`
--

CREATE TABLE `audit_estado` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_linea_naviera`
--

CREATE TABLE `audit_linea_naviera` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_nave`
--

CREATE TABLE `audit_nave` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_operador`
--

CREATE TABLE `audit_operador` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_operador`
--

INSERT INTO `audit_operador` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(14, 'Juan Calderón More', 'TPSAC', 'ELIMINAR', '2017-05-29', '10:34:35', '179.7.132.187', '179.7.132.187'),
(15, 'APPBOSA SAMAN SAMAN', 'Jeniff SAC', 'INSERTAR', '2017-07-08', '10:24:42', '::1', 'Luis'),
(16, 'APPBOSA SAMAN SAMAN', 'Jeniff Ivonne SAC', 'ACTUALIZAR', '2017-07-08', '10:27:17', '::1', 'Luis'),
(17, 'APPBOSA SAMAN SAMAN', 'Jeniff SAC', 'ACTUALIZAR', '2017-07-08', '10:29:16', '::1', 'Luis'),
(18, 'APPBOSA SAMAN SAMAN', 'Jeniff SAC', 'ELIMINAR', '2017-07-08', '10:32:40', '::1', 'Luis'),
(19, 'APPBOSA SAMAN SAMAN', 'LUIS TRANSAPORTATION S.A.C.', 'INSERTAR', '2017-07-08', '10:33:44', '::1', 'Luis'),
(20, 'APPBOSA SAMAN SAMAN', 'LUIS TRANSAPORTATION S.A.C.', 'ELIMINAR', '2017-07-08', '10:35:18', '::1', 'Luis'),
(21, 'APPBOSA SAMAN SAMAN', 'DARWIN EXPRESS', 'INSERTAR', '2017-07-08', '10:36:00', '::1', 'Luis'),
(22, 'APPBOSA SAMAN SAMAN', 'DARWIN EXPRESS SAC', 'ACTUALIZAR', '2017-07-08', '10:37:06', '::1', 'Luis'),
(23, 'APPBOSA SAMAN SAMAN', 'DARWIN EXPRESS SAC', 'ELIMINAR', '2017-07-08', '10:41:25', '::1', 'Luis'),
(24, 'APPBOSA SAMAN SAMAN', 'GRUPO ALYSSA', 'INSERTAR', '2017-07-08', '10:41:58', '::1', 'Luis'),
(25, 'APPBOSA SAMAN SAMAN', 'GRUPO ALYSSA SAC', 'ACTUALIZAR', '2017-07-08', '10:57:40', '::1', 'Luis'),
(26, 'APPBOSA SAMAN SAMAN', 'GRUPO ALYSSA SAC', 'ACTUALIZAR', '2017-07-08', '11:00:50', '::1', 'Luis'),
(27, 'APPBOSA SAMAN SAMAN', 'GRUPO ALYSSA SAC 12', 'ACTUALIZAR', '2017-07-08', '11:02:19', '::1', 'Luis'),
(28, 'APPBOSA SAMAN SAMAN', 'GRUPO ALYSSA SAC', 'ACTUALIZAR', '2017-07-08', '11:03:28', '::1', 'Luis'),
(29, 'APPBOSA SAMAN SAMAN', 'Darwin sac', 'INSERTAR', '2017-07-08', '11:27:09', '::1', 'Luis'),
(30, 'APPBOSA SAMAN SAMAN', 'GRUPO ALYSSA SAC', 'ELIMINAR', '2017-07-08', '11:27:32', '::1', 'Luis'),
(31, 'APPBOSA SAMAN SAMAN', 'Darwin sac', 'ELIMINAR', '2017-07-08', '11:27:36', '::1', 'Luis'),
(32, 'APPBOSA SAMAN SAMAN', 'AFE TRANSPORTATION S.A.C.', 'ACTUALIZAR', '2017-07-08', '11:51:32', '::1', 'Luis'),
(33, 'APPBOSA SAMAN SAMAN', 'BLUE EXPRESS', 'ACTUALIZAR', '2017-07-08', '11:52:11', '::1', 'Luis'),
(34, 'APPBOSA SAMAN SAMAN', 'BLUE EXPRESS RED', 'ACTUALIZAR', '2017-07-08', '11:54:02', '::1', 'Luis'),
(35, 'APPBOSA SAMAN SAMAN', 'JeniffSAC', 'INSERTAR', '2017-07-08', '11:57:28', '::1', 'Luis'),
(36, 'APPBOSA SAMAN SAMAN', 'AFE TRANSPORTATION S.A.C.', 'ACTUALIZAR', '2017-07-08', '12:09:36', '::1', 'Luis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_pais`
--

CREATE TABLE `audit_pais` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_puerto_destino`
--

CREATE TABLE `audit_puerto_destino` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_puerto_destino`
--

INSERT INTO `audit_puerto_destino` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(21, 'Juan Calderón More', 'AMBERES', 'INSERTAR', '2017-05-31', '12:37:56', '181.67.4.220', '181.67.4.220');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_semana`
--

CREATE TABLE `audit_semana` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_semana`
--

INSERT INTO `audit_semana` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(14, 'Luis Guillermo Ramirez Coronado', '2', 'INSERTAR', '2017-05-22', '19:35:13', '190.236.31.16', '190.236.31.16'),
(15, 'Luis Guillermo Ramirez Coronado', '4', 'INSERTAR', '2017-05-22', '19:35:54', '190.236.31.16', '190.236.31.16'),
(16, 'Juan Calderón More', '2', 'ELIMINAR', '2017-05-23', '16:23:29', '179.7.139.187', '179.7.139.187'),
(17, 'Juan Calderón More', '4', 'ELIMINAR', '2017-05-23', '16:23:33', '179.7.139.187', '179.7.139.187'),
(18, 'Juan Calderón More', '20', 'INSERTAR', '2017-05-23', '16:24:33', '179.7.139.187', '179.7.139.187'),
(19, 'Juan Calderón More', '22', 'INSERTAR', '2017-05-29', '09:58:55', '179.7.132.187', '179.7.132.187'),
(20, 'Juan Calderón More', '21', 'INSERTAR', '2017-05-29', '10:49:28', '179.7.132.187', '179.7.132.187'),
(21, 'APPBOSA SAMAN SAMAN', '22', 'ELIMINAR', '2017-05-30', '07:16:04', '190.117.137.3', '190.117.137.3'),
(22, 'APPBOSA SAMAN SAMAN', '20', 'ELIMINAR', '2017-05-30', '07:16:09', '190.117.137.3', '190.117.137.3'),
(23, 'Juan Calderón More', '22', 'INSERTAR', '2017-05-30', '08:38:59', '179.7.132.187', '179.7.132.187'),
(24, 'Juan Calderón More', '20', 'INSERTAR', '2017-05-30', '10:46:57', '179.7.132.187', '179.7.132.187'),
(25, 'Juan Calderón More', '19', 'INSERTAR', '2017-05-30', '12:18:08', '179.7.132.187', '179.7.132.187'),
(26, 'Juan Calderón More', '18', 'INSERTAR', '2017-05-31', '08:23:16', '181.67.4.220', '181.67.4.220'),
(27, 'Juan Calderón More', '17', 'INSERTAR', '2017-05-31', '10:03:15', '181.67.4.220', '181.67.4.220'),
(28, 'Juan Calderón More', '16', 'INSERTAR', '2017-05-31', '11:33:14', '181.67.4.220', '181.67.4.220'),
(29, 'Juan Calderón More', '15', 'INSERTAR', '2017-06-01', '07:25:02', '190.237.186.168', '190.237.186.168'),
(30, 'Juan Calderón More', '14', 'INSERTAR', '2017-06-01', '08:47:37', '190.237.186.168', '190.237.186.168'),
(31, 'Juan Calderón More', '13', 'INSERTAR', '2017-06-01', '09:34:21', '190.237.186.168', '190.237.186.168'),
(32, 'Juan Calderón More', '12', 'INSERTAR', '2017-06-01', '10:55:06', '190.237.186.168', '190.237.186.168'),
(33, 'Juan Calderón More', '11', 'INSERTAR', '2017-06-02', '07:20:58', '190.237.186.168', '190.237.186.168'),
(34, 'Juan Calderón More', '10', 'INSERTAR', '2017-06-02', '09:53:18', '190.237.186.168', '190.237.186.168'),
(35, 'Juan Calderón More', '9', 'INSERTAR', '2017-06-02', '11:24:26', '190.237.186.168', '190.237.186.168'),
(36, 'Juan Calderón More', '9', 'ELIMINAR', '2017-06-02', '11:24:39', '190.237.186.168', '190.237.186.168'),
(37, 'Juan Calderón More', '9', 'INSERTAR', '2017-06-02', '11:24:51', '190.237.186.168', '190.237.186.168'),
(38, 'Juan Calderón More', '8', 'INSERTAR', '2017-06-05', '08:30:23', '190.237.186.168', '190.237.186.168'),
(39, 'Juan Calderón More', '7', 'INSERTAR', '2017-06-05', '10:41:01', '190.237.186.168', '190.237.186.168'),
(40, 'Juan Calderón More', '5', 'INSERTAR', '2017-06-05', '12:21:14', '190.237.186.168', '190.237.186.168'),
(41, 'Juan Calderón More', '4', 'INSERTAR', '2017-06-05', '12:21:20', '190.237.186.168', '190.237.186.168'),
(42, 'Juan Calderón More', '1', 'INSERTAR', '2017-06-05', '12:21:35', '190.237.186.168', '190.237.186.168'),
(43, 'Juan Calderón More', '2', 'INSERTAR', '2017-06-05', '12:22:21', '190.237.186.168', '190.237.186.168'),
(44, 'Juan Calderón More', '3', 'INSERTAR', '2017-06-05', '12:22:39', '190.237.186.168', '190.237.186.168'),
(45, 'Juan Calderón More', '6', 'INSERTAR', '2017-06-06', '07:16:08', '190.237.186.168', '190.237.186.168'),
(46, 'Juan Calderón More', '23', 'INSERTAR', '2017-06-06', '12:29:49', '190.237.186.168', '190.237.186.168'),
(47, 'Juan Calderón More', '24', 'INSERTAR', '2017-06-09', '11:31:07', '190.43.242.226', '190.43.242.226'),
(48, 'Juan Calderón More', '25', 'INSERTAR', '2017-06-15', '10:25:43', '201.240.113.132', 'client-201.240.113.132.speedy.net.pe'),
(49, 'Juan Calderón More', '26', 'INSERTAR', '2017-06-26', '07:32:51', '190.232.101.51', '190.232.101.51'),
(50, 'Juan Calderón More', '27', 'INSERTAR', '2017-06-30', '09:05:59', '190.232.101.51', '190.232.101.51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_usuario`
--

CREATE TABLE `audit_usuario` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(255) NOT NULL,
  `usuario_afectado` varchar(255) NOT NULL,
  `operacion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_usuario`
--

INSERT INTO `audit_usuario` (`id`, `usuario_responsable`, `usuario_afectado`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(10, 'APPBOSA SAMAN SAMAN', 'Juan Calderón More', 'ACTUALIZAR', '2017-05-23', '15:41:08', '179.7.139.187', '179.7.139.187'),
(11, 'APPBOSA SAMAN SAMAN', 'Nilton Anterio Ancajima Castro', 'INSERTAR', '2017-05-23', '15:57:09', '179.7.139.187', '179.7.139.187'),
(12, 'APPBOSA SAMAN SAMAN', 'Luis Alberto Mejias Escobar', 'INSERTAR', '2017-05-23', '16:12:02', '179.7.139.187', '179.7.139.187'),
(13, 'APPBOSA SAMAN SAMAN', 'Juan Calderón More', 'ACTUALIZAR', '2017-05-23', '16:15:56', '179.7.139.187', '179.7.139.187'),
(14, 'APPBOSA SAMAN SAMAN', 'Juan Calderón More', 'ACTUALIZAR', '2017-05-29', '09:21:33', '179.7.132.187', '179.7.132.187'),
(15, 'APPBOSA SAMAN SAMAN', 'Jeniff SAC Jeniff SAC Jeniff SAC', 'ACTUALIZAR', '2017-07-08', '10:32:00', '::1', 'Luis'),
(16, 'APPBOSA SAMAN SAMAN', 'LUIS TRANSAPORTATION S.A.C.', 'ELIMINAR', '2017-07-08', '10:35:07', '::1', 'Luis'),
(17, 'APPBOSA SAMAN SAMAN', 'DARWIN EXPRESS DARWIN EXPRESS DARWIN EXPRESS', 'ACTUALIZAR', '2017-07-08', '10:40:55', '::1', 'Luis'),
(18, 'APPBOSA SAMAN SAMAN', 'GRUPO ALYSSA GRUPO ALYSSA GRUPO ALYSSA', 'ACTUALIZAR', '2017-07-08', '10:43:26', '::1', 'Luis'),
(19, 'APPBOSA SAMAN SAMAN', 'GRUPO ALYSSA GRUPO ALYSSA GRUPO ALYSSA', 'ACTUALIZAR', '2017-07-08', '10:43:44', '::1', 'Luis'),
(20, 'APPBOSA SAMAN SAMAN', 'GRUPO ALYSSA SAC GRUPO ALYSSA SAC GRUPO ALYSSA SAC', 'ACTUALIZAR', '2017-07-08', '11:03:49', '::1', 'Luis'),
(21, 'APPBOSA SAMAN SAMAN', 'Jeniffer Viera Justiniano', 'INSERTAR', '2017-07-08', '11:06:24', '::1', 'Luis'),
(22, 'APPBOSA SAMAN SAMAN', 'Jeniffer Viera Justiniano', 'ELIMINAR', '2017-07-08', '11:06:31', '::1', 'Luis'),
(23, 'APPBOSA SAMAN SAMAN', 'BLUE EXPRESS RED BLUE EXPRESS RED BLUE EXPRESS RED', 'ACTUALIZAR', '2017-07-08', '11:55:13', '::1', 'Luis'),
(24, 'APPBOSA SAMAN SAMAN', 'AFE TRANSPORTATION S.A.C. AFE TRANSPORTATION S.A.C. AFE TRANSPORTATION S.A.C.', 'ACTUALIZAR', '2017-07-08', '12:11:31', '::1', 'Luis'),
(25, 'APPBOSA SAMAN SAMAN', 'AFE TRANSPORTATION S.A.C.  ', 'ACTUALIZAR', '2017-07-08', '12:13:12', '::1', 'Luis'),
(26, 'APPBOSA SAMAN SAMAN', 'TRANSASTRA  ', 'ACTUALIZAR', '2017-07-08', '12:13:20', '::1', 'Luis'),
(27, 'APPBOSA SAMAN SAMAN', 'EQUIFRUIT  ', 'ACTUALIZAR', '2017-07-08', '12:13:27', '::1', 'Luis'),
(28, 'APPBOSA SAMAN SAMAN', 'BIODYNAMISKA  ', 'ACTUALIZAR', '2017-07-08', '12:14:06', '::1', 'Luis'),
(29, 'APPBOSA SAMAN SAMAN', 'DOLE  ', 'ACTUALIZAR', '2017-07-08', '12:14:24', '::1', 'Luis'),
(30, 'APPBOSA SAMAN SAMAN', 'AGROFAIR  ', 'ACTUALIZAR', '2017-07-08', '12:15:32', '::1', 'Luis'),
(31, 'APPBOSA SAMAN SAMAN', 'BLUE EXPRESS RED  ', 'ACTUALIZAR', '2017-07-08', '12:16:05', '::1', 'Luis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `booking`
--

INSERT INTO `booking` (`id`, `codigo`, `activo`) VALUES
(1, '572123443', 1),
(2, '087LIM227108 ', 1),
(3, '087LIM227108 ', 1),
(4, '98171199', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id` int(11) NOT NULL,
  `marca` varchar(50) NOT NULL,
  `peso` decimal(10,2) NOT NULL DEFAULT '18.14',
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`id`, `marca`, `peso`, `activo`) VALUES
(1, 'A. HEIJN', '18.14', 1),
(2, 'ALTOMERCATO', '18.14', 1),
(3, 'BIO BIO FAIRTRADE', '18.14', 1),
(4, 'CLUSTERBAGS', '18.14', 1),
(5, 'EDEKA BIO FAIRTRADE', '18.14', 1),
(6, 'EDEKA BIO ORGANICO', '18.14', 1),
(7, 'EKOOKE FAIRTRADE', '18.14', 1),
(8, 'EQUIFRUIT FLO', '18.14', 1),
(9, 'EKOOKE VERDE', '18.14', 1),
(10, 'SAMAN FT', '18.14', 1),
(11, 'SAMAN ORGANIC SPP', '18.14', 1),
(12, 'SPAR', '18.14', 1),
(13, 'YC. BIO GERMANY FLO', '18.14', 1),
(14, 'YC. SPAR  FLO', '18.14', 1),
(15, 'KOREA 13.5 KG', '13.50', 1),
(16, 'DOLE  JAPON 13 KG', '18.14', 1),
(17, 'CHIQUITA NORMAL', '18.14', 1),
(18, 'CHIQUITA AL VACIO FLO', '18.14', 1),
(20, 'SAMAN FLO ORGANIC', '18.14', 1),
(21, 'WHOLE TRADE', '18.14', 1),
(22, 'ALBERTH HEIJING', '18.14', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_contenedor`
--

CREATE TABLE `caja_contenedor` (
  `id` int(11) NOT NULL,
  `contenedor_id` int(11) NOT NULL,
  `caja_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_contenedor`
--

INSERT INTO `caja_contenedor` (`id`, `contenedor_id`, `caja_id`, `cantidad`) VALUES
(58, 26, 9, 864),
(59, 26, 7, 216),
(60, 27, 17, 1080),
(61, 28, 5, 1080),
(62, 29, 5, 1080),
(63, 30, 5, 1080),
(64, 31, 5, 1080),
(65, 32, 15, 1400),
(66, 33, 6, 1080),
(67, 34, 7, 1080),
(68, 35, 7, 1080),
(69, 36, 7, 1080),
(70, 37, 7, 1080),
(71, 38, 7, 1080),
(72, 39, 9, 864),
(73, 39, 7, 216),
(74, 41, 20, 1080),
(75, 42, 20, 540),
(76, 42, 11, 540),
(77, 43, 1, 1080),
(78, 44, 12, 1080),
(79, 45, 13, 1080),
(80, 46, 8, 960),
(81, 47, 8, 960),
(82, 48, 13, 1080),
(83, 49, 5, 1080),
(84, 50, 5, 1080),
(85, 51, 5, 1080),
(86, 52, 15, 1400),
(87, 53, 7, 1083),
(88, 54, 7, 1080),
(89, 55, 7, 1080),
(90, 56, 7, 1080),
(91, 57, 7, 1080),
(95, 61, 13, 960),
(96, 62, 13, 960),
(98, 64, 13, 960),
(99, 65, 8, 960),
(100, 66, 5, 1080),
(101, 67, 5, 1080),
(102, 68, 15, 1400),
(103, 69, 6, 1080),
(104, 70, 7, 1080),
(105, 71, 7, 1080),
(106, 72, 7, 1080),
(107, 73, 7, 11080),
(108, 74, 7, 1080),
(109, 75, 7, 1080),
(110, 76, 7, 1080),
(111, 77, 9, 864),
(112, 77, 7, 216),
(113, 78, 10, 1080),
(114, 79, 10, 540),
(115, 79, 11, 540),
(116, 80, 1, 1080),
(117, 82, 12, 1080),
(118, 83, 8, 960),
(119, 84, 21, 960),
(120, 85, 21, 960),
(121, 86, 5, 1080),
(122, 87, 5, 1080),
(123, 88, 5, 1080),
(124, 89, 5, 1080),
(125, 90, 15, 1400),
(126, 91, 3, 1079),
(127, 92, 3, 1080),
(128, 93, 3, 1080),
(129, 94, 7, 1080),
(130, 95, 7, 1080),
(131, 96, 7, 216),
(132, 96, 9, 864),
(133, 97, 7, 1080),
(134, 98, 10, 1080),
(135, 99, 10, 540),
(136, 99, 11, 540),
(137, 100, 1, 1080),
(138, 101, 12, 1080),
(139, 102, 21, 960),
(140, 103, 21, 960),
(141, 104, 8, 960),
(142, 105, 8, 960),
(143, 106, 8, 960),
(144, 107, 5, 1080),
(145, 108, 5, 1080),
(146, 109, 5, 1080),
(147, 110, 15, 1400),
(148, 111, 5, 1080),
(149, 112, 5, 1080),
(150, 113, 7, 1080),
(151, 114, 7, 1080),
(152, 115, 7, 1080),
(153, 116, 3, 1080),
(154, 117, 7, 1080),
(155, 118, 7, 216),
(156, 118, 9, 864),
(157, 119, 10, 1080),
(158, 120, 10, 540),
(159, 120, 11, 540),
(160, 121, 1, 1080),
(161, 122, 12, 1080),
(162, 123, 8, 960),
(163, 124, 13, 1080),
(164, 125, 14, 1080),
(165, 126, 8, 960),
(166, 127, 5, 1080),
(167, 128, 5, 1080),
(168, 129, 5, 1080),
(169, 130, 15, 1400),
(170, 131, 7, 1080),
(171, 132, 7, 1080),
(172, 133, 7, 1080),
(173, 134, 7, 1080),
(174, 135, 7, 1080),
(175, 136, 7, 216),
(176, 136, 9, 864),
(177, 137, 2, 540),
(178, 137, 7, 540),
(179, 138, 10, 1080),
(180, 139, 10, 1080),
(181, 140, 10, 540),
(182, 140, 11, 540),
(183, 141, 12, 1080),
(184, 142, 1, 1080),
(185, 143, 13, 1080),
(186, 144, 8, 960),
(187, 145, 14, 1080),
(188, 146, 8, 960),
(189, 147, 8, 960),
(190, 148, 5, 1080),
(191, 149, 5, 1080),
(192, 150, 15, 1400),
(193, 151, 7, 1080),
(194, 152, 7, 1080),
(195, 153, 7, 1080),
(196, 154, 7, 1080),
(197, 155, 7, 1080),
(198, 156, 7, 1080),
(199, 157, 7, 216),
(200, 157, 9, 864),
(201, 158, 2, 1080),
(202, 159, 10, 1080),
(203, 160, 10, 1080),
(204, 161, 11, 540),
(205, 161, 10, 540),
(206, 162, 1, 1080),
(207, 163, 12, 1080),
(208, 164, 8, 1080),
(209, 165, 8, 1080),
(210, 166, 13, 1080),
(211, 167, 14, 1080),
(212, 168, 5, 1080),
(213, 169, 5, 1080),
(214, 170, 15, 1400),
(215, 171, 7, 1080),
(216, 172, 7, 1080),
(217, 173, 7, 1080),
(218, 174, 7, 1080),
(219, 175, 7, 1080),
(220, 176, 6, 1080),
(221, 177, 6, 1080),
(222, 178, 9, 864),
(223, 178, 7, 216),
(224, 179, 2, 1080),
(225, 180, 14, 1080),
(226, 181, 10, 1080),
(227, 182, 10, 1080),
(228, 183, 14, 1080),
(229, 184, 14, 1080),
(230, 185, 12, 1080),
(231, 186, 1, 1080),
(232, 187, 8, 960),
(233, 188, 21, 960),
(234, 189, 21, 960),
(235, 190, 5, 1080),
(236, 191, 5, 1080),
(237, 192, 15, 1400),
(238, 193, 7, 1080),
(239, 194, 7, 1080),
(240, 195, 7, 1080),
(241, 196, 7, 1080),
(242, 197, 7, 1080),
(243, 198, 7, 1080),
(244, 199, 9, 864),
(245, 199, 7, 216),
(246, 200, 7, 540),
(247, 200, 2, 540),
(248, 201, 10, 1080),
(249, 202, 10, 540),
(250, 202, 11, 540),
(251, 203, 12, 1080),
(252, 204, 1, 1080),
(253, 205, 13, 1080),
(255, 206, 21, 960),
(256, 207, 8, 960),
(257, 208, 3, 1080),
(259, 209, 7, 1080),
(260, 210, 7, 1080),
(261, 211, 5, 1080),
(264, 212, 5, 1080),
(265, 213, 5, 1080),
(266, 214, 10, 1080),
(267, 215, 10, 540),
(268, 215, 11, 540),
(269, 216, 7, 1080),
(270, 217, 7, 1080),
(271, 218, 9, 864),
(272, 218, 7, 216),
(273, 219, 12, 1080),
(274, 220, 12, 1080),
(275, 221, 8, 1080),
(276, 222, 8, 1080),
(277, 223, 13, 1080),
(278, 224, 14, 1080),
(279, 225, 21, 960),
(280, 226, 3, 1080),
(281, 228, 5, 1080),
(282, 229, 15, 1400),
(283, 230, 5, 1080),
(284, 231, 5, 1081),
(285, 232, 7, 1080),
(286, 233, 7, 1080),
(287, 234, 6, 1080),
(288, 235, 7, 1080),
(289, 236, 9, 1080),
(290, 237, 7, 540),
(291, 237, 2, 540),
(292, 238, 10, 1080),
(293, 239, 10, 540),
(294, 239, 11, 540),
(295, 240, 12, 1080),
(296, 241, 12, 1080),
(297, 242, 21, 960),
(298, 243, 21, 960),
(299, 244, 8, 960),
(300, 245, 8, 960),
(301, 246, 3, 1080),
(302, 247, 10, 1080),
(303, 248, 10, 1080),
(304, 249, 10, 540),
(305, 249, 11, 540),
(306, 250, 10, 1080),
(309, 252, 5, 1080),
(310, 251, 5, 1080),
(311, 253, 7, 216),
(312, 253, 9, 864),
(321, 257, 7, 1080),
(322, 256, 7, 1080),
(323, 255, 6, 1080),
(324, 254, 14, 1080),
(325, 258, 12, 1080),
(326, 259, 12, 1080),
(327, 260, 8, 960),
(328, 261, 8, 960),
(329, 262, 8, 960),
(330, 263, 3, 1080),
(331, 264, 3, 1080),
(332, 265, 3, 1080),
(333, 266, 5, 1080),
(334, 267, 5, 1080),
(335, 268, 5, 1080),
(336, 269, 7, 1080),
(337, 270, 7, 1080),
(339, 272, 7, 1080),
(340, 271, 6, 1080),
(341, 273, 9, 864),
(342, 273, 7, 216),
(343, 274, 13, 1080),
(345, 275, 14, 1080),
(346, 276, 10, 540),
(347, 277, 10, 1080),
(348, 276, 11, 540),
(349, 278, 12, 1080),
(350, 279, 12, 1080),
(351, 280, 8, 960),
(352, 281, 15, 1400),
(353, 282, 15, 1400),
(354, 283, 3, 1080),
(356, 285, 3, 1080),
(357, 286, 3, 1080),
(358, 284, 7, 1078),
(359, 287, 7, 1080),
(360, 288, 7, 1080),
(363, 291, 2, 1080),
(365, 290, 7, 1080),
(366, 289, 7, 1080),
(367, 292, 7, 1080),
(369, 293, 7, 432),
(370, 293, 9, 648),
(371, 294, 10, 1080),
(373, 296, 10, 1080),
(374, 297, 10, 1080),
(375, 295, 10, 540),
(376, 295, 11, 540),
(377, 298, 8, 960),
(378, 299, 8, 960),
(379, 300, 14, 1080),
(381, 301, 13, 1080),
(382, 302, 1, 1080),
(384, 303, 12, 1080),
(385, 304, 5, 1080),
(387, 306, 5, 1080),
(388, 305, 3, 1080),
(389, 307, 2, 1080),
(390, 308, 2, 1080),
(391, 309, 7, 1080),
(392, 310, 7, 1080),
(393, 311, 7, 1080),
(394, 312, 7, 1080),
(395, 313, 7, 1080),
(397, 314, 9, 1080),
(398, 315, 5, 1080),
(400, 317, 5, 1080),
(401, 316, 3, 1080),
(402, 318, 10, 1080),
(404, 319, 10, 540),
(405, 319, 11, 540),
(406, 320, 8, 960),
(407, 321, 21, 960),
(408, 322, 21, 960),
(409, 323, 17, 1080),
(410, 324, 17, 1080),
(411, 325, 17, 1080),
(412, 326, 17, 1080),
(413, 327, 17, 1080),
(414, 328, 17, 1080),
(415, 329, 17, 1080),
(419, 333, 2, 1080),
(420, 330, 7, 1080),
(421, 332, 7, 1080),
(422, 331, 7, 1080),
(423, 334, 9, 1080),
(425, 335, 6, 1080),
(426, 336, 7, 1080),
(427, 337, 7, 1080),
(428, 338, 7, 1080),
(429, 339, 7, 1080),
(430, 340, 10, 1080),
(432, 342, 10, 1080),
(433, 341, 10, 540),
(434, 341, 11, 540),
(435, 343, 2, 1080),
(436, 344, 2, 1080),
(437, 345, 8, 960),
(438, 346, 8, 960),
(439, 347, 21, 960),
(440, 348, 21, 960),
(441, 349, 1, 108),
(442, 350, 1, 108),
(443, 351, 3, 1080),
(444, 352, 5, 1080),
(445, 353, 5, 1080),
(446, 354, 5, 1080),
(447, 355, 2, 1080),
(448, 356, 2, 1080),
(452, 358, 7, 1080),
(453, 357, 7, 1080),
(454, 359, 7, 270),
(455, 359, 9, 810),
(456, 360, 7, 1080),
(457, 361, 7, 1080),
(458, 362, 7, 1080),
(459, 363, 7, 1080),
(460, 364, 7, 1080),
(461, 365, 7, 1080),
(462, 366, 7, 1080),
(463, 367, 10, 1080),
(465, 368, 10, 540),
(466, 368, 11, 540),
(467, 369, 8, 960),
(468, 63, 21, 960),
(469, 60, 1, 1080),
(470, 59, 10, 594),
(471, 59, 11, 486),
(472, 58, 7, 216),
(473, 58, 9, 864),
(474, 370, 12, 1080),
(475, 371, 7, 1080),
(476, 372, 3, 1080),
(477, 373, 6, 1080),
(478, 374, 6, 1080),
(479, 375, 6, 1080),
(480, 376, 2, 1080),
(481, 377, 7, 1080),
(482, 378, 7, 1080),
(483, 379, 7, 1080),
(484, 380, 7, 1080),
(485, 381, 7, 1080),
(486, 382, 7, 1080),
(487, 383, 7, 1080),
(488, 384, 7, 1080),
(489, 385, 6, 1080),
(490, 386, 10, 1080),
(491, 387, 10, 1080),
(492, 388, 10, 1080),
(493, 389, 10, 540),
(494, 389, 11, 540),
(495, 390, 8, 960),
(496, 391, 21, 960),
(497, 392, 18, 1080),
(498, 393, 18, 1080),
(499, 394, 3, 1080),
(503, 395, 5, 1080),
(504, 397, 2, 1080),
(505, 396, 5, 1080),
(506, 398, 7, 1081),
(508, 399, 5, 1080),
(509, 400, 7, 1080),
(510, 401, 7, 1080),
(511, 402, 7, 1080),
(512, 403, 7, 1080),
(513, 404, 7, 1080),
(515, 406, 7, 1080),
(516, 407, 7, 1080),
(517, 408, 7, 1080),
(518, 405, 7, 270),
(519, 405, 9, 810),
(520, 409, 2, 1080),
(521, 410, 8, 960),
(522, 411, 8, 960),
(524, 413, 17, 1080),
(525, 414, 17, 1080),
(526, 412, 1, 1080),
(528, 416, 6, 1080),
(530, 418, 6, 1080),
(531, 417, 2, 1080),
(532, 415, 7, 1080),
(533, 419, 3, 1080),
(537, 422, 5, 1080),
(538, 421, 5, 1080),
(539, 420, 7, 1080),
(540, 423, 7, 1080),
(541, 424, 7, 1080),
(542, 425, 7, 1080),
(543, 426, 7, 1080),
(544, 427, 7, 1080),
(547, 430, 7, 1080),
(548, 429, 2, 1080),
(549, 428, 7, 432),
(550, 428, 9, 648),
(552, 432, 10, 108),
(553, 431, 11, 540),
(554, 431, 10, 540),
(555, 433, 8, 960),
(556, 434, 21, 960),
(557, 435, 21, 960),
(558, 436, 1, 1080),
(559, 437, 1, 1080),
(560, 438, 1, 1080),
(561, 439, 6, 1080),
(562, 440, 6, 1080),
(567, 444, 7, 1080),
(568, 443, 7, 1080),
(569, 442, 7, 1080),
(570, 441, 2, 1080),
(573, 447, 3, 1080),
(574, 446, 5, 1080),
(575, 445, 5, 1080),
(576, 448, 7, 1080),
(577, 449, 7, 1080),
(578, 450, 7, 1080),
(579, 451, 9, 864),
(580, 451, 7, 216),
(581, 452, 5, 1080),
(583, 454, 5, 1080),
(584, 455, 15, 1400),
(585, 456, 7, 1080),
(586, 457, 7, 1080),
(587, 458, 7, 1080),
(593, 462, 7, 1080),
(594, 461, 7, 1080),
(596, 459, 7, 216),
(597, 459, 9, 864),
(599, 465, 5, 1080),
(600, 466, 5, 1080),
(603, 467, 7, 1080),
(604, 468, 7, 1080),
(605, 469, 7, 1080),
(606, 470, 7, 1080),
(607, 471, 7, 1080),
(620, 476, 7, 378),
(621, 476, 9, 648),
(622, 476, 4, 54),
(623, 475, 2, 1080),
(624, 474, 10, 1080),
(625, 473, 10, 540),
(626, 473, 11, 540),
(627, 472, 8, 960),
(628, 477, 13, 1078),
(629, 478, 21, 960),
(630, 479, 22, 1080),
(631, 480, 22, 1080),
(634, 481, 17, 1080),
(635, 482, 17, 1080),
(636, 483, 6, 1080),
(639, 486, 6, 1080),
(640, 485, 7, 1080),
(641, 484, 7, 1080),
(642, 487, 7, 1080),
(643, 488, 5, 1080),
(644, 489, 5, 1080),
(646, 490, 3, 1080),
(647, 491, 7, 1080),
(648, 492, 7, 1080),
(650, 494, 7, 1080),
(651, 493, 2, 1080),
(652, 495, 7, 378),
(653, 495, 9, 648),
(654, 495, 4, 54),
(656, 497, 10, 1080),
(657, 498, 10, 1080),
(658, 499, 10, 1080),
(659, 496, 10, 540),
(660, 496, 11, 540),
(661, 500, 8, 960),
(662, 501, 21, 960),
(663, 502, 21, 960),
(664, 503, 1, 1080),
(665, 504, 1, 1080),
(668, 505, 10, 540),
(669, 505, 11, 540),
(670, 453, 12, 1080),
(671, 506, 1, 1080),
(672, 507, 12, 1080),
(673, 508, 13, 1080),
(675, 509, 21, 960),
(676, 510, 8, 960),
(677, 511, 8, 960),
(678, 512, 7, 1080),
(679, 513, 7, 1080),
(681, 514, 7, 1080),
(682, 515, 7, 1080),
(683, 516, 7, 1080),
(686, 518, 9, 864),
(687, 518, 7, 216),
(689, 520, 5, 1080),
(690, 521, 5, 1080),
(691, 519, 15, 1400),
(692, 464, 15, 1400),
(693, 463, 6, 1080),
(694, 460, 7, 1080),
(695, 522, 10, 1080),
(697, 523, 10, 540),
(698, 523, 11, 540),
(699, 524, 12, 1080),
(701, 525, 1, 1080),
(702, 526, 8, 960),
(703, 527, 8, 960),
(704, 528, 13, 1080),
(706, 529, 14, 1080),
(707, 530, 7, 1080),
(708, 531, 7, 1080),
(709, 532, 7, 1080),
(718, 537, 6, 1080),
(720, 536, 7, 1080),
(721, 535, 7, 1080),
(722, 534, 7, 1080),
(723, 533, 7, 216),
(724, 533, 9, 864),
(727, 540, 5, 1080),
(729, 538, 5, 1080),
(730, 539, 15, 1400),
(731, 517, 6, 1080),
(732, 541, 10, 1080),
(734, 542, 10, 540),
(735, 542, 11, 540),
(736, 543, 10, 540),
(737, 543, 11, 540),
(739, 545, 12, 1080),
(740, 544, 1, 1080),
(742, 547, 13, 1080),
(743, 546, 21, 960),
(744, 548, 8, 960),
(745, 549, 8, 960),
(746, 550, 3, 1080),
(748, 551, 7, 1080),
(749, 552, 7, 1080),
(750, 553, 6, 1080),
(751, 554, 7, 1080),
(752, 555, 7, 1080),
(754, 556, 9, 576),
(755, 556, 7, 504),
(756, 557, 3, 1080),
(758, 558, 5, 1080),
(759, 559, 5, 1080),
(761, 560, 15, 1400);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `nombre`, `activo`) VALUES
(1, 'Gerente', 1),
(2, 'Administrador', 1),
(8, 'Cliente', 1),
(9, 'Jefe Trazabilidad', 1),
(10, 'Auxiliar Trazabilidad', 1),
(11, 'Operador Logístico', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificado`
--

CREATE TABLE `certificado` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id`, `id_pais`, `nombre`, `activo`) VALUES
(1, 1, 'HOLANDA MERI...', 1),
(2, 2, 'HAMBURGO', 1),
(3, 3, 'CANADA', 1),
(4, 1, 'HOLANDA', 1),
(5, 1, 'DOVER', 1),
(6, 3, 'LOUSIANA', 1),
(7, 3, 'CALIFORNIA', 1),
(8, 3, 'RODMAN', 1),
(9, 4, 'PANAMA', 1),
(10, 4, 'Paita', 0),
(11, 3, 'DOVER USA', 1),
(12, 3, 'FLORIDA', 1),
(13, 5, 'BELGICA', 1),
(14, 3, 'NEW YOURK', 1),
(15, 6, 'JAPON', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `razon_social` varchar(50) NOT NULL,
  `slug` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `razon_social`, `slug`, `email`, `telefono`, `activo`) VALUES
(1, 'AGROFAIR', 'agrofair', '', '', 1),
(2, 'BIODYNAMISKA', 'biodynamiska', '', '', 1),
(3, 'DOLE', 'dole', '', '', 1),
(4, 'TRANSASTRA', 'transastra', '', '', 1),
(5, 'EQUIFRUIT', 'equifruit', '', '', 1),
(6, 'Luis', 'luis', 'luis13r@hotmail.com', '000000003', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenedor`
--

CREATE TABLE `contenedor` (
  `id` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `referencia` varchar(20) NOT NULL,
  `booking` varchar(20) NOT NULL,
  `numero_contenedor` varchar(50) DEFAULT NULL,
  `id_lineanaviera` int(11) NOT NULL,
  `nave` varchar(50) NOT NULL,
  `viaje` varchar(20) NOT NULL,
  `id_puertodestino` int(11) NOT NULL,
  `id_operador` int(11) NOT NULL,
  `fecha_proceso_inicio` date NOT NULL,
  `fecha_proceso_fin` date NOT NULL,
  `fecha_zarpe` date DEFAULT NULL,
  `fecha_llegada` date DEFAULT NULL,
  `peso_bruto` double DEFAULT NULL,
  `peso_neto` double DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `valija` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contenedor`
--

INSERT INTO `contenedor` (`id`, `id_semana`, `id_cliente`, `referencia`, `booking`, `numero_contenedor`, `id_lineanaviera`, `nave`, `viaje`, `id_puertodestino`, `id_operador`, `fecha_proceso_inicio`, `fecha_proceso_fin`, `fecha_zarpe`, `fecha_llegada`, `peso_bruto`, `peso_neto`, `activo`, `valija`) VALUES
(26, 22, 1, '22513', '960685565', 'PONU 481624-0', 5, 'NORTHERN DEXTERITY 4', '1708', 1, 2, '2017-05-29', '2017-05-30', '2017-05-31', '2017-06-01', NULL, NULL, 0, 0),
(27, 22, 2, '1223', '1222', NULL, 2, 'rrrrrggg', '133', 1, 7, '2017-05-09', '2017-05-11', '2017-05-22', '2017-05-24', NULL, NULL, 0, 0),
(28, 23, 1, '22595', '960758731', 'PONU 497834-4', 5, 'MSK-SEALAND MANZANILLA', '1708', 1, 2, '2017-05-22', '2017-05-22', '2017-05-28', '2017-06-16', NULL, NULL, 1, 0),
(29, 23, 1, '22596', '960758731', 'MNBU 315895-0', 5, 'MSK-SEALAND MANZANILLA', '1708', 1, 2, '2017-05-22', '2017-05-22', '2017-05-28', '2017-06-16', NULL, NULL, 1, 0),
(30, 23, 1, '22597', '960758731', 'PONU 480767-6', 5, 'MSK-SEALAND MANZANILLA', '1708', 1, 2, '2017-05-22', '2017-05-22', '2017-05-28', '2017-06-16', NULL, NULL, 1, 0),
(31, 23, 1, '22598', '960758731', 'PONU 482052-8', 5, 'SEALAND MANZANILLA', '1708', 1, 2, '2017-05-23', '2017-05-23', '2017-05-28', '2017-06-16', NULL, NULL, 1, 0),
(32, 23, 1, '22599', '960758739', 'MMAU 107222-6', 5, 'SEALAND MANZANILLA', '1708', 7, 2, '2017-05-23', '2017-05-23', '2017-05-28', '2017-06-29', NULL, NULL, 1, 0),
(33, 23, 1, '22588', 'MBM140178149', 'BMOU 965137-1', 8, 'POLAR LIGHT', 'SR 17014EB', 3, 2, '2017-05-24', '2017-05-24', '2017-05-28', '2017-06-15', NULL, NULL, 1, 0),
(34, 23, 1, '22589', 'MBM140178149', 'TTNU 896246-2', 8, 'POLAR LIGHT', 'SR 17014EB', 3, 2, '2017-05-24', '2017-05-24', '2017-05-28', '2017-06-15', NULL, NULL, 1, 0),
(35, 23, 1, '22590', 'MBM 140178149', 'BMOU 961635-0', 8, 'POLAR LIGHT', 'SR 17014EB', 3, 2, '2017-05-24', '2017-05-24', '2017-05-28', '2017-06-15', NULL, NULL, 1, 0),
(36, 23, 1, '22591', '7LIMES2775', 'SUDU 605016-3', 3, 'HSD-MAGARI', '722', 3, 2, '2017-05-24', '2017-05-24', '2017-05-30', '2017-06-20', NULL, NULL, 1, 0),
(37, 23, 1, '22592', '7LIMES2775', 'SUDU 605417-4', 3, 'HSD-MAGARI', '722', 3, 2, '2017-05-25', '2017-05-25', '2017-05-30', '2017-06-20', NULL, NULL, 1, 0),
(38, 23, 1, '22593', '7LIMES2775', 'SUDU 604948-1', 3, 'HSD-MAGARI', '722', 3, 2, '2017-05-25', '2017-05-25', '2017-05-30', '2017-06-20', NULL, NULL, 1, 0),
(39, 23, 1, '22594', '7LIMES2775', 'SUDU 529208-0', 3, 'HSD-MAGARI', '722', 3, 2, '2017-05-25', '2017-05-25', '2017-05-30', '2017-06-20', NULL, NULL, 1, 0),
(41, 23, 2, '1', '12629836', 'HLXU 876009-0', 4, 'HLE- MAGARI', '722', 3, 1, '2017-05-26', '2017-05-26', '2017-05-30', '2017-06-20', NULL, NULL, 1, 0),
(42, 23, 2, '2', '12629836', 'HLBU 900451-0', 4, 'HLE- MAGARI', '722', 3, 1, '2017-05-26', '2017-05-26', '2017-05-30', '2017-06-20', NULL, NULL, 1, 0),
(43, 23, 4, '1', '98517939', 'TCLU 102844-3', 4, 'HLE- MAGARI', '722', 3, 2, '2017-05-27', '2017-05-27', '2017-05-30', '2017-06-20', NULL, NULL, 1, 0),
(44, 23, 4, '2', '98517939', 'CRLU 137750-9', 4, 'HLE- MAGARI', '722', 3, 2, '2017-05-27', '2017-05-27', '2017-05-30', '2017-06-20', NULL, NULL, 1, 0),
(45, 23, 3, '1', 'LMM0215232', 'TLLU 106897-4', 2, 'MAGARI', '016', 6, 1, '2017-05-27', '2017-05-27', '2017-05-29', '2017-06-20', NULL, NULL, 1, 0),
(46, 23, 5, '1', '7LIMAG1275', 'SUDU 823613-6', 3, 'HSD-MAGARI', '722', 2, 2, '2017-05-27', '2017-05-27', '2017-05-30', '2017-06-20', NULL, NULL, 1, 0),
(47, 23, 5, '2', '7LIMAG1275', 'SUDU  523335-0', 3, 'HSD-MAGARI', '722', 2, 2, '2017-05-28', '2017-05-28', '2017-05-30', '2017-06-20', NULL, NULL, 1, 0),
(48, 23, 3, '2', 'B6JBK024778', 'CXRU 161009-2', 9, 'ALIOTH', '171', 6, 1, '2017-05-28', '2017-05-28', '2017-05-31', '2017-06-20', NULL, NULL, 1, 0),
(49, 24, 1, '22694', '960851846', 'MWCU 657259-1', 5, 'SEALAND GUAYAQUIL', '1710', 1, 2, '2017-05-28', '2017-05-28', '2017-06-02', '2017-06-25', NULL, NULL, 1, 0),
(50, 24, 1, '22695', '960851846', 'DAYU 670586-6', 5, 'SEALAND GUAYAQUIL', '1710', 1, 2, '2017-05-29', '2017-05-29', '2017-06-02', '2017-06-25', NULL, NULL, 1, 0),
(51, 24, 1, '22696', '960851846', 'MSWU 102357-9', 5, 'SEALAND GUAYAQUIL', '1710', 1, 2, '2017-05-30', '2017-05-30', '2017-06-02', '2017-06-25', NULL, NULL, 1, 0),
(52, 24, 1, '22698', '960851855', 'MMAU 107723-3', 5, 'SEALAND GUAYAQUIL', '1710', 7, 2, '2017-05-30', '2017-05-30', '2017-06-02', '2017-07-02', NULL, NULL, 1, 0),
(53, 24, 1, '22687', 'MBM140178152', 'TCLU 138551-7', 8, 'NEDERLAND REEFER', 'RA17015EB', 3, 2, '2017-05-30', '2017-05-30', '2017-06-05', '2017-05-22', NULL, NULL, 1, 0),
(54, 24, 1, '22688', 'MBM140178152', 'SZLU 980331-0', 8, 'NEDERLAND REEFER', 'RA17015EB', 3, 2, '2017-06-02', '2017-06-02', '2017-06-05', '2017-06-22', NULL, NULL, 1, 0),
(55, 24, 1, '22689', 'MBM140178152', 'RRSU 100351-4', 8, 'NEDERLAND REEFER', 'RA17015EB', 3, 2, '2017-06-02', '2017-06-02', '2017-06-05', '2017-06-22', NULL, NULL, 1, 0),
(56, 24, 1, '22690', '7LIMES2945', 'CNIU 222198-8', 3, 'MAGARI', '723', 3, 2, '2017-05-30', '2017-05-30', '2017-06-06', '2017-06-27', NULL, NULL, 1, 0),
(57, 24, 1, '22691', '7LIMES2945', 'SUDU 600809-7', 3, 'MAGARI', '723', 3, 2, '2017-06-03', '2017-06-03', '2017-06-06', '2017-06-27', NULL, NULL, 1, 0),
(58, 24, 1, '22693', '7LIMES2945', 'TEMU 906017-7', 3, 'MAGARI', '723', 3, 2, '2017-06-03', '2017-06-03', '2017-06-06', '2017-06-27', NULL, NULL, 1, 0),
(59, 24, 2, '1', '98538041', 'HXLU 877232-1', 4, 'MAGARI', '723', 3, 1, '2017-06-03', '2017-06-03', '2017-06-08', '2017-07-08', NULL, NULL, 1, 0),
(60, 24, 4, '1', '087LIM234655', 'MSCU 7433652', 7, 'EANLAND LOS ANGELES', '1710', 3, 2, '2017-06-05', '2017-06-05', '2017-06-10', '2017-07-10', NULL, NULL, 1, 0),
(61, 24, 3, '1', '1', NULL, 2, 'MAGARI', '1', 6, 1, '2017-06-04', '2017-06-04', NULL, NULL, NULL, NULL, 0, 0),
(62, 24, 3, '1', '1', NULL, 2, 'MAGARI', '1', 6, 1, '2017-06-04', '2017-06-04', NULL, NULL, NULL, NULL, 0, 0),
(63, 24, 3, '1', 'B6JBK024823', 'DFIU 215125-2', 9, 'DOLE', '1', 4, 1, '2017-06-04', '2017-06-04', '2017-06-09', '2017-07-09', NULL, NULL, 1, 0),
(64, 24, 3, '1', '1', NULL, 2, 'MAGARI', '1', 6, 1, '2017-06-04', '2017-06-04', NULL, NULL, NULL, NULL, 0, 0),
(65, 24, 5, '1', '7LIMAG1334', 'SUDU 621885-3', 3, 'MAGARI', '723', 2, 2, '2017-06-04', '2017-06-04', '2017-06-09', '2017-07-09', NULL, NULL, 1, 0),
(66, 25, 1, '22513', '960685565', 'PONU 481624-0', 5, 'SEALAND PHILADELPHIA', '1708', 1, 2, '2017-05-13', '2017-05-13', '2017-05-17', '2017-06-03', NULL, NULL, 1, 0),
(67, 25, 1, '22514', '960685565', NULL, 5, 'SEALAND PHILADELPHIA', '1708', 1, 2, '2017-05-16', '2017-05-16', '2017-05-24', '2017-05-16', NULL, NULL, 1, 0),
(68, 25, 1, '22515', '960685575', NULL, 5, 'SEALAND PHILADELPHIA', '1708', 7, 2, '2017-05-16', '2017-05-16', '2017-05-24', '2017-06-15', NULL, NULL, 1, 0),
(69, 25, 1, '22505', 'MBM140178147', NULL, 8, 'SCHWEIZ REEFER', '17013', 3, 2, '2017-05-17', '2017-05-17', '2017-05-25', '2017-06-17', NULL, NULL, 1, 0),
(70, 25, 1, '22506', 'MBM140178147', NULL, 8, 'SCHWEIZ REEFER', '17013', 3, 2, '2017-05-17', '2017-05-17', '2017-05-25', '2017-05-25', NULL, NULL, 1, 0),
(71, 25, 1, '22507', 'MBM140178147', NULL, 7, 'SCHWEIZ REEFER', '17013', 3, 2, '2017-05-17', '2017-05-17', '2017-05-25', '2017-05-22', NULL, NULL, 1, 0),
(72, 25, 1, '22508', 'MBM140178147', NULL, 8, 'SCHWEIZ REEFER', '17013', 3, 2, '2017-05-18', '2017-05-18', '2017-05-25', '2017-06-14', NULL, NULL, 1, 0),
(73, 25, 1, '22509', '7LIMES2647', NULL, 3, 'MAGARI', '721', 3, 2, '2017-05-18', '2017-05-18', '2017-05-25', '2017-05-25', NULL, NULL, 1, 0),
(74, 25, 1, '22510', '7LIMES2647', NULL, 3, 'MAGARI', '721', 3, 2, '2017-05-18', '2017-05-18', '2017-05-28', '2017-06-21', NULL, NULL, 1, 0),
(75, 25, 1, '22511', '7LIMES2647', NULL, 3, 'MAGARI', '721', 3, 2, '2017-05-19', '2017-05-19', '2017-05-29', '2017-06-24', NULL, NULL, 1, 0),
(76, 25, 1, '22574', '7LIMES2647', NULL, 3, 'MAGARI', '721', 3, 2, '2017-05-19', '2017-05-19', '2017-05-27', '2017-06-22', NULL, NULL, 1, 0),
(77, 25, 1, '22512', '7LIMES2647', NULL, 3, 'MAGARI', '721', 3, 2, '2017-05-19', '2017-05-19', '2017-05-28', '2017-06-29', NULL, NULL, 1, 0),
(78, 25, 2, '1', '98481475', NULL, 4, 'MAGARI', '721', 3, 1, '2017-05-19', '2017-05-19', '2017-05-29', '2017-06-29', NULL, NULL, 1, 0),
(79, 25, 2, '2', '98481475', NULL, 4, 'MAGARI', '721', 3, 1, '2017-05-20', '2017-05-20', '2017-05-30', '2017-06-30', NULL, NULL, 1, 0),
(80, 25, 4, '1', '98488003', NULL, 4, 'MAGARI', '721', 3, 2, '2017-05-20', '2017-05-20', '2017-05-30', '2017-06-30', NULL, NULL, 1, 0),
(82, 25, 4, '2', '98488003', NULL, 4, 'MAGARI', '721', 3, 2, '2017-05-20', '2017-05-20', '2017-05-30', '2017-06-30', NULL, NULL, 1, 0),
(83, 25, 5, '1', '7LIMAG1194', NULL, 3, 'MAGARI', '721', 2, 2, '2017-05-21', '2017-05-21', '2017-06-03', '2017-06-03', NULL, NULL, 1, 0),
(84, 25, 3, '1', 'B6JBK024736', NULL, 9, 'JAMILA', '051', 4, 1, '2017-05-21', '2017-05-21', '2017-06-02', '2017-07-02', NULL, NULL, 1, 0),
(85, 25, 2, '2', 'B6JBK024735', NULL, 9, 'JAMILA', '051', 4, 1, '2017-05-21', '2017-05-21', '2017-06-01', '2017-07-01', NULL, NULL, 1, 0),
(86, 26, 1, '22383', '960593965', NULL, 5, 'MARINER', '1708', 1, 2, '2017-05-07', '2017-05-07', '2017-05-13', '2017-06-13', NULL, NULL, 1, 0),
(87, 26, 1, '22384', '960593965', NULL, 5, 'MARINER', '1708', 1, 2, '2017-05-07', '2017-05-07', '2017-05-13', '2017-06-13', NULL, NULL, 1, 0),
(88, 26, 1, '22385', '960593965', NULL, 5, 'MARINER', '1708', 1, 2, '2017-05-08', '2017-05-08', '2017-05-14', '2017-06-14', NULL, NULL, 1, 0),
(89, 26, 1, '22386', '960593965', NULL, 5, 'MARINER', '1708', 1, 2, '2017-05-08', '2017-05-08', '2017-05-15', '2017-06-15', NULL, NULL, 1, 0),
(90, 26, 1, '22387', '960593971', NULL, 5, 'MARINER', '1708', 7, 2, '2017-05-09', '2017-05-09', '2017-05-16', '2017-06-16', NULL, NULL, 1, 0),
(91, 26, 1, '22308', 'MBM140178145', NULL, 8, 'LOMBOK STRAIT', 'SR17012EB', 3, 2, '2017-05-09', '2017-05-09', '2017-05-16', '2017-06-16', NULL, NULL, 0, 0),
(92, 26, 1, '22378', 'MBM140178145', NULL, 8, 'LOMBOK STRAIT', 'SR17012EB', 3, 2, '2017-05-09', '2017-05-09', '2017-05-15', '2017-06-15', NULL, NULL, 1, 0),
(93, 26, 1, '22378', 'MBM140178145', NULL, 8, 'LOMBOK STRAIT', 'SR17012EB', 3, 2, '2017-05-09', '2017-05-09', '2017-05-15', '2017-06-15', NULL, NULL, 1, 0),
(94, 26, 1, '22379', 'MBM140178145', NULL, 8, 'LOMBOK STRAIT', 'SR17012EB', 3, 2, '2017-05-09', '2017-05-09', '2017-05-14', '2017-06-14', NULL, NULL, 1, 0),
(95, 26, 1, '22380', '7LIMES2508', NULL, 3, 'MAGARI', '720', 3, 2, '2017-05-10', '2017-05-10', NULL, NULL, NULL, NULL, 1, 0),
(96, 26, 1, '22382', '7LIMES2508', NULL, 3, 'MAGARI', '720', 3, 2, '2017-05-11', '2017-05-11', '2017-05-18', '2017-06-18', NULL, NULL, 1, 0),
(97, 26, 1, '22381', '7LIMES2508', NULL, 3, 'MAGARI', '720', 3, 2, '2017-05-10', '2017-05-10', '2017-05-17', '2017-06-17', NULL, NULL, 1, 0),
(98, 26, 2, '1', '98450104', NULL, 4, 'MAGARI', '720', 3, 1, '2017-05-11', '2017-05-11', '2017-05-19', '2017-06-19', NULL, NULL, 1, 0),
(99, 26, 2, '2', '98450104', NULL, 4, 'MAGARI', '720', 3, 1, '2017-05-11', '2017-05-11', '2017-05-18', '2017-06-18', NULL, NULL, 1, 0),
(100, 26, 4, '1', '98463454', NULL, 4, 'MAGARI', '720', 3, 2, '2017-05-11', '2017-05-11', '2017-05-19', '2017-06-19', NULL, NULL, 1, 0),
(101, 26, 4, '2', '98463454', NULL, 4, 'MAGARI', '720', 3, 9, '2017-05-12', '2017-05-12', '2017-05-20', '2017-06-20', NULL, NULL, 1, 0),
(102, 26, 3, '1', 'B6JBK024710', NULL, 9, 'ALIOTH', '170', 4, 1, '2017-05-12', '2017-05-12', '2017-05-20', '2017-06-20', NULL, NULL, 1, 0),
(103, 26, 1, '2', 'B6JBK024709', NULL, 9, 'ALIOTH', '710', 4, 1, '2017-05-12', '2017-05-12', '2017-05-20', '2017-05-20', NULL, NULL, 1, 0),
(104, 26, 5, '1', '7LIMAG1117', NULL, 2, 'MAGARI', '720', 6, 1, '2017-05-13', '2017-05-13', '2017-05-21', '2017-06-21', NULL, NULL, 1, 0),
(105, 26, 5, '2', '7LIMAG1117', NULL, 3, 'MAGARI', '720', 2, 2, '2017-05-13', '2017-05-13', '2017-05-23', '2017-06-23', NULL, NULL, 1, 0),
(106, 26, 5, '3', '7LIMAG1117', NULL, 3, 'MAGARI', '721', 2, 2, '2017-05-13', '2017-05-13', '2017-05-14', '2017-05-14', NULL, NULL, 1, 0),
(107, 27, 1, '22312', '960508266', NULL, 5, 'SEALAND BALBOA', '1708', 1, 2, '2017-04-29', '2017-04-29', '2017-05-06', '2017-06-06', NULL, NULL, 1, 0),
(108, 27, 1, '22313', '960508266', NULL, 5, 'SEALAND BALBOA', '1708', 1, 2, '2017-04-29', '2017-05-29', '2017-05-06', '2017-05-24', NULL, NULL, 1, 0),
(109, 27, 1, '22314', '960508266', NULL, 5, 'SEALAND BALBOA', '1708', 1, 2, '2017-04-29', '2017-04-29', '2017-05-06', '2017-05-24', NULL, NULL, 1, 0),
(110, 27, 1, '22316', '960508294', NULL, 5, 'SEALAND BALBOA', '1708', 7, 2, '2017-04-30', '2017-04-30', '2017-05-07', '2017-05-23', NULL, NULL, 1, 0),
(111, 27, 1, '22315', '960508266', NULL, 5, 'SEALAND BALBOA', '1708', 1, 2, '2017-04-30', '2017-04-30', '2017-05-07', '2017-05-23', NULL, NULL, 1, 0),
(112, 27, 1, '22315', '960508266', NULL, 5, 'SEALAND BALBOA', '1708', 1, 2, '2017-04-30', '2017-04-30', '2017-05-07', '2017-05-23', NULL, NULL, 0, 0),
(113, 27, 1, '22307', 'MBM140178142', NULL, 8, 'ATLANTIC KLIPPER', 'SR17011EB', 3, 2, '2017-04-30', '2017-04-30', '2017-05-07', '2017-05-23', NULL, NULL, 1, 0),
(114, 27, 1, '22309', '7LIMES2374', NULL, 3, 'MAGARI', '719', 3, 2, '2017-05-03', '2017-05-03', '2017-05-08', '2017-05-24', NULL, NULL, 1, 0),
(115, 27, 1, '22310', '7LIMES2374', NULL, 3, 'MAGARI', '719', 3, 2, '2017-05-03', '2017-05-03', '2017-05-08', '2017-05-24', NULL, NULL, 1, 0),
(116, 27, 1, '22355', 'MBM140178142', NULL, 8, 'ATLANTIC KLIPPER', 'SR17011EB', 3, 2, '2017-05-04', '2017-05-04', '2017-05-09', '2017-05-25', NULL, NULL, 1, 0),
(117, 27, 1, '22311', '7LIMES2374', NULL, 3, 'MAGARI', '719', 3, 2, '2017-05-04', '2017-05-04', '2017-05-09', '2017-05-29', NULL, NULL, 1, 0),
(118, 27, 1, '22317', '7LIMES2374', NULL, 3, 'MAGARI', '719', 3, 2, '2017-05-04', '2017-05-04', '2017-05-09', '2017-05-29', NULL, NULL, 1, 0),
(119, 27, 2, '1', '98443785', NULL, 4, 'MAGARI', '719', 3, 1, '2017-05-04', '2017-05-04', '2017-05-09', '2017-05-29', NULL, NULL, 1, 0),
(120, 27, 2, '2', '98450709', NULL, 4, 'MAGARI', '719', 3, 1, '2017-05-05', '2017-05-05', '2017-05-10', '2017-05-30', NULL, NULL, 1, 0),
(121, 27, 4, '1', '98436819', NULL, 4, 'MAGARI', '719', 3, 2, '2017-05-05', '2017-05-05', '2017-05-10', '2017-05-30', NULL, NULL, 1, 0),
(122, 27, 4, '2', '98436819', NULL, 4, 'MAGARI', '719', 3, 2, '2017-05-06', '2017-05-06', '2017-05-11', '2017-05-31', NULL, NULL, 1, 0),
(123, 27, 5, '1', '7LIMAG1025', NULL, 3, 'MAGARI', '719', 2, 2, '2017-05-06', '2017-05-06', '2017-05-11', '2017-05-31', NULL, NULL, 1, 0),
(124, 27, 3, '1', 'LMM0214243', NULL, 2, 'MAGARI', '013', 6, 1, '2017-05-06', '2017-05-06', '2017-05-11', '2017-05-31', NULL, NULL, 1, 0),
(125, 27, 3, '2', 'LMM0214243', NULL, 3, 'MAGARI', '013', 6, 1, '2017-05-06', '2017-05-06', '2017-05-11', '2017-05-31', NULL, NULL, 1, 0),
(126, 27, 5, '2', '7LIMAG1026', NULL, 3, 'MAGARI', '719', 2, 2, '2017-05-07', '2017-05-07', '2017-05-12', '2017-06-01', NULL, NULL, 1, 0),
(127, 28, 1, '22182', '960375921', NULL, 5, 'SEALAND LOS ANGELES', '1708', 1, 2, '2017-04-23', '2017-04-23', '2017-04-28', '2017-05-08', NULL, NULL, 1, 0),
(128, 28, 1, '22185', '960375921', NULL, 5, 'SEALAND LOS ANGELES', '1708', 1, 2, '2017-04-23', '2017-04-23', '2017-04-28', '2017-05-08', NULL, NULL, 1, 0),
(129, 28, 1, '22186', '960375921', NULL, 5, 'SEALAND LOS ANGELES', '1708', 1, 2, '2017-04-23', '2017-04-23', '2017-04-28', '2017-05-13', NULL, NULL, 1, 0),
(130, 28, 1, '22187', '960375916', NULL, 5, 'SEALAND LOS ANGELES', '1708', 7, 2, '2017-04-24', '2017-04-24', '2017-04-30', '2017-05-10', NULL, NULL, 1, 0),
(131, 28, 1, '22175', 'MBM140178139', NULL, 8, 'BALTIC KLIPPER', '17010', 3, 2, '2017-04-24', '2017-04-24', '2017-04-29', '2017-05-09', NULL, NULL, 1, 0),
(132, 28, 1, '22176', 'MBM140178139', NULL, 8, 'BALTIC KLIPPER', 'SR17010EB', 3, 2, '2017-04-24', '2017-04-24', '2017-04-29', '2017-05-12', NULL, NULL, 1, 0),
(133, 28, 1, '22177', 'MBM140178139', NULL, 8, 'BALTIC KLIPPER', 'SR17010EB', 3, 2, '2017-04-24', '2017-04-24', '2017-04-29', '2017-05-18', NULL, NULL, 1, 0),
(134, 28, 1, '22178', '7LIMES2192', NULL, 3, 'MAGARI', '718', 3, 2, '2017-04-25', '2017-05-25', '2017-05-30', '2017-05-17', NULL, NULL, 1, 0),
(135, 28, 1, '22179', '7LIMES2192', NULL, 3, 'MAGARI', '718', 3, 2, '2017-04-25', '2017-05-25', '2017-04-30', '2017-05-17', NULL, NULL, 1, 0),
(136, 28, 1, '22180', '7LIMES2192', NULL, 3, 'MAGARI', '718', 3, 2, '2017-04-26', '2017-04-26', '2017-05-01', '2017-05-19', NULL, NULL, 1, 0),
(137, 28, 1, '22181', '7LIMES2192', NULL, 3, 'MAGARI', '718', 3, 2, '2017-04-26', '2017-04-26', '2017-05-01', '2017-05-19', NULL, NULL, 1, 0),
(138, 28, 2, '1', '983909905', NULL, 4, 'MAGARI', '718', 3, 1, '2017-04-26', '2017-04-26', '2017-05-01', '2017-05-20', NULL, NULL, 0, 0),
(139, 28, 2, '1', '983909905', NULL, 4, 'MAGARI', '718', 3, 1, '2017-04-26', '2017-04-26', '2017-05-01', '2017-05-20', NULL, NULL, 1, 0),
(140, 28, 2, '2', '9839095', NULL, 4, 'MAGARI', '718', 3, 1, '2017-04-26', '2017-04-26', '2017-05-01', '2017-05-20', NULL, NULL, 1, 0),
(141, 28, 4, '1', '98390933', NULL, 4, 'MAGARI', '718', 3, 2, '2017-04-27', '2017-04-27', '2017-05-02', '2017-05-22', NULL, NULL, 1, 0),
(142, 28, 4, '2', '98390933', NULL, 4, 'MAGARI', '718', 3, 2, '2017-04-27', '2017-04-27', '2017-05-02', '2017-05-22', NULL, NULL, 1, 0),
(143, 28, 3, '1', 'LMM0213991', NULL, 2, 'MAGARI', '012', 6, 1, '2017-04-27', '2017-04-27', '2017-05-02', '2017-05-22', NULL, NULL, 1, 0),
(144, 28, 5, '1', '7LIMAG0937', NULL, 3, 'MAGARI', '718', 2, 2, '2017-04-27', '2017-04-27', '2017-05-02', '2017-05-23', NULL, NULL, 1, 0),
(145, 28, 3, '2', 'LMM0213991', NULL, 2, 'MAGARI', '012', 6, 1, '2017-04-28', '2017-04-28', '2017-05-03', '2017-05-23', NULL, NULL, 1, 0),
(146, 28, 5, '2', '7LIMAG0967', NULL, 3, 'MAGARI', '718', 2, 2, '2017-04-28', '2017-04-28', '2017-05-03', '2017-05-23', NULL, NULL, 1, 0),
(147, 28, 5, '3', '7LIMAG0938', NULL, 3, 'MAGARI', '718', 2, 2, '2017-04-28', '2017-04-28', '2017-05-03', '2017-05-23', NULL, NULL, 1, 0),
(148, 29, 1, '22079', '960279460', NULL, 5, 'SEALAND GUAYAQUIL', '1708', 1, 2, '2017-04-17', '2017-04-17', '2017-05-21', '2017-06-04', NULL, NULL, 1, 0),
(149, 29, 1, '22080', '960279460', NULL, 5, 'SEALAND GUAYAQUIL', '1708', 1, 2, '2017-04-18', '2017-04-18', '2017-04-22', '2017-05-03', NULL, NULL, 1, 0),
(150, 29, 1, '22081', '960279493', NULL, 5, 'SEALAND GUAYAQUIL', '1708', 7, 2, '2017-04-18', '2017-04-18', '2017-04-23', '2017-05-06', NULL, NULL, 1, 0),
(151, 29, 1, '22089', '960322352', NULL, 5, 'SEALAND GUAYAQUIL', '1708', 3, 2, '2017-04-18', '2017-04-18', '2017-05-23', '2017-05-10', NULL, NULL, 1, 0),
(152, 29, 1, '22082', 'MBM140093082', NULL, 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-04-18', '2017-04-18', '2017-04-23', '2017-05-15', NULL, NULL, 1, 0),
(153, 29, 1, '22083', 'MBM140178138', NULL, 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-04-19', '2017-04-19', '2017-04-24', '2017-05-10', NULL, NULL, 1, 0),
(154, 29, 1, '22084', 'MBM140178138', NULL, 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-04-19', '2017-04-19', '2017-04-24', '2017-05-17', NULL, NULL, 1, 0),
(155, 29, 1, '22085', '7LIMES2056', NULL, 3, 'MAGARI', '717', 3, 2, '2017-04-19', '2017-04-19', '2017-05-24', '2017-05-19', NULL, NULL, 1, 0),
(156, 29, 1, '22086', '7LIMES2056', NULL, 3, 'MAGARI', '717', 3, 2, '2017-04-19', '2017-04-19', '2017-04-24', '2017-05-23', NULL, NULL, 1, 0),
(157, 29, 1, '22088', '7LIMES2056', NULL, 3, 'MAGARI', '717', 3, 2, '2017-04-20', '2017-04-20', '2017-05-25', '2017-05-17', NULL, NULL, 1, 0),
(158, 29, 1, '22087', '7LIMES2056', NULL, 3, 'MAGARI', '717', 3, 2, '2017-04-20', '2017-04-20', '2017-05-25', '2017-05-17', NULL, NULL, 1, 0),
(159, 29, 2, '1', '98376171', NULL, 4, 'MAGARI', '717', 3, 1, '2017-04-20', '2017-04-20', '2017-04-25', '2017-05-15', NULL, NULL, 1, 0),
(160, 29, 2, '2', '98376171', NULL, 4, 'MAGARI', '717', 3, 1, '2017-04-20', '2017-04-20', '2017-05-25', '2017-05-25', NULL, NULL, 1, 0),
(161, 29, 2, '3', '98376171', NULL, 5, 'MAGARI', '717', 3, 1, '2017-04-21', '2017-04-21', '2017-04-26', '2017-05-18', NULL, NULL, 1, 0),
(162, 29, 4, '1', '98376965', NULL, 4, 'MAGARI', '717', 3, 1, '2017-04-21', '2017-04-21', '2017-04-26', '2017-05-18', NULL, NULL, 1, 0),
(163, 29, 4, '2', '98376965', NULL, 4, 'MAGARI', '717', 3, 1, '2017-04-21', '2017-04-21', '2017-04-26', '2017-05-20', NULL, NULL, 1, 0),
(164, 29, 5, '1', '7LIMAG0875', NULL, 3, 'MAGARI', '717', 2, 2, '2017-04-21', '2017-04-21', '2017-04-27', '2017-05-19', NULL, NULL, 1, 0),
(165, 29, 5, '2', '7LIMAG0875', NULL, 3, 'MAGARI', '717', 2, 2, '2017-04-22', '2017-04-22', '2017-05-27', '2017-05-23', NULL, NULL, 1, 0),
(166, 29, 3, '1', 'LMM0213636', NULL, 2, 'MAGARI', '011', 24, 2, '2017-04-22', '2017-04-22', '2017-04-27', '2017-05-17', NULL, NULL, 1, 0),
(167, 29, 3, '2', 'LMM0213636', NULL, 2, 'MAGARI', '011', 24, 2, '2017-04-22', '2017-04-22', '2017-04-27', '2017-05-18', NULL, NULL, 1, 0),
(168, 30, 1, '21943', '960206947', NULL, 5, 'SEALAND MANZANILLO', '1706', 1, 2, '2017-04-10', '2017-04-10', '2017-04-15', '2017-06-30', NULL, NULL, 1, 0),
(169, 30, 1, '21944', '960206947', NULL, 5, 'SEALAND MANZANILLO', '1706', 1, 2, '2017-04-10', '2017-04-10', '2017-04-15', '2017-04-30', NULL, NULL, 1, 0),
(170, 30, 1, '21945', '960206956', NULL, 5, 'SEALAND MANZANILLO', '1706', 7, 2, '2017-04-10', '2017-04-10', '2017-04-15', '2017-04-30', NULL, NULL, 1, 0),
(171, 30, 1, '21935', '087LIM229775', NULL, 7, 'SEALAND MANZANILLO', '1706', 3, 2, '2017-04-11', '2017-04-11', '2017-04-16', '2017-05-01', NULL, NULL, 1, 0),
(172, 30, 1, '21936', '087LIM229775', NULL, 7, 'SEALAND MANZANILLO', '1706', 3, 2, '2017-04-11', '2017-04-11', '2017-04-16', '2017-05-01', NULL, NULL, 1, 0),
(173, 30, 1, '21937', '7LIMES1944', NULL, 3, 'MAGARI', '716', 3, 2, '2017-04-11', '2017-04-11', '2017-04-16', '2017-05-01', NULL, NULL, 1, 0),
(174, 30, 1, '21938', '7LIMES1944', NULL, 3, 'MAGARI', '716', 3, 2, '2017-04-11', '2017-04-11', '2017-04-16', '2017-05-01', NULL, NULL, 0, 0),
(175, 30, 1, '21938', '7LIMES1944', NULL, 3, 'MAGARI', '716', 3, 2, '2017-04-11', '2017-04-11', '2017-04-16', '2017-05-01', NULL, NULL, 1, 0),
(176, 30, 1, '21939', 'MBM140093078', NULL, 8, 'NORDSERENA', '17006', 3, 2, '2017-04-12', '2017-04-12', '2017-04-17', '2017-05-02', NULL, NULL, 1, 0),
(177, 30, 1, '21940', 'MBM140093078', NULL, 8, 'NORDSERENA', '17006', 3, 2, '2017-04-12', '2017-04-12', '2017-04-17', '2017-05-03', NULL, NULL, 1, 0),
(178, 30, 1, '21941', '7LIMES1944', NULL, 3, 'MAGARI', '716', 3, 2, '2017-04-12', '2017-04-12', '2017-04-17', '2017-05-03', NULL, NULL, 1, 0),
(179, 30, 1, '21942', '7LIMES1944', NULL, 3, 'MAGARI', '716', 3, 2, '2017-04-15', '2017-04-15', '2017-04-20', '2017-05-10', NULL, NULL, 1, 0),
(180, 30, 2, '1', 'LMM0213386', NULL, 2, 'MAGARI', '716', 6, 1, '2017-04-15', '2017-04-15', '2017-04-20', '2017-05-10', NULL, NULL, 0, 0),
(181, 30, 2, '1', '98334557', NULL, 4, 'MAGARI', '716', 3, 1, '2017-04-15', '2017-04-15', '2017-04-20', '2017-05-10', NULL, NULL, 1, 0),
(182, 30, 2, '1', '98334557', NULL, 4, 'MAGARI', '716', 3, 1, '2017-04-15', '2017-04-15', '2017-04-20', '2017-05-10', NULL, NULL, 1, 0),
(183, 30, 3, '1', 'LMM0213386', NULL, 2, 'MAGARI', '716', 6, 1, '2017-04-15', '2017-04-15', '2017-04-20', '2017-05-10', NULL, NULL, 1, 0),
(184, 30, 3, '2', 'B6JBK024528', NULL, 2, 'ALIOTH', '168', 6, 1, '2017-04-16', '2017-04-16', '2017-04-20', '2017-05-10', NULL, NULL, 1, 0),
(185, 30, 4, '1', '98340958', NULL, 4, 'MAGARI', '716', 3, 2, '2017-04-16', '2017-04-16', '2017-04-21', '2017-05-12', NULL, NULL, 1, 0),
(186, 30, 4, '2', '98340958', NULL, 4, 'MAGARI', '716', 3, 2, '2017-04-16', '2017-04-16', '2017-04-21', '2017-05-13', NULL, NULL, 1, 0),
(187, 30, 5, '1', '7LIMAG0870', NULL, 3, 'MAGARI', '716', 2, 2, '2017-04-16', '2017-04-16', '2017-04-21', '2017-05-13', NULL, NULL, 1, 0),
(188, 30, 3, '3', 'B6JBK024543', NULL, 9, 'ALIOTH', '168', 5, 1, '2017-04-17', '2017-04-17', '2017-04-22', '2017-05-14', NULL, NULL, 1, 0),
(189, 30, 3, '3', 'B6JBK024543', NULL, 9, 'ALIOTH', '168', 5, 1, '2017-04-17', '2017-04-17', '2017-04-22', '2017-05-14', NULL, NULL, 0, 0),
(190, 31, 1, '21823', '960124055', NULL, 5, 'SEALAND PHILADELPHIA', '1706', 1, 2, '2017-04-04', '2017-04-04', '2017-06-09', '2017-06-01', NULL, NULL, 1, 0),
(191, 31, 1, '21824', '960124055', NULL, 5, 'SEALAND PHILADELPHIA', '1706', 1, 2, '2017-04-04', '2017-04-04', '2017-06-09', '2017-05-01', NULL, NULL, 1, 0),
(192, 31, 1, '21740', '960031000', NULL, 5, 'SEALAND PHILADELPHIA', '1706', 7, 2, '2017-04-04', '2017-04-04', '2017-04-09', '2017-05-01', NULL, NULL, 1, 0),
(193, 31, 1, '21815', 'MBM140160107', NULL, 8, 'LUZON STRAIT', '170017', 3, 2, '2017-04-05', '2017-04-05', '2017-04-09', '2017-05-01', NULL, NULL, 1, 0),
(194, 31, 1, '21816', 'MBM140160107', NULL, 8, 'LUZON STRAIT', '170017', 3, 2, '2017-04-05', '2017-04-05', '2017-04-10', '2017-05-01', NULL, NULL, 1, 0),
(195, 31, 1, '21817', '7LIMES1771', NULL, 3, 'MAGARI', '715', 3, 2, '2017-04-05', '2017-04-05', '2017-04-10', '2017-05-01', NULL, NULL, 1, 0),
(196, 31, 1, '21818', '7LIMES1771', NULL, 3, 'MAGARI', '715', 3, 2, '2017-04-06', '2017-04-06', '2017-04-10', '2017-05-01', NULL, NULL, 1, 0),
(197, 31, 1, '21819', '7LIMES1771', NULL, 3, 'MAGARI', '715', 3, 2, '2017-04-06', '2017-04-06', '2017-04-11', '2017-05-01', NULL, NULL, 1, 0),
(198, 31, 1, '21820', '7LIMES1771', NULL, 3, 'MAGARI', '715', 3, 2, '2017-04-06', '2017-04-06', '2017-04-11', '2017-05-01', NULL, NULL, 1, 0),
(199, 31, 1, '21821', '7LIMES1771', NULL, 3, 'MAGARI', '715', 3, 2, '2017-04-07', '2017-04-07', '2017-04-12', '2017-05-02', NULL, NULL, 1, 0),
(200, 31, 1, '21822', '7LIMES1771', NULL, 3, 'MAGARI', '715', 3, 2, '2017-04-07', '2017-04-07', '2017-04-12', '2017-05-02', NULL, NULL, 1, 0),
(201, 31, 2, '1', '98303394', NULL, 4, 'MAGARI', '715', 3, 2, '2017-04-07', '2017-04-07', '2017-04-12', '2017-05-02', NULL, NULL, 1, 0),
(202, 31, 2, '2', '98303394', NULL, 4, 'MAGARI', '715', 3, 2, '2017-04-07', '2017-04-07', '2017-04-12', '2017-05-02', NULL, NULL, 1, 0),
(203, 31, 4, '1', '98310300', NULL, 4, 'MAGARI', '715', 3, 2, '2017-04-09', '2017-04-09', '2017-06-14', '2017-06-03', NULL, NULL, 1, 0),
(204, 31, 4, '2', '98310300', NULL, 4, 'MAGARI', '715', 3, 2, '2017-04-09', '2017-04-09', '2017-04-14', '2017-05-03', NULL, NULL, 1, 0),
(205, 31, 3, '1', 'LMM0213101', NULL, 2, 'MAGARI', '715', 6, 1, '2017-04-09', '2017-04-09', '2017-06-14', '2017-05-03', NULL, NULL, 1, 0),
(206, 31, 3, '2', 'B6JBK024502', NULL, 2, 'JAMILA', '048', 6, 1, '2017-04-09', '2017-04-09', '2017-06-14', '2017-05-03', NULL, NULL, 1, 0),
(207, 31, 5, '1', '27803830725A', NULL, 6, 'JAMILA', '00048', 3, 2, '2017-04-09', '2017-04-09', '2017-04-14', '2017-05-04', NULL, NULL, 1, 0),
(208, 32, 1, '21735', '960030669', NULL, 5, 'MARINER', '1706', 1, 2, '2017-03-28', '2017-03-28', '2017-05-03', '2017-05-30', NULL, NULL, 1, 0),
(209, 32, 1, '21257', 'MBM140178133', NULL, 8, 'LOMBOK STRAIT', '17006', 3, 2, '2017-03-28', '2017-03-28', '2017-05-03', '2017-05-30', NULL, NULL, 1, 0),
(210, 32, 1, '21258', 'MBM140178133', NULL, 8, 'LOMBOK STRAIT', '17006', 3, 2, '2017-03-29', '2017-03-29', '2017-04-04', '2017-04-30', NULL, NULL, 1, 0),
(211, 32, 1, '21736', '960030669', NULL, 8, 'LOMBOK STRAIT', '17006', 1, 2, '2017-03-30', '2017-03-30', '2017-04-05', '2017-06-02', NULL, NULL, 1, 0),
(212, 32, 1, '21738', '960030669', NULL, 4, 'MAGARI', '714', 1, 2, '2017-04-01', '2017-04-01', '2017-04-05', '2017-06-02', NULL, NULL, 1, 0),
(213, 32, 1, '21737', '960030669', NULL, 8, 'LOMBOK STRAIT', '17006', 3, 2, '2017-03-31', '2017-03-31', '2017-04-05', '2017-05-01', NULL, NULL, 1, 0),
(214, 32, 2, '1', '98265992', NULL, 4, 'MAGARI', '714', 3, 1, '2017-04-01', '2017-04-01', '2017-04-06', '2017-05-06', NULL, NULL, 1, 0),
(215, 32, 2, '21730', '98265992', NULL, 3, 'MAGARI', '714', 3, 1, '2017-04-01', '2017-04-01', '2017-04-06', '2017-05-06', NULL, NULL, 1, 0),
(216, 32, 1, '21731', '7LIMES1669', NULL, 3, 'MAGARI', '714', 3, 2, '2017-03-29', '2017-03-29', '2017-04-04', '2017-04-30', NULL, NULL, 1, 0),
(217, 32, 1, '21732', '7LIMES1669', NULL, 3, 'MAGARI', '714', 3, 2, '2017-03-29', '2017-03-29', '2017-04-05', '2017-05-05', NULL, NULL, 1, 0),
(218, 32, 1, '21733', '7LIMES1669', NULL, 3, 'MAGARI', '714', 3, 2, '2017-03-01', '2017-03-01', '2017-04-05', '2017-04-27', NULL, NULL, 1, 0),
(219, 32, 4, '1', '98290989', NULL, 4, 'MAGARI', '714', 3, 2, '2017-04-01', '2017-04-01', '2017-04-07', '2017-05-21', NULL, NULL, 1, 0),
(220, 32, 4, '2', '98290989', NULL, 4, 'MAGARI', '714', 3, 2, '2017-04-01', '2017-04-01', '2017-04-06', '2017-05-10', NULL, NULL, 1, 0),
(221, 32, 5, '1', '27803823109A', NULL, 6, 'ALIOTH', '167', 2, 2, '2017-04-03', '2017-04-03', '2017-04-08', '2017-05-08', NULL, NULL, 1, 0),
(222, 32, 5, '2', '98303756', NULL, 6, 'ALIOTH', '167', 2, 2, '2017-04-03', '2017-04-03', '2017-04-08', '2017-05-07', NULL, NULL, 1, 0),
(223, 32, 3, '1', 'B6JBK024445', NULL, 2, 'ALIOTH', '167', 6, 1, '2017-04-02', '2017-04-02', '2017-04-06', '2017-05-03', NULL, NULL, 1, 0),
(224, 32, 3, '2', 'B6JBK024445', NULL, 2, 'ALIOTH', '167', 6, 2, '2017-04-02', '2017-04-02', '2017-04-07', '2017-05-07', NULL, NULL, 1, 0),
(225, 32, 3, '3', 'B6JBK024459', NULL, 9, 'ALIOTH', '167', 22, 1, '2017-04-03', '2017-04-03', '2017-04-07', '2017-05-07', NULL, NULL, 1, 0),
(226, 33, 1, '21606', '959974970', NULL, 5, 'SEALAND BALBOA', '1706', 1, 2, '2017-03-20', '2017-03-20', '2017-03-26', '2017-04-28', NULL, NULL, 1, 0),
(228, 33, 1, '21475', '959974970', NULL, 5, 'SEALAND BALBOA', '1706', 1, 2, '2017-03-20', '2017-03-20', '2017-03-25', '2017-04-11', NULL, NULL, 1, 0),
(229, 33, 1, '21676', '959976568', NULL, 5, 'SEALAND BALBOA', '1706', 7, 2, '2017-03-21', '2017-03-21', '2017-03-26', '2017-04-26', NULL, NULL, 1, 0),
(230, 33, 1, '21474', '959974970', NULL, 5, 'SEALAND BALBOA', '1706', 1, 2, '2017-03-20', '2017-03-20', '2017-03-25', '2017-04-25', NULL, NULL, 1, 0),
(231, 33, 1, '21598', 'MBM160065012', NULL, 8, 'ATLANTIC KLIPPER', '17005', 1, 2, '2017-03-21', '2017-03-21', '2017-03-26', '2017-04-26', NULL, NULL, 1, 0),
(232, 33, 1, '21257', 'MBM140178130', NULL, 8, 'ATLANTIC KLIPPER', '17005', 3, 2, '2017-03-21', '2017-03-21', '2017-03-26', '2017-04-26', NULL, NULL, 1, 0),
(233, 33, 1, '21258', 'MBM140178130', NULL, 8, 'ATLANTIC KLIPPER', '17005', 3, 2, '2017-03-23', '2017-03-23', '2017-03-28', '2017-04-26', NULL, NULL, 1, 0),
(234, 33, 1, '21601', '7LIMES1578', NULL, 3, 'MAGARI', '713', 3, 2, '2017-03-23', '2017-03-23', '2017-03-30', '2017-04-30', NULL, NULL, 1, 0),
(235, 33, 1, '21603', '7LIMES1578', NULL, 3, 'MAGARI', '713', 3, 2, '2017-03-23', '2017-03-23', '2017-03-29', '2017-04-30', NULL, NULL, 1, 0),
(236, 33, 1, '21604', '7LIMES1578', NULL, 3, 'MAGARI', '713', 3, 2, '2017-03-24', '2017-03-24', '2017-03-29', '2017-04-29', NULL, NULL, 1, 0),
(237, 33, 1, '21605', '7LIMES1578', NULL, 3, 'MAGARI', '713', 3, 2, '2017-03-24', '2017-03-24', '2017-03-30', '2017-04-24', NULL, NULL, 1, 0),
(238, 33, 2, '1', '98231039', NULL, 4, 'MAGARI', '713', 3, 1, '2017-03-24', '2017-03-24', '2017-03-30', '2017-04-30', NULL, NULL, 1, 0),
(239, 33, 2, '2', '98231039', NULL, 4, 'MAGARI', '713', 3, 1, '2017-03-25', '2017-03-25', '2017-03-31', '2017-04-30', NULL, NULL, 1, 0),
(240, 33, 4, '1', '98252089', NULL, 4, 'MAGARI', '713', 3, 2, '2017-03-25', '2017-03-25', '2017-03-30', '2017-04-30', NULL, NULL, 1, 0),
(241, 33, 4, '2', '087LIM228826', NULL, 7, 'MAGARI', '713', 3, 2, '2017-03-28', '2017-03-28', '2017-04-01', '2017-04-30', NULL, NULL, 1, 0),
(242, 33, 3, '1', 'B6JBK024411', NULL, 9, 'JAMILA', '047', 6, 1, '2017-03-26', '2017-03-26', '2017-04-01', '2017-04-30', NULL, NULL, 1, 0),
(243, 33, 3, '2', 'B6JBK024394', NULL, 9, 'JAMILA', '047', 6, 1, '2017-03-27', '2017-03-27', '2017-04-01', '2017-04-30', NULL, NULL, 1, 0),
(244, 33, 5, '2', '27803820368A', NULL, 6, 'JAMILA', '00047', 2, 2, '2017-03-27', '2017-03-27', '2017-04-01', '2017-05-27', NULL, NULL, 1, 0),
(245, 33, 5, '1', '27803820352A', NULL, 6, 'JAMILA', '00047', 2, 2, '2017-03-27', '2017-03-27', '2017-04-01', '2017-05-27', NULL, NULL, 1, 0),
(246, 34, 1, '21473', '959853379', NULL, 5, 'LOS  ANGELES', '1706', 1, 2, '2017-03-14', '2017-03-14', '2017-03-19', '2017-04-14', NULL, NULL, 1, 0),
(247, 34, 2, '1', '98201974', NULL, 4, 'MAGARI', '702', 2, 1, '2017-03-14', '2017-03-14', '2017-03-19', '2017-04-19', NULL, NULL, 1, 0),
(248, 34, 2, '2', '98201974', NULL, 4, 'MAGARI', '702', 2, 1, '2017-03-14', '2017-03-14', '2017-03-19', '2017-04-19', NULL, NULL, 1, 0),
(249, 34, 2, '3', '98201974', NULL, 4, 'MAGARI', '702', 3, 1, '2017-03-15', '2017-03-15', '2017-03-20', '2017-04-20', NULL, NULL, 1, 0),
(250, 34, 2, '4', '7LIMES1569', NULL, 3, 'MAGARI', '702', 3, 1, '2017-03-15', '2017-03-15', '2017-03-20', '2017-04-20', NULL, NULL, 1, 0),
(251, 34, 1, '21466', 'MBM160065011', NULL, 8, 'BALTIC KLIPPER', '17004', 1, 1, '2017-03-15', '2017-03-15', '2017-03-20', '2017-04-20', NULL, NULL, 1, 0),
(252, 34, 1, '21465', 'MBM160065011', NULL, 8, 'BALTIC KLIPPER', '17004', 1, 2, '2017-03-16', '2017-03-16', '2017-03-20', '2017-04-20', NULL, NULL, 1, 0),
(253, 34, 1, '21472', '7LIMES1569', NULL, 3, 'MAGARI', '702', 3, 2, '2017-03-16', '2017-03-16', '2017-03-21', '2017-04-16', NULL, NULL, 1, 0),
(254, 34, 3, '1', 'LMM021215', NULL, 2, 'MAGARI', '006', 3, 1, '2017-03-18', '2017-03-18', '2017-03-23', '2017-04-16', NULL, NULL, 1, 0),
(255, 34, 1, '21471', '7LIMES1569', NULL, 3, 'MAGARI', '702', 3, 2, '2017-03-18', '2017-03-18', '2017-03-21', '2017-04-16', NULL, NULL, 1, 0),
(256, 34, 1, '21469', '7LIMES1569', NULL, 3, 'MAGARI', '702', 3, 2, '2017-03-17', '2017-03-17', '2017-03-21', '2017-04-16', NULL, NULL, 1, 0),
(257, 34, 1, '21468', '7LIMES1569', NULL, 3, 'MAGARI', '702', 3, 2, '2017-03-16', '2017-03-16', '2017-03-21', '2017-04-16', NULL, NULL, 1, 0),
(258, 34, 4, '1', '98219408', NULL, 4, 'MAGARI', '702', 3, 2, '2017-03-19', '2017-03-19', '2017-03-23', '2017-04-23', NULL, NULL, 1, 0),
(259, 34, 4, '1', '98219408', NULL, 4, 'MAGARI', '702', 3, 2, '2017-03-19', '2017-03-19', '2017-03-23', '2017-04-23', NULL, NULL, 1, 0),
(260, 34, 5, '2', '27803805527A', NULL, 6, 'ALIOTH', '00166', 2, 2, '2017-03-19', '2017-03-19', '2017-03-25', '2017-04-25', NULL, NULL, 1, 0),
(261, 34, 5, '3', '27803812640A', NULL, 6, 'ALIOTH', '00166', 2, 2, '2017-03-30', '2017-03-30', '2017-04-05', '2017-05-05', NULL, NULL, 1, 0),
(262, 34, 5, '1', '27803805532A', NULL, 6, 'ALIOTH', '00166', 2, 2, '2017-03-19', '2017-03-19', '2017-03-26', '2017-04-26', NULL, NULL, 1, 0),
(263, 35, 1, '21354', '572123443', NULL, 5, 'SEALAND GUAYAQUIL', '1706', 1, 2, '2017-03-07', '2017-03-07', '2017-03-12', '2017-04-07', NULL, NULL, 1, 0),
(264, 35, 1, '21356', '572123443', NULL, 5, 'SEALAND GUAYAQUIL', '1706', 1, 2, '2017-03-07', '2017-03-07', '2017-03-12', '2017-04-07', NULL, NULL, 1, 0),
(265, 35, 1, '21355', '572123443', NULL, 5, 'SEALAND GUAYAQUIL', '1706', 1, 2, '2017-03-07', '2017-03-07', '2017-03-12', '2017-04-07', NULL, NULL, 1, 0),
(266, 35, 1, '21347', 'MBM160065010', NULL, 8, 'NEDERLAND REEFER', '17003', 1, 2, '2017-03-08', '2017-03-08', '2017-03-13', '2017-04-13', NULL, NULL, 1, 0),
(267, 35, 1, '21349', 'MBM160065010', NULL, 8, 'NEDERLAND REEFER', '17003', 1, 2, '2017-03-08', '2017-03-08', '2017-03-13', '2017-04-13', NULL, NULL, 1, 0),
(268, 35, 1, '21348', 'MBM160065010', NULL, 8, 'NEDERLAND REEFER', '17003', 1, 2, '2017-03-08', '2017-03-08', '2017-03-13', '2017-04-13', NULL, NULL, 1, 0),
(269, 35, 1, '21351', '087LIM227108', NULL, 7, 'ARUSHIV', 'NQ701R', 3, 2, '2017-03-09', '2017-03-09', '2017-03-14', '2017-04-14', NULL, NULL, 1, 0),
(270, 35, 1, '21352', '087LIM227108', NULL, 7, 'ARUSHIV', 'NQ701R', 3, 2, '2017-03-09', '2017-03-09', '2017-03-14', '2017-04-14', NULL, NULL, 1, 0),
(271, 35, 1, '21353', '087LIM227108', NULL, 7, 'ARUSHIV', 'NQ701R', 3, 2, '2017-03-09', '2017-03-09', '2017-03-14', '2017-04-14', NULL, NULL, 1, 0),
(272, 35, 1, '21350', '087LIM227108', NULL, 7, 'ARUSHIV', 'NQ701R', 3, 2, '2017-03-08', '2017-03-08', '2017-03-14', '2017-04-14', NULL, NULL, 1, 0),
(273, 35, 1, '21357', '087LIM227108', NULL, 7, 'ARUSHIV.', 'NQ701R', 3, 2, '2017-03-10', '2017-03-10', '2017-03-15', '2017-04-14', NULL, NULL, 1, 0),
(274, 35, 3, '1', 'LMM0211849', NULL, 2, 'MAGARI', '005', 6, 1, '2017-03-11', '2017-03-11', '2017-03-16', '2017-04-16', NULL, NULL, 1, 0),
(275, 35, 3, '2', 'LMM0211849', NULL, 2, 'MAGARI', '005', 6, 1, '2017-03-11', '2017-03-11', '2017-03-16', '2017-04-16', NULL, NULL, 1, 0),
(276, 35, 2, '2', '98171199', NULL, 4, 'MAGARI', '005', 3, 1, '2017-03-13', '2017-03-13', '2017-03-17', '2017-04-17', NULL, NULL, 1, 0),
(277, 35, 2, '1', '087LIM227465', NULL, 7, 'ARUSHIV.', 'NQ701R', 3, 1, '2017-03-11', '2017-03-11', '2017-03-17', '2017-04-17', NULL, NULL, 1, 0),
(278, 35, 4, '1', '98183899', NULL, 4, 'MAGARI', '711', 3, 2, '2017-03-12', '2017-03-12', '2017-03-17', '2017-04-17', NULL, NULL, 1, 0),
(279, 35, 4, '2', '98183899', NULL, 4, 'MAGARI', '711', 3, 2, '2017-03-12', '2017-03-12', '2017-03-17', '2017-04-17', NULL, NULL, 1, 0),
(280, 35, 5, '1', '27803800762A', NULL, 6, 'JAMILA', '00046', 2, 2, '2017-03-13', '2017-03-13', '2017-03-18', '2017-04-13', NULL, NULL, 1, 0),
(281, 35, 1, '21491', 'APLU 902297338', NULL, 1, 'JAMILA', '046', 7, 2, '2017-03-13', '2017-03-13', '2017-03-18', '2017-04-13', NULL, NULL, 1, 0),
(282, 35, 1, '21492', 'APLU 902297338', NULL, 1, 'JAMILA', '046', 7, 2, '2017-03-13', '2017-03-13', '2017-03-18', '2017-04-13', NULL, NULL, 1, 0),
(283, 37, 1, '21262', '959681955', NULL, 5, 'SEALAND MANZANILLO', '1704', 1, 2, '2017-02-27', '2017-02-27', '2017-03-01', '2017-04-01', NULL, NULL, 1, 0),
(284, 37, 1, '21263', 'MBM140178128', NULL, 8, 'STR HELLAS  REEFER', '17018', 1, 2, '2017-03-01', '2017-03-01', '2017-03-05', '2017-04-05', NULL, NULL, 1, 0),
(285, 37, 1, '21322', '959681955', NULL, 5, 'SEALAND MANZANILLO', '1704', 1, 2, '2017-02-28', '2017-02-28', '2017-03-01', '2017-04-01', NULL, NULL, 1, 0),
(286, 37, 1, '21321', '959681955', NULL, 5, 'SEALAND MANZANILLO', '1704', 1, 2, '2017-02-28', '2017-02-28', '2017-03-01', '2017-04-01', NULL, NULL, 1, 0),
(287, 37, 1, '21264', 'MBM140178128', NULL, 8, 'STR HELLAS  REEFER', '17018', 1, 2, '2017-03-01', '2017-03-01', '2017-03-05', '2017-04-05', NULL, NULL, 1, 0),
(288, 37, 1, '21265', 'MBM140178128', NULL, 8, 'STR HELLAS  REEFER', '17018', 1, 2, '2017-03-01', '2017-03-01', '2017-03-05', '2017-04-05', NULL, NULL, 1, 0),
(289, 37, 1, '21259', '087LIME226348', NULL, 7, 'VIDISHA', '709', 3, 2, '2017-03-02', '2017-03-02', '2017-03-07', '2017-04-07', NULL, NULL, 1, 0),
(290, 37, 1, '21256', '087LIME226348', NULL, 7, 'VIDISHA', '709', 3, 2, '2017-03-02', '2017-03-02', '2017-03-07', '2017-04-07', NULL, NULL, 1, 0),
(291, 37, 1, '21266', '087LIME226348', NULL, 7, 'VIDISHA', '709', 3, 2, '2017-03-02', '2017-03-02', '2017-03-07', '2017-04-07', NULL, NULL, 1, 0),
(292, 37, 1, '21260', '087LIME226348', NULL, 7, 'VIDISHA 709', '709', 3, 2, '2017-03-03', '2017-03-03', '2017-03-08', '2017-04-08', NULL, NULL, 1, 0),
(293, 37, 1, '21261', '087LIME226348', NULL, 7, 'VIDISHA 709', '709', 3, 2, '2017-03-03', '2017-03-03', '2017-03-08', '2017-04-08', NULL, NULL, 1, 0),
(294, 37, 2, '1', '98141286', NULL, 4, 'MAGARI', '710', 3, 1, '2017-03-03', '2017-03-03', NULL, NULL, NULL, NULL, 1, 0),
(295, 37, 2, '3', '98141286', NULL, 4, 'MAGARI', '710', 3, 1, '2017-03-06', '2017-03-06', '2017-03-11', '2017-04-11', NULL, NULL, 1, 0),
(296, 37, 2, '4', '087LIM227132', NULL, 4, 'VIDISHA', '709', 3, 1, '2017-03-04', '2017-03-04', '2017-03-09', '2017-04-09', NULL, NULL, 1, 0),
(297, 37, 2, '2', '98141286', NULL, 4, 'MAGARI', '710', 3, 1, '2017-03-03', '2017-03-03', '2017-03-08', '2017-04-08', NULL, NULL, 1, 0),
(298, 37, 5, '1', '27803785159A', NULL, 6, 'ALIOTH', '00165', 2, 2, '2017-03-16', '2017-03-16', '2017-03-21', '2017-04-21', NULL, NULL, 1, 0),
(299, 37, 5, '2', '278037898449A', NULL, 6, 'ALIOTH', '00165', 2, 2, '2017-03-16', '2017-03-16', '2017-03-21', '2017-04-21', NULL, NULL, 1, 0),
(300, 37, 3, '1', 'LMM0211529', NULL, 9, 'MAGARI', '004', 6, 1, '2017-03-04', '2017-03-04', '2017-03-09', '2017-04-09', NULL, NULL, 1, 0),
(301, 37, 3, '2', 'LMM0211529', NULL, 9, 'MAGARI', '004', 6, 1, '2017-03-04', '2017-03-04', '2017-03-09', '2017-04-09', NULL, NULL, 1, 0),
(302, 37, 4, '1', '98152394', NULL, 4, 'MAGARI', '710', 3, 2, '2017-03-05', '2017-04-05', '2017-03-10', '2017-04-10', NULL, NULL, 1, 0),
(303, 37, 4, '2', '98152394', NULL, 4, 'MAGARI', '710', 3, 2, '2017-03-05', '2017-04-05', '2017-03-10', '2017-04-10', NULL, NULL, 1, 0),
(304, 38, 1, '21132', '959600274', NULL, 5, 'SEALAND PHILADELPHIA', '1704', 1, 2, '2017-02-20', '2017-02-20', '2017-02-25', '2017-03-25', NULL, NULL, 1, 0),
(305, 38, 1, '21131', '959600274', NULL, 5, 'SEALAND PHILADELPHIA', '1704', 1, 2, '2017-02-20', '2017-02-20', '2017-02-25', '2017-03-25', NULL, NULL, 1, 0),
(306, 38, 1, '21133', '959600274', NULL, 5, 'SEALAND PHILADELPHIA', '1704', 1, 2, '2017-02-20', '2017-02-20', '2017-02-25', '2017-03-25', NULL, NULL, 1, 0),
(307, 38, 1, '21134', '969500277', NULL, 5, 'SEALAND PHILADELPHIA', '1704', 3, 2, '2017-02-21', '2017-02-21', '2017-02-26', '2017-03-26', NULL, NULL, 1, 0),
(308, 38, 1, '21135', '969500277', NULL, 5, 'SEALAND PHILADELPHIA', '1704', 3, 2, '2017-02-21', '2017-02-21', '2017-02-26', '2017-03-26', NULL, NULL, 1, 0),
(309, 38, 1, '21125', 'MBM140178126', NULL, 8, 'LUZON STRAIT', '17001', 3, 2, '2017-02-22', '2017-02-22', '2017-02-26', '2017-03-26', NULL, NULL, 1, 0),
(310, 38, 1, '21127', 'MBM140178126', NULL, 8, 'LUZON STRAIT', '17001', 3, 2, '2017-02-22', '2017-02-22', '2017-02-26', '2017-03-26', NULL, NULL, 1, 0),
(311, 38, 1, '21126', 'MBM140178126', NULL, 8, 'LUZON STRAIT', '17001', 3, 2, '2017-02-22', '2017-02-22', '2017-02-26', '2017-03-26', NULL, NULL, 1, 0),
(312, 38, 1, '21128', '087LIM225770', NULL, 7, 'VAISHNAVIR', '708', 3, 2, '2017-02-23', '2017-02-23', '2017-02-28', '2017-04-28', NULL, NULL, 1, 0),
(313, 38, 1, '21129', '087LIM225770', NULL, 7, 'VAISHNAVIR', '708', 3, 2, '2017-02-23', '2017-02-23', '2017-02-28', '2017-04-28', NULL, NULL, 1, 0),
(314, 38, 1, '21130', '087LIM225770', NULL, 7, 'VAISHNAVIR', '708', 3, 2, '2017-02-23', '2017-02-23', '2017-02-28', '2017-04-28', NULL, NULL, 1, 0),
(315, 38, 1, '21048', '7LIMES1152', NULL, 3, 'MAGARI', '709', 1, 2, '2017-02-23', '2017-02-23', '2017-02-27', '2017-03-27', NULL, NULL, 1, 0),
(316, 38, 1, '21047', '7LIMES1152', NULL, 3, 'MAGARI', '709', 1, 2, '2017-02-23', '2017-02-23', '2017-02-27', '2017-03-27', NULL, NULL, 1, 0),
(317, 38, 1, '21049', '7LIMES1152', NULL, 3, 'MAGARI', '709', 1, 2, '2017-02-23', '2017-02-23', '2017-02-27', '2017-03-27', NULL, NULL, 1, 0),
(318, 38, 2, '1', '98109596', NULL, 4, 'MAGARI', '709', 3, 1, '2017-02-24', '2017-02-24', NULL, '2017-03-29', NULL, NULL, 1, 0),
(319, 38, 2, '2', '98109596', NULL, 4, 'MAGARI', '709', 3, 1, '2017-02-24', '2017-02-24', NULL, '2017-03-29', NULL, NULL, 1, 0),
(320, 38, 5, '1', '27803785117A', NULL, 6, 'JAMILA', '045', 2, 2, '2017-02-27', '2017-02-27', '2017-03-01', '2017-03-30', NULL, NULL, 1, 0),
(321, 38, 3, '1', 'B6JBK024175', NULL, 9, 'JAMILA', '045', 6, 1, '2017-02-25', '2017-02-25', '2017-03-01', '2017-04-01', NULL, NULL, 1, 0),
(322, 38, 3, '2', 'B6JBK024175', NULL, 9, 'JAMILA', '045', 6, 1, '2017-02-27', '2017-02-27', '2017-07-02', '2017-07-09', NULL, NULL, 1, 0),
(323, 38, 4, '1', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', NULL, '2017-03-30', NULL, NULL, 0, 0),
(324, 38, 4, '1', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', NULL, '2017-03-30', NULL, NULL, 0, 0),
(325, 38, 4, '1', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', NULL, '2017-03-30', NULL, NULL, 0, 0),
(326, 38, 4, '1', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', NULL, '2017-03-30', NULL, NULL, 0, 0),
(327, 38, 4, '1', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', NULL, '2017-03-30', NULL, NULL, 1, 0),
(328, 38, 4, '3', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', '2017-02-27', '2017-03-30', NULL, NULL, 1, 0),
(329, 38, 4, '2', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-25', '2017-02-25', NULL, '2017-03-30', NULL, NULL, 1, 0),
(330, 39, 1, '20910', '087LIM225155', NULL, 7, 'JULIA', '707', 3, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-03-19', NULL, NULL, 1, 0),
(331, 39, 1, '20914', '087LIM225155', NULL, 7, 'JULIA', '707', 3, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-03-19', NULL, NULL, 1, 0),
(332, 39, 1, '20911', '087LIM225155', NULL, 7, 'JULIA', '707', 3, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-03-19', NULL, NULL, 1, 0),
(333, 39, 1, '20913', '087LIM225155', NULL, 7, 'JULIA', '707', 3, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-03-19', NULL, NULL, 1, 0),
(334, 39, 1, '21046', '087LIM225155', NULL, 7, 'JULIA', '707', 3, 2, '2017-02-15', '2017-02-15', '2017-02-20', '2017-03-20', NULL, NULL, 1, 0),
(335, 39, 1, '21045', '087LIM225155', NULL, 7, 'JULIA', '707', 3, 2, '2017-02-15', '2017-02-15', '2017-02-20', '2017-03-20', NULL, NULL, 1, 0),
(336, 39, 1, '21041', 'MBM140178124', NULL, 8, 'LOMBOK STRAIT', '17013', 3, 2, '2017-02-16', '2017-02-16', '2017-02-21', '2017-03-21', NULL, NULL, 1, 0),
(337, 39, 1, '21043', 'MBM140178124', NULL, 8, 'LOMBOK STRAIT', '17013', 3, 2, '2017-02-16', '2017-02-16', '2017-02-19', '2017-03-19', NULL, NULL, 1, 0),
(338, 39, 1, '21044', 'MBM140178124', NULL, 8, 'LOMBOK STRAIT', '17013', 3, 2, '2017-02-17', '2017-02-17', '2017-02-19', '2017-03-19', NULL, NULL, 1, 0),
(339, 39, 1, '21045', 'MBM140178124', NULL, 8, 'LOMBOK STRAIT', '17013', 3, 2, '2017-02-16', '2017-02-16', '2017-02-19', '2017-03-19', NULL, NULL, 1, 0),
(340, 39, 2, '1', '98079254', NULL, 4, 'MAGARI', '708', 3, 1, '2017-02-18', '2017-02-18', '2017-02-23', '2017-03-23', NULL, NULL, 1, 0),
(341, 39, 2, '2', '98079254', NULL, 4, 'MAGARI', '708', 3, 1, '2017-02-18', '2017-02-18', '2017-02-23', '2017-03-23', NULL, NULL, 1, 0),
(342, 39, 2, '3', '98079254', NULL, 4, 'MAGARI', '708', 3, 1, '2017-02-20', '2017-02-20', '2017-02-25', '2017-03-23', NULL, NULL, 1, 0),
(343, 39, 1, '21050', '1', NULL, 5, 'MARINER', '1704', 2, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-03-19', NULL, NULL, 1, 0),
(344, 39, 1, '21051', '2', NULL, 5, 'MARINER', '1704', 2, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-03-19', NULL, NULL, 1, 0),
(345, 39, 5, '2', '087LIM25927', NULL, 6, 'SEALAND PHILADELPHIA', '1704', 2, 2, '2017-02-21', '2017-02-21', '2017-02-25', '2017-03-25', NULL, NULL, 1, 0),
(346, 39, 5, '1', '087LIM25927', NULL, 6, 'SEALAND PHILADELPHIA', '1704', 2, 2, '2017-02-20', '2017-02-20', '2017-02-25', '2017-03-25', NULL, NULL, 1, 0),
(347, 39, 3, '1', 'B6JBK024107', NULL, 9, 'ALIOTH', '164', 22, 1, '2017-02-19', '2017-02-19', '2017-02-24', '2017-03-24', NULL, NULL, 1, 0),
(348, 39, 3, '2', 'B6JBK024107', NULL, 9, 'ALIOTH', '164', 22, 1, '2017-02-19', '2017-02-19', '2017-02-24', '2017-03-24', NULL, NULL, 1, 0),
(349, 39, 4, '1', '98103030', NULL, 4, 'MAGARI', '708', 3, 9, '2017-02-17', '2017-02-17', '2017-02-21', '2017-03-21', NULL, NULL, 1, 0),
(350, 39, 4, '1', '98103030', NULL, 4, 'MAGARI', '708', 3, 9, '2017-02-17', '2017-02-17', '2017-02-21', '2017-03-21', NULL, NULL, 1, 0),
(351, 45, 1, '20915', '959459191', NULL, 5, 'MARIA  KATHARINA', '1706', 1, 2, '2017-02-06', '2017-02-06', '2017-02-11', '2017-03-11', NULL, NULL, 1, 0),
(352, 45, 1, '20916', '959459191', NULL, 5, 'MARIA  KATHARINA', '1706', 1, 2, '2017-02-06', '2017-02-06', '2017-02-11', '2017-03-11', NULL, NULL, 1, 0),
(353, 45, 1, '20918', '959459191', NULL, 5, 'MARIA  KATHARINA', '1706', 1, 2, '2017-02-06', '2017-02-06', '2017-02-11', '2017-03-11', NULL, NULL, 1, 0),
(354, 45, 1, '20917', '959459191', NULL, 5, 'MARIA  KATHARINA', '1706', 1, 2, '2017-02-06', '2017-02-06', '2017-02-11', '2017-03-11', NULL, NULL, 1, 0),
(355, 45, 1, '20919', '959459207', NULL, 5, 'MARIA  KATHARINA', '1706', 3, 2, '2017-02-08', '2017-02-05', '2017-02-12', '2017-03-12', NULL, NULL, 1, 0),
(356, 45, 1, '20805', '087LIM224347', NULL, 7, 'ZLATA', '707', 3, 2, '2017-02-07', '2017-02-07', '2017-02-12', '2017-03-12', NULL, NULL, 1, 0),
(357, 45, 1, '20803', '087LIM224347', NULL, 7, 'ZLATA', '707', 3, 2, '2017-02-07', '2017-02-07', '2017-02-12', '2017-03-12', NULL, NULL, 1, 0),
(358, 45, 1, '20804', '087LIM224347', NULL, 7, 'ZLATA', '707', 3, 2, '2017-02-07', '2017-02-07', '2017-02-12', '2017-03-12', NULL, NULL, 1, 0),
(359, 45, 1, '20806', '087LIM224347', NULL, 7, 'ZLATA', '706', 3, 2, '2017-02-08', '2017-02-08', '2017-02-13', '2017-03-13', NULL, NULL, 1, 0),
(360, 45, 1, '20904', 'MBM140178122', NULL, 8, 'ATLANTIC REEFER', '17012', 3, 2, '2017-02-08', '2017-02-08', '2017-02-13', '2017-03-13', NULL, NULL, 1, 0),
(361, 45, 1, '20904', 'MBM140178122', NULL, 8, 'ATLANTIC REEFER', '17012', 3, 2, '2017-02-08', '2017-02-08', '2017-02-13', '2017-03-13', NULL, NULL, 1, 0),
(362, 45, 1, '20908', 'MBM140178122', NULL, 8, 'ATLANTIC REEFER', '17012', 3, 2, '2017-02-10', '2017-02-10', '2017-02-12', '2017-03-14', NULL, NULL, 1, 0),
(363, 45, 1, '20907', 'MBM140178122', NULL, 8, 'ATLANTIC REEFER', '17012', 3, 2, '2017-02-08', '2017-02-08', '2017-02-13', '2017-03-13', NULL, NULL, 1, 0),
(364, 45, 1, '20906', 'MBM140178122', NULL, 8, 'ATLANTIC REEFER', '17012', 3, 2, '2017-02-09', '2017-02-09', '2017-02-13', '2017-03-13', NULL, NULL, 1, 0),
(365, 45, 1, '20905', 'MBM140178122', NULL, 8, 'ATLANTIC REEFER', '17012', 3, 2, '2017-02-09', '2017-02-09', '2017-02-13', '2017-03-13', NULL, NULL, 1, 0),
(366, 45, 1, '20909', '087LIM225155', NULL, 7, 'ZLATA', '706', 3, 2, '2017-02-11', '2017-02-11', '2017-02-16', '2017-03-16', NULL, NULL, 1, 0),
(367, 45, 2, '1', '98062266', NULL, 4, 'MAGARI', '707', 3, 1, '2017-02-10', '2017-02-10', '2017-02-15', '2017-03-10', NULL, NULL, 1, 0),
(368, 45, 2, '2', '98062266', NULL, 4, 'MAGARI', '707', 3, 1, '2017-02-11', '2017-02-11', '2017-02-15', '2017-03-10', NULL, NULL, 1, 0),
(369, 45, 5, '1', '27803765949A', NULL, 6, 'JAMILA', '044', 2, 2, '2017-02-12', '2017-02-12', '2017-02-17', '2017-03-17', NULL, NULL, 1, 0),
(370, 24, 4, '2', '98542528', 'CSVU 750614-3', 4, 'MAGARI', '723', 3, 2, '2017-06-03', '2017-06-03', '2017-06-08', '2017-07-08', NULL, NULL, 1, 0),
(371, 24, 1, '22692', '7LIMES2945', 'SUDU 616397-7', 3, 'MAGARI', '723', 3, 2, '2017-06-03', '2017-06-03', '2017-06-08', '2017-07-08', NULL, NULL, 1, 0),
(372, 40, 1, '20807', '959403367', NULL, 5, 'MERIDIAN', '1704', 1, 2, '2017-06-29', '2017-06-29', '2017-06-30', '2017-07-30', NULL, NULL, 1, 0);
INSERT INTO `contenedor` (`id`, `id_semana`, `id_cliente`, `referencia`, `booking`, `numero_contenedor`, `id_lineanaviera`, `nave`, `viaje`, `id_puertodestino`, `id_operador`, `fecha_proceso_inicio`, `fecha_proceso_fin`, `fecha_zarpe`, `fecha_llegada`, `peso_bruto`, `peso_neto`, `activo`, `valija`) VALUES
(373, 40, 1, '20808', '959403367', NULL, 5, 'MERIDIAN', '1704', 1, 2, '2017-06-29', '2017-06-29', '2017-06-07', '2017-07-07', NULL, NULL, 1, 0),
(374, 40, 1, '20809', '959403367', NULL, 5, 'MERIDIAN', '1704', 1, 2, '2017-06-30', '2017-06-30', '2017-06-07', '2017-07-07', NULL, NULL, 1, 0),
(375, 40, 1, '20810', '959403367', NULL, 5, 'MERIDIAN', '1704', 1, 2, '2017-06-30', '2017-06-30', '2017-06-07', '2017-07-07', NULL, NULL, 1, 0),
(376, 40, 1, '20811', '959403356', NULL, 5, 'MERIDIAN', 'V.1704', 3, 2, '2017-02-28', '2017-02-28', '2017-03-01', '2017-04-01', NULL, NULL, 1, 0),
(377, 40, 1, '20812', '959403356', NULL, 5, 'MERIDIAN', 'V.1704', 3, 2, '2017-01-31', '2017-01-31', '2017-02-06', '2017-02-06', NULL, NULL, 1, 0),
(378, 40, 1, '20797', 'MBM140178119', NULL, 8, 'BALTIC KLIPPER', '1701', 3, 2, '2017-02-01', '2017-02-01', '2017-02-06', '2017-03-06', NULL, NULL, 1, 0),
(379, 40, 1, '20798', 'MBM140178119', NULL, 8, 'BALTIC KLIPPER', '17011', 3, 2, '2017-02-01', '2017-02-01', '2017-02-06', '2017-03-06', NULL, NULL, 1, 0),
(380, 40, 1, '20800', 'MBM140178119', NULL, 8, 'BALTIC KLIPPER', '17011', 3, 2, '2017-02-01', '2017-02-01', '2017-02-06', '2017-03-06', NULL, NULL, 1, 0),
(381, 40, 1, '20799', 'MBM140178119', NULL, 8, 'BALTIC KLIPPER', '17011', 3, 2, '2017-02-01', '2017-02-01', '2017-02-06', '2017-03-06', NULL, NULL, 1, 0),
(382, 46, 1, '22787', 'MBM140178155', 'TRIU 835537-6', 8, 'BALTIC KLIPPER', '17016', 3, 2, '2017-06-06', '2017-06-06', '2017-06-13', '2017-07-13', NULL, NULL, 1, 0),
(383, 46, 1, '22788', 'MBM140178155', 'TRIU 811487-2', 8, 'BALTIC KLIPPER', '17016', 3, 2, '2017-06-07', '2017-06-07', '2017-06-29', '2017-07-29', NULL, NULL, 1, 0),
(384, 46, 1, '22789', 'MBM140178155', 'CAIU 554907-9', 8, 'BALTIC KLIPPER', '17016', 3, 2, '2017-06-07', '2017-06-04', '2017-06-29', '2017-07-29', NULL, NULL, 1, 0),
(385, 46, 1, '22790', '7LIMES3087', 'SUDU 625143-0', 3, 'MAGARI', '724', 3, 2, '2017-06-08', '2017-06-08', '2017-06-13', '2017-07-13', NULL, NULL, 1, 0),
(386, 40, 2, '1', '087LIM223577', NULL, 7, 'KATYA', '705', 3, 1, '2017-01-31', '2017-01-31', '2017-02-06', '2017-03-06', NULL, NULL, 1, 0),
(387, 40, 2, '2', '087LIM223577', NULL, 7, 'KATYA', '705', 3, 1, '2017-02-01', '2017-02-01', '2017-02-05', NULL, NULL, NULL, 1, 0),
(388, 40, 2, '3', '98029733', NULL, 4, 'NORASIA ALYA', '7202', 3, 1, '2017-02-01', '2017-02-01', '2017-02-05', NULL, NULL, NULL, 1, 0),
(389, 40, 2, '4', '98029733', NULL, 4, 'NORASIA ALYA', '7202', 3, 1, '2017-02-03', '2017-02-03', '2017-03-08', '2017-04-08', NULL, NULL, 1, 0),
(390, 40, 5, '1', '27803765933A', NULL, 6, 'HANSA MEERSBURG', '0008', 2, 2, '2017-02-05', '2017-02-05', '2017-02-10', '2017-03-10', NULL, NULL, 1, 0),
(391, 40, 3, '1', 'B6JBK023987', NULL, 9, 'HANSA MEERSBURG', '008', 4, 1, '2017-02-05', '2017-02-05', '2017-02-10', '2017-03-10', NULL, NULL, 1, 0),
(392, 40, 4, '1', '98038004', NULL, 4, 'NORASIA ALYA', '7202', 3, 2, '2017-02-04', '2017-02-04', '2017-02-09', '2017-03-09', NULL, NULL, 1, 0),
(393, 40, 4, '2', '98038004', NULL, 4, 'NORASIA ALYA', '7202', 3, 2, '2017-02-04', '2017-02-04', '2017-02-09', '2017-03-09', NULL, NULL, 1, 0),
(394, 41, 1, '20710', '959341518', NULL, 7, 'SEALAND GUAYAQUIL', '1704', 1, 2, '2017-01-23', '2017-01-23', '2017-01-28', '2017-02-28', NULL, NULL, 1, 0),
(395, 41, 1, '20712', '959341518', NULL, 7, 'SEALAND GUAYAQUIL', '1704', 1, 2, '2017-01-23', '2017-01-23', '2017-01-28', '2017-02-28', NULL, NULL, 1, 0),
(396, 41, 1, '20711', '959341518', NULL, 7, 'SEALAND GUAYAQUIL', '1704', 1, 2, '2017-01-23', '2017-01-23', '2017-01-28', '2017-02-28', NULL, NULL, 1, 0),
(397, 41, 1, '20713', '959341542', NULL, 7, 'SEALAND GUAYAQUIL', '1704', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', '2017-01-29', NULL, NULL, 1, 0),
(398, 41, 1, '20714', '959341542', NULL, 7, 'MSK SEALAND GUAYAQUIL', '1704', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', NULL, NULL, NULL, 1, 0),
(399, 41, 1, '20777', '959341542', NULL, 7, 'MSK SEALAND GUAYAQUIL', '1704', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', NULL, NULL, NULL, 1, 0),
(400, 41, 1, '20704', 'MBM140178116', NULL, 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', NULL, NULL, NULL, 1, 0),
(401, 41, 1, '20694', 'MBM140178116', NULL, 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', NULL, NULL, NULL, 1, 0),
(402, 41, 1, '20703', 'MBM140178116', NULL, 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', NULL, NULL, NULL, 1, 0),
(403, 41, 1, '20702', 'MBM140178116', NULL, 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-01-25', '2017-01-25', '2017-01-29', NULL, NULL, NULL, 1, 0),
(404, 41, 1, '20701', 'MBM140178116', NULL, 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', NULL, NULL, NULL, 1, 0),
(405, 41, 1, '20707', '7LIMES0443', NULL, 3, 'JACK LONDON', '651', 3, 2, '2017-01-26', '2017-01-26', '2017-01-30', NULL, NULL, NULL, 1, 0),
(406, 41, 1, '20705', '7LIMES0443', NULL, 3, 'JACK LONDON', '651', 3, 2, '2017-01-26', '2017-01-26', '2017-01-30', NULL, NULL, NULL, 1, 0),
(407, 41, 1, '20709', '7LIMES0443', NULL, 3, 'JACK LONDON', '651', 3, 2, '2017-01-26', '2017-01-26', '2017-01-30', NULL, NULL, NULL, 1, 0),
(408, 41, 1, '20708', '7LIMES0443', NULL, 3, 'JACK LONDON', '651', 3, 2, '2017-01-26', '2017-01-26', '2017-01-30', NULL, NULL, NULL, 1, 0),
(409, 41, 1, '20706', '7LIMES0443', NULL, 3, 'JACK LONDON', '651', 3, 2, '2017-01-27', '2017-01-27', '2017-02-01', NULL, NULL, NULL, 1, 0),
(410, 41, 5, '1', '27803759611A', NULL, 6, 'JAMILA', '043', 2, 2, '2017-01-28', '2017-01-28', '2017-02-01', NULL, NULL, NULL, 1, 0),
(411, 41, 5, '2', '27803759611A', NULL, 6, 'JAMILA', '043', 2, 2, '2017-01-28', '2017-01-28', '2017-02-01', NULL, NULL, NULL, 1, 0),
(412, 41, 4, '3', '98021416', NULL, 4, 'HANSA EUROPE', '6252', 3, 2, '2017-01-28', '2017-01-28', '2017-01-30', NULL, NULL, NULL, 1, 0),
(413, 41, 4, '1', '97987759', NULL, 4, 'JACK LONDON', '651', 3, 2, '2017-01-25', '2017-01-25', '2017-01-30', NULL, NULL, NULL, 1, 0),
(414, 41, 4, '2', '98021416', NULL, 4, 'HANSA EUROPE', '6252', 3, 2, '2017-01-27', '2017-01-27', '2017-01-30', NULL, NULL, NULL, 1, 0),
(415, 44, 1, '20600', '959266652', NULL, 8, 'MARIA  KATHARINA', '1704', 3, 2, '2017-01-16', '2017-01-16', '2017-01-19', '2017-02-19', NULL, NULL, 1, 0),
(416, 44, 1, '20597', '959266652', NULL, 8, 'MARIA  KATHARINA', '1704', 3, 2, '2017-01-14', '2017-01-14', '2017-01-19', '2017-02-19', NULL, NULL, 1, 0),
(417, 44, 1, '20599', '959266652', NULL, 8, 'MARIA  KATHARINA', '1704', 3, 2, '2017-01-16', '2017-01-16', '2017-01-19', '2017-02-19', NULL, NULL, 1, 0),
(418, 44, 1, '20598', '959266652', NULL, 8, 'MARIA  KATHARINA', '1704', 3, 2, '2017-01-16', '2017-01-16', '2017-01-19', '2017-02-19', NULL, NULL, 1, 0),
(419, 44, 1, '20602', '959266659', NULL, 5, 'MARIA  KATHARINA', '1704', 1, 2, '2017-01-17', '2017-01-17', '2017-01-21', '2017-02-21', NULL, NULL, 1, 0),
(420, 44, 1, '20601', '959266659', NULL, 5, 'MARIA  KATHARINA', '1704', 1, 2, '2017-01-17', '2017-01-17', '2017-01-21', '2017-02-21', NULL, NULL, 1, 0),
(421, 44, 1, '20604', '959266659', NULL, 5, 'MARIA  KATHARINA', '1704', 1, 2, '2017-01-17', '2017-01-17', '2017-01-21', '2017-02-21', NULL, NULL, 1, 0),
(422, 44, 1, '20603', '959266659', NULL, 5, 'MARIA  KATHARINA', '1704', 1, 2, '2017-01-17', '2017-01-17', '2017-01-21', '2017-02-21', NULL, NULL, 1, 0),
(423, 44, 1, '20593', 'MBM140178114', NULL, 8, 'ATLANTIC REEFER', '17008', 1, 2, '2017-01-20', '2017-01-20', '2017-01-24', '2017-02-24', NULL, NULL, 1, 0),
(424, 44, 1, '20592', 'MBM140178114', NULL, 3, 'ATLANTIC REEFER', '17008', 1, 2, '2017-01-19', '2017-01-19', '2017-01-23', '2017-02-23', NULL, NULL, 1, 0),
(425, 44, 1, '20591', 'MBM140178114', NULL, 3, 'ATLANTIC REEFER', '17008', 1, 2, '2017-01-19', '2017-01-19', '2017-01-23', '2017-02-23', NULL, NULL, 1, 0),
(426, 44, 1, '20590', 'MBM140178114', NULL, 8, 'ATLANTIC REEFER', '17008', 1, 2, '2017-01-18', '2017-01-18', '2017-01-23', '2017-02-23', NULL, NULL, 1, 0),
(427, 44, 1, '20589', 'MBM140178114', NULL, 8, 'ATLANTIC REEFER', '17008', 1, 2, '2017-01-18', '2017-01-18', '2017-01-23', '2017-02-23', NULL, NULL, 1, 0),
(428, 44, 1, '20596', '7LIMES0316', NULL, 3, 'HAMMONIA VENETIA', '550', 1, 2, '2017-01-20', '2017-01-20', '2017-01-25', '2017-02-20', NULL, NULL, 1, 0),
(429, 44, 1, '20591', '7LIMES0316', NULL, 3, 'HAMMONIA VENETIA', '550', 1, 2, '2017-01-20', '2017-01-20', '2017-01-25', '2017-02-20', NULL, NULL, 1, 0),
(430, 44, 1, '20594', '7LIMES0316', NULL, 3, 'HAMMONIA VENETIA', '550', 1, 2, '2017-01-20', '2017-01-20', '2017-01-25', '2017-02-20', NULL, NULL, 1, 0),
(431, 44, 2, '2', '97975720', NULL, 4, 'CAP DUOKATO', '6251', 3, 1, '2017-01-21', '2017-01-21', '2017-01-26', '2017-02-26', NULL, NULL, 1, 0),
(432, 44, 2, '1', '97975720', NULL, 4, 'CAP DUOKATO', '6251', 3, 1, '2017-01-21', '2017-01-21', '2017-01-26', '2017-02-26', NULL, NULL, 1, 0),
(433, 44, 5, '1', '27803747380A', NULL, 6, 'HANSA MEERSBURG', '07', 2, 2, '2017-01-22', '2017-01-22', '2017-01-27', '2017-02-27', NULL, NULL, 1, 0),
(434, 44, 3, '1', 'B6JBK023876', NULL, 9, 'HANSA MEERSBURG', '07', 4, 1, '2017-01-22', '2017-01-22', '2017-01-27', '2017-02-27', NULL, NULL, 1, 0),
(435, 44, 3, '1', 'B6JBK023876', NULL, 9, 'HANSA MEERSBURG', '07', 4, 1, '2017-01-22', '2017-01-22', '2017-01-27', '2017-02-27', NULL, NULL, 0, 0),
(436, 44, 4, '2', '97987759', NULL, 4, 'CAP DUOKATO', '6251', 3, 2, '2017-01-19', '2017-01-19', '2017-01-23', '2017-02-25', NULL, NULL, 1, 0),
(437, 44, 4, '3', '97987759', NULL, 4, 'CAP DUOKATO', '6251', 3, 2, '2017-01-19', '2017-01-19', '2017-01-23', '2017-02-25', NULL, NULL, 1, 0),
(438, 44, 4, '1', '97987759', NULL, 4, 'CAP DUOKATO', '6251', 3, 2, '2017-01-18', '2017-01-18', '2017-01-23', '2017-02-25', NULL, NULL, 1, 0),
(439, 43, 1, '20466', '959182291', NULL, 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', NULL, NULL, 1, 0),
(440, 43, 1, '20467', '959182291', NULL, 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', NULL, NULL, 1, 0),
(441, 43, 1, '20474', '959182291', NULL, 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-09', '2017-01-09', '2017-01-12', '2017-02-12', NULL, NULL, 1, 0),
(442, 43, 1, '20473', '959182291', NULL, 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', NULL, NULL, 1, 0),
(443, 43, 1, '20472', '959182291', NULL, 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-08', '2017-01-06', '2017-01-12', '2017-02-12', NULL, NULL, 1, 0),
(444, 43, 1, '20471', '959182291', NULL, 5, 'SEALAND PHILADELPHIA', '1702', 1, 2, '2017-01-08', '2017-01-08', '2017-01-12', '2017-02-12', NULL, NULL, 1, 0),
(445, 43, 1, '20470', '959182308', NULL, 5, 'SEALAND PHILADELPHIA', '1702', 1, 2, '2017-01-09', '2017-01-09', '2017-01-14', '2017-02-09', NULL, NULL, 1, 0),
(446, 43, 1, '20469', '959182308', NULL, 5, 'SEALAND PHILADELPHIA', '1702', 1, 2, '2017-01-09', '2017-01-09', '2017-01-14', '2017-02-09', NULL, NULL, 1, 0),
(447, 43, 1, '20468', '959182308', NULL, 5, 'SEALAND PHILADELPHIA', '1702', 1, 2, '2017-01-09', '2017-01-09', '2017-01-14', '2017-02-09', NULL, NULL, 1, 0),
(448, 46, 2, '1', '98568834', 'HLXU 877275-9', 4, 'MAGARI', '724', 3, 1, '2017-06-09', '2017-06-09', '2017-06-16', '2017-07-16', NULL, NULL, 1, 0),
(449, 46, 1, '22791', '7LIMES3087', 'SUDU 800455-2', 3, 'MAGARI', '724', 3, 2, '2017-06-08', '2017-06-08', '2017-06-16', '2017-07-16', NULL, NULL, 1, 0),
(450, 46, 1, '22792', '7LIMES3087', 'SUDU 801296-4', 3, 'MAGARI', '724', 3, 2, '2017-06-11', '2017-06-11', '2017-06-16', '2017-07-16', NULL, NULL, 1, 0),
(451, 46, 1, '22794', '7LIMES3087', 'SUDU 823377-5', 3, 'MAGARI', '724', 3, 2, '2017-06-10', '2017-06-10', '2017-06-18', '2017-07-18', NULL, NULL, 1, 0),
(452, 46, 1, '22797', '960930886', 'MNBU 041418-0', 5, 'SEALAND LOS ANGELES', '1710', 1, 2, '2017-06-06', '2017-06-06', '2017-06-09', '2017-07-02', NULL, NULL, 1, 0),
(453, 46, 1, '1', '98586654', 'CPSU 515501-3', 4, 'MAGARI', '724', 3, 2, '2017-06-09', '2017-06-09', '2017-06-14', '2017-07-02', NULL, NULL, 0, 0),
(454, 46, 1, '22796', '960930886', 'MNBU 308760-4', 5, 'SEALAND LOS ANGELES', '1710', 1, 2, '2017-06-05', '2017-06-05', '2017-06-09', '2017-07-02', NULL, NULL, 1, 0),
(455, 46, 1, '22798', '960921067', 'MMAU 124524-0', 5, 'SEALAND LOS ANGELES', '1710', 7, 2, '2017-06-06', '2017-06-06', '2017-06-09', '2017-07-09', NULL, NULL, 1, 0),
(456, 47, 1, '22897', 'MBM140178156', 'SZLU 980178-6', 8, 'ATLANTIC KLIPPER', '17017', 3, 2, '2017-06-14', '2017-06-14', '2017-06-18', '2017-07-06', NULL, 21600, 1, 0),
(457, 47, 1, '22895', 'MBM140178156', 'CAIU 556259-5', 8, 'ATLANTIC KLIPPER', '17017', 3, 2, '2017-06-13', '2017-06-13', '2017-06-18', '2017-07-06', NULL, 21600, 1, 0),
(458, 47, 1, '22896', 'MBM140178156', 'CAIU 556413-4', 8, 'ATLANTIC KLIPPER', '17017', 3, 2, '2017-06-13', '2017-06-13', '2017-06-18', '2017-07-06', NULL, 21600, 1, 0),
(459, 47, 1, '22902', '7LIMES3276', 'SUDU 801981-9', 3, 'MAGARI', '725', 3, 2, '2017-06-17', '2017-06-17', '2017-06-20', '2017-07-11', NULL, 21600, 1, 0),
(460, 47, 1, '22901', '7LIMES3276', 'SUDU 629428-3', 3, 'MAGARI', '725', 3, 2, '2017-06-15', '2017-06-15', '2017-06-20', '2017-07-11', NULL, 21600, 1, 0),
(461, 47, 1, '22900', '7LIMES3276', 'SUDU 628780-7', 3, 'MAGARI', '725', 3, 2, '2017-06-14', '2017-06-14', '2017-06-20', '2017-07-11', NULL, 21600, 1, 0),
(462, 47, 1, '22899', '7LIMES3276', 'SUDU 801152-5', 3, 'MAGARI', '725', 3, 2, '2017-06-14', '2017-06-14', '2017-06-20', '2017-07-11', NULL, 21600, 1, 0),
(463, 47, 1, '22898', '7LIMES3276', 'SUDU 803505-0', 3, 'MAGARI', '725', 3, 2, '2017-06-15', '2017-06-15', '2017-06-20', '2017-07-11', NULL, 21600, 1, 0),
(464, 47, 1, '22905', '961020455', 'MMAU 111927-8', 5, 'SEALAND BALBOA', '1710', 7, 2, '2017-06-13', '2017-06-13', '2017-06-16', '2017-07-09', NULL, 28000, 1, 0),
(465, 47, 1, '22903', '961030470', 'MNBU 365704-0', 5, 'SEALAND BALBOA', '1710', 1, 2, '2017-06-12', '2017-06-12', '2017-06-16', '2017-07-09', NULL, 21600, 1, 0),
(466, 47, 1, '22904', '961030470', 'MWCU 678940-0', 5, 'SEALAND BALBOA', '1710', 1, 2, '2017-06-12', '2017-06-12', '2017-06-16', '2017-07-09', NULL, 21600, 1, 0),
(467, 43, 1, '20459', 'MBM140178112', NULL, 8, 'LUZON STRAIT', '17007', 2, 2, '2017-01-10', '2017-01-10', '2017-01-15', '2017-02-14', NULL, NULL, 1, 0),
(468, 43, 1, '20461', 'MBM140178112', NULL, 8, 'LUZON STRAIT', '17007', 3, 2, '2017-01-10', '2017-01-10', '2017-01-15', '2017-02-10', NULL, NULL, 1, 0),
(469, 43, 1, '20460', 'MBM140178112', NULL, 8, 'LUZON STRAIT', '17007', 3, 2, '2017-01-10', '2017-01-10', '2017-01-15', '2017-02-10', NULL, NULL, 1, 0),
(470, 43, 1, '20463', 'MBM140178112', NULL, 8, 'LUZON STRAIT', '17007', 3, 2, '2017-01-10', '2017-01-11', '2017-01-15', '2017-02-10', NULL, NULL, 1, 0),
(471, 43, 1, '20466', 'MBM140178112', NULL, 8, 'LUZON STRAIT', '17007', 3, 2, '2017-01-10', '2017-01-10', '2017-01-15', '2017-02-15', NULL, NULL, 1, 0),
(472, 43, 5, '20465', '27803738075A', NULL, 6, 'JAMILA', '0042', 2, 2, '2017-01-14', '2017-01-14', '2017-01-17', '2017-02-15', NULL, NULL, 1, 0),
(473, 43, 2, '2', '97948060', NULL, 4, 'BALTASAR SCHULTE', '6250', 3, 1, '2017-01-12', '2017-01-12', '2017-01-17', '2017-02-15', NULL, NULL, 1, 0),
(474, 43, 2, '1', '97948060', NULL, 4, 'BALTASAR SCHULTE', '6250', 3, 1, '2017-01-12', '2017-01-12', '2017-01-17', '2017-02-15', NULL, NULL, 1, 0),
(475, 43, 1, '20464', '7LIMES0207', NULL, 3, 'GLASGOW EXPRESS', '6149', 3, 2, '2017-01-11', '2017-01-11', '2017-01-17', '2017-02-15', NULL, NULL, 1, 0),
(476, 43, 1, '20465', '7LIMES0207', NULL, 3, 'GLASGOW EXPRESS', '6149', 3, 2, '2017-01-11', '2017-01-11', '2017-01-17', '2017-02-15', NULL, NULL, 1, 0),
(477, 43, 3, '1', 'LMM0209565', NULL, 9, 'JAMILA', '042', 6, 1, '2017-01-13', '2017-01-13', '2017-01-18', '2017-02-18', NULL, NULL, 1, 0),
(478, 43, 3, '2', 'LMM0209565', NULL, 9, 'JAMILA', '042', 5, 1, '2017-01-14', '2017-01-14', '2017-01-19', '2017-02-12', NULL, NULL, 1, 0),
(479, 43, 4, '2', '97963010', NULL, 4, 'BALTASAR SCHULTE', '6250', 1, 2, '2017-01-12', '2017-01-12', '2017-01-17', '2017-02-12', NULL, NULL, 1, 0),
(480, 43, 4, '1', '97963010', NULL, 4, 'BALTASAR SCHULTE', '6250', 1, 2, '2017-01-12', '2017-01-12', '2017-01-17', '2017-02-12', NULL, NULL, 1, 0),
(481, 43, 4, '3', '97963010', NULL, 4, 'BALTASAR SCHULTE', '6250', 1, 2, '2017-01-12', '2017-01-12', '2017-01-17', '2017-02-12', NULL, NULL, 1, 0),
(482, 43, 4, '4', '97963010', NULL, 4, 'BALTASAR SCHULTE', '6250', 3, 2, '2017-01-13', '2017-01-13', '2017-01-17', '2017-02-12', NULL, NULL, 1, 0),
(483, 42, 1, '20386', '959097108', NULL, 5, 'MARINER', '1702', 3, 2, '2016-12-31', '2016-12-31', '2017-01-04', '2017-02-01', NULL, NULL, 1, 0),
(484, 42, 1, '20381', '959097108', NULL, 5, 'MARINER', '1702', 3, 2, '2016-12-02', '2016-12-02', '2017-01-06', '2017-02-01', NULL, NULL, 1, 0),
(485, 42, 1, '20380', '959097108', NULL, 5, 'MARINER', '1702', 1, 2, '2016-12-31', '2016-12-31', '2017-01-04', '2017-02-01', NULL, NULL, 1, 0),
(486, 42, 1, '20387', '959097108', NULL, 5, 'MARINER', '1702', 3, 2, '2016-12-02', '2016-12-02', '2017-01-07', '2017-02-01', NULL, NULL, 1, 0),
(487, 42, 1, '20382', '959097108', NULL, 5, 'MARINER', '1702', 3, 2, '2017-01-03', '2017-01-03', '2017-01-08', '2017-02-08', NULL, NULL, 1, 0),
(488, 42, 1, '20384', '959097140', NULL, 5, 'MARINER', '1702', 3, 9, '2017-01-03', '2017-01-03', '2017-01-08', '2017-02-08', NULL, NULL, 1, 0),
(489, 42, 1, '20383', '959097140', NULL, 5, 'MARINER', '1702', 3, 9, '2017-01-03', '2017-01-03', '2017-01-08', '2017-02-08', NULL, NULL, 1, 0),
(490, 42, 1, '20385', '959097140', NULL, 5, 'MARINER', '1702', 3, 9, '2017-01-03', '2017-01-03', '2017-01-08', '2017-02-08', NULL, NULL, 1, 0),
(491, 42, 1, '20376', 'MBM140178110', NULL, 8, 'LOMBOK STRAIT', '16103', 3, 2, '2017-01-04', '2017-01-04', '2017-01-09', '2017-02-10', NULL, NULL, 1, 0),
(492, 42, 1, '20374', 'MBM140178110', NULL, 8, 'LOMBOK STRAIT', '16103', 3, 2, '2017-01-04', '2017-01-04', '2017-01-09', '2017-02-10', NULL, NULL, 1, 0),
(493, 42, 1, '20378', '6LIMES8757', NULL, 3, 'NORTHERN DEXTERITY', '6249', 3, 2, '2017-11-06', '2017-01-06', '2017-01-08', '2017-02-08', NULL, NULL, 1, 0),
(494, 42, 1, '20377', '6LIMES8757', NULL, 3, 'NORTHERN DEXTERITY', '6249', 3, 2, '2017-11-05', '2017-01-05', '2017-01-08', '2017-02-08', NULL, NULL, 1, 0),
(495, 42, 1, '20379', '6LIMES8757', NULL, 3, 'NORTHERN DEXTERITY', '6249', 3, 2, '2017-01-06', '2017-01-06', '2017-01-11', '2017-02-11', NULL, NULL, 1, 0),
(496, 42, 2, '4', '97916134', NULL, 3, 'NORTHERN DEXTERITY', '6249', 3, 1, '2017-01-04', '2017-01-04', '2017-01-09', '2017-02-09', NULL, NULL, 1, 0),
(497, 42, 2, '3', '97916134', NULL, 3, 'NORTHERN DEXTERITY', '6249', 3, 1, '2017-01-05', '2017-01-01', '2017-01-09', '2017-02-09', NULL, NULL, 1, 0),
(498, 42, 2, '2', '97916134', NULL, 3, 'NORTHERN DEXTERITY', '6249', 3, 1, '2017-01-05', '2017-01-31', '2017-01-09', '2017-02-09', NULL, NULL, 1, 0),
(499, 42, 2, '1', '97916134', NULL, 3, 'NORTHERN DEXTERITY', '6249', 3, 1, '2017-01-04', '2017-01-04', '2017-01-09', '2017-02-09', NULL, NULL, 1, 0),
(500, 42, 5, '1', '27803720646A', NULL, 6, 'HANSA MEERS BURG', '06', 2, 2, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', NULL, NULL, 1, 0),
(501, 42, 3, '2', 'B6JBK023765', NULL, 6, 'HANSA MEERS BURG', '06', 4, 1, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', NULL, NULL, 1, 0),
(502, 42, 3, '1', 'B6JBK023765', NULL, 8, 'HANSA MEERS BURG', '06', 4, 1, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', NULL, NULL, 1, 0),
(503, 42, 4, '1', '97916337', NULL, 4, 'NORTHERN DEXTERITY', '6249', 1, 2, '2017-01-06', '2017-01-06', '2017-01-11', '2017-02-11', NULL, NULL, 1, 0),
(504, 42, 4, '2', '97916337', NULL, 4, 'NORTHERN DEXTERITY', '6249', 3, 2, '2017-01-06', '2017-01-06', '2017-01-11', '2017-02-11', NULL, NULL, 1, 0),
(505, 46, 2, '2', '98568834', 'HLXU 876189-9', 4, 'MAGARI', '724', 3, 1, '2017-06-09', '2017-06-09', '2017-06-14', '2017-07-14', NULL, NULL, 1, 0),
(506, 46, 4, '2', '98586654', 'HLXU 874770-9', 4, 'MAGARI', '724', 3, 2, '2017-06-10', '2017-06-10', '2017-06-15', '2017-07-15', NULL, NULL, 1, 0),
(507, 46, 4, '1', '98586654', 'CPSU 515501-3', 4, 'MAGARI', '724', 3, 2, '2017-06-09', '2017-06-09', '2017-06-14', '2017-07-14', NULL, NULL, 1, 0),
(508, 46, 3, '1', 'LMM0215762', 'TLLU 104789-0', 2, 'MAGARI', '018', 24, 1, '2017-06-10', '2017-06-10', '2017-06-15', '2017-07-15', NULL, NULL, 1, 0),
(509, 46, 3, '2', 'B6JBK024858', 'DFIU 811104-6', 9, 'ALIOTH', '712', 4, 1, '2017-06-12', '2017-06-12', '2017-06-17', '2017-07-17', NULL, NULL, 1, 0),
(510, 46, 5, '1', '7LIMAG1480', 'SUDU 623810-3', 3, 'MAGARI', '724', 2, 2, '2017-06-10', '2017-06-10', '2017-06-15', '2017-07-15', NULL, NULL, 1, 0),
(511, 46, 5, '2', '7LIMAG1501', 'SUDU 627365-5', 3, 'MAGARI', '724', 2, 2, '2017-06-10', '2017-06-10', '2017-06-15', '2017-07-15', NULL, NULL, 1, 0),
(512, 48, 1, '22997', 'MBM140178159', 'CXRU 111677-3', 8, 'LUZON STRAIT', '17019', 3, 2, '2017-06-20', '2017-07-20', '2017-06-25', '2017-07-13', NULL, 19591.2, 1, 0),
(513, 48, 1, '22999', 'MBM140178159', 'CXRU 152387-1', 8, 'LUZON STRAIT', '17019', 3, 2, '2017-06-21', '2017-07-16', '2017-06-25', '2017-07-13', NULL, 19591.2, 1, 0),
(514, 48, 1, '22998', 'MBM140178159', 'TRIU 859069-4', 8, 'LUZON STRAIT', '17019', 3, 2, '2017-06-20', '2017-07-20', '2017-06-25', '2017-07-13', NULL, 19591.2, 1, 0),
(515, 48, 1, '23002', '7LIMES3383', 'SUDU 805116-9', 3, 'LISZT', '726', 3, 2, '2017-06-21', '2017-06-21', '2017-06-27', '2017-07-18', NULL, 19591.2, 1, 0),
(516, 48, 1, '23001', '7LIMES3383', 'SUDU 821029-7', 3, 'LISZT', '726', 3, 2, '2017-06-17', '2017-06-21', '2017-06-27', '2017-07-18', NULL, 19591.2, 1, 0),
(517, 48, 1, '23000', '7LIMES3383', 'SUDU 811441-5', 3, 'LISZT', '726', 3, 2, '2017-06-22', '2017-06-22', '2017-06-27', '2017-06-18', NULL, 19591.2, 1, 0),
(518, 48, 1, '23003', '7LIMES3383', 'SUDU 817060-9', 3, 'LISZT', '726', 3, 2, '2017-06-24', '2017-06-24', '2017-06-27', '2017-06-18', NULL, 19591.2, 1, 0),
(519, 48, 1, '23006', '961097019', 'MMAU 101782-5', 5, 'MARINER', '1710', 7, 2, '2017-06-20', '2017-06-20', '2017-06-23', '2017-07-16', NULL, 18900, 1, 0),
(520, 48, 1, '23004', '961106881', 'PONU 483173-3', 5, 'MARINER', '1710', 1, 2, '2017-06-19', '2017-06-19', '2017-06-23', '2017-07-16', NULL, 19591.2, 1, 0),
(521, 48, 1, '23005', '961106881', 'PONU 484117-7', 5, 'MARINER', '1710', 1, 2, '2017-06-19', '2017-06-19', '2017-06-23', '2017-07-16', NULL, 19591.2, 1, 0),
(522, 47, 2, '1', '98598984', 'HLXU 875946-4', 4, 'MAGARI', '725', 3, 1, '2017-06-15', '2017-06-15', '2017-06-21', '2017-07-19', NULL, 21600, 1, 0),
(523, 47, 2, '2', '98598984', 'HLBU 900823-9', 4, 'MAGARI', '725', 3, 1, '2017-06-16', '2017-06-16', '2017-06-21', '2017-07-19', NULL, 21600, 1, 0),
(524, 47, 4, '1', '98622252', 'TCLU 117750-8', 4, 'MAGARI', '725', 3, 2, '2017-06-16', '2017-06-16', '2017-06-22', '2017-07-12', NULL, 21600, 1, 0),
(525, 47, 4, '2', '98622252', 'HLBU 902370-0', 4, 'MAGARI', '725', 3, 2, '2017-06-16', '2017-06-16', '2017-06-22', '2017-07-12', NULL, 21600, 1, 0),
(526, 47, 5, '2', '7LIMAG1548', 'SUDU 810704-1', 3, 'MAGARI', '725', 2, 2, '2017-06-17', '2017-06-17', '2017-06-24', '2017-07-14', NULL, 19200, 1, 0),
(527, 47, 5, '2', '7LIMAG1552', 'SUDU 803122-3', 3, 'MAGARI', '725', 2, 2, '2017-06-17', '2017-06-17', '2017-06-24', '2017-07-13', NULL, 19200, 1, 0),
(528, 47, 3, '1', 'LMM0216123', 'TLLU 105911-8', 2, 'MAGARI', '019', 6, 1, '2017-06-17', '2017-06-17', '2017-06-22', '2017-07-13', NULL, 21600, 1, 0),
(529, 47, 3, '2', 'LMM0216123', 'BMOU 964121-8', 2, 'MAGARI', '019', 6, 1, '2017-06-17', '2017-06-17', '2017-06-22', '2017-07-13', NULL, 21600, 1, 0),
(530, 49, 1, '23109', '1', NULL, 8, 'SCHWEIZ REEFER', '1', 3, 2, '2017-06-26', '2017-06-26', '2017-07-02', '2017-07-20', NULL, 19591.2, 1, 0),
(531, 49, 1, '23111', '1', NULL, 8, 'SCHWEIZ REEFER', '1', 3, 2, '2017-06-26', '2017-06-26', '2017-07-02', '2017-07-20', NULL, 19591.2, 1, 0),
(532, 49, 1, '23110', '1', NULL, 8, 'SCHWEIZ REEFER', '1', 3, 2, '2017-06-26', '2017-06-26', '2017-07-02', '2017-07-20', NULL, 19591.2, 1, 0),
(533, 49, 1, '23116', '1', NULL, 3, 'CALLAO EXPRESS', '1', 3, 2, '2017-06-26', '2017-06-26', '2017-07-04', '2017-07-25', NULL, 19591.2, 1, 0),
(534, 49, 1, '23115', '1', NULL, 3, 'CALLAO EXPRESS', '1', 3, 2, '2017-06-26', '2017-06-26', '2017-07-04', '2017-07-25', NULL, 19591.2, 1, 0),
(535, 49, 1, '23114', '1', NULL, 3, 'CALLAO EXPRESS', '1', 3, 2, '2017-06-26', '2017-06-26', '2017-07-04', '2017-07-25', NULL, 19591.2, 1, 0),
(536, 49, 1, '23113', '1', NULL, 3, 'CALLAO EXPRESS', '1', 3, 2, '2017-06-26', '2017-06-26', '2017-07-04', '2017-07-25', NULL, 19591.2, 1, 0),
(537, 49, 1, '23112', '1', NULL, 3, 'CALLAO EXPRESS', '1', 3, 2, '2017-06-26', '2017-06-26', '2017-07-04', '2017-07-25', NULL, 19591.2, 1, 0),
(538, 49, 1, '23117', '1', NULL, 5, 'SEALAND PHILADELPHIA', '1', 1, 2, '2017-06-26', '2017-06-26', '2017-06-30', '2017-07-23', NULL, 19591.2, 1, 0),
(539, 49, 1, '23119', '1', NULL, 5, 'SEALAND PHILADELPHIA', '1', 7, 2, '2017-06-26', '2017-06-26', '2017-06-30', '2017-08-03', NULL, 18900, 1, 0),
(540, 49, 1, '23118', '1', NULL, 5, 'SEALAND PHILADELPHIA', '1', 1, 2, '2017-06-26', '2017-06-26', '2017-06-30', '2017-07-23', NULL, 19591.2, 1, 0),
(541, 48, 2, '1', '98628647', 'HLBU 904294-8', 4, 'LISZT', '726', 3, 1, '2017-06-22', '2017-06-22', '2017-06-29', '2017-05-29', NULL, 19591.2, 1, 0),
(542, 48, 2, '2', '98628647', 'HLBU 904186-0', 4, 'LISZT', '729', 3, 1, '2017-06-22', '2017-06-22', '2017-06-29', '2017-05-29', NULL, 19591.2, 0, 0),
(543, 48, 2, '2', '98628647', 'HLBU 904186-0', 4, 'LISZT', '726', 3, 1, '2017-06-22', '2017-06-22', '2017-06-28', '2017-07-29', NULL, 19591.2, 1, 0),
(544, 48, 4, '2', '98653827', 'CPSU 511286-0', 4, 'LISZT', '726', 3, 2, '2017-06-23', '2017-06-20', '2017-06-28', '2017-07-28', NULL, 19591.2, 1, 0),
(545, 48, 4, '1', '98653827', 'LNXU 845859-8', 4, 'LISZT', '726', 3, 2, '2017-06-22', '2017-06-22', '2017-06-28', '2017-07-28', NULL, 19591.2, 1, 0),
(546, 48, 3, '2', 'B6JBK024927', 'DFIU 723082-1', 9, 'ALIOTH', '173', 4, 1, '2017-06-23', '2017-06-23', '2017-06-28', '2017-07-28', 0, 17414.4, 1, 0),
(547, 48, 3, '1', 'LMM0216476', 'CAIU 558762-8', 2, 'LISZT', '020', 6, 1, '2017-06-23', '2017-06-23', '2017-06-28', '2017-07-28', 0, 19591.2, 1, 0),
(548, 48, 5, '2', '7LIMAG1611', 'SUDU 810355-5', 3, 'LISZT', '726', 2, 2, '2017-06-25', '2017-06-25', '2017-06-28', '2017-07-28', NULL, 17414.4, 1, 0),
(549, 48, 5, '1', '7LIMAG1612', 'SUDU 803934-8', 3, 'LISZT', '726', 2, 2, '2017-06-24', '2017-06-24', '2017-06-28', '2017-07-28', NULL, 17414.4, 1, 0),
(550, 50, 1, '23147', '1', NULL, 8, 'ATLANTIC REEFER', '1', 3, 2, '2017-07-04', '2017-07-04', '2017-07-09', '2017-07-29', NULL, 19591.2, 1, 0),
(551, 50, 1, '23148', '1', NULL, 8, 'ATLANTIC REEFER', '1', 3, 2, '2017-07-04', '2017-07-04', '2017-07-09', '2017-07-29', NULL, 19591.2, 1, 0),
(552, 50, 1, '23149', '1', NULL, 8, 'ATLANTIC REEFER', '1', 3, 2, '2017-07-04', '2017-07-04', '2017-07-09', '2017-07-27', NULL, 19591.2, 1, 0),
(553, 50, 1, '23150', '1', NULL, 3, 'LISZT', '1', 3, 2, '2017-07-06', '2017-07-06', '2017-07-11', '2017-08-01', NULL, 19591.2, 1, 0),
(554, 50, 1, '23151', '1', NULL, 3, 'LISZT', '1', 3, 2, '2017-07-06', '2017-07-06', '2017-07-11', '2017-08-01', NULL, 19591.2, 1, 0),
(555, 50, 1, '23152', '1', NULL, 3, 'LISZT', '1', 3, 2, '2017-07-06', '2017-07-06', '2017-07-11', '2017-08-01', NULL, 19591.2, 1, 0),
(556, 50, 1, '23153', '1', NULL, 3, 'LISZT', '1', 3, 2, '2017-07-06', '2017-07-06', '2017-07-11', '2017-08-01', NULL, 19591.2, 1, 0),
(557, 50, 1, '23154', '1', NULL, 8, 'ATLANTIC REEFER', '1', 1, 2, '2017-07-04', '2017-07-04', '2017-07-09', '2017-07-27', NULL, 19591.2, 1, 0),
(558, 50, 1, '23155', '1', NULL, 8, 'ATLANTIC REEFER', '1', 1, 2, '2017-07-04', '2017-07-04', '2017-07-09', '2017-07-27', NULL, 19591.2, 1, 0),
(559, 50, 1, '23156', '1', NULL, 8, 'ATLANTIC REEFER', '1', 1, 2, '2017-07-06', '2017-07-06', '2017-07-09', '2017-07-27', NULL, 19591.2, 1, 0),
(560, 50, 1, '23157', '1', NULL, 5, 'n', '1', 7, 2, '2017-07-06', '2017-07-06', '2017-07-09', '2017-07-27', NULL, 18900, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenedor_valija`
--

CREATE TABLE `contenedor_valija` (
  `id` int(11) NOT NULL,
  `valija_id` int(11) NOT NULL,
  `contenedor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `color` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id`, `nombre`, `color`, `activo`) VALUES
(1, 'Pendiente', '#ff0013', 1),
(2, 'En Proceso', '#D35400', 1),
(3, 'Enviado', '#27AE60', 1),
(4, 'En Puerto Paita', '#2980B9', 1),
(5, 'En la nave', '#004040', 1),
(6, 'Llegó a destino', '#b516e9', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id`, `id_cliente`, `id_semana`, `nombre`, `fecha`, `hora`, `url`) VALUES
(2, 1, 47, '1_24_2017_(1)AGROFAIR01.pdf', '2017-06-22', '15:15:07', '1_24_2017_(1)AGROFAIR01.pdf'),
(3, 1, 47, '1_24_2017_(2)AGROFAIR2.pdf', '2017-06-22', '15:19:18', '1_24_2017_(2)AGROFAIR2.pdf'),
(4, 3, 47, '3_24_2017_(1)DOLE.pdf', '2017-06-22', '15:24:23', '3_24_2017_(1)DOLE.pdf'),
(5, 2, 47, '2_24_2017_(1)BIODYNAMISKA.pdf', '2017-06-22', '15:25:36', '2_24_2017_(1)BIODYNAMISKA.pdf'),
(6, 5, 47, '5_24_2017_(1)EQUIFRUIT.pdf', '2017-06-22', '15:26:28', '5_24_2017_(1)EQUIFRUIT.pdf'),
(7, 4, 47, '4_24_2017_(1)TRANSASTRA.pdf', '2017-06-22', '15:28:13', '4_24_2017_(1)TRANSASTRA.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `icono`
--

CREATE TABLE `icono` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linea_naviera`
--

CREATE TABLE `linea_naviera` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `sigla` varchar(5) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `linea_naviera`
--

INSERT INTO `linea_naviera` (`id`, `nombre`, `sigla`, `activo`) VALUES
(1, 'APL', 'APL', 1),
(2, 'CMA - CGM', 'CMA', 1),
(3, 'HAMBURG SUD - HSD', 'HSD', 1),
(4, 'HAPAG LLOYD - HLE', 'HLE', 1),
(5, 'MAERSK MSK', 'MSK', 1),
(6, 'MOL', 'MOL', 1),
(7, 'MSC', 'MSC', 1),
(8, 'SEATRADE - STR', 'STR', 1),
(9, 'DOLE', 'DOLE', 1),
(18, 'YOKOHAMA-JAPON', 'JAPON', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `nombre`, `activo`) VALUES
(14, 'Administrador', 1),
(15, 'Jefe Trazabilidad', 1),
(16, 'Cliente', 1),
(20, 'Gerencia', 1),
(21, 'Auxiliar Trazabilidad', 1),
(22, 'Tesorería', 0),
(23, 'Tesorería', 0),
(24, 'Operador', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_opcion`
--

CREATE TABLE `menu_opcion` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `opcion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_opcion`
--

INSERT INTO `menu_opcion` (`id`, `menu_id`, `opcion_id`) VALUES
(1, 14, 7),
(2, 14, 8),
(3, 14, 9),
(4, 14, 10),
(6, 14, 12),
(7, 15, 7),
(8, 15, 8),
(9, 15, 9),
(10, 15, 10),
(11, 15, 11),
(19, 14, 11),
(24, 20, 7),
(25, 20, 8),
(26, 20, 9),
(27, 20, 10),
(28, 20, 11),
(29, 16, 13),
(30, 21, 8),
(31, 21, 7),
(32, 21, 10),
(33, 21, 11),
(34, 22, 7),
(35, 24, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `id` int(11) NOT NULL,
  `id_contenedor` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `id_estado` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`id`, `id_contenedor`, `id_usuario`, `fecha`, `hora`, `id_estado`, `activo`) VALUES
(73, 26, 4, '2017-05-29', '10:15:54', 1, 0),
(74, 26, 4, '2017-05-29', '10:18:43', 2, 0),
(75, 26, 4, '2017-05-29', '10:19:24', 3, 1),
(76, 27, 4, '2017-05-29', '12:18:59', 1, 1),
(77, 28, 4, '2017-05-29', '12:33:50', 1, 0),
(78, 28, 4, '2017-05-29', '12:34:12', 5, 0),
(79, 29, 4, '2017-05-29', '12:42:51', 1, 0),
(80, 29, 4, '2017-05-29', '12:43:12', 5, 0),
(81, 30, 4, '2017-05-29', '12:49:33', 1, 0),
(82, 30, 4, '2017-05-29', '12:49:36', 5, 0),
(83, 31, 4, '2017-05-29', '12:52:50', 1, 0),
(84, 32, 4, '2017-05-29', '12:56:27', 1, 0),
(85, 32, 4, '2017-05-29', '12:56:36', 5, 0),
(86, 31, 4, '2017-05-29', '12:56:39', 5, 0),
(87, 33, 4, '2017-05-29', '15:11:35', 1, 0),
(88, 33, 4, '2017-05-29', '15:11:49', 5, 0),
(89, 34, 4, '2017-05-29', '15:17:08', 1, 0),
(90, 35, 4, '2017-05-29', '15:19:48', 1, 0),
(91, 35, 4, '2017-05-29', '15:20:59', 5, 0),
(92, 34, 4, '2017-05-29', '15:21:03', 5, 1),
(93, 36, 4, '2017-05-29', '16:13:40', 1, 0),
(94, 37, 4, '2017-05-29', '16:15:15', 1, 0),
(95, 38, 4, '2017-05-29', '16:17:18', 1, 0),
(96, 39, 4, '2017-05-29', '16:20:48', 1, 0),
(97, 41, 4, '2017-05-29', '16:24:37', 1, 0),
(98, 36, 4, '2017-05-29', '16:24:55', 4, 0),
(99, 37, 4, '2017-05-29', '16:24:57', 4, 0),
(100, 38, 4, '2017-05-29', '16:25:00', 4, 0),
(101, 39, 4, '2017-05-29', '16:25:03', 4, 0),
(102, 41, 4, '2017-05-29', '16:25:06', 4, 0),
(103, 42, 4, '2017-05-29', '16:27:25', 1, 0),
(104, 42, 4, '2017-05-29', '16:27:31', 4, 0),
(105, 43, 4, '2017-05-29', '16:29:35', 1, 0),
(106, 43, 4, '2017-05-29', '16:29:39', 4, 0),
(107, 44, 4, '2017-05-29', '16:31:34', 1, 0),
(108, 45, 4, '2017-05-29', '16:40:33', 1, 0),
(109, 46, 4, '2017-05-29', '16:42:35', 1, 0),
(110, 47, 4, '2017-05-29', '16:44:06', 1, 0),
(111, 48, 4, '2017-05-29', '16:48:31', 1, 0),
(112, 43, 2, '2017-05-29', '21:28:08', 2, 0),
(113, 43, 2, '2017-05-29', '21:28:21', 3, 0),
(114, 49, 4, '2017-05-30', '08:41:26', 1, 0),
(115, 50, 4, '2017-05-30', '08:45:52', 1, 0),
(116, 51, 4, '2017-05-30', '08:49:30', 1, 0),
(117, 52, 4, '2017-05-30', '08:52:21', 1, 0),
(118, 53, 4, '2017-05-30', '09:05:37', 1, 0),
(119, 54, 4, '2017-05-30', '09:08:10', 1, 0),
(120, 55, 4, '2017-05-30', '09:11:01', 1, 0),
(121, 56, 4, '2017-05-30', '09:14:17', 1, 0),
(122, 57, 4, '2017-05-30', '09:20:06', 1, 0),
(123, 58, 4, '2017-05-30', '09:23:09', 1, 0),
(124, 59, 4, '2017-05-30', '10:26:21', 1, 0),
(125, 60, 4, '2017-05-30', '10:29:37', 1, 0),
(126, 61, 4, '2017-05-30', '10:34:36', 1, 1),
(127, 62, 4, '2017-05-30', '10:34:36', 1, 1),
(128, 63, 4, '2017-05-30', '10:34:36', 1, 0),
(129, 64, 4, '2017-05-30', '10:34:36', 1, 1),
(130, 65, 4, '2017-05-30', '10:41:36', 1, 0),
(131, 66, 4, '2017-05-30', '10:52:17', 1, 0),
(132, 66, 4, '2017-05-30', '10:53:02', 5, 0),
(133, 67, 4, '2017-05-30', '11:18:40', 1, 0),
(134, 67, 4, '2017-05-30', '11:18:47', 5, 0),
(135, 68, 4, '2017-05-30', '11:21:45', 1, 0),
(136, 68, 4, '2017-05-30', '11:21:53', 5, 0),
(137, 69, 4, '2017-05-30', '11:25:41', 1, 0),
(138, 69, 4, '2017-05-30', '11:25:44', 5, 0),
(139, 70, 4, '2017-05-30', '11:28:18', 1, 0),
(140, 70, 4, '2017-05-30', '11:28:21', 5, 0),
(141, 71, 4, '2017-05-30', '11:30:56', 1, 0),
(142, 71, 4, '2017-05-30', '11:31:00', 6, 0),
(143, 71, 4, '2017-05-30', '11:31:02', 5, 1),
(144, 72, 4, '2017-05-30', '11:33:42', 1, 0),
(145, 72, 4, '2017-05-30', '11:33:45', 5, 0),
(146, 73, 4, '2017-05-30', '11:40:12', 1, 0),
(147, 73, 4, '2017-05-30', '11:40:15', 5, 0),
(148, 74, 4, '2017-05-30', '11:42:30', 1, 0),
(149, 74, 4, '2017-05-30', '11:42:33', 5, 0),
(150, 75, 4, '2017-05-30', '11:45:39', 1, 0),
(151, 75, 4, '2017-05-30', '11:45:42', 5, 0),
(152, 76, 4, '2017-05-30', '11:48:11', 1, 0),
(153, 76, 4, '2017-05-30', '11:48:15', 5, 0),
(154, 77, 4, '2017-05-30', '11:51:08', 1, 0),
(155, 77, 4, '2017-05-30', '11:51:12', 5, 0),
(156, 78, 4, '2017-05-30', '11:54:13', 1, 0),
(157, 78, 4, '2017-05-30', '11:54:17', 5, 0),
(158, 79, 4, '2017-05-30', '11:56:22', 1, 0),
(159, 79, 4, '2017-05-30', '11:56:25', 5, 0),
(160, 80, 4, '2017-05-30', '11:59:41', 1, 0),
(161, 80, 4, '2017-05-30', '11:59:45', 5, 0),
(162, 82, 4, '2017-05-30', '12:03:27', 1, 0),
(163, 82, 4, '2017-05-30', '12:03:30', 5, 0),
(164, 83, 4, '2017-05-30', '12:07:06', 1, 0),
(165, 83, 4, '2017-05-30', '12:07:09', 6, 0),
(166, 83, 4, '2017-05-30', '12:07:13', 5, 1),
(167, 84, 4, '2017-05-30', '12:11:27', 1, 0),
(168, 84, 4, '2017-05-30', '12:11:30', 5, 0),
(169, 85, 4, '2017-05-30', '12:15:02', 1, 0),
(170, 85, 4, '2017-05-30', '12:15:05', 5, 0),
(171, 86, 4, '2017-05-30', '12:22:31', 1, 0),
(172, 86, 4, '2017-05-30', '12:22:39', 5, 0),
(173, 87, 4, '2017-05-30', '12:25:50', 1, 0),
(174, 87, 4, '2017-05-30', '12:25:53', 5, 0),
(175, 88, 4, '2017-05-30', '12:27:40', 1, 0),
(176, 88, 4, '2017-05-30', '12:33:07', 5, 0),
(177, 89, 4, '2017-05-30', '12:36:13', 1, 0),
(178, 89, 4, '2017-05-30', '12:36:17', 5, 0),
(179, 90, 4, '2017-05-30', '12:40:49', 1, 0),
(180, 90, 4, '2017-05-30', '12:40:54', 5, 0),
(181, 91, 4, '2017-05-31', '07:21:05', 1, 0),
(182, 92, 4, '2017-05-31', '07:23:14', 1, 0),
(183, 93, 4, '2017-05-31', '07:23:26', 1, 0),
(184, 93, 4, '2017-05-31', '07:27:22', 5, 0),
(185, 92, 4, '2017-05-31', '07:27:26', 5, 0),
(186, 91, 4, '2017-05-31', '07:27:29', 5, 1),
(187, 94, 4, '2017-05-31', '07:30:45', 1, 0),
(188, 94, 4, '2017-05-31', '07:30:49', 5, 0),
(189, 95, 4, '2017-05-31', '07:35:05', 1, 0),
(190, 95, 4, '2017-05-31', '07:35:09', 5, 0),
(191, 96, 4, '2017-05-31', '07:38:46', 1, 0),
(192, 96, 4, '2017-05-31', '07:38:50', 5, 0),
(193, 97, 4, '2017-05-31', '07:40:36', 1, 0),
(194, 97, 4, '2017-05-31', '07:40:48', 5, 0),
(195, 98, 4, '2017-05-31', '07:43:34', 1, 0),
(196, 98, 4, '2017-05-31', '07:43:37', 5, 0),
(197, 99, 4, '2017-05-31', '07:46:13', 1, 0),
(198, 100, 4, '2017-05-31', '07:52:45', 1, 0),
(199, 100, 4, '2017-05-31', '07:52:52', 5, 0),
(200, 99, 4, '2017-05-31', '07:52:54', 5, 0),
(201, 101, 4, '2017-05-31', '07:59:23', 1, 0),
(202, 101, 4, '2017-05-31', '08:03:58', 5, 0),
(203, 102, 4, '2017-05-31', '08:07:02', 1, 0),
(204, 102, 4, '2017-05-31', '08:07:06', 5, 0),
(205, 103, 4, '2017-05-31', '08:08:47', 1, 0),
(206, 103, 4, '2017-05-31', '08:08:58', 6, 0),
(207, 103, 4, '2017-05-31', '08:08:59', 5, 1),
(208, 104, 4, '2017-05-31', '08:12:56', 1, 0),
(209, 105, 4, '2017-05-31', '08:15:36', 1, 0),
(210, 106, 4, '2017-05-31', '08:17:15', 1, 0),
(211, 104, 4, '2017-05-31', '08:17:19', 5, 0),
(212, 105, 4, '2017-05-31', '08:17:22', 5, 0),
(213, 106, 4, '2017-05-31', '08:17:24', 5, 0),
(214, 107, 4, '2017-05-31', '08:29:44', 1, 0),
(215, 107, 4, '2017-05-31', '08:34:54', 6, 1),
(216, 108, 4, '2017-05-31', '08:37:11', 1, 0),
(217, 108, 4, '2017-05-31', '08:37:30', 6, 1),
(218, 109, 4, '2017-05-31', '08:40:06', 1, 0),
(219, 109, 4, '2017-05-31', '08:40:10', 5, 0),
(220, 109, 4, '2017-05-31', '08:41:08', 6, 1),
(221, 110, 4, '2017-05-31', '08:44:18', 1, 0),
(222, 110, 4, '2017-05-31', '08:44:41', 5, 0),
(223, 110, 4, '2017-05-31', '08:44:42', 6, 1),
(224, 111, 4, '2017-05-31', '08:47:25', 1, 0),
(225, 112, 4, '2017-05-31', '08:47:43', 1, 0),
(226, 111, 4, '2017-05-31', '08:48:08', 6, 1),
(227, 112, 4, '2017-05-31', '08:48:10', 6, 1),
(228, 113, 4, '2017-05-31', '08:53:29', 1, 0),
(229, 113, 4, '2017-05-31', '08:53:32', 6, 1),
(230, 114, 4, '2017-05-31', '08:57:09', 1, 0),
(231, 114, 4, '2017-05-31', '08:57:12', 6, 1),
(232, 115, 4, '2017-05-31', '09:00:28', 1, 0),
(233, 116, 4, '2017-05-31', '09:04:00', 1, 0),
(234, 116, 4, '2017-05-31', '09:04:29', 6, 1),
(235, 115, 4, '2017-05-31', '09:04:31', 6, 1),
(236, 117, 4, '2017-05-31', '09:07:43', 1, 0),
(237, 117, 4, '2017-05-31', '09:07:47', 6, 1),
(238, 118, 4, '2017-05-31', '09:10:13', 1, 0),
(239, 118, 4, '2017-05-31', '09:10:17', 6, 1),
(240, 119, 4, '2017-05-31', '09:12:45', 1, 0),
(241, 119, 4, '2017-05-31', '09:12:52', 6, 1),
(242, 120, 4, '2017-05-31', '09:17:36', 1, 0),
(243, 120, 4, '2017-05-31', '09:17:43', 6, 1),
(244, 121, 4, '2017-05-31', '09:20:03', 1, 0),
(245, 121, 4, '2017-05-31', '09:20:07', 6, 1),
(246, 122, 4, '2017-05-31', '09:21:47', 1, 0),
(247, 122, 4, '2017-05-31', '09:21:51', 6, 1),
(248, 123, 4, '2017-05-31', '09:24:13', 1, 0),
(249, 123, 4, '2017-05-31', '09:24:25', 6, 1),
(250, 124, 4, '2017-05-31', '09:37:03', 1, 0),
(251, 124, 4, '2017-05-31', '09:37:52', 6, 1),
(252, 125, 4, '2017-05-31', '09:40:10', 1, 0),
(253, 125, 4, '2017-05-31', '09:40:17', 6, 1),
(254, 126, 4, '2017-05-31', '09:42:00', 1, 0),
(255, 127, 4, '2017-05-31', '10:11:58', 1, 0),
(256, 127, 4, '2017-05-31', '10:12:03', 6, 1),
(257, 128, 4, '2017-05-31', '10:14:11', 1, 0),
(258, 128, 4, '2017-05-31', '10:14:17', 6, 1),
(259, 129, 4, '2017-05-31', '10:16:45', 1, 0),
(260, 129, 4, '2017-05-31', '10:16:52', 6, 1),
(261, 130, 4, '2017-05-31', '10:18:53', 1, 0),
(262, 130, 4, '2017-05-31', '10:18:57', 6, 1),
(263, 131, 4, '2017-05-31', '10:23:04', 1, 0),
(264, 131, 4, '2017-05-31', '10:23:07', 6, 1),
(265, 132, 4, '2017-05-31', '10:47:00', 1, 0),
(266, 132, 4, '2017-05-31', '10:47:13', 6, 1),
(267, 133, 4, '2017-05-31', '10:49:54', 1, 0),
(268, 133, 4, '2017-05-31', '10:50:00', 6, 1),
(269, 134, 4, '2017-05-31', '10:52:54', 1, 0),
(270, 134, 4, '2017-05-31', '10:52:57', 6, 1),
(271, 135, 4, '2017-05-31', '10:58:46', 1, 0),
(272, 135, 4, '2017-05-31', '10:58:49', 6, 1),
(273, 136, 4, '2017-05-31', '11:02:55', 1, 0),
(274, 136, 4, '2017-05-31', '11:03:02', 5, 0),
(275, 136, 4, '2017-05-31', '11:03:05', 6, 1),
(276, 137, 4, '2017-05-31', '11:04:54', 1, 0),
(277, 137, 4, '2017-05-31', '11:04:58', 6, 1),
(278, 138, 4, '2017-05-31', '11:07:42', 1, 1),
(279, 139, 4, '2017-05-31', '11:07:42', 1, 0),
(280, 139, 4, '2017-05-31', '11:08:01', 6, 1),
(281, 140, 4, '2017-05-31', '11:09:29', 1, 0),
(282, 140, 4, '2017-05-31', '11:09:32', 6, 1),
(283, 141, 4, '2017-05-31', '11:11:19', 1, 0),
(284, 141, 4, '2017-05-31', '11:11:22', 5, 0),
(285, 141, 4, '2017-05-31', '11:11:24', 6, 1),
(286, 142, 4, '2017-05-31', '11:12:36', 1, 0),
(287, 142, 4, '2017-05-31', '11:12:45', 6, 1),
(288, 143, 4, '2017-05-31', '11:14:47', 1, 0),
(289, 143, 4, '2017-05-31', '11:14:50', 6, 1),
(290, 144, 4, '2017-05-31', '11:17:39', 1, 0),
(291, 144, 4, '2017-05-31', '11:17:48', 6, 1),
(292, 145, 4, '2017-05-31', '11:19:54', 1, 0),
(293, 145, 4, '2017-05-31', '11:19:58', 6, 1),
(294, 146, 4, '2017-05-31', '11:21:41', 1, 0),
(295, 146, 4, '2017-05-31', '11:21:49', 6, 1),
(296, 147, 4, '2017-05-31', '11:23:58', 1, 0),
(297, 147, 4, '2017-05-31', '11:24:07', 5, 0),
(298, 147, 4, '2017-05-31', '11:24:11', 6, 1),
(299, 148, 4, '2017-05-31', '11:40:50', 1, 0),
(300, 148, 4, '2017-05-31', '11:40:53', 6, 1),
(301, 149, 4, '2017-05-31', '11:43:54', 1, 0),
(302, 149, 4, '2017-05-31', '11:43:57', 6, 1),
(303, 150, 4, '2017-05-31', '11:46:05', 1, 0),
(304, 150, 4, '2017-05-31', '11:46:08', 6, 1),
(305, 151, 4, '2017-05-31', '11:51:48', 1, 0),
(306, 151, 4, '2017-05-31', '11:51:51', 6, 1),
(307, 152, 4, '2017-05-31', '12:01:02', 1, 0),
(308, 152, 4, '2017-05-31', '12:01:07', 6, 1),
(309, 153, 4, '2017-05-31', '12:03:40', 1, 0),
(310, 153, 4, '2017-05-31', '12:03:45', 6, 1),
(311, 154, 4, '2017-05-31', '12:06:33', 1, 0),
(312, 154, 4, '2017-05-31', '12:06:37', 6, 1),
(313, 155, 4, '2017-05-31', '12:08:51', 1, 0),
(314, 155, 4, '2017-05-31', '12:08:56', 6, 1),
(315, 156, 4, '2017-05-31', '12:11:04', 1, 0),
(316, 156, 4, '2017-05-31', '12:11:10', 6, 1),
(317, 157, 4, '2017-05-31', '12:12:52', 1, 0),
(318, 157, 4, '2017-05-31', '12:12:56', 6, 1),
(319, 158, 4, '2017-05-31', '12:15:26', 1, 0),
(320, 158, 4, '2017-05-31', '12:15:41', 6, 1),
(321, 159, 4, '2017-05-31', '12:23:27', 1, 0),
(322, 159, 4, '2017-05-31', '12:23:34', 5, 0),
(323, 159, 4, '2017-05-31', '12:23:35', 6, 1),
(324, 160, 4, '2017-05-31', '12:25:09', 1, 0),
(325, 160, 4, '2017-05-31', '12:25:17', 6, 1),
(326, 161, 4, '2017-05-31', '12:27:09', 1, 0),
(327, 161, 4, '2017-05-31', '12:27:14', 6, 1),
(328, 162, 4, '2017-05-31', '12:28:51', 1, 0),
(329, 162, 4, '2017-05-31', '12:28:54', 6, 1),
(330, 163, 4, '2017-05-31', '12:30:12', 1, 0),
(331, 163, 4, '2017-05-31', '12:30:16', 6, 1),
(332, 164, 4, '2017-05-31', '12:32:17', 1, 0),
(333, 164, 4, '2017-05-31', '12:32:20', 6, 1),
(334, 165, 4, '2017-05-31', '12:33:55', 1, 0),
(335, 165, 4, '2017-05-31', '12:33:57', 6, 1),
(336, 166, 4, '2017-05-31', '12:40:02', 1, 0),
(337, 166, 4, '2017-05-31', '12:40:04', 6, 1),
(338, 167, 4, '2017-05-31', '12:41:14', 1, 0),
(339, 167, 4, '2017-05-31', '12:41:22', 6, 1),
(340, 168, 4, '2017-06-01', '07:31:12', 1, 0),
(341, 168, 4, '2017-06-01', '07:32:21', 6, 1),
(342, 169, 4, '2017-06-01', '07:34:06', 1, 0),
(343, 170, 4, '2017-06-01', '07:36:09', 1, 0),
(344, 169, 4, '2017-06-01', '07:36:13', 6, 1),
(345, 171, 4, '2017-06-01', '07:38:16', 1, 0),
(346, 170, 4, '2017-06-01', '07:38:25', 6, 1),
(347, 171, 4, '2017-06-01', '07:38:28', 6, 1),
(348, 172, 4, '2017-06-01', '07:40:08', 1, 0),
(349, 172, 4, '2017-06-01', '07:40:27', 6, 1),
(350, 173, 4, '2017-06-01', '07:41:56', 1, 0),
(351, 173, 4, '2017-06-01', '07:41:59', 6, 1),
(352, 174, 4, '2017-06-01', '07:43:43', 1, 1),
(353, 175, 4, '2017-06-01', '07:43:51', 1, 0),
(354, 175, 4, '2017-06-01', '07:45:00', 6, 1),
(355, 176, 4, '2017-06-01', '07:49:08', 1, 0),
(356, 176, 4, '2017-06-01', '07:51:07', 6, 1),
(357, 177, 4, '2017-06-01', '07:54:31', 1, 0),
(358, 177, 4, '2017-06-01', '07:55:10', 6, 1),
(359, 178, 4, '2017-06-01', '07:57:14', 1, 0),
(360, 178, 4, '2017-06-01', '07:58:38', 6, 1),
(361, 179, 4, '2017-06-01', '08:00:20', 1, 0),
(362, 179, 4, '2017-06-01', '08:00:55', 6, 1),
(363, 180, 4, '2017-06-01', '08:09:46', 1, 1),
(364, 181, 4, '2017-06-01', '08:12:51', 1, 0),
(365, 181, 4, '2017-06-01', '08:13:10', 6, 1),
(366, 182, 4, '2017-06-01', '08:13:17', 1, 0),
(367, 183, 4, '2017-06-01', '08:15:20', 1, 0),
(368, 184, 4, '2017-06-01', '08:16:33', 1, 0),
(369, 182, 4, '2017-06-01', '08:19:32', 6, 1),
(370, 184, 4, '2017-06-01', '08:19:58', 6, 1),
(371, 183, 4, '2017-06-01', '08:20:03', 6, 1),
(372, 185, 4, '2017-06-01', '08:27:07', 1, 0),
(373, 185, 4, '2017-06-01', '08:27:19', 6, 1),
(374, 186, 4, '2017-06-01', '08:29:17', 1, 0),
(375, 186, 4, '2017-06-01', '08:29:31', 6, 1),
(376, 187, 4, '2017-06-01', '08:31:21', 1, 0),
(377, 187, 4, '2017-06-01', '08:31:42', 6, 1),
(378, 188, 4, '2017-06-01', '08:34:36', 1, 0),
(379, 189, 4, '2017-06-01', '08:34:51', 1, 1),
(380, 188, 4, '2017-06-01', '08:35:27', 6, 1),
(381, 190, 4, '2017-06-01', '08:51:19', 1, 0),
(382, 190, 4, '2017-06-01', '08:51:23', 6, 1),
(383, 191, 4, '2017-06-01', '08:53:23', 1, 0),
(384, 191, 4, '2017-06-01', '08:53:40', 6, 1),
(385, 192, 4, '2017-06-01', '08:55:08', 1, 0),
(386, 192, 4, '2017-06-01', '08:55:13', 6, 1),
(387, 193, 4, '2017-06-01', '08:59:29', 1, 0),
(388, 193, 4, '2017-06-01', '08:59:45', 6, 1),
(389, 194, 4, '2017-06-01', '09:01:32', 1, 0),
(390, 194, 4, '2017-06-01', '09:01:37', 6, 1),
(391, 195, 4, '2017-06-01', '09:04:13', 1, 0),
(392, 196, 4, '2017-06-01', '09:04:33', 1, 0),
(393, 195, 4, '2017-06-01', '09:04:55', 6, 1),
(394, 196, 4, '2017-06-01', '09:06:37', 6, 1),
(395, 197, 4, '2017-06-01', '09:10:29', 1, 0),
(396, 197, 4, '2017-06-01', '09:11:49', 6, 1),
(397, 198, 4, '2017-06-01', '09:13:03', 1, 0),
(398, 198, 4, '2017-06-01', '09:13:48', 6, 1),
(399, 199, 4, '2017-06-01', '09:16:52', 1, 0),
(400, 199, 4, '2017-06-01', '09:17:08', 6, 1),
(401, 200, 4, '2017-06-01', '09:18:41', 1, 0),
(402, 200, 4, '2017-06-01', '09:18:48', 6, 1),
(403, 201, 4, '2017-06-01', '09:20:57', 1, 0),
(404, 202, 4, '2017-06-01', '09:22:09', 1, 0),
(405, 201, 4, '2017-06-01', '09:22:14', 6, 1),
(406, 203, 4, '2017-06-01', '09:24:00', 1, 0),
(407, 202, 4, '2017-06-01', '09:24:09', 6, 1),
(408, 203, 4, '2017-06-01', '09:24:13', 6, 1),
(409, 204, 4, '2017-06-01', '09:25:36', 1, 0),
(410, 205, 4, '2017-06-01', '09:27:54', 1, 0),
(411, 206, 4, '2017-06-01', '09:27:55', 1, 0),
(412, 204, 4, '2017-06-01', '09:28:03', 6, 1),
(413, 205, 4, '2017-06-01', '09:28:16', 6, 1),
(414, 206, 4, '2017-06-01', '09:30:40', 6, 1),
(415, 207, 4, '2017-06-01', '09:33:09', 1, 0),
(416, 207, 4, '2017-06-01', '09:33:20', 6, 1),
(417, 208, 4, '2017-06-01', '09:36:51', 1, 0),
(418, 209, 4, '2017-06-01', '09:36:54', 1, 0),
(419, 208, 4, '2017-06-01', '09:38:30', 6, 1),
(420, 209, 4, '2017-06-01', '09:38:36', 6, 1),
(421, 210, 4, '2017-06-01', '09:46:43', 1, 0),
(422, 210, 4, '2017-06-01', '09:47:01', 6, 1),
(423, 211, 4, '2017-06-01', '09:53:28', 1, 0),
(424, 211, 4, '2017-06-01', '09:53:49', 6, 1),
(425, 212, 4, '2017-06-01', '09:55:19', 1, 0),
(426, 213, 4, '2017-06-01', '10:03:32', 1, 0),
(427, 212, 4, '2017-06-01', '10:03:51', 6, 1),
(428, 213, 4, '2017-06-01', '10:07:28', 6, 1),
(429, 214, 4, '2017-06-01', '10:10:28', 1, 0),
(430, 214, 4, '2017-06-01', '10:10:32', 6, 1),
(431, 215, 4, '2017-06-01', '10:13:19', 1, 0),
(432, 215, 4, '2017-06-01', '10:13:31', 6, 1),
(433, 216, 4, '2017-06-01', '10:16:00', 1, 0),
(434, 216, 4, '2017-06-01', '10:18:48', 6, 1),
(435, 217, 4, '2017-06-01', '10:23:26', 1, 0),
(436, 217, 4, '2017-06-01', '10:23:30', 6, 1),
(437, 218, 4, '2017-06-01', '10:28:27', 1, 0),
(438, 218, 4, '2017-06-01', '10:28:34', 6, 1),
(439, 219, 4, '2017-06-01', '10:39:09', 1, 0),
(440, 220, 4, '2017-06-01', '10:40:16', 1, 0),
(441, 220, 4, '2017-06-01', '10:40:26', 6, 1),
(442, 219, 4, '2017-06-01', '10:40:29', 6, 1),
(443, 221, 4, '2017-06-01', '10:43:15', 1, 0),
(444, 222, 4, '2017-06-01', '10:44:52', 1, 0),
(445, 222, 4, '2017-06-01', '10:45:17', 6, 1),
(446, 221, 4, '2017-06-01', '10:45:20', 6, 1),
(447, 223, 4, '2017-06-01', '10:48:59', 1, 0),
(448, 224, 4, '2017-06-01', '10:50:51', 1, 0),
(449, 223, 4, '2017-06-01', '10:50:55', 6, 1),
(450, 225, 4, '2017-06-01', '10:52:32', 1, 0),
(451, 225, 4, '2017-06-01', '10:52:52', 6, 1),
(452, 224, 4, '2017-06-01', '10:52:53', 6, 1),
(453, 226, 4, '2017-06-01', '11:06:27', 1, 0),
(454, 226, 4, '2017-06-01', '11:16:51', 6, 1),
(455, 228, 4, '2017-06-01', '11:20:01', 1, 0),
(456, 228, 4, '2017-06-01', '11:20:13', 5, 0),
(457, 228, 4, '2017-06-01', '11:27:45', 6, 1),
(458, 229, 4, '2017-06-01', '11:30:21', 1, 0),
(459, 229, 4, '2017-06-01', '11:30:43', 6, 1),
(460, 230, 4, '2017-06-01', '11:33:22', 1, 0),
(461, 231, 4, '2017-06-01', '11:35:39', 1, 0),
(462, 230, 4, '2017-06-01', '11:36:17', 6, 1),
(463, 231, 4, '2017-06-01', '11:36:23', 6, 1),
(464, 232, 4, '2017-06-01', '11:38:45', 1, 0),
(465, 232, 4, '2017-06-01', '11:39:10', 6, 1),
(466, 233, 4, '2017-06-01', '11:50:02', 1, 0),
(467, 234, 4, '2017-06-01', '11:52:12', 1, 0),
(468, 233, 4, '2017-06-01', '11:52:20', 6, 1),
(469, 234, 4, '2017-06-01', '11:52:22', 6, 1),
(470, 235, 4, '2017-06-01', '11:58:10', 1, 0),
(471, 235, 4, '2017-06-01', '11:58:17', 6, 1),
(472, 236, 4, '2017-06-01', '12:04:14', 1, 0),
(473, 236, 4, '2017-06-01', '12:04:24', 6, 1),
(474, 237, 4, '2017-06-01', '12:06:21', 1, 0),
(475, 237, 4, '2017-06-01', '12:06:29', 6, 1),
(476, 238, 4, '2017-06-01', '12:09:36', 1, 0),
(477, 238, 4, '2017-06-01', '12:09:43', 6, 1),
(478, 239, 4, '2017-06-01', '12:13:26', 1, 0),
(479, 239, 4, '2017-06-01', '12:13:32', 6, 1),
(480, 240, 4, '2017-06-01', '12:16:28', 1, 0),
(481, 241, 4, '2017-06-01', '12:18:34', 1, 0),
(482, 241, 4, '2017-06-01', '12:18:58', 6, 1),
(483, 240, 4, '2017-06-01', '12:19:00', 6, 1),
(484, 242, 4, '2017-06-01', '12:22:59', 1, 0),
(485, 243, 4, '2017-06-01', '12:23:01', 1, 0),
(486, 242, 4, '2017-06-01', '12:23:53', 6, 1),
(487, 243, 4, '2017-06-01', '12:23:56', 6, 1),
(488, 244, 4, '2017-06-01', '12:26:02', 1, 0),
(489, 245, 4, '2017-06-01', '12:26:05', 1, 0),
(490, 245, 4, '2017-06-01', '12:26:39', 6, 1),
(491, 244, 4, '2017-06-01', '12:26:41', 6, 1),
(492, 246, 4, '2017-06-02', '08:20:54', 1, 0),
(493, 246, 4, '2017-06-02', '08:21:58', 6, 1),
(494, 247, 4, '2017-06-02', '08:31:13', 1, 0),
(495, 248, 4, '2017-06-02', '08:31:13', 1, 0),
(496, 248, 4, '2017-06-02', '08:32:18', 6, 1),
(497, 247, 4, '2017-06-02', '08:32:20', 6, 1),
(498, 249, 4, '2017-06-02', '08:33:50', 1, 0),
(499, 249, 4, '2017-06-02', '08:34:15', 6, 1),
(500, 250, 4, '2017-06-02', '08:35:54', 1, 0),
(501, 251, 4, '2017-06-02', '08:35:55', 1, 0),
(502, 252, 4, '2017-06-02', '08:35:57', 1, 0),
(503, 252, 4, '2017-06-02', '08:42:20', 6, 1),
(504, 251, 4, '2017-06-02', '08:44:31', 6, 1),
(505, 250, 4, '2017-06-02', '08:44:33', 6, 1),
(506, 253, 4, '2017-06-02', '08:51:02', 1, 0),
(507, 254, 4, '2017-06-02', '08:51:02', 1, 0),
(508, 255, 4, '2017-06-02', '08:51:02', 1, 0),
(509, 256, 4, '2017-06-02', '08:51:02', 1, 0),
(510, 257, 4, '2017-06-02', '08:51:03', 1, 0),
(511, 257, 4, '2017-06-02', '09:07:24', 6, 1),
(512, 256, 4, '2017-06-02', '09:07:26', 6, 1),
(513, 255, 4, '2017-06-02', '09:08:24', 6, 1),
(514, 254, 4, '2017-06-02', '09:20:37', 6, 1),
(515, 253, 4, '2017-06-02', '09:20:48', 6, 1),
(516, 258, 4, '2017-06-02', '09:24:46', 1, 0),
(517, 259, 4, '2017-06-02', '09:24:48', 1, 0),
(518, 260, 4, '2017-06-02', '09:34:13', 1, 0),
(519, 260, 4, '2017-06-02', '09:48:29', 6, 1),
(520, 259, 4, '2017-06-02', '09:48:59', 6, 1),
(521, 258, 4, '2017-06-02', '09:49:01', 6, 1),
(522, 261, 4, '2017-06-02', '09:50:38', 1, 0),
(523, 262, 4, '2017-06-02', '09:50:38', 1, 0),
(524, 262, 4, '2017-06-02', '09:51:31', 6, 1),
(525, 261, 4, '2017-06-02', '09:52:35', 6, 1),
(526, 263, 4, '2017-06-02', '10:01:55', 1, 0),
(527, 264, 4, '2017-06-02', '10:01:55', 1, 0),
(528, 265, 4, '2017-06-02', '10:01:55', 1, 0),
(529, 265, 4, '2017-06-02', '10:02:49', 6, 1),
(530, 263, 4, '2017-06-02', '10:02:55', 6, 1),
(531, 264, 4, '2017-06-02', '10:03:11', 6, 1),
(532, 266, 4, '2017-06-02', '10:05:08', 1, 0),
(533, 267, 4, '2017-06-02', '10:05:15', 1, 0),
(534, 268, 4, '2017-06-02', '10:05:15', 1, 0),
(535, 268, 4, '2017-06-02', '10:06:06', 6, 1),
(536, 267, 4, '2017-06-02', '10:07:12', 6, 1),
(537, 266, 4, '2017-06-02', '10:07:14', 6, 1),
(538, 269, 4, '2017-06-02', '10:12:19', 1, 0),
(539, 270, 4, '2017-06-02', '10:12:19', 1, 0),
(540, 271, 4, '2017-06-02', '10:12:20', 1, 0),
(541, 272, 4, '2017-06-02', '10:12:20', 1, 0),
(542, 269, 4, '2017-06-02', '10:13:12', 6, 1),
(543, 270, 4, '2017-06-02', '10:14:44', 6, 1),
(544, 271, 4, '2017-06-02', '10:16:46', 6, 1),
(545, 272, 4, '2017-06-02', '10:16:48', 6, 1),
(546, 273, 4, '2017-06-02', '10:20:09', 1, 0),
(547, 273, 4, '2017-06-02', '10:20:24', 6, 1),
(548, 274, 4, '2017-06-02', '10:22:30', 1, 0),
(549, 275, 4, '2017-06-02', '10:22:30', 1, 0),
(550, 275, 4, '2017-06-02', '10:26:54', 6, 1),
(551, 274, 4, '2017-06-02', '10:26:56', 6, 1),
(552, 276, 4, '2017-06-02', '10:29:05', 1, 0),
(553, 277, 4, '2017-06-02', '10:29:05', 1, 0),
(554, 277, 4, '2017-06-02', '10:33:03', 6, 1),
(555, 276, 4, '2017-06-02', '10:33:08', 6, 1),
(556, 278, 4, '2017-06-02', '10:38:28', 1, 0),
(557, 279, 4, '2017-06-02', '10:38:29', 1, 0),
(558, 278, 4, '2017-06-02', '10:39:51', 6, 1),
(559, 279, 4, '2017-06-02', '10:40:41', 6, 1),
(560, 280, 4, '2017-06-02', '10:42:46', 1, 0),
(561, 281, 4, '2017-06-02', '10:44:33', 1, 0),
(562, 282, 4, '2017-06-02', '10:44:34', 1, 0),
(563, 280, 4, '2017-06-02', '10:44:50', 6, 1),
(564, 282, 4, '2017-06-02', '10:46:58', 6, 1),
(565, 281, 4, '2017-06-02', '11:09:44', 6, 1),
(566, 283, 4, '2017-06-02', '11:44:33', 1, 0),
(567, 284, 4, '2017-06-02', '11:44:36', 1, 0),
(568, 285, 4, '2017-06-02', '11:44:37', 1, 0),
(569, 286, 4, '2017-06-02', '11:44:37', 1, 0),
(570, 286, 4, '2017-06-02', '11:50:07', 6, 1),
(571, 285, 4, '2017-06-02', '11:50:07', 6, 1),
(572, 284, 4, '2017-06-02', '11:50:10', 6, 1),
(573, 283, 4, '2017-06-02', '11:50:20', 5, 0),
(574, 283, 4, '2017-06-02', '11:50:24', 6, 1),
(575, 287, 4, '2017-06-02', '11:57:29', 1, 0),
(576, 288, 4, '2017-06-02', '11:57:29', 1, 0),
(577, 288, 4, '2017-06-02', '11:57:56', 6, 1),
(578, 287, 4, '2017-06-02', '11:57:58', 6, 1),
(579, 289, 4, '2017-06-02', '12:04:47', 1, 0),
(580, 290, 4, '2017-06-02', '12:04:48', 1, 0),
(581, 291, 4, '2017-06-02', '12:04:48', 1, 0),
(582, 291, 4, '2017-06-02', '12:08:26', 6, 1),
(583, 290, 4, '2017-06-02', '12:08:26', 6, 1),
(584, 289, 4, '2017-06-02', '12:08:26', 6, 1),
(585, 292, 4, '2017-06-02', '12:10:24', 1, 0),
(586, 293, 4, '2017-06-02', '12:10:26', 1, 0),
(587, 292, 4, '2017-06-02', '12:12:40', 6, 1),
(588, 293, 4, '2017-06-02', '12:12:42', 6, 1),
(589, 294, 4, '2017-06-05', '07:28:26', 1, 0),
(590, 295, 4, '2017-06-05', '07:28:26', 1, 0),
(591, 296, 4, '2017-06-05', '07:28:26', 1, 0),
(592, 297, 4, '2017-06-05', '07:28:27', 1, 0),
(593, 297, 4, '2017-06-05', '07:31:29', 6, 1),
(594, 296, 4, '2017-06-05', '07:31:32', 6, 1),
(595, 294, 4, '2017-06-05', '07:31:36', 6, 1),
(596, 295, 4, '2017-06-05', '07:33:25', 6, 1),
(597, 298, 4, '2017-06-05', '07:59:51', 1, 0),
(598, 298, 4, '2017-06-05', '08:00:45', 6, 1),
(599, 299, 4, '2017-06-05', '08:02:28', 1, 0),
(600, 299, 4, '2017-06-05', '08:06:51', 6, 1),
(601, 300, 4, '2017-06-05', '08:12:20', 1, 0),
(602, 301, 4, '2017-06-05', '08:12:21', 1, 0),
(603, 300, 4, '2017-06-05', '08:17:05', 6, 1),
(604, 301, 4, '2017-06-05', '08:17:07', 6, 1),
(605, 302, 4, '2017-06-05', '08:19:44', 1, 0),
(606, 303, 4, '2017-06-05', '08:19:44', 1, 0),
(607, 302, 4, '2017-06-05', '08:26:23', 6, 1),
(608, 303, 4, '2017-06-05', '08:26:25', 6, 1),
(609, 304, 4, '2017-06-05', '08:33:20', 1, 0),
(610, 305, 4, '2017-06-05', '08:33:23', 1, 0),
(611, 306, 4, '2017-06-05', '08:33:24', 1, 0),
(612, 306, 4, '2017-06-05', '08:36:05', 6, 1),
(613, 305, 4, '2017-06-05', '08:36:06', 6, 1),
(614, 304, 4, '2017-06-05', '08:42:18', 6, 1),
(615, 307, 4, '2017-06-05', '08:50:03', 1, 0),
(616, 308, 4, '2017-06-05', '08:50:03', 1, 0),
(617, 307, 4, '2017-06-05', '08:51:15', 6, 1),
(618, 308, 4, '2017-06-05', '08:51:18', 6, 1),
(619, 309, 4, '2017-06-05', '08:56:03', 1, 0),
(620, 310, 4, '2017-06-05', '08:56:03', 1, 0),
(621, 311, 4, '2017-06-05', '08:56:04', 1, 0),
(622, 311, 4, '2017-06-05', '08:57:08', 6, 1),
(623, 310, 4, '2017-06-05', '08:57:09', 6, 1),
(624, 309, 4, '2017-06-05', '08:57:11', 6, 1),
(625, 312, 4, '2017-06-05', '08:59:32', 1, 0),
(626, 313, 4, '2017-06-05', '08:59:33', 1, 0),
(627, 314, 4, '2017-06-05', '08:59:33', 1, 0),
(628, 314, 4, '2017-06-05', '09:00:36', 6, 1),
(629, 313, 4, '2017-06-05', '09:00:38', 6, 1),
(630, 312, 4, '2017-06-05', '09:00:39', 6, 1),
(631, 315, 4, '2017-06-05', '09:11:01', 1, 0),
(632, 316, 4, '2017-06-05', '09:11:03', 1, 0),
(633, 317, 4, '2017-06-05', '09:11:04', 1, 0),
(634, 317, 4, '2017-06-05', '09:13:07', 6, 1),
(635, 316, 4, '2017-06-05', '09:13:08', 6, 1),
(636, 315, 4, '2017-06-05', '09:13:10', 6, 1),
(637, 318, 4, '2017-06-05', '09:26:02', 1, 0),
(638, 319, 4, '2017-06-05', '09:27:26', 1, 0),
(639, 319, 4, '2017-06-05', '10:11:26', 6, 1),
(640, 318, 4, '2017-06-05', '10:11:27', 6, 1),
(641, 320, 4, '2017-06-05', '10:19:45', 1, 0),
(642, 320, 4, '2017-06-05', '10:19:59', 6, 1),
(643, 321, 4, '2017-06-05', '10:22:11', 1, 0),
(644, 322, 4, '2017-06-05', '10:22:12', 1, 0),
(645, 322, 4, '2017-06-05', '10:22:19', 6, 1),
(646, 321, 4, '2017-06-05', '10:22:51', 6, 1),
(647, 323, 4, '2017-06-05', '10:31:21', 1, 1),
(648, 324, 4, '2017-06-05', '10:31:22', 1, 1),
(649, 325, 4, '2017-06-05', '10:31:22', 1, 1),
(650, 326, 4, '2017-06-05', '10:31:22', 1, 1),
(651, 327, 4, '2017-06-05', '10:31:22', 1, 0),
(652, 328, 4, '2017-06-05', '10:31:22', 1, 0),
(653, 329, 4, '2017-06-05', '10:31:24', 1, 0),
(654, 329, 4, '2017-06-05', '10:39:39', 6, 1),
(655, 328, 4, '2017-06-05', '10:39:40', 6, 1),
(656, 327, 4, '2017-06-05', '10:39:41', 6, 1),
(657, 330, 4, '2017-06-05', '11:04:32', 1, 0),
(658, 331, 4, '2017-06-05', '11:04:32', 1, 0),
(659, 332, 4, '2017-06-05', '11:04:32', 1, 0),
(660, 333, 4, '2017-06-05', '11:04:33', 1, 0),
(661, 333, 4, '2017-06-05', '11:08:01', 6, 1),
(662, 332, 4, '2017-06-05', '11:08:44', 6, 1),
(663, 331, 4, '2017-06-05', '11:09:08', 6, 1),
(664, 330, 4, '2017-06-05', '11:09:15', 6, 1),
(665, 334, 4, '2017-06-05', '11:25:12', 1, 0),
(666, 335, 4, '2017-06-05', '11:25:13', 1, 0),
(667, 335, 4, '2017-06-05', '11:28:43', 6, 1),
(668, 334, 4, '2017-06-05', '11:28:45', 5, 0),
(669, 336, 4, '2017-06-05', '11:31:13', 1, 0),
(670, 334, 4, '2017-06-05', '11:31:53', 6, 1),
(671, 336, 4, '2017-06-05', '11:31:56', 6, 1),
(672, 337, 4, '2017-06-05', '11:36:15', 1, 0),
(673, 338, 4, '2017-06-05', '11:36:16', 1, 0),
(674, 339, 4, '2017-06-05', '11:36:16', 1, 0),
(675, 339, 4, '2017-06-05', '11:47:10', 6, 1),
(676, 338, 4, '2017-06-05', '11:47:12', 6, 1),
(677, 337, 4, '2017-06-05', '11:47:15', 6, 1),
(678, 340, 4, '2017-06-05', '11:49:21', 1, 0),
(679, 341, 4, '2017-06-05', '11:49:21', 1, 0),
(680, 342, 4, '2017-06-05', '11:49:21', 1, 0),
(681, 342, 4, '2017-06-05', '11:53:21', 6, 1),
(682, 341, 4, '2017-06-05', '11:53:56', 6, 1),
(683, 340, 4, '2017-06-05', '11:53:57', 6, 1),
(684, 343, 4, '2017-06-05', '11:56:28', 1, 0),
(685, 344, 4, '2017-06-05', '11:56:28', 1, 0),
(686, 344, 4, '2017-06-05', '11:57:16', 6, 1),
(687, 343, 4, '2017-06-05', '11:57:18', 6, 1),
(688, 345, 4, '2017-06-05', '11:59:40', 1, 0),
(689, 346, 4, '2017-06-05', '11:59:40', 1, 0),
(690, 346, 4, '2017-06-05', '12:00:37', 6, 1),
(691, 345, 4, '2017-06-05', '12:01:02', 6, 1),
(692, 347, 4, '2017-06-05', '12:03:10', 1, 0),
(693, 348, 4, '2017-06-05', '12:03:11', 1, 0),
(694, 348, 4, '2017-06-05', '12:06:55', 6, 1),
(695, 347, 4, '2017-06-05', '12:06:58', 6, 1),
(696, 349, 4, '2017-06-05', '12:09:31', 1, 0),
(697, 350, 4, '2017-06-05', '12:09:31', 1, 0),
(698, 350, 4, '2017-06-05', '12:10:36', 6, 1),
(699, 349, 4, '2017-06-05', '12:10:38', 6, 1),
(700, 351, 4, '2017-06-06', '07:19:04', 1, 0),
(701, 351, 4, '2017-06-06', '07:19:16', 6, 1),
(702, 352, 4, '2017-06-06', '07:21:13', 1, 0),
(703, 353, 4, '2017-06-06', '07:21:14', 1, 0),
(704, 354, 4, '2017-06-06', '07:21:14', 1, 0),
(705, 354, 4, '2017-06-06', '07:22:16', 6, 1),
(706, 353, 4, '2017-06-06', '07:22:18', 6, 1),
(707, 352, 4, '2017-06-06', '07:22:20', 6, 1),
(708, 355, 4, '2017-06-06', '07:27:41', 1, 0),
(709, 356, 4, '2017-06-06', '07:27:42', 1, 0),
(710, 357, 4, '2017-06-06', '07:27:42', 1, 0),
(711, 358, 4, '2017-06-06', '07:27:43', 1, 0),
(712, 358, 4, '2017-06-06', '07:39:19', 6, 1),
(713, 357, 4, '2017-06-06', '07:39:21', 6, 1),
(714, 356, 4, '2017-06-06', '07:39:22', 6, 1),
(715, 355, 4, '2017-06-06', '07:42:14', 6, 1),
(716, 359, 4, '2017-06-06', '07:45:54', 1, 0),
(717, 360, 4, '2017-06-06', '07:50:10', 1, 0),
(718, 361, 4, '2017-06-06', '07:50:11', 1, 0),
(719, 362, 4, '2017-06-06', '07:50:13', 1, 0),
(720, 363, 4, '2017-06-06', '07:50:14', 1, 0),
(721, 364, 4, '2017-06-06', '07:50:14', 1, 0),
(722, 365, 4, '2017-06-06', '07:50:14', 1, 0),
(723, 364, 4, '2017-06-06', '07:54:42', 6, 1),
(724, 363, 4, '2017-06-06', '07:54:43', 6, 1),
(725, 362, 4, '2017-06-06', '07:54:44', 6, 1),
(726, 361, 4, '2017-06-06', '07:54:45', 5, 0),
(727, 361, 4, '2017-06-06', '07:54:48', 6, 1),
(728, 365, 4, '2017-06-06', '07:54:56', 6, 1),
(729, 359, 4, '2017-06-06', '08:02:05', 6, 1),
(730, 360, 4, '2017-06-06', '08:02:07', 5, 0),
(731, 360, 4, '2017-06-06', '08:02:08', 6, 1),
(732, 366, 4, '2017-06-06', '08:04:43', 1, 0),
(733, 366, 4, '2017-06-06', '08:06:42', 6, 1),
(734, 367, 4, '2017-06-06', '08:08:39', 1, 0),
(735, 368, 4, '2017-06-06', '08:08:39', 1, 0),
(736, 368, 4, '2017-06-06', '08:14:23', 6, 1),
(737, 367, 4, '2017-06-06', '08:14:24', 6, 1),
(738, 369, 4, '2017-06-06', '08:17:18', 1, 0),
(739, 369, 4, '2017-06-06', '08:17:29', 6, 1),
(740, 63, 4, '2017-06-06', '08:28:02', 5, 0),
(741, 63, 4, '2017-06-06', '08:28:06', 2, 0),
(742, 63, 4, '2017-06-06', '08:28:09', 3, 1),
(743, 65, 4, '2017-06-06', '08:29:49', 3, 1),
(744, 60, 4, '2017-06-06', '08:32:18', 3, 1),
(745, 59, 4, '2017-06-06', '08:34:53', 3, 1),
(746, 58, 4, '2017-06-06', '08:36:35', 3, 1),
(747, 57, 4, '2017-06-06', '08:37:56', 3, 1),
(748, 56, 4, '2017-06-06', '08:39:35', 3, 1),
(749, 55, 4, '2017-06-06', '08:47:34', 3, 1),
(750, 54, 4, '2017-06-06', '08:48:56', 3, 1),
(751, 53, 4, '2017-06-06', '08:50:18', 3, 1),
(752, 52, 4, '2017-06-06', '08:59:57', 3, 1),
(753, 51, 4, '2017-06-06', '09:01:14', 3, 1),
(754, 50, 4, '2017-06-06', '09:01:58', 3, 1),
(755, 49, 4, '2017-06-06', '09:02:37', 3, 1),
(756, 370, 4, '2017-06-06', '09:07:48', 1, 0),
(757, 370, 4, '2017-06-06', '09:07:54', 3, 1),
(758, 371, 4, '2017-06-06', '09:13:28', 1, 0),
(759, 371, 4, '2017-06-06', '09:13:35', 3, 1),
(760, 372, 4, '2017-06-06', '09:34:35', 1, 0),
(761, 372, 4, '2017-06-06', '09:34:48', 6, 1),
(762, 373, 4, '2017-06-06', '09:39:05', 1, 0),
(763, 374, 4, '2017-06-06', '09:39:06', 1, 0),
(764, 375, 4, '2017-06-06', '09:39:06', 1, 0),
(765, 375, 4, '2017-06-06', '09:43:30', 6, 1),
(766, 374, 4, '2017-06-06', '09:44:08', 6, 1),
(767, 373, 4, '2017-06-06', '09:44:11', 6, 1),
(768, 376, 4, '2017-06-06', '10:12:56', 1, 0),
(769, 376, 4, '2017-06-06', '10:13:27', 6, 1),
(770, 377, 4, '2017-06-06', '10:16:10', 1, 0),
(771, 377, 4, '2017-06-06', '10:19:25', 6, 1),
(772, 378, 4, '2017-06-06', '10:27:36', 1, 0),
(773, 378, 4, '2017-06-06', '10:27:41', 5, 0),
(774, 378, 4, '2017-06-06', '10:28:28', 6, 1),
(775, 379, 4, '2017-06-06', '10:30:49', 1, 0),
(776, 380, 4, '2017-06-06', '10:30:49', 1, 0),
(777, 381, 4, '2017-06-06', '10:30:49', 1, 0),
(778, 382, 4, '2017-06-06', '12:34:45', 1, 0),
(779, 383, 4, '2017-06-06', '12:37:56', 1, 0),
(780, 384, 4, '2017-06-06', '12:41:01', 1, 0),
(781, 385, 4, '2017-06-06', '12:45:25', 1, 0),
(782, 385, 4, '2017-06-06', '12:51:40', 3, 0),
(783, 384, 4, '2017-06-06', '12:51:44', 3, 0),
(784, 383, 4, '2017-06-06', '12:51:47', 2, 0),
(785, 382, 4, '2017-06-06', '12:51:50', 3, 0),
(786, 384, 4, '2017-06-06', '12:52:02', 2, 0),
(787, 382, 4, '2017-06-06', '12:52:04', 2, 0),
(788, 385, 4, '2017-06-06', '12:52:06', 2, 0),
(789, 380, 4, '2017-06-07', '08:51:36', 6, 1),
(790, 379, 4, '2017-06-07', '08:51:38', 6, 1),
(791, 381, 4, '2017-06-07', '08:51:39', 6, 1),
(792, 386, 4, '2017-06-07', '08:54:25', 1, 0),
(793, 386, 4, '2017-06-07', '08:54:39', 6, 1),
(794, 387, 4, '2017-06-07', '08:56:29', 1, 0),
(795, 388, 4, '2017-06-07', '08:56:29', 1, 0),
(796, 387, 4, '2017-06-07', '09:01:07', 5, 0),
(797, 388, 4, '2017-06-07', '09:01:09', 6, 1),
(798, 389, 4, '2017-06-07', '09:03:17', 1, 0),
(799, 387, 4, '2017-06-07', '09:05:09', 6, 1),
(800, 389, 4, '2017-06-07', '09:05:09', 6, 1),
(801, 390, 4, '2017-06-07', '09:07:06', 1, 0),
(802, 390, 4, '2017-06-07', '09:07:58', 6, 1),
(803, 391, 4, '2017-06-07', '09:09:34', 1, 0),
(804, 392, 4, '2017-06-07', '09:19:50', 1, 0),
(805, 393, 4, '2017-06-07', '09:19:51', 1, 0),
(806, 391, 4, '2017-06-07', '09:20:02', 6, 1),
(807, 392, 4, '2017-06-07', '09:20:07', 6, 1),
(808, 393, 4, '2017-06-07', '09:21:59', 6, 1),
(809, 394, 4, '2017-06-09', '07:41:52', 1, 0),
(810, 395, 4, '2017-06-09', '07:41:52', 1, 0),
(811, 396, 4, '2017-06-09', '07:41:52', 1, 0),
(812, 396, 4, '2017-06-09', '07:46:41', 6, 1),
(813, 395, 4, '2017-06-09', '07:46:43', 6, 1),
(814, 394, 4, '2017-06-09', '07:46:44', 6, 1),
(815, 397, 4, '2017-06-09', '07:56:46', 1, 0),
(816, 397, 4, '2017-06-09', '07:57:17', 6, 1),
(817, 398, 4, '2017-06-09', '08:06:21', 1, 0),
(818, 399, 4, '2017-06-09', '08:06:21', 1, 0),
(819, 398, 4, '2017-06-09', '08:09:45', 6, 1),
(820, 399, 4, '2017-06-09', '08:09:47', 6, 1),
(821, 400, 4, '2017-06-09', '08:31:03', 1, 0),
(822, 401, 4, '2017-06-09', '08:31:03', 1, 0),
(823, 402, 4, '2017-06-09', '08:31:04', 1, 0),
(824, 403, 4, '2017-06-09', '08:31:04', 1, 0),
(825, 404, 4, '2017-06-09', '08:31:04', 1, 0),
(826, 404, 4, '2017-06-09', '08:35:13', 6, 1),
(827, 401, 4, '2017-06-09', '08:35:45', 6, 1),
(828, 402, 4, '2017-06-09', '08:36:40', 6, 1),
(829, 403, 4, '2017-06-09', '08:36:42', 6, 1),
(830, 400, 4, '2017-06-09', '08:37:20', 6, 1),
(831, 405, 4, '2017-06-09', '08:40:40', 1, 0),
(832, 406, 4, '2017-06-09', '08:40:40', 1, 0),
(833, 407, 4, '2017-06-09', '08:40:40', 1, 0),
(834, 408, 4, '2017-06-09', '08:40:41', 1, 0),
(835, 408, 4, '2017-06-09', '08:41:39', 6, 1),
(836, 407, 4, '2017-06-09', '08:42:32', 6, 1),
(837, 406, 4, '2017-06-09', '08:42:35', 6, 1),
(838, 405, 4, '2017-06-09', '08:43:49', 6, 1),
(839, 409, 4, '2017-06-09', '08:45:44', 1, 0),
(840, 409, 4, '2017-06-09', '08:46:03', 6, 1),
(841, 410, 4, '2017-06-09', '08:48:47', 1, 0),
(842, 411, 4, '2017-06-09', '08:48:47', 1, 0),
(843, 411, 4, '2017-06-09', '08:50:56', 6, 1),
(844, 410, 4, '2017-06-09', '08:50:58', 6, 1),
(845, 412, 4, '2017-06-09', '08:53:22', 1, 0),
(846, 413, 4, '2017-06-09', '08:53:22', 1, 0),
(847, 414, 4, '2017-06-09', '08:53:22', 1, 0),
(848, 414, 4, '2017-06-09', '08:54:45', 6, 1),
(849, 413, 4, '2017-06-09', '08:54:47', 6, 1),
(850, 412, 4, '2017-06-09', '08:56:22', 6, 1),
(851, 415, 4, '2017-06-09', '09:08:49', 1, 0),
(852, 416, 4, '2017-06-09', '09:08:49', 1, 0),
(853, 417, 4, '2017-06-09', '09:08:50', 1, 0),
(854, 418, 4, '2017-06-09', '09:08:50', 1, 0),
(855, 419, 4, '2017-06-09', '09:18:21', 1, 0),
(856, 420, 4, '2017-06-09', '09:18:21', 1, 0),
(857, 421, 4, '2017-06-09', '09:18:21', 1, 0),
(858, 422, 4, '2017-06-09', '09:18:21', 1, 0),
(859, 418, 4, '2017-06-09', '09:18:42', 6, 1),
(860, 417, 4, '2017-06-09', '09:18:45', 6, 1),
(861, 416, 4, '2017-06-09', '09:18:46', 6, 1),
(862, 415, 4, '2017-06-09', '09:18:47', 6, 1),
(863, 419, 4, '2017-06-09', '09:18:56', 6, 1),
(864, 422, 4, '2017-06-09', '09:21:56', 6, 1),
(865, 421, 4, '2017-06-09', '09:22:45', 6, 1),
(866, 420, 4, '2017-06-09', '09:23:47', 6, 1),
(867, 423, 4, '2017-06-09', '09:34:03', 1, 0),
(868, 424, 4, '2017-06-09', '09:34:03', 1, 0),
(869, 425, 4, '2017-06-09', '09:34:03', 1, 0),
(870, 426, 4, '2017-06-09', '09:34:05', 1, 0),
(871, 427, 4, '2017-06-09', '09:35:54', 1, 0),
(872, 427, 4, '2017-06-09', '09:44:06', 6, 1),
(873, 426, 4, '2017-06-09', '09:44:41', 6, 1),
(874, 425, 4, '2017-06-09', '09:49:20', 6, 1),
(875, 424, 4, '2017-06-09', '09:50:23', 6, 1),
(876, 423, 4, '2017-06-09', '09:51:55', 6, 1),
(877, 428, 4, '2017-06-09', '09:58:34', 1, 0),
(878, 429, 4, '2017-06-09', '09:58:35', 1, 0),
(879, 430, 4, '2017-06-09', '09:58:35', 1, 0),
(880, 430, 4, '2017-06-09', '10:01:08', 6, 1),
(881, 429, 4, '2017-06-09', '10:01:10', 6, 1),
(882, 428, 4, '2017-06-09', '10:01:10', 6, 1),
(883, 431, 4, '2017-06-09', '10:03:29', 1, 0),
(884, 432, 4, '2017-06-09', '10:03:29', 1, 0),
(885, 432, 4, '2017-06-09', '10:04:22', 6, 1),
(886, 431, 4, '2017-06-09', '10:04:24', 6, 1),
(887, 433, 4, '2017-06-09', '10:09:58', 1, 0),
(888, 434, 4, '2017-06-09', '10:12:39', 1, 0),
(889, 435, 4, '2017-06-09', '10:12:45', 1, 1),
(890, 433, 4, '2017-06-09', '10:13:03', 6, 1),
(891, 436, 4, '2017-06-09', '10:15:03', 1, 0),
(892, 437, 4, '2017-06-09', '10:15:03', 1, 0),
(893, 438, 4, '2017-06-09', '10:15:07', 1, 1),
(894, 434, 4, '2017-06-09', '10:16:19', 6, 1),
(895, 436, 4, '2017-06-09', '10:16:47', 6, 1),
(896, 437, 4, '2017-06-09', '10:17:42', 6, 1),
(897, 439, 4, '2017-06-09', '10:22:48', 1, 0),
(898, 440, 4, '2017-06-09', '10:26:08', 1, 0),
(899, 441, 4, '2017-06-09', '10:26:09', 1, 0),
(900, 442, 4, '2017-06-09', '10:26:09', 1, 0),
(901, 443, 4, '2017-06-09', '10:26:09', 1, 0),
(902, 444, 4, '2017-06-09', '10:26:09', 1, 0),
(903, 439, 4, '2017-06-09', '10:29:27', 6, 1),
(904, 444, 4, '2017-06-09', '10:32:20', 6, 1),
(905, 443, 4, '2017-06-09', '10:49:47', 6, 1),
(906, 442, 4, '2017-06-09', '10:51:29', 6, 1),
(907, 441, 4, '2017-06-09', '10:52:47', 6, 1),
(908, 440, 4, '2017-06-09', '10:52:58', 6, 1),
(909, 445, 4, '2017-06-09', '10:57:20', 1, 0),
(910, 446, 4, '2017-06-09', '10:57:23', 1, 0),
(911, 447, 4, '2017-06-09', '10:57:23', 1, 0),
(912, 447, 4, '2017-06-09', '10:57:59', 6, 1),
(913, 446, 4, '2017-06-09', '10:58:56', 6, 1),
(914, 445, 4, '2017-06-09', '10:59:55', 6, 1),
(915, 448, 4, '2017-06-09', '11:03:33', 1, 0),
(916, 449, 4, '2017-06-09', '11:03:40', 1, 0),
(917, 450, 4, '2017-06-09', '11:03:41', 1, 0),
(918, 449, 4, '2017-06-09', '11:04:40', 6, 0),
(919, 449, 4, '2017-06-09', '11:07:28', 2, 0),
(920, 450, 4, '2017-06-09', '11:07:32', 2, 0),
(921, 448, 4, '2017-06-09', '11:07:35', 2, 0),
(922, 451, 4, '2017-06-09', '11:15:23', 1, 0),
(923, 451, 4, '2017-06-09', '11:15:38', 2, 0),
(924, 452, 4, '2017-06-09', '11:19:56', 1, 0),
(925, 453, 4, '2017-06-09', '11:19:57', 1, 0),
(926, 454, 4, '2017-06-09', '11:19:58', 1, 0),
(927, 454, 4, '2017-06-09', '11:23:13', 6, 0),
(928, 453, 4, '2017-06-09', '11:23:14', 6, 0),
(929, 454, 4, '2017-06-09', '11:24:21', 2, 0),
(930, 453, 4, '2017-06-09', '11:24:24', 2, 0),
(931, 452, 4, '2017-06-09', '11:24:25', 2, 0),
(932, 455, 4, '2017-06-09', '11:27:05', 1, 0),
(933, 456, 4, '2017-06-09', '11:33:06', 1, 0),
(934, 457, 4, '2017-06-09', '11:33:06', 1, 0),
(935, 458, 4, '2017-06-09', '11:33:06', 1, 0),
(936, 458, 4, '2017-06-09', '11:33:55', 6, 0),
(937, 457, 4, '2017-06-09', '11:33:56', 6, 0),
(938, 456, 4, '2017-06-09', '11:34:14', 2, 0),
(939, 457, 4, '2017-06-09', '11:34:18', 2, 0),
(940, 459, 4, '2017-06-09', '11:36:47', 1, 0),
(941, 460, 4, '2017-06-09', '11:36:47', 1, 0),
(942, 461, 4, '2017-06-09', '11:36:48', 1, 0),
(943, 462, 4, '2017-06-09', '11:36:48', 1, 0),
(944, 463, 4, '2017-06-09', '11:36:48', 1, 0),
(945, 458, 4, '2017-06-09', '11:37:00', 2, 0),
(946, 463, 4, '2017-06-09', '11:38:26', 2, 0),
(947, 462, 4, '2017-06-09', '11:38:28', 2, 0),
(948, 461, 4, '2017-06-09', '11:39:07', 2, 0),
(949, 460, 4, '2017-06-09', '11:39:52', 2, 0),
(950, 459, 4, '2017-06-09', '11:42:39', 2, 0),
(951, 464, 4, '2017-06-09', '11:46:12', 1, 0),
(952, 465, 4, '2017-06-09', '11:46:12', 1, 0),
(953, 466, 4, '2017-06-09', '11:46:12', 1, 0),
(954, 466, 4, '2017-06-09', '11:46:49', 2, 0),
(955, 465, 4, '2017-06-09', '11:46:50', 2, 0),
(956, 464, 4, '2017-06-09', '11:46:52', 2, 0),
(957, 467, 4, '2017-06-12', '07:33:37', 1, 0),
(958, 468, 4, '2017-06-12', '07:37:55', 1, 0),
(959, 469, 4, '2017-06-12', '07:37:55', 1, 0),
(960, 470, 4, '2017-06-12', '07:37:55', 1, 0),
(961, 468, 4, '2017-06-12', '08:08:25', 6, 1),
(962, 467, 4, '2017-06-12', '08:08:27', 6, 1),
(963, 469, 4, '2017-06-12', '08:08:31', 6, 1),
(964, 470, 4, '2017-06-12', '08:09:13', 6, 1),
(965, 471, 4, '2017-06-12', '08:12:55', 1, 0),
(966, 471, 4, '2017-06-12', '08:20:13', 6, 1),
(967, 472, 4, '2017-06-12', '08:37:37', 1, 0),
(968, 473, 4, '2017-06-12', '08:37:37', 1, 0),
(969, 474, 4, '2017-06-12', '08:37:38', 1, 0),
(970, 475, 4, '2017-06-12', '08:37:38', 1, 0),
(971, 476, 4, '2017-06-12', '08:37:38', 1, 0),
(972, 476, 4, '2017-06-12', '08:38:23', 6, 1),
(973, 475, 4, '2017-06-12', '08:39:36', 6, 1),
(974, 474, 4, '2017-06-12', '08:44:38', 6, 1),
(975, 473, 4, '2017-06-12', '08:50:45', 6, 1),
(976, 472, 4, '2017-06-12', '08:59:38', 6, 1),
(977, 477, 4, '2017-06-12', '09:06:46', 1, 0),
(978, 477, 4, '2017-06-12', '09:08:59', 6, 1),
(979, 478, 4, '2017-06-12', '09:10:32', 1, 0),
(980, 478, 4, '2017-06-12', '09:12:38', 6, 1),
(981, 479, 4, '2017-06-12', '09:16:21', 1, 0),
(982, 480, 4, '2017-06-12', '09:16:21', 1, 0),
(983, 481, 4, '2017-06-12', '09:16:22', 1, 0),
(984, 482, 4, '2017-06-12', '09:17:31', 1, 0),
(985, 479, 4, '2017-06-12', '09:17:52', 6, 1),
(986, 481, 4, '2017-06-12', '09:23:21', 6, 1),
(987, 480, 4, '2017-06-12', '09:23:23', 6, 1),
(988, 482, 4, '2017-06-12', '09:24:46', 6, 1),
(989, 483, 4, '2017-06-12', '09:30:09', 1, 0),
(990, 484, 4, '2017-06-12', '09:30:09', 1, 0),
(991, 485, 4, '2017-06-12', '09:30:09', 1, 0),
(992, 486, 4, '2017-06-12', '09:30:09', 1, 0),
(993, 486, 4, '2017-06-12', '09:31:28', 6, 1),
(994, 485, 4, '2017-06-12', '09:34:38', 6, 1),
(995, 484, 4, '2017-06-12', '09:34:42', 6, 1),
(996, 483, 4, '2017-06-12', '09:34:44', 6, 1),
(997, 487, 4, '2017-06-12', '09:39:52', 1, 0),
(998, 487, 4, '2017-06-12', '09:40:16', 6, 1),
(999, 488, 4, '2017-06-12', '09:42:22', 1, 0),
(1000, 489, 4, '2017-06-12', '09:42:23', 1, 0),
(1001, 490, 4, '2017-06-12', '09:43:13', 1, 0),
(1002, 488, 4, '2017-06-12', '09:45:51', 6, 1),
(1003, 489, 4, '2017-06-12', '09:46:54', 6, 1),
(1004, 490, 4, '2017-06-12', '09:48:40', 6, 1),
(1005, 491, 4, '2017-06-12', '09:50:38', 1, 0),
(1006, 492, 4, '2017-06-12', '09:50:39', 1, 0),
(1007, 492, 4, '2017-06-12', '09:53:10', 6, 1),
(1008, 491, 4, '2017-06-12', '09:53:12', 6, 1),
(1009, 493, 4, '2017-06-12', '09:55:05', 1, 0),
(1010, 494, 4, '2017-06-12', '09:55:05', 1, 0),
(1011, 494, 4, '2017-06-12', '09:56:15', 6, 1),
(1012, 493, 4, '2017-06-12', '09:57:53', 6, 1),
(1013, 455, 4, '2017-06-12', '10:00:23', 2, 0),
(1014, 495, 4, '2017-06-12', '10:07:14', 1, 0),
(1015, 495, 4, '2017-06-12', '10:13:29', 6, 1),
(1016, 496, 4, '2017-06-12', '10:15:04', 1, 0),
(1017, 497, 4, '2017-06-12', '10:15:05', 1, 0),
(1018, 498, 4, '2017-06-12', '10:15:07', 1, 0),
(1019, 499, 4, '2017-06-12', '10:15:07', 1, 0),
(1020, 499, 4, '2017-06-12', '10:18:40', 5, 0),
(1021, 499, 4, '2017-06-12', '10:18:41', 6, 1),
(1022, 498, 4, '2017-06-12', '10:18:42', 5, 0),
(1023, 498, 4, '2017-06-12', '10:18:44', 6, 1),
(1024, 497, 4, '2017-06-12', '10:18:47', 6, 1),
(1025, 496, 4, '2017-06-12', '10:19:29', 6, 1),
(1026, 500, 4, '2017-06-12', '10:26:38', 1, 0),
(1027, 500, 4, '2017-06-12', '10:26:43', 6, 1),
(1028, 501, 4, '2017-06-12', '10:29:19', 1, 0),
(1029, 502, 4, '2017-06-12', '10:29:19', 1, 0),
(1030, 502, 4, '2017-06-12', '10:30:28', 6, 1),
(1031, 501, 4, '2017-06-12', '10:30:30', 6, 1),
(1032, 503, 4, '2017-06-12', '10:33:15', 1, 0),
(1033, 504, 4, '2017-06-12', '10:33:15', 1, 0),
(1034, 504, 4, '2017-06-12', '10:50:57', 6, 1),
(1035, 503, 4, '2017-06-12', '10:50:59', 6, 1),
(1036, 454, 4, '2017-06-13', '07:19:17', 3, 0),
(1037, 455, 4, '2017-06-13', '07:46:59', 3, 0),
(1038, 452, 4, '2017-06-13', '07:48:55', 3, 0),
(1039, 453, 4, '2017-06-13', '07:49:10', 4, 1),
(1040, 454, 4, '2017-06-13', '07:49:12', 4, 1),
(1041, 455, 4, '2017-06-13', '07:49:15', 4, 1),
(1042, 452, 4, '2017-06-13', '07:49:18', 4, 1),
(1043, 451, 4, '2017-06-13', '07:52:04', 4, 1),
(1044, 450, 4, '2017-06-13', '07:53:37', 4, 1),
(1045, 449, 4, '2017-06-13', '07:55:32', 4, 1),
(1046, 385, 4, '2017-06-13', '07:58:18', 4, 1),
(1047, 383, 4, '2017-06-13', '08:03:58', 4, 1),
(1048, 382, 4, '2017-06-13', '08:04:01', 4, 1),
(1049, 384, 4, '2017-06-13', '08:04:25', 4, 1),
(1050, 448, 4, '2017-06-13', '08:07:54', 4, 1),
(1051, 505, 4, '2017-06-13', '08:10:42', 1, 0),
(1052, 505, 4, '2017-06-13', '08:12:39', 4, 1),
(1053, 506, 4, '2017-06-13', '08:19:20', 1, 0),
(1054, 506, 4, '2017-06-13', '08:19:25', 4, 1),
(1055, 507, 4, '2017-06-13', '08:24:20', 1, 0),
(1056, 507, 4, '2017-06-13', '08:24:32', 4, 1),
(1057, 508, 4, '2017-06-13', '08:27:25', 1, 0),
(1058, 509, 4, '2017-06-13', '08:27:25', 1, 0),
(1059, 508, 4, '2017-06-13', '08:27:33', 4, 1),
(1060, 509, 4, '2017-06-13', '08:30:21', 4, 1),
(1061, 510, 4, '2017-06-13', '08:32:49', 1, 0),
(1062, 511, 4, '2017-06-13', '08:32:49', 1, 0),
(1063, 511, 4, '2017-06-13', '08:34:20', 6, 0),
(1064, 510, 4, '2017-06-13', '08:34:21', 4, 1),
(1065, 511, 4, '2017-06-13', '08:34:23', 4, 1),
(1066, 512, 4, '2017-06-15', '10:28:25', 1, 0),
(1067, 513, 4, '2017-06-15', '10:28:25', 1, 0),
(1068, 514, 4, '2017-06-15', '10:28:25', 1, 0),
(1069, 515, 4, '2017-06-15', '10:31:57', 1, 0),
(1070, 516, 4, '2017-06-15', '10:31:57', 1, 0),
(1071, 517, 4, '2017-06-15', '10:33:45', 1, 0),
(1072, 518, 4, '2017-06-15', '10:33:45', 1, 0),
(1073, 519, 4, '2017-06-15', '10:36:55', 1, 0),
(1074, 520, 4, '2017-06-15', '10:36:55', 1, 0),
(1075, 521, 4, '2017-06-15', '10:36:55', 1, 0),
(1076, 466, 4, '2017-06-19', '11:09:42', 3, 1),
(1077, 465, 4, '2017-06-19', '11:11:18', 3, 1),
(1078, 464, 4, '2017-06-19', '11:15:33', 3, 1),
(1079, 463, 4, '2017-06-19', '11:17:25', 5, 0),
(1080, 463, 4, '2017-06-19', '11:17:33', 3, 1),
(1081, 462, 4, '2017-06-19', '11:19:17', 3, 1),
(1082, 461, 4, '2017-06-19', '11:20:43', 3, 1),
(1083, 460, 4, '2017-06-19', '11:22:22', 3, 1),
(1084, 459, 4, '2017-06-19', '11:25:05', 3, 1),
(1085, 458, 4, '2017-06-19', '11:26:47', 3, 1),
(1086, 457, 4, '2017-06-19', '11:28:31', 3, 1),
(1087, 456, 4, '2017-06-19', '11:29:55', 3, 1),
(1088, 522, 4, '2017-06-19', '11:32:44', 1, 0),
(1089, 523, 4, '2017-06-19', '11:32:44', 1, 0),
(1090, 522, 4, '2017-06-19', '11:33:42', 3, 1),
(1091, 523, 4, '2017-06-19', '11:33:44', 3, 1),
(1092, 524, 4, '2017-06-19', '11:35:56', 1, 0),
(1093, 525, 4, '2017-06-19', '11:35:57', 1, 0),
(1094, 525, 4, '2017-06-19', '11:37:22', 3, 1),
(1095, 524, 4, '2017-06-19', '11:37:25', 3, 1),
(1096, 526, 4, '2017-06-19', '11:39:47', 1, 0),
(1097, 526, 4, '2017-06-19', '11:39:51', 3, 1),
(1098, 527, 4, '2017-06-19', '11:41:29', 1, 0),
(1099, 527, 4, '2017-06-19', '11:41:33', 3, 1),
(1100, 528, 4, '2017-06-19', '11:44:01', 1, 0),
(1101, 529, 4, '2017-06-19', '11:44:02', 1, 0),
(1102, 528, 4, '2017-06-19', '11:44:53', 3, 1),
(1103, 529, 4, '2017-06-19', '11:44:58', 5, 0),
(1104, 529, 4, '2017-06-19', '11:45:10', 4, 0),
(1105, 529, 4, '2017-06-19', '11:45:25', 3, 1),
(1106, 106, 4, '2017-06-19', '12:03:07', 6, 1),
(1107, 105, 4, '2017-06-19', '12:03:08', 6, 1),
(1108, 102, 4, '2017-06-19', '12:03:12', 6, 1),
(1109, 104, 4, '2017-06-19', '12:03:16', 6, 1),
(1110, 101, 4, '2017-06-19', '12:03:19', 6, 1),
(1111, 100, 4, '2017-06-19', '12:03:26', 6, 1),
(1112, 99, 4, '2017-06-19', '12:03:27', 6, 1),
(1113, 97, 4, '2017-06-19', '12:03:31', 6, 1),
(1114, 98, 4, '2017-06-19', '12:03:33', 6, 1),
(1115, 96, 4, '2017-06-19', '12:03:40', 6, 1),
(1116, 95, 4, '2017-06-19', '12:03:42', 6, 1),
(1117, 94, 4, '2017-06-19', '12:03:44', 6, 1),
(1118, 93, 4, '2017-06-19', '12:03:45', 6, 1),
(1119, 92, 4, '2017-06-19', '12:03:48', 6, 1),
(1120, 89, 4, '2017-06-19', '12:03:55', 6, 1),
(1121, 88, 4, '2017-06-19', '12:03:57', 6, 1),
(1122, 87, 4, '2017-06-19', '12:03:59', 6, 1),
(1123, 86, 4, '2017-06-19', '12:04:00', 6, 1),
(1124, 90, 4, '2017-06-19', '12:04:10', 6, 1),
(1125, 126, 4, '2017-06-19', '12:05:34', 6, 1),
(1126, 85, 4, '2017-06-19', '12:05:51', 6, 1),
(1127, 84, 4, '2017-06-19', '12:05:52', 6, 1),
(1128, 82, 4, '2017-06-19', '12:05:57', 6, 1),
(1129, 80, 4, '2017-06-19', '12:06:01', 6, 1),
(1130, 79, 4, '2017-06-19', '12:06:03', 6, 1),
(1131, 78, 4, '2017-06-19', '12:06:03', 6, 1),
(1132, 75, 4, '2017-06-19', '12:06:07', 6, 1),
(1133, 77, 4, '2017-06-19', '12:06:16', 6, 1),
(1134, 76, 4, '2017-06-19', '12:06:17', 6, 1),
(1135, 74, 4, '2017-06-19', '12:06:27', 6, 1),
(1136, 73, 4, '2017-06-19', '12:06:27', 6, 1),
(1137, 72, 4, '2017-06-19', '12:06:48', 6, 1),
(1138, 70, 4, '2017-06-19', '12:06:51', 6, 1),
(1139, 69, 4, '2017-06-19', '12:06:58', 6, 1),
(1140, 68, 4, '2017-06-19', '12:07:01', 6, 1),
(1141, 66, 4, '2017-06-19', '12:07:04', 6, 1),
(1142, 67, 4, '2017-06-19', '12:07:08', 6, 1),
(1143, 48, 4, '2017-06-19', '12:07:45', 6, 1),
(1144, 46, 4, '2017-06-19', '12:07:48', 6, 1),
(1145, 47, 4, '2017-06-19', '12:07:51', 6, 1),
(1146, 44, 4, '2017-06-19', '12:07:53', 6, 1),
(1147, 45, 4, '2017-06-19', '12:07:56', 6, 1),
(1148, 43, 4, '2017-06-19', '12:08:00', 6, 1),
(1149, 42, 4, '2017-06-19', '12:08:03', 6, 1),
(1150, 39, 4, '2017-06-19', '12:08:06', 6, 1),
(1151, 38, 4, '2017-06-19', '12:08:09', 6, 1),
(1152, 41, 4, '2017-06-19', '12:08:12', 6, 1),
(1153, 37, 4, '2017-06-19', '12:08:15', 6, 1),
(1154, 36, 4, '2017-06-19', '12:08:19', 6, 1),
(1155, 35, 4, '2017-06-19', '12:08:30', 6, 1),
(1156, 33, 4, '2017-06-19', '12:08:33', 6, 1),
(1157, 32, 4, '2017-06-19', '12:08:41', 6, 1),
(1158, 29, 4, '2017-06-19', '12:08:47', 6, 1),
(1159, 28, 4, '2017-06-19', '12:08:48', 6, 1),
(1160, 30, 4, '2017-06-19', '12:08:50', 6, 1),
(1161, 31, 4, '2017-06-19', '12:08:52', 6, 1),
(1162, 530, 4, '2017-06-26', '07:54:46', 1, 1),
(1163, 531, 4, '2017-06-26', '07:54:46', 1, 1),
(1164, 532, 4, '2017-06-26', '07:54:46', 1, 1),
(1165, 533, 4, '2017-06-26', '08:01:44', 1, 1),
(1166, 534, 4, '2017-06-26', '08:01:45', 1, 1),
(1167, 535, 4, '2017-06-26', '08:01:45', 1, 1),
(1168, 536, 4, '2017-06-26', '08:01:47', 1, 1),
(1169, 537, 4, '2017-06-26', '08:01:47', 1, 1),
(1170, 538, 4, '2017-06-26', '08:11:39', 1, 1),
(1171, 539, 4, '2017-06-26', '08:11:48', 1, 1),
(1172, 540, 4, '2017-06-26', '08:11:49', 1, 1),
(1173, 520, 4, '2017-06-26', '10:10:11', 3, 1),
(1174, 521, 4, '2017-06-26', '10:12:14', 3, 1),
(1175, 519, 4, '2017-06-26', '10:13:32', 3, 1),
(1176, 518, 4, '2017-06-26', '10:18:11', 3, 1),
(1177, 517, 4, '2017-06-26', '10:18:14', 3, 1),
(1178, 516, 4, '2017-06-26', '10:23:31', 3, 1),
(1179, 515, 4, '2017-06-26', '10:25:22', 3, 1),
(1180, 514, 4, '2017-06-26', '10:30:08', 3, 1),
(1181, 513, 4, '2017-06-26', '10:30:11', 3, 1);
INSERT INTO `movimientos` (`id`, `id_contenedor`, `id_usuario`, `fecha`, `hora`, `id_estado`, `activo`) VALUES
(1182, 512, 4, '2017-06-26', '10:31:19', 3, 1),
(1183, 541, 4, '2017-06-26', '10:33:31', 1, 0),
(1184, 542, 4, '2017-06-26', '10:33:32', 1, 0),
(1185, 542, 4, '2017-06-26', '10:35:36', 5, 1),
(1186, 542, 4, '2017-06-26', '10:35:36', 3, 1),
(1187, 541, 4, '2017-06-26', '10:36:46', 3, 1),
(1188, 543, 4, '2017-06-26', '10:38:43', 1, 0),
(1189, 543, 4, '2017-06-26', '10:38:47', 3, 1),
(1190, 544, 4, '2017-06-26', '11:02:02', 1, 0),
(1191, 545, 4, '2017-06-26', '11:02:02', 1, 0),
(1192, 545, 4, '2017-06-26', '11:03:10', 3, 1),
(1193, 544, 4, '2017-06-26', '11:03:13', 3, 1),
(1194, 546, 4, '2017-06-26', '11:05:59', 1, 0),
(1195, 547, 4, '2017-06-26', '11:05:59', 1, 0),
(1196, 546, 4, '2017-06-26', '11:08:42', 3, 1),
(1197, 547, 4, '2017-06-26', '11:08:44', 3, 1),
(1198, 548, 4, '2017-06-26', '11:10:53', 1, 0),
(1199, 549, 4, '2017-06-26', '11:10:53', 1, 0),
(1200, 549, 4, '2017-06-26', '11:12:08', 3, 1),
(1201, 548, 4, '2017-06-26', '11:12:11', 3, 1),
(1202, 550, 4, '2017-06-30', '09:10:44', 1, 1),
(1203, 551, 4, '2017-06-30', '09:10:45', 1, 1),
(1204, 552, 4, '2017-06-30', '09:13:44', 1, 1),
(1205, 553, 4, '2017-06-30', '09:16:13', 1, 1),
(1206, 554, 4, '2017-06-30', '09:18:20', 1, 1),
(1207, 555, 4, '2017-06-30', '09:18:21', 1, 1),
(1208, 556, 4, '2017-06-30', '09:18:21', 1, 1),
(1209, 557, 4, '2017-06-30', '09:24:20', 1, 1),
(1210, 558, 4, '2017-06-30', '09:24:20', 1, 1),
(1211, 559, 4, '2017-06-30', '09:27:19', 1, 1),
(1212, 560, 4, '2017-06-30', '09:27:19', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nave`
--

CREATE TABLE `nave` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nave`
--

INSERT INTO `nave` (`id`, `nombre`, `activo`) VALUES
(1, 'MARINER', 1),
(3, 'LOMBOK STRAIT', 1),
(21, 'NORTHERN DEXTERITY', 1),
(22, 'HANSA MEERS BURG', 1),
(24, 'AEREO', 1),
(25, 'ALGOL', 1),
(26, 'ALIOTH', 1),
(27, 'ALM ZURICH', 1),
(28, 'AOTEA MAERSK', 1),
(29, 'ARKADIA', 1),
(30, 'AS FIORELLA', 1),
(31, 'ATLANTIC KLIPPER', 1),
(32, 'ATLANTIC REEFER', 1),
(33, 'AVELONA STAR', 1),
(34, 'AVILA STAR', 1),
(35, 'BALTHASAR SHULTE', 1),
(36, 'BALTIC KLIPPER', 1),
(37, 'BARBARA', 1),
(38, 'BELLA SHULTE', 1),
(39, 'BIRK', 1),
(40, 'BODO SHULTE', 1),
(41, 'BOMAR RESOLUTE', 1),
(42, 'CALLAO EXPRESS', 1),
(43, 'CAP DOUKATO', 1),
(44, 'CAP INES', 1),
(45, 'CAP ISABEL', 1),
(46, 'CAP PALLISER', 1),
(47, 'CAP PALMESTOR', 1),
(48, 'CAP PASLEY', 1),
(49, 'CAP PATTON', 1),
(50, 'CAP PORTLAND', 1),
(51, 'CAP SAN SOUNIO', 1),
(52, 'CAP SAN TAINARO', 1),
(53, 'CAP TALBOT', 1),
(54, 'CAPA', 1),
(55, 'CARDIFF TRADER', 1),
(56, 'CAROLINA STAR', 1),
(57, 'CARSTEN MAERSK', 1),
(58, 'CATHARINA SHULTER', 1),
(59, 'CERINTHUS', 1),
(60, 'CHOAPA TRADER', 1),
(61, 'CMA CGM AMERICA', 1),
(62, 'CMA CGM MISSISSIPPI', 1),
(63, 'CMA CGM SAMBHAR', 1),
(64, 'CNP ILO', 1),
(65, 'CNP PAITA', 1),
(66, 'CONTI ARABELLA', 1),
(67, 'CONTI SOLOME', 1),
(68, 'COSCO KOREA', 1),
(69, 'CREDO', 1),
(70, 'CSCL LONG BEACH', 1),
(71, 'DOLE CALIFORNIA', 1),
(72, 'DOLE COSTA RICA', 1),
(73, 'DOLE ECUADOR', 1),
(74, 'DOLE PACIFIC', 1),
(75, 'DOMINGO', 1),
(76, 'DUBLIAN EXPRESS', 1),
(77, 'DUNCAN ISLAND', 1),
(78, 'ELISABETH-S', 1),
(79, 'EM HYDRA', 1),
(80, 'EVER EXCEL', 1),
(81, 'EVER SAFETY', 1),
(82, 'EVER UBERTY', 1),
(83, 'EVER UNION', 1),
(84, 'EVER URANUS', 1),
(85, 'EVERGREEN LINE', 1),
(86, 'FIONA', 1),
(87, 'FRISIA ROTTERDAM', 1),
(88, 'GLASGLOW EXPRESS', 1),
(89, 'GLORIA', 1),
(90, 'HAMBURG SUDD', 1),
(91, 'HAMMONIA AMERICA', 1),
(92, 'HANNAH SHULTE', 1),
(93, 'HANSA ASIA', 1),
(94, 'HANSA AUSTRALIA', 1),
(95, 'HANSA BREMEN', 1),
(96, 'HANSA CLOPPENBURG', 1),
(97, 'HANSA EUROPE', 1),
(98, 'HANSA FLENSBURG', 1),
(99, 'HANSA FREYBURG', 1),
(100, 'HANSA MEERSBURG', 1),
(101, 'HANSA OLDERBUNG', 1),
(102, 'HELENE 5', 1),
(103, 'HELLAS REEFER', 1),
(104, 'HYUNDAI SPLENDOR', 1),
(105, 'ITAL LIRICA', 1),
(106, 'ITALIA REEFER', 1),
(107, 'JACK LONDON', 1),
(108, 'JAMILA', 1),
(109, 'JPO VELA', 1),
(110, 'JULES VERNE', 1),
(111, 'JULIANA', 1),
(112, 'KIEL TRADER', 1),
(113, 'KMARIN AQUA', 1),
(114, 'KMARIN AZUR', 1),
(115, 'KOTA LAHIR', 1),
(116, 'KOTA LATIF', 1),
(117, 'KOTA LAYANG', 1),
(118, 'KOTA LEGIT', 1),
(119, 'KOTA LUKIS', 1),
(120, 'LADY KORCULA', 1),
(121, 'LADY REEFER', 1),
(122, 'LIVERPOOL EXPRESS', 1),
(123, 'LUNA MAERSK', 1),
(124, 'LUZON STRAIT', 1),
(125, 'MAERSK ALFIRK', 1),
(126, 'MAERSK ANTARES', 1),
(127, 'MAERSK BINTAN', 1),
(128, 'MAERSK BOGOR', 1),
(129, 'MAERSK GAIRLOCH', 1),
(130, 'MAERSK GIRONDE', 1),
(131, 'MAERSK LA PAZ', 1),
(132, 'MAERSK LAUNCESTON', 1),
(133, 'MAERSK LOME', 1),
(134, 'MAERSK NEWHAVEN', 1),
(135, 'MAGARI', 1),
(136, 'MARIA - KATHARINE S', 1),
(137, 'MAX WONDER', 1),
(138, 'MEDOCEAN', 1),
(139, 'MERIDIAN', 1),
(140, 'MOL GRANDEUR', 1),
(141, 'MOL PARADISE', 1),
(142, 'MOL PRESTIGE', 1),
(143, 'MSC AGRIGENTO', 1),
(144, 'MSC ALGECIRAS', 1),
(145, 'MSC ANISHA R', 1),
(146, 'MSC ANZU', 1),
(147, 'MSC ARUSHI R', 1),
(148, 'MSC ATHENS', 1),
(149, 'MSC AZOV', 1),
(150, 'MSC BRANKA', 1),
(151, 'MSC BRUNELLA', 1),
(152, 'MSC CARMEN', 1),
(153, 'MSC CHLOE', 1),
(154, 'MSC DOMITELLE', 1),
(155, 'MSC DONATA', 1),
(156, 'MSC FLAVIA', 1),
(157, 'MSC JULIA R', 1),
(158, 'MSC KATRINA', 1),
(159, 'MSC KATYA R', 1),
(160, 'MSC KIM', 1),
(161, 'MSC LEANNE', 1),
(162, 'MSC LEIGHT', 1),
(163, 'MSC LORENA', 1),
(164, 'MSC LOS ANGELES', 1),
(165, 'MSC MANU', 1),
(166, 'MSC PALAK', 1),
(167, 'MSC ROSARIA', 1),
(168, 'MSC SARAH', 1),
(169, 'MSC SARISKA', 1),
(170, 'MSC SASHA', 1),
(171, 'MSC SILVA', 1),
(172, 'MSC SOFIA CELESTE', 1),
(173, 'MSC VAISHNAVI R', 1),
(174, 'MSC VIDISHA R.', 1),
(175, 'MSC ZLATA R', 1),
(176, 'NEDERLAND REFFER', 1),
(177, 'NORASIA ALYA', 1),
(178, 'NORDAMELIA', 1),
(179, 'NORDSERENA', 1),
(180, 'NYK LIBRA', 1),
(181, 'NYL LYRA', 1),
(182, 'PACIFIC REEFER', 1),
(183, 'PIL', 1),
(184, 'POLARLIGHT', 1),
(185, 'RT ODIN', 1),
(186, 'SAFMARINE BENGUELA', 1),
(187, 'SAFMARINE MULANJE', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opcion`
--

CREATE TABLE `opcion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT 'home.',
  `icono` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `opcion`
--

INSERT INTO `opcion` (`id`, `nombre`, `url`, `icono`, `activo`) VALUES
(7, 'Container', 'home.container', 'truck', 1),
(8, 'Semana', 'home.semana', 'calendar', 1),
(9, 'Mantenimientos', 'home.mantenimiento', 'cog', 1),
(10, 'Reportes', 'home.reporte', 'file-excel-o', 1),
(11, 'Datos Estadisticos', 'home.grafico', 'area-chart', 1),
(12, 'Gestión de Usuarios', 'home.usuario', 'users', 1),
(13, 'Cliente', 'home.containers.cliente', 'users', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opcion_subopcion`
--

CREATE TABLE `opcion_subopcion` (
  `id` int(11) NOT NULL,
  `opcion_id` int(11) NOT NULL,
  `subopcion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `opcion_subopcion`
--

INSERT INTO `opcion_subopcion` (`id`, `opcion_id`, `subopcion_id`) VALUES
(8, 7, 9),
(9, 8, 10),
(10, 9, 11),
(12, 9, 13),
(13, 9, 14),
(14, 9, 15),
(15, 12, 16),
(16, 12, 17),
(17, 12, 18),
(19, 9, 19),
(21, 9, 21),
(22, 10, 22),
(23, 10, 23),
(24, 10, 24),
(25, 11, 25),
(26, 11, 26),
(27, 11, 27),
(28, 9, 28),
(29, 13, 29),
(30, 9, 30),
(31, 9, 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operador`
--

CREATE TABLE `operador` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `operador`
--

INSERT INTO `operador` (`id`, `nombre`, `slug`, `activo`) VALUES
(1, 'SAN MIGUEL SERVICIOS LOGISTICOS S.C.R.L', '', 1),
(2, 'TRANSLOGISTCS SAC', '', 1),
(3, 'NEPTUNIA S.A.', '', 1),
(5, 'TPSAC', '', 0),
(6, 'AFE TRANSPORTATION S.A.C.', 'afetransportation123', 1),
(7, 'GRUPO ADUAN', '', 1),
(8, 'YACZ CARGO', '', 1),
(9, 'TAN CARGO PERU SAC', '', 1),
(10, 'BLUE EXPRESS RED', 'blueexpress', 1),
(11, 'TRANSPORTES GIRASOLES', '', 1),
(12, 'RANSA', '', 1),
(13, 'SMP SERVICIOS POSTALES Y LOGÍSTICOS', '', 1),
(14, 'SEATRADE', '', 1),
(20, 'GRUPO ALYSSA SAC', 'grupoalyssasac', 0),
(21, 'Darwin sac', 'darwinsac', 0),
(22, 'JeniffSAC', 'jeniff', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packing`
--

CREATE TABLE `packing` (
  `id` int(11) NOT NULL,
  `id_contenedor` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `nombre`, `activo`) VALUES
(1, 'HOLANDA', 1),
(2, 'ALEMANIA', 1),
(3, 'ESTADOS UNIDOS', 1),
(4, 'PANAMA', 1),
(5, 'BELGICA', 1),
(6, 'JAPON', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`id`, `nombre`, `slug`, `activo`) VALUES
(1, 'Registrar Contenedores', 'contenedor.registrar', 1),
(2, 'Registrar Facturas', 'facturas.registrar', 1),
(3, 'Registrar Packing', 'packings.registrar', 1),
(4, 'Registrar Certificados', 'certificados.registrar', 1),
(8, 'Aperturar Semana', 'semana.aperturar', 1),
(9, 'Eliminar Semana', 'semana.eliminar', 1),
(10, 'Actualizar Contenedor', 'contenedor.actualizar', 1),
(11, 'Eliminar Contenedor', 'contenedor.eliminar', 1),
(13, 'Agregar Registro Maestro', 'mantenimiento.registrar', 1),
(14, 'Eliminar Registro Maestro', 'mantenimiento.eliminar', 1),
(15, 'Cambiar estado de contenedor', 'contenedor.cambiar_estado', 1),
(16, 'Actualizar Registro Maestro', 'mantenimiento.actualizar', 1),
(17, 'Eliminar Ceritificados', 'certificados.eliminar', 1),
(18, 'Eliminar Facturas', 'facturas.eliminar', 1),
(19, 'Eliminar Packings', 'packings.eliminar', 1),
(20, 'Actualizar Password', 'usuario.actualizar_password', 1),
(21, 'Actualizar Nombre Usuario', 'usuario.actualizar_usuario', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso_rol`
--

CREATE TABLE `permiso_rol` (
  `id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `permiso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso_rol`
--

INSERT INTO `permiso_rol` (`id`, `rol_id`, `permiso_id`) VALUES
(23, 1, 8),
(24, 1, 9),
(26, 1, 11),
(27, 1, 13),
(28, 1, 14),
(30, 2, 1),
(31, 2, 2),
(32, 2, 3),
(33, 2, 4),
(35, 2, 9),
(36, 2, 10),
(37, 2, 11),
(39, 2, 13),
(40, 2, 14),
(42, 1, 10),
(43, 1, 15),
(44, 1, 16),
(45, 2, 15),
(46, 2, 16),
(47, 1, 20),
(48, 2, 20),
(49, 6, 20),
(50, 1, 17),
(51, 1, 18),
(52, 1, 19),
(55, 1, 4),
(57, 2, 8),
(58, 7, 3),
(59, 7, 10),
(60, 7, 11),
(61, 7, 15),
(62, 7, 19),
(63, 1, 21),
(64, 1, 1),
(65, 1, 2),
(66, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `dni` char(8) DEFAULT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_paterno` varchar(50) NOT NULL,
  `apellido_materno` varchar(50) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `id_cargo` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `dni`, `nombre`, `apellido_paterno`, `apellido_materno`, `fullname`, `email`, `telefono`, `id_cargo`, `activo`) VALUES
(6, '00000000', 'APPBOSA', 'SAMAN', 'SAMAN', 'APPBOSA SAMAN SAMAN', 'appbosa@yahoo.com', '969412141', 2, 1),
(8, '40021803', 'Juan', 'Calderón', 'More', 'Juan Calderón More', 'jucamo78appbosa@hotmail.com', '969051503', 9, 1),
(21, '90879876', 'AGROFAIR', '', '', 'AGROFAIR  ', 'agrofair@hotmail.com', '987634589', 8, 1),
(22, '45645678', 'BIODYNAMISKA', '', '', 'BIODYNAMISKA  ', 'BIODYNAMISKA@hotmail.com', '986798907', 8, 1),
(23, '89897867', 'DOLE', '', '', 'DOLE  ', 'dole@hotmail.com', '890767345', 8, 1),
(24, '98779845', 'TRANSASTRA', '', '', 'TRANSASTRA  ', 'transastra@hotmail.com', '908745345', 8, 1),
(25, '98767678', 'EQUIFRUIT', '', '', 'EQUIFRUIT  ', 'equifruit@hotmail.com', '987876789', 8, 1),
(27, '42262238', 'Nilton Anterio', 'Ancajima', 'Castro', 'Nilton Anterio Ancajima Castro', 'nilton_ancajima@hotmail.com', '938228070', 10, 1),
(28, '42661236', 'Luis Alberto', 'Mejias', 'Escobar', 'Luis Alberto Mejias Escobar', 'luisalberto@hotmail.com', '111111111', 10, 1),
(36, '00000001', 'TRANSLOGISTCS SAC', 'TRANSLOGISTCS SAC', 'TRANSLOGISTCS SAC', 'TRANSLOGISTCS SAC', NULL, '909090900', 11, 1),
(38, NULL, 'Darwin sac', 'Darwin sac', 'Darwin sac', 'Darwin sac', 'darwinsac@hotmail.com', NULL, 11, 1),
(39, NULL, 'AFE TRANSPORTATION S.A.C.', '', '', 'AFE TRANSPORTATION S.A.C.  ', 'afetransportation@hotmail.com', NULL, 11, 1),
(40, NULL, 'BLUE EXPRESS RED', '', '', 'BLUE EXPRESS RED  ', 'blueexpress@hotmail.com', NULL, 11, 1),
(41, NULL, 'JeniffSAC', 'JeniffSAC', 'JeniffSAC', 'JeniffSAC', 'jeniff@hotmail.com', NULL, 11, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerto_destino`
--

CREATE TABLE `puerto_destino` (
  `id` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puerto_destino`
--

INSERT INTO `puerto_destino` (`id`, `id_ciudad`, `nombre`, `activo`) VALUES
(1, 2, 'HAMBURGO\r\n', 1),
(2, 3, 'MONTREAL', 1),
(3, 1, 'ROTTERDAM', 1),
(4, 7, 'SAN DIEGO', 1),
(5, 1, 'WILMINGTON', 1),
(6, 1, 'ANTWERP', 1),
(7, 1, 'KOREA', 1),
(11, 4, 'VLISSINGEN', 1),
(12, 5, 'DOVER', 1),
(13, 6, 'NEW ORLEANS', 1),
(14, 7, 'PORT HUENEME', 1),
(15, 8, 'RODMAN', 1),
(16, 9, 'PORT FREEPORT', 1),
(17, 11, 'DELAWARE', 1),
(18, 12, 'EVERGALDES', 1),
(19, 13, 'PORT ANTWERP', 1),
(20, 14, 'PHILADELPHIA', 1),
(21, 12, 'MIAMI', 1),
(22, 7, 'WILMINGTON, US', 1),
(23, 15, 'ALIOTH', 1),
(24, 13, 'AMBERES', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `id_menu`, `activo`) VALUES
(1, 'Administrador', 14, 1),
(2, 'Jefe Trazabilidad', 15, 1),
(3, 'Clientes', 16, 1),
(5, 'paletizador', 16, 0),
(6, 'Gerencia', 20, 1),
(7, 'Auxiliar Trazabilidad', 21, 1),
(8, 'Tesorería', 22, 0),
(9, 'Tesorería', 23, 0),
(10, 'Operador', 24, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semana`
--

CREATE TABLE `semana` (
  `id` int(11) NOT NULL,
  `nombre` int(11) NOT NULL,
  `id_anio` int(4) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `semana`
--

INSERT INTO `semana` (`id`, `nombre`, `id_anio`, `activo`) VALUES
(19, 2, 1, 0),
(20, 4, 1, 0),
(21, 20, 1, 0),
(22, 22, 1, 0),
(23, 21, 1, 1),
(24, 22, 1, 1),
(25, 20, 1, 1),
(26, 19, 1, 1),
(27, 18, 1, 1),
(28, 17, 1, 1),
(29, 16, 1, 1),
(30, 15, 1, 1),
(31, 14, 1, 1),
(32, 13, 1, 1),
(33, 12, 1, 1),
(34, 11, 1, 1),
(35, 10, 1, 1),
(36, 9, 1, 0),
(37, 9, 1, 1),
(38, 8, 1, 1),
(39, 7, 1, 1),
(40, 5, 1, 1),
(41, 4, 1, 1),
(42, 1, 1, 1),
(43, 2, 1, 1),
(44, 3, 1, 1),
(45, 6, 1, 1),
(46, 23, 1, 1),
(47, 24, 1, 1),
(48, 25, 1, 1),
(49, 26, 1, 1),
(50, 27, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subopcion`
--

CREATE TABLE `subopcion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `icono` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subopcion`
--

INSERT INTO `subopcion` (`id`, `nombre`, `icono`, `url`, `activo`) VALUES
(9, 'Lista', 'cog', 'home.container.lista', 1),
(10, 'Semanas', 'calendar', 'home.semana.lista_semana', 1),
(11, 'Clientes', 'users', 'home.mantenimiento.cliente', 1),
(13, 'Linea Naviera', 'ship', 'home.mantenimiento.linea_naviera', 1),
(14, 'Puerto Destino', 'anchor', 'home.mantenimiento.puerto_destino', 1),
(15, 'Operadores', 'user', 'home.mantenimiento.operador', 1),
(16, 'Usuario', 'user', 'home.usuario.usuario', 1),
(17, 'Roles', 'sitemap', 'home.usuario.rol', 1),
(18, 'Menú', 'list-ul', 'home.usuario.menu', 1),
(19, 'Cargo', 'vcard-o', 'home.mantenimiento.cargo', 1),
(20, 'Nave', 'train', 'home.mantenimiento.nave', 1),
(21, 'Estados', 'tags', 'home.mantenimiento.estado', 1),
(22, 'Contenedores Por Semana', 'file-excel-o', 'home.reporte.reporte_semana', 1),
(23, 'Contenedores Por Cliente', 'file-excel-o', 'home.reporte.contenedores_cliente', 1),
(24, 'Movimiento de Contenedores', 'file-excel-o', 'home.reporte.movimiento_contenedores', 1),
(25, 'Contenedores Por Cliente', 'bar-chart', 'home.grafico.grafico_cliente', 1),
(26, 'Contenedores Por Semana', 'bar-chart', 'home.grafico.grafico_semana', 1),
(27, 'Contenedores Por Año', 'bar-chart', 'home.grafico.grafico_anio', 1),
(28, 'Caja', 'inbox', 'home.mantenimiento.caja', 1),
(29, 'Mis Contenedores', 'list', 'home.container.cliente', 1),
(30, 'Pais', 'flag', 'home.mantenimiento.pais', 1),
(31, 'Ciudad', 'institution', 'home.mantenimiento.ciudad', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `id_cliente_operador` int(11) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nick`, `password`, `id_persona`, `id_rol`, `id_cliente_operador`, `remember_token`, `activo`) VALUES
(2, '20484062031', '$2y$10$QLduX8C9SFiE.QyCObyMGet8y8dwP2eibs8oe2ZJpA3TT49Z6gtsq', 6, 1, NULL, 'DZx0SRq5KloIWOdR7MPwfDZuNXVmnyr5wKeikIrxRoY2G9sx70M2ynEPxjXZ', 1),
(4, '40021803', '$2y$10$XprjHwr7bHnTuqJCS18bzuwI8TpHLwXUh913k8uBYJVXfCDxcd39u', 8, 2, NULL, 'DEqbpq85k3ZjM1RfOPApzzf6X9mcHjlhPDcS5waQSmKYtY99AW45Y9ZKVepV', 1),
(10, 'agrofair', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 21, 3, NULL, 'q0rWasi7HvbptIUnqNNlYzbfEITlMOnWqsGGqmfiZLDyNlj70XY4Byt6yNOl', 1),
(11, 'biodynamiska', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 22, 3, NULL, 'MOsOg5bAZAwG4LesiqaFRBV3I3jHfHxRVfRV1RzxCdLCKvdj0hX04DoAoypC', 1),
(12, 'dole', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 23, 3, NULL, 'fouUVNJgV5R3YI3O5VnSNmg3MsCou23spW4dBHtKXu9tLPM3lqbkFajeGLbo', 1),
(13, 'transastra', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 24, 3, NULL, NULL, 1),
(14, 'equifruit', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 25, 3, NULL, NULL, 1),
(16, '42262238', '$2y$10$i6mvzD0R2Spqmgh0hiM1GuDyvGt.g0ZhcN88zDJjK1NdDhS9NbiSu', 27, 7, NULL, '47Gt6aRKaBAx76EoYgjUV68qR3hDTEOHQqk6nafOoMaXge752PRxO1QXqqhA', 1),
(17, '42661236', '$2y$10$Vpah7zygI9rECUQm94vOuudUrfieTYJuUT5xyz/OnlJR2ZoCwrQTO', 28, 7, NULL, NULL, 1),
(24, 'darwinsac', '$2y$10$Y89HR4/Rlh283EfHKTut3uTz8sUWhw/CdaiHjorkgXbayBtnrhgiG', 38, 10, NULL, NULL, 0),
(25, 'afetransportation123', '$2y$10$fQm/D9NnzNzMRQ/Gq9m/EudLeYU4KDCXcWhgV8pZ1J9PSfDf7r4OW', 39, 10, NULL, NULL, 1),
(26, 'blueexpress', '$2y$10$hCHNUIx.JMQtROCE1/rx7.5DCTWXmRA3m9R8OWrxsmWH1T3BWbjeu', 40, 10, NULL, 'M8exlHdPds8bmaQllapO9WSw75K5dbTZ8KqbkeEXkG1epPJe6uIZDiZM9q07', 1),
(27, 'jeniff', '$2y$10$st1NZLYvoADlaqmtMKn.f.vXRIurxR7gIJulIbb1a7YWM1TyMLOva', 41, 10, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valija`
--

CREATE TABLE `valija` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anio`
--
ALTER TABLE `anio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_caja`
--
ALTER TABLE `audit_caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_cargo`
--
ALTER TABLE `audit_cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_ciudad`
--
ALTER TABLE `audit_ciudad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_cliente`
--
ALTER TABLE `audit_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_contenedor`
--
ALTER TABLE `audit_contenedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_estado`
--
ALTER TABLE `audit_estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_linea_naviera`
--
ALTER TABLE `audit_linea_naviera`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_nave`
--
ALTER TABLE `audit_nave`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_operador`
--
ALTER TABLE `audit_operador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_pais`
--
ALTER TABLE `audit_pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_puerto_destino`
--
ALTER TABLE `audit_puerto_destino`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_semana`
--
ALTER TABLE `audit_semana`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_usuario`
--
ALTER TABLE `audit_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caja_contenedor`
--
ALTER TABLE `caja_contenedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contendor_id` (`contenedor_id`),
  ADD KEY `caja_id` (`caja_id`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `certificado`
--
ALTER TABLE `certificado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente_certificado` (`id_cliente`),
  ADD KEY `id_semana_certificado` (`id_semana`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pais_id` (`id_pais`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenedor`
--
ALTER TABLE `contenedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_booking` (`booking`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_lineanaviera` (`id_lineanaviera`),
  ADD KEY `id_puertodestino` (`id_puertodestino`),
  ADD KEY `id_operador` (`id_operador`),
  ADD KEY `id_semana` (`id_semana`),
  ADD KEY `id_nave` (`nave`);

--
-- Indices de la tabla `contenedor_valija`
--
ALTER TABLE `contenedor_valija`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contenedor_id` (`contenedor_id`),
  ADD KEY `valija_id` (`valija_id`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente_factura` (`id_cliente`),
  ADD KEY `id_semana_factura` (`id_semana`);

--
-- Indices de la tabla `icono`
--
ALTER TABLE `icono`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `linea_naviera`
--
ALTER TABLE `linea_naviera`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu_opcion`
--
ALTER TABLE `menu_opcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `opcionm_id` (`opcion_id`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_contenedor` (`id_contenedor`),
  ADD KEY `id_estado` (`id_estado`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `nave`
--
ALTER TABLE `nave`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `opcion`
--
ALTER TABLE `opcion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `opcion_subopcion`
--
ALTER TABLE `opcion_subopcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `opcion_id` (`opcion_id`),
  ADD KEY `subopcion_id` (`subopcion_id`);

--
-- Indices de la tabla `operador`
--
ALTER TABLE `operador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `packing`
--
ALTER TABLE `packing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_contenedor_packing` (`id_contenedor`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permiso_rol`
--
ALTER TABLE `permiso_rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rol_id` (`rol_id`),
  ADD KEY `permiso_id` (`permiso_id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cargo` (`id_cargo`);

--
-- Indices de la tabla `puerto_destino`
--
ALTER TABLE `puerto_destino`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ciudad_id` (`id_ciudad`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_menu` (`id_menu`);

--
-- Indices de la tabla `semana`
--
ALTER TABLE `semana`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_anio` (`id_anio`);

--
-- Indices de la tabla `subopcion`
--
ALTER TABLE `subopcion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_persona` (`id_persona`),
  ADD KEY `id_rol` (`id_rol`);

--
-- Indices de la tabla `valija`
--
ALTER TABLE `valija`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente_valija` (`id_cliente`),
  ADD KEY `id_semana_valija` (`id_semana`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anio`
--
ALTER TABLE `anio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `audit_caja`
--
ALTER TABLE `audit_caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_cargo`
--
ALTER TABLE `audit_cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `audit_ciudad`
--
ALTER TABLE `audit_ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_cliente`
--
ALTER TABLE `audit_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `audit_contenedor`
--
ALTER TABLE `audit_contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT de la tabla `audit_estado`
--
ALTER TABLE `audit_estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_linea_naviera`
--
ALTER TABLE `audit_linea_naviera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_nave`
--
ALTER TABLE `audit_nave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_operador`
--
ALTER TABLE `audit_operador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `audit_pais`
--
ALTER TABLE `audit_pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_puerto_destino`
--
ALTER TABLE `audit_puerto_destino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `audit_semana`
--
ALTER TABLE `audit_semana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT de la tabla `audit_usuario`
--
ALTER TABLE `audit_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `caja_contenedor`
--
ALTER TABLE `caja_contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=762;
--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `certificado`
--
ALTER TABLE `certificado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `contenedor`
--
ALTER TABLE `contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=561;
--
-- AUTO_INCREMENT de la tabla `contenedor_valija`
--
ALTER TABLE `contenedor_valija`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `linea_naviera`
--
ALTER TABLE `linea_naviera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `menu_opcion`
--
ALTER TABLE `menu_opcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1213;
--
-- AUTO_INCREMENT de la tabla `nave`
--
ALTER TABLE `nave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT de la tabla `opcion`
--
ALTER TABLE `opcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `opcion_subopcion`
--
ALTER TABLE `opcion_subopcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `operador`
--
ALTER TABLE `operador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `packing`
--
ALTER TABLE `packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `permiso_rol`
--
ALTER TABLE `permiso_rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT de la tabla `puerto_destino`
--
ALTER TABLE `puerto_destino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `semana`
--
ALTER TABLE `semana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT de la tabla `subopcion`
--
ALTER TABLE `subopcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `valija`
--
ALTER TABLE `valija`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `caja_contenedor`
--
ALTER TABLE `caja_contenedor`
  ADD CONSTRAINT `caja_id` FOREIGN KEY (`caja_id`) REFERENCES `caja` (`id`),
  ADD CONSTRAINT `contendor_id` FOREIGN KEY (`contenedor_id`) REFERENCES `contenedor` (`id`);

--
-- Filtros para la tabla `certificado`
--
ALTER TABLE `certificado`
  ADD CONSTRAINT `id_cliente_certificado` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_semana_certificado` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `pais_id` FOREIGN KEY (`id_pais`) REFERENCES `pais` (`id`);

--
-- Filtros para la tabla `contenedor`
--
ALTER TABLE `contenedor`
  ADD CONSTRAINT `id_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_lineanaviera` FOREIGN KEY (`id_lineanaviera`) REFERENCES `linea_naviera` (`id`),
  ADD CONSTRAINT `id_operador` FOREIGN KEY (`id_operador`) REFERENCES `operador` (`id`),
  ADD CONSTRAINT `id_puertodestino` FOREIGN KEY (`id_puertodestino`) REFERENCES `puerto_destino` (`id`),
  ADD CONSTRAINT `id_semana` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

--
-- Filtros para la tabla `contenedor_valija`
--
ALTER TABLE `contenedor_valija`
  ADD CONSTRAINT `contenedor_id` FOREIGN KEY (`contenedor_id`) REFERENCES `contenedor` (`id`),
  ADD CONSTRAINT `valija_id` FOREIGN KEY (`valija_id`) REFERENCES `valija` (`id`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `id_cliente_factura` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_semana_factura` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

--
-- Filtros para la tabla `menu_opcion`
--
ALTER TABLE `menu_opcion`
  ADD CONSTRAINT `menu_id` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `opcionm_id` FOREIGN KEY (`opcion_id`) REFERENCES `opcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD CONSTRAINT `id_contenedor` FOREIGN KEY (`id_contenedor`) REFERENCES `contenedor` (`id`),
  ADD CONSTRAINT `id_estado` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id`),
  ADD CONSTRAINT `id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `opcion_subopcion`
--
ALTER TABLE `opcion_subopcion`
  ADD CONSTRAINT `opcion_id` FOREIGN KEY (`opcion_id`) REFERENCES `opcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subopcion_id` FOREIGN KEY (`subopcion_id`) REFERENCES `subopcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `packing`
--
ALTER TABLE `packing`
  ADD CONSTRAINT `id_contenedor_packing` FOREIGN KEY (`id_contenedor`) REFERENCES `contenedor` (`id`);

--
-- Filtros para la tabla `permiso_rol`
--
ALTER TABLE `permiso_rol`
  ADD CONSTRAINT `permiso_id` FOREIGN KEY (`permiso_id`) REFERENCES `permiso` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rol_id` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `id_cargo` FOREIGN KEY (`id_cargo`) REFERENCES `cargo` (`id`);

--
-- Filtros para la tabla `puerto_destino`
--
ALTER TABLE `puerto_destino`
  ADD CONSTRAINT `ciudad_id` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudad` (`id`);

--
-- Filtros para la tabla `rol`
--
ALTER TABLE `rol`
  ADD CONSTRAINT `id_menu` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `semana`
--
ALTER TABLE `semana`
  ADD CONSTRAINT `id_anio` FOREIGN KEY (`id_anio`) REFERENCES `anio` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `id_persona` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `valija`
--
ALTER TABLE `valija`
  ADD CONSTRAINT `id_cliente_valija` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_semana_valija` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
