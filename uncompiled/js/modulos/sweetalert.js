(function(){
	'use strict';
	angular.module('ng-SweetAlert',[])
	.factory('$sweetalert',$sweetalert)
	.service('sSweetalert',sSweetalert);

	function $sweetalert(sSweetalert){
		return{

			open : function(res){
				return sSweetalert.open(res);
			},
            confirm : function(res){
                return sSweetalert.confirm(res);
            }

		}
	}

	function sSweetalert($crud){
		var self = this;

        self.message = 'This message is Empty!';

        this.open = function (res) {
            if (res.data.success) {
                if (!angular.isUndefined(res.data.message)) {
                    swal("Éxito!", res.data.message, "success");
                } else {
                    swal("Éxito!", 'Todo correcto!', "success");
                }
                return true;
            } else {
                if (angular.isUndefined(res.data.messages)) {
                    swal("Advertencia!", res.data.message, "warning");
                } else {
                    var cadena = '';
                    angular.forEach(res.data.messages, function (p, k) {
                        cadena += p + ' \n ';
                    });
                    swal("Advertencia!", cadena, "warning");
                }

                return false;
            }
        };	

        this.confirm = function(res){
            swal({
              title: "",
              text: "¿Desea Enviar a Correo?",
              type: "info",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Si",              
              cancelButtonText: "No",
              closeOnConfirm: false
            },
            res);
        }
	}

})()