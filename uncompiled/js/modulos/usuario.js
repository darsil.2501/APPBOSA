(function(){
    'use strict';
    angular.module('ng-Usuario',[])
    .factory('$usuario',$usuario)
    .service('sUsuario',sUsuario)
    .service('sCallsUsuario',sCallsUsuario)
    .service('sSession',sSession);

    function $usuario(sUsuario){
        return{

            userLogin : function(usuario){
                return sUsuario.login(usuario);
            },
            userLogout: function () {
                return sUsuario.logout();
            }

        }
    }

    function sUsuario(sCallsUsuario,$q,sSession,$location,sPermiso){
        var defer;

        this.login = function (usuario) {
            defer = $q.defer();
            sCallsUsuario.callLogin(usuario).then(
                    function (res) {
                        if (res.status >= 200 && res.status <= 299) {
                            if (res.data.success) {
                                sSession.setVariable('data_menu',JSON.stringify(res.data.data_menu));
                                sSession.setVariable('data_persona',JSON.stringify(res.data.data_persona));
                                sSession.setVariable('data_user',JSON.stringify(res.data.data_usuario));
                                sSession.setVariable('permisos',JSON.stringify(res.data.permisos));
                                sSession.setVariable('data_subopciones',JSON.stringify(res.data.data_subopciones));
                                sPermiso.arrayPermisos = angular.fromJson(sSession.getVariable('permisos'));
                                sPermiso.arraySubopciones = angular.fromJson(sSession.getVariable('data_subopciones'));
                                $location.url('admin/dashboard');
                            }
                        } else if (res.status >= 400) {
                            //console.log('Error en la petición.');
                        }
                        defer.resolve(res);
                    }
            );
            return defer.promise;
        }; 

        this.logout = function(){
           var key;
            for (key in sSession.getSessionStorage()) {
                sSession.destroyVariable(key);
            }
            sCallsUsuario.callLogout().then(
                    function (res) {
                        if (res.status >= 200 && res.status <= 299) {
                            $location.url('api/login');
                        } else if (res.status >= 400) {
                            //console.log('Error en la petición.');
                        }
                    }
            );
        }  
    }

    function sCallsUsuario($http){
        this.callLogin = function(usuario){
            return $http.post('../Server/public/index.php/usuario/auth',JSON.stringify(usuario));
        };

        this.callLogout = function () {
            return $http.get('../Server/public/index.php/usuario/logout');
        };    
    }

    function sSession() {

        this.getVariable = function (name) {
            return sessionStorage.getItem(name);
        };

        this.setVariable = function (name, param) {
            return sessionStorage.setItem(name, param);
        };

        this.destroyVariable = function (name) {
            return sessionStorage.removeItem(name);
        };

        this.getSessionStorage = function () {
            return sessionStorage;

        };
    }

})()