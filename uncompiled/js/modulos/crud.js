(function(){
	'use strict';
	angular.module('ng-Crud',[])
	.factory('$crud',$crud)
	.service('sCrud',scrud);

	function $crud(sCrud){
		return{

			list : function(route){
				return sCrud.list(route);
			},

			listEspecial : function(route){
				return sCrud.listEspecial(route);
			},

			edit : function(route,data){
				return sCrud.edit(route,data);
			},
			create : function(route,data){
				return sCrud.create(route,data);
			},
			delete : function(route,id){
				return sCrud.delete(route,id);
			},
			show : function(route,id){
				return sCrud.show(route,id);
			},
			especialId : function(route,id){
				return sCrud.especialId(route,id);
			},
			especialPost : function(route,data){
				return sCrud.especialPost(route,data);
			},
			deleteObj : function(route,data){
				return sCrud.deleteObj(route,data);
			}

		}
	}

	function scrud($http){
		this.list = function(route){
			return $http.get('../Server/public/index.php/'+route+'/getdata');
		};

		this.listEspecial = function(route){
			return $http.get('../Server/public/index.php/'+route);
		};

		this.edit = function(route,data){
			return $http.post('../Server/public/index.php/'+route+'/update/'+data.id,JSON.stringify(data));
		};

		this.create = function(route,data){
			return $http.post('../Server/public/index.php/'+route+'/create',JSON.stringify(data));
		};

		this.delete = function(route,id){
			return $http.delete('../Server/public/index.php/'+route+'/delete/'+id);
		};

		this.show = function(route,id){
			return $http.post('../Server/public/index.php/'+route+'/show/'+id);
		};

		this.especialId = function(route,id){
			return $http.get('../Server/public/index.php/'+route+'/'+id);
		};

		this.especialPost = function(route,data){
			return $http.post('../Server/public/index.php/'+route,JSON.stringify(data));
		};

		this.deleteObj = function(route,data){
			return $http.post('../Server/public/index.php/'+route,JSON.stringify(data));
		};
	}

})()