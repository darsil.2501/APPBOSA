/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function () {

    'use strict';

    angular.module('ngGraphics', [
    ])
            .factory('$graphics', $graphics)
            .service('sGraphics', sGraphics);

    function $graphics(sGraphics) {
        return {
            graphImage: function (route) {
                return sGraphics.read(route);
            },
            graphImageId: function (route,valor) {
                return sGraphics.readwithId(route,valor);
            },
            graphImageData: function (route , data) {
                return sGraphics.readwithData(route , data);
            }
        };
    }

    function sGraphics($http) {

        this.read = function (route) {
            return $http.get('../Server/public/index.php/'+ route);
        };
        this.readwithId = function (route,valor) {
            return $http.get('../Server/public/index.php/'+ route+'/'+valor);
        };
        this.readwithData = function(route , data){
            return $http.post('../Server/public/index.php/' + route , angular.toJson(data));
        }
    }
})();