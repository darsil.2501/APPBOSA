(function () {

    'use strict';

    angular.module('app')
            .factory('$generic', $generic)
            .service('sGeneric', sGeneric);

            function $generic(sGeneric){
                return{

                    filterDate : function(date){
                        return sGeneric.filterDate(date);
                    },
                    validatePassword : function(obj){
                        return sGeneric.compareString(obj);
                    },
                    addVariable : function(name,value){
                        sGeneric.addVariable(name,value);
                    },
                    getVariable : function(name){
                       return sGeneric.getVariable(name);
                    },
                    getSesion : function(){
                        return sGeneric.getSesion();
                    },
                    uploadFile : function(route,data){
                        return sGeneric.uploadFile(route,data);
                    },

                    // Para descargar Zip
                    descargarZip: function(route,id)
                    {
                        return sGeneric.descargarZip(route,id);
                    }

                }
            }

            function sGeneric($filter,sSession,$q,$http) {

                this.filterDate = function (date) {
                    return $filter('date')(date, 'yyyy/MM/dd');
                };

                this.compareString = function(obj){
                    return (obj.password_nuevo === obj.password_repeat);
                };

                this.addVariable = function(name,value){
                    sSession.setVariable(name,value);
                };

                this.getVariable = function(name){
                    return sSession.getVariable(name);
                };

                this.getSesion = function(){
                    return sSession.getSessionStorage();
                }

                this.uploadFile = function(route,data){
                    var deferred = $q.defer();
                    var formdata = new FormData();
                    angular.forEach(data.flow.files, function (value, key) {
                        formdata.append(key, value.file);
                    });

                    var request = {
                        method: 'POST',
                        url: 'http://localhost/appbosa/Server/public/index.php/'+route+'/create/'+data.id,
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    
                    return $http(request)
                    .success(function (res) {
                        deferred.resolve(res);
                    })
                    .error(function (msg, code) {
                        deferred.reject(msg);
                    });
                    return deferred.promise;
                };

                // para descargar zip
                this.descargarZip = function(route,id){
                    return $http({
                        url : '../Server/public/index.php/'+route+'/' + id,
                        method : 'POST',
                        params : id,
                        headers : {
                            'Content-type' : 'application/octet-stream',
                        },
                        responseType : 'arraybuffer'
                    }).success(function(data, status, headers, config) {
                        var file = new Blob([ data ], {
                            type : 'application/octet-stream'
                        });
                        //trick to download store a file having its URL
                        var fileURL = URL.createObjectURL(file);
                        var a         = document.createElement('a');
                        a.href        = fileURL; 
                        a.target      = '_blank';
                        a.download    = 'consolidado.zip';
                        // document.body.appendChild(a);
                        a.click();
                    }).error(function(data, status, headers, config) {
                        
                    });
                };

            }
})();