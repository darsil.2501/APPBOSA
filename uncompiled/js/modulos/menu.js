(function(){
	'use strict';
	angular.module('ng-Menu',[])
	.factory('$menu',$menu)
	.service('sMenu',sMenu);

	function $menu(sMenu){
		return{

			asignar : function(route,data){
				return sMenu.asignar(route,data);
			},
			quitar : function(route,data){
				return sMenu.quitar(route,data);
			},
			especialId : function(route,id){
				return sMenu.especialId(route,id);
			}

		}
	}

	function sMenu($http){
		this.asignar = function(route,data){
			return $http.post('../Server/public/index.php/'+route,JSON.stringify(data));
		};

		this.quitar = function(route,data){
			return $http.post('../Server/public/index.php/'+route,JSON.stringify(data));
		};

		this.especialId = function(route,id){
			return $http.get('../Server/public/index.php/'+route+'/'+id);
		};
	}

})()