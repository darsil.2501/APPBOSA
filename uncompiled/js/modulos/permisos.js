(function(){
	'use strict';
	angular.module('ng-Permiso',['ng-Usuario'])
	.factory('$permiso',$permiso)
	.service('sPermiso',sPermiso);

	function $permiso(sPermiso){
		return{

			validate : function(param){
				return sPermiso.validate(param);
			},
			validateRoute : function(route){
				return sPermiso.validateRoute(route);
			} 
		}
	}

	function sPermiso(sSession,$filter){

		this.arrayPermisos = angular.fromJson(sSession.getVariable('permisos'));

		this.arraySubopciones = angular.fromJson(sSession.getVariable('data_subopciones'));

		this.validate = function(param){
			return (this.arrayPermisos.includes(param));
		};	

        this.validateRoute = function(route){
        	return (this.arraySubopciones.includes(route));            
        };
	}

})()