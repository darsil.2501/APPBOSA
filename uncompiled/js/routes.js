(function(){
	'use strict';
	angular.module('app')
	
	.config(mainConfig)
	.run(mainRun);

	 
	function mainConfig($stateProvider, $urlRouterProvider) {

		$stateProvider

		//app core pages (errors, login,signup)
		.state('api', {
		  abstract: true,
		  url: '/api',
		  template: '<div ui-view></div>'
		})
		//login
		.state('api.login', {
		  url: '/login',
		  controller: 'LoginCtrl',
		  templateUrl: 'views/login/login.html',
		  resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
		    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
		      // you can lazy load files for an existing module
		             return $ocLazyLoad.load('js/controllers/login/CtrlLogin.js');
		    }]
		  }
		})

		.state('admin', {
		  abstract: true,
		  url: '/admin',
		  controller: 'HomeCtrl',
		  templateUrl: 'views/admin/admin.html',
		  resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
		    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
		        // you can lazy load files for an existing module
		        return $ocLazyLoad.load('js/controllers/admin/CtrlHome.js');

		    }],
		    security: ['$q','$generic', function($q,$generic){
                var menu = $generic.getSesion().length;
                if(menu === 0){
                    return $q.reject("Not Authorized");
                }
           }]
		  }
		})
		.state('admin.dashboard',{
			url: '/dashboard',
			templateUrl: 'views/admin/others/dashboard.html',
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/others/CtrlDashboard.js');
				}]
			}
		})
		.state('admin.perfil',{
			url: '/perfil',
			templateUrl: 'views/admin/others/perfil.html',
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/others/CtrlPerfil.js');
				}]
			}
		})
		.state('admin.container', {
		  abstract: true,
		  url: '/container',
		  template: '<div ui-view></div>'
		})
		.state('admin.container.lista', {
			url: '/lista',
			templateUrl: 'views/admin/container/container.html',
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/container/CtrlContenedor.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.container.lista');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.container.detalle', {
			url: '/detalle',
			templateUrl: 'views/admin/container/detalle.html',
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/container/CtrlDetalle.js');
				}]
			}
        })
        // contenedores de los clientes
        .state('admin.container.cliente', {
			url: '/containers/cliente',
			templateUrl: 'views/admin/container/containers_cliente.html',
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/container/CtrlContainerCliente.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.container.cliente');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
		// contenedores de los operadores
        .state('admin.container.operador', {
			url: '/containers/operador',
			templateUrl: 'views/admin/container/containers_operador.html',
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/container/CtrlContainerOperador.js');
				}],
			    // security: ['$q','$permiso', function($q,$permiso){
	      //           var menu = $permiso.validateRoute('admin.container.operador');
	      //           if(!menu){
	      //           	return $q.reject("Not Authorized");
	      //           }
	      //      }]
			}
        })
        //lista de archivos por operador
        .state('admin.container.my_files_o', {
			url: '/my_files_o',
			templateUrl: 'views/admin/container/file_operador.html',
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/container/CtrlFileOperador.js');
				}],
			    // security: ['$q','$permiso', function($q,$permiso){
	      //           var menu = $permiso.validateRoute('admin.container.my_files_o');
	      //           if(!menu){
	      //               return $q.reject("Not Authorized");
	      //           }
	      //      }]
			}
        })
        //lista de archivos por cliente
        .state('admin.container.my_files_c', {
			url: '/my_files_c',
			templateUrl: 'views/admin/container/file_client.html',
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/container/CtrlFileClient.js');
				}],
			    // security: ['$q','$permiso', function($q,$permiso){
	      //           var menu = $permiso.validateRoute('admin.container.my_files_c');
	      //           if(!menu){
	      //               return $q.reject("Not Authorized");
	      //           }
	      //      }]
			}
        })
        //semana
        .state('admin.semana', {
		  abstract: true,
		  url: '/semana',
		  template: '<div ui-view></div>'
		})
        .state('admin.semana.lista_semana',{
            url: '/lista_semana',
            templateUrl: 'views/admin/semana/lista_semana.html',
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/semana/CtrlListaSemana.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.semana.lista_semana');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.semana.upload',{
            url: '/upload',
            templateUrl: 'views/admin/semana/upload_files.html',
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/semana/CtrlUploadFile.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.semana.lista_semana');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.semana.detalle_semana',{
            url: '/detalle_semana',
            templateUrl: 'views/admin/semana/detalle_semana.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/semana/CtrlDetalleSemana.js');
				}]
			}
        })
        //mantenimientos
        .state('admin.mantenimiento', {
		  abstract: true,
		  url: '/mantenimiento',
		  template: '<div ui-view></div>'
		})
        .state('admin.mantenimiento.cliente', {
			url: '/cliente',
			templateUrl: 'views/admin/mantenimiento/cliente.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlCliente.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.cliente');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.mantenimiento.anio', {
			url: '/anio',
			templateUrl: 'views/admin/mantenimiento/anio.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlAnio.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.anio');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.mantenimiento.caja', {
			url: '/caja',
			templateUrl: 'views/admin/mantenimiento/caja.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlCaja.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.caja');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.mantenimiento.linea_naviera', {
			url: '/linea_naviera',
			templateUrl: 'views/admin/mantenimiento/linea_naviera.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlLineaNaviera.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.linea_naviera');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.mantenimiento.operador', {
			url: '/operador',
			templateUrl: 'views/admin/mantenimiento/operador.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlOperador.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.operador');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.mantenimiento.puerto_destino', {
			url: '/puerto',
			templateUrl: 'views/admin/mantenimiento/puerto.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlPuerto.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.puerto_destino');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.mantenimiento.nave', {
            url: '/nave',
            templateUrl: 'views/admin/mantenimiento/nave.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlNave.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.nave');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.mantenimiento.estado', {
            url: '/estado',
            templateUrl: 'views/admin/mantenimiento/estado.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlEstado.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.estado');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.mantenimiento.cargo', {
            url: '/cargo',
            templateUrl: 'views/admin/mantenimiento/cargo.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlCargo.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.cargo');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.mantenimiento.pais', {
            url: '/pais',
            templateUrl: 'views/admin/mantenimiento/pais.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlPais.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.pais');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.mantenimiento.empacadora', {
            url: '/empacadora',
            templateUrl: 'views/admin/mantenimiento/empacadora.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlEmpacadora.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.empacadora');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })

        .state('admin.mantenimiento.sector', {
            url: '/sector',
            templateUrl: 'views/admin/mantenimiento/sector.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlSector.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.sector');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })

        .state('admin.mantenimiento.ciudad', {
            url: '/ciudad',
            templateUrl: 'views/admin/mantenimiento/ciudad.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/mantenimiento/CtrlCiudad.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.mantenimiento.ciudad');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        //usuarios
        .state('admin.usuario', {
		  abstract: true,
		  url: '/usuario',
		  template: '<div ui-view></div>'
		})
        .state('admin.usuario.usuario',{
        	url: '/usuario',
        	templateUrl:'views/admin/usuario/usuario.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/usuario/CtrlUsuario.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.usuario.usuario');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.usuario.configuracion',{
        	url: '/configuracion',
        	templateUrl:'views/admin/usuario/configuracion.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					// you can lazy load controllers
					return $ocLazyLoad.load({
					  files: ['js/controllers/admin/usuario/CtrlConfiguracion.js',
					  		  'js/controllers/admin/usuario/includes_rol/CtrlTab1.js',
					  		  'js/controllers/admin/usuario/includes_rol/CtrlTab2.js']
					});
				}],
			    // security: ['$q','$permiso', function($q,$permiso){
	      //           var menu = $permiso.validateRoute('admin.usuario.rol');
	      //           if(!menu){
	      //               return $q.reject("Not Authorized");
	      //           }
	      //      }]
			}
        })
        .state('admin.usuario.menu',{
        	url: '/menu',
        	templateUrl:'views/admin/usuario/menu.html',
        	resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					// you can lazy load controllers
					return $ocLazyLoad.load({
					  files: ['js/controllers/admin/usuario/CtrlMenu.js',
					  		  'js/controllers/admin/usuario/includes_menu/CtrlTab1.js',
					  		  'js/controllers/admin/usuario/includes_menu/CtrlTab2.js',
					  		  'js/controllers/admin/usuario/includes_menu/CtrlTab3.js']
					});
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.usuario.menu');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        //reporte
        .state('admin.reporte', {
		  abstract: true,
		  url: '/reporte',
		  template: '<div ui-view></div>'
		})
        .state('admin.reporte.reporte_semana',{
            url: '/reporte_semana',
            templateUrl:'views/admin/reportes/reportesemana.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/reportes/CtrlReporteSemana.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.reporte.reporte_semana');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.reporte.contenedores_cliente',{
            url: '/contenedores_cliente',
            templateUrl:'views/admin/reportes/reportecliente.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/reportes/CtrlReporteCliente.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.reporte.contenedores_cliente');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.reporte.contenedores_operador',{
            url: '/contenedores_operador',
            templateUrl:'views/admin/reportes/reporteoperador.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/reportes/CtrlReporteOperador.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.reporte.contenedores_operador');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.reporte.buscar_contenedor',{
            url: '/buscar_contenedor',
            templateUrl:'views/admin/reportes/buscar_contenedor.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/reportes/CtrlBuscarContenedor.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.reporte.buscar_contenedor');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.reporte.movimiento_contenedores',{
            url: '/movimiento_contenedores',
            templateUrl:'views/admin/reportes/reportecontenedor.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/reportes/CtrlMovimientoContenedor.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.reporte.movimiento_contenedores');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.reporte.cajas_exportadas',{
            url: '/cajas_exportadas',
            templateUrl:'views/admin/reportes/reporte_cajas_exportadas.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/reportes/CtrlReporteCajasExportadas.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.reporte.cajas_exportadas');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        //graficos
        .state('admin.grafico', {
		  abstract: true,
		  url: '/grafico',
		  template: '<div ui-view></div>'
		})
        .state('admin.grafico.grafico_cliente',{
            url: '/grafico_cliente',
            templateUrl:'views/admin/graficos/contenedorcliente.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/graficos/CtrlContenedorCliente.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.grafico.grafico_cliente');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.grafico.grafico_semana',{
            url: '/grafico_semana',
            templateUrl:'views/admin/graficos/contenedorsemana.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/graficos/CtrlContenedorSemana.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.grafico.grafico_semana');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        .state('admin.grafico.grafico_anio',{
            url: '/grafico_anio',
            templateUrl:'views/admin/graficos/contenedoranio.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/graficos/CtrlContenedorAnio.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.grafico.grafico_anio');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        // auditoria(reportes)
        .state('admin.auditoria', {
		  abstract: true,
		  url: '/auditoria',
		  template: '<div ui-view></div>'
		})
        .state('admin.auditoria.auditoria_reporte',{
            url: '/auditoria_reporte',
            templateUrl:'views/admin/auditoria/reporte_auditoria.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/auditoria/CtrlReporteAuditoria.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.auditoria.auditoria_reporte');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        //productors
        .state('admin.productor', {
		  abstract: true,
		  url: '/productor',
		  template: '<div ui-view></div>'
		})
        .state('admin.productor.productor',{
        	url: '/productor',
        	templateUrl:'views/admin/productor/productor.html',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
				  // you can lazy load files for an existing module
				         return $ocLazyLoad.load('js/controllers/admin/productor/CtrlProductor.js');
				}],
			    security: ['$q','$permiso', function($q,$permiso){
	                var menu = $permiso.validateRoute('admin.productor.productor');
	                if(!menu){
	                    return $q.reject("Not Authorized");
	                }
	           }]
			}
        })
        
		.state('admin.productor.parcela', {
	        url: '/parcela',
	        templateUrl: 'views/admin/productor/parcela/parcela.html',
	        resolve: {
	        	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
		        // you can lazy load files for an existing module
		        return $ocLazyLoad.load('js/controllers/admin/productor/parcela/CtrlParcela.js');
		        }],
	          // security: ['$q','$permiso', function($q,$permiso){
	          //     var menu = $permiso.validateRoute('admin.usuario.usuario');
	          //     if(!menu){
	          //         return $q.reject("Not Authorized");
	          //     }
	          // }]
	        }
	    })

	    .state('admin.productor.trabajador', {
	        url: '/trabajador',
	        templateUrl: 'views/admin/productor/trabajador/trabajador.html',
	        resolve: {
	        	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
		        // you can lazy load files for an existing module
		        return $ocLazyLoad.load('js/controllers/admin/productor/trabajador/CtrlTrabajador.js');
		        }],
	          // security: ['$q','$permiso', function($q,$permiso){
	          //     var menu = $permiso.validateRoute('admin.usuario.usuario');
	          //     if(!menu){
	          //         return $q.reject("Not Authorized");
	          //     }
	          // }]
	        }
	    });

     //    $urlRouterProvider.rule(function ($injector, $location) {
	    //    //what this function returns will be set as the $location.url
	    //    var flag = sessionStorage.getItem("data_menu");
	    //     if (!flag) {
	    //     	$location.url('api/login');
	    //     }
	    // });
	    $urlRouterProvider.otherwise('/api/login');
		
	}

	function mainRun($rootScope,$usuario,$templateCache){

		// $rootScope.$on('$viewContentLoaded', function() {
	 //      	$templateCache.removeAll();
	 //   	});

        $rootScope.$on('$stateChangeError', change);

        function change(e, toState, toParams, fromState, fromParams, error){
        	if(error === "Not Authorized"){
        		$usuario.userLogout();
            }
        }

    }

})()