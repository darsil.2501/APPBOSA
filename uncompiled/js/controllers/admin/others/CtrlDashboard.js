(function(){
	'use strict';
	angular.module('app')
	.controller('DashboardCtrl', DashboardCtrl);

	function DashboardCtrl($scope,$crud,$generic,$permiso){

		var vm = $scope;

		vm.persona = angular.fromJson($generic.getVariable('data_persona'));

		vm.imgs = [
			{id:1,url:'images/carousel/1.jpg',texto:'Exportadores líderes de Banano Orgánico en el Perú'},
			{id:2,url:'images/carousel/2.jpg',texto:'Modernización en los procesos'},
			{id:3,url:'images/carousel/3.jpg',texto:'Socios comprometidos'},
			{id:4,url:'images/carousel/4.jpg',texto:'Certificaciones de calidad'}
		];

  	}	

})()