(function(){
	'use strict';
	angular.module('app')
	.controller('PerfilCtrl', PerfilCtrl)
	.controller('ModalPerfilCtrl',ModalPerfilCtrl);

	function PerfilCtrl($scope,$crud,$generic,$uibModal,$permiso){

		var vm = $scope;

		vm.obj = {};
		vm.flagCrud = true;

		vm.list = function(){
			vm.persona = angular.fromJson($generic.getVariable('data_persona'));
			vm.user = angular.fromJson($generic.getVariable('data_user'));
		}

		vm.list();
		
		vm.load = function(data){
			vm.flagCrud = false;
			vm.obj = angular.copy(data);
		};

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = {};
		}

		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}

		vm.openModalPersona = function(event, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.flag = true;
			var modalInstance = $uibModal.open({
				templateUrl: 'perfil_persona.html',
				controller: 'ModalPerfilCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				$generic.addVariable('data_persona',JSON.stringify(obj));
				vm.list();
			}, function () {

			});
	    };

		vm.openModalUser = function(event,form, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.flag = false;
			vm.obj.form = form;
			var modalInstance = $uibModal.open({
				templateUrl: 'perfil_usuario.html',
				controller: 'ModalPerfilCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				$generic.addVariable('data_user',JSON.stringify(obj));
				vm.list();	
			}, function () {

			});
	    };

  	}	

  	function ModalPerfilCtrl($scope, $crud, $uibModalInstance, obj,$sweetalert,$generic){
		var vm = $scope;

		vm.obj = {};	
		vm.flagpass = true;
		vm.errorinput = {success: true};	
		vm.user = angular.fromJson($generic.getVariable('data_user'));

		vm.obj = obj;

		vm.validate = function(){
			if(obj.nick == vm.user.nick){
				vm.errorinput = {success: true,message:'nombre de usuario disponible'};
			}else{
				vm.errorinput = false;
				$crud.especialPost('cuenta/validar_nick/'+obj.nick,).then(res =>{
					if(res.data.success){
						vm.errorinput = (res.data);	
					}else{
						vm.errorinput = (res.data);	
					}
				});
			}
		}

		vm.ok = function () {
			obj = angular.copy(vm.obj);
			if(obj.flag){
				$crud.especialPost('cuenta/update_persona/'+obj.id,obj).then(res =>{
					if($sweetalert.open(res)){
						$uibModalInstance.close(obj);	
					}					
				});
			}
			else 
			{
				if (obj.form) {
					$crud.especialPost('cuenta/update_nick/'+obj.id,obj).then(res =>{
						if($sweetalert.open(res)){
							$uibModalInstance.close(obj);	
						}					
					});
				}
				else
				{
					vm.flagpass = ($generic.validatePassword(vm.obj));
					if(vm.flagpass){
						$crud.especialPost('cuenta/update_password/'+obj.id,obj).then(res =>{
							if($sweetalert.open(res)){
								$uibModalInstance.close(obj);	
							}					
						});
					}
				}
			}	      	
	    };

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}	

})()