(function(){
	'use strict';
	angular.module('app')
	.controller('HomeCtrl',HomeCtrl);

	function HomeCtrl($scope,$usuario,sSession){
		var vm = $scope;

		vm.setData = function(){
			// vm.menu = angular.fromJson(sSession.getVariable('data_menu'))[0];
			vm.menu = angular.fromJson(sSession.getVariable('data_menu'));
			vm.persona = angular.fromJson(sSession.getVariable('data_persona'));
			vm.permisos = angular.fromJson(sSession.getVariable('permisos'));
		}

		vm.setData();

		vm.logout = function(){
			$usuario.userLogout();
		};

	}

})()