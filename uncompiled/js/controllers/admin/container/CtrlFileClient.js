	(function(){
	'use strict';
	angular.module('app')
	.controller('FileClientCtrl', FileClientCtrl)
	.controller('ModalFileClientCtrl', ModalFileClientCtrl);

	function FileClientCtrl($scope,$crud,$sweetalert,$generic,$location,$permiso,$uibModal,$http,$q,$file,$reporte,Upload){

		var vm = $scope;

		vm.flag = true;
		vm.flagUpload = true;
		vm.progress = 0;
		vm.tipo = 1;
		vm.data = {};
		vm.obj = {};

		vm.usuario = angular.fromJson($generic.getVariable('data_user'));
		vm.id_semana = $generic.getVariable('id_semana');

		vm.listFiles = function(id){
			$crud.especialPost('cliente/withArchivos/'+vm.usuario.id_cliente_operador,{id_semana:id}).then(function(res){
				vm.files = res.data;
			});
		}
		vm.listOtros = function(id){
			$crud.show('otros_archivos',id).then(function(res){
				vm.otros_archivos = res.data.info;
				vm.flagLoading = false;
			});
		};


		if(vm.id_semana){
			vm.listFiles(vm.id_semana);
			vm.listOtros(vm.id_semana);
		}
		else{
			$location.url('home/container/containers/cliente');
		}
		

		// vm.listAnio = function(){
		// 	$crud.list('anio').then(function(res){
		// 		vm.anios = res.data.info;
		// 		vm.data.anio = vm.anios[0].id;
		// 		vm.listSemana(vm.data.anio);
		// 	});
		// };
		// vm.listAnio();

		vm.listCliente = function(){
			$crud.list('cliente').then(function(res){
				vm.clientes = res.data.info;
			});
		};
		vm.listCliente();

		// vm.listSemana = function(id_anio){
		// 	$crud.especialId ('anio/withSemana',id_anio).then(function(res){
		// 		vm.semanas = (res.data.info);
		// 		if (vm.semanas.length > 0) {
		// 			vm.id_semana = vm.semanas[0].id;	
		// 		}
		// 	});
		// };	

	    // vm.changeSemana = function(semana){
	    // 	vm.listFiles(semana);
	    // };

		vm.download = function(params,typeFile,route){
			if(!params.tipo){
				params.tipo = typeFile;
			}
			$file.download(params,route);
		};	

		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}

		vm.openLink = function(packing){
			vm.rutaPacking = 'https://view.officeapps.live.com/op/view.aspx?src=http://www.appbosa.com.pe/Server/public/archivos/packing/'+packing.url;
			var win = window.open(vm.rutaPacking, '_blank');
  			win.focus();
	    };

		vm.openModalPreview = function(event, obj,type) {
	    	obj.tipo = type;

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_preview.html',
				controller: 'ModalFileClientCtrl',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,cajas) {
						
			}, function () {
				
			});
	    };

  }	

  function ModalFileClientCtrl($scope, $crud, $uibModalInstance, obj,$file,$sweetalert, Upload,$timeout){
		var vm = $scope;

		vm.obj = {};

		// if(obj.id_cliente){
  // 			$crud.especialPost('contenedor/ofClienteAndSemana',obj).then(res => {
  // 				vm.contenedores = (res.data.info);
  // 			});
  // 		}

		if(obj){
  			vm.flagButton = true;
			vm.obj = obj;	
			switch(obj.tipo){
				case 1:
					vm.route = 'factura';
					break;
				case 2:
					vm.route = 'certificado';
					break;
				case 3:
					vm.route = 'valija';
					break;
			}

			vm.ruta = '../Server/public/archivos/'+vm.route+'/'+obj.url;
		}

		vm.select = function(){
          	$uibModalInstance.close(vm.contenedores);
        }

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };

	}	

})()