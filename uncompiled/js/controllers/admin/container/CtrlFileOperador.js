	(function(){
	'use strict';
	angular.module('app')
	.controller('FileOperadorCtrl', FileOperadorCtrl)
	.controller('ModalFileOperadorCtrl', ModalFileOperadorCtrl);

	function FileOperadorCtrl($scope,$crud,$sweetalert,$generic,$location,$permiso,$uibModal,$http,$q,$file,$reporte,Upload){

		var vm = $scope;

		vm.flag = true;
		vm.flagUpload = true;
		vm.progress = 0;
		vm.data = {};
		vm.obj = {};

		vm.usuario = angular.fromJson($generic.getVariable('data_user'));

		vm.id_semana = $generic.getVariable('id_semana');

		vm.listFiles = function(id){
			$crud.especialPost('operador/withArchivos/'+vm.usuario.id_cliente_operador,{id_semana:id}).then(function(res){
				vm.files = res.data;
			});
		}

		vm.listFiles(vm.id_semana);

		vm.listOtros = function(id){
			$crud.show('otros_archivos',id).then(function(res){
				vm.otros_archivos = res.data.info;
				vm.flagLoading = false;
			});
		};
		vm.listOtros(vm.id_semana);

		vm.openLink = function(packing){
			vm.rutaPacking = 'https://view.officeapps.live.com/op/view.aspx?src=http://www.appbosa.com.pe/Server/public/archivos/packing/'+packing.url;
			var win = window.open(vm.rutaPacking, '_blank');
  			win.focus();
	    };

		vm.listCliente = function(){
			$crud.list('cliente').then(function(res){
				vm.clientes = res.data.info;
			});
		};
		vm.listCliente();

		vm.download = function(params,typeFile,route){
			if(!params.tipo){
				params.tipo = typeFile;
			}
			$file.download(params,route);
		};	

		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}

		vm.upload = function(file){
			vm.flag = false;
			vm.tipo = file;
		}	

		vm.uploadFiles = function(files) {
        	vm.files = files;        	
    	}

    	vm.deleteItemArray = function(index){
	    	vm.filest = [];
			for (var i in vm.files) {
				if(i != index){
					vm.filest.push(vm.files[i]);
				}
			}
			vm.files = (vm.filest);
		}

    	vm.save = function(){
    		if (vm.files && vm.files.length) {
    			vm.flagUpload = false;
	            Upload.upload({
	                url: '../Server/public/index.php/valija/create',
	                data: {
	                	id_semana : vm.id_semana,
	                	id_cliente : vm.id_cliente,
	                    files: vm.files,
	                    contenedores : vm.contTemp
	                }
	            }).then(function (response) {
	                if($sweetalert.open(response)){
	                	vm.obj = {};
	                	vm.files = [];
	                	vm.id_cliente = null;
	                	vm.listFiles(vm.id_semana);
	                }
	                vm.flag = true;
	                vm.progress = 0;
	                vm.flagUpload = true;
	            }, function (response) {
	                if (response.status > 0) {
	                    $scope.errorMsg = response.status + ': ' + response.data;
	                }
	            }, function (evt) {
	                vm.progress = 
	                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
	            });
	        }
	    }

    	vm.openModalSelect = function(event) {
	    	vm.obj.id_cliente = vm.id_cliente;
	    	vm.obj.id_cliente_operador = vm.usuario.id_cliente_operador;
	    	vm.obj.id_semana = Number(vm.id_semana);
	    	//var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_select_containers.html',
				controller: 'ModalFileOperadorCtrl',
				size: null,
				backdropClass: 'splash splash-2 splash-ef-14',
				windowClass: 'splash splash-2 splash-ef-14',
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,crud) {
				vm.contTemp = [];
				for(var co in obj){
					if(obj[co].enable){
						vm.contTemp.push(obj[co]);
					}
				}
			}, function () {
				vm.listFiles();
			});			
	    };	

		vm.openModalPreview = function(event, obj,type) {
	    	obj.tipo = type;

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_preview.html',
				controller: 'ModalFileOperadorCtrl',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,cajas) {
						
			}, function () {
				
			});
	    };

	    vm.openModalDelete = function(event,obj,type,size) {
	    	obj.tipo = type;
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_delete_file.html',
				controller: 'ModalFileOperadorCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,cajas) {
						
			}, function () {
				vm.listFiles(vm.id_semana);
			});
	    };

	    vm.openModalSend = function(event,obj,params,size) {
	    	obj.params = params;
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_send_file.html',
				controller: 'ModalFileOperadorCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,cajas) {
						
			}, function () {
				vm.listFactura(vm.id_semana);
				vm.listCertificado(vm.id_semana);
				vm.listValija(vm.id_semana);
				vm.listPacking(vm.id_semana);
			});
	    };

  }	

  function ModalFileOperadorCtrl($scope, $crud, $uibModalInstance, obj,$file,$sweetalert, Upload,$timeout){
		var vm = $scope;

		vm.obj = {};

		if(obj.id_cliente){
  			$crud.especialPost('contenedor/ofClienteAndSemanaAndOperador',obj).then(res => {
  				vm.contenedores = (res.data.info);
  			});
  		}

		if(obj){
  			vm.flagButton = true;
			vm.obj = obj;	
			switch(obj.tipo){
				case 1:
					vm.route = 'factura';
					break;
				case 2:
					vm.route = 'certificado';
					break;
				case 3:
					vm.route = 'valija';
					break;
			}

			vm.ruta = '../Server/public/archivos/'+vm.route+'/'+obj.url;
		}

		vm.select = function(){
          	$uibModalInstance.close(vm.contenedores);
        }

        vm.flagSend = true;
		vm.sendEmail = function(){
			vm.flagSend = false;
			switch (obj.params) {
    			case 1:
    				vm.route1 = 'factura';
    				break;
    			case 2:
    				vm.route1 = 'certificado';
    				break;
    			case 3:
    				vm.route1 = 'valija';
    				break;
    			case 4:
    				vm.route1 = 'packing';
    				break;
    			case 5:
    				vm.route1 = 'vl';
    				break;
    		}

    		$crud.especialPost(vm.route1+'/enviar_email/'+obj.id).then(res=>{
    			$uibModalInstance.dismiss('cancel');
    			vm.flagSend = true;
    			$sweetalert.open(res);
    		});
		}

        vm.ok = function(){
        	vm.flagButton = false;
        	$file.destroy(vm.route,vm.obj.id).then(function(res){
        		if($sweetalert.open(res)){
					$uibModalInstance.dismiss('cancel');
				}
        		vm.flagButton = true;
        	});
        }

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };

	}	

})()