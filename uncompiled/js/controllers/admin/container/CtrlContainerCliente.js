	(function(){
	'use strict';
	angular.module('app')
	.controller('ContainerClienteCtrl', ContainerClienteCtrl)

	function ContainerClienteCtrl($scope,$crud,$sweetalert,$permiso,$generic,$location,toastr,$uibModal,$http,sSession){

		var vm = $scope;

		vm.flag = true;
        vm.flagLoading = true;
        vm.obj = {};
		vm.usuario  = angular.fromJson(sSession.getVariable('data_user'));
		vm.id_semana = $generic.getVariable('id_semana');


		vm.list = function(id){
			vm.flagLoading = true;
			$crud.especialPost('cliente/withContenedores/'+vm.usuario.id_cliente_operador,{id_semana:id}).then(function(res){
				if(res.data.message){
					$sweetalert.open(res);
				}

				if(res.data.info && res.data.info.length === 0){
					vm.flag = false;
				}
				else{
					vm.flag = true;
				}
        		vm.flagLoading = false;
				vm.containers = (res.data.info);
			});
		};

		vm.redirect = function(){
			$generic.addVariable('id_semana',vm.obj.id_semana);
			$generic.idSemana = vm.obj.id_semana;
		}

		vm.listAnio = function(){
			$crud.list('anio').then(function(res){
				vm.anios = res.data.info;
                vm.obj.anio = vm.anios[0].id;
                vm.listSemana(vm.obj.anio);
			});
		};
		vm.listAnio();

		vm.listSemana = function(id_anio){
			$crud.especialId ('anio/withSemana',id_anio).then(function(res){
				vm.semanas = (res.data.info);
				if(vm.semanas.length>0){
					if(!vm.id_semana){
                    	vm.obj.id_semana = vm.semanas[0].id;
                    	vm.list(vm.obj.id_semana);			
					}
		
					if(vm.id_semana){
						vm.list(vm.id_semana);
						vm.obj.id_semana = Number(angular.copy(vm.id_semana));
					}
                }
			});
		};	

	    vm.changeSemana = function(semana){
	    	vm.list(semana);
	    };

		vm.redirectDetalle = function(data){
			$generic.addVariable('infoContenedor',JSON.stringify(data));
			$location.url('home/container/detalle');
		};

  	}		

})()