	(function(){
	'use strict';
	angular.module('app')
	.controller('DetalleCtrl', DetalleCtrl)
	.controller('ModalUploadCtrl', ModalUploadCtrl);

	function DetalleCtrl($scope,$crud,$sweetalert,$generic,$location,$permiso,$uibModal,$http,$q,$file,$reporte){

		var vm = $scope;

		vm.flagCrud = true;
		vm.obj = {};
		vm.container = angular.fromJson($generic.getVariable('infoContenedor'));

		vm.listPaking = function(id){
			$crud.show('packing',id).then(function(res){
				vm.packings = res.data.info;
			});
		};

		// vm.listFactura = function(id){
		// 	$crud.show('factura',id).then(function(res){
		// 		vm.facturas = res.data.info;
		// 	});
		// };

		// vm.listCertificado = function(id){
		// 	$crud.show('certificado',id).then(function(res){
		// 		vm.certificados = res.data.info;
		// 	});
		// };

		if(vm.container){
			vm.listPaking(vm.container.id_contenedor);
			// vm.listFactura(vm.container.id_contenedor);
			// vm.listCertificado(vm.container.id_contenedor);
		}else{
			$location.url('home/aperturar_semana');
		}

		vm.load = function(file){
			vm.obj = angular.copy(file);
		};

		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}

		vm.download = function(params,typeFile){
			params.tipo = typeFile;
			$file.download(params);
		};

		vm.openModalPreview = function(event, size) {

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				templateUrl: 'modal_preview.html',
				controller: 'ModalUploadCtrl',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,cajas) {
						
			}, function () {
				
			});
	    };

	    vm.openModalDelete = function(event,id, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.route = id;
			var modalInstance = $uibModal.open({
				templateUrl: 'modal_delete_file.html',
				controller: 'ModalUploadCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,cajas) {
						
			}, function () {
				vm.listPaking(vm.container.id_contenedor);
				vm.listFactura(vm.container.id_contenedor);
				vm.listCertificado(vm.container.id_contenedor);
			});
	    };

		vm.openModalSave = function(event,id, size) {

			var options = angular.element(event.target).data('options');
			vm.container.route = id;
			var modalInstance = $uibModal.open({
				templateUrl: 'modal_upload.html',
				controller: 'ModalUploadCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.container;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
					
			}, function () {
				vm.listPaking(vm.container.id_contenedor);
				// vm.listFactura(vm.container.id_contenedor);
				// vm.listCertificado(vm.container.id_contenedor);
			});
	    };

	    // -----------Para descargar el consolidado de archivos de un contenedor---------------------
	    vm.loadDownload = false;
	    vm.descargarZip = function()
	    {
	    	vm.loadDownload = true;
	    	$generic.descargarZip('contenedor/descargarzip',vm.container.id_contenedor).then(function(){
	    		vm.loadDownload = false;
	    	});
	    }
	    // ------------------------------------------------------------------------------------------

	    // --------------- Para descargar el detalle del contenedor ---------------------------------
	    vm.loadDownloadDetalle = false;
	    vm.downloadDetalle = function()
	    {
	    	vm.loadDownloadDetalle = true;
	    	$reporte.excelDetalleContenedor('detalle_contenedor',vm.container.id_contenedor).then(function(){
	    		vm.loadDownloadDetalle = false;
	    	});
	    }
	    // ------------------------------------------------------------------------------------------
		

  }	

  function ModalUploadCtrl($scope, $crud, $uibModalInstance, obj,$file,$sweetalert, Upload,$timeout){
		var vm = $scope;

		vm.obj = {};

		vm.flagButton = true;
		vm.route = '';
		vm.picFile = [];

		if(obj){
			vm.obj = obj;
			vm.ruta = '../Server/public/archivos/factura/'+obj.url;
			
			switch(obj.route){
				case 1:
					vm.route = 'packing';
					break;
				case 2:
					vm.route = 'factura';
					break;
				case 3:
					vm.route = 'certificado';
					break;
			}
		}	


		vm.load = false;
        vm.uploadFiles = function (files) {
        	vm.flagButton = false;
        	vm.load = true;
        	if (files && files.length) {
	            Upload.upload({
	                url: '../Server/public/index.php/'+vm.route+'/create/'+obj.id_contenedor,
	                data: {
	                    files: files
	                }
	            }).then(function (response) {
	            	$timeout(function(){
	            		if($sweetalert.open(response)){
							$uibModalInstance.dismiss('cancel');
						}
		        		vm.flagButton = true;
		        		vm.load = false;
			        });
	                
	            }, function (response) {
	                if (response.status > 0) {
	                    vm.errorMsg = response.status + ': ' + response.data;
	                }
	            }, function (evt) {
	                vm.progress = 
	                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
	            });
	        }
        }

        vm.ok = function(){
        	vm.flagButton = false;
        	$file.destroy(vm.route,vm.obj.id).then(function(res){
        		if($sweetalert.open(res)){
					$uibModalInstance.dismiss('cancel');
				}
        		vm.flagButton = true;
        	});	
        }

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };

	    vm.deleteItemArray = function(files,index){
	    	vm.files = [];
			for (var i in files) {
				if(i != index){
					vm.files.push(files[i]);
				}
			}
			vm.picFile = (vm.files);
		}
	}	

})()