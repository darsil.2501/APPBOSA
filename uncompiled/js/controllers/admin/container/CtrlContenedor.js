	(function(){
	'use strict';
	angular.module('app')
	.controller('ContenedorCtrl', ContenedorCtrl);

	function ContenedorCtrl($scope,$crud,$sweetalert,$generic,$timeout){

		var vm = $scope;

		vm.flag = true;
		vm.obj = {};
		vm.semana = 1;

		vm.list = function(){
			$crud.show('contenedor',vm.obj.id_semana).then(function(res){
				vm.containers = (res.data.info);
				vm.flag = false;
			});
		};
		

		vm.listAnio = function(){
			$crud.list('anio').then(function(res){
				vm.anios = res.data.info;
				vm.obj.id_anio = vm.anios[0].id;
				vm.listSemana(vm.obj.id_anio);
			});
		};
		vm.listAnio();

		vm.listSemana = function(anio){
			$crud.especialId('anio/withSemana',anio).then(function(res){
				if(res.data.success){
					vm.semanas = res.data.info;
					vm.obj.id_semana = vm.semanas[0].id;
					vm.list();
				}
			});
		};
		
		vm.periodicQuery = function(){
            vm.stopped = $timeout(function(){
                vm.periodicQuery();
                vm.list();
            }, 10000);
        };

        vm.periodicQuery();

		vm.full = function(){
			 $('#panel_full').toggleClass('fullscreen');
		}
  }		

})()