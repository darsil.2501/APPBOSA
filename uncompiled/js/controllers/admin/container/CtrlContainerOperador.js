(function(){
    'use strict';
    angular.module('app')
    .controller('ContainerOperadorCtrl', ContainerOperadorCtrl)
    .controller('ModalContainerOperadorCtrl', ModalContainerOperadorCtrl);


    function ContainerOperadorCtrl($scope,$crud,$sweetalert,$permiso,$generic,$location,toastr,$uibModal){

        var vm = $scope;

        vm.flag = true;
        vm.flagLoading = true;
        vm.usuario  = angular.fromJson($generic.getVariable('data_user'));
        vm.obj = {};

        vm.id_semana_LS = $generic.getVariable('id_semana');


        vm.list = function(id){
            vm.flagLoading = true;
            $crud.especialPost('operador/withContenedores/'+vm.usuario.id_cliente_operador,{id_semana:id}).then(function(res){
                if(res.data.message){
                    $sweetalert.open(res);
                }

                if(res.data.info && res.data.info.length === 0){
                    vm.flag = false;
                }
                else{
                    vm.flag = true;
                }
                vm.flagLoading = false;
                vm.containers = (res.data.info);
            });
        };

        //vm.list();

        vm.listAnio = function(){
            $crud.list('anio').then(function(res){
                vm.anios = res.data.info;
                vm.id_anio = vm.anios[0].id;
                vm.listSemana(vm.id_anio);
            });
        };
        vm.listAnio();

        vm.listSemana = function(id_anio){
            $crud.especialId ('anio/withSemana',id_anio).then(function(res){
                vm.semanas = (res.data.info);
                if(vm.semanas.length>0){
                    if(!vm.id_semana_LS){
                        vm.id_semana = vm.semanas[0].id;
                        vm.list(vm.id_semana);          
                    }
        
                    if(vm.id_semana_LS){
                        vm.list(vm.id_semana_LS);
                        vm.id_semana = Number(angular.copy(vm.id_semana_LS));
                    }
                }
            });
        };	

        vm.redirect = function(){
            $generic.addVariable('id_semana',vm.id_semana);
        }

        vm.changeSemana = function(semana){
           vm.list(semana);
        };

        vm.load = function(data){
            vm.id_contenedor            = data.id_contenedor;
            data.fecha_proceso_inicio_t = new Date($generic.filterDate(data.dia_proceso_inicio));
            data.fecha_proceso_fin_t    = new Date($generic.filterDate(data.dia_proceso_fin));
            data.fecha_zarpe_t          = (data.dia_zarpe != null) ? new Date($generic.filterDate(data.dia_zarpe)) : null ;
            data.fecha_llegada_t        = (data.dia_llegada != null) ? new Date($generic.filterDate(data.dia_llegada)) : null ;
            vm.obj = angular.copy(data);
        }

        vm.redirectDetalle = function(data){
            $generic.addVariable('infoContenedor',JSON.stringify(data));
            $location.url('home/container/detalle');
        };

        vm.openModalSave = function(event) {
            var options = angular.element(event.target).data('options');
            var modalInstance = $uibModal.open({
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_container.html',
                controller: 'ModalContainerOperadorCtrl',
                size: 'lg',
                backdropClass: 'splash' + ' ' + options,
                windowClass: 'splash' + ' ' + options,
                resolve: {
                  obj: function () {
                    return vm.obj;
                  }
                }
            });

            modalInstance.result.then(function (obj,cajas) {
                     vm.list(vm.id_semana);   
            }, function () {
                
            });
        };

    }

    function ModalContainerOperadorCtrl($scope, $crud, $uibModalInstance,obj,$generic,$file,$sweetalert, Upload,$timeout){
        var vm = $scope;

        vm.obj = {};

        if(obj){
            vm.obj = obj;
        }

        vm.obj.num = 'San Miguel';
        vm.id_contenedor = obj.id_contenedor;
        vm.isSave = true;

        vm.listLineaNaviera = function(){
            $crud.list('linea_naviera').then(function(res){
                vm.lineas = (res.data.info);
            });
        };

        vm.listLineaNaviera();

        vm.listPuerto = function(){
            $crud.list('puerto_destino').then(function(res){
                vm.puertos = (res.data.info);
            });
        };

        vm.listPuerto();

        vm.ok = function(){
            vm.isSave = false;
            obj.fecha_llegada           = $generic.filterDate(obj.fecha_llegada_t);
            obj.fecha_zarpe             = $generic.filterDate(obj.fecha_zarpe_t);
            obj.fecha_proceso_inicio    = $generic.filterDate(obj.fecha_proceso_inicio_t);
            obj.fecha_proceso_fin       = $generic.filterDate(obj.fecha_proceso_fin_t);
            obj.id = vm.id_contenedor;
            $crud.especialPost('contenedor/update_operador/'+obj.id,obj).then(function(res){
                if($sweetalert.open(res)){
                    $uibModalInstance.close(obj);
                }
                vm.isSave = true;
            });
        }

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})()