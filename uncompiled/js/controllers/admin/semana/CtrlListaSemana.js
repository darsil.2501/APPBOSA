(function(){
	'use strict';
	angular.module('app')
	.controller('ListaSemanaCtrl', ListaSemanaCtrl)
	.controller('ModalListaCtrl',ModalListaCtrl);

	function ListaSemanaCtrl($scope,$crud,$generic,$sweetalert,$permiso,$location,$uibModal,$reporte){

		var vm = $scope;

		vm.flagCrud     = true;
		vm.flag  = true;
		vm.loadExportar = false;

		vm.obj = {};
		vm.obj.crud = 1;
		

		vm.list = function($anio_id){
			vm.flag  = true;
			$crud.especialId('semana/getdata',$anio_id).then(function(res){
				vm.semanas = (res.data.info);
				vm.flag = false;
			});
		};

		vm.listAnios = function(){
			$crud.listEspecial('anio/give_semanas').then(function(res){
				vm.anios = res.data.info;
				vm.obj.anio = (vm.anios[vm.anios.length-1].id);
				vm.list(vm.obj.anio);
			});
		};
		vm.listAnios();

		vm.load = function(data){
			vm.flagCrud = false;
			vm.obj = angular.copy(data);
		}

		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}		

		vm.delete = function(id){
			$crud.delete('semana',id).then(function(res){
				if($sweetalert.open(res)){
					vm.list();
				}
			});
		}

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = {};
		}

		vm.redirectContenedor = function(data){
			$generic.addVariable('id_semana',data.id_semana);
			$generic.idSemana = data.id_semana;
		}

		vm.openModalSave = function(event, size) {

			vm.obj.flagCrud = vm.flagCrud;
			vm.obj.id_anio = 1;
			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_open_week.html',
				controller: 'ModalListaCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
							
			}, function () {
				vm.list();
				vm.clear();
			});
	    };

	    vm.openModalDelete = function(event, size) {

	    	var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_week_delete.html',
				controller: 'ModalListaCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				vm.delete(obj.id_semana);
			}, function () {
				
			});
	    };

	    // para exportar a excel los contenedores de una semana
	    vm.exportarData = function(sem)
	    {
	    	sem.down = true;
	    	$reporte.excelId('contenedores',sem.id_semana).then(function(){
	    		sem.down = false;
	    	});
	    }
	    

	};

	function ModalListaCtrl($scope, $crud, $uibModalInstance, $sweetalert, obj){
		var vm = $scope;

		vm.obj = {};		

		vm.obj = obj;

		vm.semanasAnio = [];
		for (var i = 1; i < 54; i++) {
			vm.semanasAnio.push({'id':i,'nombre':i});
		}

		vm.listAnio = function(){
			$crud.list('anio').then(function(res){
				vm.anios = (res.data.info);
			});
		};

		vm.listAnio();

		vm.ok = function () {
	      	$uibModalInstance.close(vm.obj);
	    };

	    vm.createEdit = function(){
			if (vm.obj.flagCrud) {
				$crud.create('semana',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.dismiss('cancel');
					}
				});
			}
			else{
				$crud.edit('semana',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.dismiss('cancel');
					}
				});
			}
		}

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()