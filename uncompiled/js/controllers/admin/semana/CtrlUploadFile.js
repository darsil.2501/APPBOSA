	(function(){
	'use strict';
	angular.module('app')
	.controller('UploadFileCtrl', UploadFileCtrl)
	.controller('ModalUploadFileCtrl', ModalUploadFileCtrl);

	function UploadFileCtrl($scope,$crud,$sweetalert,$file,$permiso,$generic,$location,toastr,Upload,$uibModal){

		var vm = $scope;

		vm.flag = true;
		vm.flagUpload = true;
		vm.flagLoading = true;
		vm.progress = 0;
		vm.tipo = 1;
		vm.data = {};
		vm.obj = {};
		vm.id_semana = $generic.getVariable('id_semana');

		vm.listFactura = function(id){
			$crud.show('factura',id).then(function(res){
				vm.facturas = res.data.info;
				vm.flagLoading = false;
			});
		}

		vm.listFactura(vm.id_semana);

		vm.listCertificado = function(id){
			$crud.show('certificado',id).then(function(res){
				vm.certificados = res.data.info;
				vm.flagLoading = false;
			});
		};

		vm.listCertificado(vm.id_semana);

		vm.listPacking = function(id){
			$crud.show('packing',id).then(function(res){
				vm.packings = res.data.info;
				vm.flagLoading = false;
			});
		};

		vm.listPacking(vm.id_semana);

		vm.listVl = function(id){
			$crud.show('vl',id).then(function(res){
				vm.vls = res.data.info;
				vm.flagLoading = false;
			});
		};

		vm.listVl(vm.id_semana);

		vm.listValija = function(id){
			$crud.show('valija',id).then(function(res){
				vm.valijas = res.data.info;
				vm.flagLoading = false;
			});
		};
		vm.listValija(vm.id_semana);

		vm.listOtros = function(id){
			$crud.show('otros_archivos',id).then(function(res){
				vm.otros_archivos = res.data.info;
				vm.flagLoading = false;
			});
		};
		vm.listOtros(vm.id_semana);

		vm.listCliente = function(){
			$crud.list('cliente').then(function(res){
				vm.clientes = res.data.info;
			});
		};
		vm.listCliente();

		vm.listEstado = function(){
			$crud.list('estado_archivo').then(function(res){
				vm.estados = res.data.info;
			});
		};
		vm.listEstado();

		vm.listOperadores = function(id){
			$crud.especialPost('cliente/operadoresAsigned/'+id,{}).then(function(res){
				vm.operadores = (res.data.info);
			});
		};
		//vm.listOperadores();

		vm.changeEstado = function(certificado){
			var data = {
					'id_certificado' : certificado.id,
					'id_estado_archivo' : certificado.id_estado_archivo
				};
	    	$crud.deleteObj('certificado/cambiar_estado',data).then(function(res){
	    		if (res.data.success) {
	    			toastr.info(res.data.message);
	    		}else{
	    			toastr.error(res.data.message);
	    		}
	    		vm.listCertificado(vm.id_semana);
			});
		}

		vm.upload = function(file,so){
			vm.flag = false;
			vm.tipo = file;
			vm.showOperators = so;
		}

		vm.preview = function(data,event){
			if (data.tipo!=3) {
				vm.rutaOtros = 'https://view.officeapps.live.com/op/view.aspx?src=http://www.appbosa.com.pe/Server/public/archivos/otros_archivos/'+data.url;
				var win = window.open(vm.rutaOtros, '_blank');
				win.focus();
			}
			if (data.tipo===3) {
				vm.openModalPreview(event,data,6);
			}
		}

		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}

		vm.uploadFiles = function(files) {
        	vm.files = files;        	
    	}

    	vm.download = function(params,typeFile,route){
    		if(!params.tipo){
				params.tipo = typeFile;
			}
			$file.download(params,route);
		};

    	vm.deleteItemArray = function(index){
	    	vm.filest = [];
			for (var i in vm.files) {
				if(i != index){
					vm.filest.push(vm.files[i]);
				}
			}
			vm.files = (vm.filest);
		}

		vm.deleteContainerofValija = function(val,con){
			vm.data = {
				id_valija 	  : val.id,
				id_contenedor : con.id_contenedor
			}
			$crud.especialPost('valija/eliminar_contenedor',vm.data).then(res=>{
				if(res.data.success){
					toastr.success(res.data.message);
					vm.listValija(vm.id_semana);
				}
				else{
					toastr.error(res.data.message);
				}
			})
		}

		vm.deleteContainerofCertificado = function(cer,con){
			vm.data = {
				id_certificado : cer.id,
				id_contenedor  : con.id_contenedor
			}
			$crud.especialPost('certificado/eliminar_contenedor',vm.data).then(res=>{
				if(res.data.success){
					toastr.success(res.data.message);
					vm.listCertificado(vm.id_semana);
				}
				else{
					toastr.error(res.data.message);
				}
			})
		}

    	vm.save = function(){
    		//console.log(vm.files,vm.id_semana,vm.id_cliente);return;
    		switch (vm.tipo) {
    			case 1:
    				vm.route = 'factura';
    				break;
    			case 2:
    				vm.route = 'certificado';
    				break;
    			case 3:
    				vm.route = 'valija';
    				break;
    			case 4:
    				vm.route = 'packing';
    				break;
    			case 5:
    				vm.route = 'vl';
    				break;
    			case 6:
    				vm.route = 'otros_archivos';
    				break;
    		}
    		if (vm.files && vm.files.length) {
    			vm.flagUpload = false;
	            Upload.upload({
	                url: '../Server/public/index.php/'+vm.route+'/create',
	                data: {
	                	id_semana : vm.id_semana,
	                	id_cliente : vm.id_cliente,
	                	id_operador : vm.data.id_operador,
	                	descripcion : vm.data.descripcion,
	                    files: vm.files,
	                    contenedores : vm.contTemp
	                }
	            }).then(function (response) {
	                if($sweetalert.open(response)){
	                	vm.obj = {};
	                	vm.files = [];
	                	vm.data = {};
	                	vm.id_cliente = null;
	                	vm.listFactura(vm.id_semana);
	                	vm.listCertificado(vm.id_semana);
						vm.listValija(vm.id_semana);
						vm.listPacking(vm.id_semana);
						vm.listVl(vm.id_semana);
						vm.listOtros(vm.id_semana);
	                }
	                vm.flag = true;
	                vm.progress = 0;
	                vm.flagUpload = true;
	            }, function (response) {
	                if (response.status > 0) {
	                    $scope.errorMsg = response.status + ': ' + response.data;
	                }
	            }, function (evt) {
	                vm.progress = 
	                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
	            });
	        }
	    }

	    vm.openModalPreview = function(event, obj,type) {
	    	obj.tipo = type;

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_preview.html',
				controller: 'ModalUploadFileCtrl',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,cajas) {
						
			}, function () {
				
			});
	    };

	    vm.openLink = function(packing){
			vm.rutaPacking = 'https://view.officeapps.live.com/op/view.aspx?src=http://www.appbosa.com.pe/Server/public/archivos/packing/'+packing.url;
			var win = window.open(vm.rutaPacking, '_blank');
  			win.focus();
	    };

	    vm.openModalSelect = function(event,val,tipo) {
	    	vm.obj.id_cliente = vm.id_cliente;
	    	vm.obj.crud = 0;
	    	vm.obj.tipo = vm.tipo;
			if(val){
				vm.obj = val;
				vm.tipo = tipo;
				vm.obj.tipo = tipo;
				vm.obj.crud = 1;
			}
	    	vm.obj.id_semana = Number(vm.id_semana);
	    	if(vm.tipo === 3 || vm.tipo === 2){
				//var options = angular.element(event.target).data('options');
				var modalInstance = $uibModal.open({
					backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'modal_select_containers.html',
					controller: 'ModalUploadFileCtrl',
					size: null,
					backdropClass: 'splash splash-2 splash-ef-14',
					windowClass: 'splash splash-2 splash-ef-14',
					resolve: {
					  obj: function () {
					    return vm.obj;
					  }
					}
				});

				modalInstance.result.then(function (obj,crud) {
					vm.contTemp = [];
					for(var co in obj){
						if(obj[co].enable){
							vm.contTemp.push(obj[co]);
						}
					}
					
					if(vm.obj.crud === 1){
						var data = {
							contenedores : vm.contTemp,
							id_valija  : vm.obj.id,
							id_certificado : vm.obj.id
						};
						//console.log(vm.obj),
						if(vm.obj.tipo===2) {
							vm.r = 'certificado';
						}
						if(vm.obj.tipo===3) {
							vm.r = 'valija';
						}
						vm.obj = {};
						$crud.especialPost(vm.r+'/agregar_contenedor',data).then(res=>{
							if(res.data.success){
								toastr.success(res.data.message);
								vm.listValija(vm.id_semana);								
								vm.listCertificado(vm.id_semana);
							}
							else{
								toastr.error(res.data.message);
							}
						});
					}
					//vm.save(obj);
				}, function () {
					
				});
			}			
	    };

	    vm.openModalDelete = function(event,obj,type,size) {
	    	obj.tipo = type;
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_delete_file.html',
				controller: 'ModalUploadFileCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,cajas) {
						
			}, function () {
				vm.listFactura(vm.id_semana);
				vm.listCertificado(vm.id_semana);
				vm.listValija(vm.id_semana);
				vm.listPacking(vm.id_semana);
				vm.listVl(vm.id_semana);
				vm.listOtros(vm.id_semana);
			});
	    };

	    vm.openModalSend = function(event,obj,params,size) {
	    	obj.params = params;
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_send_file.html',
				controller: 'ModalUploadFileCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,cajas) {
						
			}, function () {
				vm.listFactura(vm.id_semana);
				vm.listCertificado(vm.id_semana);
				vm.listValija(vm.id_semana);
				vm.listPacking(vm.id_semana);
			});
	    };

  	}

  	function ModalUploadFileCtrl($scope, $crud, $uibModalInstance, obj, $file, $sweetalert){
  		var vm = $scope;

  		if(obj.id_cliente){
  			if (obj.tipo === 3) {
	  			$crud.especialPost('contenedor/ofValijaClienteAndSemana',obj).then(res => {
	  				vm.contenedores = (res.data.info);
	  			});
  			}
  			if (obj.tipo === 2) {
	  			$crud.especialPost('contenedor/ofCertificadoClienteAndSemana',obj).then(res => {
	  				vm.contenedores = (res.data.info);
	  			});
  			}
  		}

  		if(obj){
  			vm.flagButton = true;
			vm.obj = obj;	
			switch(obj.tipo){
				case 1:
					vm.route = 'factura';
					break;
				case 2:
					vm.route = 'certificado';
					break;
				case 3:
					vm.route = 'valija';
					break;
				case 4:
					vm.route = 'packing';
					break;
				case 5:
					vm.route = 'vl';
					break;
				case 6:
					vm.route = 'otros_archivos';
					break;
			}

			vm.ruta = '../Server/public/archivos/'+vm.route+'/'+obj.url;
		}

		vm.flagSend = true;
		vm.sendEmail = function(){
			vm.flagSend = false;
			switch (obj.params) {
    			case 1:
    				vm.route1 = 'factura';
    				break;
    			case 2:
    				vm.route1 = 'certificado';
    				break;
    			case 3:
    				vm.route1 = 'valija';
    				break;
    			case 4:
    				vm.route1 = 'packing';
    				break;
    			case 5:
    				vm.route1 = 'vl';
    				break;
    		}

    		$crud.especialPost(vm.route1+'/enviar_email/'+obj.id).then(res=>{
    			$uibModalInstance.dismiss('cancel');
    			vm.flagSend = true;
    			$sweetalert.open(res);
    		});
		}

		vm.ok = function(){
        	vm.flagButton = false;
        	$file.destroy(vm.route,vm.obj.id).then(function(res){
        		if($sweetalert.open(res)){
					$uibModalInstance.dismiss('cancel');
				}
        		vm.flagButton = true;
        	});	
        }

        vm.select = function(){
          	$uibModalInstance.close(vm.contenedores);
        }

		vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };
  	}

})()