	(function(){
	'use strict';
	angular.module('app')
	.controller('DetalleSemanaCtrl', DetalleSemanaCtrl)
	.controller('ModalDetalleSemanaCtrl', ModalDetalleSemanaCtrl);

	function DetalleSemanaCtrl($scope,$crud,$sweetalert,$permiso,$generic,$location,toastr,$uibModal){

		var vm = $scope;

		vm.flagCrud = true;
		vm.flag = true;
		vm.id_semana = $generic.getVariable('id_semana');

		vm.pagination = {
	        current: 1
	    }

		vm.list = function(id){
			vm.flag = true;
			$crud.show('contenedor',id).then(function(res){
				vm.containers = (res.data.info);
				vm.flag = false;
			});
		};

		if(vm.id_semana){
			vm.list(vm.id_semana);
		}
		else{
			$location.url('home/semana/lista_semana');
		}	

		vm.listEstado = function(){
			$crud.list('estado').then(function(res){
				vm.estados = (res.data.info);
			});
		};

		vm.listEstado();	

		vm.load = function(data){
			vm.flagCrud 				= false;
			vm.id_contenedor 			= data.id_contenedor;
			data.fecha_proceso_inicio_t = new Date($generic.filterDate(data.dia_proceso_inicio));
			data.fecha_proceso_fin_t 	= new Date($generic.filterDate(data.dia_proceso_fin));
			data.fecha_zarpe_t 			= (data.dia_zarpe != null) ? new Date($generic.filterDate(data.dia_zarpe)) : null ;
			data.fecha_llegada_t 		= (data.dia_llegada != null) ? new Date($generic.filterDate(data.dia_llegada)) : null ;
			vm.obj = angular.copy(data);
		}


		vm.delete = function(){
			$crud.delete('contenedor',vm.obj.id_contenedor).then(function(res){
				if($sweetalert.open(res)){
					vm.list(vm.id_semana);
				}
			});
		}

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = {};
		}

		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}		

	    vm.changeEstado = function(con){
	    	var data = {
					'id_contenedor' : con.id_contenedor,
					'id_estado' : con.id_estado
				}
	    	$crud.deleteObj('contenedor/cambiar_estado',data).then(function(res){
	    		if (res.data.success) {
	    			toastr.info(res.data.message);
	    		}else{
	    			toastr.error(res.data.message);
	    		}
				
				vm.list(vm.id_semana);
			});
	    };

		vm.redirectDetalle = function(data){
			$generic.addVariable('infoContenedor',JSON.stringify(data));
			$location.url('home/container/detalle');
		};

		vm.openModalSave = function(event, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.crud = vm.flagCrud;
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_container.html',
				controller: 'ModalDetalleSemanaCtrl',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj,cajas) {
				vm.list(vm.id_semana);		
			}, function () {

			});
	    };

		vm.openModalDelete = function(event, size) {

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_container_delete.html',
				controller: 'ModalDetalleSemanaCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				vm.delete(obj.id);			
			}, function () {
				
			});
	    };

  	}		

  	function ModalDetalleSemanaCtrl($scope, $crud, $uibModalInstance,toastr, obj,$generic,$sweetalert){
		var vm = $scope;

		vm.obj = {};		

		vm.obj = obj;

		vm.id_contenedor = obj.id_contenedor;
		vm.id_semana = $generic.getVariable('id_semana');

		vm.listCliente = function(){
			$crud.list('cliente').then(function(res){
				vm.clientes = (res.data.info);
			});
		};

		vm.listCliente();

		vm.listLineaNaviera = function(){
			$crud.list('linea_naviera').then(function(res){
				vm.lineas = (res.data.info);
			});
		};

		vm.listLineaNaviera();

		vm.listPuerto = function(){
			$crud.list('puerto_destino').then(function(res){
				vm.puertos = (res.data.info);
			});
		};

		vm.listPuerto();

		vm.selectCliente = function(id){
			$crud.especialPost('cliente/operadoresAsigned/'+id,{}).then(function(res){
				vm.operadores = (res.data.info);
			});
			$crud.especialPost('cliente/cajasAsigned/'+id,{}).then(function(res){
				vm.cajas = (res.data.info);
			});
		};

		// vm.listCaja = function(){
		// 	$crud.list('caja').then(function(res){
		// 		vm.cajas = (res.data.info);
		// 	});
		// };

		// vm.listCaja();

		vm.listNave = function(){
			$crud.list('nave').then(function(res){
				vm.naves = (res.data.info);
			});
		};

		vm.listNave();

		vm.cajasAdd = [];

		if (obj.cajas) {
			vm.cajasAdd = angular.copy(obj.cajas);
		}

		vm.searchCaja = function(id){
			for (var i = 0; i < vm.cajas.length; i++) {
		        if (vm.cajas[i].id === id) {
		            return vm.cajas[i];
		        }
		    }
		    return null;
		};

		vm.changeCajas = function(){
			vm.caja = (vm.searchCaja(vm.objCajas.id_caja));
		}

		vm.addCajas = function(){
			vm.caja.cantidad_cajas = vm.objCajas.total;
			if(vm.cajasAdd.length <= 3){
				vm.flag = false;
				for (var i = 0; i < vm.cajasAdd.length; i++) {
					if (vm.cajasAdd[i].id === vm.caja.id) {
						vm.flag = true;
						break;
					}
				}
				if (vm.flag) {
					toastr.error('Tipo de caja ya esta agregada');
				}else{
					vm.cajasAdd.push(angular.copy(vm.caja));
				}
			}
			vm.getTotalCajas();
			vm.objCajas = {};
		}

		vm.deleteArray = function(param){
			vm.cajasTemp = [];
			for (var i = 0; i < vm.cajasAdd.length; i++) {
	    		if (i != param) {
	    			vm.cajasTemp.push(vm.cajasAdd[i]);
	    		}
	    		else if (vm.cajasAdd[param].save === true)
	    		{
	    			var data = {
					'id_contenedor' : vm.id_contenedor,
					'id_caja' : vm.cajasAdd[param].id
					}
					$crud.deleteObj('contenedor/eliminar_caja',data).then(function(res){
						toastr.success(res.data.message);
						vm.getTotalCajas();
					});
	    		}    	
	    	}
	    	vm.getTotalCajas();
	    	vm.cajasAdd = angular.copy(vm.cajasTemp);
	    }

		vm.ok = function () {
			
			obj = angular.copy(vm.obj);
			obj.cajas = vm.cajasAdd;
	      	vm.createEdit();
	    };

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };

	    vm.totalCajas = 0;

	    vm.getTotalCajas = function(){
	    	vm.totalCajas = 0;
	    	for (var i = 0; i < vm.cajasAdd.length; i++) {
	    		vm.totalCajas += vm.cajasAdd[i].cantidad_cajas;	    	
	    	}
	    }

	    vm.getTotalCajas();

		if(!vm.obj.crud)
		{
			vm.selectCliente(vm.obj.id_cliente);
		}

	    vm.isSave = true;
	    vm.createEdit = function(){
	    	vm.isSave = false;
			obj.id_semana				= vm.id_semana;
			obj.fecha_llegada 			= $generic.filterDate(obj.fecha_llegada_t);
			obj.fecha_zarpe 			= $generic.filterDate(obj.fecha_zarpe_t);
			obj.fecha_proceso_inicio 	= $generic.filterDate(obj.fecha_proceso_inicio_t);
			obj.fecha_proceso_fin 		= $generic.filterDate(obj.fecha_proceso_fin_t);
			obj.id = vm.id_contenedor;
			if (vm.obj.crud) {
				$crud.create('contenedor',obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.close(obj);
					}
					vm.isSave = true;
				});
			}
			else{
				$crud.edit('contenedor',obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.close(obj);
					}
					vm.isSave = true;
				});
			}
		};
	}

})()