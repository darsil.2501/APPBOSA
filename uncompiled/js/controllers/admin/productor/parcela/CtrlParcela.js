(function(){
	'use strict';
	angular.module('app')
	.controller('ParcelaCtrl', ParcelaCtrl)
	.controller('ModalParcelaCtrl', ModalParcelaCtrl);

	function ParcelaCtrl($scope,$crud,$sweetalert,$uibModal,$generic){

		var vm = $scope;

		vm.obj = {};
		vm.flagCrud = true;
		vm.flag = true;

		vm.productor = JSON.parse($generic.getVariable('productor_parcela'));

		vm.list = function(){
			vm.flag = true;
			$crud.especialPost('productor/obtener_parcelas/'+vm.productor.id).then(function(res){
				vm.parcelas = res.data.info;
				vm.flag = false;
			});
		};
		vm.list();

		vm.load = function(data){
	     	vm.flagCrud = false;
	     	vm.obj = angular.copy(data);
	    };

	    vm.clear = function(){
	     	vm.flagCrud = true;
	     	vm.obj = {};
	    }

	    vm.openModalSave = function(event, size) {

	    	vm.obj.crud = vm.flagCrud;
	    	vm.obj.id_productor = vm.productor.id;

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
	            keyboard: false,
	            templateUrl: 'modal_parcela.html',
				controller: 'ModalParcelaCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  	obj: function () {
				    	return vm.obj;
				  	}
				}
			});

			modalInstance.result.then(function (obj) {
							
			}, function () {
				vm.list();
		        vm.clear();
			});
	    };

	    vm.openModalDelete = function(event, size) {

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_parcela_delete.html',
				controller: 'ModalParcelaCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  	obj: function () {
				    	return vm.obj;
				  	}
				}
			});

			modalInstance.result.then(function (obj) {
							
			}, function () {
				vm.list();
		        vm.clear();
			});
	    };

  	}	

  	function ModalParcelaCtrl($scope,$crud,$sweetalert,$uibModalInstance,obj){	

	  	var vm = $scope;

	  	vm.obj = {};

	  	vm.obj = obj;

	  	vm.tiposPropiedad = [
			{'id':1,'nombre':'Propia'},
			{'id':0,'nombre':'Arrendada'}
		];

		vm.deshijes = [
			{'id':1,'nombre':'SÍ'},
			{'id':0,'nombre':'NO'}
		];

	  	vm.listSector = function(){
			$crud.list('sector').then(function(res){
				vm.sectores = res.data.info;
			});
		};
		vm.listSector();

		vm.listEmpacadora = function(){
			$crud.list('empacadora').then(function(res){
				vm.empacadoras = res.data.info;
			});
		};
		vm.listEmpacadora();

	  	vm.createEdit = function(){
	  		if(vm.obj.crud){
	       		$crud.create('parcela',vm.obj).then(function(res){
	         		if($sweetalert.open(res)){
	           			vm.cancel();
	         		}
	       		});
	 		}
		    else
		    {
	       		$crud.edit('parcela',vm.obj).then(function(res){
	         		if($sweetalert.open(res)){
	           			vm.cancel();
	         		}
	       		});
	     	}
	    };

	    vm.delete = function(){
	     	$crud.delete('parcela',vm.obj.id).then(function(res){
	       		if($sweetalert.open(res)){
	         		vm.cancel();
	       		}
	     	});
	    }

	    vm.ok = function () {
	      	$uibModalInstance.close();
	    };

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };

  	}

})()