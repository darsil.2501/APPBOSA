	(function(){
	'use strict';
	angular.module('app')
	.controller('ProductorCtrl', ProductorCtrl)
	.controller('ModalProductorCtrl', ModalProductorCtrl);

	function ProductorCtrl($scope,$crud,$sweetalert,$uibModal,$generic){

		var vm = $scope;

		vm.flagCrud = true;
		vm.flag = true;

		vm.list = function(){
			vm.flag = true;
			$crud.list('productor').then(function(res){
				vm.productores = res.data.info;
				vm.flag = false;
			});
		};
		vm.list();		

		vm.load = function(data){
			vm.flagCrud = false;
	    	if(data.fecha_ingreso){
	 	   		data.fecha_ingreso_t = new Date(data.fecha_ingreso);
	 		}
	    	vm.obj = angular.copy(data);
	    };

	    vm.clear = function(){
		    vm.flagCrud = true;
		    vm.obj = {};
	    }

	    vm.redirectTo = function(productor)
	    {
	    	$generic.addVariable('productor_parcela',JSON.stringify(productor));
	    }

	    vm.openModalTrabajador = function(event, size) {

	    	vm.obj.trabajador = true;
	    	vm.obj.productores = vm.productores;

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
	            keyboard: false,
	            templateUrl: 'modal_trabajador.html',
				controller: 'ModalProductorCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				vm.list();
		        vm.clear();			
			}, function () {
				
			});
	    };
	    vm.openModalSave = function(event, size) {

	    	vm.obj.crud = vm.flagCrud;

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
	            keyboard: false,
	            templateUrl: 'modal_productor.html',
				controller: 'ModalProductorCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				vm.list();
		        vm.clear();			
			}, function () {
				
			});
	    };

	    vm.openModalDelete = function(event, size) {

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_productor_delete.html',
				controller: 'ModalProductorCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				vm.list();
		        vm.clear();		
			}, function () {
				
			});
	    };

  	}	

  	function ModalProductorCtrl($scope, $crud, $uibModalInstance, $sweetalert, obj, $generic, toastr){
  		var vm = $scope;

  		vm.obj = {};

  		vm.obj = obj;

  		vm.flagListCreate = true;

  		vm.listTrabajadores = function(){
	  		$crud.listEspecial('productor/obtener_trabajadores/'+vm.obj.id).then(function(res){
				vm.trabajadores = res.data.info;
			});
	  	}

  		if(vm.obj.trabajador){
  			//vm.trabajadores = [];
  			vm.listTrabajadores();
  		}

  		vm.sociosEstado = [
			{'id':1,'nombre':'SI'},
			{'id':0,'nombre':'NO'}
		];

		vm.estadoPersona = [
			{'id':0,'nombre':'No'},
			{'id':1,'nombre':'Si'}
		];

		vm.condiciones = [
			{'id':1,'nombre':'Permanente'},
			{'id':0,'nombre':'Eventual'}
		];

		vm.estado_persona = 0;
		vm.modelo= {id_persona : null,crud : true};

		vm.setPersona = function(){
			if(vm.modelo.id_persona){
				for (var i = 0; i < vm.obj.productores.length; i++) {
					if(vm.modelo.id_persona === vm.obj.productores[i].id_persona){
						vm.modelo = (vm.obj.productores[i]);
						vm.modelo.crud = true;
					}
				}
			}
		}

  		vm.createEdit = function(){
		    vm.obj.fecha_ingreso = $generic.filterDate(obj.fecha_ingreso_t);
	     	if(vm.obj.crud){
		       	$crud.create('productor',vm.obj).then(function(res){
		         	if($sweetalert.open(res)){
		           		vm.ok();
		         	}
		        });
		    }
		    else
		    {
		       	$crud.edit('productor',vm.obj).then(function(res){
		         	if($sweetalert.open(res)){
		         		vm.ok();
		         	}
		       	});
		    }
  		}

  		vm.delete = function(){
		   	$crud.delete('productor',vm.obj.id).then(function(res){
		       	if($sweetalert.open(res)){
		        	vm.ok();
		       	}
		    });
	    }

	    vm.clear = function(){
	    	vm.modelo= {id_persona : null,crud : true};
	    }

	    vm.loadTrabajador = function (trabajador){
	    	vm.flagListCreate = false;
	    	trabajador.crud = false;
	    	vm.estado_persona = trabajador.is_productor;
	    	vm.modelo = angular.copy(trabajador);
	    }

	    vm.saveTrabajador = function(){
	    	vm.modelo.id_productor = vm.obj.id;
	    	vm.obj.fecha_ingreso = $generic.filterDate(obj.fecha_ingreso_t);
	     	if(vm.modelo.crud){
		       	$crud.create('trabajador',vm.modelo).then(function(res){
		         	if(res.data.success){
						toastr.success(res.data.message);
						vm.listTrabajadores();
						vm.flagListCreate = true;
					}
					else{
						toastr.error(res.data.message);
					}
		        });
		    }
		    else
		    {
		       	$crud.edit('trabajador',vm.modelo).then(function(res){
		         	if(res.data.success){
						toastr.success(res.data.message);
						vm.listTrabajadores();
						vm.flagListCreate = true;
					}
					else{
						toastr.error(res.data.message);
					}
		       	});
		    }
	    }

	    vm.deleteTrabajador = function(trabajador){
		   	$crud.delete('trabajador',trabajador.id).then(function(res){
		       	if(res.data.success){
					toastr.success(res.data.message);
					vm.listTrabajadores();
				}
				else{
					toastr.error(res.data.message);
				}
		    });
	    }

  		vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };

	    vm.ok = function () {
	      	$uibModalInstance.close();
	    };

  }			

})()