	(function(){
	'use strict';
	angular.module('app')
	.controller('ReporteAuditoriaCtrl', ReporteAuditoriaCtrl);

	function ReporteAuditoriaCtrl($scope,$crud,$reporte,$sweetalert){

		var vm = $scope;

		vm.flag     = false;
		vm.flagLoad = false;
		vm.flagLoadReport = false;

		vm.listTablas = function(){
			$crud.list('tablas').then(function(res){
				vm.tablas = res.data.info;
			});
		};
		vm.listTablas();

		vm.downloadReport = function(data){
			vm.flagLoadReport = true;
			$reporte.excelData('auditoria',data).then(function(res){
				vm.flagLoadReport = false;
			});
		};

  }		

})()