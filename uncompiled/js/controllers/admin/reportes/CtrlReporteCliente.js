	(function(){
	'use strict';
	angular.module('app')
	.controller('ReporteClienteCtrl', ReporteClienteCtrl);

	function ReporteClienteCtrl($scope,$crud,$reporte,$sweetalert){

		var vm = $scope;

		vm.flag     = false;
		vm.flagLoad = false;
		vm.flagLoadReport = false;

		vm.listAnio = function(){
			$crud.listEspecial('anio/give_semanas').then(function(res){
				vm.anios = res.data.info;
			});
		};
		vm.listAnio();

		vm.listCliente = function(){
			$crud.list('cliente').then(function(res){
				vm.clientes = res.data.info;
			});
		};
		vm.listCliente();

		vm.listSemana = function(anio){
			$crud.especialId('anio/withSemana',anio).then(function(res){
				if(res.data.success){
				vm.semanas = res.data.info;
				}
			});
		};

		vm.preview = function(obj){
			vm.flagLoad = true;
			$crud.especialPost('data/contenedores_cliente',obj).then(function(res){
				vm.flagLoad = false;
				vm.flag = res.data.success;
				vm.containers = res.data.info;
			});
		};

		vm.downloadReport = function(data){
			vm.flagLoadReport = true;
			$reporte.excelData('contenedores_cliente',data).then(function(res){
				vm.flagLoadReport = false;
			});
		};

  }		

})()