	(function(){
	'use strict';
	angular.module('app')
	.controller('ReporteSemanaCtrl', ReporteSemanaCtrl);

	function ReporteSemanaCtrl($scope,$crud,$reporte,$sweetalert){

		var vm = $scope;

		vm.flag     = false;
		vm.flagLoad = false;
		vm.flagLoadReport = false;

		vm.listAnios = function(){
			$crud.listEspecial('anio/give_semanas').then(function(res){
				vm.anios = res.data.info;
			});
		};
		vm.listAnios();

		vm.listSemana = function(anio){
			$crud.especialId('anio/withSemana',anio).then(function(res){
				if(res.data.success){
				vm.semanas = res.data.info;
				}
			});
		};

		
		vm.preview = function(obj){
			var data = {
				id_anio : obj.anio,
				id_semana : obj.semana
			};
			vm.flagLoad = true;
			$crud.especialPost('data/contenedores_semana',data).then(function(res){
				vm.flagLoad = false;
				vm.flag = res.data.success;
				vm.containers = res.data.info;
			});
		};

		vm.downloadReport = function(){
			var data = {
				id_anio : vm.obj.anio,
				id_semana : vm.obj.semana
			};
			vm.flagLoadReport = true;
			$reporte.excelData('contenedores_semana',data).then(function(res){
				vm.flagLoadReport = false;
			});
		};

  }		

})()