(function(){
	'use strict';
	angular.module('app')
	.controller('BuscarContenedorCtrl', BuscarContenedorCtrl);

	function BuscarContenedorCtrl($scope,$crud,$reporte,$sweetalert){

		var vm = $scope;

		vm.flag     = false;
		vm.flagLoad = false;
		vm.flagLoadReport = false;

		vm.preview = function(obj){
			vm.flagLoad = true;
			$crud.especialPost('data/buscar_contenedor',obj).then(function(res){
				vm.flagLoad   = false;
				vm.flag       = res.data.success;
				vm.containers = res.data.info;
			});
		};

		vm.downloadReport = function(data){
			vm.flagLoadReport = true;
			$reporte.excelData('buscar_contenedor',data).then(function(res){
				vm.flagLoadReport = false;
			});
		};

  }	

})()