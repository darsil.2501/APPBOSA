(function(){
	'use strict';
	angular.module('app')
	.controller('ReporteOperadorCtrl', ReporteOperadorCtrl);

	function ReporteOperadorCtrl($scope,$crud,$reporte,$sweetalert){

		var vm = $scope;

		vm.flag     = false;
		vm.flagLoad = false;
		vm.flagLoadReport = false;

		vm.listAnio = function(){
			$crud.listEspecial('anio/give_semanas').then(function(res){
				vm.anios = res.data.info;
			});
		};
		vm.listAnio();

		vm.listOperador = function(){
			$crud.list('operador').then(function(res){
				vm.operadores = res.data.info;
			});
		};
		vm.listOperador();

		vm.listSemana = function(anio){
			$crud.especialId('anio/withSemana',anio).then(function(res){
				if(res.data.success){
				vm.semanas = res.data.info;
				}
			});
		};

		vm.preview = function(obj){
			vm.flagLoad = true;
			$crud.especialPost('data/contenedores_operador',obj).then(function(res){
				vm.flagLoad = false;
				vm.flag = res.data.success;
				vm.containers = res.data.info;
			});
		};

		vm.downloadReport = function(data){
			vm.flagLoadReport = true;
			$reporte.excelData('contenedores_operador',data).then(function(res){
				vm.flagLoadReport = false;
			});
		};

  }	

})()