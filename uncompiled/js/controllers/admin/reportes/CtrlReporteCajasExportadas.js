	(function(){
	'use strict';
	angular.module('app')
	.controller('ReporteCajasExportadasCtrl', ReporteCajasExportadasCtrl);

	function ReporteCajasExportadasCtrl($scope,$crud,$reporte,$sweetalert){

		var vm = $scope;

		vm.flag     = false;
		vm.flagLoad = false;
		vm.flagLoadReport = false;

		vm.listAnios = function(){
			$crud.listEspecial('anio/give_semanas').then(function(res){
				vm.anios = res.data.info;
			});
		};
		vm.listAnios();

		vm.downloadReport = function(data){
			vm.flagLoadReport = true;
			$reporte.excelData('cajas_exportadas',data).then(function(res){
				vm.flagLoadReport = false;
			});
		};

  }		

})()