(function(){
	'use strict';
	angular.module('app')
	.controller('PaisCtrl',PaisCtrl)
	.controller('ModalPaisCtrl',ModalPaisCtrl);

	function PaisCtrl($scope,$crud,$sweetalert, $uibModal,$permiso){
		var vm = $scope;

		vm.flagCrud = true;
		vm.flag = true;

		vm.list = function(){
			$crud.list('pais').then(function(res){
				vm.paises = res.data.info;
				vm.flag = false;
			});
		}

		vm.list();

		vm.load = function(data){
			vm.flagCrud = false;
			vm.obj = angular.copy(data);
		}

		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}

		vm.delete = function(){
			$crud.delete('pais',vm.obj.id).then(function(res){
				if($sweetalert.open(res)){
					vm.list();
				}
			});
		}

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = {};
		}

		vm.openModalSave = function(event, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.crud = vm.flagCrud;
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_pais.html',
				controller: 'ModalPaisCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
						
			}, function () {
				vm.list();
				vm.clear();
			});
	    };

		vm.openModalDelete = function(event, size) {

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_pais_delete.html',
				controller: 'ModalPaisCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				vm.delete(obj.id);			
			}, function () {
				
			});
	    };

	}

	function ModalPaisCtrl($scope, $crud, $uibModalInstance, $sweetalert, obj){
		var vm = $scope;

		vm.obj = {};		

		vm.obj = obj;

		vm.ok = function () {
	      	$uibModalInstance.close(vm.obj);
	    };

		vm.createEdit = function(){
			if (vm.obj.crud) {
				$crud.create('pais',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.dismiss('cancel');
					}
				});
			}
			else{
				$crud.edit('pais',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.dismiss('cancel');
					}
				});
			}
		}

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()