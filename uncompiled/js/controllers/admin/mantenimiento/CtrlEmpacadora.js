(function(){
	'use strict';
	angular.module('app')
	.controller('EmpacadoraCtrl', EmpacadoraCtrl)
	.controller('ModalEmpacadoraCtrl', ModalEmpacadoraCtrl);

	function EmpacadoraCtrl($scope,$crud,$sweetalert,$uibModal){

		var vm = $scope;

		vm.obj = {};
		vm.flagCrud = true;
		vm.flag = true;

		vm.list = function(){
			vm.flag = true;
			$crud.list('empacadora').then(function(res){
				vm.empacadoras = res.data.info;
				vm.flag = false;
			});
		};
		vm.list();

		vm.load = function(data){
	     	vm.flagCrud = false;
	     	vm.obj = angular.copy(data);
	    };

	    vm.clear = function(){
	     	vm.flagCrud = true;
	     	vm.obj = {};
	    }

	    vm.openModalSave = function(event, size) {

	    	vm.obj.crud = vm.flagCrud;

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
	            keyboard: false,
	            templateUrl: 'modal_empacadora.html',
				controller: 'ModalEmpacadoraCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  	obj: function () {
				    	return vm.obj;
				  	}
				}
			});

			modalInstance.result.then(function (obj) {
							
			}, function () {
				vm.list();
		        vm.clear();
			});
	    };

	    vm.openModalDelete = function(event, size) {

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_empacadora_delete.html',
				controller: 'ModalEmpacadoraCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  	obj: function () {
				    	return vm.obj;
				  	}
				}
			});

			modalInstance.result.then(function (obj) {
							
			}, function () {
				vm.list();
		        vm.clear();
			});
	    };

  	}	

  	function ModalEmpacadoraCtrl($scope,$crud,$sweetalert,$uibModalInstance,obj){	

	  	var vm = $scope;

	  	vm.obj = {};

	  	vm.obj = obj;

	  	vm.createEdit = function(){
	     	if(vm.obj.crud){
	       		$crud.create('empacadora',vm.obj).then(function(res){
	         		if($sweetalert.open(res)){
	           			vm.cancel();
	         		}
	       		});
	 		}
		    else
		    {
	       		$crud.edit('empacadora',vm.obj).then(function(res){
	         		if($sweetalert.open(res)){
	           			vm.cancel();
	         		}
	       		});
	     	}
	    };

	    vm.delete = function(){
	     	$crud.delete('empacadora',vm.obj.id).then(function(res){
	       		if($sweetalert.open(res)){
	         		vm.cancel();
	       		}
	     	});
	    }

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };

  	}

})()