(function(){
	'use strict';
	angular.module('app')
	.controller('PuertoCtrl',PuertoCtrl)
	.controller('ModalPuertoCtrl',ModalPuertoCtrl);

	function PuertoCtrl($scope,$crud,$sweetalert,$uibModal,$permiso){
		var vm = $scope;

		vm.flagCrud = true;
		vm.flag = true;
		vm.obj = {};

		vm.list = function(){
			$crud.list('puerto_destino').then(function(res){
				vm.puertos = res.data.info;
				vm.flag = false;
			});
		}

		vm.list();

		vm.load = function(data){
			vm.flagCrud = false;
			vm.obj = angular.copy(data);
		}
		
		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}

		vm.delete = function(){
			$crud.delete('puerto_destino',vm.obj.id).then(function(res){
				if($sweetalert.open(res)){
					vm.list();
				}
			});
		}

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = {};
		}

		vm.openModalSave = function(event, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.crud = vm.flagCrud;
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_puerto.html',
				controller: 'ModalPuertoCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
						
			}, function () {
				vm.list();
				vm.clear();
			});
	    };

		vm.openModalDelete = function(event, size) {

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_puerto_delete.html',
				controller: 'ModalPuertoCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				vm.delete(obj.id);			
			}, function () {

			});
	    };

	}

	function ModalPuertoCtrl($scope, $crud, $uibModalInstance, $sweetalert, obj){
		var vm = $scope;

		vm.obj = {};		

		vm.obj = obj;

		vm.listCiudad = function(){
			$crud.list('ciudad').then(function(res){
				vm.ciudades = res.data.info;
			});
		}

		vm.listCiudad();

		vm.ok = function () {
	      	$uibModalInstance.close(vm.obj);
	    };

		vm.createEdit = function(){
			if (vm.obj.crud) {
				$crud.create('puerto_destino',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.dismiss('cancel');
					}
				});
			}
			else{
				$crud.edit('puerto_destino',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.dismiss('cancel');
					}
				});
			}
		}

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()