(function(){
	'use strict';
	angular.module('app')
	.controller('AnioCtrl',AnioCtrl);

	function AnioCtrl($scope,$crud,$sweetalert){
		var vm = $scope;

		vm.flagCrud = true;

		vm.list = function(){
			$crud.list('anio').then(function(res){
				vm.anios = res.data.info;
			});
		}

		vm.list();

		vm.load = function(data){
			vm.flagCrud = false;
			vm.obj = angular.copy(data);
		}

		vm.createEdit = function(){
			if (vm.flagCrud) {
				$crud.create('anio',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						vm.list();
					}
				});
			}
			else{
				$crud.edit('anio',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						vm.list();
					}
				});
			}
		}

		vm.delete = function(){
			$crud.delete('anio',vm.obj.id).then(function(res){
				if($sweetalert.open(res)){
					vm.list();
				}
			});
		}

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = null;
		}

	}

})()