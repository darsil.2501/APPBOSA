	(function(){
	'use strict';
	angular.module('app')
	.controller('EstadoCtrl', EstadoCtrl)
	.controller('ModalEstadoCtrl', ModalEstadoCtrl);

	function EstadoCtrl($scope,$crud,$sweetalert,$uibModal,$permiso){

		var vm = $scope;

		vm.flagCrud = true;
		vm.flag = true;

		vm.list = function(){
			$crud.list('estado').then(function(res){
				vm.estados = res.data.info;
				vm.flag = false;
			});
		};
		vm.list();

		vm.load = function(data){
	     vm.flagCrud = false;
	     vm.obj = angular.copy(data);
	    };

	    vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}

	    vm.delete = function(){
	     $crud.delete('estado',vm.obj.id).then(function(res){
	       if($sweetalert.open(res)){
	         vm.list();
	         vm.clear();
	       }
	     });
	    }

	    vm.clear = function(){
	     vm.flagCrud = true;
	     vm.obj = {};
	    }

	    vm.openModalSave = function(event, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.crud = vm.flagCrud;
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_estado.html',
				controller: 'ModalEstadoCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
						
			}, function () {
				vm.list();
	            vm.clear();
			});
	    };

		vm.openModalDelete = function(event, size) {

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_estado_delete.html',
				controller: 'ModalEstadoCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				vm.delete(obj.id);			
			}, function () {
				
			});
	    };

  	}	

  	function ModalEstadoCtrl($scope, $crud, $uibModalInstance, $sweetalert, obj){
		var vm = $scope;

		vm.obj = {};		

		vm.obj = obj;

		vm.ok = function () {
	      	$uibModalInstance.close(vm.obj);
	    };

	    vm.createEdit = function(){
	     if(vm.obj.crud){
	       $crud.create('estado',vm.obj).then(function(res){
	         if($sweetalert.open(res)){
	          $uibModalInstance.dismiss('cancel');
	         }
	       });
	     }
	     else
	     {
	       $crud.edit('estado',vm.obj).then(function(res){
	         if($sweetalert.open(res)){
	           $uibModalInstance.dismiss('cancel');
	         }
	       });
	     }
	    };

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}	

})()