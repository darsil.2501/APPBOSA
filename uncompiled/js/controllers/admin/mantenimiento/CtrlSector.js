(function(){
	'use strict';
	angular.module('app')
	.controller('SectorCtrl', SectorCtrl)
	.controller('ModalSectorCtrl', ModalSectorCtrl);

	function SectorCtrl($scope,$crud,$sweetalert,$uibModal){

		var vm = $scope;

		vm.obj = {};
		vm.flagCrud = true;

		vm.flag = true;

		vm.list = function(){
		vm.flag = true;
			$crud.list('sector').then(function(res){
				vm.sectores = res.data.info;
				vm.flag = false;
			});
		};
		vm.list();

		vm.load = function(data){
	     	vm.flagCrud = false;
	     	vm.obj = angular.copy(data);
	    };

	    vm.clear = function(){
	     	vm.flagCrud = true;
	     	vm.obj = {};
	    }

	    vm.openModalDelegado = function(event, size) {

	    	vm.obj.delegado = true;

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
	            keyboard: false,
	            templateUrl: 'modal_delegados.html',
				controller: 'ModalSectorCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  	obj: function () {
				    	return vm.obj;
				  	}
				}
			});

			modalInstance.result.then(function (obj) {
							
			}, function () {
				// vm.list();
		  //       vm.clear();
			});
	    };

	    vm.openModalSave = function(event, size) {

	    	vm.obj.crud = vm.flagCrud;

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
	            keyboard: false,
	            templateUrl: 'modal_sector.html',
				controller: 'ModalSectorCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  	obj: function () {
				    	return vm.obj;
				  	}
				}
			});

			modalInstance.result.then(function (obj) {
							
			}, function () {
				vm.list();
		        vm.clear();
			});
	    };

	    vm.openModalDelete = function(event, size) {

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_sector_delete.html',
				controller: 'ModalSectorCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  	obj: function () {
				    	return vm.obj;
				  	}
				}
			});

			modalInstance.result.then(function (obj) {
							
			}, function () {
				vm.list();
		        vm.clear();
			});
	    };

  	}	

  	function ModalSectorCtrl($scope,$crud,$sweetalert,$uibModalInstance,obj,toastr,$generic){	

	  	var vm = $scope;

	  	vm.obj = {};

	  	vm.obj = obj;

	  	vm.flagListAdd = true;
	  	vm.flagCrud = false;

	  	vm.listProductores = function(id_sector){
	  		$crud.listEspecial('sector/obtener_productores/'+id_sector).then(function(res){
				vm.productores = res.data.info;
			});
	  	}

	  	vm.listDelegados = function(id_sector){
	  		$crud.listEspecial('sector/obtener_delegados/'+id_sector).then(function(res){
				vm.delegados = res.data.info;
			});
	  	}

	  	if (vm.obj.delegado) {
	  		vm.listProductores(vm.obj.id);
	  		vm.listDelegados(vm.obj.id);
	  	}

	  	vm.createEdit = function(){
	     	if(vm.obj.crud){
	       		$crud.create('sector',vm.obj).then(function(res){
	         		if($sweetalert.open(res)){
	           			vm.cancel();
	         		}
	       		});
	 		}
		    else
		    {
	       		$crud.edit('sector',vm.obj).then(function(res){
	         		if($sweetalert.open(res)){
	           			vm.cancel();
	         		}
	       		});
	     	}
	    };

	    vm.asignarDelegado = function(){
	    	vm.flagCrud = true;
	    	vm.model.fecha_inicio_cargo	= $generic.filterDate(vm.model.fecha_inicio_t);
	    	vm.model.id_sector = vm.obj.id;
	    	$crud.especialPost('sector/create_delegado',vm.model).then(function(res){
	    		vm.flagCrud = false;
         		if(res.data.success){
					toastr.success(res.data.message);
					vm.listDelegados(vm.obj.id);
					vm.clear();
				}
				else{
					toastr.error(res.data.message);
				}
       		});
	    }

	    vm.quitarDelegado = function(delegado){
	    	$crud.especialId('sector/delete_delegado',delegado.id).then(function(res){
         		if(res.data.success){
					toastr.success(res.data.message);
					vm.listDelegados(vm.obj.id);
					vm.clear();
				}
				else{
					toastr.error(res.data.message);
				}
       		});
	    }

	    vm.delete = function(){
	     	$crud.delete('sector',vm.obj.id).then(function(res){
	       		if($sweetalert.open(res)){
	         		vm.cancel();
	       		}
	     	});
	    }

	    vm.new = function(){
	    	vm.flagListAdd = false;
	    	vm.model = {};
	    }

	    vm.clear = function(){
	    	vm.flagListAdd = true;
	    	vm.model = {};
	    }

	    vm.ok = function () {
	      	$uibModalInstance.close();
	    };

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };

  	}

})()