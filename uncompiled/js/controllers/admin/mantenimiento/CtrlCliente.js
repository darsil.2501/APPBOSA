(function(){
	'use strict';
	angular.module('app')
	.controller('ClienteCtrl',ClienteCtrl)
	.controller('ModalCienteCtrl',ModalCienteCtrl);

	function ClienteCtrl($scope,$crud,$sweetalert,$uibModal,$permiso){
		var vm = $scope;

		vm.flagmenu = true;
		vm.flag = true;

		vm.list = function(){
			$crud.list('cliente').then(function(res){
				vm.clientes = res.data.info;
				vm.flag = false;
			});
		}

		vm.list();

		vm.load = function(data){
			vm.flagCrud = false;
			vm.obj = angular.copy(data);
		}

		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}

		vm.delete = function(id){
			$crud.delete('cliente',id).then(function(res){
				if($sweetalert.open(res)){
					vm.list();
				}
			});
		}

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = {};
		}

		vm.openModalSave = function(event, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.crud = vm.flagCrud;
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_cliente.html',
				controller: 'ModalCienteCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
						
			}, function () {
				vm.list();
				vm.clear();
			});
	    };

		vm.openModalDelete = function(event, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.crud = vm.flagCrud;
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_cliente_delete.html',
				controller: 'ModalCienteCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				vm.delete(obj.id);			
			}, function () {
				
			});
	    };

	    vm.openModalOperadores = function(event, cliente, size) {

	    	vm.obj = (cliente);
	    	vm.obj.tipo = 1;

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_list_operadores.html',
				controller: 'ModalCienteCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
					
			}, function () {
				
			});
	    };

	    vm.openModalCajas = function(event, cliente, size) {

	    	vm.obj = (cliente);
	    	vm.obj.tipo = 2;

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_list_cajas.html',
				controller: 'ModalCienteCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
					
			}, function () {
				
			});
	    };

	    vm.openModalCorreos = function(event, cliente, size) {

	    	vm.obj = (cliente);
	    	vm.obj.tipo = 3;

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_list_correos.html',
				controller: 'ModalCienteCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
					
			}, function () {
				
			});
	    };

	}

	function ModalCienteCtrl($scope, $crud, $uibModalInstance, $sweetalert, obj,$menu, toastr){
		var vm = $scope;

		vm.obj = {};
		vm.d = {};		

		vm.obj = obj;
		vm.flag = true;
		vm.flagCorreo = true;

		vm.ok = function () {
	      	$uibModalInstance.close(vm.obj);
	    };

	    vm.listOperadores = function(id){
			vm.id_cliente = id;
			$crud.especialId('cliente/veroperadores',id).then(function(res){
				vm.flag = false;
				vm.operadores = res.data.info;
			});
	    };

	    vm.listCajas = function(id){
			vm.id_cliente = id;
			$crud.especialId('cliente/vercajas',id).then(function(res){
				vm.flag = false;
				vm.cajas = res.data.info;
			});
	    };

	    vm.listCorreos = function(id){
			vm.id_cliente = id;
			$crud.especialId('cliente/vercorreos',id).then(function(res){
				vm.flag = false;
				vm.correos = res.data.info;
			});
	    };

	    if(vm.obj.tipo === 1){
	    	vm.listOperadores(vm.obj.id)
	    }
	    if(vm.obj.tipo === 2){
	    	vm.listCajas(vm.obj.id)
	    }

	    if(vm.obj.tipo === 3){
	    	vm.listCorreos(vm.obj.id);
	    }

	    vm.saveOperador = function(operador){
	    	vm.data = {'id_cliente': vm.id_cliente,'id_operador':operador.id};
			if (operador.enable) {
				$menu.asignar('cliente/asignaroperador',vm.data).then(function(res){
					if(res.data.success){
						toastr.success(res.data.message);
					}
					else{
						toastr.error(res.data.message);
					}
				});
			}
			else{
				$menu.asignar('cliente/quitaroperador',vm.data).then(function(res){
					if(res.data.success){
						toastr.success(res.data.message);
					}
					else{
						toastr.error(res.data.message);
					}
				});
			}
	    }

	    vm.saveCaja = function(caja){
	    	vm.data = {'id_cliente': vm.id_cliente,'id_caja':caja.id};
			if (caja.enable) {
				$menu.asignar('cliente/asignarcaja',vm.data).then(function(res){
					if(res.data.success){
						toastr.success(res.data.message);
					}
					else{
						toastr.error(res.data.message);
					}
				});
			}
			else{
				$menu.asignar('cliente/quitarcaja',vm.data).then(function(res){
					if(res.data.success){
						toastr.success(res.data.message);
					}
					else{
						toastr.error(res.data.message);
					}
				});
			}
	    }

	    vm.saveCorreo = function(d){
	    	d.id_cliente = vm.id_cliente;
	    	$crud.especialPost('cliente/agregarcorreos',d).then(res=>{
	    		if(res.data.success){
					toastr.success(res.data.message);
					vm.listCorreos(vm.id_cliente);
					console.log(vm.d)
				}
				else{
					toastr.error(res.data.message);
				}
	    	});
	    }

	    vm.eliminarCorreo = function(correo){
	    	$crud.especialPost('cliente/eliminarcorreos/'+correo.id,correo).then(res=>{
	    		if(res.data.success){
					toastr.success(res.data.message);
					vm.listCorreos(vm.id_cliente);
					//vm.d = {};
				}
				else{
					toastr.error(res.data.message);
				}
	    	});
	    }

		vm.createEdit = function(){
			if (vm.obj.crud) {
				$crud.create('cliente',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.dismiss('cancel');
					}
				});
			}
			else{
				$crud.edit('cliente',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.dismiss('cancel');
					}
				});
			}
		}

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()