(function(){
	'use strict';
	angular.module('app')
	.controller('ConfiguracionCtrl', ConfiguracionCtrl);

	function ConfiguracionCtrl($scope,$crud,$sweetalert,$generic){

		var vm = $scope;

    vm.user = angular.fromJson($generic.getVariable('user_subopcion'));

		vm.rutasTab = 'views/home/usuario/includes_rol/tab_';

        vm.changeTab = function(tab){
          vm.currentTab = vm.rutasTab + tab.id + '.html';
        };

        vm.dataTab = [
        {id: 1, name: "Opciones",active: true},
        {id: 2, name: "Permisos"}
       ];
       vm.changeTab(vm.dataTab[0]);

	};

})()