(function(){
	'use strict';
	angular.module('app')
	.controller('RTab1Ctrl', RTab1Ctrl);

	function RTab1Ctrl($scope,$crud,$sweetalert,toastr,$menu,$generic,$location){

		var vm = $scope;

    vm.flagCrud = true;
    vm.flag = true;

    vm.list = function(){
      //vm.flag = true;
      $crud.especialPost('usuario/listar_menu/'+vm.user.id_user,null).then(function(res){
        vm.opciones = res.data.info;
        vm.opciones_temp = angular.copy(vm.opciones);
        vm.flag = false;
      });
    };
    
    if (!vm.user) {
      $location.url('home/usuario/usuario');
    }
    else
    {
      vm.list();
    }

    vm.saveOpcion = function(){
      
      if(vm.subopciones_cambiadas.length > 0){
        var data = {'subopciones' : vm.subopciones_cambiadas, 'usuario_id' : vm.user.id_user};
        $menu.asignar('usuario/asignar_subopcion',data).then(function(res){
            if($sweetalert.open(res)){
              vm.list();
            }            
        });
      }
      else
      {
        toastr.warning("No hay Cambios Realizados");
      }     
    }

    vm.loadData = function(){
      vm.subopciones_cambiadas = [];
      vm.opciones.forEach( function(element, index) {
         for (var i = 0; i < vm.opciones[index].subopciones.length; i++) {
          if(vm.opciones[index].subopciones[i].enable !== vm.opciones_temp[index].subopciones[i].enable){
              vm.subopciones_cambiadas.push(vm.opciones[index].subopciones[i])
           }
         }
      });
    }

  }

})()