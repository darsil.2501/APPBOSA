(function(){
	'use strict';
	angular.module('app')
	.controller('RTab2Ctrl', RTab2Ctrl);

	function RTab2Ctrl($scope,$crud,$sweetalert,$menu){

		var vm = $scope;
 		
		vm.flagCrud = true;
		vm.flag = true;

		vm.list = function(){
			$crud.especialPost('usuario/listar_permisos/'+vm.user.id_user,null).then(function(res){
				vm.permisos = res.data.info;
				vm.permisos_temp = angular.copy(vm.permisos);
				vm.flag = false;
			});
		};
		if (!vm.user) {
	      $location.url('home/usuario/usuario');
	    }
	    else
	    {
	      vm.list();
    	}

		vm.loadData = function(){
	      	vm.permisos_cambiados = [];
	      	vm.permisos.forEach( function(element, index) {
	      		if(vm.permisos[index].enable !== vm.permisos_temp[index].enable){
	              	vm.permisos_cambiados.push(element);
	        	}
	      	});
	    }

	    vm.saveOpcion = function(){      
	      	if(vm.permisos_cambiados.length > 0){
		        var data = {'permisos' : vm.permisos_cambiados, 'usuario_id' : vm.user.id_user};
		        $menu.asignar('usuario/asignar_permiso',data).then(function(res){
		            if($sweetalert.open(res)){
		              vm.list();
		            }            
		        });
	      	}
	      	else
	      	{
	        	toastr.warning("No hay Cambios Realizados");
	      	}     
    	}

	};

})()