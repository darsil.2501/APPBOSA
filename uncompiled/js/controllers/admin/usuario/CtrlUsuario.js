(function(){
	'use strict';
	angular.module('app')
	.controller('UsuarioCtrl', UsuarioCtrl)
	.controller('ModalUsuarioCtrl',ModalUsuarioCtrl);

	function UsuarioCtrl($scope,$crud,$sweetalert,$uibModal,$generic,$permiso){

		var vm = $scope;

		vm.flagCrud = true;
		vm.flag = true;
		
		vm.list = function(){
			$crud.list('usuario').then(function(res){
				vm.usuarios = res.data.usuarios;
				vm.flag = false;
			});
		};

		vm.list();		

		vm.load = function(data){
			vm.flagCrud = false;
			vm.obj = angular.copy(data);
		};

		vm.permisosHas = function(permiso){
			return ($permiso.validate(permiso));
		}

		vm.delete = function(){
			$crud.delete('usuario',vm.obj.id_user).then(function(res){
				if($sweetalert.open(res)){
					vm.list();
					vm.clear();
				}
			});
		}

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = {};
		}

		vm.redirectToOpciones = function(user){
			$generic.addVariable('user_subopcion',JSON.stringify(user));
		}

		vm.openModalSave = function(event, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.crud = vm.flagCrud;
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_usuario.html',
				controller: 'ModalUsuarioCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
						
			}, function () {
				vm.list();
				vm.clear();
			});
	    };

		vm.openModalDelete = function(event, size) {

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_usuario_delete.html',
				controller: 'ModalUsuarioCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
				vm.delete(obj.id);			
			}, function () {
				
			});
	    };

	};

	function ModalUsuarioCtrl($scope, $crud, $uibModalInstance, obj,$sweetalert){
		var vm = $scope;

		vm.obj = {};		

		vm.obj = obj;

		vm.viewOC = 0;

		vm.listCliente = function(){
			$crud.list('cliente').then(function(res){
				vm.clientes = res.data.info;
			});
		}

		vm.listCliente();

		vm.listOperador = function(){
			$crud.list('operador').then(function(res){
				vm.operadores = res.data.info;
			});
		}

		vm.listOperador();

		vm.changeCargo = function(){
			vm.viewOC = (vm.searchArray());
		}

		vm.searchArray = function(){
			var key;
            for (key in vm.cargos) {
            	if(vm.cargos[key].id === vm.obj.id_cargo){
            		if(vm.cargos[key].nombre.toLowerCase().indexOf("opera")===0){
            			return 1;
            		}
            		if(vm.cargos[key].nombre.toLowerCase().indexOf("client")===0){
            			return 2;
            		}
            		return 0;
            	}
            }
		}

		vm.listCargo = function(){
			$crud.list('cargo').then(function(res){
				vm.cargos = res.data.info;
			});
		};

		vm.listCargo();

		vm.createEdit = function(){
			if(vm.obj.crud){
				$crud.create('usuario',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.dismiss('cancel');		
					}
				});
			}
			else
			{
				vm.obj.id = vm.obj.id_user;
				$crud.edit('usuario',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						$uibModalInstance.dismiss('cancel');
					}
				});
			}
		};

		vm.ok = function () {
			$uibModalInstance.close(vm.obj);
	    };

	    vm.cancel = function () {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()