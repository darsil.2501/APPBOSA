(function(){
	'use strict';
	angular.module('app')
	.controller('MenuCtrl', MenuCtrl);

	function MenuCtrl($scope,$crud,$sweetalert){

		var vm = $scope;
		
		vm.rutasTab = 'views/home/usuario/includes_menu/tab_';

        vm.changeTab = function(tab){
          vm.currentTab = vm.rutasTab + tab.id + '.html';
        };

        vm.dataTab = [
        {id: 1, name: "Menú",active: true},
        {id: 2, name: "Opciones"},
        {id: 3, name: "Sub Opciones"}
       ];
       vm.changeTab(vm.dataTab[0]);

	};

})()