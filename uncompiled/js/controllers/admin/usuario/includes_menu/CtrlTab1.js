(function(){
	'use strict';
	angular.module('app')
	.controller('Tab1Ctrl', Tab1Ctrl)
	.controller('ModalTab1Ctrl', ModalTab1Ctrl);

	function Tab1Ctrl($scope,$crud,$sweetalert,$menu,toastr,$uibModal){

		var vm = $scope;

		vm.flagCrud = true;
		vm.flag = true;
		
		vm.list = function(){
			vm.flag = true;
			$crud.list('menu').then(function(res){
				vm.menus = res.data.info;
				vm.flag = false;
			});
		};

		vm.list();		

		vm.load = function(data){
			vm.flagCrud = false;
			vm.obj = angular.copy(data);
		};

		vm.createEdit = function(){
			if(vm.flagCrud){
				$crud.create('menu',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						vm.list();
						vm.clear();
					}
				});
			}
			else
			{
				$crud.edit('menu',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						vm.list();
						vm.clear();
					}
				});
			}
		};

		vm.delete = function(){
			$crud.delete('menu',vm.obj.id).then(function(res){
				if($sweetalert.open(res)){
					vm.list();
					vm.clear();
				}
			});
		}
		

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = {};
		}

		vm.openModalAsignar = function(event, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.crud = vm.flagCrud;
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_list_opciones.html',
				controller: 'ModalTab1Ctrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
			// vm.flagCrud = (obj.crud);
			// vm.obj = (obj); 
			// vm.createEdit();    
			}, function () {

			});

		};

	    vm.openModalSave = function(event, size) {

	      var options = angular.element(event.target).data('options');
	      vm.obj.crud = vm.flagCrud;
	      var modalInstance = $uibModal.open({
	        backdrop: 'static',
            keyboard: false,
            templateUrl: 'modal_menu.html',
	        controller: 'ModalTab1Ctrl',
	        size: size,
	        backdropClass: 'splash' + ' ' + options,
	        windowClass: 'splash' + ' ' + options,
	        resolve: {
	          obj: function () {
	            return vm.obj;
	          }
	        }
	      });

	      modalInstance.result.then(function (obj) {
	        vm.flagCrud = (obj.crud);
	        vm.obj = (obj); 
	        vm.createEdit();    
	      }, function () {

	      });
	      };

	    vm.openModalDelete = function(event, size) {

	      var options = angular.element(event.target).data('options');
	      var modalInstance = $uibModal.open({
	        backdrop: 'static',
            keyboard: false,
            templateUrl: 'modal_menu_delete.html',
	        controller: 'ModalTab1Ctrl',
	        size: size,
	        backdropClass: 'splash' + ' ' + options,
	        windowClass: 'splash' + ' ' + options,
	        resolve: {
	          obj: function () {
	            return vm.obj;
	          }
	        }
	      });

	      modalInstance.result.then(function (obj) {
	        vm.delete(obj.id);      
	      }, function () {
	      	
	      });
	    };

	}	

	function ModalTab1Ctrl($scope, $crud, $uibModalInstance, obj,$menu,toastr){
	    var vm = $scope;

	    vm.obj = {};    

	    vm.obj = obj;

	    vm.id_menu = obj.id;
	    vm.flag = true;

		vm.listOpcion = function(id){
			vm.id_menu = id;
			$crud.especialId('menu/veropcion',id).then(function(res){
				vm.flag = false;
				vm.opciones = res.data.info;
			});
		};

		if (obj.id) {
			vm.listOpcion(vm.id_menu);
		}	    

	    vm.saveOpcion = function(d){
			vm.data = {'id_menu': vm.id_menu,'id_opcion':d.id};
			if (d.enable) {
				$menu.asignar('menu/asignaropcion',vm.data).then(function(res){
					if(res.data.success){
						toastr.success(res.data.message);
					}
					else{
						toastr.error(res.data.message);
					}
				});
			}
			else{
				$menu.asignar('menu/quitaropcion',vm.data).then(function(res){
					if(res.data.success){
						toastr.success(res.data.message);
					}
					else{
						toastr.error(res.data.message);
					}
				});
			}
		}

	    vm.ok = function () {
		  	$uibModalInstance.close(vm.obj);
		};

		vm.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}	

})()