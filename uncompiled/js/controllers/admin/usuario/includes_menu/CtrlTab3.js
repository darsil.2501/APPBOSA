(function(){
	'use strict';
	angular.module('app')
	.controller('Tab3Ctrl', Tab3Ctrl)
	.controller('ModalTab3Ctrl', ModalTab3Ctrl);

	function Tab3Ctrl($scope,$crud,$sweetalert,$uibModal){

		var vm = $scope;

		vm.obj = {};
		vm.flag = true;

		vm.list = function(){
			vm.flag = true;
			$crud.list('subopcion').then(function(res){
				vm.subopciones = res.data.info;
				vm.flag = false;
			});
		};

		vm.list();

		vm.load = function(data){
			vm.flagCrud = false;
			vm.obj = angular.copy(data);
		};

		vm.edit = function(){
			$crud.edit('subopcion',vm.obj).then(function(res){
				if($sweetalert.open(res)){
					vm.list();
					vm.clear();
				}
			});
		};

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = {};
		}

		vm.openModalSave = function(event, size) {

	      var options = angular.element(event.target).data('options');
	      vm.obj.crud = vm.flagCrud;
	      var modalInstance = $uibModal.open({
	        backdrop: 'static',
            keyboard: false,
            templateUrl: 'modal_subopcion.html',
	        controller: 'ModalTab3Ctrl',
	        size: size,
	        backdropClass: 'splash' + ' ' + options,
	        windowClass: 'splash' + ' ' + options,
	        resolve: {
	          obj: function () {
	            return vm.obj;
	          }
	        }
	      });

	      modalInstance.result.then(function (obj) {
	        vm.obj = (obj); 
	        vm.edit();    
	      }, function () {
	      	
	      });
	    };

		
	};

	function ModalTab3Ctrl($scope, $crud, $uibModalInstance, obj){
	    var vm = $scope;

	    vm.obj = {};    

	    vm.obj = obj;

	    vm.ok = function () {
		  	$uibModalInstance.close(vm.obj);
		};

		vm.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}

})()