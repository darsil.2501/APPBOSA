(function(){
	'use strict';
	angular.module('app')
	.controller('Tab2Ctrl', Tab2Ctrl)
	.controller('ModalTab2Ctrl', ModalTab2Ctrl);

	function Tab2Ctrl($scope,$crud,$sweetalert,$menu,toastr,$uibModal){

		var vm = $scope;

		vm.flagCrud = true;
		vm.flag = true;
		
		vm.list = function(){
			vm.flag = true;
			$crud.list('opcion').then(function(res){
				vm.opciones = res.data.info;
				vm.flag = false;
			});
		};

		vm.list();

		vm.load = function(data){
			vm.flagCrud = false;
			vm.obj = angular.copy(data);
		};

		vm.createEdit = function(){
			if(vm.flagCrud){
				$crud.create('opcion',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						vm.list();
						vm.clear();
					}
				});
			}
			else
			{
				$crud.edit('opcion',vm.obj).then(function(res){
					if($sweetalert.open(res)){
						vm.list();
						vm.clear();
					}
				});
			}
		};

		vm.delete = function(){
			$crud.delete('opcion',vm.obj.id).then(function(res){
				if($sweetalert.open(res)){
					vm.list();
					vm.clear();
				}
			});
		}		

		vm.clear = function(){
			vm.flagCrud = true;
			vm.obj = {};
		}
		

		vm.openModalAsignar = function(event, size) {

			var options = angular.element(event.target).data('options');
			vm.obj.crud = vm.flagCrud;
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'modal_list_subopciones.html',
				controller: 'ModalTab2Ctrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  obj: function () {
				    return vm.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
			// vm.flagCrud = (obj.crud);
			// vm.obj = (obj); 
			// vm.createEdit();    
			}, function () {

			});

		};

	    vm.openModalSave = function(event, size) {

	      var options = angular.element(event.target).data('options');
	      vm.obj.crud = vm.flagCrud;
	      var modalInstance = $uibModal.open({
	        backdrop: 'static',
            keyboard: false,
            templateUrl: 'modal_opcion.html',
	        controller: 'ModalTab2Ctrl',
	        size: size,
	        backdropClass: 'splash' + ' ' + options,
	        windowClass: 'splash' + ' ' + options,
	        resolve: {
	          obj: function () {
	            return vm.obj;
	          }
	        }
	      });

	      modalInstance.result.then(function (obj) {
	        vm.flagCrud = (obj.crud);
	        vm.obj = (obj); 
	        vm.createEdit();    
	      }, function () {

	      });
	      };

	    vm.openModalDelete = function(event, size) {

	      var options = angular.element(event.target).data('options');
	      var modalInstance = $uibModal.open({
	        backdrop: 'static',
            keyboard: false,
            templateUrl: 'modal_opcion_delete.html',
	        controller: 'ModalTab2Ctrl',
	        size: size,
	        backdropClass: 'splash' + ' ' + options,
	        windowClass: 'splash' + ' ' + options,
	        resolve: {
	          obj: function () {
	            return vm.obj;
	          }
	        }
	      });

	      modalInstance.result.then(function (obj) {
	        vm.delete(obj.id);      
	      }, function () {
	      	
	      });
	    };

	}	

	function ModalTab2Ctrl($scope, $crud, $uibModalInstance, obj,$menu,toastr){
	    var vm = $scope;

	    vm.obj = {};    

	    vm.obj = obj;

	    vm.id_opcion = obj.id;
	    vm.flag = true;

		vm.listSubopcion = function(id){
			vm.id_opcion = id;
			$crud.especialId('opcion/versubopcion',id).then(function(res){
				vm.subopciones = res.data.info;
				vm.flag = false;
			});
		};

		if (obj.id) {
			vm.listSubopcion(vm.id_opcion);
		}	    

	    vm.saveSubopcion = function(d){

			vm.data = {'id_opcion' : vm.id_opcion,'id_subopcion':d.id};

			if (d.enable) {
				$menu.asignar('opcion/asignarsubopcion',vm.data).then(function(res){
					if(res.data.success){
						toastr.success(res.data.message);
					}
					else{
						toastr.error(res.data.message);
					}
				});
			}else{
				$menu.asignar('opcion/quitarsubopcion',vm.data).then(function(res){
					if(res.data.success){
						toastr.success(res.data.message);
					}
					else{
						toastr.error(res.data.message);
					}
				});
			}

		}

	    vm.ok = function () {
		  	$uibModalInstance.close(vm.obj);
		};

		vm.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}
})()