	(function(){
	'use strict';
	angular.module('app')
	.controller('ContenedorSemanaCtrl', ContenedorSemanaCtrl);

	function ContenedorSemanaCtrl($scope,$crud,$sweetalert,$http){

		var vm = $scope;

		vm.obj = {};
		vm.flag = true;

		vm.obj.anio = 1;

		vm.listAnios = function(){
			$crud.listEspecial('anio/give_semanas').then(function(res){
				vm.anios = res.data.info;
				vm.obj.anio = (vm.anios[vm.anios.length-1].id);
				vm.consultarData(vm.obj.anio);
			});
		};
		vm.listAnios();

		vm.consultarData = function(id_anio)
		{
			vm.flag = true;
			$http.post('../Server/public/index.php/grafico/contenedores_semana_new/'+id_anio).then(function(res){
	  			vm.data = res.data.data_provider;
	  			vm.flag = false;
	  			vm.promedio_semanal = res.data.promedio_semanal;
	  			vm.getGrafico(res.data.data_provider);
  			});
		};

		// $http.post('../Server/public/index.php/grafico/contenedores_semana_new/'+null).then(function(res){
  // 			vm.data = res.data.data_provider;
  // 			vm.flag = false;
  // 			vm.promedio_semanal = res.data.promedio_semanal;
  // 			vm.getGrafico(res.data.data_provider);
  // 		});



		vm.getGrafico = function(data)
		{
			AmCharts.makeChart( "chartdiv", {
			  	"type": "serial",
			  	"theme": "light",
			  	"balloon": {
			    	"adjustBorderColor": false,
			    	"horizontalPadding": 10,
			    	"verticalPadding": 8,
			    	"color": "#ffffff"
			  	},
   			  	"dataProvider": data,
			  	"valueAxes": [ {
				    "axisAlpha": 0,
				    "position": "left",
				    "title": "Cantidad de Contenedores",
				    "minimum": 0
			  	}],
			  	"startDuration": 1,
			  	"graphs": [ {
				    "alphaField": "alpha",
				    "balloonText": "<span style='font-size:12px;'>[[title]] en [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
				    "fillAlphas": 1,
					// "fillColorsField": "color_barra",
				    "title": "Total Contenedores",
				    "type": "column",
				    "valueField": "total_contenedores",
				    "dashLengthField": "dashLengthColumn",
				    "labelText":"[[value]]",
				    "fontSize":10
			  	}, {
				    "id": "graph2",
				    "balloonText": "<span style='font-size:12px;'>[[title]] en [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "bulletSize": 7,
				    "bulletBorderAlpha": 1,
				    "bulletColor": "#FFFFFF",
				    "useLineColorForBulletBorder": true,
				    "bulletBorderThickness": 3,
				    "fillAlphas": 0,
					"lineColorField": "color_linea",
					// "fillColorsField": "color_barra",
				    "lineAlpha": 1,
				    "title": "Total Contenedores",
				    "valueField": "total_contenedores1",
				    "dashLengthField": "dashLengthLine"
			  	} ],
			  	"categoryField": "semana",
			  	"categoryAxis": {
				    "gridPosition": "start",
				    "axisAlpha": 0,
				    "tickLength": 0,
				    "labelRotation": 90,
				    "minorGridEnabled": true,
				    /* ENSURE 2 LINES BELOW ARE ADDED */
				    "autoGridCount": false,
				    "gridCount": data.length
			  	},
			  	"export": {
				    "enabled": true,
				    "menu": [ {
				      "class": "export-main",
				      "menu": [ {
				        "label": "Descargar",
				        "menu": [ "PNG", "JPG", "PDF" ]
				      }, 
				      { "format": "PRINT",
					    "label": "Imprimir"
					  }]
				    } ]			  
				},
				"responsive": {
				    "enabled": true
				}
			} );
		}

  		
    }	

})()