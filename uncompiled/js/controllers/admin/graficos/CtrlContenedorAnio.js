	(function(){
	'use strict';
	angular.module('app')
	.controller('ContenedorAnioCtrl', ContenedorAnioCtrl);

	function ContenedorAnioCtrl($scope,$crud,$sweetalert,$http){

		var vm = $scope;
		var chart;

		vm.flag = true;

  		$http.get('../Server/public/index.php/grafico/contenedores_anio_new').then(function(res){
  			vm.flag = false;
  			vm.data   = res.data.data_provider;
  			vm.getdata(vm.data,res.data.data_graphs);
  			var graph = new AmCharts.AmGraph();
			graph.valueField = "total_year";
			graph.labelText = "[[total_year]]";
			graph.visibleInLegend = false;
			graph.showBalloon = false;
			graph.lineAlpha = 0;
			graph.fontSize = 15;
			chart.addGraph(graph);
		    chart.validateData();
		    chart.animateAgain();
  		});

  		vm.getdata = function(data,data_graph){

			if(undefined === chart){

				chart = AmCharts.makeChart("chartdiv", {
				"type": "serial",
				"theme": "light",
				"legend": {
				"horizontalGap": 10,
				"maxColumns": 1,
				"position": "right",
				"useGraphSettings": true,
				"markerSize": 10
				},
				"dataProvider": data,
				"valueAxes": [{
				"stackType": "regular",
				"zeroGridAlpha": 1
				}],
				"graphs": data_graph,
				"categoryField": "year",
				"categoryAxis": {
				"gridPosition": "start"
				},
              "export": {
                "enabled": true,
                "menu": [ {
                  "class": "export-main",
                  "menu": [ {
                    "label": "Descargar",
                    "menu": [ "PNG", "JPG", "PDF" ]
                  }, 
                  { "format": "PRINT",
                  "label": "Imprimir"
                }]
                } ]
                    
              },
              "responsive": {
                "enabled": true
              }

				});
			}
  		}
    }	

})()