	(function(){
	'use strict';
	angular.module('app')
	.controller('ContenedorClienteCtrl', ContenedorClienteCtrl);

	function ContenedorClienteCtrl($scope,$crud,$sweetalert,$http){

		var vm = $scope;
		
		vm.flag = true;

		vm.obj = {};

		vm.obj.anio = 1;
		

    vm.listAnios = function(){
      $crud.listEspecial('anio/give_semanas').then(function(res){
        vm.anios = res.data.info;
        vm.obj.anio = (vm.anios[vm.anios.length-1].id);
        vm.consultarData(vm.obj.anio);
      });
    };
    vm.listAnios();


		// $http.post('../Server/public/index.php/grafico/contenedores_cliente_new/'+null).then(function(res){
		// 	vm.flag = false;
		// 	vm.data        = res.data.data_provider;
		// 	vm.getGrafico(res.data.data_provider,res.data.data_graphs);
		// });

		vm.consultarData = function(anio){
			vm.flag = true;
			$http.post('../Server/public/index.php/grafico/contenedores_cliente_new/'+anio).then(function(res){
				vm.flag = false;
				vm.data        = res.data.data_provider;
				vm.getGrafico(res.data.data_provider,res.data.data_graphs);
			});
		}

	    vm.titleGraph  = 'Contenedores enviados mensualmente a los clientes';
		
		
		// /*Config graphic*/
		// vm.type = 'bar';
	 //    vm.colors = ['#97BBCD', '#FDB45C', '#F7464A', '#46BFBD', '#FDB45C'];
	 //    vm.labels = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre"
	 //    ,"Octubre","Noviembre","Diciembre"];
	 //    vm.options = { legend: { display: true },scales: {
	 //        yAxes: [{ticks: {min: 0,}}]
	 //      } };

	 	vm.getGrafico = function(data,data_graph){
            
            var chart = AmCharts.makeChart("chartdiv", {
              "type": "serial",
              "theme": "light",
              "legend": {
              "horizontalGap": 10,
              "maxColumns": 10,
              "position": "top",
              "useGraphSettings": true,
              "markerSize": 10
              },
              "categoryField": "mes",
              "startDuration": 1,
              "categoryAxis": {
                "gridPosition": "start",
                "position": "left",
                "labelRotation": 45,
              },
              "trendLines": [],
              "graphs": data_graph,
              "guides": [],
              "valueAxes": [
                {
                  "id": "ValueAxis-1",
                  "position": "left",
                  "axisAlpha": 0,
                  "minimum": 0
                }
              ],
              "allLabels": [],
              "balloon": {},
              "titles": [],
              "dataProvider": data,
              "export": {
                "enabled": true,
                "menu": [ {
                  "class": "export-main",
                  "menu": [ {
                    "label": "Descargar",
                    "menu": [ "PNG", "JPG", "PDF" ]
                  }, 
                  { "format": "PRINT",
                  "label": "Imprimir"
                }]
                } ]
                    
              },
              "responsive": {
                "enabled": true
              }

            });
        }
    		

  }	

})()