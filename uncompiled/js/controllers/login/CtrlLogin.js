(function(){
	'use strict';
	angular.module('app')
	.controller('LoginCtrl', LoginCtrl);

	function LoginCtrl($scope,$usuario,toastr,$window){

		var vm = $scope;

		vm.anio = new Date().getFullYear();
		vm.flag = false;	

		vm.logueo = function(){
			vm.flag = true;
			if (vm.user) {
				$usuario.userLogin(vm.user).then(function(res){
					if(!res.data.success){
						vm.flag = false;
						toastr.error(res.data.message);
					}
				});
			}
		};

	};

})()