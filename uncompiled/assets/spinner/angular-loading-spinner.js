(function () {
    angular.module('ngLoadingSpinner', ['angularSpinner'])
            .directive('usSpinner', ['$http', '$rootScope', function ($http, $rootScope) {
                    return {
                        link: function (scope, elm, attrs)
                        {
                            $rootScope.spinnerActive = false;
                            scope.isLoading = function () {
                                // Se agregó esta línea de código para que no saliera el spinner al consulta esta ruta
                                if (angular.isDefined($http.pendingRequests[0])) {
                                    return !angular.equals($http.pendingRequests[0].url,'../Server/public/index.php/contenedor/getdata');
                                }
                                // ------------------------------------------------------------
                                return $http.pendingRequests.length > 0;
                            };

                            scope.$watch(scope.isLoading, function (loading)
                            {
                                $rootScope.spinnerActive = loading;
                                if (loading) {
                                    elm.removeClass('ng-hide');
                                } else {
                                    elm.addClass('ng-hide');
                                }
                            });
                        }
                    };

                }]);
}).call(this);