<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	{{-- <link rel="stylesheet" href="../assets/pdf/estilos.css"> --}}
	<link rel="stylesheet" href="assets/pdf/estilos.css">
</head>
<body>
		<center><img src="assets/pdf/logo.png" alt="logo" width="20%" style="float: left;"></center>
		{{-- <img src="../assets/pdf/logo.png" alt="logo" width="20%" style="float: left;"> --}}
		<p class="text-center">
			<strong>ASOCIACIÓN DE PEQUEÑOS PRODUCTORES DE BANANO ORGÁNICO SAMAN Y ANEXOS</strong><br>
			Sector Nueva Esperanza S/N Sullana - Perú, Telef: 504119
		</p>
		{{-- <h4 class="text-center">Sector Nueva Esperanza S/N Sullana - Perú, Telef: 504119</h4> --}}
		<div class="row">
			<div class="table-responsive">
				<table class="table table-hover" border="1" width="100%">
					<thead>
						<tr>
							<th class="font-size-11">CÓDIGO DE PRODUCTOR</th>
							<th class="font-size-11">DNI</th>
							<th class="font-size-11">APELLIDOS Y NOMBRES</th>
							<th class="font-size-11">GGN</th>
							<th class="font-size-11">¿ES SOCIO?</th>
							<th class="font-size-11">N° DE PARCELAS</th>
							<th class="font-size-11">FECHA DE INGRESO</th>
							<th class="font-size-11">CONDICIÓN</th>
						</tr>
					</thead>
					<tbody>
						@foreach($productores as $productor)
						<tr>
							<td style="vertical-align: middle;white-space: nowrap;" class="font-size-11 text-center">{{$productor->codigo_productor}}</td>
							<td style="vertical-align: middle;white-space: nowrap;" class="font-size-11 text-center">{{$productor->dni}}</td>
							<td style="vertical-align: middle;white-space: nowrap;" class="font-size-11">{{$productor->fullname}}</td>
							<td style="vertical-align: middle;white-space: nowrap;" class="font-size-11 text-center">{{$productor->ggn}}</td>
							<td style="vertical-align: middle;white-space: nowrap;" class="font-size-11 text-center">{{ ($productor->socio == 1) ? 'SÍ':'NO'}}</td>
							<td style="vertical-align: middle;white-space: nowrap;" class="font-size-11 text-center">{{$productor->numero_parcelas}}</td>
							<td style="vertical-align: middle;white-space: nowrap;" class="font-size-11 text-center">{{$productor->fecha_ingreso}}</td>
							<td style="vertical-align: middle;white-space: nowrap; background-color: {{$productor->color_condicion}}; color: white;" class="font-size-11 text-center">{{$productor->nombre_condicion}}</td>
						@endforeach
					</tbody>
				</table>
				<p class="text-right font-size-11">FECHA DE CONSULTA: {{date('d-m-Y')}} - LGRC</p>
			</div>
		</div>
</body>
</html>