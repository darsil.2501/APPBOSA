<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>REPORTE SIC</title>
		<style type="text/css">

			body{

				font-family: Calibri, sans-serif;
				font-size: 9px;

			}

			.table{
				border-collapse: collapse;
				border: 2px solid #424242;
			}

			.td{
				border-collapse: collapse;
				border: 1px solid #424242;
			}

			.th{
				border-collapse: collapse;
				border: 1px solid #424242;
			}
		</style>
	</head>
	<body>
		<table border="0" width="100%" style="border-style: solid;">
			<thead>
				<tr>
					<th align="center" style="font-weight: bold;font-size: 11px;">
						VISITA N° : {{$data['visita']['id']}}
					</th>
				</tr>
			</thead>
		</table>
		<table border="0" width="100%">
			<thead>
				<tr>
					<th align="left" width="33.33333333%">
						<img src="assets/logo_mini.png" alt="logo">
					</th>
					<th align="center">
						<table border="0" width="100%" style="border-style: solid;">
							<thead>
								<tr>
									<th align="center" style="font-weight: bold;font-size: 11px;">
										SISTEMA INTERNO DE CONTROL - COOPERATIVA AGRARIA APPBOSA
									</th>
								</tr>
								<tr>
									<th align="center" style="font-weight: bold;font-size: 11px;">
										AUDITORIA INTERNA - SIC
									</th>
								</tr>
							</thead>
						</table>
					</th>
				</tr>
			</thead>
		</table>

		<table border="0" width="100%" style="border-style: solid;">
			<tbody>
				<tr>
					<td align="center" style="font-weight: bold;">
						NOMBRE DE PRODUCTOR:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['productor'])}}
					</td>
					<td align="center" style="font-weight: bold;">
						RESPONSABLE DE CAMPO:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['responsable'])}}
					</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold;">
						CÓDIGO DEL PRODUCTOR:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['codigo_productor'])}}
					</td>
					<td align="center" style="font-weight: bold;">
						FECHA DE AUDITORIA:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['fecha'])}}
					</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold;">
						INSPECTOR RESPONSABLE:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['inspector'])}}
					</td>
					<td align="center" style="font-weight: bold;">
						ÁREA:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['area_parcela'])}} HA
					</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold;">
						EMPACADORA:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['empacadora'])}}
					</td>
					<td align="center" style="font-weight: bold;">
						SECTOR:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['sector'])}}
					</td>
				</tr>
			</tbody>
		</table>
		<br>
		<table class="table" width="100%">
			<thead style="border: 2px solid #424242;">
				<tr>
					<th class="th" align="center">N°</th>
					<th class="th" align="center">PUNTO A EVALUAR</th>
					<th class="th" align="center">CUMPLE CON LAS NORMAS</th>
					<th class="th" align="center">OBSERVACIONES</th>
				</tr>
			</thead>
			<tbody>
				@foreach($data['formulario'] as $key => $pregunta)
				<tr>
					<td class="td" align="center" style="vertical-align: middle;white-space: nowrap;">{{$key+1}}</td>
					<td class="td" style="vertical-align: middle;">{{mb_strtoupper($pregunta['titulo'])}}</td>
					<td class="td" align="center" style="vertical-align: middle;white-space: nowrap;">{{mb_strtoupper($pregunta['respuesta'])}}</td>
					<td class="td" style="vertical-align: middle;">{{mb_strtoupper($pregunta['observacion'])}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<br>
		<table border="0" width="100%" style="border-style: solid;">
			<tbody>
				<tr>
					<td align="left" style="font-weight: bold;">
						OBSEVACIONES GENERALES
					</td>
				</tr>
				<tr>
					<td align="left">
						{{mb_strtoupper($data['visita']['observacion'])}}
					</td>
				</tr>
			</tbody>
		</table>
		<br>
		<br>
		<br>
		<table border="0" width="100%" style="border-style: solid;">
			<tbody>
				<tr>
					<td align="center" style="font-weight: bold;">
						______________________________________ <br>
									FIRMA EVALUADOR
					</td>
					<td align="center" style="font-weight: bold;">
						______________________________________ <br>
									FIRMA PRODUCTOR
					</td>
				</tr>
			</tbody>
		</table>
		<div style="page-break-after: always;"></div>
		<h3 align="center">EVIDENCIAS</h3>
		<h4>{{(count($data['imagenes']) > 0) ? '' : 'NO HAY IMAGENES'}}</h4>
		@foreach($data['imagenes'] as $key => $imagen)
			<img style="margin-bottom: 10px;" src="archivos/galeria_visita/{{$imagen['url']}}" width="100%" alt="{{$imagen['url']}}">
		@endforeach
	</body>
</html>