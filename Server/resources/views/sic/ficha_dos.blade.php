<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>REPORTE SIC</title>
		<style type="text/css">

			body{

				font-family: Calibri, sans-serif;
				font-size: 9px;

			}

			.table{
				border-collapse: collapse;
				border: 2px solid #424242;
			}

			.td{
				border-collapse: collapse;
				border: 1px solid #424242;
			}

			.th{
				border-collapse: collapse;
				border: 1px solid #424242;
			}
		</style>
	</head>
	<body>
		<table border="0" width="100%" style="border-style: solid;">
			<thead>
				<tr>
					<th align="center" style="font-weight: bold;font-size: 11px;">
						VISITA N° : {{$data['visita']['id']}}
					</th>
				</tr>
			</thead>
		</table>
		<table border="0" width="100%">
			<thead>
				<tr>
					<th align="left" width="33.33333333%">
						<img src="assets/logo_mini.png" alt="logo">
					</th>
					<th align="center">
						<table border="0" width="100%" style="border-style: solid;">
							<thead>
								<tr>
									<th align="center" style="font-weight: bold;font-size: 11px;">
										SISTEMA INTERNO DE CONTROL - COOPERATIVA AGRARIA APPBOSA
									</th>
								</tr>
								<tr>
									<th align="center" style="font-weight: bold;font-size: 11px;">
										EVALUACIÓN DE PLAGAS Y CALIDAD PREVENTIVA
									</th>
								</tr>
							</thead>
						</table>
					</th>
				</tr>
			</thead>
		</table>

		<table border="0" width="100%" style="border-style: solid;">
			<tbody>
				<tr>
					<td align="center" style="font-weight: bold;">
						NOMBRE DE PRODUCTOR:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['productor'])}}
					</td>
					<td align="center" style="font-weight: bold;">
						RESPONSABLE DE CAMPO:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['responsable'])}}
					</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold;">
						CÓDIGO DEL PRODUCTOR:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['codigo_productor'])}}
					</td>
					<td align="center" style="font-weight: bold;">
						FECHA DE AUDITORIA:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['fecha'])}}
					</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold;">
						INSPECTOR RESPONSABLE:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['inspector'])}}
					</td>
					<td align="center" style="font-weight: bold;">
						ÁREA:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['area_parcela'])}} HA
					</td>
				</tr>
				<tr>
					<td align="center" style="font-weight: bold;">
						EMPACADORA:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['empacadora'])}}
					</td>
					<td align="center" style="font-weight: bold;">
						SECTOR:
					</td>
					<td align="center">
						{{mb_strtoupper($data['visita']['sector'])}}
					</td>
				</tr>
			</tbody>
		</table>
		<br>
		<table class="table" width="100%">
			<thead style="border: 2px solid #424242;">
				<tr>
					<th class="th" align="center">LABOR</th>
					<th class="th" align="center">PARAMETROS</th>
					<th class="th" align="center">PUNTO EVALUAR</th>
					<th class="th" align="center">TOTAL</th>
					<th class="th" align="center">PROM.</th>
					<th class="th" align="center">PROM. / LABOR</th>
				</tr>
			</thead>
			<tbody>
				@foreach($data['formulario'] as $labor)
					<tr>
						<td rowspan="{{count($labor['items'])}}" class="td" align="center" style="vertical-align: middle;white-space: nowrap;">{{mb_strtoupper($labor['nombre'])}}</td>
						<td class="td" style="vertical-align: middle;">{{mb_strtoupper($labor['items'][0]['nombre'])}}</td>
						<td class="td" align="center" style="vertical-align: middle;">{{mb_strtoupper($labor['items'][0]['punto_evaluar'])}}</td>
						<td class="td" align="center" style="vertical-align: middle;">{{($labor['items'][0]['total'])}}</td>
						<td class="td" align="center" style="vertical-align: middle;">{{number_format(($labor['items'][0]['total'])/10,2)}}</td>
						<td rowspan="{{count($labor['items'])}}" class="td" align="center" style="vertical-align: middle;white-space: nowrap;">
							{{(mb_strtoupper($labor['nombre']) === 'PLAGAS') ? 'NIVEL POBLACIONAL' : number_format(($labor['total_labor']/count($labor['items'])),2)}}
						</td>
					</tr>
					@foreach(array_slice($labor['items'],1) as $item)
						<tr>
							<td class="td" align="left" style="vertical-align: middle;white-space: nowrap;">{{mb_strtoupper($item['nombre'])}}</td>
							<td class="td" align="center" style="vertical-align: middle;white-space: nowrap;">{{mb_strtoupper($item['punto_evaluar'])}}</td>
							<td class="td" align="center" style="vertical-align: middle;white-space: nowrap;">{{($item['total'])}}</td>
							<td class="td" align="center" style="vertical-align: middle;white-space: nowrap;">{{number_format(($item['total']/10),2)}}</td>
						</tr>
					@endforeach
				@endforeach
			</tbody>
		</table>
		<br>
		<table border="0" width="100%" style="border-style: solid;">
			<tbody>
				<tr>
					<td align="left" style="font-weight: bold;">
						OBSEVACIONES GENERALES
					</td>
				</tr>
				<tr>
					<td align="left">
						{{mb_strtoupper($data['visita']['observacion'])}}
					</td>
				</tr>
			</tbody>
		</table>
		<br>
		<br>
		<br>
		<table border="0" width="100%" style="border-style: solid;">
			<tbody>
				<tr>
					<td align="center" style="font-weight: bold;">
						______________________________________ <br>
									FIRMA EVALUADOR
					</td>
					<td align="center" style="font-weight: bold;">
						______________________________________ <br>
									FIRMA PRODUCTOR
					</td>
				</tr>
			</tbody>
		</table>
		<div style="page-break-after: always;"></div>
		<h3 align="center">EVIDENCIAS</h3>
		<h4>{{(count($data['imagenes']) > 0) ? '' : 'NO HAY IMAGENES'}}</h4>
		@foreach($data['imagenes'] as $key => $imagen)
			<img style="margin-bottom: 10px;" src="archivos/galeria_visita/{{$imagen['url']}}" width="100%" alt="{{$imagen['url']}}">
		@endforeach
	</body>
</html>