<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>LLENADO DE CONTENDOR</title>
		<style type="text/css">

			body{

				font-family: Calibri, sans-serif;
				font-size: 9px;
				color: #1A5276;

			}

			.table{
				border-collapse: collapse;
				border: 1px solid #1A5276;
			}

			.td{
				border-collapse: collapse;
				border: 1px solid #1A5276;
			}

			.th{
				border-collapse: collapse;
				border: 1px solid #1A5276;
			}
		</style>
	</head>
	<body>
		<table border="0" width="100%">
			<thead>
				<tr>
					<th align="left" width="33.33333333%">
						<img src="assets/logo_mini.png" alt="logo">
					</th>
					<th align="center">
						<table border="0" width="100%" style="border-style: solid;">
							<thead>
								<tr>
									<th align="center" style="font-weight: bold;font-size: 11px;">
										<strong>COOPERATIVA AGRARIA APPBOSA</strong>
									</th>
								</tr>
								<tr>
									<th align="center" style="font-weight: bold;font-size: 11px;">
										Sector Nueva Esperanza S/N Samán ,Sullana Perú-Telef.: 504119
									</th>
								</tr>
							</thead>
						</table>
					</th>
				</tr>
			</thead>
		</table>
		
		<br>
		<table style="width: 100%">
			<tbody>
				<tr>
					<td width="40%"  valign="top">
						<table>
							<thead>
								<tr>
									<th class="th">
										CONTENEDOR
									</th>									
									<th class="th">
										{{ $data['contenedor']['numero_contenedor'] }}
									</th>
								</tr>
								<tr>
									<th class="th">
										N° BOOKING
									</th>									
									<th class="th">
										{{ $data['contenedor']['booking'] }}
									</th>
								</tr>
								<tr>
									<th class="th">
										SEMANA
									</th>									
									<th class="th">
										{{ $data['contenedor']['semana'] }}
									</th>
								</tr>
								<tr>
									<th colspan="{{ count($data['contenedor']['cajas']) }}" class="th" align="center">
										CAJAS
									</th>
								</tr>
								<tr>
									@foreach($data['contenedor']['cajas'] as $key => $caja)
									<th class="th">
										{{ $caja['cantidad_cajas'] }} - {{ $caja['marca'] }}
									</th>
									@endforeach
								</tr>
							</thead>
						</table>
					</td>
					<td width="60%"  valign="top">
						<table style="width: 100%">
							<tbody>
								<tr>
									<td width="50%" valign="top">
										@foreach($data['llenados'] as $key => $llenado)
										@if($key%2==0)
										<table class="table" style="width: 100%">
											<thead>
												<tr>
													<th align="center" class="th" colspan="2">
														PALLET {{ $key + 1 }}
													</th>
												</tr>
												<tr>
													<th align="center" class="th">
														CODIGO
													</th>
													<th align="center" class="th">
														CAJAS
													</th>
												</tr>
											<tbody>
												@foreach($llenado as $k => $pallet)
												<tr>
													<td align="center" class="td" width="50%">
														{{ $pallet['codigo_parcela'] }}
													</td>
													<td align="center" class="td" width="50%">
														{{ $pallet['cajas'] }}
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
										@endif
										@endforeach
									</td>
									<td width="50%" valign="top">
										@foreach($data['llenados'] as $key => $llenado)
										@if($key%2!=0)
										<table class="table" style="width: 100%">
											<thead>
												<tr>
													<th align="center" class="th" colspan="2">
														PALLET {{ $key + 1 }}
													</th>
												</tr>
												<tr>
													<th align="center" class="th">
														CODIGO
													</th>
													<th align="center" class="th">
														CAJAS
													</th>
												</tr>
											<tbody>
												@foreach($llenado as $k => $pallet)
												<tr>
													<td align="center" class="td" width="50%">
														{{ $pallet['codigo_parcela'] }}
													</td>
													<td align="center" class="td" width="50%">
														{{ $pallet['cajas'] }}
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
										@endif
										@endforeach
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<br>
	</body>
</html>