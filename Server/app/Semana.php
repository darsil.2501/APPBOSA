<?php

namespace App;

use App\Anio;
use App\Certificado;
use App\Contenedor;
use App\Factura;
use App\OtrosArchivos;
use App\Packing;
use App\Cinta;
use App\Enfunde;
use App\Recobro;
use Illuminate\Database\Eloquent\Model;


class Semana extends Model
{
	protected $table    = 'semana';
	protected $fillable = ['nombre','id_anio','id_cinta','is_close','activo'];
	public $timestamps  = false;

	public function contenedor()
	{
		return $this->hasMany(Contenedor::class,'id_semana');
	}

	public function anio()
	{
		return $this->belongsTo(Anio::class,'id_anio');
	}

	public function facturas()
    {
        return $this->hasMany(Factura::class,'id_semana');
    }

    public function certificados()
    {
        return $this->hasMany(Certificado::class,'id_semana');
    }

    public function packings()
    {
        return $this->hasMany(Packing::class,'id_semana');
    }

    public function otros_archivos()
    {
    	return $this->hasMany(OtrosArchivos::class,'id_semana');	
    }

    public function cinta()
    {
        return $this->belongsTo(Cinta::class,'id_cinta');
    }

    public function enfundes()
    {
        return $this->hasMany(Enfunde::class,'id_semana');
    }

    public function cosechas()
    {
        return $this->hasMany(Recobro::class,'id_semana');
    }

}
