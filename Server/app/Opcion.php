<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Menu;
use App\Subopcion;

class Opcion extends Model
{

    protected $table    = 'opcion';
    protected $fillable = ['nombre','url','icono','id_menu','activo'];
    protected $hidden   = ['pivot'];
    public $timestamps  = false;


    public function menu()
    {
    	return $this->belongsTo(Menu::class,'id_menu');
    }

    public function subopciones()
    {
    	return $this->hasMany(Subopcion::class);
    }
}
