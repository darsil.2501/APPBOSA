<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Semana;

class Anio extends Model
{
    protected $table    = 'anio';
	protected $fillable = ['anio','activo'];
	public $timestamps  = false;

	public function semana()
	{
		return $this->hasMany(Semana::class,'id_anio');
	}
}
