<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Labor;

class ItemLabor extends Model
{
    protected $table    = 'item_labor';
    protected $fillable = ['nombre','punto_evaluar','id_labor','activo'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    public function labor()
    {
    	return $this->belongsTo(Labor::class,'id_labor');
    }

}
