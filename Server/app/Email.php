<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cliente;

class Email extends Model
{
    protected $table    = 'emails';
	protected $fillable = ['id_cliente','email','activo'];
	public $timestamps  = false;

	public function cliente()
	{
		return $this->belongsTo(Cliente::class,'id_cliente');
	}
}
