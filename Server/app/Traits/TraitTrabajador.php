<?php

namespace App\Traits;
use App\Persona;
use App\Productor;
use App\ProductorTrabajador;
use App\Trabajador;
use Illuminate\Support\Facades\DB;

trait TraitTrabajador
{
	# Obtener todos los trabajadores de un productor
	public static function obtenerTrabajadoresPorProductor($id_productor)
	{
		try{
            $trabajadores = ProductorTrabajador::where('id_productor',$id_productor)->where('activo',1)->get();
            $data = array();
            foreach ($trabajadores as $value) {
                $registro = ($value->is_productor == 1) ? Productor::find($value->id_trabajador) : Trabajador::find($value->id_trabajador);
                $data[] = array(
                    // 'dni'              => ($value->is_productor == 1) ? $value->productor->persona->dni : $value->trabajador->persona->dni,
                    // 'nombre'           => ($value->is_productor == 1) ? $value->productor->persona->nombre : $value->trabajador->persona->nombre,
                    // 'apellido_paterno' => ($value->is_productor == 1) ? $value->productor->persona->apellido_paterno : $value->trabajador->persona->apellido_paterno,
                    // 'apellido_materno' => ($value->is_productor == 1) ? $value->productor->persona->apellido_materno : $value->trabajador->persona->apellido_materno,
                    // 'fullname'         => ($value->is_productor == 1) ? $value->productor->persona->fullname : $value->trabajador->persona->fullname,
                    'id'               => $value->id,
                    'dni'              => $registro->persona->dni,
                    'nombre'           => $registro->persona->nombre,
                    'apellido_paterno' => $registro->persona->apellido_paterno,
                    'apellido_materno' => $registro->persona->apellido_materno,
                    'fullname'         => $registro->persona->fullname,
                    'id_parentesco'    => $value->parentesco->id,
                    'nombre_parentesco'=> $value->parentesco->nombre,
                    'id_condicion'     => $value->id_condicion,
                    'nombre_condicion' => ($value->id_condicion == 1) ? 'Permanente' : 'Eventual',
                    'descripcion'      => $value->descripcion,
                    'is_productor'     => $value->is_productor
                );
            }

            $collecion = collect($data);
            $ordenar   = $collecion->sortBy('apellido_paterno');
            $data      = $ordenar->values()->all();
            
            return response()->json(['info'=>$data,'success'=>true]);    
        }catch(Exception $e){
            return response()->json(['info'=>'Error al listar los registros'.$e->getMessage(),'success'=>false]);    
        }
	}

    public static function obtenerTrabajadores()
    {
        try{
            $trabajadores = Trabajador::where('activo',1)->get();
            $data = array();
            foreach ($trabajadores as $trabajador) {
                $data[] = array(
                    'id'               => $trabajador->id,
                    'id_persona'       => $trabajador->persona->id,
                    'dni'              => $trabajador->persona->dni,
                    'nombre'           => $trabajador->persona->nombre,
                    'apellido_paterno' => $trabajador->persona->apellido_paterno,
                    'apellido_materno' => $trabajador->persona->apellido_materno,
                    'fullname'         => $trabajador->persona->fullname,
                );
            }

            $collecion = collect($data);
            $ordenar   = $collecion->sortBy('apellido_paterno');
            $data      = $ordenar->values()->all();
            
            return response()->json(['info'=>$data,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Error al listar los registros'.$e->getMessage(),'success'=>false]);    
        }
    }

	public static function registrarTrabajador($request)
	{
		try{
            DB::beginTransaction();
                switch ($request->operacion) {
                    case 1:
                        self::nuevoTrabajador($request);
                        break;
                    case 2:
                        self::trabajadorExistente($request);
                        break;

                    case 3:
                        self::productorExistente($request);
                        break;
                    case 4:
                        self::productorMismo($request);
                        break;
                }
            DB::commit();
            // self::auditarProductor('INSERTAR',$full_name);
            return response()->json(['message'=>'Trabajador creado correctamente','success'=>true]);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['message'=>'Error el ralizar la operación.Error: '.$ex->getMessage(),'success'=>false]);
        }
	}

	public static function actualizarTrabajador($request, $id)
	{
		try{
            DB::beginTransaction();
                $registro = ProductorTrabajador::find($id);
                # Validar que la persona a actualizar no sea un productor
                if($registro->is_productor != 1){
                    $persona    = Persona::find(Trabajador::find($registro->id_persona));
                    $validar    = Trabajador::validateUpdateProductor($request->all(),$persona->id_persona);

                    if($validar->fails()){return response()->json(['success'=>false,'messages'=>$validar->errors()->all()]);}
                    
                    $full_name = $request['nombre'].' '.$request['apellido_paterno'].' '.$request['apellido_materno'];
                    $persona->fill([
                        'dni'               =>  $request->dni,
                        'nombre'            =>  $request->nombre,
                        'apellido_paterno'  =>  $request->apellido_paterno,
                        'apellido_materno'  =>  $request->apellido_materno,
                        'fullname'          =>  $full_name,
                    ])->save();

                    $registro->fill([
                        'id_parentesco' => $request->id_parentesco,
                        'id_condicion'  => $request->id_condicion,
                        'descripcion'   => $request->descripcion
                    ])->save();
                }else{
                    $registro->fill([
                        'id_parentesco' => $request->id_parentesco,
                        'id_condicion'  => $request->id_condicion,
                        'descripcion'   => $request->descripcion
                    ])->save();
                }
            DB::commit();
            // self::auditarProductor('ACTUALIZAR',$full_name);
            return response()->json(['success'=>true,'message'=>'Trabajador actualizado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>true,'message'=>'Error al actualizar datos del Productor. Error: '.$ex->getMessage()]);
        }
	}

	public static function eliminarTrabajador($id)
	{
		try{
            // if(!self::hasPermiso('productor.eliminar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $registro = ProductorTrabajador::find($id);
                $registro->fill(['activo' => DB::raw(0)])->save();
            DB::commit();
            // self::auditarProductor('ELIMINAR',$productor->persona->fullname);
            return response()->json(['success'=>true,'message'=>'Trabajador eliminado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el Productor. Error:' .$ex->getMessage()]);
        }
	}




    // Funciones para agregar un trabajador de acuerdo a lo que es
    public static function nuevoTrabajador($request)
    {
        $full_name = $request['nombre'].' '.$request['apellido_paterno'].' '.$request['apellido_materno'];
        $persona   = Persona::create([
            'dni'               =>  $request->dni,
            'nombre'            =>  $request->nombre,
            'apellido_paterno'  =>  $request->apellido_paterno,
            'apellido_materno'  =>  $request->apellido_materno,
            'fullname'          =>  $full_name
        ]);
        
        $trabajador = Trabajador::create(['id_persona'=>$persona->id]);
        ProductorTrabajador::create([
            'id_productor'  => $request->id_productor,
            'id_trabajador' => $trabajador->id,
            'id_parentesco' => $request->id_parentesco,
            'id_condicion'  => $request->id_condicion,
            'descripcion'   => $request->descripcion,
            'is_productor'  => 0
        ]);
    }

    public static function trabajadorExistente($request)
    {
        ProductorTrabajador::create([
            'id_productor'  => $request->id_productor,
            'id_trabajador' => $request->id_trabajador,
            'id_parentesco' => $request->id_parentesco,
            'id_condicion'  => $request->id_condicion,
            'descripcion'   => $request->descripcion,
            'is_productor'  => 0
        ]);
    }

    public static function productorExistente($request)
    {
        ProductorTrabajador::create([
            'id_productor'  => $request->id_productor,
            'id_trabajador' => $request->id_trabajador,
            'id_parentesco' => $request->id_parentesco,
            'id_condicion'  => $request->id_condicion,
            'descripcion'   => $request->descripcion,
            'is_productor'  => 1
        ]);
    }

    public static function productorMismo($request)
    {
        ProductorTrabajador::create([
            'id_productor'  => $request->id_productor,
            'id_trabajador' => $request->id_productor,
            'id_parentesco' => $request->id_parentesco,
            'id_condicion'  => $request->id_condicion,
            'descripcion'   => $request->descripcion,
            'is_productor'  => 1
        ]);
    }
}