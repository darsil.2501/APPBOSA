<?php

namespace App\Traits;

use App\Parcela;
use App\Persona;
use App\Productor;
use Illuminate\Support\Facades\DB;

trait TraitProductor
{
	# Obtener todos los productores con su respectiva información
	public static function obtenerProductores()
	{
		try{
            $productores = Productor::with('persona')->where('activo',1)->get();
            $data        = array();
            foreach ($productores as $productor) {
                $data[] = array(
                    'id'              =>$productor->id,
                    'id_persona'      =>$productor->persona->id,
                    'dni'             =>$productor->persona->dni,
                    'nombre'          =>$productor->persona->nombre,
                    'apellido_paterno'=>$productor->persona->apellido_paterno,
                    'apellido_materno'=>$productor->persona->apellido_materno,
                    'fullname'        =>$productor->persona->fullname,
                    'direccion'       =>$productor->persona->direccion,
                    'socio'           =>$productor->socio,
                    'codigo_productor'=>$productor->codigo_productor,
                    'ggn'             =>$productor->ggn,
                    'fecha_ingreso'   =>$productor->fecha_ingreso,
                    'fecha_cese'      =>$productor->fecha_cese,
                    'activo'          =>$productor->activo,
                    'id_persona'      =>$productor->persona->id,
                    'id_condicion'    =>$productor->id_condicion,
                    'nombre_condicion'=>$productor->condicion_productor->nombre,
                    'color_condicion' =>$productor->condicion_productor->color,
                    // 'motivo'          =>(!empty($productor->motivo)) ? $productor->motivo->nombre : null,
                    'cantidad_parcelas'=>$productor->parcelas->count()
                );
            }            

            $collecion = collect($data);
            $ordenar   = $collecion->sortBy('apellido_paterno');
            $data      = $ordenar->values()->all();
            
            return response()->json(['info'=>$data,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Error al listar los registros'.$e->getMessage(),'success'=>false]);    
        }
	}

	# Registrar un nuevo productor
	public static function registrarProductor($request)
	{
		try{
            // if(!self::hasPermiso('productor.registrar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $full_name = $request['nombre'].' '.$request['apellido_paterno'].' '.$request['apellido_materno'];
                $persona   = Persona::create([
                    'dni'               =>  $request['dni'],
                    'nombre'            =>  $request['nombre'],
                    'apellido_paterno'  =>  $request['apellido_paterno'],
                    'apellido_materno'  =>  $request['apellido_materno'],
                    'fullname'          =>  $full_name,
                    'direccion'         =>  $request['direccion']
                ]);
                
                Productor::create([
                    'id_persona'       => $persona->id,
                    'socio'            => $request['socio'],
                    'codigo_productor' => $request['codigo_productor'],
                    'fecha_ingreso'    => $request['fecha_ingreso'],
                    // 'fecha_cese'       => $request['fecha_cese'],
                ]);

            DB::commit();
            // self::auditarProductor('INSERTAR',$full_name);
            return response()->json(['message'=>'Productor creado correctamente','success'=>true]);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['message'=>'Error el ralizar la operación.Error: '.$ex->getMessage(),'success'=>false]);
        }
	}

	# Actualizar datos de un Productor
	public static function actualizarProductor($request, $id_productor)
	{
		try{
            // if(!self::hasPermiso('productor.actualizar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $productor = Productor::find($id_productor);
                $persona   = Persona::find($productor->id_persona);
                $validar   = Productor::validateUpdateProductor($request->all(),$productor->id_persona);

                if($validar->fails()){return response()->json(['success'=>false,'messages'=>$validar->errors()->all()]);}
                
                $full_name = $request['nombre'].' '.$request['apellido_paterno'].' '.$request['apellido_materno'];
                $persona->fill([
                    'dni'               =>  $request['dni'],
                    'nombre'            =>  $request['nombre'],
                    'apellido_paterno'  =>  $request['apellido_paterno'],
                    'apellido_materno'  =>  $request['apellido_materno'],
                    'direccion'         =>  $request['direccion'],
                    'fullname'          =>  $full_name,
                ])->save();

                $productor->fill([
                    'socio'            => $request['socio'],
                    'codigo_productor' => $request['codigo_productor'],
                    'ggn'              => $request['ggn'],
                    'fecha_ingreso'    => $request['fecha_ingreso']
                ])->save();
            DB::commit();
            // self::auditarProductor('ACTUALIZAR',$full_name);
            return response()->json(['success'=>true,'message'=>'Productor actualizado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>true,'message'=>'Error al actualizar datos del Productor. Error: '.$ex->getMessage()]);
        }
	}

	# Eliminar Productor
	public static function eliminarProductor($id_productor)
	{
		try{
            // if(!self::hasPermiso('productor.eliminar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $productor = Productor::find($id_productor);
                $productor->fill(['activo'=>DB::raw(0)])->save();
                $persona = Persona::find($productor->id_persona);
                $persona->fill(['activo'=>DB::raw(0)])->save();
            DB::commit();
            // self::auditarProductor('ELIMINAR',$productor->persona->fullname);
            return response()->json(['success'=>true,'message'=>'Productor eliminado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el Productor. Error:' .$ex->getMessage()]);
        }
	}

	# Cambiar la condicion del productor
	public static function cambiarCondicionProductor($request)
	{	
		try{
            DB::beginTransaction();
                $productor = Productor::find($request->id);
                switch ($request->id_condicion) {
                    case 1:
                        $productor->update(['id_condicion'=>DB::raw($request->id_condicion)]);
                        break;
                    case 2:
                        $productor->update(['id_condicion'=>DB::raw($request->id_condicion)]);
                        # deshabilitar todas las parcelas
                        self::cambiarEstadoParcelas($productor,1);
                        break;
                    case 4:
                        $productor->update(['id_condicion'=>DB::raw($request->id_condicion)]);
                        # deshabilitar todas las parcelas
                        self::cambiarEstadoParcelas($productor,0);
                        break;
                    case 5:
                        $productor->update(['id_condicion'=>DB::raw($request->id_condicion)]);
                        # deshabilitar todas las parcelas
                        self::cambiarEstadoParcelas($productor,0);
                        break;
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Estado del productor actualizado correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al realizar la operación.'.$ex->getMessage()]);
        }
	}

    # Cambiar el estado de las parcelas de un productor cuando es sancionado parcial o total
    public static function cambiarEstadoParcelas($productor,$estado_parcela)
    {
        try{
            DB::beginTransaction();
                foreach ($productor->parcelas->where('activo',1) as $parcela) {
                    Parcela::find($parcela->id)->update(['estado_parcela'=>DB::raw($estado_parcela)]);
                }
            DB::commit();
        }catch(\Exception $ex){
            DB::rollback();
        }
    }
}