<?php

namespace App\Traits;


use App\VisitaAuditoriaInterna;
use Illuminate\Support\Facades\DB;
use App\Visita;

trait TraitVisitaAuditoriaInterna
{
	public static function obtenerVisitasAuditoriaInterna($id_parcela)
	{
		try{
            $visitas_temp = Visita::where('id_parcela',$id_parcela)->where('formulario',1)->where('activo',1)->orderBy('fecha','desc')->get();

            $visitas = array();
            foreach ($visitas_temp as $key => $value) {
                $visitas[] = array(
                    'id'              => $value->id,
                    'observacion'     => $value->observacion,
                    'fecha'           => $value->fecha,
                    'id_parcela'      => $value->id_parcela,
                    'activo'          => $value->activo,
                    'numero_imagenes' => count($value->galeria_visita),
                    'isExport'        => false
                );
            }
                     
            return response()->json(['info'=>$visitas,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al listar los registros'.$e->getMessage(),'success'=>false]);    
        }
	}

	public static function registrarVisitaAuditoriaInterna($request)
	{
		try{
            //return $request->all();
            // if(!self::hasPermiso('productor.registrar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $visita = Visita::create([
                    'fecha'         =>  $request['fecha'],
                    'observacion'   =>  $request['observacion'],
                    'responsable'   =>  $request['responsable'],
                    'formulario'    =>  1,
                    'id_parcela'    =>  $request['id_parcela']
                ]);

                foreach ($request['preguntas'] as $pregunta) {
                    
                    VisitaAuditoriaInterna::create([
                        'respuesta'                        =>  intval($pregunta['respuesta']),
                        'observacion'                      =>  $pregunta['observacion'],
                        'id_visita'                        =>  $visita->id,
                        'id_formulario_auditoria_interna'  =>  $pregunta['id']
                    ]);

                }

            DB::commit();
            // self::auditarVisita('INSERTAR',$full_name);
            return response()->json(['message'=>'Visita creada correctamente.','success'=>true]);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['message'=>'Error el ralizar la operación.Error: '.$ex->getMessage(),'success'=>false]);
        }
	}

	public static function actualizarVisitaAuditoriaInterna($request, $id)
	{
		try{
            DB::beginTransaction();

            $item = VisitaAuditoriaInterna::find($id);
            $item->fill([
                'total'         => $request['total'],
                'id_visita'     => $request['id_visita'],
                'id_item_labor' => $request['id_item_labor']
            ])->save();

            DB::commit();
            return response()->json(['message'=>'Registro Actualizado Correctamente','success'=>true]); 
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en item de labor, inténtelo nuevamente. '.$ex->getMessage()]); 
        }
	}

	public static function eliminarVisitaAuditoriaInterna($id_visita)
	{
		try{
            // if(!self::hasPermiso('visita.eliminar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $visita = Visita::find($id_visita);
                $visita->fill(['activo'=>DB::raw(0)])->save();
            DB::commit();
            // self::auditarVisita('ELIMINAR',$productor->persona->fullname);
            return response()->json(['success'=>true,'message'=>'Visita eliminada correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar la Visita. Error:' .$ex->getMessage()]);
        }
	}

    # Exportar PDF
    public static function exportPDFVisita($data)
    {
        try{
            $pdf = \PDF::loadView('sic.ficha_uno',['data'=>$data]);
            return $pdf->download('reporte_sic.pdf');
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al generar pdf la Visita. Error:' .$ex->getMessage()]);
        }
    }
}