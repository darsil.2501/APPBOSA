<?php
namespace App\Traits;

use App\Enfunde;
use App\Semana;
use App\Empacadora;
use App\Parcela;
use App\Recobro;
use App\Traits\TraitAuditoria;
use DB;
use Excel;
use PHPExcel_Style_Alignment;
use PHPExcel_Worksheet_Drawing;

trait TraitEnfunde
{
    use TraitAuditoria;

    public static function obtenerInformacionEnfunde($request)
    {
        try {
            $data = Enfunde::where('id_parcela', '=', $request['id_parcela'])
                ->where('id_semana', '=', $request['id_semana'])
                ->get();
            return response()->json(["success" => true, "info" => $data]);
        } catch (\Exception $ex) {
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }

    public static function registrarEnfunde($request)
    {
        try {

            DB::beginTransaction();
            # Validar que no si está el registro con la combinación de parcel y semana.
            $registro = Enfunde::where(['id_parcela'=> $request['id_parcela'],
                                        'id_semana'=> $request['id_semana']])->first();
            //return $value['semana'];
            if ($registro == null) {
                $enfunde = Enfunde::create($request->all());

                self::auditar('audit_enfunde',$enfunde->id,'INSERTAR');
                DB::commit();
                return response()->json(["message" => "Registro creado correctamente", "success" => true]);
            } else {
                $registro = Enfunde::find($request['id']);
                $registro->fill(['racimas_enfundadas'=>$request['racimas_enfundadas']])->save();
                self::auditar('audit_enfunde',$registro->id,'ACTUALIZAR');
            }
            DB::commit();
            return response()->json(["message" => "Registro actualizados correctamente", "success" => true]);
        } catch (Exception $ex) {
            DB::rollback();
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }

    public static function obtenerParcelaEnfunde($id_parcela, $id_semana)
    {
        try {
            $data = Enfunde::where('id_parcela', '=', $id_parcela)->where('id_semana', '=', $id_semana)->first();
            return $data;
        } catch (\Exception $ex) {
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }

    public static function obtenerEnfundeProductoresNoData()
    {
        try {
            $parcelas = array();
            $productores = array();
            foreach (Parcela::all() as $parcela) {
                $enfundes = Enfunde::where('activo', 1)->where('id_parcela', $parcela->id)->get();
                if (count($enfundes) < 5) {
                    $parcela['empacadora']  = $parcela->empacadora;
                    $parcela['sector']      = $parcela->sector;
                    $parcelas[]             = collect($parcela)->except(['productor'])->all();
                    $productores[]          = $parcela->productor;
                }
            }

            $collection    = collect($productores);
            $productores   = $collection->unique('id');
            $productores   = $productores->values()->all();

            $parcelas = collect($parcelas);
            $productoresParcelas = array();

            foreach ($productores as $productor) {
                $productoresParcelas[] = array(
                    'id'       => $productor->id,
                    'fullname' => $productor->persona->fullname,
                    'dni'      => $productor->persona->dni,
                    'codigo_productor' => $productor->codigo_productor,
                    'parcelas' => $parcelas->where('id_productor', $productor->id)->values()->all()
                );
            }

            return $productoresParcelas;
        } catch (\Exception $ex) {
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }

    public static function obtenerEmpacadoraDataEnfunde()
    {       
            
        $semanas_enfunde = Enfunde::where('activo',1)->distinct()->get(['id_semana']);
        $empacadoras_temp = Empacadora::where('activo',1)->get();//enviar

        $empacadoras = array();
        foreach ($empacadoras_temp as $empacadora) {
            $semanas_data = array();
            foreach ($semanas_enfunde as $semana) {
                $total_enfunde = 0;
                foreach ($empacadora->parcelas as $parcela) {
                    $enfunde = Enfunde::where('id_parcela',$parcela->id)->where('id_semana',$semana->id_semana)->where('activo',1)->first();
                    if ($enfunde != null) {
                        $total_enfunde += $enfunde->racimas_enfundadas;
                    }                        
                }
                $recobro = Recobro::where('id_empacadora',$empacadora->id)->where('id_semana',$semana->id_semana)->where('activo',1)->first();
                $semanas_data[] = array(
                    'total_enfunde' => $total_enfunde,
                    'total_cosecha' => ($recobro) ? $recobro->racimas_cosechadas : 0
                );
            }
            $empacadoras[] = array(
                'id'        => $empacadora->id,
                'nombre'    => $empacadora->nombre,
                'cantidad_parcelas'  => count($empacadora->parcelas),
                'data'      => $semanas_data
            );
        }


        $semanas = array();
        foreach ($semanas_enfunde as $semana) {
            $semana_find = Semana::where('id',$semana->id_semana)->first();
            $semanas[] = array(
                'id' => $semana_find->id,
                'nombre' => $semana_find->nombre,
                'cinta_color' => $semana_find->cinta->color
            );
        }

        $data = array(
            'empacadoras' => $empacadoras,
            'semanas'     => $semanas
        );

        return $data;
        
    }

    public static function reporteEnfundeProductores()
    {
        try {
            $productores = self::obtenerEnfundeProductoresNoData();
            Excel::create('Reporte_ enfundes', function ($excel) use ($productores) {

                $excel->sheet('Productor', function ($sheet) use ($productores) {

                    $objDrawing = new PHPExcel_Worksheet_Drawing;
                    $objDrawing->setPath(public_path('logo.png')); //your image path
                    $objDrawing->setCoordinates('A2');
                    $objDrawing->setWorksheet($sheet);

                    $sheet->mergeCells('A2:L2');
                    $sheet->setCellvalue('A2', 'ASOCIACION DE PEQUEÑOS PRODUCTORES DE BANANO ORGANICO SAMAN Y ANEXOS');
                    $sheet->cells('A2:L2', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                            'bold'   => true,
                        ));
                    });
                    $sheet->cell('A2', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('A3:L3');
                    $sheet->setCellvalue('A3', 'Sector Nueva Esperanza s/n Samán ,sullana Perú-Telef.: 504119');
                    $sheet->cells('A3:L3', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                            'bold'   => false,
                        ));
                    });
                    $sheet->cell('A3', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('A4:L4');
                    $sheet->setCellvalue('A4', 'LISTA DE PRODUCTORES SIN INFUNDE');
                    $sheet->cells('A4:L4', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                            'bold'   => true,
                        ));
                    });

                    $sheet->cell('A4', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'medium', 'thin');
                    });

                    $sheet->setCellvalue('A5', 'CÓDIGO PRODUCTOR');
                    $sheet->setCellvalue('B5', 'APELLIDOS Y NOMBRES');
                    $sheet->setCellvalue('C5', 'DNI');
                    $sheet->setCellvalue('D5', 'CÓDIGO PARCELA');
                    $sheet->setCellvalue('E5', 'AREA SEMBRADA');
                    $sheet->setCellvalue('F5', 'EMPACADORA');
                    $sheet->setCellvalue('G5', 'SECTOR');

                    $sheet->setColumnFormat(array('E' => '0.00'));
                    $sheet->setColumnFormat(array('C' => '00000000'));

                    $letras = ['H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

                    $semanas = Semana::where('activo',1)->where('id','>=',72)->get();
                    foreach ($semanas as $key => $semana) {
                        $sheet->setCellvalue($letras[$key].'5', $semana->nombre);
                        $sheet->cells($letras[$key].'5:'.$letras[$key].'5', function ($cells) use ($semana) {
                            $cells->setBackground($semana->cinta->color);
                        });
                    }

                    $sheet->getStyle('A5:T5')->getAlignment()->setWrapText(true);

                    $sheet->cells('A5:T5', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '8',
                            'bold'   => true,
                        ));
                        //$cells->setTextRotation(90);
                    });

                    $sheet->cells('A5:G5', function ($cells) {
                        $cells->setBackground('#808B96');
                    });

                    $sheet->setBorder('A5:T5', 'thin');
                    $sheet->setAutoFilter('A5:T5');

                    $fila_inicio = 6;

                    foreach ($productores as $productor) {

                        $fila_sgte = $fila_inicio + count($productor['parcelas']) - 1;

                        $sheet->mergeCells('A' . $fila_inicio . ':A' . $fila_sgte);
                        $sheet->setCellvalue('A' . $fila_inicio, $productor['codigo_productor']);

                        $sheet->mergeCells('B' . $fila_inicio . ':B' . $fila_sgte);
                        $sheet->setCellvalue('B' . $fila_inicio, $productor['fullname']);

                        $sheet->mergeCells('C' . $fila_inicio . ':C' . $fila_sgte);
                        $sheet->setCellvalue('C' . $fila_inicio, $productor['dni']);

                        $fila_parcela = $fila_inicio;
                        foreach ($productor['parcelas'] as $parcela) {
                            $sheet->setCellvalue('D' . $fila_parcela, $parcela['codigo_parcela']);

                            $sheet->setCellvalue('E' . $fila_parcela, $parcela['ha_sembrada']);

                            $sheet->setCellvalue('F' . $fila_parcela, $parcela['empacadora']['nombre']);

                            $sheet->setCellvalue('G' . $fila_parcela, $parcela['sector']['nombre']);

                            foreach ($semanas as $key => $semana) {
                                $enfunde = Enfunde::where('id_semana',$semana->id)->where('id_parcela',$parcela['id'])->first();
                                $sheet->setCellvalue($letras[$key].$fila_parcela, ($enfunde == null) ? 0 : $enfunde->racimas_enfundadas);
                            }


                            $fila_parcela++;
                        }

                        $fila_inicio = $fila_sgte + 1;
                    }
                    // Ajustar y alinear Texto
                    

                    $sheet->cells('A6:T'. $fila_inicio, function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                            'bold'   => false,
                        ));
                    });

                    $sheet->cells('B6:B'. $fila_inicio, function ($cells) {
                        $cells->setAlignment('left');
                    });
                    $sheet->setBorder('A6:T' . ($fila_inicio-1), 'thin');
                    
                    $sheet->setFreeze('A6');
                    $sheet->setWidth(array(
                        'A' => 16, 'B' => 35, 'C' => 9.86, 'D' => 7, 'E' => 9, 'F' => 14, 'G' => 17,
                    ));

                });

            })->export('xlsx');

        } catch (\Exception $ex) {
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }
    
}
