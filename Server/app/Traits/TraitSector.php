<?php

namespace App\Traits;

use App\Delegado;
use App\Productor;
use App\Sector;
use Illuminate\Support\Facades\DB;

trait TraitSector
{
	public static function obtenerSectores()
	{
		try{
            $sectores = Sector::where('activo',1)->get();
            return response()->json(['info'=>$sectores,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Error al listar los registros.'.$e->getMessage(),'success'=>false]);    
        }
	}

	public static function registrarSector($request)
	{
		try{
			DB::beginTransaction();
	            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
	            $registro = Sector::create($request->all());
	            // self::auditar('audit_sector',$registro->nombre,'INSERTAR');
	            DB::commit();
	            return self::RegistroCreateSuccess();    
	        
        }catch(\Exception $e){
        	DB::rollback();
            return self::ErrorInOperation($e);       
        }
	}

	public static function actualizarSector($request,$id)
	{
		try{
			DB::beginTransaction();
	            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
	            $registro = Sector::find($id);
	            if($registro){
	                $registro->fill($request->all())->save();
	                // self::auditar('audit_sector',$registro->nombre,'ACTUALIZAR');
	                DB::commit();
	                return self::RegistroUpdateSuccess();        
	            }
        }catch(\Exception $e){
        	DB::rollback();
            return self::ErrorInOperation($e);
        }
	}

	public static function eliminarSector($id)
	{
		try{
			DB::beginTransaction();
	            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
	            $registro = Sector::find($id);
	            if($registro){
	                if(count($registro->parcelas) > 0){
	                    return self::RegistroInUse();
	                }        
	                $registro->fill(['activo'=>DB::raw(0)])->save();
	                // self::auditar('audit_sector',$registro->nombre,'ELIMINAR');
	                DB::commit();
	                return self::RegistroDeleteSuccess();        
	            }
        }catch(\Exception $e){
        	DB::rollback();
            return self::ErrorInOperation($e);
        }
	}

	public static function obtenerProductoresPorSector($id_sector)
	{
		try{
            $data = Productor::whereHas('parcelas',function($query) use ($id_sector){
                $query->where('id_sector',$id_sector);
            })->with('persona')->where('activo',1)->get();
            return response()->json(['success'=>true,'info'=>$data]);
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
	}

	public static function obtenerDelegados($id_sector)
    {
        try{
            // $delegados = Delegado::where('id_sector',$id_sector)->where('activo',1)->get();
            $delegado = Delegado::where('fecha_fin_cargo',null)->where('id_sector',$id_sector)->orderBy('id','DESC')->first();
            if($delegado != null){
                $data['id']                 = $delegado->id;
                $data['fullname']           = $delegado->productor->persona->fullname;
                $data['fecha_inicio_cargo'] = $delegado->fecha_inicio_cargo;
                $data['fecha_fin_cargo']    = ($delegado->fecha_fin_cargo == null) ? 'Aún sigue en el cargo' : $delegado->fecha_fin_cargo;
                $data = array($data);
            }else{
                $data = [];
            }
            // $data     = array();
            // foreach ($delegados as $delegado) {
            //     $data[] = array('id'                => $delegado->id,
            //                     'fullname'          => $delegado->productor->persona->fullname,
            //                     'fecha_inicio_cargo'=> $delegado->fecha_inicio_cargo,
            //                     'fecha_fin_cargo'   => ($delegado->fecha_fin_cargo == null) ? 'Aún sigue en el cargo' : $delegado->fecha_fin_cargo);
            // }
            return response()->json(['success'=>true,'info'=>$data]);   
        }catch(Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al listar los delegados.'.$ex->getMessage()]);
        }
    }

    public function registrarDelegado($request)
    {
        try{
            DB::beginTransaction();
                # Validar que no si está el registro con la combinación de parcel y semana.

                // $registro = Delegado::where(['id_sector'   =>$request->id_sector,
                //                              'id_productor'=>$request->id_productor,
                //                              'activo'      => 1])->first();
                // if($registro != null){
                //     return response()->json(['success'=>false,'message'=>'Este productor ya está registrado como delegado.']);   
                // }


                $ultimo_delegado_sector = Delegado::where('fecha_fin_cargo',null)->where('id_sector',$request->id_sector)->orderBy('id','DESC')->first();
                $delegado_nuevo = Delegado::create($request->all());
                if($ultimo_delegado_sector != null){
                    Delegado::find($ultimo_delegado_sector->id)->fill(['fecha_fin_cargo'=>$delegado_nuevo->fecha_inicio_cargo])->save();    
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Delegado registrado correctemante.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al registrar el delegado.'.$ex->getMessage()]);
        }
    }

    public function eliminarDelegado($id_delegado)
    {
        try{
            DB::beginTransaction();
                $delegado = Delegado::find($id_delegado);
                $delegado->delete();
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Delegado eliminado correctemante.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el delegado.'.$ex->getMessage()]);
        }
    }
}