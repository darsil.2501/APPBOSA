<?php

namespace App\Traits;
use Mail;

trait TraitEmail
{
	public static function sendEmailCliente($tipo_archivo,$url_archivo,$cliente,$semana)
	{
        try{
    		
            $horario = (date('H') < 12) ? 'Buenos días' : (date('H')) < 18 ? 'Buenas tardes' : 'Buenas noches';

            $data    = ['archivo'=>ucfirst($tipo_archivo),'semana'=>$semana,'horario'=>$horario];

            Mail::send('email.plantilla',$data,function($correo) use($tipo_archivo,$url_archivo,$cliente,$semana){
                $archivo = public_path('archivos/'.$tipo_archivo.'/'.$url_archivo);
                $correo->subject(ucfirst($tipo_archivo).' de exportación, Semana '.$semana.' - APPBOSA - '.ucfirst($cliente->razon_social));
                foreach ($cliente->emails as $email) {
                	$correo->cc($email->email);
                }
                // if (file_exists($archivo))
                $correo->attach($archivo);
            });
            return response()->json(['success'=>true,'message'=>'Correo enviado correctamente a los destinatarios']);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'message'=>'Ocurrió un error al enviar el correo, inténtelo nuevamente. '.$ex->getMessage()]);
        }
    }
}