<?php

namespace App\Traits;
use App\Movimiento;
use App\Contenedor;
use App\Semana;

trait TraitGrafico
{
	// función para devolver la cantidad de contenedores de un cliente por mes
	public static function getContenedoresClienteMensual($id_cliente,$mes,$anio)
    {
        $contenedores = Contenedor::where('id_cliente',$id_cliente)
                        ->whereMonth('fecha_proceso_inicio','=',$mes)
                        ->whereYear('fecha_proceso_inicio','=',$anio)->where('activo',1)
                        ->get();
        return count($contenedores);
    }

    // Función para devolver la cantidad de contenedores enviados por año
    public static function getContenedoresAnio($id_anio)
    {
        $semanas = Semana::where('id_anio',$id_anio)->where('activo',1)->orderBy('nombre','ASC')->get();
        $total_contenedores = 0;
        foreach ($semanas as $value) {
            $total_contenedores = $total_contenedores + self::getContenedoresPorSemana($value->id);
        }
        return $total_contenedores;
    }

    public static function getContenedoresAnioNew($id_cliente,$id_anio)
    {
        $semanas = Semana::where('id_anio',$id_anio)->where('activo',1)->orderBy('nombre','ASC')->get();
        $total_contenedores = 0;
        foreach ($semanas as $value) {
            $total_contenedores = $total_contenedores + self::getContenedoresPorSemanaAndCliente($id_cliente,$value->id);
        }
        return $total_contenedores;
    }
    //edit by darwin
    public static function getContenedoresClienteNew($id_cliente,$mes,$id_anio,$anio)
    {   
        $semanas                         = Semana::where('id_anio',$id_anio)->where('activo',1)->orderBy('nombre','ASC')->get();
        $contenedores_anio_anteritor_tmp = 0;
        if(count($semanas)>0 && $mes == "01"){
            $contenedores_anio_anteritor_tmp = count(Contenedor::where('id_semana',$semanas->first()->id)->where('id_cliente',$id_cliente)->whereYear('fecha_proceso_inicio','=',$anio-1)->get());
        }

        $total_contenedores = 0;
        foreach ($semanas as $value) {
            $total_contenedores = $total_contenedores + self::getContenedoresPorSemanaAndClienteAndMes($id_cliente,$value->id,$mes,$anio);
        }
        return $total_contenedores + $contenedores_anio_anteritor_tmp;
    }

    // Función para devolver la cantidad de contenedores enviados por semana
    public static function getContenedoresPorSemanaAndCliente($id_cliente,$id_semana)
    {
        $cantidad_contenedores = count(Contenedor::where('id_semana',$id_semana)->where('id_cliente',$id_cliente)->where('activo',1)->get());
        return $cantidad_contenedores;
    }

    // Función para devolver la cantidad de contenedores enviados por semana
    public static function getContenedoresPorSemanaAndClienteAndMes($id_cliente,$id_semana,$mes,$anio)
    {
        return count(Contenedor::where('id_semana',$id_semana)->where('id_cliente',$id_cliente)
               ->whereMonth('fecha_proceso_inicio','=',$mes)->whereYear('fecha_proceso_inicio','=',$anio)->where('activo',1)->get());
    }

    // Función para devolver la cantidad de contenedores enviados por semana
    public static function getContenedoresPorSemana($id_semana)
	{
		$cantidad_contenedores = count(Contenedor::where('id_semana',$id_semana)->where('activo',1)->get());
		return $cantidad_contenedores;
    }

    public static function getColorHaxadecimal()
    {
        $colores = ['#FF0F00','#FF6600','#FF9E01','#FCD202','#F8FF01','#B0DE09','#04D215','#0D8ECF','#0D52D1','#2A0CD0',
                    '#8A0CCF','#CD0D74','#754DEB','#DDDDDD','#999999','#A93226','#CB4335','#884EA0','#6C3483','#1F618D',
                    '#2874A6','#148F77','#117A65','#1E8449','#239B56','#B7950B','#B9770E','#AF601A','#A04000','#B3B6B7',
                    '#641E16','#78281F','#512E5F','#4A235A','#154360','#1B4F72','#0E6251','#0B5345','#186A3B','#7D6608',
                    '#784212','#6E2C00','#626567','#C0392B','#E74C3C','#9B59B6','#8E44AD','#2980B9','#3498DB','#1ABC9C',
                    '#27AE60','#C39BD3','#7FB3D5','#85C1E9','#76D7C4','#76D7C4','#73C6B6','#F7DC6F','#F8C471','#E59866'];

        return $colores[array_rand($colores)];
    }

    public static function getArrayMeses() 
    { 
        return array(['id'=>'01','nombre'=>'Enero'],['id'=>'02','nombre'=>'Febrero'],['id'=>'03','nombre'=>'Marzo'], 
                     ['id'=>'04','nombre'=>'Abril'],['id'=>'05','nombre'=>'Mayo'],['id'=>'06','nombre'=>'junio'], 
                     ['id'=>'07','nombre'=>'julio'],['id'=>'08','nombre'=>'Agosto'],['id'=>'09','nombre'=>'Septiembre'], 
                     ['id'=>'10','nombre'=>'Octubre'],['id'=>'11','nombre'=>'Noviembre'],['id'=>'12','nombre'=>'Diciembre']); 
    } 
}