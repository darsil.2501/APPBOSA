<?php

namespace App\Traits;


use App\FormularioAuditoriaInterna;
use Illuminate\Support\Facades\DB;

trait TraitFormularioAuditoriaInterna
{
	public static function obtenerPreguntas()
	{
		try{
            $formularios = FormularioAuditoriaInterna::where('activo',1)->get();

            $data = array();
            foreach ($formularios as $key => $formulario) {
                $data[] = array(
                    'id'          => $formulario->id,
                    'titulo'      => $formulario->titulo,
                    'isOpen'      => true,
                    'respuesta'   => 3,//0->no,1->si,2->na
                    'observacion' => "",  
                );
            }
            return response()->json(['info'=>$data,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>false]);    
        }
	}

	public static function registrarPregunta($request)
	{
		try{
            DB::beginTransaction();
            FormularioAuditoriaInterna::create(['titulo'=>mb_strtoupper($request['titulo'])]);
            DB::commit();
            return response()->json(['message'=>'Registro Creado Correctamente','success'=>true]);    
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en guardar Pregunta, inténtelo nuevamente. '.$ex->getMessage()]);       
        }
	}

	public static function actualizarPregunta($request, $id)
	{
		try{
            DB::beginTransaction();

            $formulario = FormularioAuditoriaInterna::find($id);
            $formulario->fill(['titulo'=>mb_strtoupper($request['titulo'])])->save();

            DB::commit();
            return response()->json(['message'=>'Registro Actualizado Correctamente','success'=>true]); 
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en editar Pregunta, inténtelo nuevamente. '.$ex->getMessage()]); 
        }
	}

	public static function eliminarPregunta($id)
	{
		try{
            DB::beginTransaction();

            $formulario = FormularioAuditoriaInterna::find($id);
            $formulario->fill(['activo'=>DB::raw(0)])->save(); 

            DB::commit();               
            return response()->json(['message'=>'Registro Eliminado Correctamente','success'=>true]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en eliminar Pregunta, inténtelo nuevamente. '.$ex->getMessage()]); 
        }
	}
}