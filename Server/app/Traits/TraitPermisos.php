<?php

namespace App\Traits;

trait TraitPermisos
{
	public static function hasPermiso($permiso)
	{
		try {
			return in_array($permiso,\Session::get('permisos'));	
		} catch (\Exception $e) {
			return response()->json(['success'=>false,'message'=>'Ocurrió un incoveniente, inténtelo nuevamente. Error: '.$e->getMessage()]);
		}
		
	}
}