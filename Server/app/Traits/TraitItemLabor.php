<?php

namespace App\Traits;


use App\ItemLabor;
use App\Labor;
use Illuminate\Support\Facades\DB;

trait TraitItemLabor
{
	public static function obtenerItemsLabor()
	{
		try{
            $labores = Labor::where('activo',1)->get();

            $data_labores = array();
            foreach ($labores as $key => $labor) {
                $data_labores [] = array(
                    'id' => $labor->id,
                    'nombre' => $labor->nombre, 
                    'items' => $labor->items()->where('activo',1)->get()
                );
            }

            return response()->json(['info'=>$data_labores,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>false]);    
        }
	}

	public static function registrarItemLabor($request)
	{
		try{
            DB::beginTransaction();
            ItemLabor::create([
                'nombre'        => $request['nombre'],
                'punto_evaluar' => $request['punto_evaluar'],
                'id_labor'      => $request['id_labor']
            ]);
            DB::commit();
            return response()->json(['message'=>'Registro Creado Correctamente','success'=>true]);    
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en guardar item de labor, inténtelo nuevamente. '.$ex->getMessage()]);       
        }
	}

	public static function actualizarItemLabor($request, $id)
	{
		try{
            DB::beginTransaction();

            $item = ItemLabor::find($id);
            $item->fill([
                'nombre'        => $request['nombre'],
                'punto_evaluar' => $request['punto_evaluar']
            ])->save();

            DB::commit();
            return response()->json(['message'=>'Registro Actualizado Correctamente','success'=>true]); 
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en item de labor, inténtelo nuevamente. '.$ex->getMessage()]); 
        }
	}

	public static function eliminarItemLabor($id)
	{
		try{
            DB::beginTransaction();

            $item = ItemLabor::find($id);
            $item->fill(['activo'=>DB::raw(0)])->save(); 

            DB::commit();               
            return response()->json(['message'=>'Registro Eliminado Correctamente','success'=>true]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en eliminar item de labor, inténtelo nuevamente. '.$ex->getMessage()]); 
        }
	}
}