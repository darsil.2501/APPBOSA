<?php

namespace App\Traits;

use App\Empacadora;
use Illuminate\Support\Facades\DB;
use App\Traits\TraitEnfunde;


trait TraitEmpacadora
{
    use TraitEnfunde;
	public static function obtenerEmpacadoras()
	{
		try{
            $empacadoras = Empacadora::where('activo',1)->get();
            return response()->json(['info'=>$empacadoras,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Error al listar los registros.'.$e->getMessage(),'success'=>false]);    
        }
	}

	public static function registrarEmpacadora($request)
	{
		try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            $registro = Empacadora::create(array_map("mb_strtoupper",$request->all()));
            // self::auditar('audit_empacadora',$registro->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();    
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
	}

	public static function actualizarEmpacadora($request,$id_empacadora)
	{
		try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            $registro = Empacadora::find($id_empacadora);
            if($registro){
                $registro->fill(array_map("mb_strtoupper",$request->all()))->save();
                // self::auditar('audit_empacadora',$registro->nombre,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
	}

	public static function eliminarEmpacadora($id_empacadora)
	{
		try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            $registro = Empacadora::find($id_empacadora);
            if($registro){
                if(count($registro->parcelas) > 0){return self::RegistroInUse();}        
                $registro->fill(['activo'=>DB::raw(0)])->save();
                // self::auditar('audit_empacadora',$registro->nombre,'ELIMINAR');
                return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
	}

    public static function getProductoresEmpacadora($request)
    {
        try
        {
            $empacadora = Empacadora::where('id',$request['id_empacadora'])->where('activo',1)->first();

            $productoresTemp = array();
            foreach ($empacadora->parcelas as $key => $parcela) {
               $productoresTemp[] = $parcela->productor; 
            }

            $collection   = collect($productoresTemp);
            $productoresTemp  = $collection->unique('id');
            $productoresTemp  = $productoresTemp->values()->all();

            $productores = array();
            foreach ($productoresTemp as $key => $productor) {
                $parcelasProductor = \App\Parcela::where('id_productor',$productor->id)->
                                      where('id_empacadora',$request['id_empacadora'])->get();
                $areaTotal = 0;
                $parcelas = array();
                foreach ($parcelasProductor as $key => $parcela) {
                   $areaTotal += $parcela->ha_sembrada;
                   $parcelas[] = array(
                    'id'            => $parcela->id,
                    'numero'        => $parcela->numero_parcela,
                    'ha_sembrada'   => $parcela->ha_sembrada,
                    'ha_total'      => $parcela->ha_total,
                    'empacadora'    => $parcela->empacadora->nombre,
                    'sector'        => $parcela->sector->nombre,
                    'enfunde'       => self::obtenerParcelaEnfunde($parcela->id, $request['id_semana']),
                    'enable'        => !self::obtenerParcelaEnfunde($parcela->id, $request['id_semana'])
                   );
                }

                $productores[] = array(
                    'id'          => $productor->id,
                    'fullname'    => $productor->persona->fullname,
                    'area_total'  => $areaTotal,
                    'parcelas'    => $parcelas
                );
            }

            return response()->json(['info'=>$productores,'success'=>true]);
        }
        catch(Exception $ex)
        {
            return self::ErrorInOperation($ex);
        }
    }
}