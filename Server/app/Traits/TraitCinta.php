<?php
namespace App\Traits;
use DB;
use App\Cinta;

trait TraitCinta
{
    public static function obtenerInformacionCinta()
    {
        try{
            $data = Cinta::where('activo',1)->get();
            return response()->json(["success"=>true,"info"=>$data]);
        }catch(\Exception $ex){
            return response()->json(["success"=>false,"message"=>$ex->getMessage()]);
        }
    }

    public static function registrarCinta($request)
    {
        try{
            DB::beginTransaction();
                Cinta::create(array_map("mb_strtoupper",$request->all()));
            DB::commit();
            return response()->json(["message"=>"Registro creado correctamente","success"=>true]);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(["success"=>false,"message"=>$ex->getMessage()]);
        }
    }

    public static function actualizarCinta($request,$id)
    {
        try{
            DB::beginTransaction();
                $registro = Cinta::find($id);
                $registro->fill(array_map("mb_strtoupper",$request->all()))->save();
            DB::commit();
            return response()->json(["message"=>"Registro actualizado correctamente","success"=>true]);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(["success"=>false,"message"=>$ex->getMessage()]);
        }
    }

    public static function eliminarCinta($id)
    {
        try{
            DB::beginTransaction();
                $registro = Cinta::find($id);
                $registro->fill(['activo'=>DB::raw(0)])->save();;
            DB::commit();
            return response()->json(["message"=>"Registro eliminado correctamente","success"=>true]);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(["success"=>false,"message"=>$ex->getMessage()]);
        }
    }
}