<?php

namespace App\Traits;


use App\Labor;
use Illuminate\Support\Facades\DB;

trait TraitLabor
{
	public static function obtenerLabores()
	{
		try{
            $labores = Labor::where('activo',1)->get();

            return response()->json(['info'=>$labores,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>false]);    
        }
	}

	public static function registrarLabor($request)
	{
		try{
            DB::beginTransaction();
            Labor::create(['nombre'=>$request['nombre']]);
            DB::commit();
            return response()->json(['message'=>'Registro Creado Correctamente','success'=>true]);    
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en guardar Labor, inténtelo nuevamente. '.$ex->getMessage()]);       
        }
	}

	public static function actualizarLabor($request, $id)
	{
		try{
            DB::beginTransaction();

            $labor = Labor::find($id);
            $labor->fill(['nombre'=>$request['nombre']])->save();

            DB::commit();
            return response()->json(['message'=>'Registro Actualizado Correctamente','success'=>true]); 
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en editar Labor, inténtelo nuevamente. '.$ex->getMessage()]); 
        }
	}

	public static function eliminarLabor($id)
	{
		try{
            DB::beginTransaction();

            $labor = Labor::find($id);
            $labor->fill(['activo'=>DB::raw(0)])->save(); 

            DB::commit();               
            return response()->json(['message'=>'Registro Eliminado Correctamente','success'=>true]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en eliminar Labor, inténtelo nuevamente. '.$ex->getMessage()]); 
        }
	}
}