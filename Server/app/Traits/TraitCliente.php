<?php

namespace App\Traits;
use App\Movimiento;
use App\Semana;
use App\Cliente;
use App\Operador;
use App\Caja;
use App\Contenedor;
use App\Factura;
use App\Certificado;
use App\Valija;
use App\Packing;
use App\Traits\TraitValija;
use App\Traits\TraitEstadoArchivo;
use DB;
use App\Email;


trait TraitCliente
{
    use TraitValija, TraitCertificado, TraitEstadoArchivo;

    // Obtenero el id del cliente a partir de su SLUG
	public static function getIdCliente($slug_cliente)
	{
		return Cliente::where('slug',$slug_cliente)->where('activo',1)->first()->id;
	}

    // Listar loc contenedores de un determinado cliente
	public static function getContenedoresCliente($id_cliente,$id_semana)
    {
        $contenedores = Contenedor::where('id_cliente',$id_cliente)->where('id_semana',$id_semana)->orderBy('fecha_proceso_inicio')->where('activo',1)->get();   
        
        $data_total = array();
        foreach ($contenedores as $contenedor) {
            $movimiento = $contenedor->movimientos->last();
            
            // recorremos para traer la información de las cajas que contiene
            $cajas_total = array();
            foreach ($contenedor->cajas as $value) {
                $caja['id']         	= $value->id;
                $caja['marca']      	= $value->marca;
                $caja['cantidad_cajas'] = $value->pivot->cantidad;
                $cajas_total []     	= $caja;
            }

            $data['cajas']             = $cajas_total;
            $data['id_contenedor']     = $contenedor->id;
            $data['id_semana']         = $contenedor->id_semana;
            $data['semana']            = $contenedor->semana->nombre;
            $data['id_cliente']        = $contenedor->id_cliente;
            $data['cliente']           = $contenedor->cliente->razon_social;
            $data['email_cliente']     = $contenedor->cliente->email;
            $data['referencia']        = $contenedor->referencia;
            $data['booking']           = $contenedor->booking;
            $data['numero_contenedor'] = $contenedor->numero_contenedor;
            $data['id_lineanaviera']   = $contenedor->id_lineanaviera;
            $data['linea_naviera']     = $contenedor->linea_naviera->nombre;
            $data['sigla_linea_naviera']     = $contenedor->linea_naviera->sigla;
            // $data['id_nave']           = $contenedor->id_nave;
            $data['nave']              = $contenedor->nave;
            $data['id_puertodestino']  = $contenedor->id_puertodestino;
            $data['puerto_destino']    = $contenedor->puerto_destino->nombre;
            $data['id_operador']       = $contenedor->id_operador;
            $data['operador']          = $contenedor->operador->nombre;
            $data['dia_proceso_inicio']= $contenedor->fecha_proceso_inicio;
            $data['dia_proceso_fin']   = $contenedor->fecha_proceso_fin;
            $data['dia_zarpe']         = $contenedor->fecha_zarpe;
            $data['dia_llegada']       = $contenedor->fecha_llegada;
            $data['peso_bruto']        = $contenedor->peso_bruto;
            $data['peso_neto']         = $contenedor->peso_neto;
            $data['viaje']             = $contenedor->viaje;
            $data['id_estado']         = $movimiento->id_estado;
            $data['estado']            = $movimiento->estado->nombre;
            $data['color_estado']      = $movimiento->estado->color;
            $data['estado_factura']         = self::getNameStatusFile($contenedor->factura);
            $data['color_estado_factura']   = self::getColorStatusFile($contenedor->factura);
            $data['estado_packing']         = self::getNameStatusFile($contenedor->packing);
            $data['color_estado_packing']   = self::getColorStatusFile($contenedor->packing);
            $data['estado_certificado']     = self::getNameStatusFile($contenedor->certificado);
            $data['color_estado_certificado']   = self::getColorStatusFile($contenedor->certificado);
            $data['estado_valija']              = self::getNameStatusFile($contenedor->valija);
            $data['color_estado_valija']        = self::getColorStatusFile($contenedor->valija);
            $data_total[]              = $data;
            unset($cajas_total); // vaciamos el arreglo de cajas
        }
       return $data_total;
    }

    // ------------------------------- Listar las facturas de un cliente cuando inicia sessión------------------------
    public static function getFacturasCliente($id_cliente,$id_semana)
    {
        $data  = Factura::where('id_cliente',$id_cliente)->where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        return self::procesarData($data);
    }
    // ------------------------------- Listar las facturas de un cliente cuando inicia sessión------------------------

    // ------------------------------- Listar los certificados de un cliente cuando inicia sessión--------------------
    public static function getCertificadosCliente($id_cliente,$id_semana)
    {
        $data         = Certificado::where('id_cliente',$id_cliente)->where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        $certificados = array();
        foreach ($data as $key => $value) {
            $certificados[] = array(
                'id'          => $value->id,
                'cliente'     => $value->cliente->razon_social,
                'id_cliente'  => $value->cliente->id,
                'semana'      => $value->semana->nombre,
                'contenedores'=> self::getContenedoresOfCertificado($value),
                'nombre'      => $value->nombre,
                'fecha'       => $value->fecha,
                'hora'        => $value->hora,
                'url'         => $value->url,
                'descripcion' => $value->descripcion,
            );
        }
        return $certificados;
    }
    // ------------------------------- Listar los certificados de un cliente cuando inicia sessión--------------------

    public static function getValijasCliente($id_cliente,$id_semana)
    {
        $data = Valija::where('id_cliente',$id_cliente)->where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        $valijas = array();
        foreach ($data as $key => $value) {
            $valijas[] = array(
                'id'          => $value->id,
                'cliente'     => $value->cliente->razon_social,
                'id_cliente'  => $value->cliente->id,
                'semana'      => $value->semana->nombre,
                'contenedores'=> self::getContenedoresOfValija($value),
                'nombre'      => $value->nombre,
                'fecha'       => $value->fecha,
                'hora'        => $value->hora,
                'url'         => $value->url
            );
        }
        return $valijas;
    }

    public static function getPackingsCliente($id_cliente,$id_semana)
    {
        $data = Packing::where('id_cliente',$id_cliente)->where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        $packings = array();
        foreach ($data as $key => $value) {
            $packings[] = array(
                'id'          => $value->id,
                'cliente'     => $value->cliente->razon_social,
                'id_cliente'  => $value->cliente->id,
                'semana'      => $value->semana->nombre,
                'nombre'      => $value->nombre,
                'fecha'       => $value->fecha,
                'hora'        => $value->hora,
                'url'         => $value->url
            );
        }
        return $packings;
    }

    public static function procesarData($informacion)
    {
        $data = array();
        foreach ($informacion as $key => $value) {
            $data[] = array(
                'id'        => $value->id,
                'cliente'   => $value->cliente->razon_social,
                'semana'    => $value->semana->nombre,
                'nombre'    => $value->nombre,
                'fecha'     => $value->fecha,
                'hora'      => $value->hora,
                'url'       => $value->url
            );
        }
        return $data;
    }


    // para asignar operadores a un determinado cliente
    public static function asignarOperador($request)
    {
        try{
            DB::beginTransaction();
                $cliente = Cliente::find($request['id_cliente']);
                $cliente->operadores()->attach($request['id_operador']);
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Operador asignado correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al asignar el operador a este cliente. '.$ex->getMessage()]);
        }
    }

    public static function quitarOperador($request)
    {
        try{
            DB::beginTransaction();
                $cliente = Cliente::find($request['id_cliente']);
                $cliente->operadores()->detach($request['id_operador']);
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Operador quitado correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al quitar el operador a este cliente. '.$ex->getMessage()]);
        }
    }

    public static function showOperadores($id_cliente)
    {
        try{
            $cliente      = Cliente::find($id_cliente);
            $operadores   = Operador::where('activo',1)->get();
            $data = $cliente->operadores;
            $flag = false;
            foreach ($operadores as $key) {
                foreach ($data as $k) {
                    $flag = false;
                    if($key->id == $k->id){
                        $flag = true;
                        break;
                    }
                }
                if($flag){
                    $key->enable = true; 
                }else{
                    $key->enable = false; 
                }
                
            }
            return response()->json(['info'=>$operadores,'success'=>true]);            
        }catch(\Exception $ex){
            return response()->json(['info'=>'Error al listar los operadores de este cliente. '.$ex->getMessage(),'success'=>false]);   
        }
    }



    public static function operadoresAsignados($id_cliente)
    { 
        try{
            return response()->json(['info'=>Cliente::find($id_cliente)->operadores,'success'=>true]);
        }catch(\Exception $ex){
            return response()->json(['message'=>'Error al listar los contenedores que tiene asignados este cliente. '.$ex->getMessage(),'success'=>false]);
        } 
    }



    public static function asignarCaja($request){
        try{
            DB::beginTransaction();
                $cliente = Cliente::find($request['id_cliente']);
                $cliente->cajas()->attach($request['id_caja']);
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Tipo de Caja asignada correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al asignar el tipo de caja a este cliente. '.$ex->getMessage()]);
        }
    }

    public static function quitarCaja($request)
    {
        try{
            DB::beginTransaction();
                $cliente = Cliente::find($request['id_cliente']);
                $cliente->cajas()->detach($request['id_caja']);
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Tipo de Caja quitada correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al quitar el tipo de caja a este cliente. '.$ex->getMessage()]);
        }
    }

    public static function showCajas($id_cliente)
    {
        try{
            $cliente      = Cliente::find($id_cliente);
            $cajas        = Caja::where('activo',1)->get();
            $data = $cliente->cajas;
            $flag = false;
            foreach ($cajas as $key) {
                foreach ($data as $k) {
                    $flag = false;
                    if($key->id == $k->id){
                        $flag = true;
                        break;
                    }
                }
                if($flag){
                    $key->enable = true; 
                }else{
                    $key->enable = false; 
                }
                
            }
            return response()->json(['info'=>$cajas,'success'=>true]);
        }catch(\Exception $ex){
            return response()->json(['info'=>'Error al listar las cajas de este cliente. '.$ex->getMessage(),'success'=>false]);   
        }
    }


    public static function cajasAsignadas($id_cliente)
    {
        try{
            return response()->json(['info'=>Cliente::find($id_cliente)->cajas,'success'=>true]);
        }catch(\Exception $ex){
            return response()->json(['message'=>'Error al listar las cajas que tiene asignadas este cliente. '.$ex->getMessage(),'success'=>false]);
        }   
    }

    public static function agregarEmail($request)
    {
        try{
            DB::beginTransaction();
                Email::create([
                    'id_cliente' => $request['id_cliente'],
                    'email'      => $request['email'],
                ]);
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Emails agregados correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al agregar emails a este cliente.']);
        }
    }

    public static function eliminarEmail($id_email)
    {
        try{
            DB::beginTransaction();
                $email = Email::find($id_email);
                $email->delete();
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Email eliminado correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el email.'.$ex->getMessage()]);
        }
    }

    public static function actualizarEmail($request,$id_email)
    {
        try{
            DB::beginTransaction();
                $email = Email::find($id_email);
                $email->fill('email',$request['email'])->save();
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Email actualizado correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al actualizar el email.'.$ex->getMessage()]);
        }
    }

    public static function verCorreos($id_cliente)
    {
        try{
            $correos =  Cliente::find($id_cliente)->emails;
            return response()->json(['success'=>true,'info'=>$correos]);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al listar los correos.'.$ex->getMessage()]);
        }
    }

}