<?php

namespace App\Traits;
use App\Factura;
use DB;

trait TraitFactura
{
	public static function getFacturas($id_semana)
	{
		$data = Factura::where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        $facturas = array();
        foreach ($data as $key => $value) {
            $facturas[] = array(
                'id'                  => $value->id,
                'id_cliente'          => $value->cliente->id,
                'cliente'             => $value->cliente->razon_social,
                'id_semana'           => $value->semana->id,
                'semana'              => $value->semana->nombre,
                'id_operador'         => $value->id_operador,
                'operador'            => ($value->id_operador) ? self::getNameOperador($value->id_operador) : null ,
                'nombre'              => $value->nombre,
                'fecha'               => $value->fecha,
                'hora'                => $value->hora,
                'url'                 => $value->url,
                'estado_archivo'      => $value->estado_archivo->nombre,
                'color_estado_archivo'=> $value->estado_archivo->color,
            );
        }
        return $facturas;
	}

    public static function changeStatusFactura($id_factura)
    {
        try{
            DB::beginTransaction();
                $factura = Factura::find($id_factura);
                $factura->fill(['id_estado_archivo'=>DB::raw(3)])->save();
            DB::commit();   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al cambiar de estado el archivo.'.$ex->getMessage()]);
        }
    }
}