<?php

namespace App\Traits;
use App\Valija;
use DB;

trait TraitValija
{
	public static function getValijas($id_semana)
	{
		$data = Valija::where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        $valijas = array();
        foreach ($data as $key => $value) {
            $valijas[] = array(
                'id'                  => $value->id,
                'cliente'             => $value->cliente->razon_social,
                'id_cliente'          => $value->cliente->id,
                'semana'              => $value->semana->nombre,
                'contenedores'        => self::getContenedoresOfValija($value),
                'nombre'              => $value->nombre,
                'fecha'               => $value->fecha,
                'hora'                => $value->hora,
                'url'                 => $value->url,
                'estado_archivo'      => $value->estado_archivo->nombre,
                'color_estado_archivo'=> $value->estado_archivo->color,
            );
        }
        return $valijas;
	}

    public static function getContenedoresOfValija($valija)
    {
        $contenedores = array();
        foreach ($valija->contenedores as $key => $value) {
            $contenedores[] = array(
                'id_contenedor' => $value->id,
                'numero'        => $value->nombre,
                'referencia'    => $value->referencia
            );
        }
        return $contenedores;
    }

    public static function changeStatusValija($id_valija)
    {
        try{
            DB::beginTransaction();
                $valija = Valija::find($id_valija);
                $valija->fill(['id_estado_archivo'=>DB::raw(3)])->save();
            DB::commit();   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al cambiar de estado el archivo.'.$ex->getMessage()]);
        }
    }
}