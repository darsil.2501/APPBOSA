<?php

namespace App\Traits;
use App\Certificado;
use App\Contenedor;
use App\Factura;
use App\Operador;
use App\Packing;
use App\Traits\TraitCertificado;
use App\Traits\TraitEstadoArchivo;
use App\Traits\TraitValija;
use App\Valija;

trait TraitOperador
{
    use TraitValija, TraitEstadoArchivo, TraitCertificado;
	// Obtenero el id del operador a partir de su SLUG
	public static function getIdOperador($slug_operador)
	{
		return Operador::where('slug',$slug_operador)->where('activo',1)->first()->id;
	}

	// Listar loc contenedores de un determinado operador
	public static function getContenedoresOperador($id_operador,$id_semana)
    {
        $contenedores = Contenedor::where('id_operador',$id_operador)
                       ->where('id_semana',$id_semana)
                       ->orderBy('fecha_proceso_inicio')
                       ->where('activo',1)->get();   
        
        $data_total = array();
        foreach ($contenedores as $contenedor) {
            $movimiento = $contenedor->movimientos->last();
            
            // recorremos para traer la información de las cajas que contiene
            $cajas_total = array();
            foreach ($contenedor->cajas as $value) {
                $caja['id']         	= $value->id;
                $caja['marca']      	= $value->marca;
                $caja['cantidad_cajas'] = $value->pivot->cantidad;
                $cajas_total []     	= $caja;
            }

            $data['cajas']                      = $cajas_total;
            $data['id_contenedor']              = $contenedor->id;
            $data['id_semana']                  = $contenedor->id_semana;
            $data['semana']                     = $contenedor->semana->nombre;
            $data['id_cliente']                 = $contenedor->id_cliente;
            $data['cliente']                    = $contenedor->cliente->razon_social;
            $data['email_cliente']              = $contenedor->cliente->email;
            $data['referencia']                 = $contenedor->referencia;
            $data['booking']                    = $contenedor->booking;
            $data['numero_contenedor']          = $contenedor->numero_contenedor;
            $data['id_lineanaviera']            = $contenedor->id_lineanaviera;
            $data['linea_naviera']              = $contenedor->linea_naviera->nombre;
            $data['sigla_linea_naviera']        = $contenedor->linea_naviera->sigla;
            // $data['id_nave']           = $contenedor->id_nave;
            $data['nave']                       = $contenedor->nave;
            $data['viaje']                      = $contenedor->viaje;
            $data['id_puertodestino']           = $contenedor->id_puertodestino;
            $data['puerto_destino']             = $contenedor->puerto_destino->nombre;
            $data['id_operador']                = $contenedor->id_operador;
            $data['operador']                   = $contenedor->operador->nombre;
            $data['dia_proceso_inicio']         = $contenedor->fecha_proceso_inicio;
            $data['dia_proceso_fin']            = $contenedor->fecha_proceso_fin;
            $data['dia_zarpe']                  = $contenedor->fecha_zarpe;
            $data['dia_llegada']                = $contenedor->fecha_llegada;
            $data['peso_bruto']                 = $contenedor->peso_bruto;
            $data['peso_neto']                  = $contenedor->peso_neto;
            $data['id_estado']                  = $movimiento->id_estado;
            $data['estado']                     = $movimiento->estado->nombre;
            $data['color_estado']               = $movimiento->estado->color;
            $data['estado_factura']             = self::getNameStatusFile($contenedor->factura);
            $data['color_estado_factura']       = self::getColorStatusFile($contenedor->factura);
            $data['estado_packing']             = self::getNameStatusFile($contenedor->packing);
            $data['color_estado_packing']       = self::getColorStatusFile($contenedor->packing);
            $data['estado_certificado']         = self::getNameStatusFile($contenedor->certificado);
            $data['color_estado_certificado']   = self::getColorStatusFile($contenedor->certificado);
            $data['estado_valija']              = self::getNameStatusFile($contenedor->valija);
            $data['color_estado_valija']        = self::getColorStatusFile($contenedor->valija);
            $data_total[]                       = $data;
            unset($cajas_total); // vaciamos el arreglo de cajas
        }
       return $data_total;
    }

    // ------------------------------- Listar las facturas de un operador cuando inicia sessión------------------------
    public static function getFacturasOperador($id_operador,$id_semana)
    {
        $data     = Factura::where('id_operador',$id_operador)->where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        $facturas = self::procesarData($data);
        return $facturas;
    }

    public static function getPackingsOperador($id_operador,$id_semana)
    {
        $clientes  = Contenedor::where('id_semana',$id_semana)
                         ->where('id_operador',$id_operador)
                         ->where('activo',1)
                         ->select('id_cliente')
                         ->get();

        // no tenero id de clientes repetidos
        $clientes = collect($clientes);
        $unique   = $clientes->unique();
        $clientes = $unique->values()->all();

        $data_packings = array();
        foreach ($clientes as $value) {
            $data_packings[] =  self::obtenerPackings($value->id_cliente,$id_semana);   
        }

        $data_packings = collect($data_packings);
        $collapsed     = $data_packings->collapse();
        $data_packings = $collapsed->all();
        return $data_packings;
    }

    public static function obtenerPackings($id_cliente,$id_semana)
    {
        $data = Packing::where('id_cliente',$id_cliente)->where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        $packings = array();
        foreach ($data as $key => $value) {
            $packings[] = array(
                'id'          => $value->id,
                'cliente'     => $value->cliente->razon_social,
                'id_cliente'  => $value->cliente->id,
                'semana'      => $value->semana->nombre,
                'nombre'      => $value->nombre,
                'fecha'       => $value->fecha,
                'hora'        => $value->hora,
                'url'         => $value->url,
                'estado_archivo'      => $value->estado_archivo->nombre,
                'color_estado_archivo'=> $value->estado_archivo->color,
            );
        }
        return $packings;
    }
    // ------------------------------- Listar las facturas de un cliente cuando inicia sessión------------------------

    // ------------------------------- Listar los certificados de un operador cuando inicia sessión--------------------
    public static function getCertificadosOperador($id_operador,$id_semana)
    {
        // $data         = Certificado::where('id_operador',$id_operador)->where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        // $certificados = self::procesarData($data);
        // return $certificados;
        $data_certificados = Certificado::whereHas('contenedores',function($query) use ($id_operador,$id_semana){
            $query->where('id_operador',$id_operador)->where('id_semana',$id_semana)->where('activo',1);
        })->get();
        
        $certificados = array();
        foreach ($data_certificados as $key => $value) {
            $certificados[] = array(
                'id'          => $value->id,
                'cliente'     => $value->cliente->razon_social,
                'id_cliente'  => $value->cliente->id,
                'semana'      => $value->semana->nombre,
                'contenedores'=> self::getContenedoresOfCertificado($value),
                'nombre'      => $value->nombre,
                'fecha'       => $value->fecha,
                'hora'        => $value->hora,
                'url'         => $value->url,
                'descripcion' => $value->descripcion,
                'estado_archivo'      => $value->estado_archivo->nombre,
                'color_estado_archivo'=> $value->estado_archivo->color,
            );
        }
        return $certificados;
    }
    // ------------------------------- fin Listar los certificados de un operador cuando inicia sessión--------------------

    public static function procesarData($informacion)
    {
        $data = array();
        foreach ($informacion as $key => $value) {
            $data[] = array(
                'id'        => $value->id,
                'cliente'   => $value->cliente->razon_social,
                'semana'    => $value->semana->nombre,
                'nombre'    => $value->nombre,
                'fecha'     => $value->fecha,
                'hora'      => $value->hora,
                'url'       => $value->url,
                'estado_archivo'      => $value->estado_archivo->nombre,
                'color_estado_archivo'=> $value->estado_archivo->color,
            );
        }
        return $data;
    }

    // Valijas del Operador cuando inicia sessión
    public static function getValijasOperador($id_operador,$id_semana)
    {
        $data_valijas = Valija::whereHas('contenedores',function($query) use ($id_operador,$id_semana){
            $query->where('id_operador',$id_operador)->where('id_semana',$id_semana)->where('activo',1);
        })->get();
        
        $valijas = array();
        foreach ($data_valijas as $key => $value) {
            $valijas[] = array(
                'id'          => $value->id,
                'cliente'     => $value->cliente->razon_social,
                'id_cliente'  => $value->cliente->id,
                'semana'      => $value->semana->nombre,
                'contenedores'=> self::getContenedoresOfValija($value),
                'nombre'      => $value->nombre,
                'fecha'       => $value->fecha,
                'hora'        => $value->hora,
                'url'         => $value->url,
                'estado_archivo'      => $value->estado_archivo->nombre,
                'color_estado_archivo'=> $value->estado_archivo->color,
            );
        }
        return $valijas;
    }
}