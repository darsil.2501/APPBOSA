<?php

namespace App\Traits;

use App\Persona;
use App\Inspector;
use App\Productor;
use App\User;
use App\Cargo;
use Hash;
use Illuminate\Support\Facades\DB;

trait TraitInspector
{
	# Obtener todos los productores con su respectiva información
	public static function obtenerInspectores()
	{
		try{
            $inspectores = Inspector::with('persona')->where('activo',1)->get();
            $data        = array();
            foreach ($inspectores as $inspector) {
                $data[] = array(
                    'id'                =>$inspector->id,
                    'id_persona'        =>$inspector->persona->id,
                    'dni'               =>$inspector->persona->dni,
                    'nombre'            =>$inspector->persona->nombre,
                    'apellido_paterno'  =>$inspector->persona->apellido_paterno,
                    'apellido_materno'  =>$inspector->persona->apellido_materno,
                    'fullname'          =>$inspector->persona->fullname,
                    'direccion'         =>$inspector->persona->direccion,
                    'email'             =>$inspector->persona->email,
                    'cantidad_parcelas' =>count($inspector->parcelas),
                    'activo'            =>$inspector->activo
                );
            }            

            $collecion = collect($data);
            $ordenar   = $collecion->sortBy('apellido_paterno');
            $data      = $ordenar->values()->all();
            
            return response()->json(['info'=>$data,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Error al listar los registros'.$e->getMessage(),'success'=>false]);    
        }
	}

	# Registrar un nuevo productor
	public static function registrarInspector($request)
	{
		try{
            // if(!self::hasPermiso('productor.registrar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $cargo = Cargo::where('nombre', 'like', '%nspector%')->first();
                $full_name = $request['apellido_paterno'].' '.$request['apellido_materno'].' '.$request['nombre'];

                $persona   = Persona::create([
                    'dni'               =>  $request['dni'],
                    'nombre'            =>  $request['nombre'],
                    'apellido_paterno'  =>  $request['apellido_paterno'],
                    'apellido_materno'  =>  $request['apellido_materno'],
                    'fullname'          =>  $full_name,
                    'direccion'         =>  $request['direccion'],
                    'email'             =>  $request['email'],
                    'id_cargo'          =>  $cargo->id
                ]);
                
                Inspector::create([
                    'id_persona'       => $persona->id
                ]);

                User::create([
                    'nick'                =>  $request['dni'],
                    'password'            =>  Hash::make($request['dni']),
                    'id_persona'          =>  $persona->id
                ]);

            DB::commit();
            // self::auditarInspector('INSERTAR',$full_name);
            return response()->json(['message'=>'Inspector creado con su usuario correctamente. Usuario  y password es su dni.','success'=>true]);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['message'=>'Error el ralizar la operación.Error: '.$ex->getMessage(),'success'=>false]);
        }
	}

	# Actualizar datos de un Inspector
	public static function actualizarInspector($request, $id_inspector)
	{
		try{
            // if(!self::hasPermiso('productor.actualizar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $productor = Inspector::find($id_inspector);
                $persona   = Persona::find($productor->id_persona);
                // $validar   = Inspector::validateUpdateInspector($request->all(),$productor->id_persona);

                // if($validar->fails()){return response()->json(['success'=>false,'messages'=>$validar->errors()->all()]);}
                
                $full_name = $request['nombre'].' '.$request['apellido_paterno'].' '.$request['apellido_materno'];
                $persona->fill([
                    'dni'               =>  $request['dni'],
                    'nombre'            =>  $request['nombre'],
                    'apellido_paterno'  =>  $request['apellido_paterno'],
                    'apellido_materno'  =>  $request['apellido_materno'],
                    'fullname'          =>  $full_name,
                    'direccion'         =>  $request['direccion'],
                    'email'             =>  $request['email']
                ])->save();

                // $productor->fill([
                //     'socio'            => $request['socio'],
                //     'codigo_productor' => $request['codigo_productor'],
                //     'ggn'              => $request['ggn'],
                //     'fecha_ingreso'    => $request['fecha_ingreso']
                // ])->save();
            DB::commit();
            // self::auditarInspector('ACTUALIZAR',$full_name);
            return response()->json(['success'=>true,'message'=>'Inspector actualizado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>true,'message'=>'Error al actualizar datos del Inspector. Error: '.$ex->getMessage()]);
        }
	}

	# Eliminar Inspector
	public static function eliminarInspector($id_inspector)
	{
		try{
            // if(!self::hasPermiso('inspector.eliminar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $inspector = Inspector::find($id_inspector);
                $inspector->fill(['activo'=>DB::raw(0)])->save();
                $persona = Persona::find($inspector->id_persona);
                $persona->fill(['activo'=>DB::raw(0)])->save();
            DB::commit();
            // self::auditarInspector('ELIMINAR',$productor->persona->fullname);
            return response()->json(['success'=>true,'message'=>'Inspector eliminado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el Inspector. Error:' .$ex->getMessage()]);
        }
	}

    # Listar Sus Parcelas
    public static function obtenerParcelasInspector($request)
    {
        try{

            // $parcelas_temp = \App\Parcela::where('id_sector',$request['id_sector'])->
            // whereHas('inspector' , function ($query) {
            //     $query->where('id', 4);
            // })->get();

            $parcelas_temp = \App\Parcela::where('id_sector',$request['id_sector'])->where('id_inspector',$request['id_inspector'])->get();

            $productores = array();
            $productores_temp = array();

            foreach ($parcelas_temp as $parcela) {
                $productores_temp[] = array(
                    'id'                => $parcela->productor->id,
                    'fullname'          => $parcela->productor->persona->fullname,
                    'codigo_productor'  => $parcela->productor->codigo_productor,
                    'ggn'               => $parcela->productor->ggn,
                    'direccion'         => $parcela->productor->persona->direccion,
                    'dni'               => $parcela->productor->persona->dni,
                    'fecha_ingreso'     => $parcela->productor->fecha_ingreso                  
                );
            }

            $productores_temp = array_map("unserialize", array_unique(array_map("serialize", $productores_temp)));

            foreach ($productores_temp as $productor) {
                $parcelas = [];
                foreach ($parcelas_temp as $parcela) {
                    if($parcela->productor->id == $productor['id'])
                    {
                        $parcelas[] = array(
                            'id'                    => $parcela->id,
                            'empacadora'            => $parcela->empacadora->nombre,
                            'sector'                => $parcela->sector->nombre,
                            'ha_total'              => $parcela->ha_total,
                            'ha_sembrada'           => $parcela->ha_sembrada,
                            'numero_parcela'        => $parcela->numero_parcela,
                            'codigo_parcela'        => $parcela->codigo_parcela,
                            'id_sector'             => $parcela->id_sector,
                            'id_empacadora'         => $parcela->id_empacadora
                        );
                    }
                }
                $productores[] = array(
                    'id'                => $productor["id"],
                    'fullname'          => $productor["fullname"],
                    'codigo_productor'  => $productor["codigo_productor"],
                    'ggn'               => $productor["ggn"],
                    'dni'               => $productor["dni"],
                    'direccion'         => $productor["direccion"],
                    'fecha_ingreso'     => $productor["fecha_ingreso"],
                    'parcelas'          => $parcelas,
                    'cantidad_parcelas' => count($parcelas_temp)
                );
            }

            return $productores;

        }catch(Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al eliminar el Inspector. Error:' .$ex->getMessage()]);
        }
    }
}