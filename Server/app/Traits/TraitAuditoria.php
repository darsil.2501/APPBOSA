<?php

namespace App\Traits;
use DB;
use Auth;

trait TraitAuditoria
{
	// Para realizar las auditorias de los usuarios
	public static function auditarUser($operacion,$usuario_afectado)
	{
		try{
		    DB::beginTransaction();
		    	DB::table('audit_usuario')->insert([
		    		'usuario_responsable' => Auth::user()->persona->fullname,
		    		'usuario_afectado'	  => $usuario_afectado,
		    		'operacion'			  => $operacion,
		    		'fecha'				  => date('Y-m-d'),
		    		'hora'				  => date('H:i:s'),
		    		'ip'				  => $_SERVER['REMOTE_ADDR'], 
		    		'dispositivo'		  => gethostbyaddr($_SERVER['REMOTE_ADDR'])
		    	]);
		    DB::commit();
		}catch(\Exception $ex){
		    DB::rollback();
		}
	}

	// Para realizar las auditorias de los productores
	public static function auditarProductor($operacion,$usuario_afectado)
	{
		try{
		    DB::beginTransaction();
		    	DB::table('audit_productor')->insert([
		    		'usuario_responsable' => Auth::user()->persona->fullname,
		    		'usuario_afectado'	  => $usuario_afectado,
		    		'operacion'			  => $operacion,
		    		'fecha'				  => date('Y-m-d'),
		    		'hora'				  => date('H:i:s'),
		    		'ip'				  => $_SERVER['REMOTE_ADDR'], 
		    		'dispositivo'		  => gethostbyaddr($_SERVER['REMOTE_ADDR'])
		    	]);
		    DB::commit();
		}catch(\Exception $ex){
		    DB::rollback();
		}
	}

	// Para registrar auditorias de los mantenimientos
	public static function auditar($tabla_auditoria,$registro,$operacion)
	{
		try{
		    DB::beginTransaction();
		    	DB::table($tabla_auditoria)->insert([
		    		'usuario_responsable' => Auth::user()->persona->fullname,
		    		'registro'			  => $registro,
		    		'operacion' 		  => $operacion,
		    		'fecha'				  => date('Y-m-d'),
		    		'hora'				  => date('H:i:s'),
		    		'ip'				  => $_SERVER['REMOTE_ADDR'], 
		    		'dispositivo'		  => gethostbyaddr($_SERVER['REMOTE_ADDR'])
		    	]);
		    DB::commit();
		}catch(\Exception $ex){
		    DB::rollback();
		}
	}

	// Para registrar auditorias del contenedor
	public static function auditarContenedor($semana,$numero_contenedor,$referencia,$operacion)
	{
		try{
		    DB::beginTransaction();
		    	DB::table('audit_contenedor')->insert([
		    		'usuario_responsable'	=> Auth::user()->persona->fullname,
		    		'semana'				=> $semana,
		    		'numero_contenedor'		=> $numero_contenedor,
		    		'referencia'			=> $referencia,
		    		'operacion'				=> $operacion,
		    		'fecha'				  	=> date('Y-m-d'),
		    		'hora'				  	=> date('H:i:s'),
		    		'ip'				  	=> $_SERVER['REMOTE_ADDR'], 
		    		'dispositivo'		  	=> gethostbyaddr($_SERVER['REMOTE_ADDR'])
		    	]);
		    DB::commit();
		}catch(\Exception $ex){
		    DB::rollback();
		}
	}

	public static function getNameTableUpper($tabla,$accion = 0)
	{
		return ($accion == 1)	? ucfirst(substr($tabla,6)) : substr($tabla,6);
	}
}