<?php

namespace App\Traits;
use App\Contenedor;

trait TraitCaja
{
	public static function getCantidadCajasPorClienteAndSemana($id_cliente,$id_caja,$id_semana)
    {
        $total_cajas  = 0;
        $contenedores = Contenedor::where('id_cliente',$id_cliente)->where('id_semana',$id_semana)->where('activo',1)->get();
        foreach ($contenedores as $contenedor) {
            $cajas = $contenedor->cajas->where('id',$id_caja);
            // dd($cajas);
            foreach ($cajas as $caja) {
                $total_cajas = $total_cajas + $caja->pivot->cantidad;
            }
        }
        return $total_cajas;
    }
}