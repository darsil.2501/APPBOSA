<?php

namespace App\Traits;

use App\Visita;
use App\VisitaAuditoriaInterna;
use Illuminate\Support\Facades\DB;

trait TraitVisita
{
	# Obtener todos los productores con su respectiva información
	public static function obtenerVisitas($id_parcela)
	{
		try{
            $visitas_temp = Visita::where('id_parcela',$id_parcela)->where('activo',1)->get();

            $visitas = array();
            foreach ($visitas_temp as $key => $value) {
                $visitas[] = array(
                    'id'              => $value->id,
                    'observacion'     => $value->observacion,
                    'fecha'           => $value->fecha,
                    'id_parcela'      => $value->id_parcela,
                    'activo'          => $value->activo,
                    'numero_imagenes' => count($value->galeria_visita),
                    'isExport'        => false
                );
            }
                     
            return response()->json(['info'=>$visitas,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Error al listar los registros'.$e->getMessage(),'success'=>false]);    
        }
	}

	# Registrar un nuevo productor
	public static function registrarVisita($request)
	{
		try{
            //return $request->all();
            // if(!self::hasPermiso('productor.registrar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $visita = Visita::create([
                    'fecha'       =>  $request['fecha'],
                    'observacion' =>  $request['observacion'],
                    'responsable'  =>  $request['responsable'],
                    'id_parcela'  =>  $request['id_parcela']
                ]);

                foreach ($request['preguntas'] as $pregunta) {
                    
                    VisitaAuditoriaInterna::create([
                        'respuesta'                        =>  intval($pregunta['respuesta']),
                        'observacion'                      =>  $pregunta['observacion'],
                        'id_visita'                        =>  $visita->id,
                        'id_formulario_auditoria_interna'  =>  $pregunta['id']
                    ]);

                }

            DB::commit();
            // self::auditarVisita('INSERTAR',$full_name);
            return response()->json(['message'=>'Visita creada con su usuario correctamente.','success'=>true]);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['message'=>'Error el ralizar la operación.Error: '.$ex->getMessage(),'success'=>false]);
        }
	}

	# Actualizar datos de un Visita
	public static function actualizarVisita($request, $id_visita)
	{
		try{
            // if(!self::hasPermiso('productor.actualizar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $visita   = Visita::find($id_visita);
                
                $visita->fill([
                    'fecha'       =>  $request['fecha'],
                    'observacion' =>  $request['observacion']
                ])->save();

            DB::commit();
            // self::auditarVisita('ACTUALIZAR',$full_name);
            return response()->json(['success'=>true,'message'=>'Visita actualizada correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>true,'message'=>'Error al actualizar datos de la Visita. Error: '.$ex->getMessage()]);
        }
	}

	# Eliminar Visita
	public static function eliminarVisita($id_visita)
	{
		try{
            // if(!self::hasPermiso('visita.eliminar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $visita = Visita::find($id_visita);
                $visita->fill(['activo'=>DB::raw(0)])->save();
            DB::commit();
            // self::auditarVisita('ELIMINAR',$productor->persona->fullname);
            return response()->json(['success'=>true,'message'=>'Visita eliminada correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar la Visita. Error:' .$ex->getMessage()]);
        }
	}

}