<?php

namespace App\Traits;

use App\EstadoArchivo;

trait TraitEstadoArchivo
{

    public function getEstadoArchivos()
    {
        try{
            $estados_archivos = EstadoArchivo::where('activo',1)->get();
            return response()->json(['info'=>$estados_archivos,'success'=>true]);
        }catch (\Exception $ex)
        {
            return response()->json(['message'=>'Error al listar los estado de los archivos.'.$ex->getMessage(),'success'=>false]);
        }
    }


   // ------------------------ Obtener el nombre de estado del archivo ------------------------------------------------------
    public static function getNameStatusFile($id_estado_archivo)
    {
        $estado_archivo = EstadoArchivo::find($id_estado_archivo);
        return $estado_archivo->nombre;
    }
    // ------------------------ Fin Obtener el nombre de estado del archivo --------------------------------------------------
    
    // ------------------------ Obtener el Color de estado del archivo ------------------------------------------------------
    public static function getColorStatusFile($id_estado_archivo)
    {
        $estado_archivo = EstadoArchivo::find($id_estado_archivo);
        return $estado_archivo->color;
    }
    // ------------------------ Fin Obtener el nombre de estado del archivo --------------------------------------------------
 }