<?php

namespace App\Traits;

trait TraitRespuesta
{
	public static function RegistroInUse()
	{
		return response()->json(['message'=>'Este registro no se puede eliminar, está en uso.','success'=>false]);
	}

	public static function RegistroCreateSuccess()
	{
		return response()->json(['message'=>'Registro creado correctamente','success'=>true]);    
	}

	public static function RegistroUpdateSuccess()
	{
		return response()->json(['message'=>'Registro actualizado correctamente','success'=>true]);        
	}		

	public static function RegistroDeleteSuccess()
	{
		return response()->json(['message'=>'Registro eliminado correctamente','success'=>true]);        
	}

	public static function HasNoPermiso()
	{
		return response()->json(['message'=>'No tiene permisos para realizar esta operación','success'=>false]);        	
	}

	public static function MensajePersonalizado($success,$mensaje)
	{
		return response()->json(['success'=>$success,'message'=>$mensaje]);	
	}

	public static function ErrorInOperation($error)
	{
		return response()->json(['success'=>false,'message'=>'Ocurrió un problema al intentar realizar esta operación, inténtelo nuevamente. Error: '.$error->getMessage()]);
	}

	public static function errors($errors)
	{
		return response()->json(['success'=>false,'messages'=>$errors]);		
	}
}