<?php

namespace App\Traits;


use App\VisitaItemLabor;
use App\Visita;
use Illuminate\Support\Facades\DB;

trait TraitVisitaItemLabor
{
	public static function obtenerVisitasItemLabor($id_parcela)
	{
		try{
            $visitas_temp = Visita::where('id_parcela',$id_parcela)->where('formulario',2)->where('activo',1)->orderBy('fecha','desc')->get();

            $visitas = array();
            foreach ($visitas_temp as $key => $value) {
                $visitas[] = array(
                    'id'              => $value->id,
                    'observacion'     => $value->observacion,
                    'fecha'           => $value->fecha,
                    'id_parcela'      => $value->id_parcela,
                    'activo'          => $value->activo,
                    'numero_imagenes' => count($value->galeria_visita),
                    'isExport'        => false
                );
            }
                     
            return response()->json(['info'=>$visitas,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al listar los registros'.$e->getMessage(),'success'=>false]);    
        }
	}

	public static function registrarVisitaItemLabor($request)
	{
        try{
            //return $request->all();
            // if(!self::hasPermiso('productor.registrar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $visita = Visita::create([
                    'fecha'         =>  $request['fecha'],
                    'observacion'   =>  $request['observacion'],
                    'responsable'   =>  $request['responsable'],
                    'formulario'    =>  2,
                    'id_parcela'    =>  $request['id_parcela']
                ]);

                foreach ($request['labores'] as $labor) {
                    foreach ($labor['items'] as $item) {
                        if ($item['total'] >= 0) {
                            VisitaItemLabor::create([
                                'total'         => $item['total'],
                                'id_visita'     => $visita->id,
                                'id_item_labor' => $item['id']
                            ]);
                        }                        
                    } 
                }

            DB::commit();
            // self::auditarVisita('INSERTAR',$full_name);
            return response()->json(['message'=>'Visita creada correctamente.','success'=>true]);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['message'=>'Error el ralizar la operación.Error: '.$ex->getMessage(),'success'=>false]);
        }
	}

	public static function actualizarVisitaItemLabor($request, $id)
	{
		try{
            DB::beginTransaction();

            $item = VisitaItemLabor::find($id);
            $item->fill([
                'total'         => $request['total'],
                'id_visita'     => $request['id_visita'],
                'id_item_labor' => $request['id_item_labor']
            ])->save();

            DB::commit();
            return response()->json(['message'=>'Registro Actualizado Correctamente','success'=>true]); 
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en item de labor, inténtelo nuevamente. '.$ex->getMessage()]); 
        }
	}

	public static function eliminarVisitaItemLabor($id)
	{
		try{
            DB::beginTransaction();

            $item = Visita::find($id);
            $item->fill(['activo'=>DB::raw(0)])->save(); 

            DB::commit();               
            return response()->json(['message'=>'Registro Eliminado Correctamente','success'=>true]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en eliminar item de labor, inténtelo nuevamente. '.$ex->getMessage()]); 
        }
	}

    # Exportar PDF
    public static function exportPDFVisitaLabores($data)
    {
        try{
            $pdf = \PDF::loadView('sic.ficha_dos',['data'=>$data]);
            return $pdf->download('reporte_sic.pdf');
        }catch(Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al generar pdf la Visita. Error:' .$ex->getMessage()]);
        }
    }
}