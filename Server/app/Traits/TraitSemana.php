<?php

namespace App\Traits;
use App\Contenedor;
use App\Semana;

trait TraitSemana
{
	public static function getContenedoresPorSemana($id_semana)
	{
		$cantidad_contenedores = count(Contenedor::where('id_semana',$id_semana)->where('activo',1)->get());
		return $cantidad_contenedores;
	}

	public static function existSemana($data)
	{
		$registro = Semana::where('nombre',$data['nombre'])->where('id_anio',$data['id_anio'])->where('activo',1)->first();
        if ($registro) {
            // return self::MensajePersonalizado(false,'La semana que está intentando registrar para este año ya existe.');
            return true;
        }else{
        	return false;
        }
	}
}