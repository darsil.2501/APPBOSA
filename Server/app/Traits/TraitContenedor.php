<?php

namespace App\Traits;
use App\Movimiento;
use App\Semana;
use App\Anio;
use App\Contenedor;
use App\Traits\TraitEstadoArchivo;

trait TraitContenedor
{
    use TraitEstadoArchivo;

    // está consulta se usa cuando no se envía el año y es solo para cuando se listan
    // las semanas
	public static function cargarContenedores($id_semana = "")
	{
        $contenedores = Contenedor::where('activo',1)->orderBy('fecha_proceso_inicio');
        if ($id_semana != "") {$contenedores->where('id_semana',$id_semana);}
        else{$contenedores->where('id_semana',self::getIdUltimoSemana());}
        $contenedores = $contenedores->get();

        $data_total = array();

        foreach ($contenedores as $contenedor) {
            $movimiento = $contenedor->movimientos->last();
            // recorremos para traer la información de las cajas que contiene
            $cajas_total = array();
            foreach ($contenedor->cajas as $value) {
                $caja['id']               = $value->id;
                $caja['marca']            = $value->marca;
                $caja['peso']             = $value->peso;
                $caja['cantidad_cajas']   = $value->pivot->cantidad;
                $caja['save']             = true;
                $cajas_total []           = $caja;
            }
            $data['cajas']                  = $cajas_total;
            $data['id_contenedor']          = $contenedor->id;
            $data['id_semana']              = $contenedor->id_semana;
            $data['semana']                 = $contenedor->semana->nombre;
            $data['id_cliente']             = $contenedor->id_cliente;
            $data['cliente']                = $contenedor->cliente->razon_social;
            $data['email_cliente']          = $contenedor->cliente->email;
            $data['referencia']             = $contenedor->referencia;
            $data['booking']                = $contenedor->booking;
            $data['numero_contenedor']      = $contenedor->numero_contenedor;
            $data['id_lineanaviera']        = $contenedor->id_lineanaviera;
            $data['linea_naviera']          = $contenedor->linea_naviera->nombre;
            $data['sigla_linea_naviera']    = $contenedor->linea_naviera->sigla;
            $data['nave']                   = $contenedor->nave;
            $data['viaje']                  = $contenedor->viaje;
            $data['id_puertodestino']       = $contenedor->id_puertodestino;
            $data['puerto_destino']         = $contenedor->puerto_destino->nombre;
            $data['id_operador']            = $contenedor->id_operador;
            $data['operador']               = $contenedor->operador->nombre;
            $data['dia_proceso_inicio']     = $contenedor->fecha_proceso_inicio;
            $data['dia_proceso_fin']        = $contenedor->fecha_proceso_fin;
            $data['dia_zarpe']              = $contenedor->fecha_zarpe;
            $data['dia_llegada']            = $contenedor->fecha_llegada;
            $data['id_estado']              = $movimiento->id_estado;
            $data['estado']                 = $movimiento->estado->nombre;
            $data['color_estado']           = $movimiento->estado->color;
            $data['peso_bruto']             = $contenedor->peso_bruto;
            $data['peso_neto']              = $contenedor->peso_neto;
            $data['estado_factura']         = self::getNameStatusFile($contenedor->factura);
            $data['color_estado_factura']   = self::getColorStatusFile($contenedor->factura);
            $data['estado_packing']         = self::getNameStatusFile($contenedor->packing);
            $data['color_estado_packing']   = self::getColorStatusFile($contenedor->packing);
            $data['estado_certificado']     = self::getNameStatusFile($contenedor->certificado);
            $data['color_estado_certificado']   = self::getColorStatusFile($contenedor->certificado);
            $data['estado_valija']              = self::getNameStatusFile($contenedor->valija);
            $data['color_estado_valija']        = self::getColorStatusFile($contenedor->valija);
            $data['estado_vl']                  = self::getNameStatusFile($contenedor->vl);
            $data['color_estado_vl']            = self::getColorStatusFile($contenedor->vl);
            $data_total[]                       = $data;
            unset($cajas_total); // vaciamos el arreglo de cajas
        }
       return $data_total;
	}


    #--------------------- Funciones para reportes--------------------------------------
    public static function contenedoresPorSemana($id_anio,$id_semana = "")
    {
        if ($id_semana != "") {
            $contenedores = Contenedor::where('id_semana',$id_semana)
                            ->where('activo',1)
                            ->orderBy('fecha_proceso_inicio')
                            ->get();   
        }else{
            $semanas = Semana::where('id_anio',$id_anio)->where('activo',1)->get();
            foreach ($semanas as $semana) {
                $containers[] = $semana->contenedor->where('activo',1);
            }

            // para hacer uno solo la collecion
            $collection   = collect($containers);
            $collapsed    = $collection->collapse();
            $contenedores = $collapsed->all();

            // para ordenar por un atributo la coleccion
            $collection    = collect($contenedores);
            $sorted        = $collection->sortBy('fecha_proceso_inicio');
            $contenedores  = $sorted->values()->all();

        }
        $data_total = array();

        foreach ($contenedores as $contenedor) {
            $movimiento = $contenedor->movimientos->last();
            // recorremos para traer la información de las cajas que contiene
            $cajas_total = array();
            foreach ($contenedor->cajas as $value) {
                $caja['id']               = $value->id;
                $caja['marca']            = $value->marca;
                $caja['peso']             = $value->peso;
                $caja['cantidad_cajas']   = $value->pivot->cantidad;
                $caja['save']             = true;
                $cajas_total []           = $caja;
            }
            $data['cajas']                  = $cajas_total;
            $data['id_contenedor']          = $contenedor->id;
            $data['id_semana']              = $contenedor->id_semana;
            $data['semana']                 = $contenedor->semana->nombre;
            $data['id_cliente']             = $contenedor->id_cliente;
            $data['cliente']                = $contenedor->cliente->razon_social;
            $data['email_cliente']          = $contenedor->cliente->email;
            $data['referencia']             = $contenedor->referencia;
            $data['booking']                = $contenedor->booking;
            $data['numero_contenedor']      = $contenedor->numero_contenedor;
            $data['id_lineanaviera']        = $contenedor->id_lineanaviera;
            $data['linea_naviera']          = $contenedor->linea_naviera->nombre;
            $data['sigla_linea_naviera']    = $contenedor->linea_naviera->sigla;
            $data['nave']                   = $contenedor->nave;
            $data['viaje']                  = $contenedor->viaje;
            $data['id_puertodestino']       = $contenedor->id_puertodestino;
            $data['puerto_destino']         = $contenedor->puerto_destino->nombre;
            $data['id_operador']            = $contenedor->id_operador;
            $data['operador']               = $contenedor->operador->nombre;
            $data['dia_proceso_inicio']     = $contenedor->fecha_proceso_inicio;
            $data['dia_proceso_fin']        = $contenedor->fecha_proceso_fin;
            $data['dia_zarpe']              = $contenedor->fecha_zarpe;
            $data['dia_llegada']            = $contenedor->fecha_llegada;
            $data['id_estado']              = $movimiento->id_estado;
            $data['estado']                 = $movimiento->estado->nombre;
            $data['color_estado']           = $movimiento->estado->color;
            $data['peso_bruto']             = $contenedor->peso_bruto;
            $data['peso_neto']              = $contenedor->peso_neto;
            $data['estado_factura']         = self::getNameStatusFile($contenedor->factura);
            $data['color_estado_factura']   = self::getColorStatusFile($contenedor->factura);
            $data['estado_packing']         = self::getNameStatusFile($contenedor->packing);
            $data['color_estado_packing']   = self::getColorStatusFile($contenedor->packing);
            $data['estado_certificado']     = self::getNameStatusFile($contenedor->certificado);
            $data['color_estado_certificado']   = self::getColorStatusFile($contenedor->certificado);
            $data['estado_valija']              = self::getNameStatusFile($contenedor->valija);
            $data['color_estado_valija']        = self::getColorStatusFile($contenedor->valija);
            $data['estado_vl']                  = self::getNameStatusFile($contenedor->vl);
            $data['color_estado_vl']            = self::getColorStatusFile($contenedor->vl);
            $data_total[]                       = $data;
            unset($cajas_total); // vaciamos el arreglo de cajas
        }
        // para ordenar por un atributo la coleccion
        // $collection    = collect($data_total);
        // $sorted        = $collection->sortByDesc('semana');
        // $data_total    = $sorted->values()->all();
        return $data_total;
    }

    public static function getUltimoIdMovimiento($id_contenedor)
    {
        return Movimiento::where('id_contenedor',$id_contenedor)->max('id');
    }

    public static function getIdUltimoSemana()
    {
        $id_anio_actual = Anio::where('anio',date('Y'))->select('id')->first();
        $semana         = Semana::where('id_anio',$id_anio_actual->id)->where('activo',1)->max('nombre');
        $id_semana      = Semana::where('nombre',$semana)->where('activo',1)->select('id')->first();
        return $id_semana->id;
        // return Semana::where('id_anio',$id_anio_actual->id)->where('activo',1)->max('nombre');
    }

    public static function contenedorPorCliente($id_cliente,$id_anio,$id_semana = "")
    {
        if ($id_semana != "") {
            $contenedores = Contenedor::where('id_cliente',$id_cliente)->where('id_semana',$id_semana)->where('activo',1)->orderBy('fecha_proceso_inicio')->get();   
        }else{
            $semanas = Semana::where('id_anio',$id_anio)->where('activo',1)->get();
            foreach ($semanas as $semana) {
                $containers[] = $semana->contenedor->where('id_cliente',$id_cliente)->where('activo',1);
            }

            // para hacer uno solo la collecion
            $collection   = collect($containers);
            $collapsed    = $collection->collapse();
            $contenedores = $collapsed->all();

            // para ordenar por un atributo la coleccion
            $collection    = collect($contenedores);
            $sorted        = $collection->sortBy('fecha_proceso_inicio');
            $contenedores  = $sorted->values()->all();

        }
        $data_total = array();
        foreach ($contenedores as $contenedor) {
            $movimiento = $contenedor->movimientos->last();
            // recorremos para traer la información de las cajas que contiene
            $cajas_total = array();
            foreach ($contenedor->cajas as $value) {
                $caja['id']             = $value->id;
                $caja['marca']          = $value->marca;
                $caja['peso']           = $value->peso;
                $caja['cantidad_cajas'] = $value->pivot->cantidad;
                $cajas_total []         = $caja;
            }
            $data['cajas']                  = $cajas_total;
            $data['id_contenedor']          = $contenedor->id;
            $data['id_semana']              = $contenedor->id_semana;
            $data['semana']                 = $contenedor->semana->nombre;
            $data['id_cliente']             = $contenedor->id_cliente;
            $data['cliente']                = $contenedor->cliente->razon_social;
            $data['email_cliente']          = $contenedor->cliente->email;
            $data['referencia']             = $contenedor->referencia;
            $data['booking']                = $contenedor->booking;
            $data['numero_contenedor']      = $contenedor->numero_contenedor;
            $data['id_lineanaviera']        = $contenedor->id_lineanaviera;
            $data['linea_naviera']          = $contenedor->linea_naviera->nombre;
            $data['sigla_linea_naviera']    = $contenedor->linea_naviera->sigla;
            $data['nave']                   = $contenedor->nave;
            $data['viaje']                  = $contenedor->viaje;
            $data['id_puertodestino']       = $contenedor->id_puertodestino;
            $data['puerto_destino']         = $contenedor->puerto_destino->nombre;
            $data['id_operador']            = $contenedor->id_operador;
            $data['operador']               = $contenedor->operador->nombre;
            $data['dia_proceso_inicio']     = $contenedor->fecha_proceso_inicio;
            $data['dia_proceso_fin']        = $contenedor->fecha_proceso_fin;
            $data['dia_zarpe']              = $contenedor->fecha_zarpe;
            $data['dia_llegada']            = $contenedor->fecha_llegada;
            $data['peso_bruto']             = $contenedor->peso_bruto;
            $data['peso_neto']              = $contenedor->peso_neto;
            $data['id_estado']              = $movimiento->id_estado;
            $data['estado']                 = $movimiento->estado->nombre;
            $data['color_estado']           = $movimiento->estado->color;
            $data['estado_factura']         = self::getNameStatusFile($contenedor->factura);
            $data['color_estado_factura']   = self::getColorStatusFile($contenedor->factura);
            $data['estado_packing']         = self::getNameStatusFile($contenedor->packing);
            $data['color_estado_packing']   = self::getColorStatusFile($contenedor->packing);
            $data['estado_certificado']     = self::getNameStatusFile($contenedor->certificado);
            $data['color_estado_certificado']   = self::getColorStatusFile($contenedor->certificado);
            $data['estado_valija']              = self::getNameStatusFile($contenedor->valija);
            $data['color_estado_valija']        = self::getColorStatusFile($contenedor->valija);
            $data['estado_vl']                  = self::getNameStatusFile($contenedor->vl);
            $data['color_estado_vl']            = self::getColorStatusFile($contenedor->vl);
            $data_total[]                       = $data;
            unset($cajas_total); // vaciamos el arreglo de cajas
        }
       return $data_total;
    }

    public static function contenedorPorOperador($id_operador,$id_anio,$id_semana = "")
    {
        if ($id_semana != "") {
            $contenedores = Contenedor::where('id_operador',$id_operador)->where('id_semana',$id_semana)->where('activo',1)->orderBy('fecha_proceso_inicio')->get();   
        }else{
            $semanas = Semana::where('id_anio',$id_anio)->where('activo',1)->get();
            foreach ($semanas as $semana) {
                $containers[] = $semana->contenedor->where('id_operador',$id_operador)->where('activo',1);
            }

            // para hacer uno solo la collecion
            $collection   = collect($containers);
            $collapsed    = $collection->collapse();
            $contenedores = $collapsed->all();

            // para ordenar por un atributo la coleccion
            $collection    = collect($contenedores);
            $sorted        = $collection->sortBy('fecha_proceso_inicio');
            $contenedores  = $sorted->values()->all();

        }
        $data_total = array();
        foreach ($contenedores as $contenedor) {
            $movimiento = $contenedor->movimientos->last();
            // recorremos para traer la información de las cajas que contiene
            $cajas_total = array();
            foreach ($contenedor->cajas as $value) {
                $caja['id']             = $value->id;
                $caja['marca']          = $value->marca;
                $caja['peso']           = $value->peso;
                $caja['cantidad_cajas'] = $value->pivot->cantidad;
                $cajas_total []         = $caja;
            }
            $data['cajas']                  = $cajas_total;
            $data['id_contenedor']          = $contenedor->id;
            $data['id_semana']              = $contenedor->id_semana;
            $data['semana']                 = $contenedor->semana->nombre;
            $data['id_cliente']             = $contenedor->id_cliente;
            $data['cliente']                = $contenedor->cliente->razon_social;
            $data['email_cliente']          = $contenedor->cliente->email;
            $data['referencia']             = $contenedor->referencia;
            $data['booking']                = $contenedor->booking;
            $data['numero_contenedor']      = $contenedor->numero_contenedor;
            $data['id_lineanaviera']        = $contenedor->id_lineanaviera;
            $data['linea_naviera']          = $contenedor->linea_naviera->nombre;
            $data['sigla_linea_naviera']    = $contenedor->linea_naviera->sigla;
            $data['nave']                   = $contenedor->nave;
            $data['viaje']                  = $contenedor->viaje;
            $data['id_puertodestino']       = $contenedor->id_puertodestino;
            $data['puerto_destino']         = $contenedor->puerto_destino->nombre;
            $data['id_operador']            = $contenedor->id_operador;
            $data['operador']               = $contenedor->operador->nombre;
            $data['dia_proceso_inicio']     = $contenedor->fecha_proceso_inicio;
            $data['dia_proceso_fin']        = $contenedor->fecha_proceso_fin;
            $data['dia_zarpe']              = $contenedor->fecha_zarpe;
            $data['dia_llegada']            = $contenedor->fecha_llegada;
            $data['peso_bruto']             = $contenedor->peso_bruto;
            $data['peso_neto']              = $contenedor->peso_neto;
            $data['id_estado']              = $movimiento->id_estado;
            $data['estado']                 = $movimiento->estado->nombre;
            $data['color_estado']           = $movimiento->estado->color;
            $data['estado_factura']         = self::getNameStatusFile($contenedor->factura);
            $data['color_estado_factura']   = self::getColorStatusFile($contenedor->factura);
            $data['estado_packing']         = self::getNameStatusFile($contenedor->packing);
            $data['color_estado_packing']   = self::getColorStatusFile($contenedor->packing);
            $data['estado_certificado']     = self::getNameStatusFile($contenedor->certificado);
            $data['color_estado_certificado']   = self::getColorStatusFile($contenedor->certificado);
            $data['estado_valija']              = self::getNameStatusFile($contenedor->valija);
            $data['color_estado_valija']        = self::getColorStatusFile($contenedor->valija);
            $data['estado_vl']                  = self::getNameStatusFile($contenedor->vl);
            $data['color_estado_vl']            = self::getColorStatusFile($contenedor->vl);
            $data_total[]                       = $data;
            unset($cajas_total); // vaciamos el arreglo de cajas
        }
       return $data_total;
    }


    public static function buscarContenedor($numero,$booking)
    {
        
        $contenedores = Contenedor::where('activo',1)->orderBy('fecha_proceso_inicio');   
        if(!empty($numero)){
            $contenedores = $contenedores->where('numero_contenedor','like','%'.$numero.'%');
        }
        if(!empty($booking)){
            $contenedores = $contenedores->where('booking','like','%'.$booking.'%');
        }
        $contenedores = $contenedores->get();
        $data_total = array();
        foreach ($contenedores as $contenedor) {
            $movimiento = $contenedor->movimientos->last();
            // recorremos para traer la información de las cajas que contiene
            $cajas_total = array();
            foreach ($contenedor->cajas as $value) {
                $caja['id']             = $value->id;
                $caja['marca']          = $value->marca;
                $caja['peso']           = $value->peso;
                $caja['cantidad_cajas'] = $value->pivot->cantidad;
                $cajas_total []         = $caja;
            }
            $data['cajas']                  = $cajas_total;
            $data['id_contenedor']          = $contenedor->id;
            $data['id_semana']              = $contenedor->id_semana;
            $data['semana']                 = $contenedor->semana->nombre;
            $data['id_cliente']             = $contenedor->id_cliente;
            $data['cliente']                = $contenedor->cliente->razon_social;
            $data['email_cliente']          = $contenedor->cliente->email;
            $data['referencia']             = $contenedor->referencia;
            $data['booking']                = $contenedor->booking;
            $data['numero_contenedor']      = $contenedor->numero_contenedor;
            $data['id_lineanaviera']        = $contenedor->id_lineanaviera;
            $data['linea_naviera']          = $contenedor->linea_naviera->nombre;
            $data['sigla_linea_naviera']    = $contenedor->linea_naviera->sigla;
            $data['nave']                   = $contenedor->nave;
            $data['viaje']                  = $contenedor->viaje;
            $data['id_puertodestino']       = $contenedor->id_puertodestino;
            $data['puerto_destino']         = $contenedor->puerto_destino->nombre;
            $data['id_operador']            = $contenedor->id_operador;
            $data['operador']               = $contenedor->operador->nombre;
            $data['dia_proceso_inicio']     = $contenedor->fecha_proceso_inicio;
            $data['dia_proceso_fin']        = $contenedor->fecha_proceso_fin;
            $data['dia_zarpe']              = $contenedor->fecha_zarpe;
            $data['dia_llegada']            = $contenedor->fecha_llegada;
            $data['peso_bruto']             = $contenedor->peso_bruto;
            $data['peso_neto']              = $contenedor->peso_neto;
            $data['id_estado']              = $movimiento->id_estado;
            $data['estado']                 = $movimiento->estado->nombre;
            $data['color_estado']           = $movimiento->estado->color;
            $data['estado_factura']         = self::getNameStatusFile($contenedor->factura);
            $data['color_estado_factura']   = self::getColorStatusFile($contenedor->factura);
            $data['estado_packing']         = self::getNameStatusFile($contenedor->packing);
            $data['color_estado_packing']   = self::getColorStatusFile($contenedor->packing);
            $data['estado_certificado']     = self::getNameStatusFile($contenedor->certificado);
            $data['color_estado_certificado']   = self::getColorStatusFile($contenedor->certificado);
            $data['estado_valija']              = self::getNameStatusFile($contenedor->valija);
            $data['color_estado_valija']        = self::getColorStatusFile($contenedor->valija);
            $data['estado_vl']                  = self::getNameStatusFile($contenedor->vl);
            $data['color_estado_vl']            = self::getColorStatusFile($contenedor->vl);
            $data_total[]                       = $data;
            unset($cajas_total); // vaciamos el arreglo de cajas
        }
       return $data_total;
    }

    public static function reporteMovimientoContenedor($id_anio,$id_semana = "")
    {
        // $contenedores = Contenedor::where('activo',1)->get();
        if ($id_semana != "") {
            $contenedores = Contenedor::where('activo',1)->where('id_semana',$id_semana)->get();   
        }else{
            $semanas = Semana::where('id_anio',$id_anio)->get();
            foreach ($semanas as $semana) {
                $containers[] = $semana->contenedor->where('activo',1);
            }
            $collection   = collect($containers);
            $collapsed    = $collection->collapse();
            $contenedores = $collapsed->all();
        }
        $anio = Anio::where('id',$id_anio)->select('anio')->first()->anio;
        $data_total = array();
        foreach ($contenedores as $contenedor) {
            foreach ($contenedor->movimientos as $value) {
                $data_movimientos['usuario_responsable']       = $value->usuario->persona->fullname;
                $data_movimientos['cargo_usuario_responsable'] = $value->usuario->persona->cargo->nombre;
                $data_movimientos['estado']                    = $value->estado->nombre;
                $data_movimientos['color_estado']              = $value->estado->color;
                $data_movimientos['fecha']                     = $value->fecha;
                $data_movimientos['hora']                      = $value->hora;
                $total_movimientos[] = $data_movimientos;
            }
            $data_contenedor['numero_contenedor']      = $contenedor->numero_contenedor;
            $data_contenedor['anio']                   = $anio;
            $data_contenedor['referencia_contenedor']  = $contenedor->referencia;
            $data_contenedor['semana']                 = $contenedor->semana->nombre;
            $data_contenedor['movimientos_contenedor'] = $total_movimientos;
            $data_total[] = $data_contenedor;
            unset($total_movimientos);
        }

        // Ordenar por semana de forma Descendente
        $collection   = collect($data_total);
        $ordenar      = $collection->sortByDesc('semana');
        $data_total   = $ordenar->values()->all();
        return $data_total;
    }

    // detalle de un solo contenedor
    public static function detalleContenedor($id_contenedor)
    {
        $contenedores = Contenedor::where('id',$id_contenedor)->where('activo',1)->get();

        $data_total = array();
        foreach ($contenedores as $contenedor) {
            $movimiento = $contenedor->movimientos->last();
            // recorremos para traer la información de las cajas que contiene
            $cajas_total = array();
            foreach ($contenedor->cajas as $value) {
                $caja['id']             = $value->id;
                $caja['marca']          = $value->marca;
                $caja['peso']           = $value->peso;
                $caja['cantidad_cajas'] = $value->pivot->cantidad;
                $cajas_total []         = $caja;
            }
            $data['cajas']             = $cajas_total;
            $data['semana']            = $contenedor->semana->nombre;
            $data['cliente']           = $contenedor->cliente->razon_social;
            $data['email_cliente']     = $contenedor->cliente->email;
            $data['referencia']        = $contenedor->referencia;
            $data['booking']           = $contenedor->booking;
            $data['numero_contenedor'] = $contenedor->numero_contenedor;
            $data['linea_naviera']     = $contenedor->linea_naviera->nombre;
            // $data['nave']              = $contenedor->nave->nombre;
            $data['nave']              = $contenedor->nave;
            $data['viaje']             = $contenedor->viaje;
            $data['puerto_destino']    = $contenedor->puerto_destino->nombre;
            $data['operador']          = $contenedor->operador->nombre;
            $data['dia_proceso_inicio']= $contenedor->fecha_proceso_inicio;
            $data['dia_proceso_fin']   = $contenedor->fecha_proceso_fin;
            $data['dia_zarpe']         = $contenedor->fecha_zarpe;
            $data['dia_llegada']       = $contenedor->fecha_llegada;
            $data['peso_bruto']        = $contenedor->peso_bruto;
            $data['peso_neto']         = $contenedor->peso_neto;
            $data['valija']            = ($contenedor->valija == 1) ? 'Emitida' : 'Pendiente';
            $data_total[]              = $data;
            unset($cajas_total); // vaciamos el arreglo de cajas
        }
       return $data_total;
    }

    // movimientos de un solo contenedor
    public static function movimientoContenedor($id_contenedor)
    {
        $contenedores = Contenedor::where('id',$id_contenedor)->where('activo',1)->get();
        $data_total = array();
        foreach ($contenedores as $contenedor) {
            foreach ($contenedor->movimientos as $value) {
                $data_movimientos['usuario_responsable']       = $value->usuario->persona->fullname;
                $data_movimientos['cargo_usuario_responsable'] = $value->usuario->persona->cargo->nombre;
                $data_movimientos['estado']                    = $value->estado->nombre;
                $data_movimientos['color_estado']              = $value->estado->color;
                $data_movimientos['fecha']                     = $value->fecha;
                $data_movimientos['hora']                      = $value->hora;
                $total_movimientos[] = $data_movimientos;
            }
            $data_contenedor['movimientos_contenedor'] = $total_movimientos;
            unset($total_movimientos);
        }
        return $data_contenedor;
    }

    // evitar que un contenedor vuelva a un estado anterior
    public static function validarMovimiento($id_contenedor,$id_estado)
    {
        $movimientos = Movimiento::where('id_contenedor',$id_contenedor)->where('id_estado',$id_estado)->first();
        return ($movimientos) ? true : false ;
    }


    // ----------------------- Listar los contenedores de un cliente segun la semana (Para Valijas) (SuperAdmin)--------------
    public static function getContenedoresClienteAndSemana($id_cliente,$id_semana,$tipo_archivo)
    {
        $contenedores = Contenedor::where('id_cliente',$id_cliente)->where('id_semana',$id_semana)->where($tipo_archivo,1)->orderBy('fecha_proceso_inicio')->where('activo',1)->get();
        $data_total   = self::procesarDataContenedores($contenedores);
        return $data_total;
    }
    // ----------------------- Fin Listar los contenedores de un cliente segun la semana (Para Valijas) (SuperAdmin)------------

    // ----------------------- Listar los contenedores de un cliente segun la semana y el operador(Para Valijas) (Operador)------------
    public static function getContenedoresClienteAndSemanaAndOperador($id_cliente,$id_semana,$id_operador)
    {
        $contenedores = Contenedor::where('id_cliente',$id_cliente)->where('id_semana',$id_semana)->where('id_operador',$id_operador)->where('valija',1)->orderBy('fecha_proceso_inicio')->where('activo',1)->get();
        $data_total   = self::procesarDataContenedores($contenedores);
        return $data_total;
    }
    // ----------------------- Fin Listar los contenedores de un cliente segun la semana y el operador(Para Valijas) (Operador)------------

    public static function procesarDataContenedores($contenedores)
    {
        $data_total   = array();
        foreach ($contenedores as $value) {
                $data['id_contenedor']     = $value->id;
                $data['id_semana']         = $value->id_semana;
                $data['semana']            = $value->semana->nombre;
                $data['id_cliente']        = $value->id_cliente;
                $data['cliente']           = $value->cliente->razon_social;
                $data['email_cliente']     = $value->cliente->email;
                $data['referencia']        = $value->referencia;
                $data['booking']           = $value->booking;
                $data['numero_contenedor'] = $value->numero_contenedor;
                $data['id_lineanaviera']   = $value->id_lineanaviera;
                $data['linea_naviera']     = $value->linea_naviera->nombre;
                $data['sigla_linea_naviera']= $value->linea_naviera->sigla;
                $data['nave']              = $value->nave;
                $data['viaje']             = $value->viaje;
                $data['id_puertodestino']  = $value->id_puertodestino;
                $data['puerto_destino']    = $value->puerto_destino->nombre;
                $data['id_operador']       = $value->id_operador;
                $data['operador']          = $value->operador->nombre;
                $data['dia_proceso_inicio']= $value->fecha_proceso_inicio;
                $data['dia_proceso_fin']   = $value->fecha_proceso_fin;
                $data['dia_zarpe']         = $value->fecha_zarpe;
                $data['dia_llegada']       = $value->fecha_llegada;
                $data['peso_bruto']        = $value->peso_bruto;
                $data['peso_neto']         = $value->peso_neto;
                $data_total[]              = $data;
        }
        return $data_total;
    }
}