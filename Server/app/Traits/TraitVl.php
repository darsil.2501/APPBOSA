<?php

namespace App\Traits;
use App\Vl;
use DB;

trait TraitVl
{
	public static function getVls($id_semana)
	{
		$data = Vl::where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        $vls  = array();
        foreach ($data as $key => $value) {
            $vls[] = array(
                'id'                  => $value->id,
                'id_cliente'          => $value->cliente->id,
                'cliente'             => $value->cliente->razon_social,
                'id_semana'           => $value->semana->id,
                'semana'              => $value->semana->nombre,
                // 'id_operador' => $value->id_operador,
                // 'operador'    => ($value->id_operador) ? self::getNameOperador($value->id_operador) : null ,
                'nombre'              => $value->nombre,
                'fecha'               => $value->fecha,
                'hora'                => $value->hora,
                'url'                 => $value->url,
                'estado_archivo'      => $value->estado_archivo->nombre,
                'color_estado_archivo'=> $value->estado_archivo->color,
            );
        }
        return $vls;
	}

    public static function changeStatusVl($id_vl)
    {
        try{
            DB::beginTransaction();
                $vl = Vl::find($id_vl);
                $vl->fill(['id_estado_archivo'=>DB::raw(3)])->save();
            DB::commit();   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al cambiar de estado el archivo.'.$ex->getMessage()]);
        }
    }
}