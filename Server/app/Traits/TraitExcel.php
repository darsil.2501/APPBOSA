<?php

namespace App\Traits;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Alignment;
use PHPExcel_Worksheet_Drawing;
use App\Traits\TraitFuncionesExcel;
use App\Traits\TraitEnfunde;

trait TraitExcel
{
	use TraitFuncionesExcel,TraitEnfunde;

	public static function excelContenedores($data)
	{
		Excel::create('Contenedores',function($excel) use($data){
			$excel->sheet('Contenedores',function($sheet) use($data){
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/logo_reportes.png'));
        		$objDrawing->setCoordinates('C2');
        		$objDrawing->setWorksheet($sheet);

        		$sheet->cells('A1:X7', function($cells) {$cells->setBackground('#ffffff');});

        		$sheet->mergeCells('J3:O3');
        		$sheet->setCellvalue('J3','PROGRAMA DE CORTE Y EXPORTACIÓN DE APPBOSA');
        		$sheet->cells('J3:O3', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('J3:O3', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'12','bold'=>true));});

        		$sheet->setCellvalue('J4','SEMANA:');
        		$sheet->setCellvalue('J5','ACTUALIZADO:');
        		$sheet->setCellvalue('J6','CONTENEDORES:');
        		$sheet->cells('J4:J6', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));});

        		$sheet->mergeCells('K4:O4');
        		$sheet->setCellvalue('K4',$data[0]['semana']);
        		$sheet->mergeCells('K5:O5');
        		$sheet->setCellvalue('K5',date('d/m/Y'));
        		$sheet->mergeCells('K6:O6');
        		$sheet->setCellvalue('K6',count($data));

        		$sheet->setCellvalue('X1','SISAPPBOSA V1.0');
        		$sheet->cells('X1', function($cells) {
        			$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>true));
        		});

				// Cabeceras
				$sheet->setCellvalue('A8','ITEM');
				$sheet->setCellvalue('B8','W');
				$sheet->setCellvalue('C8','FECHA INICIO DE PROCESO');
				$sheet->setCellvalue('D8','FECHA FIN DE PROCESO');
				$sheet->setCellvalue('E8','OPERADOR LOGÍSTICO');
				$sheet->setCellvalue('F8','LÍNEA');
				$sheet->setCellvalue('G8','NAVE');
				$sheet->setCellvalue('H8','BOOKING');
				$sheet->setCellvalue('I8','CLIENTE');
				$sheet->setCellvalue('J8','TIPO DE CAJAS');
				$sheet->setCellvalue('K8','CANTIDAD CAJAS');
				$sheet->setCellvalue('L8','CONTENEDOR');
				$sheet->setCellvalue('M8','ORDEN / REFERENCIA');
				$sheet->setCellvalue('N8','DESTINO');
				$sheet->setCellvalue('O8','PESO NETO (Kg)');
				$sheet->setCellvalue('P8','PESO BRUTO (Kg)');
				$sheet->setCellvalue('Q8','FECHA DE ZARPE');
				$sheet->setCellvalue('R8','FECHA DE LLEGADA');
				$sheet->setCellvalue('S8','ESTADO CONTENEDOR');
				$sheet->setCellvalue('T8','FACTURA');
				$sheet->setCellvalue('U8','PACKING');
				$sheet->setCellvalue('V8','CERTIFICADO');
				$sheet->setCellvalue('W8','VALIJA');
				$sheet->setCellvalue('X8','BLs');
				$sheet->getStyle('A8:X8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->getStyle('A8:X8')->getAlignment()->setWrapText(true);

				$sheet->cells('A8:X8', function($cells) {
					$cells->setAlignment('center');
					$cells->setBackground('#2E4053');
					$cells->setFontColor('#ffffff');
					$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>true));
				});

				$sheet->setHeight(8,39);
				$sheet->setBorder('A8:X8','thin');
				$sheet->setAutoFilter('A8:X8');
				$sheet->setFreeze('A9');
				$sheet->setWidth(array(
				    'A'=>5,'B'=>6,'C'=>12,'D'=>12,'E'=>23,'F'=>20,'G'=>29,'H'=>12,'I'=>12,'J'=>20,
				    'K'=>10,'L'=>12,'M'=>12,'N'=>11,'O'=>8,'P'=>8,'Q'=>10,'R'=>10,'S'=>13,'T'=>13,
				    'U'=>13,'V'=>13,'W'=>13,'X'=>13
				));

				$fila = 9;
				$item = 1;
				$total_cajas = 0;
				foreach ($data as $value) {
					$can = count($value['cajas']);
					$total = $fila + ($can - 1);
					$total = ($can > 0) ? $fila + ($can - 1) : $total = $fila + $can;
					
					$sheet->mergeCells('A'.$fila.':A'.$total);
					$sheet->setCellvalue('A'.$fila,$item);

					$sheet->mergeCells('B'.$fila.':B'.$total);
					$sheet->setCellvalue('B'.$fila,$value['semana']);

					if($value['dia_proceso_inicio'] != null){
						$fecha_proceso_inicio = date_create($value['dia_proceso_inicio']);
						$sheet->mergeCells('C'.$fila.':C'.$total);
						$sheet->setCellvalue('C'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('C'.$fila.':C'.$total);
						$sheet->setCellvalue('C'.$fila,$value['dia_proceso_inicio']);
					}

					if($value['dia_proceso_fin'] != null){
						$fecha_proceso_fin = date_create($value['dia_proceso_fin']);
						$sheet->mergeCells('D'.$fila.':D'.$total);
						$sheet->setCellvalue('D'.$fila,date_format($fecha_proceso_fin,'d-m-Y'));
					}else{
						$sheet->mergeCells('D'.$fila.':D'.$total);
						$sheet->setCellvalue('D'.$fila,$value['dia_proceso_fin']);
					}

					$sheet->mergeCells('E'.$fila.':E'.$total);
					$sheet->setCellvalue('E'.$fila,$value['operador']);

					$sheet->mergeCells('F'.$fila.':F'.$total);
					$sheet->setCellvalue('F'.$fila,$value['linea_naviera']);

					$sheet->mergeCells('G'.$fila.':G'.$total);
					$sheet->setCellvalue('G'.$fila,mb_strtoupper($value['sigla_linea_naviera'].'-'.$value['nave'].' V.'.$value['viaje']));

					$sheet->mergeCells('H'.$fila.':H'.$total);
					$sheet->setCellvalue('H'.$fila,$value['booking']);

					$sheet->mergeCells('I'.$fila.':I'.$total);
					$sheet->setCellvalue('I'.$fila,$value['cliente']);

					
					$fila_cajas = $fila;
					foreach ($value['cajas'] as $caja) {
						$sheet->setCellvalue('J'.$fila_cajas,$caja['marca']);
						$sheet->setCellvalue('K'.$fila_cajas,$caja['cantidad_cajas']);
						$total_cajas = $total_cajas + $caja['cantidad_cajas'];
						$fila_cajas++;
					}
					$sheet->mergeCells('L'.$fila.':L'.$total);
					$sheet->setCellvalue('L'.$fila,$value['numero_contenedor']);

					$sheet->mergeCells('M'.$fila.':M'.$total);
					$sheet->setCellvalue('M'.$fila,$value['referencia']);

					$sheet->mergeCells('N'.$fila.':N'.$total);
					$sheet->setCellvalue('N'.$fila,$value['puerto_destino']);

					$sheet->mergeCells('O'.$fila.':O'.$total);
					$sheet->setCellvalue('O'.$fila,$value['peso_neto']);

					$sheet->mergeCells('P'.$fila.':P'.$total);
					$sheet->setCellvalue('P'.$fila,$value['peso_bruto']);

					if($value['dia_zarpe'] != null){
						$fecha_zarpe = date_create($value['dia_zarpe']);
						$sheet->mergeCells('Q'.$fila.':Q'.$total);
						$sheet->setCellvalue('Q'.$fila,date_format($fecha_zarpe,'d-m-Y'));	
					}else{
						$sheet->mergeCells('Q'.$fila.':Q'.$total);
						$sheet->setCellvalue('Q'.$fila,$value['dia_zarpe']);	
					}
					
					if($value['dia_llegada'] != null){
						$fecha_llegada = date_create($value['dia_llegada']);
						$sheet->mergeCells('R'.$fila.':R'.$total);
						$sheet->setCellvalue('R'.$fila,date_format($fecha_llegada,'d-m-Y'));	
					}else{
						$sheet->mergeCells('R'.$fila.':R'.$total);
						$sheet->setCellvalue('R'.$fila,$value['dia_llegada']);	
					}
					

					$sheet->mergeCells('S'.$fila.':S'.$total);
					$sheet->setCellvalue('S'.$fila,$value['estado']);
					$sheet->cells('S'.$fila.':S'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado']);
	        			$cells->setFontColor('#ffffff');
        			});

					// verificar si el contenedor esta en la nave, para resaltar la fila
        			if (self::contenedorEnNave($value['estado'])){
        				$sheet->cells('A'.$fila.':R'.$total, function($cells){
	        				$cells->setBackground('#92D0FC');
        				});
        			}

					
        			$sheet->mergeCells('T'.$fila.':T'.$total);
					$sheet->setCellvalue('T'.$fila,$value['estado_factura']);
					$sheet->cells('T'.$fila.':T'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_factura']);
	        			$cells->setFontColor('#ffffff');
        			});

					
					$sheet->mergeCells('U'.$fila.':U'.$total);
					$sheet->setCellvalue('U'.$fila,$value['estado_packing']);
					$sheet->cells('U'.$fila.':U'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_packing']);
	        			$cells->setFontColor('#ffffff');
        			});

					$sheet->mergeCells('V'.$fila.':V'.$total);
					$sheet->setCellvalue('V'.$fila,$value['estado_certificado']);
					$sheet->cells('V'.$fila.':V'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_certificado']);
	        			$cells->setFontColor('#ffffff');
        			});

					$sheet->mergeCells('W'.$fila.':W'.$total);
					$sheet->setCellvalue('W'.$fila,$value['estado_valija']);
					$sheet->cells('W'.$fila.':W'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_valija']);
	        			$cells->setFontColor('#ffffff');
        			});

        			$sheet->mergeCells('X'.$fila.':X'.$total);
					$sheet->setCellvalue('X'.$fila,$value['estado_vl']);
					$sheet->cells('X'.$fila.':X'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_vl']);
	        			$cells->setFontColor('#ffffff');
        			});

					$fila = $total + 1;
					$item++;
				}

				// Para la cantidad de cajas de esa semana
				// $sheet->mergeCells('A'.$fila.':C'.$fila);
				$sheet->setCellvalue('J'.$fila,'TOTAL CAJAS:');
				$sheet->setCellvalue('K'.$fila,$total_cajas);
				$sheet->cells('J'.$fila.':K'.$fila, function($cells) {
        			$cells->setAlignment('center');
        			$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
        		});
				// Ajustar y alinear Texto
				$sheet->getStyle('A9:X'.$fila)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->cells('A9:X'.$fila, function($cells) {$cells->setAlignment('center');});

				// para todos los bordes
				$sheet->cells('A9:X'.($fila-1), function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>false));});
				$sheet->setBorder('A9:X'.($fila-1),'thin');
				
			});
		})->download('xls');
	}

	public static function excelContenedoresPorClientes($data)
	{
		Excel::create('Contenedores',function($excel) use($data){
			$excel->sheet('Contenedores',function($sheet) use($data){
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/logo_reportes.png'));
        		$objDrawing->setCoordinates('C2');
        		$objDrawing->setWorksheet($sheet);
        		$sheet->cells('A1:X7', function($cells) {$cells->setBackground('#ffffff');});

        		$sheet->mergeCells('J1:O1');
        		$sheet->setCellvalue('J1','PROGRAMA DE CORTE Y EXPORTACIÓN DE APPBOSA');
        		$sheet->cells('J1:O1', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('J1:O1', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'14','bold'=>true));});

        		$sheet->mergeCells('J2:O2');
        		$sheet->setCellvalue('J2','REPORTE DE CONTENEDORES POR CLIENTE');
        		$sheet->cells('J2:O2', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('J2:O2', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>false));});

        		$sheet->setCellvalue('J4','CLIENTE:');
        		$sheet->setCellvalue('J5','CANTIDAD CONTENEDORES:');
        		$sheet->setCellvalue('J6','FECHA:');
        		$sheet->cells('J4:J6', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));});

        		$sheet->mergeCells('M4:O4');
        		$sheet->setCellvalue('M4',$data[0]['cliente']);
        		$sheet->mergeCells('M5:O5');
        		$sheet->setCellvalue('M5',count($data));
        		$sheet->mergeCells('M6:O6');
        		$sheet->setCellvalue('M6',date('d/m/Y'));
        		$sheet->cells('M4:O6', function($cells) {$cells->setAlignment('center');});

        		$sheet->setCellvalue('X1','SISAPPBOSA V1.0');
        		$sheet->cells('X1', function($cells) {
        			$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>true));
        		});

				// Cabeceras
				$sheet->setCellvalue('A8','ITEM');
				$sheet->setCellvalue('B8','W');
				$sheet->setCellvalue('C8','FECHA INICIO DE PROCESO');
				$sheet->setCellvalue('D8','FECHA FIN DE PROCESO');
				$sheet->setCellvalue('E8','OPERADOR LOGÍSTICO');
				$sheet->setCellvalue('F8','LÍNEA');
				$sheet->setCellvalue('G8','NAVE');
				$sheet->setCellvalue('H8','BOOKING');
				$sheet->setCellvalue('I8','CLIENTE');
				$sheet->setCellvalue('J8','TIPO DE CAJAS');
				$sheet->setCellvalue('K8','CANTIDAD CAJAS');
				$sheet->setCellvalue('L8','CONTENEDOR');
				$sheet->setCellvalue('M8','ORDEN / REFERENCIA');
				$sheet->setCellvalue('N8','DESTINO');
				$sheet->setCellvalue('O8','PESO NETO (Kg)');
				$sheet->setCellvalue('P8','PESO BRUTO (Kg)');
				$sheet->setCellvalue('Q8','FECHA DE ZARPE');
				$sheet->setCellvalue('R8','FECHA DE LLEGADA');
				$sheet->setCellvalue('S8','ESTADO CONTENEDOR');
				$sheet->setCellvalue('T8','FACTURA');
				$sheet->setCellvalue('U8','PACKING');
				$sheet->setCellvalue('V8','CERTIFICADO');
				$sheet->setCellvalue('W8','VALIJA');
				$sheet->setCellvalue('X8','BLs');
				$sheet->getStyle('A8:X8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->getStyle('A8:X8')->getAlignment()->setWrapText(true);

				$sheet->cells('A8:X8', function($cells) {
					$cells->setAlignment('center');
					$cells->setBackground('#2E4053');
					$cells->setFontColor('#ffffff');
					$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>true));
				});

				$sheet->setHeight(8,39);
				$sheet->setBorder('A8:X8','thin');
				$sheet->setAutoFilter('A8:X8');
				$sheet->setFreeze('A9');
				$sheet->setWidth(array(
				    'A'=>5,'B'=>6,'C'=>12,'D'=>12,'E'=>23,'F'=>20,'G'=>29,'H'=>12,'I'=>12,'J'=>20,
				    'K'=>10,'L'=>12,'M'=>12,'N'=>11,'O'=>8,'P'=>8,'Q'=>10,'R'=>10,'S'=>13,'T'=>13,
				    'U'=>13,'V'=>13,'W'=>13,'X'=>13,
				));

				$fila = 9;
				$item = 1;
				$total_cajas = 0;
				foreach ($data as $value) {
					$can = count($value['cajas']);
					$total = $fila + ($can - 1);
					$total = ($can > 0) ? $fila + ($can - 1) : $total = $fila + $can;
					
					$sheet->mergeCells('A'.$fila.':A'.$total);
					$sheet->setCellvalue('A'.$fila,$item);

					$sheet->mergeCells('B'.$fila.':B'.$total);
					$sheet->setCellvalue('B'.$fila,$value['semana']);

					if($value['dia_proceso_inicio'] != null){
						$fecha_proceso_inicio = date_create($value['dia_proceso_inicio']);
						$sheet->mergeCells('C'.$fila.':C'.$total);
						$sheet->setCellvalue('C'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('C'.$fila.':C'.$total);
						$sheet->setCellvalue('C'.$fila,$value['dia_proceso_inicio']);
					}

					// $sheet->mergeCells('C'.$fila.':C'.$total);
					// $sheet->setCellvalue('C'.$fila,$value['dia_proceso_inicio']);

					if($value['dia_proceso_fin'] != null){
						$fecha_proceso_inicio = date_create($value['dia_proceso_fin']);
						$sheet->mergeCells('D'.$fila.':D'.$total);
						$sheet->setCellvalue('D'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('D'.$fila.':D'.$total);
						$sheet->setCellvalue('D'.$fila,$value['dia_proceso_fin']);
					}

					// $sheet->mergeCells('D'.$fila.':D'.$total);
					// $sheet->setCellvalue('D'.$fila,$value['dia_proceso_fin']);

					$sheet->mergeCells('E'.$fila.':E'.$total);
					$sheet->setCellvalue('E'.$fila,$value['operador']);

					$sheet->mergeCells('F'.$fila.':F'.$total);
					$sheet->setCellvalue('F'.$fila,$value['linea_naviera']);

					$sheet->mergeCells('G'.$fila.':G'.$total);
					$sheet->setCellvalue('G'.$fila,mb_strtoupper($value['sigla_linea_naviera'].'-'.$value['nave'].' V.'.$value['viaje']));

					$sheet->mergeCells('H'.$fila.':H'.$total);
					$sheet->setCellvalue('H'.$fila,$value['booking']);

					$sheet->mergeCells('I'.$fila.':I'.$total);
					$sheet->setCellvalue('I'.$fila,$value['cliente']);

					
					$fila_cajas = $fila;
					foreach ($value['cajas'] as $caja) {
						$sheet->setCellvalue('J'.$fila_cajas,$caja['marca']);
						$sheet->setCellvalue('K'.$fila_cajas,$caja['cantidad_cajas']);
						$total_cajas = $total_cajas + $caja['cantidad_cajas'];
						$fila_cajas++;
					}
					$sheet->mergeCells('L'.$fila.':L'.$total);
					$sheet->setCellvalue('L'.$fila,$value['numero_contenedor']);

					$sheet->mergeCells('M'.$fila.':M'.$total);
					$sheet->setCellvalue('M'.$fila,$value['referencia']);

					$sheet->mergeCells('N'.$fila.':N'.$total);
					$sheet->setCellvalue('N'.$fila,$value['puerto_destino']);

					$sheet->mergeCells('O'.$fila.':O'.$total);
					$sheet->setCellvalue('O'.$fila,$value['peso_neto']);

					$sheet->mergeCells('P'.$fila.':P'.$total);
					$sheet->setCellvalue('P'.$fila,$value['peso_bruto']);

					if($value['dia_zarpe'] != null){
						$fecha_proceso_inicio = date_create($value['dia_zarpe']);
						$sheet->mergeCells('Q'.$fila.':Q'.$total);
						$sheet->setCellvalue('Q'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('Q'.$fila.':Q'.$total);
						$sheet->setCellvalue('Q'.$fila,$value['dia_zarpe']);
					}

					if($value['dia_llegada'] != null){
						$fecha_proceso_inicio = date_create($value['dia_llegada']);
						$sheet->mergeCells('R'.$fila.':R'.$total);
						$sheet->setCellvalue('R'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('R'.$fila.':R'.$total);
						$sheet->setCellvalue('R'.$fila,$value['dia_llegada']);
					}

					$sheet->mergeCells('S'.$fila.':S'.$total);
					$sheet->setCellvalue('S'.$fila,$value['estado']);
					$sheet->cells('S'.$fila.':S'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado']);
	        			$cells->setFontColor('#ffffff');
        			});

        			// verificar si el contenedor esta en la nave, para resaltar la fila
        			if (self::contenedorEnNave($value['estado'])){
        				$sheet->cells('A'.$fila.':R'.$total, function($cells){
	        				$cells->setBackground('#92D0FC');
        				});
        			}

        			$sheet->mergeCells('T'.$fila.':T'.$total);
					$sheet->setCellvalue('T'.$fila,$value['estado_factura']);
					$sheet->cells('T'.$fila.':T'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_factura']);
	        			$cells->setFontColor('#ffffff');
        			});

					
					$sheet->mergeCells('U'.$fila.':U'.$total);
					$sheet->setCellvalue('U'.$fila,$value['estado_packing']);
					$sheet->cells('U'.$fila.':U'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_packing']);
	        			$cells->setFontColor('#ffffff');
        			});

					$sheet->mergeCells('V'.$fila.':V'.$total);
					$sheet->setCellvalue('V'.$fila,$value['estado_certificado']);
					$sheet->cells('V'.$fila.':V'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_certificado']);
	        			$cells->setFontColor('#ffffff');
        			});

					$sheet->mergeCells('W'.$fila.':W'.$total);
					$sheet->setCellvalue('W'.$fila,$value['estado_valija']);
					$sheet->cells('W'.$fila.':W'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_valija']);
	        			$cells->setFontColor('#ffffff');
        			});

        			$sheet->mergeCells('X'.$fila.':X'.$total);
					$sheet->setCellvalue('X'.$fila,$value['estado_vl']);
					$sheet->cells('X'.$fila.':X'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_vl']);
	        			$cells->setFontColor('#ffffff');
        			});

					$fila = $total + 1;
					$item++;
				}

				// Para la cantidad de cajas de esa semana
				// $sheet->mergeCells('A'.$fila.':C'.$fila);
				$sheet->setCellvalue('J'.$fila,'TOTAL CAJAS:');
				$sheet->setCellvalue('K'.$fila,$total_cajas);
				$sheet->cells('J'.$fila.':K'.$fila, function($cells) {
        			$cells->setAlignment('center');
        			$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
        		});
				// Ajustar y alinear Texto
				$sheet->getStyle('A9:X'.$fila)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->cells('A9:X'.$fila, function($cells) {$cells->setAlignment('center');});

				// para todos los bordes
				$sheet->cells('A9:X'.($fila-1), function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>false));});
				$sheet->setBorder('A9:X'.($fila-1),'thin');
				
			});
		})->download('xls');
	}

	public static function excelContenedoresPorOperadores($data)
	{
		Excel::create('Contenedores',function($excel) use($data){
			$excel->sheet('Contenedores',function($sheet) use($data){
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/logo_reportes.png'));
        		$objDrawing->setCoordinates('C2');
        		$objDrawing->setWorksheet($sheet);
        		$sheet->cells('A1:X7', function($cells) {$cells->setBackground('#ffffff');});

        		$sheet->mergeCells('J1:O1');
        		$sheet->setCellvalue('J1','PROGRAMA DE CORTE Y EXPORTACIÓN DE APPBOSA');
        		$sheet->cells('J1:O1', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('J1:O1', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'14','bold'=>true));});

        		$sheet->mergeCells('J2:O2');
        		$sheet->setCellvalue('J2','REPORTE DE CONTENEDORES POR CLIENTE');
        		$sheet->cells('J2:O2', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('J2:O2', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>false));});

        		$sheet->setCellvalue('J4','OPERADOR:');
        		$sheet->setCellvalue('J5','CANTIDAD CONTENEDORES:');
        		$sheet->setCellvalue('J6','FECHA:');
        		$sheet->cells('J4:J6', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));});

        		$sheet->mergeCells('M4:O4');
        		$sheet->setCellvalue('M4',$data[0]['operador']);
        		$sheet->mergeCells('M5:O5');
        		$sheet->setCellvalue('M5',count($data));
        		$sheet->mergeCells('M6:O6');
        		$sheet->setCellvalue('M6',date('d/m/Y'));
        		$sheet->cells('M4:O6', function($cells) {$cells->setAlignment('center');});

        		$sheet->setCellvalue('X1','SISAPPBOSA V1.0');
        		$sheet->cells('X1', function($cells) {
        			$cells->setAlignment('right');
        			$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>true));
        		});

				// Cabeceras
				$sheet->setCellvalue('A8','ITEM');
				$sheet->setCellvalue('B8','W');
				$sheet->setCellvalue('C8','FECHA INICIO DE PROCESO');
				$sheet->setCellvalue('D8','FECHA FIN DE PROCESO');
				$sheet->setCellvalue('E8','OPERADOR LOGÍSTICO');
				$sheet->setCellvalue('F8','LÍNEA');
				$sheet->setCellvalue('G8','NAVE');
				$sheet->setCellvalue('H8','BOOKING');
				$sheet->setCellvalue('I8','CLIENTE');
				$sheet->setCellvalue('J8','TIPO DE CAJAS');
				$sheet->setCellvalue('K8','CANTIDAD CAJAS');
				$sheet->setCellvalue('L8','CONTENEDOR');
				$sheet->setCellvalue('M8','ORDEN / REFERENCIA');
				$sheet->setCellvalue('N8','DESTINO');
				$sheet->setCellvalue('O8','PESO NETO (Kg)');
				$sheet->setCellvalue('P8','PESO BRUTO (Kg)');
				$sheet->setCellvalue('Q8','FECHA DE ZARPE');
				$sheet->setCellvalue('R8','FECHA DE LLEGADA');
				$sheet->setCellvalue('S8','ESTADO CONTENEDOR');
				$sheet->setCellvalue('T8','FACTURA');
				$sheet->setCellvalue('U8','PACKING');
				$sheet->setCellvalue('V8','CERTIFICADO');
				$sheet->setCellvalue('W8','VALIJA');
				$sheet->setCellvalue('X8','BLs');
				$sheet->getStyle('A8:X8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->getStyle('A8:X8')->getAlignment()->setWrapText(true);

				$sheet->cells('A8:X8', function($cells) {
					$cells->setAlignment('center');
					$cells->setBackground('#2E4053');
					$cells->setFontColor('#ffffff');
					$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>true));
				});

				$sheet->setHeight(8,39);
				$sheet->setBorder('A8:X8','thin');
				$sheet->setAutoFilter('A8:X8');
				$sheet->setFreeze('A9');
				$sheet->setWidth(array(
				    'A'=>5,'B'=>6,'C'=>12,'D'=>12,'E'=>23,'F'=>20,'G'=>29,'H'=>12,'I'=>12,'J'=>20,
				    'K'=>10,'L'=>12,'M'=>12,'N'=>11,'O'=>8,'P'=>8,'Q'=>10,'R'=>10,'S'=>13,'T'=>13,
				    'U'=>13,'V'=>13,'W'=>13,'X'=>13,
				));

				$fila = 9;
				$item = 1;
				$total_cajas = 0;
				foreach ($data as $value) {
					$can = count($value['cajas']);
					$total = $fila + ($can - 1);
					$total = ($can > 0) ? $fila + ($can - 1) : $total = $fila + $can;
					
					$sheet->mergeCells('A'.$fila.':A'.$total);
					$sheet->setCellvalue('A'.$fila,$item);

					$sheet->mergeCells('B'.$fila.':B'.$total);
					$sheet->setCellvalue('B'.$fila,$value['semana']);

					if($value['dia_proceso_inicio'] != null){
						$fecha_proceso_inicio = date_create($value['dia_proceso_inicio']);
						$sheet->mergeCells('C'.$fila.':C'.$total);
						$sheet->setCellvalue('C'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('C'.$fila.':C'.$total);
						$sheet->setCellvalue('C'.$fila,$value['dia_proceso_inicio']);
					}

					// $sheet->mergeCells('C'.$fila.':C'.$total);
					// $sheet->setCellvalue('C'.$fila,$value['dia_proceso_inicio']);

					if($value['dia_proceso_fin'] != null){
						$fecha_proceso_inicio = date_create($value['dia_proceso_fin']);
						$sheet->mergeCells('D'.$fila.':D'.$total);
						$sheet->setCellvalue('D'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('D'.$fila.':D'.$total);
						$sheet->setCellvalue('D'.$fila,$value['dia_proceso_fin']);
					}

					// $sheet->mergeCells('D'.$fila.':D'.$total);
					// $sheet->setCellvalue('D'.$fila,$value['dia_proceso_fin']);

					$sheet->mergeCells('E'.$fila.':E'.$total);
					$sheet->setCellvalue('E'.$fila,$value['operador']);

					$sheet->mergeCells('F'.$fila.':F'.$total);
					$sheet->setCellvalue('F'.$fila,$value['linea_naviera']);

					$sheet->mergeCells('G'.$fila.':G'.$total);
					$sheet->setCellvalue('G'.$fila,mb_strtoupper($value['sigla_linea_naviera'].'-'.$value['nave'].' V.'.$value['viaje']));

					$sheet->mergeCells('H'.$fila.':H'.$total);
					$sheet->setCellvalue('H'.$fila,$value['booking']);

					$sheet->mergeCells('I'.$fila.':I'.$total);
					$sheet->setCellvalue('I'.$fila,$value['cliente']);

					
					$fila_cajas = $fila;
					foreach ($value['cajas'] as $caja) {
						$sheet->setCellvalue('J'.$fila_cajas,$caja['marca']);
						$sheet->setCellvalue('K'.$fila_cajas,$caja['cantidad_cajas']);
						$total_cajas = $total_cajas + $caja['cantidad_cajas'];
						$fila_cajas++;
					}
					$sheet->mergeCells('L'.$fila.':L'.$total);
					$sheet->setCellvalue('L'.$fila,$value['numero_contenedor']);

					$sheet->mergeCells('M'.$fila.':M'.$total);
					$sheet->setCellvalue('M'.$fila,$value['referencia']);

					$sheet->mergeCells('N'.$fila.':N'.$total);
					$sheet->setCellvalue('N'.$fila,$value['puerto_destino']);

					$sheet->mergeCells('O'.$fila.':O'.$total);
					$sheet->setCellvalue('O'.$fila,$value['peso_neto']);

					$sheet->mergeCells('P'.$fila.':P'.$total);
					$sheet->setCellvalue('P'.$fila,$value['peso_bruto']);

					if($value['dia_zarpe'] != null){
						$fecha_proceso_inicio = date_create($value['dia_zarpe']);
						$sheet->mergeCells('Q'.$fila.':Q'.$total);
						$sheet->setCellvalue('Q'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('Q'.$fila.':Q'.$total);
						$sheet->setCellvalue('Q'.$fila,$value['dia_zarpe']);
					}

					if($value['dia_llegada'] != null){
						$fecha_proceso_inicio = date_create($value['dia_llegada']);
						$sheet->mergeCells('R'.$fila.':R'.$total);
						$sheet->setCellvalue('R'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('R'.$fila.':R'.$total);
						$sheet->setCellvalue('R'.$fila,$value['dia_llegada']);
					}

					$sheet->mergeCells('S'.$fila.':S'.$total);
					$sheet->setCellvalue('S'.$fila,$value['estado']);
					$sheet->cells('S'.$fila.':S'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado']);
	        			$cells->setFontColor('#ffffff');
        			});

        			// verificar si el contenedor esta en la nave, para resaltar la fila
        			if (self::contenedorEnNave($value['estado'])){
        				$sheet->cells('A'.$fila.':R'.$total, function($cells){
	        				$cells->setBackground('#92D0FC');
        				});
        			}

        			$sheet->mergeCells('T'.$fila.':T'.$total);
					$sheet->setCellvalue('T'.$fila,$value['estado_factura']);
					$sheet->cells('T'.$fila.':T'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_factura']);
	        			$cells->setFontColor('#ffffff');
        			});

					
					$sheet->mergeCells('U'.$fila.':U'.$total);
					$sheet->setCellvalue('U'.$fila,$value['estado_packing']);
					$sheet->cells('U'.$fila.':U'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_packing']);
	        			$cells->setFontColor('#ffffff');
        			});

					$sheet->mergeCells('V'.$fila.':V'.$total);
					$sheet->setCellvalue('V'.$fila,$value['estado_certificado']);
					$sheet->cells('V'.$fila.':V'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_certificado']);
	        			$cells->setFontColor('#ffffff');
        			});

					$sheet->mergeCells('W'.$fila.':W'.$total);
					$sheet->setCellvalue('W'.$fila,$value['estado_valija']);
					$sheet->cells('W'.$fila.':W'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_valija']);
	        			$cells->setFontColor('#ffffff');
        			});

        			$sheet->mergeCells('X'.$fila.':X'.$total);
					$sheet->setCellvalue('X'.$fila,$value['estado_vl']);
					$sheet->cells('X'.$fila.':X'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_vl']);
	        			$cells->setFontColor('#ffffff');
        			});

					$fila = $total + 1;
					$item++;
				}

				// Para la cantidad de cajas de esa semana
				// $sheet->mergeCells('A'.$fila.':C'.$fila);
				$sheet->setCellvalue('J'.$fila,'TOTAL CAJAS:');
				$sheet->setCellvalue('K'.$fila,$total_cajas);
				$sheet->cells('J'.$fila.':K'.$fila, function($cells) {
        			$cells->setAlignment('center');
        			$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
        		});
				// Ajustar y alinear Texto
				$sheet->getStyle('A9:X'.$fila)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->cells('A9:X'.$fila, function($cells) {$cells->setAlignment('center');});

				// para todos los bordes
				$sheet->cells('A9:X'.($fila-1), function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>false));});
				$sheet->setBorder('A9:X'.($fila-1),'thin');
				
			});
		})->download('xls');
	}

	public static function excelBusquedaContenedores($data)
	{
		Excel::create('Contenedores',function($excel) use($data){
			$excel->sheet('Contenedores',function($sheet) use($data){
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/logo_reportes.png'));
        		$objDrawing->setCoordinates('C2');
        		$objDrawing->setWorksheet($sheet);
        		$sheet->cells('A1:X7', function($cells) {$cells->setBackground('#ffffff');});

        		$sheet->mergeCells('J1:O1');
        		$sheet->setCellvalue('J1','PROGRAMA DE CORTE Y EXPORTACIÓN DE APPBOSA');
        		$sheet->cells('J1:O1', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('J1:O1', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'14','bold'=>true));});

        		$sheet->mergeCells('J2:O2');
        		$sheet->setCellvalue('J2','BÚSQUEDA DE CONTENEDORES');
        		$sheet->cells('J2:O2', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('J2:O2', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>false));});

        		// $sheet->setCellvalue('J4','OPERADOR:');
        		$sheet->setCellvalue('J5','CANTIDAD CONTENEDORES:');
        		// $sheet->setCellvalue('J6','FECHA:');
        		$sheet->cells('J4:J6', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));});

        		// $sheet->mergeCells('M4:O4');
        		// $sheet->setCellvalue('M4',$data[0]['operador']);
        		$sheet->mergeCells('M5:O5');
        		$sheet->setCellvalue('M5',count($data));
        		// $sheet->mergeCells('M6:O6');
        		// $sheet->setCellvalue('M6',date('d/m/Y'));
        		$sheet->cells('M4:O6', function($cells) {$cells->setAlignment('center');});

        		$sheet->setCellvalue('X1','SISAPPBOSA V1.0');
        		$sheet->cells('X1', function($cells) {
        			$cells->setAlignment('right');
        			$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>true));
        		});

				// Cabeceras
				$sheet->setCellvalue('A8','ITEM');
				$sheet->setCellvalue('B8','W');
				$sheet->setCellvalue('C8','FECHA INICIO DE PROCESO');
				$sheet->setCellvalue('D8','FECHA FIN DE PROCESO');
				$sheet->setCellvalue('E8','OPERADOR LOGÍSTICO');
				$sheet->setCellvalue('F8','LÍNEA');
				$sheet->setCellvalue('G8','NAVE');
				$sheet->setCellvalue('H8','BOOKING');
				$sheet->setCellvalue('I8','CLIENTE');
				$sheet->setCellvalue('J8','TIPO DE CAJAS');
				$sheet->setCellvalue('K8','CANTIDAD CAJAS');
				$sheet->setCellvalue('L8','CONTENEDOR');
				$sheet->setCellvalue('M8','ORDEN / REFERENCIA');
				$sheet->setCellvalue('N8','DESTINO');
				$sheet->setCellvalue('O8','PESO NETO (Kg)');
				$sheet->setCellvalue('P8','PESO BRUTO (Kg)');
				$sheet->setCellvalue('Q8','FECHA DE ZARPE');
				$sheet->setCellvalue('R8','FECHA DE LLEGADA');
				$sheet->setCellvalue('S8','ESTADO CONTENEDOR');
				$sheet->setCellvalue('T8','FACTURA');
				$sheet->setCellvalue('U8','PACKING');
				$sheet->setCellvalue('V8','CERTIFICADO');
				$sheet->setCellvalue('W8','VALIJA');
				$sheet->setCellvalue('X8','BLs');
				$sheet->getStyle('A8:X8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->getStyle('A8:X8')->getAlignment()->setWrapText(true);

				$sheet->cells('A8:X8', function($cells) {
					$cells->setAlignment('center');
					$cells->setBackground('#2E4053');
					$cells->setFontColor('#ffffff');
					$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>true));
				});

				$sheet->setHeight(8,39);
				$sheet->setBorder('A8:X8','thin');
				$sheet->setAutoFilter('A8:X8');
				$sheet->setFreeze('A9');
				$sheet->setWidth(array(
				    'A'=>5,'B'=>6,'C'=>12,'D'=>12,'E'=>23,'F'=>20,'G'=>29,'H'=>12,'I'=>12,'J'=>20,
				    'K'=>10,'L'=>12,'M'=>12,'N'=>11,'O'=>8,'P'=>8,'Q'=>10,'R'=>10,'S'=>13,'T'=>13,
				    'U'=>13,'V'=>13,'W'=>13,'X'=>13,
				));

				$fila = 9;
				$item = 1;
				$total_cajas = 0;
				foreach ($data as $value) {
					$can = count($value['cajas']);
					$total = $fila + ($can - 1);
					$total = ($can > 0) ? $fila + ($can - 1) : $total = $fila + $can;
					
					$sheet->mergeCells('A'.$fila.':A'.$total);
					$sheet->setCellvalue('A'.$fila,$item);

					$sheet->mergeCells('B'.$fila.':B'.$total);
					$sheet->setCellvalue('B'.$fila,$value['semana']);

					if($value['dia_proceso_inicio'] != null){
						$fecha_proceso_inicio = date_create($value['dia_proceso_inicio']);
						$sheet->mergeCells('C'.$fila.':C'.$total);
						$sheet->setCellvalue('C'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('C'.$fila.':C'.$total);
						$sheet->setCellvalue('C'.$fila,$value['dia_proceso_inicio']);
					}

					// $sheet->mergeCells('C'.$fila.':C'.$total);
					// $sheet->setCellvalue('C'.$fila,$value['dia_proceso_inicio']);

					if($value['dia_proceso_fin'] != null){
						$fecha_proceso_inicio = date_create($value['dia_proceso_fin']);
						$sheet->mergeCells('D'.$fila.':D'.$total);
						$sheet->setCellvalue('D'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('D'.$fila.':D'.$total);
						$sheet->setCellvalue('D'.$fila,$value['dia_proceso_fin']);
					}

					// $sheet->mergeCells('D'.$fila.':D'.$total);
					// $sheet->setCellvalue('D'.$fila,$value['dia_proceso_fin']);

					$sheet->mergeCells('E'.$fila.':E'.$total);
					$sheet->setCellvalue('E'.$fila,$value['operador']);

					$sheet->mergeCells('F'.$fila.':F'.$total);
					$sheet->setCellvalue('F'.$fila,$value['linea_naviera']);

					$sheet->mergeCells('G'.$fila.':G'.$total);
					$sheet->setCellvalue('G'.$fila,mb_strtoupper($value['sigla_linea_naviera'].'-'.$value['nave'].' V.'.$value['viaje']));

					$sheet->mergeCells('H'.$fila.':H'.$total);
					$sheet->setCellvalue('H'.$fila,$value['booking']);

					$sheet->mergeCells('I'.$fila.':I'.$total);
					$sheet->setCellvalue('I'.$fila,$value['cliente']);

					
					$fila_cajas = $fila;
					foreach ($value['cajas'] as $caja) {
						$sheet->setCellvalue('J'.$fila_cajas,$caja['marca']);
						$sheet->setCellvalue('K'.$fila_cajas,$caja['cantidad_cajas']);
						$total_cajas = $total_cajas + $caja['cantidad_cajas'];
						$fila_cajas++;
					}
					$sheet->mergeCells('L'.$fila.':L'.$total);
					$sheet->setCellvalue('L'.$fila,$value['numero_contenedor']);

					$sheet->mergeCells('M'.$fila.':M'.$total);
					$sheet->setCellvalue('M'.$fila,$value['referencia']);

					$sheet->mergeCells('N'.$fila.':N'.$total);
					$sheet->setCellvalue('N'.$fila,$value['puerto_destino']);

					$sheet->mergeCells('O'.$fila.':O'.$total);
					$sheet->setCellvalue('O'.$fila,$value['peso_neto']);

					$sheet->mergeCells('P'.$fila.':P'.$total);
					$sheet->setCellvalue('P'.$fila,$value['peso_bruto']);

					if($value['dia_zarpe'] != null){
						$fecha_proceso_inicio = date_create($value['dia_zarpe']);
						$sheet->mergeCells('Q'.$fila.':Q'.$total);
						$sheet->setCellvalue('Q'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('Q'.$fila.':Q'.$total);
						$sheet->setCellvalue('Q'.$fila,$value['dia_zarpe']);
					}

					if($value['dia_llegada'] != null){
						$fecha_proceso_inicio = date_create($value['dia_llegada']);
						$sheet->mergeCells('R'.$fila.':R'.$total);
						$sheet->setCellvalue('R'.$fila,date_format($fecha_proceso_inicio,'d-m-Y'));
					}else{
						$sheet->mergeCells('R'.$fila.':R'.$total);
						$sheet->setCellvalue('R'.$fila,$value['dia_llegada']);
					}

					$sheet->mergeCells('S'.$fila.':S'.$total);
					$sheet->setCellvalue('S'.$fila,$value['estado']);
					$sheet->cells('S'.$fila.':S'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado']);
	        			$cells->setFontColor('#ffffff');
        			});

        			// verificar si el contenedor esta en la nave, para resaltar la fila
        			if (self::contenedorEnNave($value['estado'])){
        				$sheet->cells('A'.$fila.':R'.$total, function($cells){
	        				$cells->setBackground('#92D0FC');
        				});
        			}

        			$sheet->mergeCells('T'.$fila.':T'.$total);
					$sheet->setCellvalue('T'.$fila,$value['estado_factura']);
					$sheet->cells('T'.$fila.':T'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_factura']);
	        			$cells->setFontColor('#ffffff');
        			});

					
					$sheet->mergeCells('U'.$fila.':U'.$total);
					$sheet->setCellvalue('U'.$fila,$value['estado_packing']);
					$sheet->cells('U'.$fila.':U'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_packing']);
	        			$cells->setFontColor('#ffffff');
        			});

					$sheet->mergeCells('V'.$fila.':V'.$total);
					$sheet->setCellvalue('V'.$fila,$value['estado_certificado']);
					$sheet->cells('V'.$fila.':V'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_certificado']);
	        			$cells->setFontColor('#ffffff');
        			});

					$sheet->mergeCells('W'.$fila.':W'.$total);
					$sheet->setCellvalue('W'.$fila,$value['estado_valija']);
					$sheet->cells('W'.$fila.':W'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_valija']);
	        			$cells->setFontColor('#ffffff');
        			});

        			$sheet->mergeCells('X'.$fila.':X'.$total);
					$sheet->setCellvalue('X'.$fila,$value['estado_vl']);
					$sheet->cells('X'.$fila.':X'.$total, function($cells) use($value){
	        			$cells->setBackground($value['color_estado_vl']);
	        			$cells->setFontColor('#ffffff');
        			});

					$fila = $total + 1;
					$item++;
				}

				// Para la cantidad de cajas de esa semana
				// $sheet->mergeCells('A'.$fila.':C'.$fila);
				$sheet->setCellvalue('J'.$fila,'TOTAL CAJAS:');
				$sheet->setCellvalue('K'.$fila,$total_cajas);
				$sheet->cells('J'.$fila.':K'.$fila, function($cells) {
        			$cells->setAlignment('center');
        			$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
        		});
				// Ajustar y alinear Texto
				$sheet->getStyle('A9:X'.$fila)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->cells('A9:X'.$fila, function($cells) {$cells->setAlignment('center');});

				// para todos los bordes
				$sheet->cells('A9:X'.($fila-1), function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>false));});
				$sheet->setBorder('A9:X'.($fila-1),'thin');
				
			});
		})->download('xls');
	}

	public static function excelMomivimientoContenedores($data)
	{
		Excel::create('Contenedores',function($excel) use($data){
			$excel->sheet('Contenedores',function($sheet) use($data){
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/logo_reportes.png'));
        		$objDrawing->setCoordinates('C2');
        		$objDrawing->setWorksheet($sheet);
        		$sheet->cells('A1:I7', function($cells) {$cells->setBackground('#ffffff');});

        		$sheet->mergeCells('G1:I1');
        		$sheet->setCellvalue('G1','PROGRAMA DE CORTE Y EXPORTACIÓN DE APPBOSA');
        		$sheet->cells('G1:I1', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('G1:I1', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'12','bold'=>true));});

        		$sheet->mergeCells('G2:I2');
        		$sheet->setCellvalue('G2','REPORTE DE MOVIMIENTO DE LOS CONTENEDORES');
        		$sheet->cells('G2:I2', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('G2:I2', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>false));});

        		$sheet->setCellvalue('G4','AÑO:');
        		$sheet->setCellvalue('G5','CANTIDAD CONTENEDORES:');
        		$sheet->cells('G4:G5', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));});

        		$sheet->setCellvalue('I4',$data[0]['anio']);
        		$sheet->setCellvalue('I5',count($data));
        		$sheet->cells('I4:I5', function($cells) {$cells->setAlignment('center');});

        		$sheet->setCellvalue('I7',date('d/m/Y').' | SISAPPBOSA V1.0');
        		$sheet->cells('I7:I7', function($cells) {
        			$cells->setAlignment('right');
        			$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>true));
        		});

				// Cabeceras
				$sheet->setCellvalue('A8','ITEM');
				$sheet->setCellvalue('B8','W');
				$sheet->setCellvalue('C8','REFERENCIA');
				$sheet->setCellvalue('D8','N° CONTENEDOR');
				$sheet->setCellvalue('E8','ESTADOS');
				$sheet->setCellvalue('F8','FECHA');
				$sheet->setCellvalue('G8','HORA');
				$sheet->setCellvalue('H8','USUARIO RESPONSABLE');
				$sheet->setCellvalue('I8','CARGO EN EMPRESA');
				$sheet->getStyle('A8:I8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->getStyle('A8:I8')->getAlignment()->setWrapText(true);

				$sheet->cells('A8:I8', function($cells) {
					$cells->setAlignment('center');
					$cells->setBackground('#2E4053');
					$cells->setFontColor('#ffffff');
					$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>true));
				});

				$sheet->setHeight(8,39);
				$sheet->setBorder('A8:I8','thin');
				$sheet->setAutoFilter('A8:I8');
				$sheet->setFreeze('A9');
				$sheet->setWidth(array(
				    'A'=>5,'B'=>6,'C'=>15,'D'=>17,'E'=>17,'F'=>15,'G'=>15,'H'=>32,'I'=>27
				));

				$fila = 9;
				$item = 1;
				foreach ($data as $value) {
					$can = count($value['movimientos_contenedor']);
					$total = ($can > 0) ? $fila + ($can - 1) : $total = $fila + $can;
					
					$sheet->mergeCells('A'.$fila.':A'.$total);
					$sheet->setCellvalue('A'.$fila,$item);

					$sheet->mergeCells('B'.$fila.':B'.$total);
					$sheet->setCellvalue('B'.$fila,$value['semana']);

					$sheet->mergeCells('C'.$fila.':C'.$total);
					$sheet->setCellvalue('C'.$fila,$value['referencia_contenedor']);

					$sheet->mergeCells('D'.$fila.':D'.$total);
					$sheet->setCellvalue('D'.$fila,$value['numero_contenedor']);

					$fila_movimientos = $fila;
					foreach ($value['movimientos_contenedor'] as $movimiento) {
						$sheet->setCellvalue('E'.$fila_movimientos,$movimiento['estado']);
						$sheet->cells('E'.$fila_movimientos.':E'.$fila_movimientos, function($cells) use($movimiento){
		        			$cells->setBackground($movimiento['color_estado']);
		        			$cells->setFontColor('#ffffff');
	        			});
	        			if($movimiento['fecha'] != null){
							$fecha = date_create($movimiento['fecha']);
							$sheet->setCellvalue('F'.$fila_movimientos,date_format($fecha,'d-m-Y'));
						}else{
							$sheet->setCellvalue('F'.$fila_movimientos,$movimiento['fecha']);
						}
						// $sheet->setCellvalue('F'.$fila_movimientos,$movimiento['fecha']);
						$sheet->setCellvalue('G'.$fila_movimientos,$movimiento['hora']);
						$sheet->setCellvalue('H'.$fila_movimientos,$movimiento['usuario_responsable']);
						$sheet->setCellvalue('I'.$fila_movimientos,$movimiento['cargo_usuario_responsable']);
						$fila_movimientos++;
					}
					$fila = $total + 1;
					$item++;
				}
				// Ajustar y alinear Texto
				$sheet->getStyle('A9:I'.$fila)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->cells('A9:I'.$fila, function($cells) {$cells->setAlignment('center');});

				// para todos los bordes
				$sheet->cells('A9:I'.($fila-1), function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>false));});
				$sheet->setBorder('A9:I'.($fila-1),'thin');
				
			});
		})->download('xls');
	}

	public static function excelDetalleContenedor($detalle,$movimientos)
	{
		Excel::create('DContenedor',function($excel) use($detalle,$movimientos){
			$excel->sheet('DContenedor',function($sheet) use($detalle,$movimientos){
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/jen.png'));
        		$objDrawing->setCoordinates('A1');
        		$objDrawing->setWorksheet($sheet);
        		$sheet->cells('A1:J30', function($cells) {$cells->setBackground('#ffffff');});
        		$sheet->setFreeze('A9');

        		$sheet->setWidth(array(
				    'A'=>11,'B'=>10,'C'=>15,'D'=>19,'E'=>10,'F'=>12,'G'=>19,'H'=>14,'I'=>11,'J'=>11
				));

				$sheet->mergeCells('C7:H7');
				$sheet->setCellvalue('C7','DETALLE DE CONTENEDOR');
				$sheet->cells('C7:H7', function($cells) {
					$cells->setAlignment('center');
        			$cells->setFont(array('family'=>'Calibri','size'=>'20','bold'=>true));
        		});
        		$sheet->mergeCells('E8:F8');
        		$sheet->setCellvalue('E8',$detalle[0]['numero_contenedor']);
        		$sheet->cells('E8:F8', function($cells) {$cells->setAlignment('center');});

        		$sheet->setCellvalue('J7','W = '.$detalle[0]['semana']);
        		$sheet->setCellvalue('J8',date('d/m/Y'));
        		$sheet->cells('J7:J8',function($cells){
        			$cells->setAlignment('center');
        			$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
        		});

        		$sheet->setCellvalue('A10','Referencia: ');
        		$sheet->setCellvalue('A11','Booking: ');
        		$sheet->setCellvalue('A12','Operador: ');
        		$sheet->setCellvalue('A13','Valija: ');

        		$sheet->setCellvalue('D10','Cliente: ');
        		$sheet->setCellvalue('D11','Línea Naviera: ');
        		$sheet->setCellvalue('D12','Peso Neto (Kg): ');
        		$sheet->setCellvalue('D13','Fecha Inicio Proceso: ');
        		$sheet->setCellvalue('D14','Fecha de Zarpe: ');

        		$sheet->setCellvalue('G10','Puerto: ');
        		$sheet->setCellvalue('G11','Nave: ');
        		$sheet->setCellvalue('G12','Peso Bruto (Kg): ');
        		$sheet->setCellvalue('G13','Fecha Fin Proceso: ');
        		$sheet->setCellvalue('G14','Fecha de Llegada: ');

        		$sheet->cells('A10:A13', function($cells) {
					$cells->setAlignment('left');
        			$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
        		});

        		$sheet->cells('D10:D14', function($cells) {
					$cells->setAlignment('left');
        			$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
        		});

        		$sheet->cells('G10:G14', function($cells) {
					$cells->setAlignment('left');
        			$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
        		});

        		// colocar la data en las celdas
        		$sheet->mergeCells('B10:C10');
        		$sheet->setCellvalue('B10',$detalle[0]['referencia']);
        		$sheet->mergeCells('B11:C11');
        		$sheet->setCellvalue('B11',$detalle[0]['booking']);
        		$sheet->mergeCells('B12:C12');
        		$sheet->setCellvalue('B12',$detalle[0]['operador']);
        		$sheet->mergeCells('B13:C13');
        		$sheet->setCellvalue('B13',$detalle[0]['valija']);

        		$sheet->mergeCells('E10:F10');
        		$sheet->setCellvalue('E10',$detalle[0]['cliente']);
        		$sheet->mergeCells('E11:F11');
        		$sheet->setCellvalue('E11',$detalle[0]['linea_naviera']);
        		$sheet->mergeCells('E12:F12');
        		$sheet->setCellvalue('E12',$detalle[0]['peso_neto']);

        		if($detalle[0]['dia_proceso_inicio'] != null){
					$fecha = date_create($detalle[0]['dia_proceso_inicio']);
					$sheet->mergeCells('E13:F13');
        			$sheet->setCellvalue('E13',date_format($fecha,'d-m-Y'));
				}else{
					$sheet->mergeCells('E13:F13');
					$sheet->setCellvalue('E13',$detalle[0]['dia_proceso_inicio']);
				}
        		// $sheet->mergeCells('E13:F13');
        		// $sheet->setCellvalue('E13',$detalle[0]['dia_proceso_inicio']);


				if($detalle[0]['dia_zarpe'] != null){
					$fecha = date_create($detalle[0]['dia_zarpe']);
					$sheet->mergeCells('E14:F14');
        			$sheet->setCellvalue('E14',date_format($fecha,'d-m-Y'));
				}else{
					$sheet->mergeCells('E14:F14');
        			$sheet->setCellvalue('E14',$detalle[0]['dia_zarpe']);
				}
        		// $sheet->mergeCells('E14:F14');
        		// $sheet->setCellvalue('E14',$detalle[0]['dia_zarpe']);

        		$sheet->mergeCells('H10:J10');
        		$sheet->setCellvalue('H10',$detalle[0]['puerto_destino']);
        		$sheet->mergeCells('H11:J11');
        		$sheet->setCellvalue('H11',$detalle[0]['sigla_linea_naviera'].'-'.$detalle[0]['nave'].' V.'.$detalle[0]['viaje']);
        		$sheet->mergeCells('H12:J12');
        		$sheet->setCellvalue('H12',$detalle[0]['peso_bruto']);

        		if($detalle[0]['dia_proceso_fin'] != null){
					$fecha = date_create($detalle[0]['dia_proceso_fin']);
					$sheet->mergeCells('H13:H13');
        			$sheet->setCellvalue('H13',date_format($fecha,'d-m-Y'));
				}else{
					$sheet->mergeCells('H13:J13');
        			$sheet->setCellvalue('H13',$detalle[0]['dia_proceso_fin']);
				}
        		// $sheet->mergeCells('H13:J13');
        		// $sheet->setCellvalue('H13',$detalle[0]['dia_proceso_fin']);

				if($detalle[0]['dia_llegada'] != null){
					$fecha = date_create($detalle[0]['dia_llegada']);
					$sheet->mergeCells('H14:H14');
        			$sheet->setCellvalue('H14',date_format($fecha,'d-m-Y'));
				}else{
					$sheet->mergeCells('H14:J14');
        			$sheet->setCellvalue('H14',$detalle[0]['dia_llegada']);
				}
        		// $sheet->mergeCells('H14:J14');
        		// $sheet->setCellvalue('H14',$detalle[0]['dia_llegada']);
        		


        		// ------------------------Cajas---------------------------------------
        		$sheet->mergeCells('D16:G16');
        		$sheet->setCellvalue('D16','CAJAS');
        		$sheet->cells('D16:G16', function($cells) {
        			$cells->setAlignment('center');
        			$cells->setFont(array('family'=>'Calibri','size'=>'12','bold'=>true));
        		});
        		// Cabeceras para las cajas
        		$sheet->mergeCells('D17:E17');
        		$sheet->setCellvalue('D17','Marca');
        		$sheet->mergeCells('F17:G17');
        		$sheet->setCellvalue('F17','Cantidad');
        		$sheet->cells('D17:F17', function($cells) {
					$cells->setAlignment('center');
					$cells->setBackground('#FF8C00');
					$cells->setFontColor('#ffffff');
					$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
				});

				$fila       = 18;
				$fila_cajas = 18;
				foreach ($detalle[0]['cajas'] as $value) {
					// para la marca
					$sheet->mergeCells('D'.$fila.':E'.$fila);
					$sheet->setCellvalue('D'.$fila,$value['marca']);
					// para la cantidad de cajas
					$sheet->mergeCells('F'.$fila.':G'.$fila);
					$sheet->setCellvalue('F'.$fila,$value['cantidad_cajas']);
					$fila++;
				}
				$sheet->cells('D'.$fila_cajas.':F'.$fila, function($cells) {
					$cells->setAlignment('center');
				});
				//-----------------------------------------------------------------------



        		// --------------------------Movimientos---------------------------------
				$fila = $fila + 1;
        		$sheet->mergeCells('D'.$fila.':G'.$fila);
        		$sheet->setCellvalue('D'.$fila,'MOVIMIENTOS');
        		$sheet->cells('D'.$fila.':G'.$fila, function($cells) {
        			$cells->setAlignment('center');
        			$cells->setFont(array('family'=>'Calibri','size'=>'12','bold'=>true));
        		});

        		// Cabecera para la tabla de los movimientos
        		$fila = $fila + 1;
        		$sheet->setCellvalue('B'.$fila,'#');
        		$sheet->setCellvalue('C'.$fila,'Estado');
        		$sheet->setCellvalue('D'.$fila,'Fecha');
        		$sheet->setCellvalue('E'.$fila,'Hora');
        		$sheet->mergeCells('F'.$fila.':G'.$fila);
        		$sheet->setCellvalue('F'.$fila,'Usuario');
        		$sheet->mergeCells('H'.$fila.':I'.$fila);
        		$sheet->setCellvalue('H'.$fila,'Cargo');
        		$sheet->cells('B'.$fila.':H'.$fila, function($cells) {
					$cells->setAlignment('center');
					$cells->setBackground('#2E4053');
					$cells->setFontColor('#ffffff');
					$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
				});
				
				$fila             = $fila + 1;
				$fila_movimientos = $fila;
				$cont 			  = 1;
				foreach ($movimientos['movimientos_contenedor'] as $value) {
					$sheet->setCellvalue('B'.$fila,$cont);
					$sheet->setCellvalue('C'.$fila,$value['estado']);
					if($value['fecha'] != null){
						$fecha = date_create($value['fecha']);
	        			$sheet->setCellvalue('D'.$fila,date_format($fecha,'d-m-Y'));
					}else{
						$sheet->setCellvalue('D'.$fila,$value['fecha']);
					}
	        		// $sheet->setCellvalue('D'.$fila,$value['fecha']);
	        		$sheet->setCellvalue('E'.$fila,$value['hora']);
	        		$sheet->mergeCells('F'.$fila.':G'.$fila);
	        		$sheet->setCellvalue('F'.$fila,$value['usuario_responsable']);
	        		$sheet->mergeCells('H'.$fila.':I'.$fila);
	        		$sheet->setCellvalue('H'.$fila,$value['cargo_usuario_responsable']);
	        		$fila++;
	        		$cont++;
				}
				$sheet->cells('B'.$fila_movimientos.':H'.$fila, function($cells) {
					$cells->setAlignment('center');
				});
				//---------------------------------------------------------------------------
			});
		})->download('xls');
	}

	public static function excelResumenCajasExportadas($semanas,$clientes,$anio)
	{
		Excel::create('ResumenCajas',function($excel)use($semanas,$clientes,$anio){
			$excel->sheet('ResumenCajas',function($sheet)use($semanas,$clientes,$anio){

				#-------------------- Logo -----------------------------------
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/logo_reportes.png'));
        		$objDrawing->setCoordinates('A1');
        		$objDrawing->setWorksheet($sheet);
        		$objDrawing->setWidthAndHeight(90,165);
				$objDrawing->setResizeProportional(true);
        		#-------------------- Fin Logo -------------------------------
        		
        		#------------------- Título del reporte ----------------------
        		$sheet->setCellvalue('C1','INFORME DE CAJAS EXPORTADAS');
        		$sheet->setCellvalue('C2','Actualizado hasta el: '.date('d-m-Y'));
        		$sheet->cells('C1', function($cells){ $cells->setFont(array('family'=>'Calibri','size'=>'14','bold'=>true));});
        		$sheet->cells('C2', function($cells){ $cells->setFont(array('family'=>'Calibri','size'=>'10','bold'=>false));});
        		#------------------- Fin Título del reporte ------------------


				#-------------------- CABECEREAS ESTÁTICAS--------------------
				$sheet->mergeCells('A3:A5');
				$sheet->setCellvalue('A3','AÑO');
				$sheet->mergeCells('B3:B5');
				$sheet->setCellvalue('B3','SEMANA CORTE');
				$sheet->cells('A3:B5', function($cells){
					$cells->setBackground('#85C1E9');
				});
				#-------------------- FIN CABECEREAS ESTÁTICAS-----------------

				#-------------------- CABECEREAS DINÁMICAS --------------------
				// $fila_cliente  = 3;
				// $fila_caja     = 4;
				$letra_cliente = 3;
				$letra_caja    = 3;
					#-------------------- RECORRIDO DE LOS CLIENTES -------------
					$color = 1;
					foreach ($clientes as $cliente) {
						$siguiente = $letra_cliente + count($cliente['cajas']);
						$sheet->mergeCells(self::getLetra($letra_cliente).'3'.':'.self::getLetra($siguiente-1).'3');
						$sheet->setCellvalue(self::getLetra($letra_cliente).'3',$cliente['razon_social']);
						$sheet->cells(self::getLetra($letra_cliente).'3'.':'.self::getLetra($siguiente-1).'5', function($cells) use($color){
							$cells->setBackground(self::getColorClientHaxadecimal($color));
						});
						$color++;
						#-------------------- RECORRIDO DE LOS CAJAS QUE LE PERTENECEN AL CLIENTE --------------------
						foreach ($cliente['cajas'] as $caja) {
							$sheet->mergeCells(self::getLetra($letra_caja).'4'.':'.self::getLetra($letra_caja).'5');
							$sheet->setCellvalue(self::getLetra($letra_caja).'4',$caja['marca']);
							#----------------- RECORRIDO TOTAL DEL NÚMERO DE CAJAS DE UNA MARCA POR SEMANA------------
							$fila_semana   = 6;
							foreach ($caja['total_cajas'] as $total) {
								$sheet->setCellvalue(self::getLetra($letra_caja).$fila_semana,$total['total_cajas']);
								$fila_semana++;
							}
							$sheet->setCellvalue(self::getLetra($letra_caja).$fila_semana,'=SUM('.self::getLetra($letra_caja).'6:'.self::getLetra($letra_caja).($fila_semana-1).')');
							##----------------- RECORRIDO TOTAL DEL NÚMERO DE CAJAS DE UNA MARCA POR SEMANA------------
							$letra_caja++;
						}
						#-------------------- FIN RECORRIDO DE LOS CAJAS QUE LE PERTENECEN AL CLIENTE ----------------
						$letra_cliente = $siguiente;
					}
					#-------------------- FIN DE RECORRIDO DE LOS CLIENTES ----------
				#-------------------- FIN CABECEREAS DINÁMICAS --------------------
		
				#------------------------ CABCECERAS (TOTAL CAJAS Y CONTENODORES)----------------------------------
				$letra_total_cajas  = $letra_cliente;
				$letra_contenedores = $letra_total_cajas + 1;
				$sheet->mergeCells(self::getLetra($letra_total_cajas).'3:'.self::getLetra($letra_total_cajas).'5');
				$sheet->setCellvalue(self::getLetra($letra_total_cajas).'3','TOTAL DE CAJAS');
				// $sheet->getStyle(self::getLetra($letra_total_cajas).'3')->getAlignment()->setTextRotation(90); // Rotación del texto
				$sheet->cells(self::getLetra($letra_total_cajas).'3', function($cells){$cells->setBackground('#D5DBDB');});
				

				$sheet->mergeCells(self::getLetra($letra_contenedores).'3:'.self::getLetra($letra_contenedores).'5');
				$sheet->setCellvalue(self::getLetra($letra_contenedores).'3','TOTAL DE CONTENEDORES');
				// $sheet->getStyle(self::getLetra($letra_contenedores).'3')->getAlignment()->setTextRotation(90); // Rotación del texto
				$sheet->cells(self::getLetra($letra_contenedores).'3', function($cells){$cells->setBackground('#AEB6BF');});
				#------------------------ FIN CABCECERAS (TOTAL CAJAS Y CONTENODORES)------------------------------
			


				#--------------------- RECORRIDO DE LA CANTIDAD DE SEMANAS Y SU CANTIDAD DE CONTENEDORES ----------------
				#----------------------Y AAGREGAR LA FUNCIÓN PARA SUMAR LA CANTIDAD DE CAJAS POR CADA SEMANA-------------
				$fila_semana = 6;
				$sheet->mergeCells('A6:A'.(($fila_semana-1) + count($semanas)));
				$sheet->setCellvalue('A6',$anio);
				$sheet->getStyle('A6')->getAlignment()->setTextRotation(90); // Rotación del texto

				$letra_total_cajas_formula  = self::getLetra($letra_total_cajas-1);
				$letra_total_cajas  		= self::getLetra($letra_total_cajas);
				$letra_contenedores 		= self::getLetra($letra_contenedores);
				
				foreach ($semanas as $semana) {
					$sheet->setCellvalue('B'.$fila_semana,$semana['nombre']);
					$sheet->setCellvalue($letra_total_cajas.$fila_semana,'=SUM(C'.$fila_semana.':'.$letra_total_cajas_formula.$fila_semana.')');
					$sheet->setCellvalue($letra_contenedores.$fila_semana,$semana['cantidad_contenedores']);
					// $letra_contenedores++;
					$fila_semana++;
				}
					#------- Estilos para las semanas y el año--------
					$sheet->getStyle('A6:B'.($fila_semana-1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
					$sheet->getStyle('A6:B'.($fila_semana-1))->getAlignment()->setWrapText(true);
					$sheet->cells('A6:B'.($fila_semana-1), function($cells) {
							$cells->setAlignment('center');
							$cells->setFont(array('family'=>'Calibri','size'=>'8','bold'=>true));
					});
					#------- Estilos para las semanas y el año---------
					#------- Totales-----------
					$sheet->mergeCells('A'.$fila_semana.':B'.$fila_semana);
					$sheet->setCellvalue('A'.$fila_semana,'TOTAL');
					$sheet->setCellvalue($letra_total_cajas.$fila_semana,'=SUM('.$letra_total_cajas.'6:'.$letra_total_cajas.($fila_semana-1).')');
					$sheet->setCellvalue($letra_contenedores.$fila_semana,'=SUM('.$letra_contenedores.'6:'.$letra_contenedores.($fila_semana-1).')');
					$sheet->cells('A'.$fila_semana.':'.$letra_contenedores.$fila_semana, function($cells) {
						$cells->setAlignment('center');
						$cells->setBackground('#27AE60');
						$cells->setFont(array('family'=>'Calibri','size'=>'8','bold'=>true));
					});
					#-------Fin totales--------
				#--------------------- FIN RECORRIDO DE LA CANTIDAD DE SEMANAS Y SU CANTIDAD DE CONTENEDORES--------------
								
				
				#---------------------- FORMATO DE CELDAS-----------------------------------------------------------------
				// $sheet->setHeight(5,53);
				// $sheet->setHeight(3,25);
					$sheet->setHeight([3=>25,5=>53]);
				$sheet->getStyle('A3:'.$letra_contenedores.'5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->getStyle('A3:'.$letra_contenedores.'5')->getAlignment()->setWrapText(true);
				$sheet->cells('A3:'.$letra_contenedores.'5', function($cells) {
						$cells->setAlignment('center');
						$cells->setFont(array('family'=>'Calibri','size'=>'8','bold'=>true));
				});

				#------------- Formata para la información generada para cada caja----------------
				$sheet->cells('C6:'.$letra_contenedores.($fila_semana-1), function($cells) {
						$cells->setAlignment('center');
						$cells->setFont(array('family'=>'Calibri','size'=>'8','bold'=>true));
				});
				#------------- Fin Formata para la información generada para cada caja------------

				$sheet->setWidth([
					'A'=>4,'B'=>9,'C'=>9,'D'=>9,'E'=>9,'F'=>9,'G'=>9,'H'=>9,'I'=>9,'J'=>9,'K'=>9,
			    	'L'=>9,'M'=>9,'N'=>9,'O'=>9,'P'=>9,'Q'=>9,'R'=>9,'S'=>9,'T'=>9,'U'=>9,'V'=>9,
			    	'W'=>9,'X'=>9,'Y'=>9,'Z'=>9,'AA'=>9,'AB'=>9,'AC'=>9,'AD'=>9,'AE'=>9,'AF'=>9,
			    	'AG'=>9,'AH'=>9,'AI'=>9,'AJ'=>9,'AK'=>9,'AL'=>9,'AM'=>9,'AN'=>9,'AO'=>9,'AP'=>9,
			    	'AQ'=>9,'AR'=>9,'AS'=>9,'AT'=>9,'AU'=>9,'AV'=>9,'AW'=>9,'AX'=>9,'AY'=>9,'AZ'=>9,
			    	'BA'=>9,'BB'=>9,'BC'=>9,'BD'=>9,'BE'=>9,'BF'=>9,'BG'=>9,'BH'=>9,'BI'=>9,'BJ'=>9,
			    	'BK'=>9,'BL'=>9,'BM'=>9,'BN'=>9,'BO'=>9,'BP'=>9,'BQ'=>9,'BR'=>9,'BS'=>9,'BT'=>9,
			    	'BU'=>9,'BV'=>9,'BW'=>9,'BX'=>9,'BY'=>9,'BZ'=>9
				]);
				$sheet->setFreeze('C6');
				$sheet->setBorder('A3:'.$letra_contenedores.$fila_semana,'thin');
				#---------------------- FIN DE FORMATO DE CELDAS----------------------------------------------------------
			});
		})->download('xls');
	}

	public static function excelAuditoria($data,$tabla)
	{
		Excel::create('ReporteAuditorias',function($excel) use($data,$tabla){
			$excel->sheet('ReporteAuditorias',function($sheet) use($data,$tabla){

				#-------------------- Logo -----------------------------------
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/logo_reportes.png'));
        		$objDrawing->setCoordinates('A1');
        		$objDrawing->setWorksheet($sheet);
        		$objDrawing->setWidthAndHeight(150,165);
				$objDrawing->setResizeProportional(true);
        		#-------------------- Fin Logo -------------------------------
        		#
				#------------------- Título del reporte ----------------------
        		$sheet->setCellvalue('C1','AUDITORÍA: '.$tabla);
        		$sheet->setCellvalue('C2','Actualizado hasta el: '.date('d-m-Y'));
        		$sheet->cells('C1', function($cells){ $cells->setFont(array('family'=>'Calibri','size'=>'14','bold'=>true));});
        		$sheet->cells('C2', function($cells){ $cells->setFont(array('family'=>'Calibri','size'=>'10','bold'=>false));});
        		#------------------- Fin Título del reporte ------------------
				
				#------------ Recorrer cabeceras de la tabla --------------------------
				$sheet->setCellvalue('A4','USUARIO RESPONSABLE');
				$sheet->setCellvalue('B4','REGISTRO AFECTADO');
				$sheet->setCellvalue('C4','OPERACIÓN');
				$sheet->setCellvalue('D4','FECHA');
				$sheet->setCellvalue('E4','HORA');
				$sheet->setCellvalue('F4','DIRECCIÓN IP');
				$sheet->setCellvalue('G4','DISPOSITIVO');
				$sheet->cells('A4:G4', function($cells){
					$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true)); 
					$cells->setBackground('#04B647');
					$cells->setFontColor('#FFFFFF');
					$cells->setAlignment('center');
				});
				#------------ Fin Recorrer cabeceras de la tabla ----------------------
				
				#------------ Recorrer información -------------------------
				$fila_data = 5;
				foreach ($data as $value) {
					$sheet->setCellvalue('A'.$fila_data,$value->usuario_responsable);
					$sheet->setCellvalue('B'.$fila_data,$value->usuario_afectado);
					$sheet->setCellvalue('C'.$fila_data,$value->operacion);
					$sheet->setCellvalue('D'.$fila_data,$value->fecha);
					$sheet->setCellvalue('E'.$fila_data,$value->hora);
					$sheet->setCellvalue('F'.$fila_data,$value->ip);
					$sheet->setCellvalue('G'.$fila_data,$value->dispositivo);
					$fila_data++;
				}
				#------------ Fin información ------------------------------
				#------------- Estilos adicionales ------------------------
				$sheet->setFreeze('A5');
				$sheet->setAutoFilter('A4:G4');
				$sheet->cells('A5:G'.$fila_data, function($cells){
					$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>false)); 
					$cells->setAlignment('center');
				});
				#------------- Fin Estilos adicionales --------------------
				
			});
		})->download('xls');
	}

	public static function excelAuditoriasSecundarias($data,$tabla)
	{
		Excel::create('ReporteAuditorias',function($excel) use($data,$tabla){
			$excel->sheet('ReporteAuditorias',function($sheet) use($data,$tabla){

				#-------------------- Logo -----------------------------------
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/logo_reportes.png'));
        		$objDrawing->setCoordinates('A1');
        		$objDrawing->setWorksheet($sheet);
        		$objDrawing->setWidthAndHeight(150,165);
				$objDrawing->setResizeProportional(true);
        		#-------------------- Fin Logo -------------------------------
        		#
				#------------------- Título del reporte ----------------------
        		$sheet->setCellvalue('C1','AUDITORÍA: '.mb_strtoupper($tabla));
        		$sheet->setCellvalue('C2','Actualizado hasta el: '.date('d-m-Y'));
        		$sheet->cells('C1', function($cells){ $cells->setFont(array('family'=>'Calibri','size'=>'14','bold'=>true));});
        		$sheet->cells('C2', function($cells){ $cells->setFont(array('family'=>'Calibri','size'=>'10','bold'=>false));});
        		#------------------- Fin Título del reporte ------------------
				
				#------------ Recorrer cabeceras de la tabla --------------------------
				$sheet->setCellvalue('A4','USUARIO RESPONSABLE');
				$sheet->setCellvalue('B4','REGISTRO AFECTADO');
				$sheet->setCellvalue('C4','OPERACIÓN');
				$sheet->setCellvalue('D4','FECHA');
				$sheet->setCellvalue('E4','HORA');
				$sheet->setCellvalue('F4','DIRECCIÓN IP');
				$sheet->setCellvalue('G4','DISPOSITIVO');
				$sheet->cells('A4:G4', function($cells){
					$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true)); 
					$cells->setBackground('#04B647');
					$cells->setFontColor('#FFFFFF');
					$cells->setAlignment('center');
				});
				#------------ Fin Recorrer cabeceras de la tabla ----------------------
				
				#------------ Recorrer información -------------------------
				$columna = ($tabla == 'usuario') ? true : false ;
				$fila_data = 5;
				foreach ($data as $value) {
					$sheet->setCellvalue('A'.$fila_data,$value->usuario_responsable);
					$sheet->setCellvalue('B'.$fila_data,($columna) ? $value->usuario_afectado : $value->registro);
					$sheet->setCellvalue('C'.$fila_data,$value->operacion);
					$sheet->setCellvalue('D'.$fila_data,$value->fecha);
					$sheet->setCellvalue('E'.$fila_data,$value->hora);
					$sheet->setCellvalue('F'.$fila_data,$value->ip);
					$sheet->setCellvalue('G'.$fila_data,$value->dispositivo);
					$fila_data++;
				}
				if(count($data) == 0){
					$sheet->mergeCells('A5:G5');
					$sheet->setCellvalue('A5','No hay registros');
				}
				#------------ Fin información ------------------------------
				#------------- Estilos adicionales ------------------------
				$sheet->setFreeze('A5');
				$sheet->setAutoFilter('A4:G4');
				$sheet->cells('A5:G'.$fila_data, function($cells){
					$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>false)); 
					$cells->setAlignment('center');
				});
				#------------- Fin Estilos adicionales --------------------
				
			});
		})->download('xls');
	}

	public static function excelAuditoriasPrincipal($data,$tabla)
	{
		Excel::create('ReporteAuditorias',function($excel) use($data,$tabla){
			$excel->sheet('ReporteAuditorias',function($sheet) use($data,$tabla){

				#-------------------- Logo -----------------------------------
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/logo_reportes.png'));
        		$objDrawing->setCoordinates('A1');
        		$objDrawing->setWorksheet($sheet);
        		$objDrawing->setWidthAndHeight(150,165);
				$objDrawing->setResizeProportional(true);
        		#-------------------- Fin Logo -------------------------------
        		#
				#------------------- Título del reporte ----------------------
        		$sheet->setCellvalue('D1','AUDITORÍA: '.mb_strtoupper($tabla));
        		$sheet->setCellvalue('D2','Actualizado hasta el: '.date('d-m-Y'));
        		$sheet->cells('D1', function($cells){ $cells->setFont(array('family'=>'Calibri','size'=>'14','bold'=>true));});
        		$sheet->cells('D2', function($cells){ $cells->setFont(array('family'=>'Calibri','size'=>'10','bold'=>false));});
        		#------------------- Fin Título del reporte ------------------
				
				#------------ Recorrer cabeceras de la tabla --------------------------
				$sheet->setCellvalue('A4','USUARIO RESPONSABLE');
				$sheet->setCellvalue('B4','SEMANA');
				$sheet->setCellvalue('C4','NÚMERO CONTENEDOR');
				$sheet->setCellvalue('D4','REFERENCIA');
				$sheet->setCellvalue('E4','OPERACIÓN');
				$sheet->setCellvalue('F4','FECHA');
				$sheet->setCellvalue('G4','HORA');
				$sheet->setCellvalue('H4','DIRECCIÓN IP');
				$sheet->setCellvalue('I4','DISPOSITIVO');
				$sheet->cells('A4:I4', function($cells){
					$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true)); 
					$cells->setBackground('#04B647');
					$cells->setFontColor('#FFFFFF');
					$cells->setAlignment('center');
				});
				#------------ Fin Recorrer cabeceras de la tabla ----------------------
				
				#------------ Recorrer información -------------------------
				$fila_data = 5;
				foreach ($data as $value) {
					$sheet->setCellvalue('A'.$fila_data,$value->usuario_responsable);
					$sheet->setCellvalue('B'.$fila_data,$value->semana);
					$sheet->setCellvalue('C'.$fila_data,$value->numero_contenedor);
					$sheet->setCellvalue('D'.$fila_data,$value->referencia);
					$sheet->setCellvalue('E'.$fila_data,$value->operacion);
					$sheet->setCellvalue('F'.$fila_data,$value->fecha);
					$sheet->setCellvalue('G'.$fila_data,$value->hora);
					$sheet->setCellvalue('H'.$fila_data,$value->ip);
					$sheet->setCellvalue('I'.$fila_data,$value->dispositivo);
					$fila_data++;
				}
				#------------ Fin información ------------------------------
				#------------- Estilos adicionales ------------------------
				$sheet->setFreeze('A5');
				$sheet->setAutoFilter('A4:I4');
				$sheet->cells('A5:I'.$fila_data, function($cells){
					$cells->setFont(array('family'=>'Calibri','size'=>'9','bold'=>false)); 
					$cells->setAlignment('center');
				});
				#------------- Fin Estilos adicionales --------------------
				
			});
		})->download('xls');
	}

	public static function excelSectoresConDelegados($data)
	{
		Excel::create('Sectores',function($excel) use($data){
			$excel->sheet('Sectores',function($sheet) use($data){
				$objDrawing = new PHPExcel_Worksheet_Drawing;
        		$objDrawing->setPath(public_path('assets/logo_reportes.png'));
        		$objDrawing->setCoordinates('A1');
        		$objDrawing->setWorksheet($sheet);

        		$sheet->cells('A1:E5', function($cells) {$cells->setBackground('#ffffff');});

        		$sheet->mergeCells('C2:E2');
        		$sheet->setCellvalue('C2','PROGRAMA DE CORTE Y EXPORTACIÓN DE APPBOSA');
        		$sheet->cells('C2:E2', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('C2:E2', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'12','bold'=>true));});

        		$sheet->mergeCells('C3:E3');
        		$sheet->setCellvalue('C3','REPORTE DE SECTORES Y DELEGADOS');
        		$sheet->cells('C3:E3', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('C3:E3', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>false));});

        		$sheet->mergeCells('C4:E4');
        		$sheet->setCellvalue('C4','CANTIDAD DE SECTORES: '.count($data).' REGISTRADOS');
        		$sheet->cells('C4:E4', function($cells) {$cells->setAlignment('center');});
        		$sheet->cells('C4:E4', function($cells) {$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));});

				// Cabeceras
				$sheet->mergeCells('A6:A7');
				$sheet->setCellvalue('A6','SECTOR');
				$sheet->mergeCells('B7:B7');
				$sheet->setCellvalue('B7','UBICACIÓN');
				$sheet->mergeCells('C6:E6');
				$sheet->setCellvalue('C6','DELEGADOS');
				$sheet->setCellvalue('C7','APELLIDOS Y NOMBRES');
				$sheet->setCellvalue('D7','FECHA INICIO');
				$sheet->setCellvalue('E7','FECHA FIN');
				$sheet->getStyle('A6:E7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->getStyle('A6:E7')->getAlignment()->setWrapText(true);

				$sheet->cells('A6:E7', function($cells) {
					$cells->setAlignment('center');
					$cells->setBackground('#2E4053');
					$cells->setFontColor('#ffffff');
					$cells->setFont(array('family'=>'Calibri','size'=>'11','bold'=>true));
				});

				$sheet->setFreeze('A8');
				$sheet->setWidth(array('A'=>21,'B'=>28,'C'=>39,'D'=>14,'E'=>20));

				$fila_inicio = 8;
				foreach ($data as $sector) {
					$cantidad_delegados = count($sector['delegados']);
					$fila_siguiente     = ($cantidad_delegados != 0) ? $fila_inicio + ($cantidad_delegados - 1) : $fila_inicio;

					# Poner contenido del sector
					$sheet->mergeCells('A'.$fila_inicio.':A'.$fila_siguiente);
					$sheet->setCellvalue('A'.$fila_inicio,$sector['nombre']);
					$sheet->mergeCells('B'.$fila_inicio.':B'.$fila_siguiente);
					$sheet->setCellvalue('B'.$fila_inicio,$sector['ubicacion']);

					# Recorrer delegado
					if(count($sector['delegados']) > 0) {
						foreach ($sector['delegados'] as $delegado) {
							# Poner contenido del delegado
							$sheet->setCellvalue('C'.$fila_inicio,$delegado['productor']['persona']['fullname']);
							$sheet->setCellvalue('D'.$fila_inicio,$delegado['fecha_inicio_cargo']);
							$sheet->setCellvalue('E'.$fila_inicio,($delegado['fecha_fin_cargo'] != null) ? $delegado['fecha_fin_cargo'] : 'Aún es delegado');
							$fila_inicio++;
						}
					}else{
						$sheet->mergeCells('C'.$fila_inicio.':E'.$fila_siguiente);
						$sheet->setCellvalue('C'.$fila_inicio,'no se asignado delegado para este sector...');
						$sheet->cells('C'.$fila_inicio, function($cells) {$cells->setAlignment('center');});
						$fila_inicio++;
					}
				}

				$sheet->cells('A8:B'.($fila_inicio-1), function($cells) {$cells->setAlignment('center');});
				$sheet->getStyle('A8:A'.($fila_inicio-1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$sheet->getStyle('A8:A'.($fila_inicio-1))->getAlignment()->setWrapText(true);

				$sheet->cells('D8:D'.($fila_inicio-1), function($cells) {$cells->setAlignment('center');});
				$sheet->cells('E8:E'.($fila_inicio-1), function($cells) {$cells->setAlignment('center');});
			});
		})->download('xls');
	}

	public static function excelInspectorConParcelas($data)
	{
		try {
            Excel::create('Reporte', function ($excel) use ($data) {

                $excel->sheet('Productor', function ($sheet) use ($data) {

                    $objDrawing = new PHPExcel_Worksheet_Drawing;
                    $objDrawing->setPath(public_path('logo.png')); //your image path
                    $objDrawing->setCoordinates('A2');
                    $objDrawing->setWorksheet($sheet);

                    $sheet->mergeCells('A2:K2');
                    $sheet->setCellvalue('A2', 'ASOCIACION DE PEQUEÑOS PRODUCTORES DE BANANO ORGANICO SAMAN Y ANEXOS');
                    $sheet->cells('A2:K2', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                            'bold'   => true,
                        ));
                    });
                    $sheet->cell('A2', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('A3:K3');
                    $sheet->setCellvalue('A3', 'Sector Nueva Esperanza s/n Samán ,sullana Perú-Telef.: 504119');
                    $sheet->cells('A3:K3', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                            'bold'   => false,
                        ));
                    });
                    $sheet->cell('A3', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('A4:K4');
                    $sheet->setCellvalue('A4', mb_strtoupper($data['inspector']->fullname));
                    $sheet->cells('A4:K4', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                            'bold'   => true,
                        ));
                    });

                    $sheet->cell('A4', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'medium', 'thin');
                    });

                    $sheet->setCellvalue('A5', 'CÓDIGO PRODUCTOR');
                    $sheet->setCellvalue('B5', 'APELLIDOS Y NOMBRES');
                    $sheet->setCellvalue('C5', 'DNI');
                    $sheet->setCellvalue('D5', 'FECHA DE INGRESO');
                    $sheet->setCellvalue('E5', 'DIRECCIÓN');
                    $sheet->setCellvalue('F5', 'CÓDIGO CAJA');
                    $sheet->setCellvalue('G5', 'N° PARCELA');
                    $sheet->setCellvalue('H5', 'HA CULTIVADA');
                    $sheet->setCellvalue('I5', 'AREA SEMBRADA');
                    $sheet->setCellvalue('J5', 'EMPACADORA');
                    $sheet->setCellvalue('K5', 'SECTOR');

                    $sheet->setColumnFormat(array('C' => '0'));

                    $fila_inicio = 6;

                    $productores = $data['productores'];

                    foreach ($productores as $productor) {

                        $fila_sgte = $fila_inicio + count($productor['parcelas']) - 1;

                        $sheet->mergeCells('A' . $fila_inicio . ':A' . $fila_sgte);
                        $sheet->setCellvalue('A' . $fila_inicio, $productor['codigo_productor']);

                        $sheet->mergeCells('B' . $fila_inicio . ':B' . $fila_sgte);
                        $sheet->setCellvalue('B' . $fila_inicio, $productor['fullname']);

                        $sheet->mergeCells('C' . $fila_inicio . ':C' . $fila_sgte);
                        $sheet->setCellvalue('C' . $fila_inicio, $productor['dni']);

                        $sheet->mergeCells('D' . $fila_inicio . ':D' . $fila_sgte);
                        $sheet->setCellvalue('D' . $fila_inicio, $productor['fecha_ingreso']);

                        $sheet->mergeCells('E' . $fila_inicio . ':E' . $fila_sgte);
                        $sheet->setCellvalue('E' . $fila_inicio, $productor['direccion']);

                        $fila_parcela = $fila_inicio;
                        foreach ($productor['parcelas'] as $parcela) {
                            $sheet->setCellvalue('F' . $fila_parcela, $parcela['codigo_parcela']);
                            $sheet->setCellvalue('G' . $fila_parcela, $parcela['numero_parcela']);
                            $sheet->setCellvalue('H' . $fila_parcela, number_format($parcela['ha_total'], 2) . ' Ha');
                            $sheet->setCellvalue('I' . $fila_parcela, number_format($parcela['ha_sembrada'], 2) . ' Ha');
                            $sheet->setCellvalue('J' . $fila_parcela, \App\Empacadora::find($parcela['id_empacadora'])->nombre);
                            $sheet->setCellvalue('K' . $fila_parcela, \App\Sector::find($parcela['id_sector'])->nombre);
                            $fila_parcela++;
                        }

                        $fila_inicio = $fila_sgte + 1;
                    }

                    $sheet->cells('A6:K'.$fila_inicio, function ($cells) {
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                        ));
                    });
                    // Ajustar y alinear Texto
                    $sheet->getStyle('A6:L' . $fila_inicio)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $sheet->cells('A6:K' . $fila_inicio, function ($cells) {$cells->setAlignment('center');});

                    $sheet->cells('B6:B' . $fila_inicio, function ($cells) {$cells->setAlignment('left');});

                    $sheet->getStyle('A5:K5')->getAlignment()->setWrapText(true);

                    $sheet->cells('A5:K5', function ($cells) {
                        $cells->setBackground('#808B96');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '8',
                            'bold'   => true,
                        ));
                        //$cells->setTextRotation(90);
                    });

                    $sheet->setBorder('A5:K'.($fila_inicio - 1),'thin');

                    $sheet->setBorder('A5:K5', 'thin');
                    $sheet->setAutoFilter('A5:K5');
                    $sheet->setFreeze('A6');
                    $sheet->setWidth(array(
                        'A' => 16, 'B' => 39,  'C' => 9.86, 'D' => 11.57, 'E' => 35, 'F' => 8, 'G' => 7, 'H' => 9, 'I' => 9, 'J' => 14, 'K' => 11,
                    ));

                });

            })->export('xls');

        } catch (Exception $ex) {
            return $ex->getMessage();
        }
	}

	public static function excelSIC($data)
	{
		try {

            Excel::create('ReporteSIC', function ($excel) use ($data) {

				
				$excel->sheet('FICHA_1', function ($sheet) use ($data) {

                    $objDrawing = new PHPExcel_Worksheet_Drawing;
                    $objDrawing->setPath(public_path('assets/logo_mini.png')); //your image path
                    $objDrawing->setCoordinates('A1');
                    $objDrawing->setWorksheet($sheet);

                    $sheet->mergeCells('J1:K1');
                    $sheet->setCellvalue('J1', 'VISITA N°');
                    $sheet->cells('J1:K1', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'bold'   => true,
                        ));
                    });
                    $sheet->cell('J1', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });

                    $sheet->mergeCells('L1:M1');
                    $sheet->setCellvalue('L1', $data['visita']['id']);
                    $sheet->cells('L1:M1', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontColor('#ff0000');
                        $cells->setFont(array(
                            'bold'   => true,
                        ));
                    });
                    $sheet->cell('L1', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });

                    $sheet->mergeCells('D2:M2');
                    $sheet->setCellvalue('D2', 'SISTEMA INTERNO DE CONTROL - COOPERATIVA AGRARIA APPBOSA');
                    $sheet->cells('D2:M2', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'bold'   => true,
                        ));
                    });
                    $sheet->cell('D2', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'thin', 'medium');
                    });

                    $sheet->mergeCells('D3:M3');
                    $sheet->setCellvalue('D3', 'AUDITORIA INTERNA - SIC');
                    $sheet->cells('D3:M3', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'bold'   => true,
                        ));
                    });
                    $sheet->cell('D3', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'medium', 'medium');
                    });

                    $sheet->mergeCells('A5:C5');
                    $sheet->setCellvalue('A5', 'NOMBRE DE PRODUCTOR:');
                    $sheet->cell('A5', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'thin', 'medium');
                    });

                    $sheet->mergeCells('D5:F5');
                    $sheet->setCellvalue('D5', mb_strtoupper($data['visita']['productor']));
                    $sheet->cell('D5', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('G5:H5');
                    $sheet->setCellvalue('G5', 'RESPONSABLE DE CAMPO:');
                    $sheet->cell('G5', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('I5:M5');
                    $sheet->setCellvalue('I5', mb_strtoupper($data['visita']['responsable']));
                    $sheet->cell('I5', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('A6:C6');
                    $sheet->setCellvalue('A6', 'CÓDIGO DEL PRODUCTOR:');
                    $sheet->cell('A6', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'medium');
                    });

                    $sheet->mergeCells('D6:F6');
                    $sheet->setCellvalue('D6', $data['visita']['codigo_productor']);
                    $sheet->cell('D6', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('G6:H6');
                    $sheet->setCellvalue('G6', 'FECHA DE AUDITORIA:');
                    $sheet->cell('G6', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('I6:M6');
                    $sheet->setCellvalue('I6', $data['visita']['fecha']);
                    $sheet->cell('I6', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('A7:C7');
                    $sheet->setCellvalue('A7', 'INSPECTOR RESPONSABLE:');
                    $sheet->cell('A7', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'medium');
                    });

                    $sheet->mergeCells('D7:F7');
                    $sheet->setCellvalue('D7', mb_strtoupper($data['visita']['inspector']));
                    $sheet->cell('D7', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('G7:H7');
                    $sheet->setCellvalue('G7', 'ÁREA:');
                    $sheet->cell('G7', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('I7:M7');
                    $sheet->setCellvalue('I7', number_format($data['visita']['area_parcela'],2).' HA');
                    $sheet->cell('I7', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('A8:C8');
                    $sheet->setCellvalue('A8', 'EMPACADORA:');
                    $sheet->cell('A8', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'medium', 'medium');
                    });

                    $sheet->mergeCells('D8:F8');
                    $sheet->setCellvalue('D8', mb_strtoupper($data['visita']['empacadora']));
                    $sheet->cell('D8', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'medium', 'thin');
                    });

                    $sheet->mergeCells('G8:H8');
                    $sheet->setCellvalue('G8', 'SECTOR');
                    $sheet->cell('G8', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'medium', 'thin');
                    });

                    $sheet->mergeCells('I8:M8');
                    $sheet->setCellvalue('I8', mb_strtoupper($data['visita']['sector']));
                    $sheet->cell('I8', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'medium', 'thin');
                    });

                    $sheet->cells('A5:I8', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'bold'   => true,
                        ));
                    });

                    $sheet->cells('D5:D8', function ($cells) {
                        $cells->setFont(array(
                            'bold' => false
                        ));
                    });

                    $sheet->cells('I5:I8', function ($cells) {
                        $cells->setFont(array(
                            'bold' => false
                        ));
                    });
                    

                    $sheet->setCellvalue('A10', 'N°');
                    $sheet->cell('A10', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });

                    $sheet->mergeCells('B10:G10');
                    $sheet->setCellvalue('B10', 'PUNTO A EVALUAR');
                    $sheet->cell('B10', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'medium', 'thin');
                    });

                    $sheet->setCellvalue('H10', 'CUMPLE CON LAS NORMAS');
                    $sheet->cell('H10', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'medium', 'thin');
                    });

                    $sheet->mergeCells('I10:M10');
                    $sheet->setCellvalue('I10', 'OBSERVACIONES');
                    $sheet->cell('I10', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'medium', 'thin');
                    });

                    $sheet->cells('A10:M10', function ($cells) {
                    	$cells->setBackground('#808B96');
                        $cells->setFont(array(
                            'bold'   => true
                        ));
                    });

                    $fila_inicio = 11;

                    foreach ($data['formulario'] as $key => $pregunta) {

                    	$sheet->setCellvalue('A'.$fila_inicio, ($fila_inicio - 10));
	                    $sheet->cell('A'.$fila_inicio, function ($cell) {
	                        $cell->setBorder('thin', 'medium', 'thin', 'medium');
	                    });

	                    $sheet->mergeCells('B'.$fila_inicio.':G'.$fila_inicio);
	                    $sheet->setCellvalue('B'.$fila_inicio, mb_strtoupper($pregunta['titulo']));
	                    $sheet->cell('B'.$fila_inicio, function ($cell) {
	                        $cell->setBorder('thin', 'medium', 'thin', 'medium');
	                    });

	                    $sheet->setCellvalue('H'.$fila_inicio, mb_strtoupper($pregunta['respuesta']));
	                    $sheet->cell('H'.$fila_inicio, function ($cell) {
	                        $cell->setBorder('thin', 'medium', 'thin', 'medium');
	                    });

	                    $sheet->mergeCells('I'.$fila_inicio.':M'.$fila_inicio);
	                    $sheet->setCellvalue('I'.$fila_inicio, mb_strtoupper($pregunta['observacion']));
	                    $sheet->cell('I'.$fila_inicio, function ($cell) {
	                        $cell->setBorder('thin', 'medium', 'thin', 'medium');
	                    });

	                    $fila_inicio++;
                    }

                    $sheet->cells('A10:M'.($fila_inicio-1), function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                    });

                    $sheet->cells('B11:B'.($fila_inicio-1), function ($cells) {
                        $cells->setAlignment('left');
                    });

                    $sheet->cells('I11:I'.($fila_inicio-1), function ($cells) {
                        $cells->setAlignment('left');
                    });

                    $sheet->setAutoSize(true);
                    $sheet->mergeCells('A'.$fila_inicio.':M'.$fila_inicio);
                    $sheet->cell('A'.$fila_inicio, function ($cell) {
                        $cell->setBorder('medium', 'none', 'none', 'none');
                    });

                    $fila_inicio++;

                    $sheet->mergeCells('A'.$fila_inicio.':C'.$fila_inicio);
                    $sheet->setCellvalue('A'.$fila_inicio, 'OBSEVACIONES GENERALES');
                    $sheet->cell('A'.$fila_inicio, function ($cell) {
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cells('A'.$fila_inicio.':B'.$fila_inicio, function ($cells) {
                    	$cells->setFont(array(
                            'bold'   => true
                        ));
                    });

                    $sheet->cells('A'.$fila_inicio.':M'.($fila_inicio), function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                    });

                    $fila_inicio++;

                    $sheet->mergeCells('A'.$fila_inicio.':M'.($fila_inicio + 2));
                    $sheet->setCellvalue('A'.$fila_inicio, mb_strtoupper($data['visita']['observacion']));
                    $sheet->cell('A'.$fila_inicio, function ($cell) {
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });

                    $sheet->cells('A'.$fila_inicio.':M'.($fila_inicio), function ($cells) {
                        $cells->setValignment('center');
                    });

                    $sheet->cells('A1:M'.($fila_inicio + 2), function ($cells) {
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '9'
                        ));
                    });

                    $sheet->getStyle('A10:M'.$fila_inicio)->getAlignment()->setWrapText(true);
                    
                    // Ajustar y alinear Texto
                    // $sheet->getStyle('A6:L' . $fila_inicio)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    // $sheet->cells('A6:K' . $fila_inicio, function ($cells) {$cells->setAlignment('center');});

                    // $sheet->cells('B6:B' . $fila_inicio, function ($cells) {$cells->setAlignment('left');});

                    // $sheet->getStyle('A5:K5')->getAlignment()->setWrapText(true);

                    $sheet->setWidth(array(
                        'A' => 4.71, 'B' => 10.71,  'C' => 18, 'D' => 10.71, 'E' => 14, 'F' => 18, 'G' => 10.71, 'H' => 10.71, 'I' => 10.71, 'J' => 10.71, 'K' => 10.71,'L' => 10.71, 'M' => 10.71
                    ));

                });

            })->export('xls');

        } catch (Exception $ex) {
            return $ex->getMessage();
        }
	}

	public static function obtenerEmpacadoraExcelEnfunde()
    {
        try {
            $data = self::obtenerEmpacadoraDataEnfunde();
            Excel::create('Reporte_ enfundes', function ($excel) use ($data) {

                $excel->sheet('Productor', function ($sheet) use ($data) {

                    $objDrawing = new PHPExcel_Worksheet_Drawing;
                    $objDrawing->setPath(public_path('logo-xs.png')); //your image path
                    $objDrawing->setCoordinates('A1');
                    $objDrawing->setWorksheet($sheet);

                    $sheet->mergeCells('C1:K1');
                    $sheet->setCellvalue('C1', 'ASOCIACION DE PEQUEÑOS PRODUCTORES DE BANANO ORGANICO SAMAN Y ANEXOS');
                    $sheet->cells('C1:K1', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                            'bold'   => true,
                        ));
                    });
                    $sheet->cell('C1', function ($cell) {
                        $cell->setBorder('medium', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('C2:K2');
                    $sheet->setCellvalue('C2', 'Sector Nueva Esperanza s/n Samán ,sullana Perú-Telef.: 504119');
                    $sheet->cells('C2:K2', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                            'bold'   => false,
                        ));
                    });
                    $sheet->cell('C2', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'thin', 'thin');
                    });

                    $sheet->mergeCells('C3:K3');
                    $sheet->setCellvalue('C3', 'REPORTE DE ENFUNDE POR EMPACADORA');
                    $sheet->cells('C3:K3', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10',
                            'bold'   => true,
                        ));
                    });

                    $sheet->cell('C3', function ($cell) {
                        $cell->setBorder('thin', 'medium', 'medium', 'thin');
                    });

                    $letras = ['C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

                    #empiezo llenar cabezeras

                    $sheet->setCellvalue('A4', 'EMPACADORA');
                    $sheet->setCellvalue('B4', 'N° PARCELAS');

                    $sheet->cells('A4:B4', function ($cells) {
                        $cells->setBackground('#808B96');
                        $cells->setFont(array(
                            'bold'   => true,
                        ));
                    });

                    $indexSemana = 0;

                    foreach ($data['semanas'] as $key => $semana) {
                        $sheet->setCellvalue($letras[$key].'4', $semana['nombre']);

                        $sheet->cells($letras[$key] . '4:' . $letras[$key] . '4', function ($cells) use ($semana){
                            $cells->setBackground($semana['cinta_color']);
                            $cells->setFont(array(
                                'bold'   => true,
                            ));
                        });

                        $indexSemana = $key;
                    }
                    $sheet->setAutoFilter('A4:'.$letras[$indexSemana].'4');

                    #final llenar cabezeras
                    #empiezo a llenar data por semana
                    $totales = [];//para calcular los totales de final de tabla
                    $fila_inicio = 5;
                    foreach ($data['empacadoras'] as $i => $empacadora) {

                        $cont = 0;
                        $totales[$cont] = ( $i === 0) ? $empacadora['cantidad_parcelas'] : $totales[$cont]+$empacadora['cantidad_parcelas'];

                        $sheet->setCellvalue('A'.$fila_inicio, $empacadora['nombre']);
                        $sheet->setCellvalue('B'.$fila_inicio, $empacadora['cantidad_parcelas']);

                        $cont++;

                        foreach ($empacadora['data'] as $j => $data) {
                            $sheet->setCellvalue($letras[$j].$fila_inicio, $data['total_enfunde'] . '/' . $data['total_cosecha']);

                            $totales[$cont] = ( $i === 0) ? $data['total_enfunde'] : $totales[$cont]+$data['total_enfunde'];

                            $cont ++;
                        }

                        $fila_inicio++;
                    }
                    #final llenar data de semanas por empacadora

                    // $sheet->setColumnFormat(array('E' => '0.00'));
                    // $sheet->setColumnFormat(array('C' => '00000000'));

                    #empiezo a llenar data de totales
                    $sheet->setCellvalue('A'.$fila_inicio, 'TOTALES');
                    $sheet->setCellvalue('B'.$fila_inicio, $totales[0]);
                    $sheet->setCellvalue('A'.($fila_inicio + 1), 'CAJAS (r=0.8)');
                    $sheet->setCellvalue('B'.($fila_inicio + 1), '-');
                    $sheet->setCellvalue('A'.($fila_inicio + 2), 'CONTENEDORES (c=1145)');
                    $sheet->setCellvalue('B'.($fila_inicio + 2), '-');

                    foreach (array_slice($totales, 1) as $key => $total) {
                        $sheet->setCellvalue($letras[$key] . $fila_inicio, $total);
                        $sheet->setCellvalue($letras[$key] . ($fila_inicio + 1), number_format(($total*0.8),2));
                        $sheet->setCellvalue($letras[$key] . ($fila_inicio + 2), number_format((($total*0.8)/1080),2));
                    }

                    $sheet->cells('A' . $fila_inicio . ':' . $letras[$indexSemana] . ($fila_inicio + 2), function ($cells){
                        $cells->setBackground('#5DADE2');
                        $cells->setFont(array(
                            'bold'   => true,
                        ));
                    });
                    #final de llenado de totales

                    $fila_inicio += 2;

                    #para toda las celdas de la data
                    $sheet->cells('A4:'. $letras[$indexSemana] . $fila_inicio, function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'size'   => '10'
                        ));
                    });

                    
                    $sheet->setBorder('A4:'. $letras[$key] . $fila_inicio, 'thin');
                    
                    $sheet->setFreeze('C5');
                    $sheet->setWidth(array(
                        'A' => 20, 'B' => 12
                    ));

                });

            })->export('xls');

        } catch (\Exception $ex) {
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }



}