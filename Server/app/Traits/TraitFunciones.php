<?php

namespace App\Traits;

use DB;
use App\Anio;
use App\Semana;
use App\Operador;
use App\Contenedor;
use App\EstadoArchivo;

trait TraitFunciones
{

    public static function getCantidadArchivos($tabla,$id_cliente)
    {
        // return count(DB::table($tabla)->where('id_contenedor',$id_contenedor)->get()) + 1;
        return count(DB::table($tabla)->where('id_cliente',$id_cliente)->get()) + 1;
    }

    public static function getCantidadPackings($tabla,$id_contenedor)
    {
        return count(DB::table($tabla)->where('id_contenedor',$id_contenedor)->get()) + 1;
    }

    public static function getCantidadOtrosArchivos()
    {
        return count(DB::table('otros_archivos')->get()) + 1;
    }

    public static function getNameFile($id_contenedor,$extension)
    {
        $nombre = $id_contenedor.'.'.$extension;
        return $nombre;
    }

    public static function getIdAnioActual()
    {
        return Anio::where('anio',date('Y'))->select('id')->first()->id;
    }

    public static function getAnio($id_anio)
    {
        return Anio::find($id_anio)->anio;
    }

    // Obtener el id del cargo cliente
    public static function getIdCargoCliente()
    {
        $cargo_cliente = DB::table('cargo')->select('id')->where('nombre','LIKE','%clien%')->where('activo',1)->first();
        return $cargo_cliente->id;
    }

    // obtenero el id del rol cliente
    public static function getIdRolCliente()
    {
        $rol_cliente = DB::table('rol')->select('id')->where('nombre','LIKE','%clien%')->where('activo',1)->first();
        return $rol_cliente->id;
    }

    // Obtener el id del cargo operador
    public static function getIdCargoOperador()
    {
        $cargo_operador = DB::table('cargo')->select('id')->where('nombre','LIKE','%operador%')->where('activo',1)->first();
        return $cargo_operador->id;
    }

    // obtenero el id del rol operador
    public static function getIdRolOperador()
    {
        $rol_operador = DB::table('rol')->select('id')->where('nombre','LIKE','%operador%')->where('activo',1)->first();
        return $rol_operador->id;
    }

    public static function getIdSemanaActual()
    {
        $id_anio_actual   = self::getIdAnioActual();
        $id_semana_actual = Semana::where('id_anio',$id_anio_actual)->where('activo',1)->max('id');
        return $id_semana_actual;
    }

    // Función para poner en minusculas y quitar espacios vacios al slug
    public function trimLower($slug)
    {
        return strtolower(trim(str_replace(' ','',$slug)));
    }

    // obtener el nombre del Operador, se hizo así porque la tabla no se relacionaba
    public static function getNameOperador($id_operador)
    {
        $operador = Operador::find($id_operador);
        return $operador->nombre;
    }

    // ---------------------- Cambiar es estado los los contenedores----------------------------------
    public static function changeStatusContenedor($estado,$tipo_archivo,$id_cliente,$id_semana)
    {
        try{
            DB::beginTransaction();
                $contenedores = Contenedor::where('id_cliente',$id_cliente)
                                            ->where('id_semana',$id_semana)
                                            ->where('activo',1)
                                            // ->where($tipo_archivo,!$estado)
                                            ->get();
                foreach ($contenedores as $value) {
                    $contenedor = Contenedor::find($value->id);
                    $contenedor->fill([$tipo_archivo=>DB::raw($estado)])->save();
                }
            DB::commit();
        }catch(\Exception $ex){
            DB::rollback();
        }
    }
    // ---------------------- Fin Cambiar es estado(pendiente,emitido) para los contenedores----------------------------------

}