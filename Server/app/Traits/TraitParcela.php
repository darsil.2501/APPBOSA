<?php

namespace App\Traits;
use App\Enfunde;
use App\Parcela;
use App\Productor;
use DB;

trait TraitParcela
{
    # Obtener las parcelas de un determinado productor para la búsqueda pública
	public static function obtenerParcelasPorProductor($productor)
    {
        $data = array();
        foreach ($productor->parcelas as $parcela) {
            $data[] = array(
                'id'                    => $parcela->id,
                'id_empacadora'         => $parcela->empacadora->id,
                'nombre_empacadora'     => $parcela->empacadora->nombre,
                'id_sector'             => $parcela->sector->id,
                'nombre_sector'         => $parcela->sector->nombre,
                'ha_total'              => $parcela->ha_total,
                'ha_sembrada'           => $parcela->ha_sembrada,
                'numero_parcela'        => $parcela->numero_parcela,
                'codigo_parcela'        => $parcela->codigo_parcela,
                'tipo_propiedad'        => $parcela->tipo_propiedad,
                'ggn'                   => $parcela->ggn,
                'nombre_tipo_propiedad' => ($parcela->tipo_propiedad == 1) ? 'PROPIA' : 'ARRENDADA',
                'id_inspector'          => $parcela->inspector->id, 
                'nombre_inspector'      => $parcela->inspector->persona->fullname, 
                'deshije'               => $parcela->deshije,
                'nombre_deshije'        => ($parcela->deshije == 1) ? 'SÍ' : 'NO',
                'nombre_estado_parcela' => ($parcela->estado_parcela == 1) ? 'ACTIVA' : 'INACTIVA',
                'color_estado_parcela'  => ($parcela->estado_parcela == 1) ? '#2ECC71' : '#E74C3C',
                'enable'                => ($parcela->estado_parcela == 1) ? true : false
            );
        }
        return $data;
    }

    # Obtener las parcelas de un determinado productor para exportar PDF
    public static function obtenerParcelasPorProductorPDF($productor)
    {
        $data_parcela = array();
        foreach ($productor->parcelas as $value) {
            $parcela = new \stdClass;
            $parcela->ha_total              = $value->ha_total;
            $parcela->ha_sembrada           = $value->ha_sembrada;
            $parcela->numero_parcela        = $value->numero_parcela;
            $parcela->codigo_parcela        = $value->codigo_parcela;
            $parcela->nombre_tipo_propiedad = ($value->tipo_propiedad == 1) ? 'PROPIA' : 'ARRENDADA';
            $parcela->empacadora            = $value->empacadora->nombre;
            $parcela->sector                = $value->sector->nombre;
            $data_parcela[]                 = $parcela;
        }
        return $data_parcela;
    }

    public static function registrarParcela($request)
    {
        try {
            DB::beginTransaction();
                $numero_parcela = Parcela::where('id_productor',$request->id_productor)->orderBy('numero_parcela', 'DESC')->first();
                $numero_parcela = ($numero_parcela != null) ? $numero_parcela->numero_parcela + 1 : 1;
                $request['numero_parcela'] = $numero_parcela;
                $parcela = Parcela::create($request->all());
                // self::auditar('audit_parcela',$parcela->productor->persona->fullname.'-'.$parcela->numero_parcela,'INSERTAR');
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Parcela Creada Correctamente.']);
        } catch (Exception $ex) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public static function actualizarParcela($request, $parcela_id)
    {
        try {
            DB::beginTransaction();
                $parcela = Parcela::find($parcela_id);
                if ($parcela) { $parcela->fill($request->all())->save();}
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Parcela Editada Correctamente.']);
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public static function eliminarParcela($id)
    {
        try {
            DB::beginTransaction();
                $parcela = Parcela::find($id);
                // self::auditar('audit_parcela',$parcela->productor->persona->fullname.'-'.$parcela->numero_parcela,'ELIMINAR');
                $parcela->delete();
            DB::commit();
            return response()->json(["message" => "Registro eliminado correctamente", "success" => true]);

        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }

    public static function obtenerInformacionParcela($request)
    {
        try {
            $parcelas = Parcela::where('productor_id', '=', $request['productor_id'])
                ->where('empacadora_id', '=', $request['empacadora_id'])
                ->orderBy('numero_parcela', 'asc')->get();

            foreach ($parcelas as $parcela) {
                $enfunde = Enfunde::where('semana_id', '=', $request['semana_id'])
                    ->where('parcela_id', '=', $parcela->id)
                    ->first();

                $parc['id']                 = $parcela->id;
                $parc['sector_id']          = $parcela->sector->id;
                $parc['sector']             = $parcela->sector->nombre;
                $parc['empacadora_id']      = $parcela->empacadora->id;
                $parc['empacadora']         = $parcela->empacadora->nombre;
                $parc['ha_total']           = $parcela->ha_total;
                $parc['ha_sembrada']        = $parcela->ha_sembrada;
                $parc['codigo_parcela']     = $parcela->codigo_parcela;
                $parc['numero_parcela']     = $parcela->numero_parcela;
                $parc['racimas_enfundadas'] = ($enfunde != null) ? $enfunde->racimas_enfundadas : null;
                $parc['enfunde_id']         = ($enfunde != null) ? $enfunde->id : null;
                $data[]                     = $parc;
            }
            return response()->json(["success" => true, "info" => $data]);
        } catch (\Exception $ex) {
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }

    # Cambiar el estado de las parcelas de un productor cuando es sancionado parcial o total
    public static function habilitarParcela($request)
    {
        try{
            DB::beginTransaction();
                Parcela::find($request->id_parcela)->update(['estado_parcela'=>DB::raw(1)]);
            DB::commit();
            return response()->json(["success" => true, "message" => 'Parcela habilitada correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }

    # Cambiar el estado de las parcelas de un productor cuando es sancionado parcial o total
    public static function deshabilitarParcela($request)
    {
        try{
            DB::beginTransaction();
                Parcela::find($request->id_parcela)->update(['estado_parcela'=>DB::raw(0)]);
                $productor = Productor::find($request->id_productor)->update(['id_condicion'=>DB::raw($request->id_condicion)]);
            DB::commit();
            return response()->json(["success" => true, "message" => 'Parcela deshabilitada correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }

    # Listar los codigos de cada parcela
    public static function getCodigosParcela()
    {
        try{
            $parcelas = Parcela::where('activo',1)->get();

            $codigos = array();
            foreach ($parcelas as $key => $parcela) {
                if ($parcela->codigo_parcela != "" && $parcela->codigo_parcela != null) {
                    $codigos[] = array(
                        'id_parcela' => $parcela->id,
                        'numero'     => $parcela->codigo_parcela,
                        'productor'  => $parcela->productor->persona->fullname
                    );
                }
            }
               
            return response()->json(["success" => true, "info" => $codigos]);
        }catch(\Exception $ex){
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }
    }
}