<?php
namespace App\Traits;

use DB;
use App\Recobro;
use App\Empacadora;

trait TraitRecobro
{

    public function getDataRecobro($id_semana)
    {
        try{

            $empacadorasTemp = Empacadora::where('activo',1)->get();

            $empacadoras = array();
            foreach ($empacadorasTemp as $empacadora) {
                $cosecha = Recobro::where('id_semana', $id_semana)->where('id_empacadora', $empacadora->id)->where('activo', 1)->first();

                $empacadoras[] = array(
                    'id'                 => $empacadora->id,
                    'nombre'             => $empacadora->nombre,
                    'ubicacion'          => $empacadora->ubicacion,
                    'total_parcelas'     => count($empacadora->parcelas),
                    'racimas_cosechadas' => ($cosecha) ? $cosecha->racimas_cosechadas : 0,
                    'id_recobro' => ($cosecha) ? $cosecha->id : null
                );
            }
             
            return response()->json(['info'=>$empacadoras,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al listar los registros'.$e->getMessage(),'success'=>false]);    
        }
    }

    public function createRecobro($request)
    {
        try{
            DB::beginTransaction();

                # Validar que no si está el registro con la combinación de empacadora y semana.
                $registro = Recobro::where(['id_empacadora'=> $request['id_empacadora'],'id_semana'=> $request['id_semana']])->first();
                //return $value['semana'];
                if ($registro == null) {
                    $enfunde = Recobro::create([
                        'racimas_cosechadas' => $request['racimas_agregar'],
                        'id_semana'          => $request['id_semana'],
                        'id_empacadora'      => $request['id_empacadora']
                    ]);
                    DB::commit();
                    return response()->json(["message" => "Registro creado correctamente", "success" => true]);
                } else {
                    $registro = Recobro::find($request['id']);
                    $registro->fill([
                        'racimas_cosechadas' => ($registro->racimas_cosechadas + $request['racimas_agregar'])
                    ])->save();
                }

            DB::commit();             
            return response()->json(['message'=>'Registro Insertado Correctamente','success'=>true]);    
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message'=>'Error al insertar registro '.$e->getMessage(),'success'=>false]);    
        }
    }

    public function updateRecobro($request, $recobro_id)
    {
        try{
            DB::beginTransaction();
            
                $recobro = Recobro::find($recobro_id);
                $recobro->fill([
                    'atr' =>  $request['atr'],
                ])->save();

            DB::commit();             
            return response()->json(['message'=>'Registro Actualizado Correctamente','success'=>true]);    
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message'=>'Error al actualizar registro '.$e->getMessage(),'success'=>false]);    
        }
    }
        
    public function deleteRecobro($recobro_id)
    {
        try{
            DB::beginTransaction();
                $recobro = Recobro::find($recobro_id);
                $recobro->delete();
            DB::commit();             
            return response()->json(['message'=>'Registro Eliminado Correctamente','success'=>true]);    
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message'=>'Error al eliminar registro '.$e->getMessage(),'success'=>false]);    
        }
    }

}