<?php

namespace App\Traits;
use App\OtrosArchivos;
use DB;

trait TraitOtrosArchivos
{
	public static function getOtrosArchivos($id_semana)
	{
		$data = OtrosArchivos::where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        $archivos = array();
        foreach ($data as $value) {
            $archivos[] = array(
                'id'         => $value->id,
                'id_semana'  => $value->semana->id,
                'semana'     => $value->semana->nombre,
                'nombre'     => $value->nombre,
                'fecha'      => $value->fecha,
                'hora'       => $value->hora,
                'url'        => $value->url,
                'tipo'       => $value->tipo,
            );
        }
        return $archivos;
	}
}