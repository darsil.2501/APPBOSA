<?php
namespace App\Traits;

use DB;
use App\Llenado;
use App\Contenedor;
use App\Parcela;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Alignment;
use PHPExcel_Worksheet_Drawing;

trait TraitLlenado
{

    public function getDataLlenado($id_semana)
    {
        try{

            $contenedores_query = Contenedor::where('id_semana',$id_semana)->where('llenado',1)->where('activo',1)->get();

            $contenedores = array();

            foreach ($contenedores_query as $contenedor) {
                $cajas = array();
                foreach ($contenedor->cajas as $value) {
                    $cajas []           = array(
                        'id'               => $value->id,
                        'marca'            => $value->marca,
                        'cantidad_cajas'   => $value->pivot->cantidad
                    );
                }
                $contenedores[] = array(
                    'cajas'                  => $cajas,
                    'id'                     => $contenedor->id,
                    'id_semana'              => $contenedor->id_semana,
                    'semana'                 => $contenedor->semana->nombre,
                    'cliente'                => $contenedor->cliente->razon_social,
                    'email_cliente'          => $contenedor->cliente->email,
                    'referencia'             => $contenedor->referencia,
                    'booking'                => $contenedor->booking,
                    'numero_contenedor'      => $contenedor->numero_contenedor,
                    'id_lineanaviera'        => $contenedor->id_lineanaviera,
                    'linea_naviera'          => $contenedor->linea_naviera->nombre,
                    'sigla_linea_naviera'    => $contenedor->linea_naviera->sigla,
                    'nave'                   => $contenedor->nave,
                    'viaje'                  => $contenedor->viaje,
                    'puerto_destino'         => $contenedor->puerto_destino->nombre,
                    'operador'               => $contenedor->operador->nombre,
                    'dia_proceso_inicio'     => $contenedor->fecha_proceso_inicio
                );
            }
             
            return response()->json(['info'=>$contenedores,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al listar los registros'.$e->getMessage(),'success'=>false]);    
        }
    }

    public function createLlenado($request)
    {
        //return $request->all();
        try{
            DB::beginTransaction();

                foreach ($request['pallets'] as $i => $pallet) {
                    foreach ($pallet as $j => $item) {
                        Llenado::create([
                            'cajas'         =>  $item['cajas'],
                            'pallet'        =>  ($i+1),
                            'id_parcela'    =>  $item['parcela']['id_parcela'],
                            'id_contenedor' =>  $request['id_contenedor']
                        ]);
                    }
                }

                $contenedor = Contenedor::find($request['id_contenedor']);
                $contenedor->fill(['llenado'=>DB::raw(1)])->save();

            DB::commit();             
            return response()->json(['message'=>'Registro Insertado Correctamente','success'=>true]);    
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['message'=>'Error al insertar registro '.$e->getMessage(),'success'=>false]);    
        }
    }

    public function updateLlenado($request, $llenado_id)
    {
        try{
            DB::beginTransaction();
            
                $llenado = Llenado::find($llenado_id);
                $llenado->fill([
                    'atr' =>  $request['atr'],
                ])->save();

            DB::commit();             
            return response()->json(['message'=>'Registro Actualizado Correctamente','success'=>true]);    
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message'=>'Error al actualizar registro '.$e->getMessage(),'success'=>false]);    
        }
    }
        
    public function deleteLlenado($llenado_id)
    {
        try{
            DB::beginTransaction();
                $llenado = Llenado::find($llenado_id);
                $llenado->delete();
            DB::commit();             
            return response()->json(['message'=>'Registro Eliminado Correctamente','success'=>true]);    
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message'=>'Error al eliminar registro '.$e->getMessage(),'success'=>false]);    
        }
    }

    public function reporteExcelLlenado($data)
    {
        try{
            Excel::create('Reporte_Llenado', function ($excel) use ($data) {
                if (mb_strtoupper($data['contenedor']['cliente']) === 'AGROFAIR') {
                    $excel->sheet($data['contenedor']['cliente'], function ($sheet) use ($data) {

                        $objDrawing = new PHPExcel_Worksheet_Drawing;
                        $objDrawing->setPath(public_path('logo-xs.png')); //your image path
                        $objDrawing->setCoordinates('A1');
                        $objDrawing->setWorksheet($sheet);

                        $sheet->mergeCells('C1:Q1');
                        $sheet->setCellvalue('C1', 'ASOCIACION DE PEQUEÑOS PRODUCTORES DE BANANO ORGANICO SAMAN Y ANEXOS');
                        $sheet->cell('C1', function ($cell) {
                            $cell->setBorder('medium', 'medium', 'thin', 'thin');
                        });

                        $sheet->mergeCells('C2:Q2');
                        $sheet->setCellvalue('C2', 'Sector Nueva Esperanza S/N Samán ,Sullana Perú-Telef.: 504119');
                        $sheet->cell('C2', function ($cell) {
                            $cell->setBorder('thin', 'medium', 'thin', 'thin');
                        });

                        $sheet->mergeCells('C3:Q3');
                        $sheet->setCellvalue('C3', 'PACKING LIST');
                        $sheet->cell('C3', function ($cell) {
                            $cell->setBorder('thin', 'medium', 'medium', 'thin');
                        });

                        $sheet->cells('A1:Q4', function ($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFont(array(
                                'family' => 'Calibri',
                                'size'   => '10',
                                'bold'   => true,
                            ));
                        });
                        $sheet->cells('A2:Q2', function ($cells) {
                            $cells->setFont(array(
                                'bold'   => false,
                            ));
                        });

                        $sheet->setColumnFormat(array(
                            'E' => '000000000000',
                            'D' => 'd-mmm'
                        ));

                        $sheet->setCellvalue('A4', 'N° PALLET');
                        $sheet->setCellvalue('B4', 'CODIGOS DE BARRA');
                        $sheet->setCellvalue('C4', 'SEMANA');
                        $sheet->setCellvalue('D4', 'FECHA');
                        $sheet->setCellvalue('E4', 'GGN');
                        $sheet->setCellvalue('F4', 'EMPACADORA');
                        $sheet->setCellvalue('G4', 'PRODUCTOR');
                        $sheet->setCellvalue('H4', 'CODIGO');
                        $sheet->setCellvalue('I4', 'TIPO CAJA');
                        $sheet->setCellvalue('J4', 'CJS/PLT');
                        $sheet->setCellvalue('K4', 'ETIQUETA');
                        $sheet->setCellvalue('L4', 'PLASTICO');
                        $sheet->setCellvalue('M4', 'CONTENEDOR');
                        $sheet->setCellvalue('N4', 'REG. DE TEMPERATURA');
                        $sheet->setCellvalue('O4', 'Nº BOOKING');
                        $sheet->setCellvalue('P4', 'PRECINTO');
                        $sheet->setCellvalue('Q4', 'LOTE');

                        $sheet->setAutoFilter('A4:Q4');
                        $sheet->cells('A4:Q4', function ($cells) {
                            $cells->setBackground('#2E4053');
                            $cells->setFontColor('#ffffff');
                        });

                        $cont = 5;
                        $total_general_cajas = 0;
                        $caja_select = $data['contenedor']['cajas'][0];
                        $tl = 1961;
                        foreach ($data['llenados'] as $i => $llenado) {
                            foreach ($llenado as $j => $pallet) {

                                $parcela = Parcela::find($pallet['id_parcela']);

                                $sheet->setCellvalue('A'.$cont, $tl);
                                $sheet->setCellvalue('C'.$cont, $data['contenedor']['semana']);
                                $sheet->setCellvalue('D'.$cont, $data['contenedor']['dia_proceso_inicio']);
                                $sheet->setCellvalue('E'.$cont, $parcela->ggn);
                                $sheet->setCellvalue('F'.$cont, $parcela->empacadora->nombre);
                                $sheet->setCellvalue('G'.$cont, $parcela->productor->persona->fullname);
                                $sheet->setCellvalue('H'.$cont, $parcela->codigo_parcela);
                                $sheet->setCellvalue('I'.$cont, $caja_select['marca']);
                                $sheet->setCellvalue('J'.$cont, $pallet['cajas']);
                                $total_general_cajas += $pallet['cajas'];
                                $sheet->setCellvalue('M'.$cont, $data['contenedor']['numero_contenedor']);
                                $sheet->setCellvalue('O'.$cont, $data['contenedor']['booking']);

                                //valido si es el ultimo registro de pallet
                                if($j === (count($llenado)-1)){
                                    //si tiene por utlimo 6 o 7 no se hace el desglose sino si
                                    if ($pallet['cajas'] == 6 || $pallet['cajas'] == 7) {
                                        $sheet->setCellvalue('A'.$cont, 'TL '.$tl);
                                    }
                                    else
                                    {
                                        //si tiene 1080 se desglosa 6 cajas para el tl
                                        if ($data['contenedor']['total_cajas']==1080) {
                                            $sheet->setCellvalue('J'.$cont, ($pallet['cajas'] - 6));
                                            $cont++;
                                            $sheet->setCellvalue('J'.$cont, 6);
                                        }
                                        //si tiene 1400 se desglosa 7 cajas para el tl
                                        if ($data['contenedor']['total_cajas']==1400) {
                                            $sheet->setCellvalue('J'.$cont, ($pallet['cajas'] - 7));
                                            $cont++;
                                            $sheet->setCellvalue('J'.$cont, 7);
                                        }
                                        //se llena la data respectiva del tl para que no quede en blanco
                                        $sheet->setCellvalue('A'.$cont, 'TL '.$tl);
                                        $sheet->setCellvalue('C'.$cont, $data['contenedor']['semana']);
                                        $sheet->setCellvalue('D'.$cont, $data['contenedor']['dia_proceso_inicio']);
                                        $sheet->setCellvalue('E'.$cont, $parcela->ggn);
                                        $sheet->setCellvalue('F'.$cont, $parcela->empacadora->nombre);
                                        $sheet->setCellvalue('G'.$cont, $parcela->productor->persona->fullname);
                                        $sheet->setCellvalue('H'.$cont, $parcela->codigo_parcela);

                                        $sheet->setCellvalue('I'.$cont, $caja_select['marca']);

                                        $sheet->setCellvalue('M'.$cont, $data['contenedor']['numero_contenedor']);
                                        $sheet->setCellvalue('O'.$cont, $data['contenedor']['booking']);

                                    }
                                }

                                $cont++;
                            }
                            $sum = 0;
                            if(count($data['contenedor']['cajas']) > 1){
                                foreach ($data['contenedor']['cajas'] as $key => $caja) {
                                    $sum += $caja['cantidad_cajas'];
                                    if ($total_general_cajas >= $sum) {
                                        $caja_select = $data['contenedor']['cajas'][$key+1];
                                        break;
                                    }
                                }
                            }
                            $tl++;
                        }

                        $sheet->setCellvalue('J'.$cont, $total_general_cajas);

                        $sheet->cells('A4:Q'.$cont, function ($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFont(array(
                                'family' => 'Calibri',
                                'size'   => '8',
                            ));
                        });
                        
                        $sheet->setFreeze('A5');
                        $sheet->setWidth(array(
                            'A' => 6, 'B' => 17, 'C' => 5, 'D' => 6, 'E' => 13, 'F' => 10, 
                            'G' => 24, 'H' => 6, 'I' => 17, 'J' => 6, 'K' => 13, 'L' => 8, 
                            'M' => 12, 'N' => 10, 'O' => 10, 'P' => 6, 'Q' => 5
                        ));

                    });
                }
                else
                {
                   $excel->sheet($data['contenedor']['cliente'], function ($sheet) use ($data) {

                        $objDrawing = new PHPExcel_Worksheet_Drawing;
                        $objDrawing->setPath(public_path('logo-xs.png')); //your image path
                        $objDrawing->setCoordinates('A1');
                        $objDrawing->setWorksheet($sheet);

                        $sheet->mergeCells('C1:Q1');
                        $sheet->setCellvalue('C1', 'ASOCIACION DE PEQUEÑOS PRODUCTORES DE BANANO ORGANICO SAMAN Y ANEXOS');
                        $sheet->cell('C1', function ($cell) {
                            $cell->setBorder('medium', 'medium', 'thin', 'thin');
                        });

                        $sheet->mergeCells('C2:Q2');
                        $sheet->setCellvalue('C2', 'Sector Nueva Esperanza S/N Samán ,Sullana Perú-Telef.: 504119');
                        $sheet->cell('C2', function ($cell) {
                            $cell->setBorder('thin', 'medium', 'thin', 'thin');
                        });

                        $sheet->mergeCells('C3:Q3');
                        $sheet->setCellvalue('C3', 'PACKING LIST');
                        $sheet->cell('C3', function ($cell) {
                            $cell->setBorder('thin', 'medium', 'medium', 'thin');
                        });

                        $sheet->cells('A1:Q4', function ($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFont(array(
                                'family' => 'Calibri',
                                'size'   => '10',
                                'bold'   => true,
                            ));
                        });
                        $sheet->cells('A2:Q2', function ($cells) {
                            $cells->setFont(array(
                                'bold'   => false,
                            ));
                        });

                        $sheet->setColumnFormat(array(
                            'E' => '000000000000',
                            'D' => 'd-mmm'
                        ));

                        $sheet->setCellvalue('A4', 'N° PALLET');
                        $sheet->setCellvalue('B4', 'CODIGOS DE BARRA');
                        $sheet->setCellvalue('C4', 'SEMANA');
                        $sheet->setCellvalue('D4', 'FECHA');
                        $sheet->setCellvalue('E4', 'GGN');
                        $sheet->setCellvalue('F4', 'EMPACADORA');
                        $sheet->setCellvalue('G4', 'PRODUCTOR');
                        $sheet->setCellvalue('H4', 'CODIGO');
                        $sheet->setCellvalue('I4', 'TIPO CAJA');
                        $sheet->setCellvalue('J4', 'CJS/PLT');
                        $sheet->setCellvalue('K4', 'ETIQUETA');
                        $sheet->setCellvalue('L4', 'PLASTICO');
                        $sheet->setCellvalue('M4', 'CONTENEDOR');
                        $sheet->setCellvalue('N4', 'REG. DE TEMPERATURA');
                        $sheet->setCellvalue('O4', 'Nº BOOKING');
                        $sheet->setCellvalue('P4', 'PRECINTO');
                        $sheet->setCellvalue('Q4', 'LOTE');

                        $sheet->setAutoFilter('A4:Q4');
                        $sheet->cells('A4:Q4', function ($cells) {
                            $cells->setBackground('#2E4053');
                            $cells->setFontColor('#ffffff');
                        });

                        $cont = 5;
                        $total_general_cajas = 0;
                        $caja_select = $data['contenedor']['cajas'][0];
                        foreach ($data['llenados'] as $i => $llenado) {
                            foreach ($llenado as $j => $pallet) {

                                $parcela = Parcela::find($pallet['id_parcela']);

                                $sheet->setCellvalue('A'.$cont, ($i + 1));
                                $sheet->setCellvalue('C'.$cont, $data['contenedor']['semana']);
                                $sheet->setCellvalue('D'.$cont, $data['contenedor']['dia_proceso_inicio']);
                                $sheet->setCellvalue('E'.$cont, $parcela->ggn);
                                $sheet->setCellvalue('F'.$cont, $parcela->empacadora->nombre);
                                $sheet->setCellvalue('G'.$cont, $parcela->productor->persona->fullname);
                                $sheet->setCellvalue('H'.$cont, $parcela->codigo_parcela);
                                $sheet->setCellvalue('I'.$cont, $caja_select['marca']);
                                $sheet->setCellvalue('J'.$cont, $pallet['cajas']);
                                $total_general_cajas += $pallet['cajas'];
                                $sheet->setCellvalue('M'.$cont, $data['contenedor']['numero_contenedor']);
                                $sheet->setCellvalue('O'.$cont, $data['contenedor']['booking']);

                                $cont++;
                            }
                            $sum = 0;
                            if(count($data['contenedor']['cajas']) > 1){
                                foreach ($data['contenedor']['cajas'] as $key => $caja) {
                                    $sum += $caja['cantidad_cajas'];
                                    if ($total_general_cajas >= $sum) {
                                        $caja_select = $data['contenedor']['cajas'][$key+1];
                                        break;
                                    }
                                }
                            }
                        }

                        $sheet->setCellvalue('J'.$cont, $total_general_cajas);

                        $sheet->cells('A4:Q'.$cont, function ($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFont(array(
                                'family' => 'Calibri',
                                'size'   => '8',
                            ));
                        });
                        
                        $sheet->setFreeze('A5');
                        $sheet->setWidth(array(
                            'A' => 6, 'B' => 17, 'C' => 5, 'D' => 6, 'E' => 13, 'F' => 10, 
                            'G' => 24, 'H' => 6, 'I' => 17, 'J' => 6, 'K' => 13, 'L' => 8, 
                            'M' => 12, 'N' => 10, 'O' => 10, 'P' => 6, 'Q' => 5
                        ));

                    }); 
                }

            })->export('xls');       
        }catch(Exception $e){
            return response()->json(['message'=>'Error al generar reporte reporte '.$e->getMessage(),'success'=>false]);    
        }
    }

    public function reportePDFLlenado($data)
    {
        $pdf = \PDF::loadView('llenado.cartilla',['data'=>$data]);
        return $pdf->download('llenado_contenedor.pdf');
    }

    public function getDataForReporte($id_contenedor)
    {
        $contenedor_query = Contenedor::find($id_contenedor);
        $cajas = array();
        $total_cajas = 0;
        foreach ($contenedor_query->cajas as $value) {
            $cajas []           = array(
                'id'               => $value->id,
                'marca'            => $value->marca,
                'cantidad_cajas'   => $value->pivot->cantidad
            );
            $total_cajas += $value->pivot->cantidad;
        }
        $contenedor = array(
            'cajas'                  => $cajas,
            'id'                     => $contenedor_query->id,
            'id_semana'              => $contenedor_query->id_semana,
            'semana'                 => $contenedor_query->semana->nombre,
            'cliente'                => $contenedor_query->cliente->razon_social,
            'email_cliente'          => $contenedor_query->cliente->email,
            'referencia'             => $contenedor_query->referencia,
            'booking'                => $contenedor_query->booking,
            'numero_contenedor'      => $contenedor_query->numero_contenedor,
            'id_lineanaviera'        => $contenedor_query->id_lineanaviera,
            'linea_naviera'          => $contenedor_query->linea_naviera->nombre,
            'sigla_linea_naviera'    => $contenedor_query->linea_naviera->sigla,
            'nave'                   => $contenedor_query->nave,
            'viaje'                  => $contenedor_query->viaje,
            'puerto_destino'         => $contenedor_query->puerto_destino->nombre,
            'operador'               => $contenedor_query->operador->nombre,
            'dia_proceso_inicio'     => date_format(date_create($contenedor_query->fecha_proceso_inicio),'d-M'),
            'total_cajas'            => $total_cajas
        );

        $llenados = array();

        for ($i=1; $i <= 20; $i++) { 
            $llenado_query = Llenado::where('id_contenedor',$id_contenedor)->where('pallet',$i)->where('activo',1)->get();
            $llenado = array();
            foreach ($llenado_query as $key => $value) {
                $llenado[] = array(
                    'id'                => $value->id,
                    'cajas'             => $value->cajas,
                    'pallet'            => $value->pallet,
                    'id_parcela'        => $value->id_parcela,
                    'id_contenedor'     => $value->id_contenedor,
                    'codigo_parcela'    => $value->parcela->codigo_parcela,
                );
            }
            $llenados[] = $llenado;
        }

        $data = array(
            'contenedor'  => $contenedor,
            'llenados'    => $llenados
        );

        return $data;
    }

    public function getDataPacking($id_semana)
    {
        try{

            $contenedores_query = Contenedor::where('id_semana',$id_semana)->where('llenado',1)->where('activo',1)->get();

            $clientes = array();

            foreach ($contenedores_query as $contenedor) {
                $clientes[] = $contenedor->cliente;
            }

            $collection = collect($clientes);
            $clientes   = $collection->unique('id');
            $clientes_query   = $clientes->values()->all();

            $clientes = array();
            foreach ($clientes_query as $cliente) {
                $contenedores_query = Contenedor::where('id_semana',$id_semana)->where('llenado',1)->where('id_cliente',$cliente->id)->where('activo',1)->get();

                $contenedores = array();
                foreach ($contenedores_query as $contenedor) {
                    $cajas = array();
                    $total_cajas = 0;
                    $llenados = array();

                    for ($i=1; $i <= 20; $i++) { 
                        $llenado_query = Llenado::where('id_contenedor',$contenedor->id)->where('pallet',$i)->where('activo',1)->get();
                        $llenado = array();
                        foreach ($llenado_query as $value) {
                            $llenado[] = array(
                                'id'                => $value->id,
                                'cajas'             => $value->cajas,
                                'pallet'            => $value->pallet,
                                'id_parcela'        => $value->id_parcela,
                                'id_contenedor'     => $value->id_contenedor,
                                'codigo_parcela'    => $value->parcela->codigo_parcela,
                            );
                        }
                        $llenados[] = $llenado;
                    }

                    foreach ($contenedor->cajas as $value) {
                        $cajas []           = array(
                            'id'               => $value->id,
                            'marca'            => $value->marca,
                            'cantidad_cajas'   => $value->pivot->cantidad
                        );
                        $total_cajas += $value->pivot->cantidad;
                    }
                    $contenedores[] = array(
                        'cajas'                  => $cajas,
                        'id'                     => $contenedor->id,
                        'id_semana'              => $contenedor->id_semana,
                        'semana'                 => $contenedor->semana->nombre,
                        'cliente'                => $contenedor->cliente->razon_social,
                        'email_cliente'          => $contenedor->cliente->email,
                        'referencia'             => $contenedor->referencia,
                        'booking'                => $contenedor->booking,
                        'numero_contenedor'      => $contenedor->numero_contenedor,
                        'id_lineanaviera'        => $contenedor->id_lineanaviera,
                        'linea_naviera'          => $contenedor->linea_naviera->nombre,
                        'sigla_linea_naviera'    => $contenedor->linea_naviera->sigla,
                        'nave'                   => $contenedor->nave,
                        'viaje'                  => $contenedor->viaje,
                        'puerto_destino'         => $contenedor->puerto_destino->nombre,
                        'operador'               => $contenedor->operador->nombre,
                        'dia_proceso_inicio'     => date_format(date_create($contenedor->fecha_proceso_inicio),'d-M'),
                        'total_cajas'            => $total_cajas,
                        'llenados'               => $llenados
                    );
                }
                $clientes[] = array(
                    'id'            => $cliente->id,
                    'razon_social'  => $cliente->razon_social,
                    'contenedores'  => $contenedores
                );
            }
            
            return $clientes;    
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al listar los registros'.$e->getMessage(),'success'=>false]);    
        }
    }

    public function reporteExcelPacking($clientes)
    {
        try{
            Excel::create('Reporte_Llenado', function ($excel) use ($clientes) {
                foreach ($clientes as $cliente) {
                    if (mb_strtoupper($cliente['razon_social']) === 'AGROFAIR') {
                        $excel->sheet($cliente['razon_social'], function ($sheet) use ($cliente) {

                            $objDrawing = new PHPExcel_Worksheet_Drawing;
                            $objDrawing->setPath(public_path('logo-xs.png')); //your image path
                            $objDrawing->setCoordinates('A1');
                            $objDrawing->setWorksheet($sheet);

                            $sheet->mergeCells('C1:Q1');
                            $sheet->setCellvalue('C1', 'ASOCIACION DE PEQUEÑOS PRODUCTORES DE BANANO ORGANICO SAMAN Y ANEXOS');
                            $sheet->cell('C1', function ($cell) {
                                $cell->setBorder('medium', 'medium', 'thin', 'thin');
                            });

                            $sheet->mergeCells('C2:Q2');
                            $sheet->setCellvalue('C2', 'Sector Nueva Esperanza S/N Samán ,Sullana Perú-Telef.: 504119');
                            $sheet->cell('C2', function ($cell) {
                                $cell->setBorder('thin', 'medium', 'thin', 'thin');
                            });

                            $sheet->mergeCells('C3:Q3');
                            $sheet->setCellvalue('C3', 'PACKING LIST');
                            $sheet->cell('C3', function ($cell) {
                                $cell->setBorder('thin', 'medium', 'medium', 'thin');
                            });

                            $sheet->cells('A1:Q4', function ($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFont(array(
                                    'family' => 'Calibri',
                                    'size'   => '10',
                                    'bold'   => true,
                                ));
                            });
                            $sheet->cells('A2:Q2', function ($cells) {
                                $cells->setFont(array(
                                    'bold'   => false,
                                ));
                            });

                            $sheet->setColumnFormat(array(
                                'E' => '000000000000',
                                'D' => 'd-mmm'
                            ));

                            $sheet->setCellvalue('A4', 'N° PALLET');
                            $sheet->setCellvalue('B4', 'CODIGOS DE BARRA');
                            $sheet->setCellvalue('C4', 'SEMANA');
                            $sheet->setCellvalue('D4', 'FECHA');
                            $sheet->setCellvalue('E4', 'GGN');
                            $sheet->setCellvalue('F4', 'EMPACADORA');
                            $sheet->setCellvalue('G4', 'PRODUCTOR');
                            $sheet->setCellvalue('H4', 'CODIGO');
                            $sheet->setCellvalue('I4', 'TIPO CAJA');
                            $sheet->setCellvalue('J4', 'CJS/PLT');
                            $sheet->setCellvalue('K4', 'ETIQUETA');
                            $sheet->setCellvalue('L4', 'PLASTICO');
                            $sheet->setCellvalue('M4', 'CONTENEDOR');
                            $sheet->setCellvalue('N4', 'REG. DE TEMPERATURA');
                            $sheet->setCellvalue('O4', 'Nº BOOKING');
                            $sheet->setCellvalue('P4', 'PRECINTO');
                            $sheet->setCellvalue('Q4', 'LOTE');

                            $sheet->setAutoFilter('A4:Q4');
                            $sheet->cells('A4:Q4', function ($cells) {
                                $cells->setBackground('#2E4053');
                                $cells->setFontColor('#ffffff');
                            });

                            $cont = 5;
                            $tl = 1961;

                            foreach ($cliente['contenedores'] as $contenedor) {
                                $total_general_cajas = 0;
                                $caja_select = $contenedor['cajas'][0];
                                foreach ($contenedor['llenados'] as $i => $llenado) {
                                    foreach ($llenado as $j => $pallet) {

                                        $parcela = Parcela::find($pallet['id_parcela']);

                                        $sheet->setCellvalue('A'.$cont, $tl);
                                        $sheet->setCellvalue('C'.$cont, $contenedor['semana']);
                                        $sheet->setCellvalue('D'.$cont, $contenedor['dia_proceso_inicio']);
                                        $sheet->setCellvalue('E'.$cont, $parcela->ggn);
                                        $sheet->setCellvalue('F'.$cont, $parcela->empacadora->nombre);
                                        $sheet->setCellvalue('G'.$cont, $parcela->productor->persona->fullname);
                                        $sheet->setCellvalue('H'.$cont, $parcela->codigo_parcela);
                                        $sheet->setCellvalue('I'.$cont, $caja_select['marca']);
                                        $sheet->setCellvalue('J'.$cont, $pallet['cajas']);
                                        $total_general_cajas += $pallet['cajas'];
                                        $sheet->setCellvalue('M'.$cont, $contenedor['numero_contenedor']);
                                        $sheet->setCellvalue('O'.$cont, $contenedor['booking']);

                                        //valido si es el ultimo registro de pallet
                                        if($j === (count($llenado)-1)){
                                            //si tiene por utlimo 6 o 7 no se hace el desglose sino si
                                            if ($pallet['cajas'] == 6 || $pallet['cajas'] == 7) {
                                                $sheet->setCellvalue('A'.$cont, 'TL '.$tl);
                                            }
                                            else
                                            {
                                                //si tiene 1080 se desglosa 6 cajas para el tl
                                                if ($contenedor['total_cajas']==1080) {
                                                    $sheet->setCellvalue('J'.$cont, ($pallet['cajas'] - 6));
                                                    $cont++;
                                                    $sheet->setCellvalue('J'.$cont, 6);
                                                }
                                                //si tiene 1400 se desglosa 7 cajas para el tl
                                                if ($contenedor['total_cajas']==1400) {
                                                    $sheet->setCellvalue('J'.$cont, ($pallet['cajas'] - 7));
                                                    $cont++;
                                                    $sheet->setCellvalue('J'.$cont, 7);
                                                }
                                                //se llena la data respectiva del tl para que no quede en blanco
                                                $sheet->setCellvalue('A'.$cont, 'TL '.$tl);
                                                $sheet->setCellvalue('C'.$cont, $contenedor['semana']);
                                                $sheet->setCellvalue('D'.$cont, $contenedor['dia_proceso_inicio']);
                                                $sheet->setCellvalue('E'.$cont, $parcela->ggn);
                                                $sheet->setCellvalue('F'.$cont, $parcela->empacadora->nombre);
                                                $sheet->setCellvalue('G'.$cont, $parcela->productor->persona->fullname);
                                                $sheet->setCellvalue('H'.$cont, $parcela->codigo_parcela);

                                                $sheet->setCellvalue('I'.$cont, $caja_select['marca']);

                                                $sheet->setCellvalue('M'.$cont, $contenedor['numero_contenedor']);
                                                $sheet->setCellvalue('O'.$cont, $contenedor['booking']);

                                            }
                                        }

                                        $cont++;
                                    }
                                    $sum = 0;
                                    if(count($contenedor['cajas']) > 1){
                                        foreach ($contenedor['cajas'] as $key => $caja) {
                                            $sum += $caja['cantidad_cajas'];
                                            if ($total_general_cajas >= $sum) {
                                                $caja_select = $contenedor['cajas'][$key+1];
                                                break;
                                            }
                                        }
                                    }
                                    $tl++;
                                }
                            }

                            $sheet->setBorder('A4:Q'.($cont-1),'thin');

                            $sheet->setCellvalue('J'.$cont, '=SUM(J5:J'.($cont-1).')');

                            $sheet->cells('A4:Q'.$cont, function ($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFont(array(
                                    'family' => 'Calibri',
                                    'size'   => '8',
                                ));
                            });
                            
                            $sheet->setFreeze('A5');
                            $sheet->setWidth(array(
                                'A' => 6, 'B' => 17, 'C' => 5, 'D' => 6, 'E' => 13, 'F' => 10, 
                                'G' => 24, 'H' => 6, 'I' => 17, 'J' => 6, 'K' => 13, 'L' => 8, 
                                'M' => 12, 'N' => 10, 'O' => 10, 'P' => 6, 'Q' => 5
                            ));

                        });
                    }
                    else
                    {
                       $excel->sheet($cliente['razon_social'], function ($sheet) use ($cliente) {

                            $objDrawing = new PHPExcel_Worksheet_Drawing;
                            $objDrawing->setPath(public_path('logo-xs.png')); //your image path
                            $objDrawing->setCoordinates('A1');
                            $objDrawing->setWorksheet($sheet);

                            $sheet->mergeCells('C1:P1');
                            $sheet->setCellvalue('C1', 'ASOCIACION DE PEQUEÑOS PRODUCTORES DE BANANO ORGANICO SAMAN Y ANEXOS');
                            $sheet->cell('C1', function ($cell) {
                                $cell->setBorder('medium', 'medium', 'thin', 'thin');
                            });

                            $sheet->mergeCells('C2:P2');
                            $sheet->setCellvalue('C2', 'Sector Nueva Esperanza S/N Samán ,Sullana Perú-Telef.: 504119');
                            $sheet->cell('C2', function ($cell) {
                                $cell->setBorder('thin', 'medium', 'thin', 'thin');
                            });

                            $sheet->mergeCells('C3:P3');
                            $sheet->setCellvalue('C3', 'PACKING LIST');
                            $sheet->cell('C3', function ($cell) {
                                $cell->setBorder('thin', 'medium', 'medium', 'thin');
                            });

                            $sheet->cells('A1:P4', function ($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFont(array(
                                    'family' => 'Calibri',
                                    'size'   => '10',
                                    'bold'   => true,
                                ));
                            });
                            $sheet->cells('A2:P2', function ($cells) {
                                $cells->setFont(array(
                                    'bold'   => false,
                                ));
                            });

                            $sheet->setColumnFormat(array(
                                'D' => '000000000000',
                                'C' => 'd-mmm'
                            ));

                            $sheet->setCellvalue('A4', 'N° PALLET');
                            $sheet->setCellvalue('B4', 'SEMANA');
                            $sheet->setCellvalue('C4', 'FECHA');
                            $sheet->setCellvalue('D4', 'GGN');
                            $sheet->setCellvalue('E4', 'EMPACADORA');
                            $sheet->setCellvalue('F4', 'PRODUCTOR');
                            $sheet->setCellvalue('G4', 'CODIGO');
                            $sheet->setCellvalue('H4', 'TIPO CAJA');
                            $sheet->setCellvalue('I4', 'CJS/PLT');
                            $sheet->setCellvalue('J4', 'ETIQUETA');
                            $sheet->setCellvalue('K4', 'PLASTICO');
                            $sheet->setCellvalue('L4', 'CONTENEDOR');
                            $sheet->setCellvalue('M4', 'REG. DE TEMPERATURA');
                            $sheet->setCellvalue('N4', 'Nº BOOKING');
                            $sheet->setCellvalue('O4', 'PRECINTO');
                            $sheet->setCellvalue('P4', 'LOTE');

                            $sheet->setAutoFilter('A4:P4');
                            $sheet->cells('A4:P4', function ($cells) {
                                $cells->setBackground('#2E4053');
                                $cells->setFontColor('#ffffff');
                            });

                            $cont = 5;
                            
                            foreach ($cliente['contenedores'] as $contenedor) {
                                $total_general_cajas = 0;
                                $caja_select = $contenedor['cajas'][0];
                                
                                foreach ($contenedor['llenados'] as $i => $llenado) {
                                    foreach ($llenado as $j => $pallet) {

                                        $parcela = Parcela::find($pallet['id_parcela']);

                                        $sheet->setCellvalue('A'.$cont, ($i + 1));
                                        $sheet->setCellvalue('B'.$cont, $contenedor['semana']);
                                        $sheet->setCellvalue('C'.$cont, $contenedor['dia_proceso_inicio']);
                                        $sheet->setCellvalue('D'.$cont, $parcela->ggn);
                                        $sheet->setCellvalue('E'.$cont, $parcela->empacadora->nombre);
                                        $sheet->setCellvalue('F'.$cont, $parcela->productor->persona->fullname);
                                        $sheet->setCellvalue('G'.$cont, $parcela->codigo_parcela);
                                        $sheet->setCellvalue('H'.$cont, $caja_select['marca']);
                                        $sheet->setCellvalue('I'.$cont, $pallet['cajas']);
                                        $total_general_cajas += $pallet['cajas'];
                                        $sheet->setCellvalue('L'.$cont, $contenedor['numero_contenedor']);
                                        $sheet->setCellvalue('N'.$cont, $contenedor['booking']);

                                        $cont++;
                                    }
                                    $sum = 0;
                                    if(count($contenedor['cajas']) > 1){
                                        foreach ($contenedor['cajas'] as $key => $caja) {
                                            $sum += $caja['cantidad_cajas'];
                                            if ($total_general_cajas >= $sum) {
                                                $caja_select = $contenedor['cajas'][$key+1];
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            $sheet->setBorder('A4:P'.($cont-1),'thin');

                            $sheet->setCellvalue('I'.$cont, '=SUM(I5:I'.($cont-1).')');

                            $sheet->cells('A4:P'.$cont, function ($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFont(array(
                                    'family' => 'Calibri',
                                    'size'   => '8',
                                ));
                            });
                            
                            $sheet->setFreeze('A5');
                            $sheet->setWidth(array(
                                'A' => 6, 'B' => 5, 'C' => 6, 'D' => 13, 'E' => 10, 
                                'F' => 24, 'G' => 6, 'H' => 17, 'I' => 6, 'J' => 13, 'K' => 8, 
                                'L' => 12, 'M' => 10, 'N' => 10, 'O' => 6, 'P' => 5
                            ));

                        }); 
                    }
                }
            })->export('xls');       
        }catch(Exception $e){
            return response()->json(['message'=>'Error al generar reporte reporte '.$e->getMessage(),'success'=>false]);    
        }
    }

}