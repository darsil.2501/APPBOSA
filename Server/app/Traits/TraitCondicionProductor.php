<?php

namespace App\Traits;
use App\CondicionProductor;

trait TraitCondicionProductor
{
	public static function obtenerCondicionProductor()
    {
        try{
            $condiciones = CondicionProductor::get();
            return response()->json(['info'=>$condiciones,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Error al listar los registros.'.$e->getMessage(),'success'=>false]);    
        }
    }
}