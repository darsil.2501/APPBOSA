<?php

namespace App\Traits;

use App\GaleriaVisita;
use App\Traits\TraitGaleriaVisita;
use App\Visita;
use Illuminate\Support\Facades\DB;

trait TraitGaleriaVisita
{
	public static function obtenerImagenesVisita($visita_id)
	{
		try {
			$imagenes = Visita::find($visita_id)->galeria_visita;
			return response()->json(['success'=>true,'info'=>$imagenes]);
		} catch (\Exception $e) {
			return response()->json(['success'=>false,'message'=>$e->getMessage()]);
		}
	}

	public static function registrarImagenesVisita($request)
	{
		try{
		    DB::beginTransaction();
		    	$imagenes = $request->file('files');
		    	foreach ($imagenes as $imagen) {
		    		$url = $request->id_visita.'_'.date("YmdHis").'_'.$imagen->getClientOriginalName();
		    		$request['url'] = $url;
		    		GaleriaVisita::create($request->all());
		    		\Storage::disk('galeria_visita')->put($url,\File::get($imagen));
		    	}
		    DB::commit();
		    return response()->json(['success'=>true,'message'=>'Imágenes registradas correctamente.']);   
		}catch(\Exception $e){
		    DB::rollback();
		    return response()->json(['success'=>false,'message'=>$e->getMessage()]);
		}
	}

	public static function eliminarImagenesVisita($imagen_id)
	{
		try{
		    DB::beginTransaction();
		    	$imagen = GaleriaVisita::find($imagen_id);
		    	$imagen->delete();
		    DB::commit();
		    return response()->json(['success'=>true,'message'=>'Imagen eliminada correctamente.']);   
		}catch(\Exception $e){
		    DB::rollback();
		    return response()->json(['success'=>false,'message'=>$e->getMessage()]);
		}
	}
}