<?php

namespace App\Traits;
use App\Certificado;
use DB;

trait TraitCertificado
{
	public static function getCertificados($id_semana)
	{
		$data = Certificado::where('id_semana',$id_semana)->orderBy('fecha','DESC')->get();
        $certificados = array();
        foreach ($data as $key => $value) {
            $certificados[] = array(
                'id'                    => $value->id,
                'id_cliente'            => $value->cliente->id,
                'cliente'               => $value->cliente->razon_social,
                'id_semana'             => $value->semana->id,
                'semana'                => $value->semana->nombre,
                'id_operador'           => $value->id_operador,
                'operador'              => ($value->id_operador) ? self::getNameOperador($value->id_operador) : null ,
                'contenedores'          => self::getContenedoresOfCertificado($value),
                'nombre'                => $value->nombre,
                'fecha'                 => $value->fecha,
                'hora'                  => $value->hora,
                'url'                   => $value->url,
                'descripcion'           => $value->descripcion,
                'id_estado_archivo'     => $value->estado_archivo->id,
                'estado_archivo'        => $value->estado_archivo->nombre,
                'color_estado_archivo'  => $value->estado_archivo->color,
            );
        }
        return $certificados;
	}

    public static function getContenedoresOfCertificado($certificado)
    {
        $contenedores = array();
        foreach ($certificado->contenedores as $key => $value) {
            $contenedores[] = array(
                'id_contenedor' => $value->id,
                'numero'        => $value->nombre,
                'referencia'    => $value->referencia
            );
        }
        return $contenedores;
    }

    // cambiar el estado del certificado (este se hace de forma manual)
    public static function changeStatusCertificado($request)
    {
        try{
            DB::beginTransaction();
                $certificado = Certificado::find($request['id_certificado']);
                $certificado->fill(['id_estado_archivo'=>$request['id_estado_archivo']])->save();
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Estado del archivo actualizado correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al cambiar estado a este archivo. '.$ex->getMessage()]);
        }
    }

    // Cambiar el estado del campo "certificado" de los contenedores que pertenecen a un certificado determinado
    public static function changeStatusContenedoresOfCertificado($request)
    {
        try{
            DB::beginTransaction();
                $certificado = Certificado::find($request['id_certificado']);
                foreach ($certificado->contenedores as $contenedor) {
                    $contenedor->fill(['certificado'=>DB::raw($request['id_estado_archivo'])])->save();
                }
            DB::commit();
            // return response()->json(['success'=>true,'message'=>'Estado del archivo actualizado correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al cambiar estado a los contenedores. '.$ex->getMessage()]);
        }
    }
}