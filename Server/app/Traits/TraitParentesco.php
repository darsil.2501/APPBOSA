<?php

namespace App\Traits;

use App\Parentesco;
use Illuminate\Support\Facades\DB;


trait TraitParentesco
{
	public static function obtenerParentescos()
	{
		try{
            $registros = Parentesco::where('activo',1)->get();
            return response()->json(['info'=>$registros,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Error al listar los registros.'.$e->getMessage(),'success'=>false]);    
        }
	}

	public static function registrarParentesco($request)
	{
		try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            $registro = Parentesco::create($request->all());
            // self::auditar('audit_empacadora',$registro->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();    
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
	}

	public static function actualizarParentesco($request,$id)
	{
		try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            $registro = Parentesco::find($id);
            if($registro){
                $registro->fill($request->all())->save();
                // self::auditar('audit_empacadora',$registro->nombre,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
	}

	public static function eliminarParentesco($id)
	{
		try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            $registro = Parentesco::find($id);
            if($registro){
                if(count($registro->trabajadores) > 0){return self::RegistroInUse();}        
                $registro->fill(['activo'=>DB::raw(0)])->save();
                // self::auditar('audit_empacadora',$registro->nombre,'ELIMINAR');
                return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
	}
}