<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contenedor;

class LineaNaviera extends Model
{
    protected $table    = 'linea_naviera';
	protected $fillable = ['nombre','sigla','activo'];
	public $timestamps  = false;


	// Relaciones
	public function contenedor()
	{
		return $this->hasMany(Contenedor::class,'id_lineanaviera');
	}
}
