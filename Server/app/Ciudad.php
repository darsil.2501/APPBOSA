<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table    = 'ciudad';
	protected $fillable = ['id_pais','nombre','activo'];
	public $timestamps  = false;

	public function pais()
	{
		return $this->belongsTo(Pais::class,'id_pais');
	}

	public function puertos()
	{
		return $this->hasMany(PuertoDestino::class,'id_ciudad');
	}
}
