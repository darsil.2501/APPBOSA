<?php

namespace App;

use App\Rol;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table    = 'permiso';
    protected $fillable = ['nombre','slug','activo'];
    protected $hidden   = ['pivot'];
    public $timestamps  = false;


    // public function rol()
    // {
    // 	return $this->belongsToMany(Rol::class);
    // }
    public function usuarios()
    {
    	return $this->belongsToMany(User::class,'permiso_usuario','usuario_id','permiso_id');
    }
}
