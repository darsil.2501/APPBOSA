<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motivo extends Model
{
    protected $table    = 'motivo';
    protected $fillable = ['nombre','activo'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    public function productores()
    {
        return $this->hasMany(Productor::class,'id_productor');
    }
}
