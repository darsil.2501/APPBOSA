<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contenedor;
use App\Cliente;

class Caja extends Model
{
    protected $table    = 'caja';
    protected $fillable = ['marca','peso','activo'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    public function contenedor()
    {
    	return $this->belongsToMany(Contenedor::class)->withPivot('cantidad');
    }

    public function cliente()
    {
    	return $this->belongsToMany(Cliente::class);
    }

}
