<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Movimiento;

class Estado extends Model
{
    protected $table    = 'estado';
	protected $fillable = ['nombre','color','activo'];
	public $timestamps  = false;

	public function movimiento()
	{
		return $this->hasMany(Movimiento::class,'id_estado');
	}
}
