<?php

namespace App;

use App\Parcela;
use Illuminate\Database\Eloquent\Model;

class Empacadora extends Model
{
    protected $table    = 'empacadora';
    protected $fillable = ['nombre','ubicacion','activo'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    public function parcelas()
    {
    	return $this->hasMany(Parcela::class,'id_empacadora');
    }
}
