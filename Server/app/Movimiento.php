<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Contenedor;
use App\Estado;
use App\User;

class Movimiento extends Model
{
    protected $table    = 'movimientos';
    protected $fillable = ['id_contenedor','id_usuario','fecha','hora','id_estado','activo'];
    public $timestamps  = false;

    public function contenedor()
    {
    	return $this->belongsTo(Contenedor::class,'id_contenedor');
    }

    public function estado()
    {
    	return $this->belongsTo(Estado::class,'id_estado');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class,'id_usuario');
    }

}
