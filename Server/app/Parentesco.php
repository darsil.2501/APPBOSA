<?php

namespace App;

use App\ProductorTrabajador;
use App\Trabajador;
use Illuminate\Database\Eloquent\Model;

class Parentesco extends Model
{
    protected $table    = 'parentesco';
    protected $fillable = ['nombre','activo'];
    public $timestamps  = false;

    public function productorTrabajadores()
    {
        return $this->hasMany(ProductorTrabajador::class,'id_parentesco');
    }
}
