<?php

namespace App;

use App\Cliente;
use App\EstadoArchivo;
use App\Semana;
use Illuminate\Database\Eloquent\Model;

class Vl extends Model
{
    protected $table    = 'vl';
    protected $fillable = ['nombre','id_cliente','id_semana','fecha','hora','url','id_estado_archivo'];
    public $timestamps  = false;

    public function cliente()
    {
    	return $this->belongsTo(Cliente::class,'id_cliente');
    }

    public function semana()
    {
    	return $this->belongsTo(Semana::class,'id_semana');
    }

    public function estado_archivo()
    {
        return $this->belongsTo(EstadoArchivo::class,'id_estado_archivo');
    }
}
