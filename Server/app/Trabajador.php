<?php

namespace App;

use App\Parentesco;
use App\Persona;
use App\Productor;
use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    protected $table    = 'trabajador';
    protected $fillable = ['id_persona','activo'];
    public $timestamps  = false;

    public function persona()
    {
    	return $this->belongsTo(Persona::class,'id_persona');
    }

    public function productorTrabajadores()
    {
        return $this->hasMany(ProductorTrabajador::class,'id_trabajador');
    }

    // Se hizo porque no encontre la forma de hacerlo desde el formrequest
    public static function validateUpdateTrabajador($data,$id_persona)
    {
        $reglas = [
            'nombre'           =>'required|min:2|max:30',
            'apellido_paterno' =>'min:2|max:30',
            'apellido_materno' =>'min:2|max:30',
            'dni'              =>'unique:persona,dni,'.$id_persona.',id,activo,1|regex:/^[0-9]{8}$/',
        ];

        $mensajes = [
            'nombre.required'           =>'Debe ingresar sus nombre',
            'nombre.min'                =>'Su nombre debe tener como mínimo 2 caracteres',
            'nombre.max'                =>'Su nombre debe tener como máximo 30 caracteres',
            'nombre.regex'              =>'Debe ingresar un nombre válido',

            'apellido_paterno.required' =>'Debe ingresar su apellido paterno',
            'apellido_paterno.min'      =>'Su apellido paterno debe tener como mínimo 2 caracteres',
            'apellido_paterno.max'      =>'Su apellido paterno debe tener como máximo 30 caracteres',
            'apellido_paterno.regex'    =>'Debe ingresar un apellido paterno válido',

            'apellido_materno.required' =>'Debe ingresar su apellido materno',
            'apellido_materno.min'      =>'Su apellido materno debe tener como mínimo 2 caracteres',
            'apellido_materno.max'      =>'Su apellido materno debe tener como máximo 30 caracteres',
            'apellido_materno.regex'    =>'Debe ingresar un apellido materno válido',

            'dni.required'              => 'Debe ingresar su DNI',
            'dni.unique'                => 'Ya existe este DNI.',
            'dni.regex'                 => 'Formato de DNI inválido.',
        ];
        return \Validator::make($data,$reglas,$mensajes);
    }


}
