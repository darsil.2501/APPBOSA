<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Labor extends Model
{
    protected $table    = 'labor';
    protected $fillable = ['nombre','activo'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    public function items()
    {
        return $this->hasMany(ItemLabor::class,'id_labor');
    }

}
