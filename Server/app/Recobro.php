<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Empacadora;
use App\Semana;

class Recobro extends Model
{
    
    protected $table    = 'recobro';

    protected $fillable = [
        'racimas_cosechadas',
        'id_semana',
        'id_empacadora',
        'activo'
    ];
    
    protected $hidden      = [
        'pivot'
    ];

    public $timestamps  = false;

    public function semana()
    {
        return $this->belongsTo(Semana::class,'id_semana');
    }

    public function empacadora()
    {
        return $this->belongsTo(Empacadora::class,'id_empacadora');
    }
    
}
        