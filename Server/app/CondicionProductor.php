<?php

namespace App;

use App\Productor;
use Illuminate\Database\Eloquent\Model;


class CondicionProductor extends Model
{
    protected $table      = 'condicion_productor';
    protected $fillable   = ['nombre','color'];
    public $timestamps    = false;

    public function productores()
    {
    	return $this->hasMany(Productor::class,'id_condicion');
    }
}
