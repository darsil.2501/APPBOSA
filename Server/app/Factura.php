<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cliente;
use App\Semana;
use App\EstadoArchivo;

class Factura extends Model
{
    protected $table    = 'factura';
    protected $fillable = ['nombre','id_cliente','id_semana','id_operador','fecha','hora','url','id_estado_archivo'];
    public $timestamps  = false;

    public function cliente()
    {
    	return $this->belongsTo(Cliente::class,'id_cliente');
    }

    public function semana()
    {
    	return $this->belongsTo(Semana::class,'id_semana');
    }

    public function estado_archivo()
    {
        return $this->belongsTo(EstadoArchivo::class,'id_estado_archivo');
    }
}
