<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class FacturaRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $cantidad_files = count($this->file('files')) - 1;
        foreach(range(0, $cantidad_files) as $index) {
            $rules['facturas.' . $index] = 'mimes:pdf|max:1024';
        }
        return $rules;

    }

    public function messages()
    {
        $messages = [];
        $cantidad_files = count($this->file('facturas')) - 1;
        foreach(range(0, $cantidad_files) as $index) {
            $messages['facturas.'. $index.'.mimes'] = 'El archivo número '.($index+1).' no es un archivo PDF. Porfavor cargue un archivo válido.';
            $messages['facturas.'. $index.'.max']   = 'El archivo número '.($index+1).' ha sobrepasado el peso permitido. Solo se aceptan archivos menos de 1MB.';
        }
        return $messages;
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
