<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class RolRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'  => 'required|min:3|max:30|unique:rol,nombre,'.$this->get('id').',id,activo,1',
            'id_menu' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required'   => 'Debe ingresar un nombre para este Rol.',
            'nombre.min'        => 'El nombre del Rol debe tener como mínimo 3 caracteres.',
            'nombre.max'        => 'El nombre del Rol debe tener como máximo 30 caracteres.',
            'nombre.unique'     => 'Este Rol ya existe.',
            'id_menu.required'  => 'Debe seleccionar un Menú para este Rol.'
        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
