<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class SemanaRequest extends Request
{
    use TraitRespuesta;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'  => 'required',
            'id_anio' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required'  => 'Debe seleccionar la semana.',
            'id_anio.required' => 'Debe seleccionar un año.'
        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
