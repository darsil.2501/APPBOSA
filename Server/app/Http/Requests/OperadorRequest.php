<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class OperadorRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|max:50|unique:operador,nombre,'.$this->get('id').',id,activo,1'
        ];
    }

    public function messages()
    {
        return[
            'nombre.required' => 'Debe ingresar el nombre para Operador.',
            'nombre.min'      => 'El nombre del Operador debe contener como mínimo 5 caracteres.',
            'nombre.max'      => 'El nombre del Operador debe contener como máximo 50 caracteres.',
            'nombre.unique'   => 'Ya existe un Operador con ese nombre.'
        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
