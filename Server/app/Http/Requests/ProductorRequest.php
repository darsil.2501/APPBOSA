<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class ProductorRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dni'            => 'regex:/^[0-9]{8}$/',
            'socio'          => 'required',
            'fecha_ingreso'  => 'required',
            'nombre'         => 'required',
        ];
    }

    public function messages()
    {
        return[
            // 'marca.required' => 'Debe ingresar la marca para la Caja.',
            // 'marca.min'      => 'La marca de la Caja debe contener como mínimo 3 caracteres.',
            // 'marca.max'      => 'La marca de la Caja debe contener como máximo 30 caracteres.',
            // 'marca.unique'   => 'Ya existe una Caja con ese marca.',

            // 'peso.required'  => 'Debe ingresar el peso de la caja.',
            // 'peso.numeric'   => 'Debe ingresar un valor válido para el peso. (Ejm: 19.5), la separación de decimales es con punto.',
            // 'peso.between'   => 'Solo se aceptan valores entre 0-99.9. El formato es (Ejm: 19.5). La separación de decimales es con punto.'
        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
