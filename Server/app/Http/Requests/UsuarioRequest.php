<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;
use App\Traits\TraitFunciones;

class UsuarioRequest extends Request
{
    use TraitRespuesta, TraitFunciones;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'              => 'required|min:2|max:30',
            'apellido_paterno'    => 'min:2|max:30',
            'apellido_materno'    => 'min:2|max:30',
            'dni'                 => 'unique:persona,dni,'.$this->get('id_persona').',id|regex:/^[0-9]{8}$/',
            'telefono'            => 'unique:persona,telefono,'.$this->get('id_persona').',id|regex:/^[0-9]{9}$/',
            'email'               => 'email|unique:persona,email,'.$this->get('id_persona').',id,activo,1',
            'id_cargo'            => 'required',
            // 'id_rol'              => 'required',
            'nick'                => 'required|min:6|max:30|unique:usuario,nick,'.$this->get('id_user').',id,activo,1',
            'password'            => 'required|min:6|max:30',
            'id_cliente_operador' => 'required_if:id_rol,'.self::getIdRolCliente().'|required_if:id_rol,'.self::getIdRolOperador()  
        ];
    }

    public function messages()
    {
        return [
            'nombre.required'           =>'Debe ingresar sus nombre',
            'nombre.min'                =>'Su nombre debe tener como mínimo 2 caracteres',
            'nombre.max'                =>'Su nombre debe tener como máximo 30 caracteres',
            'nombre.regex'              =>'Debe ingresar un nombre válido',

            'apellido_paterno.required' =>'Debe ingresar su apellido paterno',
            'apellido_paterno.min'      =>'Su apellido paterno debe tener como mínimo 2 caracteres',
            'apellido_paterno.max'      =>'Su apellido paterno debe tener como máximo 30 caracteres',
            'apellido_paterno.regex'    =>'Debe ingresar un apellido paterno válido',

            'apellido_materno.required' =>'Debe ingresar su apellido materno',
            'apellido_materno.min'      =>'Su apellido materno debe tener como mínimo 2 caracteres',
            'apellido_materno.max'      =>'Su apellido materno debe tener como máximo 30 caracteres',
            'apellido_materno.regex'    =>'Debe ingresar un apellido materno válido',

            'dni.required'              => 'Debe ingresar su DNI',
            'dni.unique'                => 'Ya existe este DNI.',
            'dni.regex'                 => 'Formato de DNI inválido.',

            'telefono.required'         => 'Debe ingresar un número de celular.',
            'telefono.unique'           => 'Ya se encuentra registrado este número de celular.',
            'telefono.regex'            => 'Debe ingresar un celular válido.', 

            'email.required'            => 'Debe ingresar su correo electrónico.',
            'email.email'               => 'Debe ingresar un correo electrónico válido.',
            'email.unique'              => 'Este correo electrónico ya se encuentra registrado.',

            'id_cargo.required'         => 'Debe seleccionar el Cargo.',
            // 'id_rol.required'           => 'Debe seleccionar el Rol',

            'nick.required'             => 'Debe ingresar su nombre de usuario.',
            'nick.min'                  => 'El nombre de usuario debe tener como mínimo 6 caracteres.',
            'nick.max'                  => 'El nombre de usuario debe tener como máximo 30 caracteres.',
            'nick.unique'               => 'Este nombre de usuario ya existe.',

            'password.required'         => 'Debe ingresar su contraseña.',
            'password.min'              => 'Su contraseña debe tener como mínimo 6 caracteres.',
            'password.max'              => 'Su contraseña debe tener como máximo 30 caracteres.',

            'id_cliente_operador.required_if' => 'Hay campos campos obligatorios que no ha seleccionado.'

        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
