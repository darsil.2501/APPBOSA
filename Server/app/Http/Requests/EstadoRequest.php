<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class EstadoRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|max:30|unique:estado,nombre,'.$this->get('id').',id,activo,1',
            'color'  => 'required'
        ];
    }

    public function messages()
    {
        return[
            'nombre.required' => 'Debe ingresar el nombre para el Estado.',
            'nombre.min'      => 'El nombre del Estado debe contener como mínimo 3 caracteres.',
            'nombre.max'      => 'El nombre del Estado debe contener como máximo 30 caracteres.',
            'nombre.unique'   => 'Ya existe un Estado con ese nombre.',

            'color.required' => 'Debe seleccionar un color para este estado.'
        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
