<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class OpcionRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|max:30|unique:opcion,nombre,'.$this->get('id').',id,activo,1',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required'=> 'El nombre de la opción es requerido',
            'nombre.min'     => 'El nombre debe tener un mínimo de 3 caracteres',
            'nombre.max'     => 'El nombre debe tener un máximo de 30 caracteres',
            'nombre.unique'  => 'Esta opción ya existe',
        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
