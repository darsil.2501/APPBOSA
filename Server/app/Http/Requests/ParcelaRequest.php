<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class ParcelaRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'codigo_parcela' => 'required|unique:parcela,codigo_parcela,'.$this->get('id').',id,activo,1',
            'codigo_parcela' => 'required',
            'tipo_propiedad' => 'required',
            'deshije'        => 'required'
        ];
    }

    public function messages()
    {
        return[
            'codigo_parcela.required' => 'Debe ingresar el código de la parcela.',
            'tipo_propiedad.required' => 'Debe seleccionar el tipo de propiedad.',
            'deshije.required'        => 'Debe seleccionar si se aplicado o no el programa de deshije en esta parcela.'
        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
