<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class LineaNavieraRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|max:30|unique:linea_naviera,nombre,'.$this->get('id').',id,activo,1'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Debe ingresar el nombre para la Línea Naviera.',
            'nombre.min'      => 'El nombre de la Línea Naviera debe contener como mínimo 3 caracteres.',
            'nombre.max'      => 'El nombre de la Línea Naviera debe contener como máximo 30 caracteres.',
            'nombre.unique'   => 'Ya existe una Línea Naviera con ese nombre.'
        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
