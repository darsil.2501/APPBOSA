<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class ClienteRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razon_social'=>'required|min:2|max:30|unique:cliente,razon_social,'.$this->get('id').',id,activo,1',
            'email'       =>'required|email|unique:cliente,email,'.$this->get('id').',id,activo,1',
            'telefono'    =>'required|regex:/^[0-9]{9}$/',
        ];
    }

    public function messages()
    {
        return[
            'razon_social.required' => 'Debe ingresar el nombre del Cliente.',
            'razon_social.min'      => 'El nombre del Cliente debe tener como mínimo 2 caracteres.',
            'razon_social.max'      => 'El nombre del Cliente debe tener como máximo 30 caracteres.',
            'razon_social.unique'   => 'Ya existe este Cliente registrado',

            'email.required'        => 'Debe ingresar un correo electrónico.',
            'email.email'           => 'El correo electrónico es inválido.',
            'email.unique'          => 'Este correo electrónico ya se encuentra registrado.',

            'telefono.required'     => 'Debe ingresar un número de celular.',
            'telefono.regex'        => 'El número de celular ingresado es inválido. (Se recomienda un celular con 9 dígitos)' 


        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
