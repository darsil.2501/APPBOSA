<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class PermisoRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|max:30|unique:permiso,nombre,'.$this->get('id').',id,activo,1',
            'slug'   => 'required|min:3|max:30|unique:permiso,slug,'.$this->get('id').',id,activo,1',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required'=> 'El nombre del permiso es requerido',
            'nombre.min'     => 'El nombre debe tener un mínimo de 3 caracteres',
            'nombre.max'     => 'El nombre debe tener un máximo de 30 caracteres',
            'nombre.unique'  => 'Este permiso ya existe',   

            'slug.required'=> 'El slug del permiso es requerido',
            'slug.min'     => 'El slug debe tener un mínimo de 3 caracteres',
            'slug.max'     => 'El slug debe tener un máximo de 30 caracteres',
            'slug.unique'  => 'Este slug ya existe',
        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
