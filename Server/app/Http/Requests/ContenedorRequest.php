<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Traits\TraitRespuesta;

class ContenedorRequest extends Request
{
    use TraitRespuesta;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fecha_inicial_tmp = date("Y/m/d", strtotime($this->get('fecha_proceso_inicio')."-1 day"));
        return [
            'id_semana'             => 'required',
            // 'numero_contenedor'     => 'required|regex:/^[A-Z]{4}[\s][0-9]{6}[\-][0-9]{1}$/',
            'referencia'            => 'required',
            'id_cliente'            => 'required',
            'id_puertodestino'      => 'required',
            'booking'               => 'required',
            'nave'                  => 'required',
            'id_lineanaviera'       => 'required',
            'viaje'                 => 'required',
            'id_operador'           => 'required',
            // 'fecha_proceso_inicio'  => 'required|date|after:yesterday',
            // 'fecha_proceso_fin'     => 'required|date|after:'.$fecha_inicial_tmp,
            // 'fecha_zarpe'           => 'date|after:fecha_proceso_fin',
            // 'fecha_llegada'         => 'date|after:fecha_zarpe',
            // 'peso_neto'             => 'digits_between:4,5',
            // 'peso_bruto'            => 'digits_between:4,5',
            'cajas'                 => 'required'
        ];
    }

    public function messages()
    {
        return[
            'numero_contenedor.required'    => 'Debe ingresar el código del contenedor.',
            'numero_contenedor.regex'       => 'El formato del código del contenedor no es válido. (Ejm. MVCU 789098-1)',
            'referencia.required'           => 'Debe ingresar una referencia para este contenedor.',
            'id_cliente.required'           => 'Es obligatorio seleccionar el Cliente.',
            'id_puertodestino.required'     => 'Es obligatorio seleccionar el Puerto Destino.',
            'booking.required'              => 'Debe ingresar el Booking.',
            'id_nave.required'              => 'Es obligatorio seleccionar una Nave.',
            'id_lineanaviera.required'      => 'Es obligatorio seleccionar una Línea Naviera.',
            'id_operador.required'          => 'Es obligatorio seleccionar el Operador.',
            'fecha_proceso_inicio.required' => 'Debe ingresar la fecha inicial del proceso de este contenedor.',
            'fecha_proceso_inicio.date'     => 'El formato ingresado para la fecha inicial de proceso no es válido.',
            'fecha_proceso_inicio.after'    => 'La fecha inicial de proceso no es válida, ingrese una fecha de hoy o hacia adelante.',

            'fecha_proceso_fin.required'    => 'Debe ingresar la fecha de finalización del proceso de este contenedor.',
            'fecha_proceso_fin.date'        => 'El formato ingresado para la fecha de finalización del proceso no es válido.',
            'fecha_proceso_fin.after'       => 'La fecha de finalización del proceso debe ser posterior a la fecha inicial del proceso.',

            'fecha_zarpe.date'              => 'El formato ingresado para la fecha de zarpe no es válido.',
            'fecha_zarpe.after'             => 'La fecha de zarpe debe ser posterior a la fecha de finalización del proceso.',

            'fecha_llegada.date'            => 'El formato ingresado para la fecha de llegada no es válido.',
            'fecha_llegada.after'           => 'La fecha de llegada debe ser posterior a la fecha de zarpe.',

            // 'peso_neto.required'            => 'Debe ingresar el peso neto del contenedor.',
            // 'peso_neto.digits_between'      => 'El peso neto ingresado es inválido.',
            // 'peso_neto.required'         => 'Debe ingresar el peso bruto del contenedor.',
            // 'peso_neto.digits_between'   => 'El peso burto ingresado es inválido.',
            'cajas.required'                => 'Este contenedor no puede ir vacío, por favor ingrese cajas.'

        ];
    }

    public function response(array $errors)
    {
        return self::errors($errors);
    }
}
