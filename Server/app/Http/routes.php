<?php

// prueba de envio de emails
Route::get('mail','ClienteController@sendEmailCliente');


Route::post('usuario/auth','UsuarioController@auth');

Route::group(['middleware' => 'auth'], function() {
    
########################## MÓDULO DE USUARIOS ###############################

	Route::group(['prefix' => 'rol'], function() {
	    Route::get('getdata','RolController@getData');
		Route::post('create','RolController@create');
		Route::post('update/{id_rol}','RolController@update');
		Route::delete('delete/{id_rol}','RolController@delete');
		Route::post('asignarpermiso','RolController@asignarPermiso');
		Route::post('quitarpermiso','RolController@quitarPermiso');
		Route::get('verPermisos/{id_rol}','RolController@verPermisos');
	});


	Route::group(['prefix' => 'permiso'], function() {
	    Route::get('getdata','PermisoController@getData');
		Route::post('create','PermisoController@create');
		Route::post('update/{id_rol}','PermisoController@update');
		Route::delete('delete/{id_rol}','PermisoController@delete');
	});


	Route::group(['prefix' => 'opcion'], function() {
	    Route::get('getdata','OpcionController@getData');
		Route::post('create','OpcionController@create');
		Route::post('update/{id}','OpcionController@update');
		Route::delete('delete/{id}','OpcionController@delete');
		Route::post('asignarsubopcion','OpcionController@asignarSubOpciones');
		Route::post('quitarsubopcion','OpcionController@quitarSubOpciones');
		Route::get('versubopcion/{id_opcion}','OpcionController@verSubopciones');
	});


	Route::group(['prefix' => 'subopcion'], function() {
	    Route::get('getdata','SubOpcionController@getData');
		Route::post('create','SubOpcionController@create');
		Route::post('update/{id}','SubOpcionController@update');
		Route::delete('delete/{id}','SubOpcionController@delete');
	});


	Route::group(['prefix' => 'usuario'], function() {
		Route::get('getdata','UsuarioController@getData');
	    Route::post('create','UsuarioController@create');
	    Route::post('update/{id}','UsuarioController@update');
	    Route::delete('delete/{id}','UsuarioController@delete');
		// Route::post('auth','UsuarioController@auth');
		Route::get('logout','UsuarioController@logout');
		Route::post('listar_menu/{usuario_id}','UsuarioController@listarMenu');
		Route::post('listar_permisos/{usuario_id}','UsuarioController@listarPermisos');
		Route::post('asignar_subopcion','UsuarioController@asignedSubOpcionToUser');
		Route::post('asignar_permiso','UsuarioController@asignedPermisoToUser');
		Route::post('quitar_subopcion','UsuarioController@unsignedSubOpcionToUser');
		Route::post('quitar_permiso','UsuarioController@unsignedPermisoToUser');
	});

########################## FIN MÓDULO DE USUARIOS #######################

########################## MÓDULO DE MANTENIMIENTOS #####################

	Route::group(['prefix' => 'semana'], function() {
	    Route::get('getdata','SemanaController@getSemanaWithContenedores');	    
	    Route::get('getdata/{anio_id}','SemanaController@getSemanaWithContenedoresParams');
		Route::post('create','SemanaController@create');
		Route::post('update/{id}','SemanaController@update');
		Route::delete('delete/{id}','SemanaController@delete');

		Route::get('close/{id}','SemanaController@close');
	});

	Route::group(['prefix' => 'puerto_destino'], function() {
	    Route::get('getdata','PuertoDestinoController@getData');
		Route::post('create','PuertoDestinoController@create');
		Route::post('update/{id}','PuertoDestinoController@update');
		Route::delete('delete/{id}','PuertoDestinoController@delete');
	});

	Route::group(['prefix' => 'operador'], function() {
	    Route::get('getdata','OperadorController@getData');
		Route::post('create','OperadorController@create');
		Route::post('update/{id}','OperadorController@update');
		Route::delete('delete/{id}','OperadorController@delete');
		Route::post('withContenedores/{id_operador}','OperadorController@whitContenedores');
		Route::post('withArchivos/{id_operador}','OperadorController@whitArchivos');
	});

	Route::group(['prefix' => 'linea_naviera'], function() {
	    Route::get('getdata','LineaNavieraController@getData');
		Route::post('create','LineaNavieraController@create');
		Route::post('update/{id}','LineaNavieraController@update');
		Route::delete('delete/{id}','LineaNavieraController@delete');
	});

	Route::group(['prefix' => 'cliente'], function() {
	    Route::get('getdata','ClienteController@getData');
		Route::post('create','ClienteController@create');
		Route::post('update/{id}','ClienteController@update');
		Route::delete('delete/{id}','ClienteController@delete');
		Route::post('withContenedores/{id_cliente}','ClienteController@whitContenedores');
		Route::post('withArchivos/{id_cliente}','ClienteController@whitArchivos');
		Route::post('asignaroperador','ClienteController@asignarOperadores');
		Route::post('quitaroperador','ClienteController@quitarOperadores');
		Route::get('veroperadores/{id_cliente}','ClienteController@verOperadores');
		Route::post('asignarcaja','ClienteController@asignarCajas');
		Route::post('quitarcaja','ClienteController@quitarCajas');
		Route::get('vercajas/{id_cliente}','ClienteController@verCajas');
		Route::post('operadoresAsigned/{id_cliente}','ClienteController@operadoresAsigned');
		Route::post('cajasAsigned/{id_cliente}','ClienteController@cajasAsigned');
		// para los emails
		Route::get('vercorreos/{id_cliente}','ClienteController@getCorreos');
		Route::post('agregarcorreos','ClienteController@agregarEmails');
		Route::post('actualizarcorreos/{id_cliente}','ClienteController@actualizarEmails');
		Route::post('eliminarcorreos/{id_email}','ClienteController@eliminarEmails');
	});

	Route::group(['prefix' => 'caja'], function() {
	    Route::get('getdata','CajaController@getData');
		Route::post('create','CajaController@create');
		Route::post('update/{id}','CajaController@update');
		Route::delete('delete/{id}','CajaController@delete');
	});

	Route::group(['prefix' => 'cargo'], function() {
	    Route::get('getdata','CargoController@getData');
		Route::post('create','CargoController@create');
		Route::post('update/{id}','CargoController@update');
		Route::delete('delete/{id}','CargoController@delete');
	});

	Route::group(['prefix' => 'booking'], function() {
	    Route::get('getdata','BookingController@getData');
		Route::post('create','BookingController@create');
		Route::post('update/{id}','BookingController@update');
		Route::delete('delete/{id}','BookingController@delete');
	});

	Route::group(['prefix' => 'nave'], function() {
	    Route::get('getdata','NaveController@getData');
		Route::post('create','NaveController@create');
		Route::post('update/{id}','NaveController@update');
		Route::delete('delete/{id}','NaveController@delete');
	});

	Route::group(['prefix' => 'estado'], function() {
	    Route::get('getdata','EstadoController@getData');
	    Route::post('create','EstadoController@create');
		Route::post('update/{id}','EstadoController@update');
		Route::delete('delete/{id}','EstadoController@delete');
	});

	Route::group(['prefix' => 'pais'], function() {
	    Route::get('getdata','PaisController@getData');
	    Route::post('create','PaisController@create');
		Route::post('update/{id}','PaisController@update');
		Route::delete('delete/{id}','PaisController@delete');
		Route::post('with_ciudades/{id}','PaisController@withCiudades');
	});

	Route::group(['prefix' => 'ciudad'], function() {
	    Route::get('getdata','CiudadController@getData');
	    Route::post('create','CiudadController@create');
		Route::post('update/{id}','CiudadController@update');
		Route::delete('delete/{id}','CiudadController@delete');
	});


	Route::group(['prefix' => 'anio'], function() {
	    Route::get('getdata','AnioController@getData');
	    Route::get('give_semanas','AnioController@getAnioGiveSemanas'); // listar solo los años que tienen semanas aperturadas
		Route::post('create','AnioController@create');
		Route::post('update/{id}','AnioController@update');
		Route::delete('delete/{id}','AnioController@delete');
		Route::get('withSemana/{id}','AnioController@withSemana');
	});

	Route::group(['prefix' => 'estado_archivo'], function() {
	    Route::get('getdata','EstadoArchivoController@getData');
	});

	Route::group(['prefix' => 'sector'], function() {
		Route::get('getdata','SectorController@getData');
		Route::post('create','SectorController@create');
		Route::post('update/{id}','SectorController@update');
		Route::delete('delete/{id}','SectorController@delete');
	});

	Route::group(['prefix' => 'empacadora'], function() {
		Route::get('getdata','EmpacadoraController@getData');
		Route::post('create','EmpacadoraController@create');
		Route::post('update/{id_empacadora}','EmpacadoraController@update');
		Route::delete('delete/{id_empacadora}','EmpacadoraController@delete');


		Route::post('getproductores','EmpacadoraController@getProductores');
	});

	Route::group(['prefix' => 'parentesco'], function() {
	    Route::get('getdata','ParentescoController@getData');
		Route::post('create','ParentescoController@create');
		Route::post('update/{id}','ParentescoController@update');
		Route::delete('delete/{id}','ParentescoController@delete');
	});

	Route::group(['prefix' => 'motivo'], function() {
		Route::get('getdata','MotivoController@getData');
	});


########################## FIN MANTENIMIENTOS ###############################

########################## MÓDULO DE CONTAINER ##############################
	Route::group(['prefix' => 'contenedor'], function() {
		Route::get('getdata/','ContenedorController@getData');
	    Route::post('create','ContenedorController@create');
	    Route::post('update/{id}','ContenedorController@update');
	    Route::post('update_operador/{id}','ContenedorController@updateByOperador');
	    
	    Route::delete('delete/{id}','ContenedorController@destroy');
	    Route::post('cambiar_estado','ContenedorController@cambiarEstado');
	    Route::post('eliminar_caja','ContenedorController@eliminarCaja');
	    Route::post('actualizar_caja','ContenedorController@actualizarCaja');
	    Route::post('show/{id_semana}','ContenedorController@contenedoresPorSemana');
	    Route::post('descargarzip/{id_contenedor}','ContenedorController@descargarZip');
	    // ruta para listar contenedores por cliente y semana (valija)
	    Route::post('ofvalijaClienteAndSemana','ContenedorController@contenedoresOfValijaPorClienteAndSemana');
	    Route::post('ofcertificadoClienteAndSemana','ContenedorController@contenedoresOfCertificadoPorClienteAndSemana');
		// ruta para listar contenedores por cliente, semana y operador (valija)
		Route::post('ofClienteAndSemanaAndOperador','ContenedorController@contenedoresPorClienteAndSemanaAndOperador');

		Route::get('sin_llenado/{id_semana}','ContenedorController@contenedoresSinLlenado');
	});

	Route::group(['prefix' => 'factura'], function() {
	    // Route::post('create/{id_contenedor}','FacturaController@create');
	    Route::post('create','FacturaController@create');
	    // Route::post('show/{id_contenedor}','FacturaController@getData');
	    Route::post('show/{id_semana}','FacturaController@getData');
	    Route::delete('delete/{id_factura}','FacturaController@destroy');
	    Route::post('download/{id}','FacturaController@download');
	    Route::post('enviar_email/{id}','FacturaController@enviarEmail');
	});

	Route::group(['prefix' => 'certificado'], function() {
		Route::post('create','CertificadoController@create');
	    Route::post('show/{id_semana}','CertificadoController@getData');
	    Route::delete('delete/{id_certificado}','CertificadoController@destroy');
	    Route::post('download/{id}','CertificadoController@download');
	    Route::post('cambiar_estado','CertificadoController@cambiarEstado');
	    Route::post('eliminar_contenedor','CertificadoController@eliminarContenedor');
		Route::post('agregar_contenedor','CertificadoController@agregarContenedor');
		Route::post('enviar_email/{id}','CertificadoController@enviarEmail');
	});

	Route::group(['prefix' => 'packing'], function() {
		// Route::post('create/{id_contenedor}','PackingController@create');
		Route::post('create','PackingController@create');
		Route::post('show/{id_semana}','PackingController@getData');
		Route::post('download/{id}','PackingController@download');
		Route::delete('delete/{id_packing}','PackingController@destroy');
		Route::post('enviar_email/{id}','PackingController@enviarEmail');
	});

	Route::group(['prefix' => 'vl'], function() {
		// Route::post('create/{id_contenedor}','PackingController@create');
		Route::post('create','VlController@create');
		Route::post('show/{id_semana}','VlController@getData');
		Route::post('download/{id}','VlController@download');
		Route::delete('delete/{id_Vl}','VlController@destroy');
		Route::post('enviar_email/{id}','VlController@enviarEmail');
	});

	Route::group(['prefix' => 'valija'], function() {
		Route::post('create','ValijaController@create');
		Route::post('show/{id_semana}','ValijaController@getData');
		Route::post('download/{id_valija}','ValijaController@download');
		Route::delete('delete/{id_valija}','ValijaController@destroy');
		Route::post('eliminar_contenedor','ValijaController@eliminarContenedor');
		Route::post('agregar_contenedor','ValijaController@agregarContenedor');
		Route::post('enviar_email/{id}','ValijaController@enviarEmail');
	});

	Route::group(['prefix' => 'otros_archivos'], function() {
		// Route::post('create/{id_contenedor}','PackingController@create');
		Route::post('create','OtrosArchivosController@create');
		Route::post('show/{id_semana}','OtrosArchivosController@getData');
		Route::post('download/{id}','OtrosArchivosController@download');
		Route::delete('delete/{id_otro_archivo}','OtrosArchivosController@destroy');
		Route::post('enviar_email/{id}','OtrosArchivosController@enviarEmail');
	});
########################## FIN MÓDULO DE CONTAINER ##########################



########################## MÓDULO DE REPORTES ###############################
	Route::group(['prefix' => 'excel'], function() {
	    Route::post('contenedores/{id_semana}','ExcelController@reporteSemana');
	    Route::post('contenedores_semana','ExcelController@reporteContenedoresPorSemana');
	    Route::post('contenedores_cliente','ExcelController@reporteContenedoresPorCliente');
	    Route::post('contenedores_operador','ExcelController@reporteContenedoresPorOperador');
	    Route::post('movimientos_contenedor','ExcelController@reporteMovimientosContenedores');
	    Route::post('detalle_contenedor/{id_contenedor}','ExcelController@reporteDetalleContenedor');
	    Route::post('cajas_exportadas','ExcelController@reporteResumenCajasExportadas');
	    Route::post('auditoria','ExcelController@reporteAuditorias');
	    Route::post('buscar_contenedor','ExcelController@reporteBuscarContenedor');
	    Route::get('sectores_with_delegados','ExcelController@reporteSectoresConDelegados');	    
	    Route::post('inspector_with_parcelas','ExcelController@reporteInspectorConParcelas');

	    Route::post('sic','ExcelController@reporteSIC');

	    Route::post('enfunde_empacadoras','ExcelController@obtenerEmpacadoraEnfunde');
	});
	Route::group(['prefix' => 'data'], function() {
	    Route::get('contenedores/{id_semana}','ExcelController@datareporteSemana');
	    Route::post('contenedores_semana','ExcelController@datareporteContenedoresPorSemana');
	    Route::post('contenedores_cliente','ExcelController@datareporteContenedoresPorCliente');
	    Route::post('contenedores_operador','ExcelController@datareporteContenedoresPorOperador');
	    Route::post('movimientos_contenedor','ExcelController@datareporteMovimientosContenedores');
	    Route::post('buscar_contenedor','ExcelController@datareporteBuscarContenedor');
	});
########################## FIN MÓDULO DE REPORTES ###########################


########################## MÓDULO DE GRÁFICOS ###############################
	Route::group(['prefix' => 'grafico'], function() {
	    // Route::post('contenedores_cliente/{id_anio}','GraficoController@contenedoresPorCliente');
	    Route::post('contenedores_cliente_new/{id_anio}','GraficoController@contenedoresPorClienteNew');
	    Route::get('contenedores_anio_new','GraficoController@contenedoresPorAnioNew');
	    Route::post('contenedores_semana_new','GraficoController@ContenedoresPorSemanaNew');
	});
########################## FIN MÓDULO DE GRÁFICOS ###########################


########################## MÓDULO DE CONFIGURACIÓN DE CUENTA ################
	Route::group(['prefix' => 'cuenta'], function() {
	    Route::post('update_persona/{id_persona}','CuentaController@updatePersona');
	    Route::post('update_nick/{id_usuario}','CuentaController@updateNick');
	    Route::post('update_password/{id_usuario}','CuentaController@updatePassword');
	    Route::post('validar_nick/{nick}','CuentaController@validarNick');
	});
########################## FIN DE CONFIGURACIÓN DE CUENTA ###################

########################## LISTAR LAS TABLAS DE LA BASE DE DATOS  ###################
	Route::get('tablas/getdata','AuditoriaController@getData');
########################## FIN LISTAR LAS TABLAS DE LA BASE DE DATOS  ###############

########################### MÓDULO DE PRODUCTORES  ##################################

	Route::group(['prefix' => 'productor'], function() {
		Route::get('getdata','ProductorController@getData');
	    Route::post('create','ProductorController@create');
	    Route::post('update/{id_productor}','ProductorController@update');
	    Route::delete('delete/{id_productor}','ProductorController@delete');
	    Route::post('cambiar_condicion','ProductorController@changeConditionProductor');
	    Route::post('obtener_parcelas/{id_productor}','ProductorController@getParcelasPorProductor');
	    Route::get('obtener_trabajadores/{id_productor}','TrabajadorController@getTrabajadoresPorProductor');
	    
	    Route::get('getCodes','ProductorController@listCode');
	    Route::post('buscar','BusquedaProductorController@buscar_productor');

	    Route::post('validar_captcha','BusquedaProductorController@validar_captcha');
	});

	Route::group(['prefix' => 'trabajador'], function() {
		Route::get('getdata','TrabajadorController@getData');
	    Route::post('create','TrabajadorController@create');
	    Route::post('update/{id_productor}','TrabajadorController@update');
	    Route::delete('delete/{id_productor}','TrabajadorController@delete');
	});

	Route::group(['prefix' => 'parcela'], function() {
		Route::get('getdata','ParcelaController@getData');
	    Route::post('create','ParcelaController@create');
	    Route::post('update/{id}','ParcelaController@update');
	    Route::delete('delete/{id}','ParcelaController@delete');
	    Route::post('enabled','ParcelaController@enabled');
	    Route::post('disabled','ParcelaController@disabled');

	    Route::get('get_codigos','ParcelaController@getCodigos');
	});


	// nota: esto hay que ponerlo dentro de la autenticación, lo dejé afuera
	// pora probar las rutas
	Route::group(['prefix' => 'sector'], function() {
		Route::get('obtener_productores/{id_sector}','SectorController@getProductoresPorSector');
		Route::get('obtener_delegados/{id_sector}','SectorController@getDelegados');
		Route::post('create_delegado','SectorController@createDelgado');
		Route::get('delete_delegado/{id_delegado}','SectorController@deleteDelegado');
	});
########################### MÓDULO DE PRODUCTORES  ##################################

	Route::post('download/pdf_consulta_publica','BusquedaProductorController@getPDFProductores');
	// Route::get('download/pdf_consulta_publica','BusquedaProductorController@getPDFProductores');

########################## LISTAR CONDICIONES DE PRODUCTOR  ###################
	Route::group(['prefix' => 'condicion_productor'], function() {
		Route::get('getdata','CondicionProductorController@getData');
	});
########################## FIN LISTAR CONDICIONES DE PRODUCTOR  ###############

########################### MÓDULO DE INSPECTORES ################################

	Route::group(['prefix' => 'inspector'], function() {
		Route::get('getdata','InspectorController@getData');
	    Route::post('create','InspectorController@create');
	    Route::post('update/{id_inspector}','InspectorController@update');
	    Route::delete('delete/{id_inspector}','InspectorController@delete');

	    Route::post('getparcelas','InspectorController@getParcelas');
	});

	Route::group(['prefix' => 'galeria_visita'], function() {
		Route::get('getdata/{visita_id}','GaleriaVisitaController@getImagenesVisita');
	    Route::post('create','GaleriaVisitaController@createImagenesVisita');
	    Route::delete('delete/{imagen_id}','GaleriaVisitaController@deleteImagenesVisita');
	});


	Route::group(['prefix' => 'menu'], function() {
	    Route::get('getdata','MenuController@getData');
		Route::post('create','MenuController@create');
		Route::post('update/{id_menu}','MenuController@update');
		Route::delete('delete/{id_menu}','MenuController@delete');
	});

	Route::group(['prefix' => 'formulario_auditoria_interna'], function() {
	    Route::get('getdata','FormularioAuditoriaInternaController@getData');
		Route::post('create','FormularioAuditoriaInternaController@create');
		Route::post('update/{id_formulario}','FormularioAuditoriaInternaController@update');
		Route::delete('delete/{id_formulario}','FormularioAuditoriaInternaController@delete');
	});

	Route::group(['prefix' => 'labor'], function() {
	    Route::get('getdata','LaborController@getData');
		Route::post('create','LaborController@create');
		Route::post('update/{id_labor}','LaborController@update');
		Route::delete('delete/{id_labor}','LaborController@delete');
	});

	Route::group(['prefix' => 'item_labor'], function() {
	    Route::get('getdata','ItemLaborController@getData');
		Route::post('create','ItemLaborController@create');
		Route::post('update/{id_item}','ItemLaborController@update');
		Route::delete('delete/{id_item}','ItemLaborController@delete');
	});



	Route::group(['prefix' => 'visita_auditoria_interna'], function() {
		Route::get('getdata/{id_parcela}','VisitaAuditoriaInternaController@getData');
	    Route::post('create','VisitaAuditoriaInternaController@create');
	    Route::post('update/{id_visita}','VisitaAuditoriaInternaController@update');
	    Route::delete('delete/{id_visita}','VisitaAuditoriaInternaController@delete');


	    Route::post('exportPDF/{id_visita}','VisitaAuditoriaInternaController@exportPDF');
	});

	Route::group(['prefix' => 'visita_formulario_dos'], function() {
		Route::get('getdata/{id_parcela}','VisitaItemLaborController@getData');
	    Route::post('create','VisitaItemLaborController@create');
	    Route::post('update/{id_visita}','VisitaItemLaborController@update');
	    Route::delete('delete/{id_visita}','VisitaItemLaborController@delete');


	    Route::post('exportPDF/{id_visita}','VisitaItemLaborController@exportPDF');
	});
########################### FIN MÓDULO DE INSPECTORES #############################

########################### MÓDULO DE ENFUNDE ################################
	Route::group(['prefix' => 'cinta'], function() {
		Route::get('getdata','CintaController@getData');
	    Route::post('create','CintaController@create');
	    Route::post('update/{id_cinta}','CintaController@update');
	    Route::delete('delete/{id_cinta}','CintaController@delete');
	});

	Route::group(['prefix' => 'enfunde'], function () {
	    Route::get('getdata', 'EnfundeController@getData');
	    Route::post('create', 'EnfundeController@create');
	    Route::post('update/{enfunde_id}', 'EnfundeController@update');

	    Route::get('getdataempacadora', 'EnfundeController@obtenerEmpacadoraData');
	});

	Route::group(['prefix' => 'llenado'], function () {
	    Route::get('getdata/{id_semana}', 'LlenadoController@getData');
	    Route::post('create', 'LlenadoController@create');
	    Route::post('update/{id_llenado}', 'LlenadoController@update');
	    Route::delete('delete/{id_llenado}','LlenadoController@destroy');

	    Route::post('packing/{id_contenedor}','LlenadoController@packingLlenado');
	    Route::post('pdf/{id_contenedor}','LlenadoController@pdfLlenado');

	    Route::post('get_packing/{id_semana}', 'LlenadoController@getPacking');
	});

	Route::group(['prefix' => 'recobro'], function () {
	    Route::get('getdata/{id_semana}', 'RecobroController@getData');
	    Route::post('create', 'RecobroController@create');
	    Route::post('update/{enfunde_id}', 'RecobroController@update');
	});
########################### FIN MÓDULO DE ENFUNDE ################################

});