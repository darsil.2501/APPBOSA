<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Persona;
use Hash;
use DB;

class CuentaController extends Controller
{

   public function updatePersona(Request $request,$id_persona)
   {
        try{
            DB::beginTransaction();
                $persona = Persona::find($id_persona);
                $persona->fill([
                    'dni'               =>$request['dni'],
                    'nombre'            =>$request['nombre'],
                    'apellido_paterno'  =>$request['apellido_paterno'],
                    'apellido_materno'  =>$request['apellido_materno'],
                    'fullname'          =>$request['nombre'].' '.$request['apellido_paterno'].' '.$request['apellido_materno'],
                    'email'             =>$request['email'],
                    'telefono'          =>$request['telefono']
                ]);
                $persona->save();
            DB::commit();
            return self::MensajePersonalizado(true,'Datos personales actualizado correctamente.');   
        }catch(\Exception $ex){
            DB::rollback();
            return self::MensajePersonalizado(false,'No se pudieron actualizar los datos, inténtelo nuevamente.');
        }
   }

   public function updateNick(Request $request,$id_usuario)
   {
        try{
            if(!self::hasPermiso('usuario.actualizar_usuario')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $usuario = User::find($id_usuario);
                $usuario->fill(['nick'=>$request['nick']])->save();
            DB::commit();
            return self::MensajePersonalizado(true,'Nombre de usuario actualizado correctamente.');
        }catch(\Exception $ex){
            DB::rollback();
            return self::MensajePersonalizado(false,'No se pudieron actualizar los datos, inténtelo nuevamente.');
        }
   }

   public function updatePassword(Request $request,$id_usuario)
   {
        try{
            if(!self::hasPermiso('usuario.actualizar_password')){ return self::HasNoPermiso(); }
            $usuario = User::find($id_usuario);
            if(Hash::check($request['password_actual'],$usuario->password)){
                $usuario->password = Hash::make($request['password_nuevo']);
                $usuario->save();
                return self::MensajePersonalizado(true,'Contraseña actualizada correctamente.');
            }else{
                return self::MensajePersonalizado(false,'La contraseña actual no coincide con la del sistema.');
            }
        }catch(\Exception $ex){
            return self::MensajePersonalizado(false,'No se pudieron actualizar los datos, inténtelo nuevamente.');
        }
   }

   public function validarNick($nick)
   {
        $usuario = User::where('nick',$nick)->first();
        if(count($usuario) > 0){
            return self::MensajePersonalizado(false,'El nombre de usuario ya está en uso.');
        }else{
            return self::MensajePersonalizado(true,'Nombre de usuario disponible');
        }
    }
}
