<?php

namespace App\Http\Controllers;

use App\Certificado;
use App\Contenedor;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Semana;
use App\Traits\TraitAuditoria;
use App\Traits\TraitCertificado;
use App\Traits\TraitFunciones;
use App\Traits\TraitEmail;;
use DB;
use Illuminate\Http\Request;

class CertificadoController extends Controller
{
    use TraitFunciones, TraitCertificado, TraitAuditoria, TraitEmail;

    public function getData($id_semana)
    {
        try{
            $certificados = self::getCertificados($id_semana);
            return response()->json(['success'=>true,'info'=>$certificados]);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'info'=>'Error al listar los certificados'.$ex->getMessage()]);
        }
    }

   public function create(Request $request)
    {
        try
        {
            if(!self::hasPermiso('certificados.registrar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $file          = $request['files'];
                $i             = self::getCantidadArchivos('certificado',$request['id_cliente']);
                $semana        = Semana::findOrFail($request['id_semana']);
                $nombre_semana = $semana->nombre;
                $nombre_anio   = $semana->anio->anio;

                foreach ($file as $value) {
                    // formato del nombre 1_20_2017_(1).Certificado_001.pdf
                    // id_cliente,semana,año_actual,cantidad_archivo,nombre_archivo_default
                    $nombre = $request['id_cliente'].'_'.$nombre_semana.'_'.$nombre_anio.'_'.'('.$i.')'.$value->getClientOriginalName();
                    $certificado = Certificado::create([
                        'id_cliente'   => $request['id_cliente'],
                        'id_semana'    => $request['id_semana'],
                        'id_operador'  => $request['id_operador'],
                        'descripcion'  => $request['descripcion'],
                        'nombre'       => $nombre,
                        'fecha'        => date('Y-m-d'),
                        'hora'         => date('H:i:s'),
                        'url'          => $nombre
                    ]);
                    $i++;

                    // $certificado = Certificado::find($certificado->id);
                    // registramos los contenedores que le pertenecen a ese certificado
                    if (count($request['contenedores']) > 0) {
                        foreach ($request['contenedores'] as $contenedor) {
                            $certificado->contenedores()->attach($contenedor['id_contenedor']);
                            $contenedor = Contenedor::find($contenedor['id_contenedor']);
                            $contenedor->fill(['certificado'=>DB::raw(2)])->save();
                        }
                        \Storage::disk('certificado')->put($nombre,  \File::get($value));
                    }else{
                        return response()->json(['success'=>false,'message'=>'No ha seleccionado contenedores para el certificado.']);
                    }
                    // \Storage::disk('certificado')->put($nombre,  \File::get($value));
                    // self::changeStatusContenedor(2,'certificado',$request['id_cliente'],$request['id_semana']);
                    self::auditar('audit_certificado',$nombre,'INSERTAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Archivos subidos correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al subir archivos.'.$ex->getMessage()]);
        }
    }


    public function destroy($id)
    {
        try{
            if(!self::hasPermiso('certificados.eliminar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $certificado = Certificado::findOrFail($id);
                if ($certificado) {
                    // Primero cambiamos de estado los contenedores que tiene asociada ese certificado
                    // y los eliminamos de la tabla intermedia certificado_contenedor
                    foreach ($certificado->contenedores as $value) {
                        $contenedor = Contenedor::find($value['id']);
                        $contenedor->fill(['certificado'=>DB::raw(1)])->save();
                        $certificado->contenedores()->detach($value['id']);
                    }
                    $certificado->delete();
                    $archivo = public_path('archivos\\certificado\\'.$certificado->nombre);
                    //$archivo = public_path('archivos/certificado/'.$certificado->nombre);
                    if (file_exists($archivo)) {
                        \Storage::delete('certificado/'.$certificado->nombre); 
                    }
                    self::changeStatusContenedor(1,'certificado',$certificado->id_cliente,$certificado->id_semana);
                    self::auditar('audit_certificado',$certificado->nombre,'ELIMINAR');
                }
                // cambiar de estado el tipo de archivo
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Arhivo eliminado correctamente.']);
        }catch(Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el archivo.']);
        }
    }

    public function download($id)
    {
        try{
            $certificado = Certificado::findOrFail($id);
            $archivo = public_path('archivos\\certificado\\'.$certificado->nombre);
            //$archivo = public_path('archivos/certificado/'.$certificado->nombre);
            if(file_exists($archivo)){
                return response()->download($archivo);    
            }
                return response()->json(['success'=>false,'message'=>'El archivo que intenta descargar no existe']);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al descargar el archivo.']);
        }
    }
    public function agregarContenedor(Request $request)
    {
        try{
            DB::beginTransaction();
                $certificado = Certificado::find($request['id_certificado']);
                foreach ($request['contenedores'] as $value) {
                    $certificado->contenedores()->attach($value['id_contenedor']);
                    $contenedor = Contenedor::find($value['id_contenedor']);
                    $contenedor->fill(['certificado'=>DB::raw(2)])->save();
                    self::auditar('audit_certificado',$certificado->nombre.'-'.$contenedor->referencia,'AGREGAR CONTENEDOR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Contenedores agregados correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al agregar los contenedores, inténtelo nuevamente. Error: '.$ex->getMessage()]);
        }
    }

    public function eliminarContenedor(Request $request)
    {
        try{
            DB::beginTransaction();
                $certificado = Certificado::find($request['id_certificado']);
                if(count($certificado->contenedores) > 1){
                    $contenedor = Contenedor::find($request['id_contenedor']);
                    $contenedor->fill(['certificado'=>DB::raw(1)])->save();
                    $certificado->contenedores()->detach($request['id_contenedor']);
                    self::auditar('audit_certificado',$certificado->nombre.'-'.$contenedor->referencia,'ELIMINAR CONTENEDOR');
                }else{
                    return response()->json(['success'=>false,'message'=>'El certificado debe tener al menos un contenedor registrado.']);           
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Contenedor eliminado correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'No se puedo eliminar el contenedor, inténtelo nuevamente. Error: '.$ex->getMessage()]);
        }
    }

    public function cambiarEstado(Request $request)
    {
        $certificado = Certificado::find($request['id_certificado']);
        self::changeStatusContenedoresOfCertificado($request);
        return self::changeStatusCertificado($request);
    }

    public function enviarEmail($id_certificado)
    {   
        if(!self::hasPermiso('emails.enviar')){ return self::HasNoPermiso(); }
        $certificado = Certificado::find($id_certificado);
        $cliente     = $certificado->cliente;
        $semana      = $certificado->semana->nombre;

        // cambiamos de estado a enviado a los contenedores
        if ($certificado) {
            foreach ($certificado->contenedores as $value) {
                $contenedor = Contenedor::find($value['id']);
                $contenedor->fill(['certificado'=>DB::raw(3)])->save();
            }
        }
        return self::sendEmailCliente('certificado',$certificado->url,$cliente,$semana);
    }
}
