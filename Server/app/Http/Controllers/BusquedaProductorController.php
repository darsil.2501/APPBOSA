<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Persona;
use App\Traits\TraitParcela;
use Illuminate\Http\Request;

class BusquedaProductorController extends Controller
{
    use TraitParcela;

    public function validar_captcha(Request $request)
    {
        $secret = '6LdPaS8UAAAAAP6sw-0J-MqXE_PuElufznsh-QDb';
        $ip = $_SERVER['REMOTE_ADDR'];
        $validation = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$request['recaptcha']."&remoteip=".$ip."");
        return $validation;
    }

    public function buscar_productor(Request $request)
    {        
        $productores = Persona::has('productor')->where('activo',1);
        if(!empty($request->dni))
        {
            $productores = $productores->where('dni',$request->dni);
        }
        if(!empty($request->nombres))
        {
            $productores = $productores->where('fullname','like','%'.$request->nombres.'%');
        }
        $productores = $productores->get();

        $data = array();
        foreach ($productores as $value) {
            $data[] = array(
                'codigo_productor' => $value->productor->codigo_productor,
                'dni'              => $value->dni,
                'fullname'         => $value->fullname,
                'ggn'              => $value->productor->ggn,
                'fecha_ingreso'    => $value->productor->fecha_ingreso,
                'socio'            => $value->productor->socio,
                'nombre_condicion' => $value->productor->condicion_productor->nombre,
                'color_condicion'  => $value->productor->condicion_productor->color,
                'numero_parcelas'  => $value->productor->parcelas->count()
            );
        }
        return response()->json(['success'=>true,'info'=>$data]);   
    }

    public function getPDFProductores(Request $request)
    {
        // $personas = Persona::has('productor')->where('activo',1)->where('fullname','like','%luis%')->get();
        $personas = Persona::has('productor')->where('activo',1);
        if(!empty($request->dni))
        {
            $personas = $personas->where('dni',$request->dni);
        }
        if(!empty($request->nombres))
        {
            $personas = $personas->where('fullname','like','%'.$request->nombres.'%');
        }
        $personas = $personas->get();

        $data_productor = array();
        foreach ($personas as $persona) {
            $productor = new \stdClass;
            $productor->codigo_productor= $persona->productor->codigo_productor;
            $productor->dni             = $persona->dni;
            $productor->fullname        = $persona->fullname;
            $productor->socio           = $persona->productor->socio;
            $productor->ggn             = $persona->productor->ggn;
            $productor->fecha_ingreso   = $persona->productor->fecha_ingreso;
            $productor->fecha_cese      = $persona->productor->fecha_cese;
            $productor->nombre_condicion= $persona->productor->condicion_productor->nombre;
            $productor->color_condicion = $persona->productor->condicion_productor->color;
            $productor->numero_parcelas = $persona->productor->parcelas->count();
            $data_productor[] = $productor;
        }

        // $view = \View::make('consulta.productores',['productores'=>$data_productor])->render();
        // $pdf = \App::make('dompdf.wrapper');
        // $pdf->loadHTML($view);
        // return $pdf->download('consulta_productores.pdf'); 
        
        $pdf = \PDF::loadView('consulta.productores',['productores'=>$data_productor]);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download('consulta_productores.pdf');
        // return view('consulta.productores',['productores'=>$data_productor]);

    }
}
