<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Traits\TraitVisitaItemLabor;
use Illuminate\Http\Request;

class VisitaItemLaborController extends Controller
{
    use TraitVisitaItemLabor;

    public function getData($id_parcela)
    {
        return self::obtenerVisitasItemLabor($id_parcela);
    }

    public function create(Request $request)
    {
        return self::registrarVisitaItemLabor($request);
    }

    public function update(Request $request,$id_visita)
    {
        return self::actualizarVisitaItemLabor($request, $id_visita);
    }

    public function delete($id_visita)
    {
        return self::eliminarVisitaItemLabor($id_visita);
    }

    public function exportPDF($id_visita)
    {
        $visita = \App\Visita::find($id_visita);

        $data_visita = array(
            'id'                => $visita->id,
            'fecha'             => $visita->fecha, 
            'observacion'       => $visita->observacion, 
            'responsable'       => $visita->responsable, 
            'productor'         => $visita->parcela->productor->persona->fullname, 
            'codigo_productor'  => $visita->parcela->productor->codigo_productor, 
            'inspector'         => $visita->parcela->inspector->persona->fullname, 
            'area_parcela'      => $visita->parcela->ha_total,
            'empacadora'        => $visita->parcela->empacadora->nombre, 
            'sector'            => $visita->parcela->sector->nombre  
        );

        $items = $visita->visitas_item_labor;

        $data_formulario = array();
        $data_labor = array();
        foreach ($items as $key => $item) {
            $data_formulario[] = array(
                'id'            => $item->id,
                'total'         => $item->total,
                'nombre'        => $item->item_labor->nombre,
                'punto_evaluar' => $item->item_labor->punto_evaluar,
                'id_labor'      => $item->item_labor->id_labor
            );

            $data_labor[] = array(
                'id'     => $item->item_labor->labor->id,
                'nombre' => $item->item_labor->labor->nombre
            );
        }

        $collection = collect($data_labor);
        $data_labor = $collection->unique('id');
        $data_labor = $data_labor->values()->all();

        $data_labores = array();
        foreach ($data_labor as $labor) {
            $data_items = array();
            $total_labor = 0;
            foreach ($data_formulario as $item) {
                if ($item['id_labor'] === $labor['id']) {
                    $total_labor += $item['total'];
                    $data_items[] = array(
                        'id'            => $item['id'],
                        'total'         => $item['total'],
                        'nombre'        => $item['nombre'],
                        'punto_evaluar' => $item['punto_evaluar']
                    );
                }
            }

            $data_labores[] = array(
                'id'          => $labor['id'],
                'nombre'      => $labor['nombre'],
                'items'       => $data_items,
                'total_labor' => $total_labor
            );
        }

        $data_imagenes = $visita->galeria_visita;

        $data = array(
            'formulario' => $data_labores,
            'visita'     => $data_visita,
            'imagenes'   => $data_imagenes
        );

        return self::exportPDFVisitaLabores($data);
    }
}
