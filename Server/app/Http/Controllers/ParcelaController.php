<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ParcelaRequest;
use App\Parcela;
use App\Traits\TraitParcela;
use DB;
use Illuminate\Http\Request;

class ParcelaController extends Controller
{
    use TraitParcela;

    public function create(ParcelaRequest $request)
    {
        return self::registrarParcela($request);
    }

    public function update(ParcelaRequest $request, $parcela_id)
    {
        return self::actualizarParcela($request, $parcela_id);
    }

    public function delete($id)
    {
        return self::eliminarParcela($id);
    }

    public function enabled(Request $request)
    {
        return self::habilitarParcela($request);
    }

    public function disabled(Request $request)
    {
        return self::deshabilitarParcela($request);
    }

    public function getCodigos()
    {
        return self::getCodigosParcela();
    }
}
