<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LineaNaviera;
use DB;
use App\Http\Requests\LineaNavieraRequest;

class LineaNavieraController extends Controller
{
    public function getData()
    {
        try{
            $lineas_navieras = LineaNaviera::where('activo',1)->get();
            return response()->json(['info'=>$lineas_navieras,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>false]);    
        }
    }

    public function create(LineaNavieraRequest $request)
    {
        try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            $linea_naviera = LineaNaviera::create(array_map("mb_strtoupper",$request->all()));
            self::auditar('audit_linea_naviera',$linea_naviera->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function update(LineaNavieraRequest $request,$id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            $linea_naviera = LineaNaviera::find($id);
            if($linea_naviera){
                $linea_naviera->fill(array_map("mb_strtoupper",$request->all()))->save();
                self::auditar('audit_linea_naviera',$linea_naviera->nombre,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();        
            }
        }catch(Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            $linea_naviera = LineaNaviera::find($id);
            if($linea_naviera){
                if (count($linea_naviera->contenedor)) {
                    return self::RegistroInUse();
                }
                $linea_naviera->fill(['activo'=>DB::raw(0)])->save();
                self::auditar('audit_linea_naviera',$linea_naviera->nombre,'ELIMINAR');
                return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }
}
