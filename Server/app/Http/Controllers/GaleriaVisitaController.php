<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Traits\TraitGaleriaVisita;
use Illuminate\Http\Request;

class GaleriaVisitaController extends Controller
{
    use TraitGaleriaVisita;

    public function getImagenesVisita($visita_id)
    {
        return self::obtenerImagenesVisita($visita_id);
    }

    public function createImagenesVisita(Request $request)
    {
        return self::registrarImagenesVisita($request);
    }

    public function deleteImagenesVisita($imagen_id)
    {
        return self::eliminarImagenesVisita($imagen_id);
    }
}
