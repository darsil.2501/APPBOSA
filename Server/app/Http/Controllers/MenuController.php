<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Opcion;
use DB;
use App\Http\Requests\MenuRequest;

class MenuController extends Controller
{

    public function getData()
    {
        try{

            $data_subopciones = \App\User::find(21)->subopciones()->orderBy('opcion_id')->get();

            # Recorrer las subopciones para sacar las opciones
            $data_opcion = array();
            foreach ($data_subopciones as $value) {
                $data_opcion[] = array('opcion_id'     =>$value->opcion->id,
                                'nombre'        =>$value->opcion->nombre,
                                'url'           =>$value->opcion->url,
                                'icono'         =>$value->opcion->icono,
                                'menu_id'       =>$value->opcion->id_menu);
            }

            # Hacer un distinct para que las opciones no se repitan
            $collection     = collect($data_opcion);
            $data_opcion    = $collection->unique('opcion_id');
            $data_opciones  = $data_opcion->values()->all();

            # Recorrer las opciones para sacar los menus
            $data_menu = array();
            foreach ($data_opciones as $value) {
                $opcion_find = Opcion::find($value['opcion_id']);
                $data_menu[] = array('menu_id'     => $opcion_find->menu->id,
                                'nombre'        => $opcion_find->menu->nombre);
            }

            # Hacer un distinct para que las menus no se repitan
            $collection     = collect($data_menu);
            $data_menu     = $collection->unique('menu_id');
            $data_menus  = $data_menu->values()->all();

            #recorrer todos los menus extraer sus opciones y subopciones
            $menus = array();
            foreach ($data_menus as $key => $menu) {
                
                $menus_temp = array();

                $menus_temp['id'] = $menu['menu_id'];
                $menus_temp['nombre'] = $menu['nombre']; 

                $opciones = array();
                foreach ($data_opciones as $key => $opcion) {
                    $opciones_temp = array();
                    $subopciones = array();
                    if ($opcion['menu_id'] === $menu['menu_id']) {
                        $opciones_temp['id'] = $opcion['opcion_id'];
                        $opciones_temp['nombre'] = $opcion['nombre'];
                        $opciones_temp['url'] = $opcion['url'];
                        $opciones_temp['icono'] = $opcion['icono'];

                        $subopciones = array();
                        foreach ($data_subopciones as $key => $subopcion) {
                            $subopciones_temp = array();
                            if ($opcion['opcion_id'] === $subopcion->opcion->id) {
                                $subopciones_temp['id'] = $subopcion->id;
                                $subopciones_temp['nombre'] = $subopcion->nombre;
                                $subopciones_temp['url'] = $subopcion->url;
                                $subopciones_temp['icono'] = $subopcion->icono;

                                $subopciones[] = $subopciones_temp;
                            }
                        }

                        //unset($subopciones);
                        $opciones_temp['subopciones'] = $subopciones;
                        $opciones[] = $opciones_temp;
                    }
                }

                //unset($opciones);
                $menus_temp['opciones'] = $opciones;

                $menus[] = $menus_temp;
            }

            return $menus;
            $menusTemp = Menu::where('activo',1)->get();

            $menus = array();
            foreach ($menusTemp as $key => $menu) {

                $opciones = array();
                foreach ($menu->opciones as $key => $opcion) {
                    $opciones[] = array(
                        'id'            => $opcion->id,
                        'nombre'        => $opcion->nombre,
                        'url'           => $opcion->url,
                        'subopciones'   => $opcion->subopciones,
                        'activo'        => $opcion->activo
                    );
                }

                $menus[] = array(
                    'id'        => $menu->id,
                    'nombre'    => $menu->nombre,
                    'opciones'  => $opciones,
                    'isOpen'    => false,
                    'activo'    => $menu->activo
                );
            }
            return response()->json(['info'=>$menus,'success'=>true]);    
        }catch(Exception $e){
            return response()->json(['info'=>'Error al listar los Menús '.$e->getMessage(),'success'=>false]);    
        }
    }

    public function create(MenuRequest $request)
    {
        try{
            DB::beginTransaction();
                $menu = Menu::create($request->all());
                // se crea un rol para ess menú creado con el mismo nombre
                Rol::create([
                    'nombre' => $menu->nombre,
                    'id_menu'=> $menu->id
                ]);
            DB::commit();
            return response()->json(['message'=>'Menú creado correctamente','success'=>true]);    
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message'=>'Error al crear el Menú'.$e->getMessage(),'success'=>false]);       
        }
    }

    public function update(MenuRequest $request,$id_menu)
    {
        try{
            DB::beginTransaction();
                $menu = Menu::find($id_menu);
                if($menu){
                    $menu->fill($request->all())->save();
                    // se actualiza el nombre del rol tambien
                    $rol = Rol::where('id_menu',$id_menu)->first();
                    $rol->fill(['nombre'=>$request['nombre']])->save();
                }else{
                    return response()->json(['message'=>'No se encontró el menú','success'=>false]);        
                }
            DB::commit();
            return response()->json(['message'=>'Menú actualizado correctamente','success'=>true]);                
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message'=>'Error al actualizar el Menú'.$e->getMessage(),'success'=>false]);       
        }
    }

    public function delete($id_menu)
    {
        try{
            $menu = Menu::find($id_menu);
            if($menu){
                $registros = Rol::where('id_menu',$id_menu)->where('activo',1)->get();
                if(count($registros)>0){return self::MensajePersonalizado(false,'Este menú tiene un Rol asociado, elimine el Rol e intente nuevamente.');}
                $menu->fill(['activo'=>DB::raw(0)])->save();
                return response()->json(['message'=>'Menú eliminado correctamente','success'=>true]);        
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>false]);       
        }
    }



    public function asignarOpciones(Request $request)
    {
        try{
            $menu = Menu::find($request['id_menu']);
            $menu->opciones()->attach($request['id_opcion']);
            return response()->json(['message'=>'Opción asignada correctamente','success'=>'false']);
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al asignar opciones','success'=>'false']);
        }
    }

    public function quitarOpciones(Request $request)
    {
        try{
            $menu = Menu::find($request['id_menu']);
            $menu->opciones()->detach($request['id_opcion']);
            return response()->json(['message'=>'Opción quitada correctamente','success'=>'true']);
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al asignar opciones','success'=>'false']);
        }
    }


    public function veropciones($id_menu)
    {
        try{
            $menu = Menu::find($id_menu);
            $opciones = Opcion::where('activo',1)->get();
            $data = $menu->opciones;
            $flag = false;
            foreach ($opciones as $key) {

                foreach ($data as $k) {
                    $flag = false;
                    if($key->id == $k->id){
                        $flag = true;
                        break;
                    }
                }
                if($flag){
                    $key->enable = true; 
                }else{
                    $key->enable = false; 
                }
                
            }

            return response()->json(['info'=>$opciones,'success'=>true]);
            
        }catch(Exception $e){
            return response()->json(['info'=>'error','success'=>false]);   
        }

    }
}
