<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Caja;
use DB;
use App\Http\Requests\CajaRequest;

class CajaController extends Controller
{

    public function getData()
    {
        try{
            $cajas = Caja::where('activo',1)->get();
            return response()->json(['info'=>$cajas,'success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>'false']);    
        }
    }

    public function create(CajaRequest $request)
    {
        try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            $caja = Caja::create(array_map("mb_strtoupper",$request->all()));
            self::auditar('audit_caja',$caja->marca,'INSERTAR');
            return self::RegistroCreateSuccess();    
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function update(CajaRequest $request,$id)
    {
        return $request->all();
        try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            $caja = Caja::find($id);
            if($caja){
                $caja->fill(array_map("mb_strtoupper",$request->all()))->save();
                self::auditar('audit_caja',$caja->marca,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            $caja = Caja::find($id);
            if($caja){
                if(count($caja->contenedor)){
                    return self::RegistroInUse();
                }        
                $caja->fill(['activo'=>DB::raw(0)])->save();
                self::auditar('audit_caja',$caja->marca,'ELIMINAR');
                return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
    }
}
