<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cliente;
use App\Persona;
use App\Operador;
use App\Caja;
use App\User;
use DB;
use Hash;
use App\Traits\TraitCliente;
use App\Traits\TraitFunciones;
use App\Http\Requests\ClienteRequest;
use Mail;
use App\Jobs\EnviarEmail;

class ClienteController extends Controller
{
    use TraitCliente, TraitFunciones;

    public function getData()
    {
        try{
            $clientes = Cliente::where('activo',1)->get();
            return response()->json(['info'=>$clientes,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Ocurrió un error al listar los clientes.'.$ex->getMessage(),'success'=>false]);    
        }
    }

    public function create(ClienteRequest $request)
    {
        try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                Cliente::create(array_map("mb_strtoupper",$request->all()));
            DB::commit();
            self::auditar('audit_cliente',$request['razon_social'],'INSERTAR');
            return self::RegistroCreateSuccess();
        }catch(\Exception $e){
            DB::rollback();
            return self::ErrorInOperation($e);       
        }
    }

    public function update(ClienteRequest $request,$id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $cliente = Cliente::find($id);
                if($cliente){
                    $cliente->fill(array_map("mb_strtoupper",$request->all()))->save();
                    DB::commit();
                    self::auditar('audit_cliente',$cliente->razon_social,'ACTUALIZAR');
                    return self::MensajePersonalizado(true,'Datos actualizados del Cliente.');        
                }
        }catch(\Exception $e){
            DB::rollback();
            return self::ErrorInOperation($e);       
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $cliente = Cliente::find($id);
                if($cliente){
                    $registros = $cliente->contenedor;
                    if (count($registros)) {
                        return self::RegistroInUse();
                    }
                    $cliente->fill(['activo'=>DB::raw(0)])->save();
            DB::commit();
            self::auditar('audit_cliente',$cliente->razon_social,'ELIMINAR');
            return self::RegistroDeleteSuccess();
            }
        }catch(\Exception $e){
            DB::rollback();
            return self::ErrorInOperation($e);       
        }
    }

    public function whitContenedores(Request $request,$id_cliente)
    {
        try {
            $id_semana    = ($request->has('id_semana')) ? $request['id_semana'] : self::getIdSemanaActual() ; // determino que contenedores de que semana se envia
            $contenedores = self::getContenedoresCliente($id_cliente,$id_semana);
            return response()->json(['info'=>$contenedores,'success'=>true]);
        } catch (\Exception $e) {
            return self::MensajePersonalizado(false,'Ocurrió un error al listar los contenedores, por favor inténtelo nuevamente. Error: '.$e->getMessage());
        }
    }

    public function whitArchivos(Request $request,$id_cliente)
    {
        try {
            // determino que archivos de que semana se listaran
            $id_semana    = ($request->has('id_semana')) ? $request['id_semana'] : self::getIdSemanaActual() ; 
            $facturas     = self::getFacturasCliente($id_cliente,$id_semana);
            $certificados = self::getCertificadosCliente($id_cliente,$id_semana);            
            $packings     = self::getPackingsCliente($id_cliente,$id_semana);
            $valijas      = self::getValijasCliente($id_cliente,$id_semana);
            return response()->json(['facturas'=>$facturas,'certificados'=>$certificados,'packings'=>$packings,'valijas'=>$valijas,'success'=>true]);
        } catch (\Exception $e) {
            return self::MensajePersonalizado(false,'Ocurrió un error al listar los archivos, por favor inténtelo nuevamente. Error: '.$e->getMessage());
        }
    }

    public function asignarOperadores(Request $request)
    {
        if(!self::hasPermiso('operador.asignar')){ return self::HasNoPermiso();}
        return self::asignarOperador($request);
    }

    public function quitarOperadores(Request $request)
    {
        return self::quitarOperador($request);
    }

    public function verOperadores($id_cliente)
    {
        return self::showOperadores($id_cliente);
    }

    public function asignarCajas(Request $request)
    {
        if(!self::hasPermiso('cajas.asignar')){ return self::HasNoPermiso();}
        return self::asignarCaja($request);
    }

    public function quitarCajas(Request $request)
    {
        return self::quitarCaja($request);
    }

    public function verCajas($id_cliente)
    {
        return self::showCajas($id_cliente);
    }

    public function operadoresAsigned($id_cliente)
    {
        return self::operadoresAsignados($id_cliente);
    }

    public function cajasAsigned($id_cliente)
    {
        return self::cajasAsignadas($id_cliente);
    }

    public function getCorreos($id_cliente)
    {
        if(!self::hasPermiso('correos.gestionar')){ return self::HasNoPermiso();}
        return self::verCorreos($id_cliente);
    }

    public function agregarEmails(Request $request)
    {
        return self::agregarEmail($request);
    }

    public function actualizarEmails(Request $request,$id_email)
    {
        return self::actualizarEmail($request,$id_email);
    }

    public function eliminarEmails($id_email)
    {
        return self::eliminarEmail($id_email);
    }
}
