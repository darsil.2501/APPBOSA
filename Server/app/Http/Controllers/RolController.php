<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Rol;
use App\Permiso;
use DB;
use App\Http\Requests\RolRequest;
use App\Traits\TraitRespuesta;
use App\User;

class RolController extends Controller
{
    use TraitRespuesta;

    public function getData()
    {
        try{
            $roles = Rol::where('activo',1)->get();
            return response()->json(['info'=>$roles,'success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>'false']);    
        }
    }

    public function create(RolRequest $request)
    {
        try{
            Rol::create($request->all());
            return response()->json(['message'=>'Rol creado correctamente','success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>false]);       
        }
    }

    public function update(RolRequest $request,$id_rol)
    {
        try{
            $rol = Rol::find($id_rol);
            if($rol){
                $rol->fill($request->all());
                $rol->save();
                return response()->json(['message'=>'Rol actualizado correctamente','success'=>true]);        
            }
            return response()->json(['message'=>'No se encontró el registro solicitado','success'=>false]);        
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>false]);       
        }
    }

    public function delete($id_rol)
    {
        try{
            $rol = Rol::find($id_rol);
            if($rol){
                $registros = User::where('id_rol',$id_rol)->where('activo',1)->get();
                if(count($registros)>0){
                    return self::RegistroInUse();
                }
                $rol->fill(['activo'=>DB::raw(0)]);
                $rol->save();
                return response()->json(['message'=>'Rol eliminado correctamente','success'=>'true']);        
            }
            return response()->json(['message'=>'No se encontró el registro solicitado','success'=>'false']);        
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data Error: '.$e->getMessage(),'success'=>false]);       
        }
    }

    // Asignar y quitar permisos
    public function asignarPermiso(Request $request)
    {
        try{
            $rol = Rol::find($request['id_rol']);
            $rol->permisos()->attach($request['id_permiso']);
            return response()->json(['message'=>'Permiso asignado correctamente','success'=>true]);
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al asignar Permiso','success'=>false]);
        }
    }

    public function quitarPermiso(Request $request)
    {
        try{
            $rol = Rol::find($request['id_rol']);
            $rol->permisos()->detach($request['id_permiso']);
            return response()->json(['message'=>'Permiso quitado correctamente','success'=>true]);
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al quitar permiso','success'=>false]);
        }
    }

    public function verPermisos($id_rol)
    {
        try
        {
            $rol = Rol::find($id_rol);
            $permisos = Permiso::where('activo',1)->get();
            $data = $rol->permisos;

            $flag = false;
            foreach ($permisos as $key) {

                foreach ($data as $k) {
                    $flag = false;
                    if($key->id == $k->id){
                        $flag = true;
                        break;
                    }
                }
                if($flag){
                    $key->enable = true; 
                }else{
                    $key->enable = false; 
                } 
            }

            return response()->json(['info'=>$permisos,'success'=>'true']);
        }
        catch(Exception $e)
        {
            return response()->json(['info'=>'error','success'=>'false']);
        }
    }
}
