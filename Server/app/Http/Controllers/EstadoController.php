<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Estado;
use DB;
use App\Http\Requests\EstadoRequest;

class EstadoController extends Controller
{
    public function getData()
    {
        try{
            $estados = Estado::where('activo',1)->get();
            return response()->json(['info'=>$estados,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>false]);    
        }
    }

    public function create(EstadoRequest $request)
    {
        try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            $estado = Estado::create(array_map("mb_strtoupper",$request->all()));
            self::auditar('audit_estado',$estado->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();    
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
    }

    public function update(EstadoRequest $request,$id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            $estado = Estado::find($id);
            if($estado){
                $estado->fill(array_map("mb_strtoupper",$request->all()))->save();
                self::auditar('audit_estado',$estado->nombre,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            $estado = Estado::find($id);
            if($estado){
                if(count($estado->movimiento)){
                    return self::RegistroInUse();
                }
                $estado->fill(['activo'=>DB::raw(0)])->save();
                self::auditar('audit_estado',$estado->nombre,'ELIMINAR');
                return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
    }
}
