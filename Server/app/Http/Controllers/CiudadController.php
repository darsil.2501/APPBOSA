<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ciudad;
use DB;


class CiudadController extends Controller
{

    public function getData()
    {
        try{
            $ciudades = Ciudad::with('pais')->orderBy('nombre','asc')->where('activo',1)->get();
            $data = array();
            foreach ($ciudades as $value) {
                $data [] = array(
                    'id'         => $value->id,
                    'nombre'     => $value->nombre,
                    'id_pais'    => $value->pais->id,
                    'nombre_pais'=> $value->pais->nombre
                );
            }
            return response()->json(['info'=>$data,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>false]);    
        }
    }

    public function create(Request $request)
    {
        try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            $ciudad = Ciudad::create(array_map("mb_strtoupper",$request->all()));
            self::auditar('audit_ciudad',$ciudad->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();    
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function update(Request $request,$id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            $ciudad = Ciudad::find($id);
            if($ciudad){
                $ciudad->fill(array_map("mb_strtoupper",$request->all()))->save();
                self::auditar('audit_ciudad',$ciudad->nombre,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            $ciudad = Ciudad::find($id);
            if($ciudad){
                if (count($ciudad->puertos->where('activo',1))) {
                    return self::RegistroInUse();
                }
                $ciudad->fill(['activo'=>DB::raw(0)])->save();
                self::auditar('audit_ciudad',$ciudad->nombre,'ELIMINAR');
                return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }
}
