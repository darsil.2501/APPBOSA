<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\TraitLabor;
use DB;

class LaborController extends Controller
{
    use TraitLabor;

    public function getData()
    {
        return self::obtenerLabores();
    }

    public function create(Request $request)
    {
        return self::registrarLabor($request);
    }

    public function update(Request $request,$id)
    {
        return self::actualizarLabor($request, $id);
    }

    public function delete($id)
    {
        return self::EliminarLabor($id);
    }
}
