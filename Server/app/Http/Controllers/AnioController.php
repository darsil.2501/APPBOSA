<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Anio;
use App\Traits\TraitRespuesta;
use App\Semana;

class AnioController extends Controller
{

    use TraitRespuesta;

    public function getData()
    {
        try{
            $anios = Anio::where('activo',1)->orderBy('anio')->get();
            return response()->json(['info'=>$anios,'success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>false]);    
        }
    }

    # Obtener solo los años que tienen semanas aperturadas
    public function getAnioGiveSemanas()
    {
        try{
            $anios = Anio::has('semana')->where('activo',1)->orderBy('anio')->get();
            return response()->json(['info'=>$anios,'success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>false]);    
        }
    }

    public function create(Request $request)
    {
        try{
            Anio::create($request->all());
            return response()->json(['message'=>'Registro creado correctamente','success'=>'true']);    
        }catch(Exception $e){
            return response()->json(['message'=>'error in data','success'=>false]);       
        }
    }

    public function update(Request $request,$id)
    {
        try{
            $anio = Anio::find($id);
            if($anio){
                $registros = $anio->semana;
                if (count($registros)) {
                    return response()->json(['message'=>'No se puede editar este registro, ya tiene datos asociados.','success'=>false]);
                }
                $anio->fill($request->all())->save();
                return response()->json(['message'=>'Registro actualizado correctamente','success'=>'true']);        
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>false]);       
        }
    }

    public function delete($id)
    {
        try{
            $anio = Anio::find($id);
            if($anio){
                $registros = $anio->semana;
                if (count($registros)) {return self::RegistroInUse();}
                $anio->fill(['activo'=>DB::raw(0)])->save();
                    return self::RegistroDeleteSuccess();
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>false]);       
        }
    }

    public function withSemana($id_anio)
    {
        try{
            DB::beginTransaction();
                $anio = Anio::find($id_anio);
                if($anio){
                    // $semanas = $anio->semana->where('activo',1);
                    $semanas = Semana::where('id_anio',$anio->id)->where('activo',1)->orderBy('nombre','DESC')->get();
                    return response()->json(['success'=>true,'info'=>$semanas]);           
                }
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al listar las semanas.']);
        }
    }
}
