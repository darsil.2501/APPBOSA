<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Cliente;
use App\Traits\TraitAuditoria;

class AuditoriaController extends Controller
{

    use TraitAuditoria;

    public function getData()
    {
        // $tablas = DB::select("SHOW TABLES");
        $data = DB::select("SELECT table_name FROM INFORMATION_SCHEMA.TABLES where table_schema='appbosac_api' and table_name like 'audit_%'");
        $tablas = array();
        foreach ($data as $tabla) {
            $tablas[] = array('slug'        =>self::getNameTableUpper($tabla->table_name),
                            'nombre_tabla'  =>self::getNameTableUpper($tabla->table_name,1));
        }
        return response()->json(['info'=>$tablas]);
    }

}
