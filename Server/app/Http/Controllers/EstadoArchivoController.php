<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\EstadoArchivo;
use App\Traits\TraitEstadoArchivo;

class EstadoArchivoController extends Controller
{
    use TraitEstadoArchivo;

    public function getData()
    {
        return self::getEstadoArchivos();
    }

}
