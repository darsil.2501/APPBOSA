<?php

namespace App\Http\Controllers;

use App\Anio;
use App\Factura;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\FacturaRequest;
use App\Semana;
use App\Traits\TraitAuditoria;
use App\Traits\TraitContenedor;
use App\Traits\TraitEmail;
use App\Traits\TraitFactura;
use App\Traits\TraitFunciones;
use DB;
use Illuminate\Http\Request;


class FacturaController extends Controller
{
    use TraitFunciones,TraitContenedor, TraitFactura, TraitAuditoria, TraitEmail;

    public function getData($id_semana)
    {
        try{
            $facturas = self::getFacturas($id_semana);
            return response()->json(['success'=>true,'info'=>$facturas]);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en listar las facturas, inténtelo nuevamente. '.$ex->getMessage()]);
        }
    }

    public function create(Request $request)
    {
       try
        {
            if(!self::hasPermiso('facturas.registrar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $file          = $request['files'];
                $i             = self::getCantidadArchivos('factura',$request['id_cliente']);
                $semana        = Semana::findOrFail($request['id_semana']);
                $nombre_semana = $semana->nombre;
                $nombre_anio   = $semana->anio->anio;

                foreach ($file as $value) {
                    // formato del nombre 1_20_2017_(1).Factura_001.pdf
                    // id_cliente,semana,año_actual,cantidad_archivo,nombre_archivo_default
                    $nombre = $request['id_cliente'].'_'.$nombre_semana.'_'.$nombre_anio.'_'.'('.$i.')'.$value->getClientOriginalName();
                    Factura::create([
                        'id_cliente'   => $request['id_cliente'],
                        'id_semana'    => $request['id_semana'],
                        'id_operador'  => $request['id_operador'],
                        'nombre'       => $nombre,
                        'fecha'        => date('Y-m-d'),
                        'hora'         => date('H:i:s'),
                        'url'          => $nombre
                    ]);
                    $i++;
                    \Storage::disk('factura')->put($nombre,  \File::get($value));
                    // cambiar de estado el tipo de documento
                    self::changeStatusContenedor(2,'factura',$request['id_cliente'],$request['id_semana']);
                    self::auditar('audit_factura',$nombre,'INSERTAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Archivos subidos correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al subir archivos.']);
        }
    }


    public function destroy($id)
    {
        try{
            if(!self::hasPermiso('facturas.eliminar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $factura = Factura::findOrFail($id);
                if ($factura) {
                    $factura->delete();
                    $archivo = public_path('archivos\\factura\\'.$factura->nombre);
                    //$archivo = public_path('archivos/factura/'.$factura->nombre);
                    if (file_exists($archivo)) {
                        \Storage::delete('factura/'.$factura->nombre); 
                    }
                    // cambiar de estado el tipo de documento
                    self::changeStatusContenedor(1,'factura',$factura->id_cliente,$factura->id_semana);
                    self::auditar('audit_factura',$factura->nombre,'ELIMINAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Arhivo eliminado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el archivo. Error: '.$ex->getMessage()]);
        }
    }

    public function download($id)
    {
        try{
            $factura = Factura::findOrFail($id);
            // Esto funciona para cuando esta en el servidor real
            //$archivo = public_path('archivos/factura/'.$factura->nombre);
            
            // Esto funciona para cuando se trabaja localmente...
            $archivo = public_path('archivos\\factura\\'.$factura->nombre);
            if(file_exists($archivo)){
                return response()->download($archivo);    
            }
                return response()->json(['success'=>false,'message'=>'El archivo que intenta descargar no existe']);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al descargar el archivo.']);
        }
    }

    public function enviarEmail($id_factura)
    {   
        if(!self::hasPermiso('emails.enviar')){ return self::HasNoPermiso(); }
        $factura = Factura::find($id_factura);
        $cliente = $factura->cliente;
        $semana  = $factura->semana->nombre;
        self::changeStatusContenedor(3,'factura',$factura->id_cliente,$factura->id_semana);
        self::changeStatusFactura($id_factura);
        return self::sendEmailCliente('factura',$factura->url,$cliente,$semana);
    }
}
