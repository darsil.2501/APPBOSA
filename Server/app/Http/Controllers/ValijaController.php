<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Factura;
use App\Contenedor;
use App\Semana;
use App\Anio;
use App\Valija;
use App\Traits\TraitEmail;
use App\Traits\TraitFunciones;
use App\Traits\TraitContenedor;
use App\Traits\TraitValija;
use App\Traits\TraitAuditoria;
use App\Http\Requests\FacturaRequest;


class ValijaController extends Controller
{
    use TraitFunciones, TraitContenedor, TraitValija, TraitAuditoria, TraitEmail;

    public function getData($id_semana)
    {
        try{
            $valijas = self::getValijas($id_semana);
            return response()->json(['success'=>true,'info'=>$valijas]);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'info'=>'Ocurrió un incoveniente en listar las valijas, inténtelo nuevamente. '.$ex->getMessage()]);
        }
    }

    public function create(Request $request)
    {
        try
        {
            if(!self::hasPermiso('valijas.registrar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $file          = $request['files'];
                $i             = self::getCantidadArchivos('valija',$request['id_cliente']);
                $semana        = Semana::findOrFail($request['id_semana']);
                $nombre_semana = $semana->nombre;
                $nombre_anio   = $semana->anio->anio;

                foreach ($file as $value) {
                    // formato del nombre 1_20_2017_(1).Factura_001.pdf
                    // id_cliente,semana,año_actual,cantidad_archivo,nombre_archivo_default
                    $nombre = $request['id_cliente'].'_'.$nombre_semana.'_'.$nombre_anio.'_'.'('.$i.')'.$value->getClientOriginalName();
                    $valija = Valija::create([
                        'id_cliente'   => $request['id_cliente'],
                        'id_semana'    => $request['id_semana'],
                        'nombre'       => $nombre,
                        'fecha'        => date('Y-m-d'),
                        'hora'         => date('H:i:s'),
                        'url'          => $nombre
                    ]);
                    $i++;

                    $valija = Valija::find($valija->id);
                    // registramos los contenedores que le pertenecen a esa valija
                    if (count($request['contenedores']) > 0) {
                        foreach ($request['contenedores'] as $contenedor) {
                            $valija->contenedores()->attach($contenedor['id_contenedor']);
                            $contenedor = Contenedor::find($contenedor['id_contenedor']);
                            $contenedor->fill(['valija'=>DB::raw(2)])->save();
                        }
                        \Storage::disk('valija')->put($nombre,  \File::get($value));
                    }else{
                        return response()->json(['success'=>false,'message'=>'No ha seleccionado contenedores para la valija.']);
                    }
                    self::auditar('audit_valija',$nombre,'INSERTAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Archivos subidos correctamente.']);
        }catch(Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al subir archivos. Error: '.$ex->getMessage()]);
        }
    }


    public function destroy($id)
    {
        try{
            if(!self::hasPermiso('valijas.eliminar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $valija = Valija::findOrFail($id);
                if ($valija) {
                    $contenedores = $valija->contenedores;
                    // Primero cambiamos de estado los contenedores que tiene asociada esa valija
                    // y los eliminamos de la tabla intermedia contenedor_valija
                    foreach ($contenedores as $value) {
                        $contenedor = Contenedor::find($value['id']);
                        $contenedor->fill(['valija'=>DB::raw(1)])->save();
                        $valija->contenedores()->detach($value['id']);
                    }
                    // Luego eliminamos la valija y su archivo
                    $valija->delete();
                    $archivo = public_path('archivos\\valija\\'.$valija->nombre);
                    //$archivo = public_path('archivos/valija/'.$valija->nombre);
                    if (file_exists($archivo)) {
                        \Storage::delete('valija/'.$valija->nombre); 
                    }
                    self::auditar('audit_valija',$valija->nombre,'ELIMINAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Arhivo eliminado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el archivo. Error: '.$ex->getMessage()]);
        }
    }

    public function download($id)
    {
        try{
            $valija = Valija::findOrFail($id);
            $archivo = public_path('archivos\\valija\\'.$valija->nombre);
            //$archivo = public_path('archivos/valija/'.$valija->nombre);
            if(file_exists($archivo)){
                return response()->download($archivo);    
            }
                return response()->json(['success'=>false,'message'=>'El archivo que intenta descargar no existe']);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al descargar el archivo.']);
        }
    }

    public function eliminarContenedor(Request $request)
    {
        try{
            DB::beginTransaction();
                $valija     = Valija::find($request['id_valija']);
                if(count($valija->contenedores) > 1){
                    $contenedor = Contenedor::find($request['id_contenedor']);
                    $contenedor->fill(['valija'=>DB::raw(1)])->save();
                    $valija->contenedores()->detach($request['id_contenedor']);

                    self::auditar('audit_valija',$valija->nombre.'-'.$contenedor->referencia,'ELIMINAR CONTENEDOR');

                }else{
                    return response()->json(['success'=>false,'message'=>'La valija debe tener al menos un contenedor registrado.']);           
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Contenedor eliminado correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'No se puedo eliminar el contenedor, inténtelo nuevamente. Error: '.$ex->getMessage()]);
        }
    }

    public function agregarContenedor(Request $request)
    {
        try{
            DB::beginTransaction();
                $valija = Valija::find($request['id_valija']);
                $data_id_contenedores = array();
                foreach ($request['contenedores'] as $value) {
                    $valija->contenedores()->attach($value['id_contenedor']);
                    $contenedor = Contenedor::find($value['id_contenedor']);
                    $contenedor->fill(['valija'=>DB::raw(2)])->save();
                    self::auditar('audit_valija',$valija->nombre.'-'.$contenedor->referencia,'AGREGAR CONTENEDOR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Contenedores agregados correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al agregar los contenedores, inténtelo nuevamente. Error: '.$ex->getMessage()]);
        }
    }

    public function enviarEmail($id_valija)
    {   
        if(!self::hasPermiso('emails.enviar')){ return self::HasNoPermiso(); }
        $valija  = Valija::find($id_valija);
        $cliente = $valija->cliente;
        $semana  = $valija->semana->nombre;
        if ($valija) {
            foreach ($valija->contenedores as $value) {
                $contenedor = Contenedor::find($value['id']);
                $contenedor->fill(['valija'=>DB::raw(3)])->save();
            }
        }
        self::changeStatusValija($id_valija);
        return self::sendEmailCliente('valija',$valija->url,$cliente,$semana);
    }
}
