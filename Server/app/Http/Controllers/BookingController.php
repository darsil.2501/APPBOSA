<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Booking;
use DB;
use App\Traits\TraitRespuesta;

class BookingController extends Controller
{
    use TraitRespuesta;

    public function getData()
    {
        try{
            $booking = Booking::where('activo',1)->get();
            return response()->json(['info'=>$booking,'success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>'false']);    
        }
    }

    public function create(Request $request)
    {
        try{
            $validar = Booking::ValidarData($request->all());
            if ($validar->fails()) {
                return response()->json(['messages'=>$validar->errors()->all(),'success'=>'false']);    
            }
            Booking::create($request->all());
            return response()->json(['message'=>'Registro creado correctamente','success'=>'true']);    
        }catch(Exception $e){
            return response()->json(['message'=>'error in data','success'=>'false']);       
        }
    }

    public function update(Request $request,$id)
    {
        try{
            $booking = Booking::find($id);
            if($booking){
                $validar = Booking::ValidarData($request->all());
                if ($validar->fails()) {
                    return response()->json(['messages'=>$validar->errors()->all(),'success'=>'false']);    
                }
                $booking->fill($request->all())->save();
                return response()->json(['message'=>'Registro actualizado correctamente','success'=>'true']);        
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>'false']);       
        }
    }

    public function delete($id)
    {
        try{
            $booking = Booking::find($id);
            if($booking){
                $registros = $booking->contenedor;
                if (count($registros)) {
                    return self::RegistroInUse();
                }
                $booking->fill(['activo'=>DB::raw(0)])->save();
                return self::RegistroDeleteSuccess();       
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>'false']);       
        }
    }
}
