<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Nave;
use App\Http\Requests\NaveRequest;

class NaveController extends Controller
{

    public function getData()
    {
        try{
            $naves = Nave::where('activo',1)->orderBy('nombre','ASC')->get();
            return response()->json(['info'=>$naves,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>false]);    
        }
    }

    public function create(NaveRequest $request)
    {
        try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            $nave = Nave::create($request->all());
            self::auditar('audit_nave',$nave->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();    
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function update(NaveRequest $request,$id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            $nave = Nave::find($id);
            if($nave){
                $nave->fill($request->all())->save();
                self::auditar('audit_nave',$nave->nombre,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            $nave = Nave::find($id);
            if($nave){
                if (count($nave->contenedor)) {
                    return self::RegistroInUse();
                }
                $nave->fill(['activo'=>DB::raw(0)])->save();
                self::auditar('audit_nave',$nave->nombre,'ELIMINAR');
                return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }
}
