<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\TraitFormularioAuditoriaInterna;
use DB;

class FormularioAuditoriaInternaController extends Controller
{
    use TraitFormularioAuditoriaInterna;

    public function getData()
    {
        return self::obtenerPreguntas();
    }

    public function create(Request $request)
    {
        return self::registrarPregunta($request);
    }

    public function update(Request $request,$id)
    {
        return self::actualizarPregunta($request, $id);
    }

    public function delete($id)
    {
        return self::EliminarPregunta($id);
    }
}
