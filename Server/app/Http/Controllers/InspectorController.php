<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Persona;
use App\Inspector;
use App\Traits\TraitInspector;
use DB;
use Illuminate\Http\Request;

class InspectorController extends Controller
{
    use TraitInspector;

    public function getData()
    {
        return self::obtenerInspectores();
    }

    public function create(Request $request)
    {
        return self::registrarInspector($request);
    }

    public function update(Request $request,$id_inspector)
    {
        return self::actualizarInspector($request, $id_inspector);
    }

    public function delete($id_inspector)
    {
        return self::eliminarInspector($id_inspector);
    }

    public function getParcelas(Request $request)
    {
        $productores =  self::obtenerParcelasInspector($request);
        return response()->json(['info'=>$productores,'success'=>true]); 
    }
}
