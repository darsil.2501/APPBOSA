<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cliente;
use App\Movimiento;
use App\Anio;
use App\Semana;
use App\Traits\TraitGrafico;
use App\Traits\TraitFunciones;

class GraficoController extends Controller
{
    use TraitGrafico,TraitFunciones;

    public function contenedoresPorClienteNew($id_anio)
    {   
        if($id_anio != 'null'){
            $anio = Anio::select('anio')->where('id',$id_anio)->where('activo',1)->first(); // seleccionar el año para consultar
            $anio = $anio->anio;
        }else{
            $anio = date('Y');
        }

        $id_anio = Anio::select('id')->where('anio',$anio)->first();

        $clientes = Cliente::where('activo',1)->orderBy('razon_social')->get();
        $data_provider_temp = array();

        // para el data provider
        foreach (self::getArrayMeses() as $mes) {
            foreach ($clientes as $cliente) {
                $data_provider_temp[$cliente->razon_social] = self::getContenedoresClienteNew($cliente->id,$mes['id'],$id_anio->id,$anio);
            }
            // XD
            $data_provider_temp['mes']   = $mes['nombre'];
            $data_provider []            = $data_provider_temp;
            unset($data_provider_temp);
        }

        foreach ($clientes as $cliente)
        {
            $data_graphs[] = array(
                "balloonText"   => $cliente->razon_social.":[[value]]",
                "fillAlphas"    => 0.8,
                "id"            => "AmGraph-".$cliente->id,
                "lineAlpha"     => 0.2,
                "title"         => $cliente->razon_social,
                "type"          => "column",
                "valueField"    => $cliente->razon_social,
                "labelText"     => "[[value]]",
                "fontSize"      => 9,
            );
        }
        return response()->json(['data_provider'=>$data_provider,'data_graphs'=>$data_graphs]);
    }
    
    public function ContenedoresPorAnioNew()
    {
        $anios    = Anio::where('activo',1)->orderBy('anio','ASC')->get();
        $clientes = Cliente::where('activo',1)->orderBy('razon_social')->get();
        $data_provider_temp = array();

        // para el data provider
        foreach ($anios as $value) {
            foreach ($clientes as $cliente) {
                $data_provider_temp[$cliente->razon_social] = self::getContenedoresAnioNew($cliente->id,$value->id);
            }
            // XD
            $data_provider_temp['year']       = $value->anio;
            $data_provider_temp['total_year'] = self::getContenedoresAnio($value->id);
            $data_provider []                 = $data_provider_temp;
            unset($data_provider_temp);
        }

        // para el graphs
        $data_graphs =  array();
        foreach ($clientes as $cliente) {
            $data_graphs[] = array(
                "balloonText"   => "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas"    => 0.8,
                "labelText"     => "[[value]]",
                "lineAlpha"     => 0.3,
                "title"         => $cliente->razon_social,
                "type"          => "column",
                "color"         => "#000000",
                "valueField"    => $cliente->razon_social
            );
        }
        return response()->json(['data_provider'=>$data_provider,'data_graphs'=>$data_graphs]);
    }

    public function ContenedoresPorSemanaNew(Request $request)
    {
        // $id_anio = 1;
        if ($request['id_semana_start'] > $request['id_semana_last']) {
            return response()->json(['success'=>false,'message'=>'La semana de inicio debe ser menor que la de fin.']);
        }

        $id_anio = ($request['id_anio'] != 'null') ? $request['id_anio'] : self::getIdAnioActual() ;
        $anio    = Anio::select('anio')->where('id',$request['id_anio'])->where('activo',1)->first();
        $semanas = Semana::where('id_anio',$request['id_anio'])->whereBetween('id', [$request['id_semana_start'], $request['id_semana_last']])->where('activo',1)->orderBy('nombre','ASC')->get();

        $suma = 0;
        foreach ($semanas as $value) {
            $suma = $suma + self::getContenedoresPorSemana($value->id);
        }

        $promedio_semanal = ($suma > 0) ? round($suma/count($semanas)) : 0;
        

        $data_provider = array();
        foreach ($semanas as $value) {
            $data_provider[] = array('semana'              => 'Semana '.$value->nombre,
                                     'total_contenedores'  => self::getContenedoresPorSemana($value->id),
                                     'total_contenedores1' => self::getContenedoresPorSemana($value->id),
                                     'color_barra'         => '#82E0AA',
                                     'color_linea'         => '#1F618D'
                                     );
        }
        return response()->json(['success'=>true,'data_provider'=>$data_provider,'promedio_semanal'=>$promedio_semanal]);
    }
}
