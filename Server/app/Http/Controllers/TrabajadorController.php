<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Productor;
use App\ProductorTrabajador;
use App\Traits\TraitTrabajador;
use DB;
use Illuminate\Http\Request;

class TrabajadorController extends Controller
{
    use  TraitTrabajador;

    public function getTrabajadoresPorProductor($id_productor)
    {
        return self::obtenerTrabajadoresPorProductor($id_productor);
    }

    public function getData()
    {
        return self::obtenerTrabajadores();
    }

    public function create(Request $request)
    {
        return self::registrarTrabajador($request);
    }

    public function update(Request $request,$id_trabajador)
    {
        return self::actualizarTrabajador($request, $id_trabajador);
    }

    public function delete($id_trabajador)
    {
        return self::eliminarTrabajador($id_trabajador);
    }
}
