<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cargo;
use DB;
use App\Http\Requests\CargoRequest;


class CargoController extends Controller
{
    public function getData()
    {
        try{
            $cargos = Cargo::where('activo',1)->get();
            return response()->json(['info'=>$cargos,'success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>'false']);    
        }
    }

    public function create(CargoRequest $request)
    {
        try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            $cargo = Cargo::create(array_map("mb_strtoupper",$request->all()));
            self::auditar('audit_cargo',$cargo->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();    
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function update(CargoRequest $request,$id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            $cargo = Cargo::find($id);
            if($cargo){
                $cargo->fill(array_map("mb_strtoupper",$request->all()))->save();
                self::auditar('audit_cargo',$cargo->nombre,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            $cargo = Cargo::find($id);
            if($cargo){
                if (count($cargo->persona)) {
                    return self::RegistroInUse();
                }
                $cargo->fill(['activo'=>DB::raw(0)])->save();
                self::auditar('audit_cargo',$cargo->nombre,'ELIMINAR');
                return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
    }
}
