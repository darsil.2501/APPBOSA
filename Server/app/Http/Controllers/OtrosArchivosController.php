<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\OtrosArchivos;
use App\Semana;
use App\Traits\TraitAuditoria;
use App\Traits\TraitEmail;
use App\Traits\TraitFunciones;
use App\Traits\TraitOtrosArchivos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OtrosArchivosController extends Controller
{
    use TraitFunciones, TraitOtrosArchivos, TraitAuditoria, TraitEmail;

    public function getData($id_semana)
    {
        try{
            $archivos = self::getOtrosArchivos($id_semana);
            return response()->json(['success'=>true,'info'=>$archivos]);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'info'=>'Error al listar los Packings.'.$ex->getMessage()]);
        }
    }

    public function create(Request $request)
    {
        try
        {
            // if(!self::hasPermiso('packings.registrar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $file          = $request['files'];
                $i             = self::getCantidadOtrosArchivos();
                $semana        = Semana::findOrFail($request['id_semana']);
                $nombre_semana = $semana->nombre;
                $nombre_anio   = $semana->anio->anio;

                foreach ($file as $value) {
                    // formato del nombre 20_2017_(1).Certificado_001.pdf
                    // semana,año_actual,cantidad_archivos,nombre_archivo_default
                    $nombre    = $nombre_semana.'_'.$nombre_anio.'_'.'('.$i.')'.$value->getClientOriginalName();
                    $extension = $value->getClientOriginalExtension();
                    switch (strtolower ($extension)) {
                        case 'doc':
                            $tipo = 1;
                            break;
                        case 'docx':
                            $tipo = 1;
                            break;
                        case 'xls':
                            $tipo = 2;
                            break;
                        case 'xlsx':
                            $tipo = 2;
                            break;
                        case 'pdf':
                            $tipo = 3;
                            break;
                    }
                    OtrosArchivos::create([
                        'id_semana'    => $request['id_semana'],
                        'nombre'       => $nombre,
                        'fecha'        => date('Y-m-d'),
                        'hora'         => date('H:i:s'),
                        'url'          => $nombre,
                        'tipo'         => $tipo
                    ]);
                    $i++;
                    \Storage::disk('otros_archivos')->put($nombre,  \File::get($value));
                    // self::changeStatusContenedor(2,'packing',$request['id_cliente'],$request['id_semana']);
                    self::auditar('audit_otros_archivos',$nombre,'INSERTAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Archivos subidos correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al subir archivos.'.$ex->getMessage()]);
        }
    }

    public function destroy($id)
    {
        try{
            // if(!self::hasPermiso('packings.eliminar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $archivo_otro = OtrosArchivos::findOrFail($id);
                if ($archivo_otro) {
                    $archivo_otro->delete();
                    $archivo = public_path('archivos/otros_archivos/'.$archivo->nombre);
                    //$archivo = public_path('archivos\\otros_archivos\\'.$archivo_otro->nombre);
                    if (file_exists($archivo)) {
                        \Storage::delete('otros_archivos/'.$archivo_otro->nombre); 
                    }
                    // cambiar de estado el tipo de archivo
                    // self::changeStatusContenedor(1,'packing',$packing->id_cliente,$packing->id_semana);
                    self::auditar('audit_otros_archivos',$archivo_otro->nombre,'ELIMINAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Arhivo eliminado correctamente.']);
        }catch(Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el archivo.']);
        }
    }

    public function download($id)
    {
        try{
            $archivo = OtrosArchivos::findOrFail($id);
            //$archivo = public_path('archivos/packing/'.$packing->nombre);
            $archivo = public_path('archivos\\otros_archivos\\'.$archivo->nombre);
            if(file_exists($archivo)){
                return response()->download($archivo);    
            }
                return response()->json(['success'=>false,'message'=>'El archivo que intenta descargar no existe']);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al descargar el archivo.']);
        }
    }

    // public function enviarEmail($id_otro_archivo)
    // {   
    //     // if(!self::hasPermiso('emails.enviar')){ return self::HasNoPermiso(); }
    //     $archivo = OtrosArchivos::find($id_otro_archivo);
    //     $cliente = $archivo->cliente;
    //     $semana  = $archivo->semana->nombre;
    //     // Cambio de estado a "Enviado" a los correos
    //     // self::changeStatusContenedor(3,'packing',$archivo->id_cliente,$archivo->id_semana);
    //     // self::changeStatusPacking($id_packing);
    //     // envió de emails
    //     return self::sendEmailCliente('packing',$archivo->url,$cliente,$semana);
    // }
}
