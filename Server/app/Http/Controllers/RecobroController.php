<?php
namespace App\Http\Controllers;

use App\Recobro;
use App\Traits\TraitRecobro;
use Illuminate\Http\Request;

class RecobroController extends Controller
{
    use TraitRecobro;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData($id_semana)
    {
        return self::getDataRecobro($id_semana);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return self::createRecobro($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  $recobro_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $recobro_id)
    {
        return self::updateRecobro($request, $recobro_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $recobro_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($recobro_id)
    {
        return self::deleteRecobro($recobro_id);
    }
}

        