<?php

namespace App\Http\Controllers;

use App\Delegado;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Productor;
use App\Sector;
use App\Traits\TraitSector;
use DB;
use Illuminate\Http\Request;

class SectorController extends Controller
{
    use TraitSector;

    public function getData()
    {
        return self::obtenerSectores();
    }

    public function create(Request $request)
    {
        return self::registrarSector($request);
    }

    public function update(Request $request,$id)
    {
        return self::actualizarSector($request,$id);
    }

    public function delete($id)
    {
        return self::eliminarSector($id);
    }

    /*
    * [Listar los productores por sector para asignarlos como delegados] 
     */
    public function getProductoresPorSector($id_sector)
    {
        return self::obtenerProductoresPorSector($id_sector);
    }

    /*
    * [Listar los delegados de un sector] 
     */
    public function getDelegados($id_sector)
    {
        return self::obtenerDelegados($id_sector);
    }

    public function createDelgado(Request $request)
    {
        return self::registrarDelegado($request);
    }

    public function deleteDelegado($id_delegado)
    {
        return self::eliminarDelegado($id_delegado);
    }
}
