<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Opcion;
use App\Subopcion;
use DB;
use App\Http\Requests\OpcionRequest;
use App\Traits\TraitRespuesta;

class OpcionController extends Controller
{
   
    public function getData()
    {
        try{
            $roles = Opcion::where('activo',1)->get();
            return response()->json(['info'=>$roles,'success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>'false']);    
        }
    }

    public function create(OpcionRequest $request)
    {
        try{
            Opcion::create($request->all());
            return response()->json(['message'=>'Opcion creado correctamente','success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>'false']);       
        }
    }

    public function update(OpcionRequest $request,$id_opcion)
    {
        try{
            $rol = Opcion::find($id_opcion);
            if($rol){
                $rol->fill($request->all())->save();
                return response()->json(['message'=>'Opcion actualizado correctamente','success'=>'true']);        
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>'false']);       
        }
    }

    public function delete($id_rol)
    {
        try{
            $rol = Opcion::find($id_rol);
            if($rol){
                $rol->fill(['activo'=>DB::raw(0)])->save();
                return response()->json(['message'=>'Opcion eliminado correctamente','success'=>'true']);        
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>'false']);       
        }
    }

    public function asignarSubOpciones(Request $request)
    {
        try{
            $opcion = Opcion::find($request['id_opcion']);
            $opcion->subopciones()->attach($request['id_subopcion']);
            return response()->json(['message'=>'SubOpción asignada correctamente','success'=>'true']);
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al asignar subopciones','success'=>'false']);
        }
    }

    public function quitarSubOpciones(Request $request)
    {
        try{
            $opcion = Opcion::find($request['id_opcion']);
            $opcion->subopciones()->detach($request['id_subopcion']);
            return response()->json(['message'=>'SubOpción quitada correctamente','success'=>'true']);
        }catch(\Exception $e){
            return response()->json(['message'=>'Error al asignar subopciones','success'=>'false']);
        }
    }

    public function verSubopciones($id_opcion)
    {
        try{
            $opcion      = Opcion::find($id_opcion);
            $subopciones = Subopcion::where('activo',1)->get();
            $data = $opcion->subopciones;
             $flag = false;
            foreach ($subopciones as $key) {
                foreach ($data as $k) {
                    $flag = false;
                    if($key->id == $k->id){
                        $flag = true;
                        break;
                    }
                }
                if($flag){
                    $key->enable = true; 
                }else{
                    $key->enable = false; 
                }
                
            }

            return response()->json(['info'=>$subopciones,'success'=>true]);
            
        }catch(Exception $e){
            return response()->json(['info'=>'Error al listar las subopciones de esta opción','success'=>false]);   
        }

    }
}
