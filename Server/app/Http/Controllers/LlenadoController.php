<?php
namespace App\Http\Controllers;

use App\Llenado;
use App\Traits\TraitLlenado;
use Illuminate\Http\Request;
use App\Contenedor;

class LlenadoController extends Controller
{
    use TraitLlenado;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData($id_semana)
    {
        return self::getDataLlenado($id_semana);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return self::createLlenado($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  $llenado_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $llenado_id)
    {
        return self::updateLlenado($request, $llenado_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $llenado_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($llenado_id)
    {
        return self::deleteLlenado($llenado_id);
    }

    /**
     * Generate packing From Llenado
     *
     * @param  $llenado_id
     * @return \Illuminate\Http\Response
     */
    public function packingLlenado($id_contenedor)
    {
        $data = self::getDataForReporte($id_contenedor);

        return self::reporteExcelLlenado($data);
    }

    /**
     * Generate pdf From Llenado
     *
     * @param  $llenado_id
     * @return \Illuminate\Http\Response
     */
    public function pdfLlenado($id_contenedor)
    {
        $data = self::getDataForReporte($id_contenedor);

        return self::reportePDFLlenado($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPacking($id_semana)
    {
        $data = self::getDataPacking($id_semana);
        return self::reporteExcelPacking($data);
    }
}

        