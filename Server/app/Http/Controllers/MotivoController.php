<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Motivo;
use DB;
use Illuminate\Http\Request;

class MotivoController extends Controller
{

    public function getData()
    {
        try{
            $registros = Motivo::where('activo',1)->get();
            return response()->json(['info'=>$registros,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Error al listar los registros.'.$e->getMessage(),'success'=>false]);    
        }
    }

    // public function create(Request $request)
    // {
    //     try{
    //         if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
    //         $registro = Sector::create($request->all());
    //         // self::auditar('audit_sector',$registro->nombre,'INSERTAR');
    //         return self::RegistroCreateSuccess();    
    //     }catch(\Exception $e){
    //         return self::ErrorInOperation($e);       
    //     }
    // }

    // public function update(Request $request,$id)
    // {
    //     try{
    //         if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
    //         $registro = Sector::find($id);
    //         if($registro){
    //             $registro->fill($request->all())->save();
    //             // self::auditar('audit_sector',$registro->nombre,'ACTUALIZAR');
    //             return self::RegistroUpdateSuccess();        
    //         }
    //     }catch(\Exception $e){
    //         return self::ErrorInOperation($e);
    //     }
    // }

    // public function delete($id)
    // {
    //     try{
    //         if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
    //         $registro = Sector::find($id);
    //         if($registro){
    //             if(count($registro->parcelas)){
    //                 return self::RegistroInUse();
    //             }        
    //             $registro->fill(['activo'=>DB::raw(0)])->save();
    //             // self::auditar('audit_sector',$registro->marca,'ELIMINAR');
    //             return self::RegistroDeleteSuccess();        
    //         }
    //     }catch(\Exception $e){
    //         return self::ErrorInOperation($e);
    //     }
    // }
}
