<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pais;
use DB;

class PaisController extends Controller
{

    public function getData()
    {
        try{
            $paises = Pais::where('activo',1)->orderBy('nombre','ASC')->get();
            return response()->json(['info'=>$paises,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>false]);    
        }
    }

    public function create(Request $request)
    {
        try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            $pais = Pais::create($request->all());
            self::auditar('audit_pais',$pais->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();    
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function update(Request $request,$id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            $pais = Pais::find($id);
            if($pais){
                $pais->fill($request->all())->save();
                self::auditar('audit_pais',$pais->nombre,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            $pais = Pais::find($id);
            if($pais){
                if (count($pais->ciudades)) {
                    return self::RegistroInUse();
                }
                $pais->fill(['activo'=>DB::raw(0)])->save();
                self::auditar('audit_pais',$pais->nombre,'ELIMINAR');
                return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function withCiudades($id_pais)
    {
        try{
            $pais = Pais::find($id_pais);
            $ciudades = $pais->ciudades;
            return response()->json(['success'=>true,'info'=>$ciudades]);
        }catch(\Exception $ex){
            return self::ErrorInOperation($e);       
        }
    }
}
