<?php

namespace App\Http\Controllers;

use App\Empacadora;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Traits\TraitEmpacadora;
use DB;
use Illuminate\Http\Request;

class EmpacadoraController extends Controller
{
    use TraitEmpacadora;

    public function getData()
    {
        return self::obtenerEmpacadoras();
    }

    public function create(Request $request)
    {
        return self::registrarEmpacadora($request);
    }

    public function update(Request $request,$id)
    {
        return self::actualizarEmpacadora($request,$id);
    }

    public function delete($id)
    {
        return self::eliminarEmpacadora($id);
    }

    public function getProductores(Request $request)
    {
        return self::getProductoresEmpacadora($request);
    }
}
