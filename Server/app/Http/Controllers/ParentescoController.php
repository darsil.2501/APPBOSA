<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Traits\TraitParentesco;
use DB;
use Illuminate\Http\Request;


class ParentescoController extends Controller
{
    use TraitParentesco;

    public function getData()
    {
        return self::obtenerParentescos();
    }

    public function create(Request $request)
    {
        return self::registrarParentesco($request);
    }

    public function update(Request $request,$id)
    {
        return self::actualizarParentesco($request,$id);
    }

    public function delete($id)
    {
        return self::eliminarParentesco($id);
    }
}
