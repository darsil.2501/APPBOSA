<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contenedor;
use App\Movimiento;
use App\Traits\TraitContenedor;
use DB;
use Auth;
use App\Http\Requests\ContenedorRequest;
use App\Factura;
use App\Certificado;
use App\Packing;

class ContenedorController extends Controller
{
    use TraitContenedor;

    public function getData()
    {
        try{
            $contenedores = self::cargarContenedores();
            return response()->json(['success'=>true,'info'=>$contenedores]);
        }catch(\Exception $ex){
            return self::MensajePersonalizado(false,'Ocurrió un error al listar los contenedores. Error: '.$ex->getMessage());
        }
    }

    public function create(ContenedorRequest $request)
    {
        
        try{
            if(!self::hasPermiso('contenedor.registrar')){ return self::HasNoPermiso(); }
                DB::beginTransaction();
                    // creamos el contenedor
                    $contenedor = Contenedor::create([
                        'id_semana'             =>mb_strtoupper ($request['id_semana']),
                        'id_cliente'            =>mb_strtoupper ($request['id_cliente']),
                        'referencia'            =>mb_strtoupper ($request['referencia']),
                        'booking'               =>mb_strtoupper ($request['booking']),
                        'numero_contenedor'     =>mb_strtoupper ($request['numero_contenedor']),
                        'nave'                  =>mb_strtoupper ($request['nave']),
                        'id_lineanaviera'       =>mb_strtoupper ($request['id_lineanaviera']),
                        'viaje'                 =>mb_strtoupper ($request['viaje']),
                        'id_puertodestino'      =>mb_strtoupper ($request['id_puertodestino']),
                        'id_operador'           =>mb_strtoupper ($request['id_operador']),
                        'fecha_proceso_inicio'  =>mb_strtoupper ($request['fecha_proceso_inicio']),
                        'fecha_proceso_fin'     =>mb_strtoupper ($request['fecha_proceso_fin']),
                        'fecha_zarpe'           =>mb_strtoupper ($request['fecha_zarpe']),
                        'fecha_llegada'         =>mb_strtoupper ($request['fecha_llegada']),
                        'peso_bruto'            =>mb_strtoupper ($request['peso_bruto']),
                        'peso_neto'             =>mb_strtoupper ($request['peso_neto']),
                    ]);
                    $contenedor_id = $contenedor->id;
                    // Asignamos las cajas que va a tener este contenedor
                    $cajas      = $request['cajas'];
                    // return response()->json($cajas);

                    $peso_neto = 0; // para obtener el peso neto del contenedor segun el peso y la cantidad de cajas
                    $contenedor = Contenedor::find($contenedor_id);
                    foreach ($cajas as $value) {
                        $contenedor->cajas()->attach($value['id'],['cantidad'=>$value['cantidad_cajas']]); // agregamos cajas al contenedor
                        $peso_neto = $peso_neto + ($value['cantidad_cajas'] * $value['peso']); // vamos sumando los pesos
                    }
                    $contenedor->fill(['peso_neto'=>$peso_neto])->save(); // actualizamos en peso neto del contenedor creado    

                    // Creamos el primer movimiento del contenedor que será Pendiente
                    $movimiento = Movimiento::create([
                        'id_contenedor' => $contenedor_id,
                        'id_usuario'    => Auth::user()->id,
                        'fecha'         => date('Y-m-d'),
                        'hora'          => date('H:i:s'),
                        'id_estado'     => 1 //pendiente
                    ]);
                DB::commit();
                self::auditarContenedor($contenedor->semana->nombre,$contenedor->numero_contenedor,$contenedor->referencia,'INSERTAR');
                return self::MensajePersonalizado(true,'Contenedor creado correctamente.');
        }catch(\Exception $ex)
        {
            DB::rollback();
            return self::MensajePersonalizado(false,'Error al intentar crear el contenedor. Error: '.$ex->getMessage());
        }
    }

    // por el momento se la a quitado las validaciones
    public function update(Request $request, $id)
    {
        try{
            if(!self::hasPermiso('contenedor.actualizar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $validar = Contenedor::validar($request->all());
                if($validar->fails()){return response()->json(['success'=>false,'messages'=>$validar->errors()->all()]);}
                $contenedor = Contenedor::find($id);
                if ($contenedor) {
                    $contenedor->fill([
                        'id_semana'             =>mb_strtoupper ($request['id_semana']),
                        'id_cliente'            =>mb_strtoupper ($request['id_cliente']),
                        'referencia'            =>mb_strtoupper ($request['referencia']),
                        'booking'               =>mb_strtoupper ($request['booking']),
                        'numero_contenedor'     =>mb_strtoupper ($request['numero_contenedor']),
                        'nave'                  =>mb_strtoupper ($request['nave']),
                        'id_lineanaviera'       =>mb_strtoupper ($request['id_lineanaviera']),
                        'viaje'                 =>mb_strtoupper ($request['viaje']),
                        'id_puertodestino'      =>mb_strtoupper ($request['id_puertodestino']),
                        'id_operador'           =>mb_strtoupper ($request['id_operador']),
                        'fecha_proceso_inicio'  =>mb_strtoupper ($request['fecha_proceso_inicio']),
                        'fecha_proceso_fin'     =>mb_strtoupper ($request['fecha_proceso_fin']),
                        'fecha_zarpe'           =>mb_strtoupper ($request['fecha_zarpe']),
                        'fecha_llegada'         =>mb_strtoupper ($request['fecha_llegada']),
                        'peso_bruto'            =>mb_strtoupper ($request['peso_bruto']),
                        'peso_neto'             =>mb_strtoupper ($request['peso_neto']),
                    ]);
                    $contenedor->save();

                    // Por sí agrega más cajas
                    $cajas      = $request['cajas'];
                    $contenedor = Contenedor::find($id);
                    $peso_neto  = 0; 
                    $cajas_id_array = array();
                    foreach ($cajas as $value) {
                        $cajas_id_array[$value['id']] = ['cantidad'=>$value['cantidad_cajas']]; // almacenamos las cajas que se envian 
                        // $contenedor->cajas()->attach($value['id'],['cantidad'=>$value['cantidad']]);
                        $peso_neto = $peso_neto + ($value['cantidad_cajas'] * $value['peso']); // vamos sumando los pesos
                    }
                    // actualizamos las cajas
                    $contenedor->cajas()->sync($cajas_id_array); 
                    // actualizamos en peso neto del contenedor creado
                    $contenedor->fill(['peso_neto'=>$peso_neto])->save();
                }else{
                    return self::MensajePersonalizado(false,'No se encontró el contenedor.');
                }
            DB::commit();
            self::auditarContenedor($contenedor->semana->nombre,$contenedor->numero_contenedor,$contenedor->referencia,'ACTUALIZAR');
            return self::MensajePersonalizado(true,'Datos del contenedor actualizados correctamente.');
        }catch(\Exception $ex){
            DB::rollback();
            return self::MensajePersonalizado(false,'Ocurrió un error al actualizar los datos del contenedor. Error: '.$ex->getMessage());
        }

    }

    public function updateByOperador(Request $request, $id)
    {
        try{
            if(!self::hasPermiso('contenedor.actualizar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $contenedor = Contenedor::find($id);
                if ($contenedor) {
                    $contenedor->fill([
                        'booking'           => mb_strtoupper ($request['booking']),
                        'numero_contenedor' => mb_strtoupper ($request['numero_contenedor']),
                        'nave'              => mb_strtoupper ($request['nave']),
                        'id_lineanaviera'   => mb_strtoupper ($request['id_lineanaviera']),
                        'viaje'             => mb_strtoupper ($request['viaje']),
                        'id_puertodestino'  => mb_strtoupper ($request['id_puertodestino']),
                        'fecha_zarpe'       => mb_strtoupper ($request['fecha_zarpe']),
                        'fecha_llegada'     => mb_strtoupper ($request['fecha_llegada']),
                    	'peso_bruto'		=> mb_strtoupper ($request['peso_bruto'])
                    ])->save();
                }else{
                    return self::MensajePersonalizado(false,'No se encontró el contenedor.');
                }
            DB::commit();
            self::auditarContenedor($contenedor->semana->nombre,$contenedor->numero_contenedor,$contenedor->referencia,'ACTUALIZAR');
            return self::MensajePersonalizado(true,'Datos del contenedor actualizados correctamente.');
        }catch(\Exception $ex){
            DB::rollback();
            return self::MensajePersonalizado(false,'Ocurrió un error al actualizar los datos del contenedor. Error: '.$ex->getMessage());
        }

    }

    
    public function destroy($id)
    {
        try{
            if(!self::hasPermiso('contenedor.eliminar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $contenedor = Contenedor::find($id);
                if($contenedor){
                    $contenedor->fill(['activo'=>DB::raw(0)])->save();
                }else{
                    return self::MensajePersonalizado(false,'No se encontro el Contenedor a eliminar.');
                }
            DB::commit();
            self::auditarContenedor($contenedor->semana->nombre,$contenedor->numero_contenedor,$contenedor->referencia,'ELIMINAR');
            return self::MensajePersonalizado(true,'Contenedor eliminado correctamente.');
        }catch(Exception $ex){
            DB::rollback();
            return self::MensajePersonalizado(false,'Error al eliminar el Contenedor'.$ex->getMessage());
        }
    }

    public function cambiarEstado(Request $request)
    {
        try{
            if(!self::hasPermiso('contenedor.cambiar_estado')){ return self::HasNoPermiso(); }
                DB::beginTransaction();
                    // if(self::validarMovimiento($request['id_contenedor'],$request['id_estado'])){
                    //     return self::MensajePersonalizado(false,'Este estado ya ha sido asignado anteriormente a este contenedor, por favor seleccione un estado posterior.');
                    // };
                    $id_last_movimiento = self::getUltimoIdMovimiento($request['id_contenedor']);
                    $movimiento         = Movimiento::find($id_last_movimiento);
                    $movimiento->fill(['activo'=>DB::raw(0)])->save();
                    $movimiento = Movimiento::create([
                        'id_contenedor' => $request['id_contenedor'],
                        'id_usuario'    => Auth::user()->id,
                        'fecha'         => date('Y-m-d'),
                        'hora'          => date('H:i:s'),
                        'id_estado'     => $request['id_estado']
                    ]);
                DB::commit();
                return self::MensajePersonalizado(true,'Cambio de estado realizado correctamente.'); 
        }catch(\Exception $ex){
            DB::rollback();
            return self::MensajePersonalizado(false,'Error al cambiar el estado del contenedor, inténtelo nuevamente. Error: '.$ex->getMessage());
        }
    }

    public function eliminarCaja(Request $request)
    {
        try{
            DB::beginTransaction();
                $contenedor = Contenedor::find($request['id_contenedor']);
                if ($contenedor) {
                    // por si acaso se quiera actualizar el peso del contenedor al eliminar una caja
                    // $caja     = $contenedor->cajas->where('id',$request['id_caja'])->first();
                    // $cantidad = $caja->pivot->cantidad;
                    // $peso_menos = $caja->peso * $cantidad;
                    // $peso_neto_nuevo = $contenedor->peso_neto - $peso_menos;
                    // $contenedor->fill(['peso_neto'=>$peso_neto_nuevo])->save();
                    // return response()->json($peso_neto_nuevo);

                    $contenedor->cajas()->detach($request['id_caja']);
                }else{
                    return self::MensajePersonalizado(false,'No se encontró el contenedor.');
                }
                // $cajas = $contenedor->cajas;
                $cajas_total = array();
                foreach ($contenedor->cajas as $value) {
                    $caja['id']               = $value->id;
                    $caja['marca']            = $value->marca;
                    $caja['cantidad_cajas']   = $value->pivot->cantidad;
                    $cajas_total []           = $caja;
                }
            DB::commit();
            // return self::MensajePersonalizado(true,'Caja eliminada correctamente.');
            return response()->json(['success'=>true,'message'=>'Caja eliminada correctamente','cajas'=>$cajas_total]);
        }catch(\Exception $ex){
            DB::rollback();
            return self::MensajePersonalizado(false,'Ocurrió un error al quitar la caja. inténtelo nuevamente. Error: '.$ex->getMessage());
        }
    }

    public function actualizarCaja(Request $request)
    {
        try{
            DB::beginTransaction();
                $contenedor = Contenedor::find($request['id_contenedor']);
                if ($contenedor) {
                    $contenedor->cajas()->updateExistingPivot($request['id_caja'],['cantidad'=>$request['cantidad']]);
                }else{
                    return self::MensajePersonalizado(false,'No se encontró el contenedor.');
                }
            DB::commit();
            return self::MensajePersonalizado(true,'Caja actualizada correctamente.');
        }catch(\Exception $ex){
            DB::rollback();
            return self::MensajePersonalizado(false,'Ocurrió un error al actualizar la caja.');
        }
    }

    // Listar Contenedores por Semana seleccionada
    public function contenedoresPorSemana($id_semana)
    {
        try{
            $id_semana = ($id_semana === "undefined") ? null : $id_semana ;
            $contenedores = self::cargarContenedores($id_semana);
            return response()->json(['success'=>true,'info'=>$contenedores]);
        }catch(\Exception $ex){
            return self::MensajePersonalizado(false,'Error al listar los contenedores de esta semana. Error: '.$ex->getMessage());
        }
    }

    // Función para descargar los archivos comprimidos de un contenedor.
    public function descargarZip($id_contenedor)
    {
        try{
            $facturas     = Factura::where('id_contenedor',$id_contenedor)->get();
            $certificados = Certificado::where('id_contenedor',$id_contenedor)->get();
            $packings     = Packing::where('id_contenedor',$id_contenedor)->get();

            $files = array();
            
            if(count($facturas)>0){
                foreach ($facturas as $value) {
                    $files[] = glob(public_path('archivos/factura/'.$value->nombre));
                }
            }

            if(count($certificados)>0){
                foreach ($certificados as $value) {
                    $files[] = glob(public_path('archivos/certificado/'.$value->nombre));
                }
            }

            if(count($packings)>0){
                foreach ($packings as $value) {
                    $files[] = glob(public_path('archivos/packing/'.$value->nombre));
                }
            }

            // Se genera un archivo zip y se agregan los archivos
            \Zipper::make('mydir/consolidado.zip')->add($files)->close();

            // Se descargan y se elimina el archivo despues de descargarlo para evitar que se 
            // quede en el servidor y ocupe espacio.
            return response()->download(public_path('mydir/consolidado.zip'))->deleteFileAfterSend(true);

        }catch(\Exception $ex){
            return self::MensajePersonalizado(false,'Ocurrió´un error al intentar descargar el consolidado de archivos de este contenedor, inténtelo nuevamente. Error: '.$ex->getMessage());
        }
    }

    // ------------------------- Listar los contenedores del cliente y por semana (Valijas)-------------
    public function contenedoresOfValijaPorClienteAndSemana(Request $request)
    {
        try{
            $contenedores = self::getContenedoresClienteAndSemana($request['id_cliente'],$request['id_semana'],'valija');
            return response()->json(['success'=>true,'info'=>$contenedores]);   
        }catch(\Exception $ex){
            return self::MensajePersonalizado(false,'Error al listar los contenedores de esta semana. Error: '.$ex->getMessage());
        }
    }
    // ------------------------- Fin Listar los contenedores del cliente y por semana (Valijas)---------
    // 
    // ------------------------- Listar los contenedores del cliente y por semana (Valijas)-------------
    public function contenedoresOfCertificadoPorClienteAndSemana(Request $request)
    {
        try{
            $contenedores = self::getContenedoresClienteAndSemana($request['id_cliente'],$request['id_semana'],'certificado');
            return response()->json(['success'=>true,'info'=>$contenedores]);   
        }catch(\Exception $ex){
            return self::MensajePersonalizado(false,'Error al listar los contenedores de esta semana. Error: '.$ex->getMessage());
        }
    }
    // ------------------------- Fin Listar los contenedores del cliente y por semana (Valijas)---------

    // ------------------------- Listar los contenedores del cliente, semana y operador (Valijas)-------------
    public function contenedoresPorClienteAndSemanaAndOperador(Request $request)
    {
        try{
            $contenedores = self::getContenedoresClienteAndSemanaAndOperador($request['id_cliente'],$request['id_semana'],$request['id_cliente_operador']);
            return response()->json(['success'=>true,'info'=>$contenedores]);   
        }catch(\Exception $ex){
            return self::MensajePersonalizado(false,'Error al listar los contenedores de esta semana. Error: '.$ex->getMessage());
        }
    }
    // ------------------------- Fin Listar los contenedores del cliente, semana y operador (Valijas)---------

    public function contenedoresSinLlenado($id_semana)
    {
        try {
            $contenedores_query = Contenedor::where('llenado',0)->where('id_semana',$id_semana)->where('activo',1)->get();

            $contenedores = array();
            foreach ($contenedores_query as $key => $contenedor) {
                $contenedores[] = array(
                    'id'     => $contenedor->id,
                    'nombre' => $contenedor->numero_contenedor . ' (' . $contenedor->referencia . ')'
                );
            }
            return response()->json(['success'=>true,'info'=>$contenedores]);
        } catch (\Exception $ex) {
             return self::MensajePersonalizado(false,'Error al listar los contenedores de esta semana. Error: '.$ex->getMessage());
        }
    }
}
