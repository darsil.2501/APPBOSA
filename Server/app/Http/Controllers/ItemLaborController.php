<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\TraitItemLabor;
use DB;

class ItemLaborController extends Controller
{
    use TraitItemLabor;

    public function getData()
    {
        return self::obtenerItemsLabor();
    }

    public function create(Request $request)
    {
        return self::registrarItemLabor($request);
    }

    public function update(Request $request,$id)
    {
        return self::actualizarItemLabor($request, $id);
    }

    public function delete($id)
    {
        return self::EliminarItemLabor($id);
    }
}
