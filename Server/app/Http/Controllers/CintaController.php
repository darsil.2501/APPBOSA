<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CintaRequest;
use App\Traits\TraitCinta;
use Illuminate\Http\Request;

class CintaController extends Controller
{

    use TraitCinta;

    public function getData()
    {
        return self::obtenerInformacionCinta();
    }

    public function create(Request $request)
    {
        return self::registrarCinta($request);
    }

    public function update(Request $request, $id)
    {
        return self::actualizarCinta($request,$id);
    }

    public function delete($id)
    {
        return self::eliminarCinta($id);
    }
}
        