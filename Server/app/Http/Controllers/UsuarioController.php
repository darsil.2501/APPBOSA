<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\UsuarioRequest;
use App\Menu;
use App\Opcion;
use App\Permiso;
use App\Persona;
use App\Rol;
use App\Traits\TraitAuditoria;
use App\Traits\TraitFunciones;
use App\Traits\TraitPermisos;
use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    use TraitPermisos, TraitAuditoria, TraitFunciones;

    public function getData()
    {
        try{
            $usuarios = User::where('activo',1)->get();
            foreach ($usuarios as $usuario) {
                $user['id_user']            = $usuario->id;
                $user['id_persona']         = $usuario->persona->id;
                $user['nick']               = $usuario->nick; 
                $user['dni']                = $usuario->persona->dni;
                $user['nombre']             = $usuario->persona->nombre;
                $user['apellido_paterno']   = $usuario->persona->apellido_paterno;
                $user['apellido_materno']   = $usuario->persona->apellido_materno;
                $user['fullname']           = $usuario->persona->fullname;
                $user['email']              = $usuario->persona->email;
                $user['telefono']           = $usuario->persona->telefono;
                $user['cargo']              = $usuario->persona->cargo->nombre;
                // $user['rol']                = $usuario->rol->nombre;
                // $user['id_rol']             = $usuario->id_rol;
                $user['id_cliente_operador']= $usuario->id_cliente_operador;
                $user['id_cargo']           = $usuario->persona->id_cargo;  
                $data[] = $user;
            }
            return response()->json(['success'=>true,'usuarios'=>$data]);
        }catch(Exception $ex){
            return response()->json(['success'=>false,'message'=>$ex->getMessage()]);
        }
    }

    public function create(UsuarioRequest $request)
    {
        try{
            if(!self::hasPermiso('usuario.registrar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
            $full_name = $request['nombre'].' '.$request['apellido_paterno'].' '.$request['apellido_materno'];
            $persona   = Persona::create([
                'dni'               =>  $request['dni'],
                'nombre'            =>  $request['nombre'],
                'apellido_paterno'  =>  $request['apellido_paterno'],
                'apellido_materno'  =>  $request['apellido_materno'],
                'fullname'          =>  $full_name,
                'email'             =>  $request['email'],
                'telefono'          =>  $request['telefono'],
                'id_cargo'          =>  $request['id_cargo']
                
            ]);
            
            User::create([
                'nick'                =>  $request['nick'],
                'password'            =>  Hash::make($request['password']),
                'id_persona'          =>  $persona->id,
                // 'id_rol'              =>  $request['id_rol'],
                'id_cliente_operador' =>  $request['id_cliente_operador']
            ]);

            DB::commit();
            self::auditarUser('INSERTAR',$full_name);
            return response()->json(['message'=>'Usuario creado correctamente','success'=>true]);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['message'=>'Ocurrio un error al intentar registrar el usuario. Error: '.$ex->getMessage(),'success'=>false]);
        }
    }

    public function update(Request $request,$id_usuario)
    {
        try{
            if(!self::hasPermiso('usuario.actualizar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $usuario = User::find($id_usuario);
                $persona = Persona::find($usuario->id_persona);
                $validar = User::validateUpdate($request->all(),$usuario->id_persona);

                if($validar->fails()){return response()->json(['success'=>false,'messages'=>$validar->errors()->all()]);}
                
                $full_name = $request['nombre'].' '.$request['apellido_paterno'].' '.$request['apellido_materno'];
                $persona->fill([
                    'dni'               =>  $request['dni'],
                    'nombre'            =>  $request['nombre'],
                    'apellido_paterno'  =>  $request['apellido_paterno'],
                    'apellido_materno'  =>  $request['apellido_materno'],
                    'fullname'          =>  $full_name,
                    'email'             =>  $request['email'],
                    'telefono'          =>  $request['telefono'],
                    'id_cargo'          =>  $request['id_cargo'],
                ]);
                $persona->save();

                // Actualizamos el usuario
                if($request->has('password')){
                    $usuario->fill([
                        'nick'               =>  $request['nick'],
                        'password'           =>  Hash::make($request['password']),
                        // 'id_rol'             =>  $request['id_rol'],
                        'id_cliente_operador'=>  $request['id_cliente_operador']
                    ]);
                }else{
                    $usuario->fill([
                        'nick'               =>  $request['nick'],
                        // 'id_rol'             =>  $request['id_rol'],
                        'id_cliente_operador'=>  $request['id_cliente_operador']
                    ]);
                }
                $usuario->save();
            DB::commit();
            self::auditarUser('ACTUALIZAR',$full_name);
            return response()->json(['success'=>true,'message'=>'Usuario actualizado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>true,'message'=>'Error al actualizar datos del Usuario. Error: '.$ex->getMessage()]);
        }
    }

    public function delete($id_usuario)
    {
        try{
            if(!self::hasPermiso('usuario.eliminar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $usuario = User::find($id_usuario);
                $usuario->fill(['activo'=>DB::raw(0)])->save();
                $persona = Persona::find($usuario->id_persona);
                $persona->fill(['activo'=>DB::raw(0)])->save();
            DB::commit();
            self::auditarUser('ELIMINAR',$usuario->persona->fullname);
            return response()->json(['success'=>true,'message'=>'Usuario eliminado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>true,'message'=>'Error al eliminar el Usuario. Error:' .$ex->getMessage()]);
        }
    }
    

    public function auth(Request $request)
    {
        try{
            $credenciales = ['nick'=>$request['nick'],'password'=>$request['password'],'activo'=>1];

            if (Auth::attempt($credenciales)) {
                $data_subopciones = Auth::user()->subopciones()->orderBy('opcion_id')->get();

                # Recorrer las subopciones para sacar las opciones
                $data_opcion = array();
                foreach ($data_subopciones as $value) {
                    $data_opcion[] = array('opcion_id'     =>$value->opcion->id,
                                    'nombre'        =>$value->opcion->nombre,
                                    'url'           =>$value->opcion->url,
                                    'icono'         =>$value->opcion->icono,
                                    'menu_id'       =>$value->opcion->id_menu);
                }

                # Hacer un distinct para que las opciones no se repitan
                $collection     = collect($data_opcion);
                $data_opcion    = $collection->unique('opcion_id');
                $data_opciones  = $data_opcion->values()->all();

                # Recorrer las opciones para sacar los menus
                $data_menu = array();
                foreach ($data_opciones as $value) {
                    $opcion_find = Opcion::find($value['opcion_id']);
                    $data_menu[] = array(
                        'menu_id' => $opcion_find->menu->id,
                        'nombre'  => $opcion_find->menu->nombre,
                        'icono'  => $opcion_find->menu->icono
                    );
                }

                # Hacer un distinct para que las menus no se repitan
                $collection     = collect($data_menu);
                $data_menu     = $collection->unique('menu_id');
                $data_menus  = $data_menu->values()->all();

                #recorrer todos los menus extraer sus opciones y subopciones
                $opciones = array();
                foreach ($data_menus as $key => $menu) {
                    
                    $opciones[] = array(
                        'id'            => $menu['menu_id'],
                        'nombre'        => $menu['nombre'],
                        'icono'         => $menu['icono'],
                        'subopciones'   => []
                    );

                    foreach ($data_opciones as $opcion) {
                        $opciones_temp = array();
                        $subopciones = array();
                        if ($opcion['menu_id'] === $menu['menu_id']) {
                            $opciones_temp['id'] = $opcion['opcion_id'];
                            $opciones_temp['nombre'] = $opcion['nombre'];
                            $opciones_temp['url'] = $opcion['url'];
                            $opciones_temp['icono'] = $opcion['icono'];

                            $subopciones = array();
                            foreach ($data_subopciones as $subopcion) {
                                $subopciones_temp = array();
                                if ($opcion['opcion_id'] === $subopcion->opcion->id) {
                                    $subopciones_temp['id'] = $subopcion->id;
                                    $subopciones_temp['nombre'] = $subopcion->nombre;
                                    $subopciones_temp['url'] = $subopcion->url;
                                    $subopciones_temp['icono'] = $subopcion->icono;

                                    $subopciones[] = $subopciones_temp;
                                }
                            }

                            //unset($subopciones);
                            $opciones_temp['subopciones'] = $subopciones;
                            $opciones[] = $opciones_temp;
                        }
                    }
                }
                
                $data_persona = Persona::where('id',Auth::user()->id_persona)->first();
                $data_usuario = Auth::user();
                unset($data_usuario['subopciones']);
                $data_persona['cargo_empresa'] = $data_persona->cargo->nombre;
                unset($data_persona['cargo']);

                // listar los permisos del usuario autenticado
                $permisos      = Auth::user()->permisos;
                
                $data_permisos = array();
                foreach ($permisos as $value) {
                    $data_permisos[] = $value->slug;
                }

                #ver si es un inspector que inicia sesion
                $inspector = [];

                if (!empty($data_persona->inspector)) {
                    
                    $inspector['id']                = $data_persona->inspector->id;
                    $inspector['nombre']            = $data_persona->nombre;
                    $inspector['apellido_paterno']  = $data_persona->apellido_paterno;
                    $inspector['apellido_materno']  = $data_persona->apellido_paterno;
                    $inspector['fullname']          = $data_persona->fullname;

                }

                # recorrer para sacar las subopciones para validar rutas lado del cliente
                $data_ruta_subopciones = array();
                foreach ($data_subopciones as $subopcion) {
                    $data_ruta_subopciones [] = $subopcion->url;
                }

                # Almacenar en sessión los permisos
                \Session::put('permisos',$data_permisos);
                return response()->json(['success'           => true,
                                        'data_menu'          => $opciones,
                                        'data_persona'       => $data_persona,
                                        'inspector'          => $inspector,
                                        'data_usuario'       => $data_usuario,
                                        'data_subopciones'   => $data_ruta_subopciones,
                                        'permisos'           => $data_permisos]); 
            }else{
                return response()->json(['success'=>false,'message'=>'Credenciales Incorrectas']); 
            }
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'Error al autenticarse.'.$ex->getMessage()]); 
        }
    }

    public function logout()
    {
        Auth::logout();
        \Session::forget('permisos');
    }

    public function listarMenu($usuario_id)
    {
        $opciones            = Opcion::with('subopciones')->get();
        $subopciones_usuario = User::find($usuario_id)->subopciones;
        $flag = false;
        foreach ($opciones as $opcion) {
            foreach ($opcion->subopciones as $subopcion) {
                foreach ($subopciones_usuario as $subopcion_usuario) {
                    $flag = false;
                    if ($subopcion->id == $subopcion_usuario->id) {
                        $flag = true;
                        break;
                    }
                }
                $enable            = ($flag) ? true : false ;
                $subopcion->enable = $enable;
            }
        }
        return response()->json(['success'=>true,'info'=>$opciones]);
    }

    public function listarPermisos($usuario_id)
    {
        $permisos            = Permiso::where('activo',1)->get();
        $permisos_usuario    = User::find($usuario_id)->permisos;
        $flag = false;
        foreach ($permisos as $permiso) {
            foreach ($permisos_usuario as $permiso_usuario) {
                $flag = false;
                if ($permiso->id == $permiso_usuario->id) {
                    $flag = true;
                    break;
                }
            }
            $enable          = ($flag) ? true : false ;
            $permiso->enable = $enable;
        }
        return response()->json(['success'=>true,'info'=>$permisos]);
    }

    # Solo se usa esto para la parte de la configuración de los usuarios
    public function asignedSubOpcionToUser(Request $request)
    {
        try{
            DB::beginTransaction();
                $usuario = User::find($request['usuario_id']);
                foreach ($request['subopciones'] as $subopcion) {
                    if($subopcion['enable']){
                        $usuario->subopciones()->attach($subopcion['id']);        
                    }else{
                        $usuario->subopciones()->detach($subopcion['id']);
                    }
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Configuración realizada correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al realizar la operación.'.$ex->getMessage()]);
        }
    }

    public function asignedPermisoToUser(Request $request)
    {
        try{
            DB::beginTransaction();
                $usuario = User::find($request['usuario_id']);
                foreach ($request['permisos'] as $permiso) {
                    if($permiso['enable']){
                        $usuario->permisos()->attach($permiso['id']);        
                    }else{
                        $usuario->permisos()->detach($permiso['id']);
                    }
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Configuración realizada correctamente.']);   
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al realizar la operación.'.$ex->getMessage()]);
        }
    }
}



/*----------------------------------------------------------------------------------------------------------*/
//
//
//
//
//
//              DESARROLLADORES: Andrade Silva Darwin Joel - FrontEnd
//                               Ramirez Coronado Luis Guillermo - BackEnd
//          
//  
//  
//              Dedicado: Para nuestro mentor y fuente de mejora, Ing. Moises David Saavedra Arango
//                        y al grupo de desarrollo hoy llamado Alyssa.
//
//
//
//
//
//                                                               Fecha: 24/08/2017 - 03:14pm                          
/*----------------------------------------------------------------------------------------------------------*/
