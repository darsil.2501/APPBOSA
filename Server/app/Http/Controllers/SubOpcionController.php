<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Opcion;
use App\Subopcion;
use App\User;
use DB;
use Illuminate\Http\Request;


class SubOpcionController extends Controller
{
   
    public function getData()
    {
        try{
            $roles = Subopcion::where('activo',1)->get();
            return response()->json(['info'=>$roles,'success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data','success'=>'false']);    
        }
    }

    public function create(Request $request)
    {
        try{
            Subopcion::create($request->all());
            return response()->json(['message'=>'Subopcion creado correctamente','success'=>'true']);    
        }catch(Exception $e){
            return response()->json(['message'=>'error in data','success'=>'false']);       
        }
    }

    public function update(Request $request,$id_subopcion)
    {
        try{
            $rol = Subopcion::find($id_subopcion);
            if($rol){
                $rol->fill($request->all())->save();
                return response()->json(['message'=>'Subopcion actualizado correctamente','success'=>'true']);        
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>'false']);       
        }
    }

    public function delete($id)
    {
        try{
            $rol = Subopcion::find($id);
            if($rol){
                $rol->fill(['activo'=>DB::raw(0)])->save();
                return response()->json(['message'=>'Sub Opcion eliminado correctamente','success'=>true]);        
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>false]);       
        }
    }
}
