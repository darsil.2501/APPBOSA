<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SemanaRequest;
use App\Http\Controllers\Controller;
use App\Semana;
use App\Anio;
use DB;
use App\Traits\TraitSemana;
use App\Traits\TraitRespuesta;
use App\Traits\TraitFunciones;
use App\Traits\TraitAuditoria;
use App\Traits\TraitPermisos;

class SemanaController extends Controller
{    
    use TraitSemana,TraitPermisos,TraitRespuesta,TraitFunciones,TraitAuditoria;

    public function getData()
    {
        try{
            $semanas = Semana::where('activo',1)->get();
            return response()->json(['info'=>$semanas,'success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Ocurrió un error al listar las semanas. Error: '.$e->getMessage(),'success'=>false]);    
        }
    }

    public function create(SemanaRequest $request)
    {
        try{
            if(!self::hasPermiso('semana.aperturar')){ return self::HasNoPermiso(); }
            // validar que la semana que se va registrar ya existe
            if(self::existSemana($request->all())){
                return self::MensajePersonalizado(false,'La semana que está intentando registrar para este año ya existe.');
            }
            $semana = Semana::create($request->all());
            self::auditar('audit_semana',$semana->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();    
        }catch(\Exception $e){
            return self::MensajePersonalizado(false,'Ocurrió un error al crear la semana. Error: '.$e->getMessage());       
        }
    }

    public function update(SemanaRequest $request,$id)
    {
        try{
            $semana = Semana::find($id);
            if($semana){
                $semana->fill($request->all())->save();
                self::auditar('audit_semana',$semana->nombre,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();        
            }
        }catch(\Exception $e){
            return self::MensajePersonalizado(false,'Error al actulizar la semana. Error: '.$e->getMessage());
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('semana.eliminar')){ return self::HasNoPermiso(); }
            $semana = Semana::find($id);
            if($semana){
                $registros = $semana->contenedor->where('activo',1);
                if (count($registros)) {
                    return self::MensajePersonalizado(false,'Este Semana no se puede eliminar, tiene contenedores registrados.');
                }
                $semana->fill(['activo'=>DB::raw(0)])->save();
                self::auditar('audit_semana',$semana->nombre,'ELIMINAR');
                return self::RegistroDeleteSuccess();
            }
        }catch(\Exception $e){
            return self::MensajePersonalizado(false,'Error al intentar eliminar esta semana. Error: '.$e->getMessage());       
        }
    }

    public function getSemanaWithContenedoresParams($anio_id)
    {
        
        try{

            $semanas = Semana::where('id_anio',$anio_id)->where('activo',1)->orderBy('nombre','DESC')->get();   
            $data_total     = array();
            foreach ($semanas as $value) {
                $data['id_semana']          = $value->id;
                $data['semana']             = $value->nombre;
                $data['total_contenedores'] = self::getContenedoresPorSemana($value->id);
                $data['id_cinta']           = ($value->cinta) ? $value->cinta->id : $value->cinta;
                $data['nombre_cinta']       = ($value->cinta) ? $value->cinta->nombre : $value->cinta;
                $data['color_cinta']        = ($value->cinta) ? $value->cinta->color : $value->cinta;
                $data['is_close']           = ($value->is_close);
                $data_total[] = $data;
            }
        
            return response()->json(['success'=>true,'info'=>$data_total]);   
        }catch(Exception $ex){
            return response()->json(['success'=>false,'message'=>'Ocurrió un error al listar las semanas aperturadas.']);
        }
    }

    public function getSemanaWithContenedores()
    {
        
        try{

            $id_anio_actual = self::getIdAnioActual();
              
            $semanas = Semana::where('id_anio',$id_anio_actual)->select('id','nombre')->where('activo',1)->orderBy('nombre','DESC')->get();   
            $data_total     = array();
            foreach ($semanas as $value) {
                $data['id_semana']          = $value->id;
                $data['semana']             = $value->nombre;
                $data['total_contenedores'] = self::getContenedoresPorSemana($value->id);
                $data_total[] = $data;
            }
        
            return response()->json(['success'=>true,'info'=>$data_total]);   
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'info'=>'Ocurrió un error al listar las semanas aperturadas.']);
        }
    }

    public function close($id)
    {
        try{
            $semana = Semana::find($id);
            if($semana){
                $semana->fill(['is_close'=>DB::raw(1)])->save();
                self::auditar('audit_semana',$semana->nombre,'CERRAR'); 
            }
            return response()->json(['success'=>true,'message'=>"Semana Cerrada Correctamente."]);
        }catch(\Exception $e){
            return self::MensajePersonalizado(false,'Error al intentar eliminar esta semana. Error: '.$e->getMessage());       
        }
    }
}
