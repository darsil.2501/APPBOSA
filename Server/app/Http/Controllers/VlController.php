<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Semana;
use App\Traits\TraitAuditoria;
use App\Traits\TraitEmail;
use App\Traits\TraitFunciones;
use App\Traits\TraitVl;
use App\Vl;
use DB;
use Illuminate\Http\Request;

class VlController extends Controller
{
    use TraitFunciones, TraitVl, TraitAuditoria, TraitEmail;

    public function getData($id_semana)
    {
        try{
            $vls = self::getVls($id_semana);
            return response()->json(['success'=>true,'info'=>$vls]);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'info'=>'Error al listar los Vls.'.$ex->getMessage()]);
        }
    }

    public function create(Request $request)
    {
        try
        {
            if(!self::hasPermiso('vls.registrar')){ return self::HasNoPermiso();}
            DB::beginTransaction();
                $file          = $request['files'];
                $i             = self::getCantidadArchivos('vl',$request['id_cliente']);
                $semana        = Semana::findOrFail($request['id_semana']);
                $nombre_semana = $semana->nombre;
                $nombre_anio   = $semana->anio->anio;

                foreach ($file as $value) {
                    // formato del nombre 1_20_2017_(1).Certificado_001.pdf
                    // id_cliente,semana,año_actual,cantidad_archivo,nombre_archivo_default
                    $nombre = $request['id_cliente'].'_'.$nombre_semana.'_'.$nombre_anio.'_'.'('.$i.')'.$value->getClientOriginalName();
                    Vl::create([
                        'id_cliente'   => $request['id_cliente'],
                        'id_semana'    => $request['id_semana'],
                        // 'id_operador'  => $request['id_operador'],
                        'nombre'       => $nombre,
                        'fecha'        => date('Y-m-d'),
                        'hora'         => date('H:i:s'),
                        'url'          => $nombre
                    ]);
                    $i++;
                    \Storage::disk('vl')->put($nombre,  \File::get($value));
                    self::changeStatusContenedor(2,'vl',$request['id_cliente'],$request['id_semana']);
                    self::auditar('audit_vl',$nombre,'INSERTAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Archivos subidos correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al subir archivos.'.$ex->getMessage()]);
        }
    }

    public function destroy($id)
    {
        try{
            if(!self::hasPermiso('vls.eliminar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $vl = Vl::findOrFail($id);
                if ($vl) {
                    $vl->delete();
                    //$archivo = public_path('archivos/vl/'.$vl->nombre);
                    $archivo = public_path('archivos\\vl\\'.$vl->nombre);
                    if (file_exists($archivo)) {
                        \Storage::delete('vl/'.$vl->nombre); 
                    }
                    // cambiar de estado el tipo de archivo
                    self::changeStatusContenedor(1,'vl',$vl->id_cliente,$vl->id_semana);
                    self::auditar('audit_vl',$vl->nombre,'ELIMINAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Arhivo eliminado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el archivo.']);
        }
    }

    public function download($id)
    {
        try{
            $vl = Vl::findOrFail($id);
            //$archivo = public_path('archivos/vl/'.$vl->nombre);
            $archivo = public_path('archivos\\vl\\'.$vl->url);
            if(file_exists($archivo)){
                return response()->download($archivo);    
            }
                return response()->json(['success'=>false,'message'=>'El archivo que intenta descargar no existe']);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al descargar el archivo.']);
        }
    }

    public function enviarEmail($id_vl)
    {   
        if(!self::hasPermiso('emails.enviar')){ return self::HasNoPermiso(); }
        $vl      = Vl::find($id_vl);
        $cliente = $vl->cliente;
        $semana  = $vl->semana->nombre;
        // Cambio de estado a "Enviado" a los correos
        self::changeStatusContenedor(3,'vl',$vl->id_cliente,$vl->id_semana);
        self::changeStatusVl($id_vl);
        // envió de emails
        return self::sendEmailCliente('vl',$vl->url,$cliente,$semana);
    }



}
