<?php

namespace App\Http\Controllers;

use App\Caja;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ProductorRequest;
use App\Persona;
use App\Productor;
use App\Traits\TraitParcela;
use App\Traits\TraitProductor;
use DB;
use Illuminate\Http\Request;

class ProductorController extends Controller
{
    use TraitParcela, TraitProductor;

    public function getData()
    {
        return self::obtenerProductores();
    }

    public function create(ProductorRequest $request)
    {
        return self::registrarProductor($request);
    }

    public function update(Request $request,$id_productor)
    {
        return self::actualizarProductor($request, $id_productor);
    }

    public function delete($id_productor)
    {
        return self::eliminarProductor($id_productor);
    }


    public function changeConditionProductor(Request $request)
    {
        return self::cambiarCondicionProductor($request);
    }

    # Obtener las parcelas de un determinado productor
    public function getParcelasPorProductor($id_productor)
    {
        $productor = Productor::find($id_productor);
        return response()->json(['info'=>self::obtenerParcelasPorProductor($productor),'success'=>true]);
    }
}
