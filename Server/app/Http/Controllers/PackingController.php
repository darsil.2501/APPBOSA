<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Packing;
use App\Semana;
use App\Traits\TraitEmail;
use App\Traits\TraitFunciones;
use App\Traits\TraitPacking;
use App\Traits\TraitAuditoria;
use DB;

class PackingController extends Controller
{
    use TraitFunciones, TraitPacking, TraitAuditoria, TraitEmail;

    public function getData($id_semana)
    {
        try{
            $packings = self::getPackings($id_semana);
            return response()->json(['success'=>true,'info'=>$packings]);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'info'=>'Error al listar los Packings.'.$ex->getMessage()]);
        }
    }
  
    public function create(Request $request)
    {
        try
        {
            if(!self::hasPermiso('packings.registrar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $file          = $request['files'];
                $i             = self::getCantidadArchivos('packing',$request['id_cliente']);
                $semana        = Semana::findOrFail($request['id_semana']);
                $nombre_semana = $semana->nombre;
                $nombre_anio   = $semana->anio->anio;

                foreach ($file as $value) {
                    // formato del nombre 1_20_2017_(1).Certificado_001.pdf
                    // id_cliente,semana,año_actual,cantidad_archivo,nombre_archivo_default
                    $nombre = $request['id_cliente'].'_'.$nombre_semana.'_'.$nombre_anio.'_'.'('.$i.')'.$value->getClientOriginalName();
                    Packing::create([
                        'id_cliente'   => $request['id_cliente'],
                        'id_semana'    => $request['id_semana'],
                        // 'id_operador'  => $request['id_operador'],
                        'nombre'       => $nombre,
                        'fecha'        => date('Y-m-d'),
                        'hora'         => date('H:i:s'),
                        'url'          => $nombre
                    ]);
                    $i++;
                    \Storage::disk('packing')->put($nombre,  \File::get($value));
                    self::changeStatusContenedor(2,'packing',$request['id_cliente'],$request['id_semana']);
                    self::auditar('audit_packing',$nombre,'INSERTAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Archivos subidos correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al subir archivos.'.$ex->getMessage()]);
        }
    }


    public function destroy($id)
    {
        try{
            if(!self::hasPermiso('packings.eliminar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $packing = Packing::findOrFail($id);
                if ($packing) {
                    $packing->delete();
                    //$archivo = public_path('archivos/packing/'.$packing->nombre);
                    $archivo = public_path('archivos\\packing\\'.$packing->nombre);
                    if (file_exists($archivo)) {
                        \Storage::delete('packing/'.$packing->nombre); 
                    }
                    // cambiar de estado el tipo de archivo
                    self::changeStatusContenedor(1,'packing',$packing->id_cliente,$packing->id_semana);
                    self::auditar('audit_packing',$packing->nombre,'ELIMINAR');
                }
            DB::commit();
            return response()->json(['success'=>true,'message'=>'Arhivo eliminado correctamente.']);
        }catch(\Exception $ex){
            DB::rollback();
            return response()->json(['success'=>false,'message'=>'Error al eliminar el archivo.']);
        }
    }

    public function download($id)
    {
        try{
            $packing = Packing::findOrFail($id);
            //$archivo = public_path('archivos/packing/'.$packing->nombre);
            $archivo = public_path('archivos\\packing\\'.$packing->nombre);
            if(file_exists($archivo)){
                return response()->download($archivo);    
            }
                return response()->json(['success'=>false,'message'=>'El archivo que intenta descargar no existe']);
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'message'=>'Error al descargar el archivo.']);
        }
    }

    public function enviarEmail($id_packing)
    {   
        if(!self::hasPermiso('emails.enviar')){ return self::HasNoPermiso(); }
        $packing = Packing::find($id_packing);
        $cliente = $packing->cliente;
        $semana  = $packing->semana->nombre;
        // Cambio de estado a "Enviado" a los correos
        self::changeStatusContenedor(3,'packing',$packing->id_cliente,$packing->id_semana);
        self::changeStatusPacking($id_packing);
        // envió de emails
        return self::sendEmailCliente('packing',$packing->url,$cliente,$semana);
    }
}
