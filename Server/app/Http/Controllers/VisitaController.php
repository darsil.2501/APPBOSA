<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Traits\TraitVisita;
use Illuminate\Http\Request;

class VisitaController extends Controller
{
    use TraitVisita;

    public function getData($id_parcela)
    {
        return self::obtenerVisitas($id_parcela);
    }

    public function create(Request $request)
    {
        return self::registrarVisita($request);
    }

    public function update(Request $request,$id_visita)
    {
        return self::actualizarVisita($request, $id_visita);
    }

    public function delete($id_visita)
    {
        return self::eliminarVisita($id_visita);
    }

    public function getParcelas($id_visita)
    {
        return self::obtenerParcelasVisita($id_visita);
    }

    public function exportPDF($id_visita)
    {
        $visita = \App\Visita::find($id_visita);

        $data_visita = array(
            'id'                => $visita->id,
            'fecha'             => $visita->fecha, 
            'observacion'       => $visita->observacion, 
            'responsable'       => $visita->responsable, 
            'productor'         => $visita->parcela->productor->persona->fullname, 
            'codigo_productor'  => $visita->parcela->productor->codigo_productor, 
            'inspector'         => $visita->parcela->inspector->persona->fullname, 
            'area_parcela'      => $visita->parcela->ha_total,
            'empacadora'        => $visita->parcela->empacadora->nombre, 
            'sector'            => $visita->parcela->sector->nombre  
        );

        $preguntas = $visita->auditorias_internas;

        $data_formulario = array();
        foreach ($preguntas as $key => $pregunta) {
            $data_formulario[] = array(
                'respuesta' => ($pregunta->respuesta === 0) ? 'NO' : (($pregunta->respuesta === 1) ? 'SI' :'NA'),
                'observacion' => $pregunta->observacion,
                'titulo' => $pregunta->formulario->titulo
            );
        }

        $data_imagenes = $visita->galeria_visita;

        $data = array(
            'formulario' => $data_formulario,
            'visita'     => $data_visita,
            'imagenes'   => $data_imagenes
        );

        return self::exportPDFVisita($data);
    }
}
