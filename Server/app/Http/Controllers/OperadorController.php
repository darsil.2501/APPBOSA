<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Operador;
use App\Persona;
use App\User;
use DB;
use Hash;
use App\Traits\TraitFunciones;
use App\Traits\TraitOperador;
use App\Http\Requests\OperadorRequest;

class OperadorController extends Controller
{
    use TraitFunciones, TraitOperador;

    public function getData()
    {
        try{
            $operadores = Operador::where('activo',1)->orderBy('nombre','ASC')->get();
            return response()->json(['info'=>$operadores,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'Error al listar los operadores.'.$e->getMessage(),'success'=>false]);    
        }
    }

    public function create(OperadorRequest $request)
    {
        try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();   
                $operador = Operador::create(array_map("mb_strtoupper",$request->all()));
            DB::commit();
            self::auditar('audit_operador',$operador->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();   
        }catch(\Exception $e){
            DB::rollback();
            return self::ErrorInOperation($e);
        }
    }

    public function update(OperadorRequest $request,$id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $operador = Operador::find($id);
                if($operador){
                    $operador->fill(array_map("mb_strtoupper",$request->all()))->save();
            DB::commit();
            self::auditar('audit_operador',$operador->nombre,'ACTUALIZAR');
            return self::RegistroUpdateSuccess();
            }
        }catch(Exception $e){
            return self::ErrorInOperation($e);
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            DB::beginTransaction();
                $operador = Operador::find($id);
                if($operador){
                    if (count($operador->contenedor)) {
                        return self::RegistroInUse();
                    }
                    $operador->fill(['activo'=>DB::raw(0)])->save();
            DB::commit();
            self::auditar('audit_operador',$operador->nombre,'ELIMINAR');
            return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);
        }
    }

    public function whitContenedores(Request $request,$id_operador)
    {
        try {
            $id_semana = ($request->has('id_semana')) ? $request['id_semana'] : self::getIdSemanaActual() ; // determino que contenedores de que semana se envia
            $contenedores = self::getContenedoresOperador($id_operador,$id_semana);
            return response()->json(['info'=>$contenedores,'success'=>true]);
        } catch (\Exception $e) {
            return self::MensajePersonalizado(false,'Ocurrió un error al listar los contenedores, por favor inténtelo nuevamente. Error: '.$e->getMessage());
        }
    }

    public function whitArchivos(Request $request,$id_operador)
    {
        try {
            // determino que archivos de que semana se listaran
            $id_semana    = ($request->has('id_semana')) ? $request['id_semana'] : self::getIdSemanaActual() ; 
            $facturas     = self::getFacturasOperador($id_operador,$id_semana);
            $certificados = self::getCertificadosOperador($id_operador,$id_semana);
            $valijas      = self::getValijasOperador($id_operador,$id_semana);
            $packings     = self::getPackingsOperador($id_operador,$id_semana);
            return response()->json([
                'facturas'      =>  $facturas,
                'certificados'  =>  $certificados,
                'valijas'       =>  $valijas,
                'packings'      =>  $packings,
                'success'       =>  true
            ]);
        } catch (\Exception $e) {
            return self::MensajePersonalizado(false,'Ocurrió un error al listar los archivos, por favor inténtelo nuevamente. Error: '.$e->getMessage());
        }
    }
}
