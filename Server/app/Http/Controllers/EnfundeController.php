<?php

namespace App\Http\Controllers;

use App\Enfunde;
use App\Semana;
use App\Parcela;
use App\Traits\TraitEnfunde;
use DB;
use Illuminate\Http\Request;

class EnfundeController extends Controller
{
    use TraitEnfunde;

    public function getData()
    {
        
    }

    public function create(Request $request)
    {
        return self::registrarEnfunde($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function obtenerEmpacadoraData()
    {
         try {

            return response()->json(['success' => true,'info' => self::obtenerEmpacadoraDataEnfunde()]);
        } catch (\Exception $ex) {
            return response()->json(["success" => false, "message" => $ex->getMessage()]);
        }

    }
}
