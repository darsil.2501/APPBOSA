<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permiso;
use \DB;
use App\Http\Requests\PermisoRequest;
use App\Traits\TraitRespuesta;

class PermisoController extends Controller
{
    use TraitRespuesta;

    public function getData()
    {
        try{
            $permisos = Permiso::where('activo',1)->orderBy('slug')->get();
            return response()->json(['info'=>$permisos,'success'=>true]);    
        }catch(\Exception $e){
            return response()->json(['info'=>'error in data. Error: '.$e->getMessage(),'success'=>false]);    
        }
    }

    public function create(PermisoRequest $request)
    {
        try{
            Permiso::create($request->all());
            return response()->json(['message'=>'Permiso creado correctamente','success'=>'true']);    
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>'false']);       
        }
    }

    public function update(PermisoRequest $request,$id_opcion)
    {
        try{
            $permiso = Permiso::find($id_opcion);
            if($permiso){
                $permiso->fill($request->all())->save();
                return response()->json(['message'=>'Permiso actualizado correctamente','success'=>'true']);        
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data','success'=>'false']);       
        }
    }

    public function delete($id_permiso)
    {
        try{
            $permiso = Permiso::find($id_permiso);
            if($permiso){
                $registros = DB::table('permiso_rol')->where('permiso_id',$id_permiso)->get();
                if(count($registros)>0){return self::RegistroInUse();}
                $permiso->fill(['activo'=>DB::raw(0)])->save();
                return response()->json(['message'=>'Permiso eliminado correctamente','success'=>'true']);        
            }
        }catch(\Exception $e){
            return response()->json(['message'=>'error in data Error:'.$e->getMessage(),'success'=>false]);       
        }
    }
}
