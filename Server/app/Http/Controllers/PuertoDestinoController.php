<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PuertoDestino;
use DB;
use App\Http\Requests\PuertoDestinoRequest;

class PuertoDestinoController extends Controller
{  

    public function getData()
    {
        try{
            $puertos = PuertoDestino::where('activo',1)->get();
            $data = array();
            foreach ($puertos as $value) {
                $data[] = array(
                    'id'                => $value->id,
                    'nombre'            => $value->nombre,
                    'id_ciudad'         => $value->ciudad->id,
                    'ciudad'            => $value->ciudad->nombre,
                    'id_pais'           => $value->ciudad->pais->id,
                    'pais'              => $value->ciudad->pais->nombre,
                );
            }        
            return response()->json(['info'=>$data,'success'=>'true']);    
        }catch(Exception $e){
            return response()->json(['info'=>'error in data','success'=>false]);    
        }
    }

    public function create(PuertoDestinoRequest $request)
    {
        try{
            if(!self::hasPermiso('mantenimiento.registrar')){ return self::HasNoPermiso(); }
            $puerto = PuertoDestino::create(array_map("mb_strtoupper",$request->all()));
            self::auditar('audit_puerto_destino',$puerto->nombre,'INSERTAR');
            return self::RegistroCreateSuccess();
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function update(PuertoDestinoRequest $request,$id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.actualizar')){ return self::HasNoPermiso(); }
            $puerto = PuertoDestino::find($id);
            if($puerto){
                $puerto->fill(array_map("mb_strtoupper",$request->all()))->save();
                self::auditar('audit_puerto_destino',$puerto->nombre,'ACTUALIZAR');
                return self::RegistroUpdateSuccess();
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);       
        }
    }

    public function delete($id)
    {
        try{
            if(!self::hasPermiso('mantenimiento.eliminar')){ return self::HasNoPermiso(); }
            $puerto = PuertoDestino::find($id);
            if($puerto){
                if (count($puerto->contenedor->where('activo',1))) {
                    return self::RegistroInUse();
                }
                $puerto->fill(['activo'=>DB::raw(0)])->save();
                self::auditar('audit_puerto_destino',$puerto->nombre,'ELIMINAR');
                return self::RegistroDeleteSuccess();        
            }
        }catch(\Exception $e){
            return self::ErrorInOperation($e);              
        }
    }
}
