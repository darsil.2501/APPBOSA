<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\FactoriaExcel;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Sector;
use App\Semana;
use App\Inspector;
use App\Traits\TraitCaja;
use App\Traits\TraitContenedor;
use App\Traits\TraitExcel;
use App\Traits\TraitFunciones;
use App\Traits\TraitSemana;
use App\Traits\TraitInspector;
use DB;
use Illuminate\Http\Request;
use Mail;

class ExcelController extends Controller
{
    use TraitExcel,TraitContenedor, TraitFunciones, TraitSemana, TraitCaja, TraitInspector;

    #-------------------------- Para listar y reporte de contenedores en la opción semanas-------------------
    public function reporteSemana($id_semana)
    {
        $contenedores = self::cargarContenedores($id_semana);
        self::excelContenedores($contenedores);
    }

    public function datareporteSemana($id_semana)
    {
        $contenedores = self::cargarContenedores($id_semana);
        if(count($contenedores)>0){
            return response()->json(['success'=>true,'info'=>$contenedores]);
        }else{
            return response()->json(['success'=>false,'info'=>'No hay registros para esta semana.']);
        }
    }
    #-------------------------- Para listar y reporte de contenedores en la opción semanas-------------------


    #-------------------------- Para los reportes -----------------------------------------------------------
    
    public function reporteContenedoresPorSemana(Request $request)
    {
        $id_anio      = intval($request['id_anio']);
        $id_semana    = ($request->has('id_semana')) ? intval($request['id_semana']) : null ;
        $contenedores = self::contenedoresPorSemana($id_anio,$id_semana);
        self::excelContenedores($contenedores);
    }

    public function datareporteContenedoresPorSemana(Request $request)
    {
        $id_anio      = intval($request['id_anio']);
        $id_semana    = ($request->has('id_semana')) ? intval($request['id_semana']) : null ;
        $contenedores = self::contenedoresPorSemana($id_anio,$id_semana);
        if(count($contenedores)>0){
            return response()->json(['success'=>true,'info'=>$contenedores]);
        }else{
            return response()->json(['success'=>false,'info'=>'No hay registros para esta semana.']);
        }
    }

    public function reporteContenedoresPorCliente(Request $request)
    {
        $id_cliente = intval($request['id_cliente']);
        $id_anio    = intval($request['id_anio']);
        $id_semana  = ($request->has('id_semana')) ? $request['id_semana'] : null ;
        $contenedores = self::contenedorPorCliente($id_cliente,$id_anio,$id_semana);
        self::excelContenedoresPorClientes($contenedores);
    }

    public function datareporteContenedoresPorCliente(Request $request)
    {
        $id_cliente = $request['id_cliente'];
        $id_anio    = $request['id_anio'];
        $id_semana  = ($request->has('id_semana')) ? $request['id_semana'] : null ;
        $contenedores = self::contenedorPorCliente($id_cliente,$id_anio,$id_semana);
        if(count($contenedores)>0){
            return response()->json(['success'=>true,'info'=>$contenedores]);
        }else{
            return response()->json(['success'=>false,'info'=>'No hay registros para mostrar.']);
        }
    }

    public function reporteContenedoresPorOperador(Request $request)
    {
        $id_operador  = intval($request['id_operador']);
        $id_anio      = intval($request['id_anio']);
        $id_semana    = ($request->has('id_semana')) ? $request['id_semana'] : null ;
        $contenedores = self::contenedorPorOperador($id_operador,$id_anio,$id_semana);
        self::excelContenedoresPorOperadores($contenedores);
    }

    public function datareporteContenedoresPorOperador(Request $request)
    {
        $id_operador  = $request['id_operador'];
        $id_anio      = $request['id_anio'];
        $id_semana    = ($request->has('id_semana')) ? $request['id_semana'] : null ;
        $contenedores = self::contenedorPorOperador($id_operador,$id_anio,$id_semana);
        if(count($contenedores)>0){
            return response()->json(['success'=>true,'info'=>$contenedores]);
        }else{
            return response()->json(['success'=>false,'info'=>'No hay registros para mostrar.']);
        }
    }

    public function reporteMovimientosContenedores(Request $request)
    {
        $id_anio    = $request['id_anio'];
        $id_semana  = $request['id_semana'];
        $movimientos = self::reporteMovimientoContenedor($id_anio,$id_semana);
        self::excelMomivimientoContenedores($movimientos);
    }

    public function datareporteMovimientosContenedores(Request $request)
    {
        $id_anio    = $request['id_anio'];
        $id_semana  = $request['id_semana'];
        $movimientos = self::reporteMovimientoContenedor($id_anio,$id_semana);
        if(count($movimientos)>0){
            return response()->json(['success'=>true,'info'=>$movimientos]);
        }else{
            return response()->json(['success'=>false,'info'=>'No hay registros para esta semana.']);
        }
    }

    public function reporteDetalleContenedor($id_contenedor)
    {
        $detalles_contenedor    = self::detalleContenedor($id_contenedor);
        $movimientos_contenedor = self::movimientoContenedor($id_contenedor);
        self::excelDetalleContenedor($detalles_contenedor,$movimientos_contenedor);
    }

    public function reporteResumenCajasExportadas(Request $request)
    {
        try{
            $id_anio        = intval($request['id']);
            $id_anio_actual = ($id_anio != "null") ? intval($id_anio) : self::getIdAnioActual();
            $data_semanas   = Semana::where('id_anio',$id_anio_actual)->where('activo',1)->orderBy('nombre','ASC')->get();
            $data_clientes  = Cliente::has('cajas')->with('cajas')->where('activo',1)->orderBy('razon_social')->get();

            $data_caja_tmp = array();
            foreach ($data_clientes as $cliente) {
                foreach ($cliente->cajas as $caja) {
                    foreach ($data_semanas as $semana) {
                        $data_caja_tmp[] = array('total_cajas'=>self::getCantidadCajasPorClienteAndSemana($cliente->id,$caja->id,$semana->id));
                    }
                    $cajitas [] = array('marca'=>$caja->marca,'total_cajas'=>$data_caja_tmp);
                    unset($data_caja_tmp);
                }
                $clientes['razon_social'] = $cliente->razon_social;
                $clientes['cajas']        = $cajitas;
                $data_cliente_total[]     = $clientes;
                unset($cajitas);
            }

            $semanas = array();
            foreach ($data_semanas as $value) {
                $semanas []= ['nombre'              =>'SEM. '.$value->nombre,
                            'cantidad_contenedores' =>self::getContenedoresPorSemana($value->id)];
            }
            self::excelResumenCajasExportadas($semanas,$data_cliente_total,self::getAnio($id_anio_actual));
        }catch(\Exception $ex){
            return response()->json(['success'=>false,'mensaje'=>'Ocurrió un error al descargar el excel. '.$ex->getMessage()]);
        }
    }

    public function datareporteBuscarContenedor(Request $request)
    {
        $numero       = $request['numero_contenedor'];
        $booking      = $request['booking'];
        $contenedores =  self::buscarContenedor($numero,$booking);
        if(count($contenedores)>0){
            return response()->json(['success'=>true,'info'=>$contenedores]);
        }else{
            return response()->json(['success'=>false,'info'=>'No hay coincidencias.']);
        }
    }

    public function reporteBuscarContenedor(Request $request)
    {
        $numero       = $request['numero_contenedor'];
        $booking      = $request['booking'];
        $contenedores =  self::buscarContenedor($numero,$booking);
        self::excelBusquedaContenedores($contenedores);
    }
    #-------------------------- Fin funciones para reportes---------------------------------------
    
    #-------------------------- Reporte de auditorías---------------------------------------------
    public function reporteAuditorias(Request $request)
    {
        try{
            $tabla = $request['slug'];
            $data  = DB::table('audit_'.$tabla)->get();
            ($tabla === 'contenedor') ? self::excelAuditoriasPrincipal($data,$tabla) : self::excelAuditoriasSecundarias($data,$tabla) ;
        }catch (Exception $ex){
            return response()->json(['success'=>false,'mensaje'=>'Ocurrió un error al descargar el excel. '.$ex->getMessage()]);
        }
    }
    #-------------------------- Fin Reporte de auditorías---------------------------------------------
    
    #-------------------------- Reporte de Sectores Con El historial de sus delegados-----------------
    public function reporteSectoresConDelegados()
    {
        try{
            $sectores = Sector::with('delegados','delegados.productor.persona')->where('activo',1)->get();
            self::excelSectoresConDelegados($sectores);
            // return response()->json($sectores);
        }catch(Exception $ex){
            return response()->json(['success'=>false,'message'=>'error al listar los sectores'.$ex->getMessage()]);
        }
    }
    #-------------------------- Fin Reporte de Sectores Con El historial de sus delegados-------------

    #-------------------------- Reporte de inspector con sus parcelas por sector-----------------
    public function reporteInspectorConParcelas(Request $request)
    {
        try{
            $productores = self::obtenerParcelasInspector($request);
            $inspector   = Inspector::find($request['id_inspector']);
            $data = array(
                'productores' => $productores,
                'inspector'   => $inspector->persona
            );
            self::excelInspectorConParcelas($data);
        }catch(Exception $ex){
            return response()->json(['success'=>false,'message'=>'error al listar los sectores'.$ex->getMessage()]);
        }
    }
    #-------------------------- Fin Reporte de inspector con sus parcelas por sector-------------

    #-------------------------- Reporte de SIC-----------------
    public function reporteSIC(Request $request)
    {
        try{ 

            $visita = \App\Visita::find($request['id']);

            $data_visita = array(
                'id'                => $visita->id,
                'fecha'             => $visita->fecha, 
                'observacion'       => $visita->observacion, 
                'responsable'       => $visita->responsable, 
                'productor'         => $visita->parcela->productor->persona->fullname, 
                'codigo_productor'  => $visita->parcela->productor->codigo_productor, 
                'inspector'         => $visita->parcela->inspector->persona->fullname, 
                'area_parcela'      => $visita->parcela->ha_total,
                'empacadora'        => $visita->parcela->empacadora->nombre, 
                'sector'            => $visita->parcela->sector->nombre  
            );

            $preguntas = $visita->auditorias_internas;

            $data_formulario = array();
            foreach ($preguntas as $key => $pregunta) {
                $data_formulario[] = array(
                    'respuesta' => ($pregunta->respuesta === 0) ? 'NO' : (($pregunta->respuesta === 1) ? 'SI' :'NA'),
                    'observacion' => $pregunta->observacion,
                    'titulo' => $pregunta->formulario->titulo
                );
            }

            $data = array(
                'formulario' => $data_formulario,
                'visita'     => $data_visita
            );     
            //return $data['visita']['sector'];   
            self::excelSIC($data);
        }catch(Exception $ex){
            return response()->json(['success'=>false,'message'=>'error al listar los sectores'.$ex->getMessage()]);
        }
    }
    #-------------------------- Fin Reporte de SIC-------------
    

    public static function obtenerEmpacadoraEnfunde()
    {
        return self::obtenerEmpacadoraExcelEnfunde();
    }    

}
