<?php

namespace App;

use App\GaleriaVisita;
use App\Parcela;
use App\VisitaAuditoriaInterna;
use App\VisitaItemLabor;
use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
    protected $table    = 'visita';
    protected $fillable = ['fecha','observacion','responsable','formulario','id_parcela','activo'];
    public $timestamps  = false;

    public function parcela()
    {
    	return $this->belongsTo(Parcela::class,'id_parcela');
    }

    public function galeria_visita()
    {
    	return $this->hasMany(GaleriaVisita::class,'id_visita');
    }

    public function auditorias_internas()
    {
        return $this->hasMany(VisitaAuditoriaInterna::class,'id_visita');
    }

    public function visitas_item_labor()
    {
        return $this->hasMany(VisitaItemLabor::class,'id_visita');
    }
}