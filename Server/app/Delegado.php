<?php

namespace App;

use App\Productor;
use App\Sector;
use Illuminate\Database\Eloquent\Model;

class Delegado extends Model
{
    protected $table    = 'delegado';
    protected $fillable = ['id_sector','id_productor','fecha_inicio_cargo','fecha_fin_cargo'];
    public $timestamps  = false;

    public function sector()
    {
    	return $this->belongsTo(Sector::class,'id_sector');
    }

    public function productor()
    {
    	return $this->belongsTo(Productor::class,'id_productor');
    }
}
