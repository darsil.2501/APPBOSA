<?php

namespace App;

use App\Cargo;
use App\Productor;
use App\User;
use App\Inspector;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
    protected $table    = 'persona';
    protected $fillable = ['dni','nombre','apellido_paterno','apellido_materno','email','telefono','fullname','direccion','id_cargo','activo'];
    public $timestamps  = false;

    // public static $rules = [
    // 	'dni'              =>'required|min:8|max:8|numeric',
    // 	'nombre' 		   =>'required|min:2|max:50|alpha_num',
    // 	'apellido_materno' =>'required|min:2|max:50|alpha',
    // 	'apellido_paterno' =>'required|min:2|max:50|alpha',
    // 	'email' 		   =>'required|email|unique:persona,email',
    // 	'telefono' 		   =>'required|min:9|max:9|numeric',
    // 	'id_cargo'		   =>'required'
    // ];

    public function usuario()
    {
    	return $this->hasOne(User::class);
    }

    public function inspector()
    {
        return $this->hasOne(Inspector::class,'id_persona');
    }

    public function productor()
    {
        return $this->hasOne(Productor::class,'id_persona');
    }

    public function trabajador()
    {
        return $this->hasMany(Trabajador::class,'id_persona');
    }

    public function cargo()
    {
    	return $this->belongsTo(Cargo::class,'id_cargo');	
    }
}
