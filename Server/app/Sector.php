<?php

namespace App;

use App\Cliente;
use App\Contenedor;
use App\Delegado;
use App\Parcela;
use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $table    = 'sector';
    protected $fillable = ['nombre','ubicacion','activo'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    public function parcelas()
    {
    	return $this->hasMany(Parcela::class,'id_sector');
    }

    public function delegados()
    {
    	return $this->hasMany(Delegado::class,'id_sector');
    }
}
