<?php

namespace App;

use App\Racima;
use Illuminate\Database\Eloquent\Model;

class Cinta extends Model
{
    protected $table    = 'cinta';
    protected $fillable = ['nombre','color','activo'];
    public $hidden      = ['pivot'];
    public $timestamps  = false;    

    public function semanas()
    {
    	return $this->hasMany(Semana::class,'id_cinta');
    }
}
