<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Menu;
use App\User;
use App\Permiso;

class Rol extends Model
{
    
    protected $table    = 'rol';
    protected $fillable = ['nombre','id_menu','activo'];
    public $timestamps  = false;

    // public function usuario()
    // {
    // 	return $this->hasOne(User::class);
    // }

    public function menu()
    {
    	return $this->hasOne(Menu::class,'id_menu');
    }

    public function permisos()
    {
        return $this->belongsToMany(Permiso::class);
    }
}
