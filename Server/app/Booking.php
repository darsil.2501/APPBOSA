<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contenedor;

class Booking extends Model
{
    protected $table    = 'booking';
    protected $fillable = ['codigo','activo'];
    public $timestamps  = false;

    public static $rules = [
    	'codigo' => 'required'
    ];

    public static $messages = [
    	'codigo.required' => 'Debe ingresar un código que identifique este Booking.'
    ];

    public static function ValidarData($data)
    {
    	$reglas   = self::$rules;
    	$mensajes = self::$messages;
    	return \Validator::make($data,$reglas,$mensajes);
    }

    // public function contenedor()
    // {
    //     return $this->hasMany(Contenedor::class,'id_booking');
    // }
}
