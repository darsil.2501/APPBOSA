<?php

namespace App;

use App\Productor;
use App\Inspector;
use App\Parcela;
use Illuminate\Database\Eloquent\Model;

class Parcela extends Model
{
    protected $table    = 'parcela';
    protected $fillable = ['id_productor','id_empacadora','id_sector','id_inspector','ggn','ha_total','ha_sembrada','numero_parcela','codigo_parcela','tipo_propiedad','deshije','activo','estado_parcela'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    public function productor()
    {
    	return $this->belongsTo(Productor::class,'id_productor');
    }

    public function inspector()
    {
        return $this->belongsTo(Inspector::class,'id_inspector');
    }

    public function empacadora()
    {
        return $this->belongsTo(Empacadora::class,'id_empacadora');
    }

    public function sector()
    {
        return $this->belongsTo(Sector::class,'id_sector');
    }

    public function llenados()
    {
        return $this->hasMany(Llenado::class,'id_parcela');
    }
}
