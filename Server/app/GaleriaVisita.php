<?php

namespace App;

use App\Visita;
use Illuminate\Database\Eloquent\Model;

class GaleriaVisita extends Model
{
    protected $table    = 'galeria_visita';
    protected $fillable = ['id_visita','url'];
    public $timestamps  = false;

    public function visita()
    {
    	return $this->belognsTo(Visita::class,'id_visita');
    }
}
