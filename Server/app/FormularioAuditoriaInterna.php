<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormularioAuditoriaInterna extends Model
{
    protected $table    = 'formulario_auditoria_interna';
    protected $fillable = ['titulo','activo'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    // public function contenedor()
    // {
    // 	return $this->belongsToMany(Contenedor::class)->withPivot('cantidad');
    // }

    // public function cliente()
    // {
    // 	return $this->belongsToMany(Cliente::class);
    // }

}
