<?php

namespace App;

use App\Opcion;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Subopcion extends Model
{
   
    protected $table    = 'subopcion';
    protected $fillable = ['nombre','opcion_id','icono','url','activo'];
    protected $hidden   = ['pivot'];
    public $timestamps  = false;

    public function opcion()
    {
    	return $this->belongsTo(Opcion::class);
    }

    public function usuarios()
    {
    	return $this->belongsToMany(User::class,'subopcion_usuario','usuario_id','subopcion_id');
    }
}
