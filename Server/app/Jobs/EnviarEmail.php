<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class EnviarEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = ['mensaje'=>'hola'];
        
        Mail::send('email.plantilla',$data,function($mensaje){
            // $archivo = public_path('archivos\packing\enviar.xlsx');
            $archivo = public_path('archivos\certificado\enviar.pdf');
            $mensaje->subject('Bienvenido a AppBosa');
            $mensaje->to('luisramirezcoronado10@gmail.com');
            $mensaje->cc('netbeans_luis@hotmail.com');
            $mensaje->attach($archivo);
        });
        
    }
}
