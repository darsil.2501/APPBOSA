<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Opcion;

class Menu extends Model
{

    protected $table    = 'menu';
    protected $fillable = ['nombre','icono','activo'];
    protected $hidden   = ['pivot'];
    public $timestamps  = false;

    public function opciones()
    {
    	return $this->hasMany(Opcion::class,'id_menu');
    }
}
