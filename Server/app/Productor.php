<?php

namespace App;

use App\CondicionProductor;
use App\Delegado;
use App\Motivo;
use App\Parentesco;
use App\Persona;
use App\Trabajador;
use Illuminate\Database\Eloquent\Model;

class Productor extends Model
{
    protected $table    = 'productor';
    protected $fillable = ['id_persona','socio','codigo_productor','fecha_ingreso','fecha_cese','id_condicion','activo'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    public function persona()
    {
    	return $this->belongsTo(Persona::class,'id_persona');
    }

    public function parcelas()
    {
    	return $this->hasMany(Parcela::class,'id_productor');
    }

    public function motivo()
    {
        return $this->belongsTo(Motivo::class,'id_motivo');
    }

    public function delegados()
    {
        return $this->hasMany(Delegado::class,'id_productor');   
    }

    public function productorTrabajadores()
    {
        return $this->hasMany(ProductorTrabajador::class,'id_productor');
    }

    public function condicion_productor()
    {
        return $this->belongsTo(CondicionProductor::class,'id_condicion');
    }


    // Se hizo porque no encontre la forma de hacerlo desde el formrequest
    public static function validateUpdateProductor($data,$id_persona)
    {
        $reglas = [
            'nombre'           =>'required|min:2|max:30',
            'apellido_paterno' =>'min:2|max:30',
            'apellido_materno' =>'min:2|max:30',
            'dni'              =>'unique:persona,dni,'.$id_persona.',id,activo,1|regex:/^[0-9]{8}$/',
        ];

        $mensajes = [
            'nombre.required'           =>'Debe ingresar sus nombre',
            'nombre.min'                =>'Su nombre debe tener como mínimo 2 caracteres',
            'nombre.max'                =>'Su nombre debe tener como máximo 30 caracteres',
            'nombre.regex'              =>'Debe ingresar un nombre válido',

            'apellido_paterno.required' =>'Debe ingresar su apellido paterno',
            'apellido_paterno.min'      =>'Su apellido paterno debe tener como mínimo 2 caracteres',
            'apellido_paterno.max'      =>'Su apellido paterno debe tener como máximo 30 caracteres',
            'apellido_paterno.regex'    =>'Debe ingresar un apellido paterno válido',

            'apellido_materno.required' =>'Debe ingresar su apellido materno',
            'apellido_materno.min'      =>'Su apellido materno debe tener como mínimo 2 caracteres',
            'apellido_materno.max'      =>'Su apellido materno debe tener como máximo 30 caracteres',
            'apellido_materno.regex'    =>'Debe ingresar un apellido materno válido',

            'dni.required'              => 'Debe ingresar su DNI',
            'dni.unique'                => 'Ya existe este DNI.',
            'dni.regex'                 => 'Formato de DNI inválido.',
        ];
        return \Validator::make($data,$reglas,$mensajes);
    }


}
