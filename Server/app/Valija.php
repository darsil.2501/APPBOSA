<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cliente;
use App\Semana;
use App\Contenedor;
use App\EstadoArchivo;

class Valija extends Model
{
    protected $table    = 'valija';
    protected $fillable = ['nombre','id_cliente','id_semana','fecha','hora','url','id_estado_archivo'];
    public $timestamps  = false;

    public function contenedores()
    {
        return $this->belongsToMany(Contenedor::class);
    }

    public function cliente()
    {
    	return $this->belongsTo(Cliente::class,'id_cliente');
    }

    public function semana()
    {
    	return $this->belongsTo(Semana::class,'id_semana');
    }

    public function estado_archivo()
    {
        return $this->belongsTo(EstadoArchivo::class,'id_estado_archivo');
    }
}