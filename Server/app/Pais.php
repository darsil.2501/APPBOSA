<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ciudad;

class Pais extends Model
{
    protected $table    = 'pais';
	protected $fillable = ['nombre','activo'];
	public $timestamps  = false;

	public function ciudades()
	{
		return $this->hasMany(Ciudad::class,'id_pais');
	}
}
