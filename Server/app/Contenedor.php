<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Caja;
use App\Valija;
use App\Semana;
use App\Cliente;
use App\Booking;
use App\LineaNaviera;
use App\PuertoDestino;
use App\Operador;
use App\Estado;
use App\Movimiento;
use App\Factura;
use App\Certificado;
use App\Packing;
use App\Nave;
use App\Llenado;

class Contenedor extends Model
{
    protected $table    = 'contenedor';
    protected $fillable = ['id_semana',
    					   'id_cliente',
    					   'referencia',
    					   'booking',
    					   'numero_contenedor',
                           // 'id_nave',
                           'nave',
    					   'id_lineanaviera',
                           'viaje',
    					   'id_puertodestino',
    					   'id_operador',
    					   'fecha_proceso_inicio',
                           'fecha_proceso_fin',
    					   'fecha_zarpe',
    					   'fecha_llegada',
                           'peso_neto',
                           'peso_bruto',
    					   'activo',
                           'factura',
                           'certificado',
                           'packing',
                           'valija',
                           'vl',
                           'llenado'];
    public $timestamps  = false;

    //--------------------- Tablas pivot----------------------------------
    public function cajas()
    {
    	return $this->belongsToMany(Caja::class)->withPivot('cantidad');
    }

    public function valijas()
    {
        return $this->belongsToMany(Valija::class);
    }

    public function certificados()
    {
        return $this->belongsToMany(Certificado::class);
    }
    //--------------------- Fin de Tablas pivot----------------------------



    public function semana()
    {
        return $this->belongsTo(Semana::class,'id_semana');
    }

    public function llenados()
    {
        return $this->hasMany(Llenado::class,'id_contenedor');
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class,'id_cliente');
    }

    // public function nave()
    // {
    //     return $this->belongsTo(Nave::class,'id_nave');
    // }

    public function linea_naviera()
    {
        return $this->belongsTo(LineaNaviera::class,'id_lineanaviera');
    }

    public function puerto_destino()
    {
        return $this->belongsTo(PuertoDestino::class,'id_puertodestino');
    }

    public function operador()
    {
        return $this->belongsTo(Operador::class,'id_operador');
    }

    public function movimientos()
    {
        return $this->hasMany(Movimiento::class,'id_contenedor');
    }

    // public function facturas()
    // {
    //     return $this->hasMany(Factura::class);
    // }

    // public function certificados()
    // {
    //     return $this->hasMany(Certificado::class);
    // }

    // public function packings()
    // {
    //     return $this->hasMany(Packing::class);
    // }

    // reglas de validación para la actulización de un contenedor
    public static $rules = [
            'id_semana'             => 'required',
            // 'numero_contenedor'     => 'required|regex:/^[A-Z]{4}[\s][0-9]{6}[\-][0-9]{1}$/',
            'referencia'            => 'required',
            'id_cliente'            => 'required',
            'id_puertodestino'      => 'required',
            'booking'               => 'required',
            // 'id_nave'               => 'required',
            'nave'                  => 'required',
            'id_lineanaviera'       => 'required',
            'viaje'                 => 'required',
            'id_operador'           => 'required',
            // 'fecha_proceso_inicio'  => 'required|date',
            // 'fecha_proceso_fin'     => 'required|date',
            // 'fecha_zarpe'           => 'date|after:fecha_proceso_fin',
            // 'fecha_llegada'         => 'date|after:fecha_zarpe',
            // 'peso_neto'             => 'digits_between:4,5',
            // 'peso_bruto'            => 'digits_between:4,5',
            'cajas'                 => 'required'
    ];

    public static $messages = [
            'numero_contenedor.required'    => 'Debe ingresar el código del contenedor.',
            'numero_contenedor.regex'       => 'El formato del código del contenedor no es válido. (Ejm. MVCU 789098-1)',
            'referencia.required'           => 'Debe ingresar una referencia para este contenedor.',
            'id_cliente.required'           => 'Es obligatorio seleccionar el Cliente.',
            'id_puertodestino.required'     => 'Es obligatorio seleccionar el Puerto Destino.',
            'booking.required'              => 'Debe ingresar el Booking.',
            'id_nave.required'              => 'Es obligatorio seleccionar una Nave.',
            'id_lineanaviera.required'      => 'Es obligatorio seleccionar una Línea Naviera.',
            'id_operador.required'          => 'Es obligatorio seleccionar el Operador.',
            'fecha_proceso_inicio.required' => 'Debe ingresar la fecha inicial del proceso de este contenedor.',
            'fecha_proceso_inicio.date'     => 'El formato ingresado para la fecha inicial de proceso no es válido.',
            // 'fecha_proceso_inicio.after'    => 'La fecha inicial de proceso no es válida, ingrese una fecha de hoy o hacia adelante.',

            'fecha_proceso_fin.required'    => 'Debe ingresar la fecha de finalización del proceso de este contenedor.',
            'fecha_proceso_fin.date'        => 'El formato ingresado para la fecha de finalización del proceso no es válido.',
            // 'fecha_proceso_fin.after'       => 'La fecha de finalización del proceso debe ser posterior a la fecha inicial del proceso.',

            'fecha_zarpe.date'              => 'El formato ingresado para la fecha de zarpe no es válido.',
            'fecha_zarpe.after'             => 'La fecha de zarpe debe ser posterior a la fecha de finalización del proceso.',

            'fecha_llegada.date'            => 'El formato ingresado para la fecha de llegada no es válido.',
            'fecha_llegada.after'           => 'La fecha de llegada debe ser posterior a la fecha de zarpe.',
            'cajas.required'                => 'Este contenedor no puede ir vacío, por favor ingrese cajas.'
    ];

    public static function validar($data)
    {
        $reglas   = self::$rules;
        $mensajes = self::$messages;
        return \Validator::make($data,$reglas,$mensajes);
    }
}
