<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contenedor;

class Nave extends Model
{
    protected $table    = 'nave';
	protected $fillable = ['nombre','activo'];
	public $timestamps  = false;

	public function contenedor()
	{
		return $this->hasMany(Contenedor::class,'id_nave');
	}
}
