<?php

namespace App;

use App\Semana;
use Illuminate\Database\Eloquent\Model;

class OtrosArchivos extends Model
{
    protected $table    = 'otros_archivos';
    protected $fillable = ['nombre','id_semana','fecha','hora','url','tipo'];
    public $timestamps  = false;

    public function semana()
    {
    	return $this->belongsTo(Semana::class,'id_semana');
    }
}
