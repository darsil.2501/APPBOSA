<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contenedor;
use App\Cliente;

class Operador extends Model
{
    protected $table 	= 'operador';
	protected $fillable = ['nombre','activo'];
	public $timestamps 	= false;
	protected $hidden   = ['pivot'];

	public function contenedor()
	{
		return $this->hasMany(Contenedor::class,'id_operador');
	}

	public function clientes()
	{
		return $this->belongsToMany(Cliente::class);
	}
}
