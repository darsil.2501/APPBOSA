<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ItemLabor;
use App\Visita;

class VisitaItemLabor extends Model
{
    protected $table    = 'visita_item_labor';
    protected $fillable = ['total','id_visita','id_item_labor','activo'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    public function visita()
    {
    	return $this->belongsTo(Visita::class,'id_visita');
    }

    public function item_labor()
    {
    	return $this->belongsTo(ItemLabor::class,'id_item_labor');
    }

}
