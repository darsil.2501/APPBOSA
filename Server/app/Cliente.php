<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contenedor;
use App\Factura;
use App\Certificado;
use App\Packing;
use App\Operador;
use App\Caja;
use App\Emails;

class Cliente extends Model
{
    protected $table    = 'cliente';
	protected $fillable = ['razon_social','email','telefono','activo'];
	public $timestamps  = false;
    protected $hidden   = ['pivot'];

	public function contenedor()
	{
		return $this->hasMany(Contenedor::class,'id_cliente');
	}

	public function facturas()
    {
        return $this->hasMany(Factura::class,'id_cliente');
    }

    public function certificados()
    {
        return $this->hasMany(Certificado::class,'id_cliente');
    }

    public function packings()
    {
        return $this->hasMany(Packing::class,'id_cliente');
    }

    // para la asignacion de operadores y cajas a un cliente
    public function operadores()
    {
        return $this->belongsToMany(Operador::class);
    }

    public function cajas()
    {
        return $this->belongsToMany(Caja::class);
    }

    // un cliente puede tener muchos emails
    public function emails()
    {
        return $this->hasMany(Email::class,'id_cliente');
    }
}
