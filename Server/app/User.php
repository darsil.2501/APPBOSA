<?php

namespace App;

use App\Movimiento;
use App\Permiso;
use App\Persona;
use App\Rol;
use App\Subopcion;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nick', 'password','id_persona','id_cliente_operador','activo'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password','remember_token'];
    public $timestamps = false;


    //Relación entre usuario y persona
    public function persona()
    {
        return $this->belongsTo(Persona::class,'id_persona');
    }

    // public function rol()
    // {
    //     return $this->belongsTo(Rol::class,'id_rol');
    // }
    // 
    public function subopciones()
    {
        return $this->belongsToMany(Subopcion::class,'subopcion_usuario','usuario_id','subopcion_id');
    }

    public function permisos()
    {
        return $this->belongsToMany(Permiso::class,'permiso_usuario','usuario_id','permiso_id');
    }

    public function movimientos()
    {
        return $this->hasMany(Movimiento::class);
    }

    // Se hizo porque no encontre la forma de hacerlo desde el formrequest
    public static function validateUpdate($data,$id_persona)
    {
        $reglas = [
            // 'nombre'           =>'required|min:2|max:30|regex:/^[\pL\s\-]+$/u',
            // 'apellido_paterno' =>'required|min:2|max:30|regex:/^[\pL\s\-]+$/u',
            // 'apellido_materno' =>'required|min:2|max:30|regex:/^[\pL\s\-]+$/u',

            'nombre'           =>'required|min:2|max:30',
            'apellido_paterno' =>'min:2|max:30',
            'apellido_materno' =>'min:2|max:30',
            'dni'              =>'unique:persona,dni,'.$id_persona.',id|regex:/^[0-9]{8}$/',
            'telefono'         =>'unique:persona,telefono,'.$id_persona.',id|regex:/^[0-9]{9}$/',
            'email'            =>'email|unique:persona,email,'.$id_persona.',id,activo,1',
            'id_cargo'         =>'required',
            // 'id_rol'           =>'required',
            'nick'             =>'required|unique:usuario,nick,'.$id_persona.',id_persona,activo,1',
        ];

        $mensajes = [
            'nombre.required'           =>'Debe ingresar sus nombre',
            'nombre.min'                =>'Su nombre debe tener como mínimo 2 caracteres',
            'nombre.max'                =>'Su nombre debe tener como máximo 30 caracteres',
            'nombre.regex'              =>'Debe ingresar un nombre válido',

            'apellido_paterno.required' =>'Debe ingresar su apellido paterno',
            'apellido_paterno.min'      =>'Su apellido paterno debe tener como mínimo 2 caracteres',
            'apellido_paterno.max'      =>'Su apellido paterno debe tener como máximo 30 caracteres',
            'apellido_paterno.regex'    =>'Debe ingresar un apellido paterno válido',

            'apellido_materno.required' =>'Debe ingresar su apellido materno',
            'apellido_materno.min'      =>'Su apellido materno debe tener como mínimo 2 caracteres',
            'apellido_materno.max'      =>'Su apellido materno debe tener como máximo 30 caracteres',
            'apellido_materno.regex'    =>'Debe ingresar un apellido materno válido',

            'dni.required'              => 'Debe ingresar su DNI',
            'dni.unique'                => 'Ya existe este DNI.',
            'dni.regex'                 => 'Formato de DNI inválido.',

            'telefono.required'         => 'Debe ingresar un número de celular.',
            'telefono.unique'           => 'Ya se encuentra registrado este número de celular.',
            'telefono.regex'            => 'Debe ingresar un celular válido.', 

            'email.required'            => 'Debe ingresar su correo electrónico.',
            'email.email'               => 'Debe ingresar un correo electrónico válido.',
            'email.unique'              => 'Este correo electrónico ya se encuentra registrado.',

            'id_cargo.required'         => 'Debe seleccionar el Cargo.',
            // 'id_rol.required'           => 'Debe seleccionar el Rol',

        ];
        return \Validator::make($data,$reglas,$mensajes);
    }


}
