<?php

namespace App;

use App\Parentesco;
use App\Persona;
use App\Productor;
use App\Trabajador;
use Illuminate\Database\Eloquent\Model;

class ProductorTrabajador extends Model
{
    protected $table    = 'productor_trabajador';
    protected $fillable = ['id_productor','id_trabajador','id_parentesco','id_condicion','descripcion','is_productor','activo'];
    public $timestamps  = false;

    public function trabajador()
    {
    	return $this->belongsTo(Trabajador::class,'id_trabajador');
    }

    public function productor()
    {
    	return $this->belongsTo(Productor::class,'id_productor');
    }

    public function parentesco()
    {
    	return $this->belongsTo(Parentesco::class,'id_parentesco');
    }
}