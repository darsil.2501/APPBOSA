<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Semana;
use App\Parcela;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enfunde extends Model
{
    protected $table    = 'enfunde';
    protected $fillable = ['racimas_enfundadas','id_semana','id_parcela','activo'];
    protected $hidden   = ['pivot'];
    public $timestamps  = false;    

    public function semana()
    {
    	return $this->belongsTo(Semana::class,'id_semana');
    }

    public function parcela()
    {
    	return $this->belongsTo(Parcela::class);
    }
}
