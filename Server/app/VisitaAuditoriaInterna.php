<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Visita;
use App\FormularioAuditoriaInterna;

class VisitaAuditoriaInterna extends Model
{
    protected $table    = 'visita_auditoria_interna';
    protected $fillable = ['respuesta','observacion','id_visita','id_formulario_auditoria_interna','activo'];
    public $timestamps  = false;
    public $hidden      = ['pivot','activo'];

    public function visita()
    {
        return $this->belongsTo(Visita::class,'id_visita');
    }

    public function formulario()
    {
        return $this->belongsTo(FormularioAuditoriaInterna::class,'id_formulario_auditoria_interna');
    }

}
