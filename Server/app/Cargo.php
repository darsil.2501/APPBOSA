<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table    = 'cargo';
    protected $fillable = ['nombre','activo'];
    public $timestamps  = false;

    public function persona()
    {
    	return $this->hasOne(Persona::class,'id_cargo');
    }
}
