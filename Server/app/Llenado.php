<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contenedor;
use App\Parcela;


class Llenado extends Model
{
    
    protected $table    = 'llenado';

    protected $fillable = [
        'cajas',
        'pallet',
        'id_parcela',
        'id_contenedor',
        'activo'
    ];
    
    protected $hidden      = [
        'activo'
    ];

    public $timestamps  = false;

    public function contenedor()
    {
        return $this->belongsTo(Contenedor::class,'id_contenedor');
    }

    public function parcela()
    {
        return $this->belongsTo(Parcela::class,'id_parcela');
    }
    
}
        