<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contenedor;

class PuertoDestino extends Model
{
    protected $table    = 'puerto_destino';
	protected $fillable = ['nombre','id_ciudad','activo'];
	public $timestamps = false;

	public function contenedor()
	{
		return $this->hasMany(Contenedor::class,'id_puertodestino');
	}

	public function ciudad()
	{
		return $this->belongsTo(Ciudad::class,'id_ciudad');
	}
}
