<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeMCT extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:MCT {ModelName : the name of the model}';
    protected $nameModel = '';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crear nuevo Modelo, Controlador y Trait';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->nameModel = $this->argument('ModelName');
        $this->makeModel();
        $this->makeController();
        $this->makeTrait();
        //$this->updateRoute();
    }

    private function makeModel()
    {
        $folderPath = 'app';

        $newFileName = $this->nameModel . '.php';
        $newFilePath = base_path($folderPath) . '\\' . $this->nameModel . '.php';
        if (file_exists($newFilePath)) {
            $this->error('Model ya existe.');
            return;
        }

        $txt = $this->modelData();
        $handle = fopen($newFilePath, "w");
        fwrite($handle, $txt);
        fclose($handle);
        
        $this->info($this->nameModel . '.php Creado Correctamente');
        return;

    }

    private function modelData()
    {
        $body = "<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class " . $this->nameModel . " extends Model
{
    use SoftDeletes;
    
    protected " . substr('$', -1) . "table    = '" . strtolower($this->nameModel) . "';

    protected " . substr('$', -1) . "fillable = [
        'atr'
    ];
    
    protected " . substr('$', -1) . "hidden      = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public " . substr('$', -1) . "timestamps  = false;
    
}
        ";
        return $body;
    }

    #para crear controlador

    private function makeController()
    {
        $folderPath = 'app\Http\Controllers';

        $newFileName = $this->nameModel . 'Controller.php';
        $newFilePath = base_path($folderPath) . '\\' . $this->nameModel . 'Controller.php';
        if (file_exists($newFilePath)) {
            $this->error('Controlador ya existe.');
            return;
        }

        $txt = $this->controllerData();
        $handle = fopen($newFilePath, "w");
        fwrite($handle, $txt);
        fclose($handle);
        
        $this->info($this->nameModel . 'Controller.php Creado Correctamente');
        return;

    }

    private function controllerData()
    {
        $body = "<?php
namespace App\Http\Controllers;

use App\\". $this->nameModel .";
use App\Traits\Trait". $this->nameModel .";
use Illuminate\Http\Request;

class " . $this->nameModel . "Controller extends Controller
{
    use Trait". $this->nameModel .";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData()
    {
        return self::getData" . $this->nameModel . "();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request " . substr('$', -1) . "request)
    {
        return self::create" . $this->nameModel . "(" . substr('$', -1) . "request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request " . substr('$', -1) . "request
     * @param  ". substr('$', -1) . strtolower($this->nameModel) . "_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request " . substr('$', -1) . "request, " . substr('$', -1) . strtolower($this->nameModel) . "_id)
    {
        return self::update" . $this->nameModel . "(" . substr('$', -1) . "request, " . substr('$', -1) . strtolower($this->nameModel) . "_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ". substr('$', -1) . strtolower($this->nameModel) . "_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(" . substr('$', -1) . strtolower($this->nameModel) . "_id)
    {
        return self::delete" . $this->nameModel . "(" . substr('$', -1) . strtolower($this->nameModel) . "_id);
    }
}

        ";
        return $body;
    }

    #para crear el trait

    private function makeTrait()
    {
        $folderPath = 'app\Traits';

        if ( ! file_exists(base_path($folderPath)))
        {
            mkdir(base_path($folderPath));
        }

        $newFileName = 'Trait'.$this->nameModel . '.php';
        $newFilePath = base_path($folderPath) . '\Trait' . $this->nameModel . '.php';
        if (file_exists($newFilePath)) {
            $this->error('Trait ya existe.');
            return;
        }

        $txt = $this->traitData();
        $handle = fopen($newFilePath, "w");
        fwrite($handle, $txt);
        fclose($handle);
        
        $this->info('Trait' . $this->nameModel . '.php Creado Correctamente');
        return;

    }

    private function traitData()
    {
        $phpOpenTag = "<?php\n";
        $nameSpace = "namespace App\Traits;\n\nuse DB;\nuse App\\" . $this->nameModel . ";\n\n";
        $traitDeclarationStart = "trait Trait" . $this->nameModel . "\n" . "{\n";
        $traitDeclarationFunctionGetData  = "
    public function getData" . $this->nameModel . "()
    {
        try{

            " . substr('$', -1) . "data = " . $this->nameModel . "::get();
             
            return response()->json(['info'=>" . substr('$', -1) . "data,'success'=>true]);    
        }catch(\Exception " . substr('$', -1) . "e){
            return response()->json(['message'=>'Error al listar los registros'." . substr('$', -1) . "e->getMessage(),'success'=>false]);    
        }
    }";
        $traitDeclarationFunctionCreate  = "

    public function create" . $this->nameModel . "(" . substr('$', -1) . "request)
    {
        try{
            DB::beginTransaction();

                " . $this->nameModel . "::create([
                    'atr' =>  " . substr('$', -1) . "request['atr'],
                ]);

            DB::commit();             
            return response()->json(['message'=>'Registro Insertado Correctamente','success'=>true]);    
        }catch(\Exception " . substr('$', -1) . "e){
            DB::rollback();
            return response()->json(['message'=>'Error al insertar registro '." . substr('$', -1) . "e->getMessage(),'success'=>false]);    
        }
    }";
        $traitDeclarationFunctionUpdate  = "

    public function update" . $this->nameModel . "(" . substr('$', -1) . "request, " . substr('$', -1) . strtolower($this->nameModel) . "_id)
    {
        try{
            DB::beginTransaction();
            
                " . substr('$', -1) . strtolower($this->nameModel) . " = " . $this->nameModel . "::find(" . substr('$', -1) . "" . strtolower($this->nameModel) . "_id);
                " . substr('$', -1) . strtolower($this->nameModel) . "->fill([
                    'atr' =>  " . substr('$', -1) . "request['atr'],
                ])->save();

            DB::commit();             
            return response()->json(['message'=>'Registro Actualizado Correctamente','success'=>true]);    
        }catch(\Exception " . substr('$', -1) . "e){
            DB::rollback();
            return response()->json(['message'=>'Error al actualizar registro '." . substr('$', -1) . "e->getMessage(),'success'=>false]);    
        }
    }";
        $traitDeclarationFunctionDelete  = "
        
    public function delete" . $this->nameModel . "(" . substr('$', -1) . "" . strtolower($this->nameModel) . "_id)
    {
        try{
            DB::beginTransaction();
                " . substr('$', -1) . strtolower($this->nameModel) . " = " . $this->nameModel . "::find(" . substr('$', -1) . "" . strtolower($this->nameModel) . "_id);
                " . substr('$', -1) . strtolower($this->nameModel) . "->delete();
            DB::commit();             
            return response()->json(['message'=>'Registro Eliminado Correctamente','success'=>true]);    
        }catch(\Exception " . substr('$', -1) . "e){
            DB::rollback();
            return response()->json(['message'=>'Error al eliminar registro '." . substr('$', -1) . "e->getMessage(),'success'=>false]);    
        }
    }";
        $traitDeclarationEnd   = "\n\n}";
        return $phpOpenTag . $nameSpace . $traitDeclarationStart . $traitDeclarationFunctionGetData  . $traitDeclarationFunctionCreate . $traitDeclarationFunctionUpdate . $traitDeclarationFunctionDelete . $traitDeclarationEnd;
    }

    #para editar archivo api.php

    private function updateRoute()
    {
        $folderPath = 'routes';

        $nameFilePath = base_path($folderPath) . '\api.php';

        $txt = "
Route::group(['prefix' => '" . strtolower($this->nameModel) . "'], function() {
    Route::get('getdata','" . $this->nameModel . "Controller@getData');
    Route::post('create','" . $this->nameModel . "Controller@create');
    Route::post('update/{" . strtolower($this->nameModel) . "_id}','" . $this->nameModel . "Controller@update');
    Route::delete('delete/{" . strtolower($this->nameModel) . "_id}','" . $this->nameModel . "Controller@destroy');
});
        ";


        $handle = fopen($nameFilePath, "a");
        fwrite($handle, $txt);
        fclose($handle);
        
        $this->info("Archivo Api.php Modificado");
        return;

    }
}
