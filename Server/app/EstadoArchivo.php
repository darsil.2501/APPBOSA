<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Certificado;
use App\Factura;
use App\Packing;
use App\Valija;

class EstadoArchivo extends Model
{
    protected $table      = 'estado_archivo';
    protected $fillable   = ['nombre','color','activo'];
    public $timestamps    = false;

    public function certificados()
    {
    	return $this->hasMany(Certificado::class,'id_estado_archivo');
    }

    public function facturas()
    {
    	return $this->hasMany(Factura::class,'id_estado_archivo');
    }

    public function packings()
    {
    	return $this->hasMany(Packing::class,'id_estado_archivo');
    }

    public function valijas()
    {
    	return $this->hasMany(Valija::class,'id_estado_archivo');
    }


}
