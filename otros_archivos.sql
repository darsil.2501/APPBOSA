-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-11-2017 a las 16:25:47
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `appbosac_api`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otros_archivos`
--

CREATE TABLE `otros_archivos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `url` text NOT NULL,
  `tipo` tinyint(1) NOT NULL COMMENT '1 word, 2 excel, 3 pdf'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `otros_archivos`
--
ALTER TABLE `otros_archivos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_semana_otros_archivos` (`id_semana`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `otros_archivos`
--
ALTER TABLE `otros_archivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `otros_archivos`
--
ALTER TABLE `otros_archivos`
  ADD CONSTRAINT `id_semana_otros_archivos` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
