(function () {
	'use strict';
	angular.module('app')

		.config(mainConfig)
		.run(mainRun);


	function mainConfig($stateProvider, $urlRouterProvider) {

		$stateProvider

			//app core pages (errors, login,signup)
			.state('auth', {
				abstract: true,
				url: '/auth',
				template: '<div ui-view></div>'
			})
			//login
			.state('auth.login', {
				url: '/login',
				controller: 'LoginCtrl',
				templateUrl: 'app/views/login/login.html',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load('app/views/login/login.controller.js');
					}]
				}
			})

			.state('admin', {
				abstract: true,
				url: '/admin',
				controller: 'AdminCtrl',
				templateUrl: 'app/views/admin/admin.html',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load('app/views/admin/admin.controller.js');
					}],
					security: ['$q','$sesion', function($q,$sesion){
				        if(!$sesion.isLogged()){
				            return $q.reject("Not Authorized");
				        }
					}]
				}
			})

			.state('admin.dashboard', {
				url: '/dashboard',
				templateUrl: 'app/views/admin/dashboard/dashboard.html',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load('app/views/admin/dashboard/dashboard.controller.js');
					}]
				}
			})
			.state('admin.perfil', {
				url: '/perfil',
				templateUrl: 'app/views/admin/perfil/perfil.html',
				resolve: {
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load('app/views/admin/perfil/perfil.controller.js');
					}]
				}
			})

			//ruta para opcion container

			.state('admin.container', {
				abstract: true,
				url: '/container',
				template: '<div ui-view></div>'
			})
			.state('admin.container.lista', {
				url: '/lista',
				templateUrl: 'app/views/admin/container/lista-container/lista-container.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/container/lista-container/lista-container.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.container.lista');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})

			//lista para cliente
			.state('admin.container.lista_cliente', {
				url: '/lista_cliente',
				templateUrl: 'app/views/admin/container/lista-cliente/lista-cliente.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/container/lista-cliente/lista-cliente.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.container.lista_cliente');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.container.archivo_cliente', {
				url: '/archivo_cliente',
				templateUrl: 'app/views/admin/container/lista-cliente/archivo-cliente/archivo-cliente.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/container/lista-cliente/archivo-cliente/archivo-cliente.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.container.lista_cliente');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			//lista para operador
			.state('admin.container.lista_operador', {
				url: '/lista_operador',
				templateUrl: 'app/views/admin/container/lista-operador/lista-operador.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/container/lista-operador/lista-operador.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.container.lista_operador');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.container.archivo_operador', {
				url: '/archivo_operador',
				templateUrl: 'app/views/admin/container/lista-operador/archivo-operador/archivo-operador.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/container/lista-operador/archivo-operador/archivo-operador.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.container.lista_operador');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})

			//enfunde
	        .state('admin.enfunde', {
			  abstract: true,
			  url: '/enfunde',
			  template: '<div ui-view></div>'
			})
	        .state('admin.enfunde.semana', {
				url: '/semana',
				templateUrl: 'app/views/admin/enfunde/enfunde-semana.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/enfunde/enfunde-semana.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.enfunde.semana');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.enfunde.productor', {
				url: '/productor',
				templateUrl: 'app/views/admin/enfunde/enfunde-productor/enfunde-productor.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/enfunde/enfunde-productor/enfunde-productor.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.enfunde.semana');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.enfunde.recobro', {
				url: '/recobro',
				templateUrl: 'app/views/admin/enfunde/recobro-empacadora/recobro-empacadora.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/enfunde/recobro-empacadora/recobro-empacadora.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.enfunde.semana');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })

			//estadisticas
	        .state('admin.estadistica', {
			  abstract: true,
			  url: '/estadistica',
			  template: '<div ui-view></div>'
			})
	        .state('admin.estadistica.contenedor_anio', {
				url: '/contenedor_anio',
				templateUrl: 'app/views/admin/estadistica/contenedor-anio/contenedor-anio.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/estadistica/contenedor-anio/contenedor-anio.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.estadistica.contenedor_anio');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.estadistica.contenedor_cliente', {
				url: '/contenedor_cliente',
				templateUrl: 'app/views/admin/estadistica/contenedor-cliente/contenedor-cliente.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/estadistica/contenedor-cliente/contenedor-cliente.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.estadistica.contenedor_cliente');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.estadistica.contenedor_semana', {
				url: '/contenedor_semana',
				templateUrl: 'app/views/admin/estadistica/contenedor-semana/contenedor-semana.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/estadistica/contenedor-semana/contenedor-semana.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.estadistica.contenedor_semana');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })

	        //Productor

	        .state('admin.inspector', {
				abstract: true,
				url: '/inspector',
				template: '<div ui-view></div>'
			})
			.state('admin.inspector.lista', {
				url: '/inspector',
				templateUrl: 'app/views/admin/inspector/inspector.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/inspector/inspector.controller.js');
					}],
					// deps: ['$ocLazyLoad', function($ocLazyLoad) {
					// 	return $ocLazyLoad.load({
	    //                     name: 'viewCliente',
	    //                     insertBefore: '#ng_load_plugins_before',
	    //                     files: [
	    //                         'app/views/admin/productor/parcela-modal/parcela-modal.controller.js'
	    //                     ] 
	    //                 });
	    //             }],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.inspector.lista');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.inspector.inspector_parcela', {
				url: '/inspector_parcela',
				templateUrl: 'app/views/admin/inspector/inspector-parcela/inspector-parcela.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/inspector/inspector-parcela/inspector-parcela.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var lista = $permiso.validateRoute('admin.inspector.lista');
						var parcela = $permiso.validateRoute('admin.inspector.inspector_parcela');
						
						if (!lista && !parcela) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})

			.state('admin.inspector.visita_form_1', {
				url: '/visita_form_1',
				templateUrl: 'app/views/admin/inspector/inspector-parcela/fomulario-uno/fomulario-uno.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/inspector/inspector-parcela/fomulario-uno/fomulario-uno.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var lista = $permiso.validateRoute('admin.inspector.lista');
						var parcela = $permiso.validateRoute('admin.inspector.inspector_parcela');
						
						if (!lista && !parcela) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})

			.state('admin.inspector.visita_form_2', {
				url: '/visita_form_2',
				templateUrl: 'app/views/admin/inspector/inspector-parcela/fomulario-dos/fomulario-dos.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/inspector/inspector-parcela/fomulario-dos/fomulario-dos.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var lista = $permiso.validateRoute('admin.inspector.lista');
						var parcela = $permiso.validateRoute('admin.inspector.inspector_parcela');
						
						if (!lista && !parcela) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})

			.state('admin.inspector.visita_file', {
				url: '/visita_file',
				templateUrl: 'app/views/admin/inspector/inspector-parcela/visita-file/visita-file.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/inspector/inspector-parcela/visita-file/visita-file.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var lista = $permiso.validateRoute('admin.inspector.lista');
						var parcela = $permiso.validateRoute('admin.inspector.inspector_parcela');
						
						if (!lista && !parcela) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})

			//Llenados
	        .state('admin.llenado', {
			  abstract: true,
			  url: '/llenado',
			  template: '<div ui-view></div>'
			})
	        .state('admin.llenado.lista', {
				url: '/lista',
				templateUrl: 'app/views/admin/llenado/lista-llenado.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/llenado/lista-llenado.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.llenado.lista');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })

			//mantenimientos
	        .state('admin.mantenimiento', {
			  abstract: true,
			  url: '/mantenimiento',
			  template: '<div ui-view></div>'
			})
	        .state('admin.mantenimiento.caja', {
				url: '/caja',
				templateUrl: 'app/views/admin/mantenimiento/caja/caja.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/caja/caja.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.caja');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.cargo', {
				url: '/cargo',
				templateUrl: 'app/views/admin/mantenimiento/cargo/cargo.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/cargo/cargo.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.cargo');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.cinta', {
				url: '/cinta',
				templateUrl: 'app/views/admin/mantenimiento/cinta/cinta.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/cinta/cinta.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.cinta');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.ciudad', {
				url: '/ciudad',
				templateUrl: 'app/views/admin/mantenimiento/ciudad/ciudad.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/ciudad/ciudad.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.ciudad');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
			.state('admin.mantenimiento.cliente', {
				url: '/cliente',
				templateUrl: 'app/views/admin/mantenimiento/cliente/cliente.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/cliente/cliente.controller.js');
					}],
					deps: ['$ocLazyLoad', function($ocLazyLoad) {
						return $ocLazyLoad.load({
	                        name: 'viewCliente',
	                        insertBefore: '#ng_load_plugins_before',
	                        files: [
	                            'app/views/admin/mantenimiento/cliente/cliente-caja/cliente-caja.controller.js',
	                            'app/views/admin/mantenimiento/cliente/cliente-operador/cliente-operador.controller.js',
	                            'app/views/admin/mantenimiento/cliente/cliente-email/cliente-email.controller.js'
	                        ] 
	                    });
	                }],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.cliente');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.empacadora', {
				url: '/empacadora',
				templateUrl: 'app/views/admin/mantenimiento/empacadora/empacadora.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/empacadora/empacadora.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.empacadora');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.estado', {
				url: '/estado',
				templateUrl: 'app/views/admin/mantenimiento/estado/estado.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/estado/estado.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.estado');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.formulario', {
				url: '/formulario',
				templateUrl: 'app/views/admin/mantenimiento/formulario/formulario.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/formulario/formulario.controller.js');
					}],
					deps: ['$ocLazyLoad', function($ocLazyLoad) {
						return $ocLazyLoad.load({
	                        name: 'viewCliente',
	                        insertBefore: '#ng_load_plugins_before',
	                        files: [
	                            'app/views/admin/mantenimiento/formulario/formulario-interno/formulario-interno.controller.js',
	                            'app/views/admin/mantenimiento/formulario/formulario-dos/formulario-dos.controller.js'
	                        ] 
	                    });
	                }],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.formulario');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.linea_naviera', {
				url: '/linea_naviera',
				templateUrl: 'app/views/admin/mantenimiento/linea-naviera/linea-naviera.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/linea-naviera/linea-naviera.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.linea_naviera');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.operador', {
				url: '/operador',
				templateUrl: 'app/views/admin/mantenimiento/operador/operador.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/operador/operador.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.operador');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.pais', {
				url: '/pais',
				templateUrl: 'app/views/admin/mantenimiento/pais/pais.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/pais/pais.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.pais');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.puerto', {
				url: '/puerto',
				templateUrl: 'app/views/admin/mantenimiento/puerto/puerto.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/puerto/puerto.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.puerto');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })
	        .state('admin.mantenimiento.sector', {
				url: '/sector',
				templateUrl: 'app/views/admin/mantenimiento/sector/sector.html',
	            resolve: { 
	            	loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					  	return $ocLazyLoad.load('app/views/admin/mantenimiento/sector/sector.controller.js');
					}],
				    security: ['$q','$permiso', function($q,$permiso){
		                var menu = $permiso.validateRoute('admin.mantenimiento.sector');
		                if(!menu){
		                    return $q.reject("Not Authorized");
		                }
		           }]
				}
	        })

	        //Productor

	        .state('admin.productor', {
				abstract: true,
				url: '/productor',
				template: '<div ui-view></div>'
			})
			.state('admin.productor.productor', {
				url: '/productor',
				templateUrl: 'app/views/admin/productor/productor.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/productor/productor.controller.js');
					}],
					deps: ['$ocLazyLoad', function($ocLazyLoad) {
						return $ocLazyLoad.load({
	                        name: 'viewCliente',
	                        insertBefore: '#ng_load_plugins_before',
	                        files: [
	                            'app/views/admin/productor/parcela-modal/parcela-modal.controller.js'
	                        ] 
	                    });
	                }],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.productor.productor');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.productor.productor_parcela', {
				url: '/productor_parcela',
				templateUrl: 'app/views/admin/productor/productor-parcela/productor-parcela.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/productor/productor-parcela/productor-parcela.controller.js');
					}]
				}
			})
			.state('admin.productor.productor_trabajador', {
				url: '/productor_trabajador',
				templateUrl: 'app/views/admin/productor/productor-trabajador/productor-trabajador.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/productor/productor-trabajador/productor-trabajador.controller.js');
					}]
				}
			})

	        //reportes

	        .state('admin.reporte', {
				abstract: true,
				url: '/reporte',
				template: '<div ui-view></div>'
			})
			.state('admin.reporte.auditoria', {
				url: '/auditoria',
				templateUrl: 'app/views/admin/reporte/auditoria/auditoria.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/reporte/auditoria/auditoria.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.reporte.auditoria');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.reporte.buscar_contenedor', {
				url: '/buscar_contenedor',
				templateUrl: 'app/views/admin/reporte/buscar-contenedor/buscar-contenedor.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/reporte/buscar-contenedor/buscar-contenedor.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.reporte.buscar_contenedor');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.reporte.cajas_exportadas', {
				url: '/cajas_exportadas',
				templateUrl: 'app/views/admin/reporte/cajas-exportadas/cajas-exportadas.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/reporte/cajas-exportadas/cajas-exportadas.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.reporte.cajas_exportadas');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.reporte.enfunde_empacadora', {
				url: '/enfunde_empacadora',
				templateUrl: 'app/views/admin/reporte/enfunde-empacadora/enfunde-empacadora.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/reporte/enfunde-empacadora/enfunde-empacadora.controller.js');
					}],
					// security: ['$q', '$permiso', function ($q, $permiso) {
					// 	var menu = $permiso.validateRoute('admin.reporte.enfunde_empacadora');
					// 	if (!menu) {
					// 		return $q.reject("Not Authorized");
					// 	}
					// }]
				}
			})
			.state('admin.reporte.reporte_semana', {
				url: '/reporte_semana',
				templateUrl: 'app/views/admin/reporte/reporte-semana/reporte-semana.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/reporte/reporte-semana/reporte-semana.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.reporte.reporte_semana');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.reporte.movimiento_contenedores', {
				url: '/movimiento_contenedores',
				templateUrl: 'app/views/admin/reporte/movimiento-contenedores/movimiento-contenedores.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/reporte/movimiento-contenedores/movimiento-contenedores.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.reporte.movimiento_contenedores');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.reporte.contenedores_operador', {
				url: '/contenedores_operador',
				templateUrl: 'app/views/admin/reporte/contenedores-operador/contenedores-operador.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/reporte/contenedores-operador/contenedores-operador.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.reporte.contenedores_operador');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.reporte.contenedores_cliente', {
				url: '/contenedores_cliente',
				templateUrl: 'app/views/admin/reporte/contenedores-cliente/contenedores-cliente.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/reporte/contenedores-cliente/contenedores-cliente.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.reporte.contenedores_cliente');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})

			//semana

	        .state('admin.semana', {
				abstract: true,
				url: '/semana',
				template: '<div ui-view></div>'
			})
			.state('admin.semana.lista', {
				url: '/lista',
				templateUrl: 'app/views/admin/semana/semana.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/semana/semana.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.semana.lista');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})

			.state('admin.semana.contenedor', {
				url: '/contenedor',
				templateUrl: 'app/views/admin/semana/semana-contenedor/semana-contenedor.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/semana/semana-contenedor/semana-contenedor.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.semana.lista');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.semana.archivo', {
				url: '/archivo',
				templateUrl: 'app/views/admin/semana/semana-archivo/semana-archivo.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/semana/semana-archivo/semana-archivo.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.semana.lista');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})

			//Usuario

	        .state('admin.seguridad', {
				abstract: true,
				url: '/seguridad',
				template: '<div ui-view></div>'
			})
			.state('admin.seguridad.usuario', {
				url: '/usuario',
				templateUrl: 'app/views/admin/seguridad/usuario.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/seguridad/usuario.controller.js');
					}],
					security: ['$q', '$permiso', function ($q, $permiso) {
						var menu = $permiso.validateRoute('admin.seguridad.usuario');
						if (!menu) {
							return $q.reject("Not Authorized");
						}
					}]
				}
			})
			.state('admin.seguridad.configuracion', {
				url: '/configuracion',
				templateUrl: 'app/views/admin/seguridad/usuario-configuracion/usuario-configuracion.html',
				resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
						// you can lazy load files for an existing module
						return $ocLazyLoad.load('app/views/admin/seguridad/usuario-configuracion/usuario-configuracion.controller.js');
					}],
					deps: ['$ocLazyLoad', function($ocLazyLoad) {
						return $ocLazyLoad.load({
	                        name: 'viewUsuario',
	                        insertBefore: '#ng_load_plugins_before',
	                        files: [
	                            'app/views/admin/seguridad/usuario-configuracion/configuracion-opcion/configuracion-opcion.controller.js',
	                            'app/views/admin/seguridad/usuario-configuracion/configuracion-permiso/configuracion-permiso.controller.js'
	                        ] 
	                    });
	                }]
				}
			});

		$urlRouterProvider.otherwise('/auth/login');

	}

	function mainRun($rootScope, $auth, $templateCache) {

		// $rootScope.$on('$viewContentLoaded', function() {
		//      	$templateCache.removeAll();
		//   	});

		$rootScope.$on('$stateChangeError', change);

		function change(e, toState, toParams, fromState, fromParams, error) {
			if (error === "Not Authorized") {
				$auth.logout();
			}
		}

	}

})()