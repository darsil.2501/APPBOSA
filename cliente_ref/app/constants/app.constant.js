(function () {
	'use strict';
	angular.module('app')

	.constant('URL_HTTP', '../Server/public/index.php/')
	.constant('URL_VIEW_CLIENTE', 'app/views/admin/mantenimiento/cliente/')
	.constant('URL_VIEW_FORM', 'app/views/admin/mantenimiento/formulario/')
	.constant('URL_VIEW_PRODUCTOR', 'app/views/admin/productor/')
	.constant('URL_VIEW_USER', 'app/views/admin/seguridad/usuario-configuracion/')	
	.constant('URL_IMAGE_VISITA', '../Server/public/archivos/galeria_visita/');

})()