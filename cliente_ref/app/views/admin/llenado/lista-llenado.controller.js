(function () {
	'use strict';
	angular.module('app')
		.controller('LlenadoController', LlenadoController)
		.controller('LlenadoControllerModal',LlenadoControllerModal);

	function LlenadoController($scope, $crud, $uibModal, $file, $swal) {

		var self = $scope;

		self.isLoading = true;
		self.contenedores = [];
		self.paramsModal = {};
		self.model = {};
		self.obj = { isSave : true, id_semana : self.model.id_semana };

		self.list = function (isLoading) {//parametro para activar el loading: true o false
			self.isLoading = isLoading;
			$crud.get('llenado/getdata/'+ self.model.id_semana).then(function (res) {
				self.contenedores = (res.data.info);
				self.isLoading = false;
				self.listNoLLenado();
				self.listCodigos();
			});
		};

		self.listNoLLenado = function () {
			$crud.get('contenedor/sin_llenado/' + self.model.id_semana).then(function (res) {
				self.paramsModal.contenedores = (res.data.info);
			});
		};

		self.listCodigos = function () {
			$crud.get('parcela/get_codigos').then(function (res) {
				self.paramsModal.codigos = (res.data.info);
			});
		};

		self.listAnio = function () {
			$crud.get('anio/give_semanas').then(function (res) {
				self.anios = res.data.info;
				self.model.id_anio = self.anios[self.anios.length - 1].id;
				self.listSemana(self.model.id_anio);
			});
		};
		self.listAnio();

		self.listSemana = function (id_anio) {
			$crud.get('anio/withSemana/' + id_anio, null).then(function (res) {
				if (res.data.success) {
					self.semanas = res.data.info;
					self.model.id_semana = self.semanas[0].id;
					self.list(true);
				}
			});
		};

		self.load = function(llenado)
		{	
			llenado.isSave = false;
			self.obj = angular.copy(llenado);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true, id_semana : self.model.id_semana };
		}

		self.download = function ()
		{
			self.isDownload = true;
			self.obj.file = 1;//especificar que es excel con 1
			self.obj.nameFile = 'semana'+self.model.id_semana;
			$file.download('llenado/get_packing/' + self.model.id_semana, self.obj)
			.then((res) => {
				self.obj = {};
				self.isDownload = false;
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.isDownload = false;
			});
		}

		self.downloadPDF = function (contenedor)
		{
			self.flag = true;
			self.obj.file = 2;//especificar que es pdf con 2
			self.obj.nameFile = contenedor.numero_contenedor;
			$file.download('llenado/pdf/'+contenedor.id,self.obj)
			.then((res) => {
				self.obj = {};
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
			});
		}

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'llenado_modal.html',
				controller: 'LlenadoControllerModal',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.paramsModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'llenado_delete_modal.html',
				controller: 'LlenadoControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.paramsModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function LlenadoControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal)
	{
		var self = $scope;

		self.obj = OBJ;
		self.contenedores = PARAMS.contenedores;
		self.codigos = PARAMS.codigos;
		self.httpRequest = false;

		self.pallets = [
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			],
			[
				{parcela : null, cajas : 54}
			]
		];

		self.addItem = function(pallet)
		{
			pallet.push({parcela : null, cajas : 54});
		}
		
		self.deleteItem = function(pallet)
		{
			if (pallet.length > 1) {
				pallet.pop();
			}
		}

		self.createEdit = function()
		{
			self.obj.pallets = self.pallets;
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('llenado',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('llenado',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('llenado',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()