(function () {
	'use strict';
	angular.module('app')
	.controller('CajasExportadasController', CajasExportadasController);

	function CajasExportadasController($scope, $crud, $file, $swal) 
	{

		var self = $scope;

		self.obj = {};
		self.flag = false;

		self.listAnio = function(){
			$crud.get('anio/give_semanas').then(function(res){
				self.anios = res.data.info;
			});
		};
		self.listAnio();

		self.download = function()
		{
			self.flag = true;
			self.obj.file = 1;//especificar que es excel con 1
			self.obj.nameFile = 'cajas_exportadas';
			$file.download('excel/cajas_exportadas',self.obj)
			.then((res) => {
				self.flag = false;
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.flag = false;
			});
		};

	}

})()