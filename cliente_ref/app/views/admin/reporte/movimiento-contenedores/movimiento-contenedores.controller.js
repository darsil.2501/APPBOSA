(function () {
	'use strict';
	angular.module('app')
	.controller('MovimientoContenedorController', MovimientoContenedorController);

	function MovimientoContenedorController($scope, $crud, $file, $swal) 
	{

		var self = $scope;

		self.obj = {};
		self.flag = false;
		self.flagLoader = false;		
		self.flagDownload = false;
		self.contenedores = [];

		self.listAnios = function(){
			$crud.get('anio/give_semanas').then(function(res){
				self.anios = res.data.info;
			});
		};
		self.listAnios();

		self.listSemana = function(id_anio){
			$crud.get('anio/withSemana/' + id_anio).then(function(res){
				if(res.data.success){
					self.semanas = res.data.info;
				}
			});
		};

		self.preview = function()
		{
			self.flag = true;
			$crud.post('data/movimientos_contenedor', self.obj)
			.then((res) => {
				self.contenedores = (res.data.success) ? res.data.info : [];
				self.flag = false;
				self.flagLoader = true;
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.flag = false;
			});
		};

		self.download = function()
		{
			self.flagDownload = true;
			self.obj.file = 1;//especificar que es excel con 1
			self.obj.nameFile = 'movimientos_contenedor';
			$file.download('excel/movimientos_contenedor',self.obj)
			.then((res) => {
				self.flagDownload = false;
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.flagDownload = false;
			});
		};

	}

})()