(function () {
	'use strict';
	angular.module('app')
	.controller('BuscarContenedorController', BuscarContenedorController);

	function BuscarContenedorController($scope, $crud, $file, $swal) 
	{

		var self = $scope;

		self.obj = {};
		self.flag = false;
		self.flagLoader = false;		
		self.flagDownload = false;
		self.contenedores = [];

		self.preview = function()
		{
			self.flag = true;
			$crud.post('data/buscar_contenedor',self.obj)
			.then((res) => {
				self.contenedores = (res.data.success) ? res.data.info : [];
				self.flag = false;
				self.flagLoader = true;
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.flag = false;
			});
		};

		self.download = function()
		{
			self.flagDownload = true;
			self.obj.file = 1;//especificar que es excel con 1
			self.obj.nameFile = 'reporte_buscar_contenedor';
			$file.download('excel/buscar_contenedor',self.obj)
			.then((res) => {
				self.flagDownload = false
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.flagDownload = false;
			});
		};

	}

})()