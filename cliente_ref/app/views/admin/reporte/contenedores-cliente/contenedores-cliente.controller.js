(function () {
	'use strict';
	angular.module('app')
	.controller('ContenedorClienteController', ContenedorClienteController);

	function ContenedorClienteController($scope, $crud, $file, $swal) 
	{

		var self = $scope;

		self.obj = {};
		self.flag = false;
		self.flagLoader = false;		
		self.flagDownload = false;
		self.contenedores = [];

		self.listAnios = function(){
			$crud.get('anio/give_semanas').then(function(res){
				self.anios = res.data.info;
			});
		};
		self.listAnios();

		self.listSemana = function(id_anio){
			$crud.get('anio/withSemana/' + id_anio).then(function(res){
				if(res.data.success){
					self.semanas = res.data.info;
				}
			});
		};

		self.listCliente = function(){
			$crud.list('cliente').then(function(res){
				self.clientes = res.data.info;
			});
		};
		self.listCliente();

		self.preview = function()
		{
			self.flag = true;
			$crud.post('data/contenedores_cliente', self.obj)
			.then((res) => {
				self.contenedores = (res.data.success) ? res.data.info : [];
				self.flag = false;
				self.flagLoader = true;
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.flag = false;
			});
		};

		self.download = function()
		{
			self.flagDownload = true;
			self.obj.file = 1;//especificar que es excel con 1
			self.obj.nameFile = 'contenedores_cliente';
			$file.download('excel/contenedores_cliente',self.obj)
			.then((res) => {
				self.flagDownload = false;
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.flagDownload = false;
			});
		};

	}

})()