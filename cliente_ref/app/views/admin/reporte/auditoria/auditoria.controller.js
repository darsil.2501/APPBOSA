(function () {
	'use strict';
	angular.module('app')
	.controller('AuditoriaController', AuditoriaController);

	function AuditoriaController($scope, $crud, $file, $swal) 
	{

		var self = $scope;

		self.obj = {};
		self.flag = false;

		self.list = function()
		{
			$crud.list('tablas').then((res) => {
				self.tablas = res.data.info;
			});
		};
		self.list();

		self.download = function()
		{
			self.flag = true;
			self.obj.file = 1;//especificar que es excel con 1
			self.obj.nameFile = 'auditoria';
			$file.download('excel/auditoria',self.obj)
			.then((res) => {
				self.flag = false;
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.flag = false;
			});
		};

	}

})()