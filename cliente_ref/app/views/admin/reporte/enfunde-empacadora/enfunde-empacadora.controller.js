(function(){
	'use strict';
	angular.module('app')
	.controller('EnfundeEmpacadoraController',EnfundeEmpacadoraController)

	function EnfundeEmpacadoraController($scope,$crud,$swal,$permiso, $file)
	{

		var self = $scope;

		self.isLoading = true;
		self.semanas = [];
		self.empacadoras = [];
		self.totales = [];
		self.isDownload = false;
		self.obj = {};

		self.preview = function()
		{
			self.isLoading = true;
			$crud.get('enfunde/getdataempacadora').then((res) => {
				self.empacadoras = res.data.info.empacadoras;
				self.semanas = res.data.info.semanas;
				self.isLoading = false;
				self.loadingTotales();
			})
		}

		self.preview();

		self.loadingTotales = function()
		{

			angular.forEach(self.empacadoras,(empacadora, i) => {
				self.cont = 0;
				self.totales[self.cont] = ( i === 0) ? empacadora.cantidad_parcelas : self.totales[self.cont]+empacadora.cantidad_parcelas;
				self.cont ++;
				angular.forEach(empacadora.data,(data, j) => {
					self.totales[self.cont] = ( i === 0) ? data.total_enfunde : self.totales[self.cont]+data.total_enfunde;
					self.cont ++;
				});
			});

		}

		self.download = function()
		{
			self.isDownload = true;
			self.obj.file = 1;//especificar que es excel con 1
			self.obj.nameFile = 'enfunde_empacadoras';
			$file.download('excel/enfunde_empacadoras',self.obj)
			.then((res) => {
				self.isDownload = false;
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.isDownload = false;
			});
		}

		

	}

})()