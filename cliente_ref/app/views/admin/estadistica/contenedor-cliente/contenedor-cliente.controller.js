(function () {
	'use strict';
	angular.module('app')
	.controller('ContenedorClienteController', ContenedorClienteController);

	function ContenedorClienteController($scope, $crud,  $swal) 
	{

		var self = $scope;
		var chart;
		
		self.isLoading = true;
		self.obj = {};

		self.listAnio = function(){
	      $crud.get('anio/give_semanas').then(function(res){
	        self.anios = res.data.info;
	        self.obj.id_anio = (self.anios[self.anios.length-1].id);
	        self.getChart();
	      });
	    };
	    self.listAnio();

		self.getChart = function()
		{
			self.isLoading = true;
			$crud.post('grafico/contenedores_cliente_new/' + self.obj.id_anio, {}).then((res) => {
				self.setData(res.data.data_provider, res.data.data_graphs);
				self.isLoading = false;
			});
		};

		self.setData = function(data,data_graph){
			chart = AmCharts.makeChart("chartdiv", {
              	"type": "serial",
              	"theme": "light",
              	"legend": {
              		"horizontalGap": 10,
              		"maxColumns": 10,
              		"position": "top",
              		"useGraphSettings": true,
              		"markerSize": 10
              	},
              	"categoryField": "mes",
              	"startDuration": 1,
              	"categoryAxis": {
                	"gridPosition": "start",
                	"position": "left",
                	"labelRotation": 45,
              	},
              	"trendLines": [],
              	"graphs": data_graph,
              	"guides": [],
              	"valueAxes": [
	                {
	                  "id": "ValueAxis-1",
	                  "position": "left",
	                  "axisAlpha": 0,
	                  "minimum": 0
	                }
              	],
              	"allLabels": [],
              	"balloon": {},
              	"titles": [],
              	"dataProvider": data,
              	"export": {
                	"enabled": true,
                	"menu": [ 
                		{
	                  		"class": "export-main",
	                  		"menu": [ 
	                  		{
	                    		"label": "Descargar",
	                    		"menu": [ "PNG", "JPG", "PDF" ]
	                  		}, 
	                  		{ 
	                  			"format": "PRINT",
	                 	 		"label": "Imprimir"
	                		}
	                		]
                		} 
                	]	                    
              	},
              	"responsive": {
                	"enabled": true
              	}
        	});
  		}

	}

})()