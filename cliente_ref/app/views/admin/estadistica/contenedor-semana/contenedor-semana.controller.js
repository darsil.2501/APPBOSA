(function () {
	'use strict';
	angular.module('app')
	.controller('ContenedorSemanaController', ContenedorSemanaController);

	function ContenedorSemanaController($scope, $crud,  $swal) 
	{

		var self = $scope;
		
		self.isLoading = false;
		self.obj = {};

		self.listAnio = function(){
	      $crud.get('anio/give_semanas').then(function(res){
	        self.anios = res.data.info;
	        self.obj.id_anio = (self.anios[self.anios.length-1].id);
	        self.listSemana(self.obj.id_anio);
	      });
	    };
	    self.listAnio();

	    self.listSemana = function(id_anio){
			$crud.get('anio/withSemana/' + id_anio).then(function(res){
				if(res.data.success){
					self.semanas = (res.data.info).reverse();
					self.obj.id_semana_start = self.semanas[0].id;
					self.obj.id_semana_last = self.semanas[self.semanas.length-1].id;
					self.getChart();
				}
			});
		};

		self.getChart = function()
		{
			self.isLoading = true;
			$crud.post('grafico/contenedores_semana_new', self.obj).then((res) => {
				if (res.data.success) {
					self.setData(res.data.data_provider);				
		  			self.promedio_semanal = res.data.promedio_semanal;
					self.isLoading = false;
				}
				else
				{
					$swal.open(res);
					self.obj.id_semana_start = self.semanas[0].id;
					self.obj.id_semana_last = self.semanas[self.semanas.length-1].id;
					self.getChart();
				}
			});
		};

		self.setData = function(data){
			AmCharts.makeChart( "chartdiv", {
			  	"type": "serial",
			  	"theme": "light",
			  	"balloon": {
			    	"adjustBorderColor": false,
			    	"horizontalPadding": 10,
			    	"verticalPadding": 8,
			    	"color": "#ffffff"
			  	},
   			  	"dataProvider": data,
			  	"valueAxes": [ {
				    "axisAlpha": 0,
				    "position": "left",
				    "title": "Cantidad de Contenedores",
				    "minimum": 0
			  	}],
			  	"startDuration": 1,
			  	"graphs": [ 
				  	{
					    "alphaField": "alpha",
					    "balloonText": "<span style='font-size:12px;'>[[title]] en [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
					    "fillAlphas": 1,
						// "fillColorsField": "color_barra",
					    "title": "Total Contenedores",
					    "type": "column",
					    "valueField": "total_contenedores",
					    "dashLengthField": "dashLengthColumn",
					    "labelText":"[[value]]",
					    "fontSize":10
				  	}, 
				  	{
					    "id": "graph2",
					    "balloonText": "<span style='font-size:12px;'>[[title]] en [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
					    "bullet": "round",
					    "lineThickness": 3,
					    "bulletSize": 7,
					    "bulletBorderAlpha": 1,
					    "bulletColor": "#FFFFFF",
					    "useLineColorForBulletBorder": true,
					    "bulletBorderThickness": 3,
					    "fillAlphas": 0,
						"lineColorField": "color_linea",
						// "fillColorsField": "color_barra",
					    "lineAlpha": 1,
					    "title": "Total Contenedores",
					    "valueField": "total_contenedores1",
					    "dashLengthField": "dashLengthLine"
				  	} 
			  	],
			  	"categoryField": "semana",
			  	"categoryAxis": {
				    "gridPosition": "start",
				    "axisAlpha": 0,
				    "tickLength": 0,
				    "labelRotation": 90,
				    "minorGridEnabled": true,
				    /* ENSURE 2 LINES BELOW ARE ADDED */
				    "autoGridCount": false,
				    "gridCount": data.length
			  	},
			  	"export": {
				    "enabled": true,
				    "menu": [ {
				      "class": "export-main",
				      "menu": [ {
				        "label": "Descargar",
				        "menu": [ "PNG", "JPG", "PDF" ]
				      }, 
				      { "format": "PRINT",
					    "label": "Imprimir"
					  }]
				    } ]			  
				},
				"responsive": {
				    "enabled": true
				}
			});
  		}

	}

})()