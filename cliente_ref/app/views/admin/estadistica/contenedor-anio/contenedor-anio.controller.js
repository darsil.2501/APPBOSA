(function () {
	'use strict';
	angular.module('app')
	.controller('ContenedorAnioController', ContenedorAnioController);

	function ContenedorAnioController($scope, $crud,  $swal) 
	{

		var self = $scope;
		var chart;
		
		self.isLoading = true;

		self.getChart = function()
		{
			self.isLoading = true;
			$crud.get('grafico/contenedores_anio_new').then((res) => {
				self.setData(res.data.data_provider, res.data.data_graphs);
				self.isLoading = false;
			});
		};
		self.getChart();

		self.setData = function(data,data_graph){

			if(undefined === chart){

				chart = AmCharts.makeChart("chartdiv", {
					"type": "serial",
					"theme": "light",
					"legend": {
						"horizontalGap": 10,
						"maxColumns": 1,
						"position": "right",
						"useGraphSettings": true,
						"markerSize": 10
					},
					"dataProvider": data,
					"valueAxes": [{
						"stackType": "regular",
						"zeroGridAlpha": 1
					}],
					"graphs": data_graph,
					"categoryField": "year",
					"categoryAxis": {
						"gridPosition": "start"
					},
	              	"export": {
		                "enabled": true,
		                "menu": [ {
		                  	"class": "export-main",
		                  	"menu": [ {
		                    	"label": "Descargar",
		                    	"menu": [ "PNG", "JPG", "PDF" ]
		                  	}, 
		                  	{ 	
		                  		"format": "PRINT",
		                  		"label": "Imprimir"
		                	}]
	                	}]
	                    
	              	},
	              	"responsive": {
	                	"enabled": true
	              	}
				});
			}

			var graph = new AmCharts.AmGraph();
			graph.valueField = "total_year";
			graph.labelText = "[[total_year]]";
			graph.visibleInLegend = false;
			graph.showBalloon = false;
			graph.lineAlpha = 0;
			graph.fontSize = 15;
			chart.addGraph(graph);
		    chart.validateData();
		    chart.animateAgain();
  		}

	}

})()