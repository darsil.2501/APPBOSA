(function(){
	'use strict';
	angular.module('app')
	.controller('SemanaController',SemanaController)
	.controller('SemanaControllerModal',SemanaControllerModal);

	function SemanaController($scope, $crud, $swal, $permiso, $file, $sesion, $uibModal)
	{

		var self = $scope;

		self.isLoading = true;
		self.semanas = [];
		self.obj = { isSave : true };
		self.modalParams = {};

		self.list = function(id_anio)
		{
			self.isLoading = true;
			$crud.get('semana/getdata/' + id_anio).then((res) => {
				self.semanas = res.data.info;
				self.isLoading = false;
			});
		}

		self.listAnios = function(){
			self.isLoading = true;
			$crud.get('anio/give_semanas').then(function(res){
				self.anios = res.data.info;
				self.modalParams.anios = self.anios;
				self.anio_id = (self.anios[self.anios.length-1].id);
				self.list(self.anio_id);
			});
		};

		self.listAnios();

		self.listCinta = function(){
			$crud.list('cinta').then(function(res){
				self.modalParams.cintas = res.data.info;
			});
		};

		self.listCinta();

		self.load = function(semana)
		{	
			semana.isSave = false;
			self.obj = angular.copy(semana);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		
		// para exportar a excel los contenedores de una semana
	    self.export = function(semana)
	    {
	    	semana.isDownload = true;
	    	semana.file = 1;
	    	semana.nameFile = 'contenedores';
	    	$file.download('excel/contenedores/' + semana.id_semana, semana).then(function(){
	    		semana.isDownload = false;
	    	});
	    }

	    self.redirect = function(semana)
	    {
			$sesion.setVariable('semana', semana);
		}

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'semana_modal.html',
				controller: 'SemanaControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.modalParams;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(self.anio_id);
						
			}, function () {

			});
	    };

		self.openModalClose = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'semana_delete_modal.html',
				controller: 'SemanaControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.modalParams;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(self.anio_id);

			}, function () {
				
			});
	    };

	}

	function SemanaControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;
		self.obj.isSave = false;//siempre voy a actualizar
		self.request = {};

		self.anios = PARAMS.anios;
		self.cintas = PARAMS.cintas;

		self.listSemana = function(id_anio){
			$crud.get('anio/withSemana/' + id_anio).then(function(res){
				if(res.data.success){
					self.semanas = res.data.info;
				}
			});
		};

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('semana',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					self.searchSemana(self.obj.id_semana);
					self.request.id_cinta = self.obj.id_cinta;
					$crud.edit('semana',self.request).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.searchSemana = function (id_semana)
		{
			for (var i = 0; i < self.semanas.length ; i++) {
				if(self.semanas[i].id === id_semana )
				{
					self.request = self.semanas[i];
					break;
				}
			}
		}

		self.close = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.get('semana/close/' + self.obj.id_semana).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()