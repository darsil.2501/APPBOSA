(function(){
	'use strict';
	angular.module('app')
	.controller('RecobroEmpacadoraController',RecobroEmpacadoraController)
	.controller('RecobroEmpacadoraControllerModal',RecobroEmpacadoraControllerModal);

	function RecobroEmpacadoraController($scope,$crud,$swal,$permiso, $uibModal,$sesion, toastr)
	{

		var self = $scope;

		self.isLoading = true;
		self.productores = [];
		self.isRequest = false;
		self.obj = { isSave : true };

		self.semana = $sesion.getVariable('semana');

		self.list = function(isLoading)
		{
			self.isLoading = isLoading;
			$crud.get('recobro/getdata/' + self.semana.id_semana).then(function(res){
				self.empacadoras = res.data.info;
				self.isLoading = false;		
			});
		}

		self.list(true);

		self.load = function(productor)
		{	
			productor.isSave = false;
			self.obj = angular.copy(productor);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}

		self.save = function(empacadora)
		{
			if( empacadora.racimas_agregar != null){
				self.request = {
					id 				: empacadora.id_recobro,
					id_empacadora 	: empacadora.id,
					id_semana  		: self.semana.id_semana,
					racimas_agregar : empacadora.racimas_agregar
				};
				if (!self.isRequest) {
					self.isRequest = true;
					empacadora.enable = false;
					$crud.create('recobro',self.request).then(res =>{
						if (res.data.success) {
							toastr.success(res.data.message);							
						}
						else
						{
							toastr.error(res.data.message);
						}
						self.isRequest = false;
						self.list(false);
					});
				}
			}
			else
			{
				toastr.info("Inserte Número de racimas cosechadas");
			}
		}

		self.enter = function(event,empacadora)
		{
			var keyCode = event.which || event.keyCode;
		    if (keyCode === 13) {
		        self.save(empacadora);
		    }
		}

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'productor_modal.html',
				controller: 'RecobroEmpacadoraControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'productor_delete_modal.html',
				controller: 'RecobroEmpacadoraControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function RecobroEmpacadoraControllerModal($scope, $crud, $uibModalInstance, OBJ, $swal)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('productor',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('productor',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('productor',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()