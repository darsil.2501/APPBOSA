(function(){
	'use strict';
	angular.module('app')
	.controller('EnfundeProductorController',EnfundeProductorController)
	.controller('EnfundeProductorControllerModal',EnfundeProductorControllerModal);

	function EnfundeProductorController($scope,$crud,$swal,$permiso, $uibModal,$sesion, toastr)
	{

		var self = $scope;

		self.isLoading = true;
		self.productores = [];
		self.obj = { isSave : true };

		self.semana = $sesion.getVariable('semana');

		self.list = function(isLoading)
		{
			self.isLoading = isLoading;
			self.data = {
				id_empacadora : self.id_empacadora,
				id_semana	  : self.semana.id_semana
			};
			$crud.post('empacadora/getproductores' , self.data).then((res) => {
				self.productores = res.data.info;
				self.isLoading = false;
			});
		}

		self.listEmpacadora = function(){
			$crud.list('empacadora').then(function(res){
				self.empacadoras = res.data.info;
				self.id_empacadora = self.empacadoras[0].id;
				self.list(true);		
			});
		}

		self.listEmpacadora();

		self.load = function(productor)
		{	
			productor.isSave = false;
			self.obj = angular.copy(productor);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}

		self.save = function(parcela)
		{
			if (parcela.enable) 
			{
				if( parcela.enfunde != null){
					self.request = {
						id 				   : parcela.enfunde.id,
						id_parcela 		   : parcela.id,
						id_semana  		   : self.semana.id_semana,
						racimas_enfundadas : parcela.enfunde.racimas_enfundadas
					};
					parcela.enable = false;
					$crud.create('enfunde',self.request).then(res =>{
						if (res.data.success) {
							toastr.success(res.data.message);							
						}
						else
						{
							toastr.error(res.data.message);
						}
						self.list(false);
					});
				}
				else
				{
					toastr.info("Inserte Número de recimas enfundadas");
				}
			}
			else
			{
				parcela.enable = true;
			}
		}

		self.enter = function(event,parcela)
		{
			var keyCode = event.which || event.keyCode;
		    if (keyCode === 13) {
		        self.save(parcela);
		    }
		}

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'productor_modal.html',
				controller: 'EnfundeProductorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'productor_delete_modal.html',
				controller: 'EnfundeProductorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function EnfundeProductorControllerModal($scope, $crud, $uibModalInstance, OBJ, $swal)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('productor',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('productor',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('productor',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()