(function(){
	'use strict';
	angular.module('app')
	.controller('ConfiguracionOpcionController', ConfiguracionOpcionController);

	function ConfiguracionOpcionController($scope, $crud, $swal, toastr)
	{

		var self = $scope;

		self.isLoading = true;
		self.httpRequest = false;

	    self.list = function(){
	      //self.isLoading = true;
	      $crud.post('usuario/listar_menu/' + self.usuario.id_user, null).then(function(res){
	        self.opciones = res.data.info;
	        self.opciones_temp = angular.copy(self.opciones);
	      	self.subopciones_cambiadas = [];
	        self.isLoading = false;
	      });
	    };

	    self.list();

	    self.saveOpcion = function(){  
			self.httpRequest = true;    
	      	if(self.subopciones_cambiadas.length > 0){
		        var data = {'subopciones' : self.subopciones_cambiadas, 'usuario_id' : self.usuario.id_user};
		        $crud.post('usuario/asignar_subopcion',data).then(function(res){
					self.httpRequest = false;
		            if($swal.open(res)){
		              self.list();
		            }            
		        });
	      	}
	      	else
	      	{
	        	toastr.warning("No hay Cambios Realizados");
	      	}     
	    }

	    self.loadData = function(){
	      self.subopciones_cambiadas = [];
	      self.opciones.forEach( function(element, index) {
	         for (var i = 0; i < self.opciones[index].subopciones.length; i++) {
	          if(self.opciones[index].subopciones[i].enable !== self.opciones_temp[index].subopciones[i].enable){
	              self.subopciones_cambiadas.push(self.opciones[index].subopciones[i])
	           }
	         }
	      });
	    }
		
	}

})()