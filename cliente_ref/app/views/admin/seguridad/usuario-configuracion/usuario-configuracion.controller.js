(function(){
	'use strict';
	angular.module('app')
	.controller('UsuarioConfiguracionController', UsuarioConfiguracionController);

	function UsuarioConfiguracionController($scope, $sesion, URL_VIEW_USER)
	{

		var self = $scope;

		self.usuario = $sesion.getVariable('usuario');

		//validar si existe un usuario seleccionado
		if(self.usuario === null || angular.isUndefined(self.usuario)){
			$location.path('admin/usuario/usuario');
		}

		self.changeTab = function(tab)
		{
			switch(tab.id)
			{
				case 1:
	                self.currentTab = URL_VIEW_USER + 'configuracion-opcion/configuracion-opcion.html';
	                break;
            	case 2:
	                self.currentTab = URL_VIEW_USER + 'configuracion-permiso/configuracion-permiso.html';
	                break;
			}          
        };

        self.tabs = [
	        {id: 1, name: "Opciones",active: true},
	        {id: 2, name: "Permisos"}
	    ];

       self.changeTab(self.tabs[0]);
		
	}

})()