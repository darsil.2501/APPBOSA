(function(){
	'use strict';
	angular.module('app')
	.controller('ConfiguracionPermisoController', ConfiguracionPermisoController);

	function ConfiguracionPermisoController($scope, $crud, $swal, toastr)
	{

		var self = $scope;

		self.isLoading = true;
		self.httpRequest = false;

		self.list = function(){
			$crud.post('usuario/listar_permisos/' + self.usuario.id_user, null).then(function(res){
				self.permisos = res.data.info;
				self.permisos_temp = angular.copy(self.permisos);
	      		self.permisos_cambiados = [];
				self.isLoading = false;
			});
		};

		self.list();

		self.loadData = function(){
	      	self.permisos_cambiados = [];
	      	self.permisos.forEach( function(element, index) {
	      		if(self.permisos[index].enable !== self.permisos_temp[index].enable){
	              	self.permisos_cambiados.push(element);
	        	}
	      	});
	    }

	    self.saveOpcion = function(){ 
	    	self.httpRequest = true;     
	      	if(self.permisos_cambiados.length > 0){
		        var data = {'permisos' : self.permisos_cambiados, 'usuario_id' : self.usuario.id_user};
		        $crud.post('usuario/asignar_permiso',data).then(function(res){
		        	self.httpRequest = false;
		            if($swal.open(res)){
		              self.list();
		            }            
		        });
	      	}
	      	else
	      	{
	        	toastr.warning("No hay Cambios Realizados");
	      	}     
    	}
		
	}

})()