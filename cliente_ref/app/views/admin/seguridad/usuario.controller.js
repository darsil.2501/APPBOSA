(function(){
	'use strict';
	angular.module('app')
	.controller('UsuarioController',UsuarioController)
	.controller('UsuarioControllerModal',UsuarioControllerModal);

	function UsuarioController($scope,$crud,$swal,$permiso, $uibModal, $sesion)
	{

		var self = $scope;

		self.isLoading = true;
		self.usuarios = [];
		self.parametrosModal = {};
		self.obj = { isSave : true };

		self.list = function()
		{
			self.isLoading = true;
			$crud.list('usuario').then((res) => {
				self.usuarios = res.data.usuarios;
				self.isLoading = false;
			});
		}

		self.list();

		self.listCliente = function(){
			$crud.list('cliente').then(function(res){
				self.parametrosModal.clientes = res.data.info;
			});
		}

		self.listCliente();

		self.listOperador = function(){
			$crud.list('operador').then(function(res){
				self.parametrosModal.operadores = res.data.info;
			});
		}

		self.listOperador();

		self.listCargo = function(){
			$crud.list('cargo').then(function(res){
				self.parametrosModal.cargos = res.data.info;
			});
		};

		self.listCargo();

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}

		self.load = function(usuario)
		{	
			usuario.isSave = false;
			if(usuario.fecha_ingreso){
	 	   		usuario.fecha_ingreso_t = new Date(usuario.fecha_ingreso);
	 		}
			self.obj = angular.copy(usuario);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.redirectTo = function(usuario)
	    {
	    	$sesion.setVariable('usuario',usuario);
	    }
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'usuario_modal.html',
				controller: 'UsuarioControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'usuario_delete_modal.html',
				controller: 'UsuarioControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function UsuarioControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal, $generic)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.cargos = PARAMS.cargos;
		self.clientes = PARAMS.clientes;
		self.operadores = PARAMS.operadores;

		self.changeCargo = function(){
			self.viewOC = (self.searchArray());
		}

		self.searchArray = function(){
			var key;
            for (key in self.cargos) {
            	if(self.cargos[key].id === self.obj.id_cargo){
            		if(self.cargos[key].nombre.toLowerCase().indexOf("opera")===0){
            			return 1;
            		}
            		if(self.cargos[key].nombre.toLowerCase().indexOf("client")===0){
            			return 2;
            		}
            		return 0;
            	}
            }
		}

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('usuario',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					self.obj.id = self.obj.id_user;
					$crud.edit('usuario',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('usuario',self.obj.id_user).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()