(function () {
	'use strict';
	angular.module('app')
		.controller('PerfilCtrl', PerfilCtrl)
		.controller('ModalPerfilCtrl', ModalPerfilCtrl);

	function PerfilCtrl($scope, $sesion, $uibModal, $permiso) {

		var self = $scope;

		self.obj = {};
		self.flagCrud = true;

		self.list = function () {
			self.persona = $sesion.getVariable('data_persona');
			self.user = $sesion.getVariable('data_user');
		}

		self.list();

		self.load = function (data) {
			self.flagCrud = false;
			self.obj = angular.copy(data);
		};

		self.clear = function () {
			self.flagCrud = true;
			self.obj = {};
		}

		self.permisosHas = function (permiso) {
			return ($permiso.validate(permiso));
		}

		self.openModalPersona = function (event, size) {

			var options = angular.element(event.target).data('options');
			self.obj.flag = true;
			var modalInstance = $uibModal.open({
				templateUrl: 'perfil_persona.html',
				controller: 'ModalPerfilCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
					OBJ: function () {
						return self.obj;
					}
				}
			});

			modalInstance.result.then(function (obj) {
				$sesion.setVariable('data_persona', obj);
				self.list();
			}, function () {

			});
		};

		self.openModalUser = function (event, form, size) {

			var options = angular.element(event.target).data('options');
			self.obj.flag = false;
			self.obj.form = form;
			var modalInstance = $uibModal.open({
				templateUrl: 'perfil_usuario.html',
				controller: 'ModalPerfilCtrl',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
					OBJ: function () {
						return self.obj;
					}
				}
			});

			modalInstance.result.then(function (obj) {
				$sesion.setVariable('data_user', obj);
				self.list();
			}, function () {

			});
		};

	}

	function ModalPerfilCtrl($scope, $crud, $uibModalInstance, OBJ, $swal, $sesion) {
		var self = $scope;

		self.obj = {};
		self.flagpass = true;
		self.errorinput = { success: true };
		self.user = $sesion.getVariable('data_user');

		self.obj = OBJ;

		self.validate = function () {
			if (obj.nick == self.user.nick) {
				self.errorinput = { success: true, message: 'nombre de usuario disponible' };
			} else {
				self.errorinput = false;
				$crud.post('cuenta/validar_nick/' + obj.nick, null).then(res => {
					if (res.data.success) {
						self.errorinput = (res.data);
					} else {
						self.errorinput = (res.data);
					}
				});
			}
		}

		self.ok = function () {
			obj = angular.copy(self.obj);
			if (obj.flag) {
				$crud.post('cuenta/update_persona/' + obj.id, obj).then(res => {
					if ($swal.open(res)) {
						$uibModalInstance.close(obj);
					}
				});
			}
			else {
				if (obj.form) {
					$crud.post('cuenta/update_nick/' + obj.id, obj).then(res => {
						if ($swal.open(res)) {
							$uibModalInstance.close(obj);
						}
					});
				}
				else {
					self.flagpass = ($generic.validatePassword(self.obj));
					if (self.flagpass) {
						$crud.post('cuenta/update_password/' + obj.id, obj).then(res => {
							if ($swal.open(res)) {
								$uibModalInstance.close(obj);
							}
						});
					}
				}
			}
		};

		self.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}

})()