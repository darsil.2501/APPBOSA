(function () {
	'use strict';
	angular.module('app')
		.controller('ContainerController', ContainerController);

	function ContainerController($scope, $crud, $timeout) {

		var self = $scope;

		var timer = null;

		self.isLoading = true;
		self.obj = {};

		self.list = function (isLoading) {//parametro para activar el loading: true o false
			self.isLoading = isLoading;
			$crud.show('contenedor', self.obj.id_semana).then(function (res) {
				self.containers = (res.data.info);
				self.isLoading = false;
			});
		};


		self.listAnio = function () {
			$crud.get('anio/give_semanas').then(function (res) {
				self.anios = res.data.info;
				self.obj.id_anio = self.anios[self.anios.length - 1].id;
				self.listSemana(self.obj.id_anio);
			});
		};
		self.listAnio();

		self.listSemana = function (id_anio) {
			$crud.get('anio/withSemana/' + id_anio, null).then(function (res) {
				if (res.data.success) {
					self.semanas = res.data.info;
					self.obj.id_semana = self.semanas[0].id;
					self.list(true);
				}
			});
		};

		self.periodicQuery = function () {
			timer = $timeout(function () {
				self.periodicQuery();
				self.list(false);
			}, 10000);
		};

		self.periodicQuery();

		self.$on('$stateChangeStart', function(){
		    $timeout.cancel(timer);
		});

	}

})()