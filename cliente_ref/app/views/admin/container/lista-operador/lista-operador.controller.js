(function(){
	'use strict';
	angular.module('app')
	.controller('ListaOperadorController',ListaOperadorController)
	.controller('ListaOperadorControllerModal',ListaOperadorControllerModal);

	function ListaOperadorController($scope, $crud, $swal,$permiso, $uibModal, $sesion, $location, toastr, $generic)
	{

		var self = $scope;

		self.isLoading = true;
		self.contenedores = [];

		self.obj = { isSave : true };

		self.model = { };

		self.parametrosModal = {};

		self.semana = $sesion.getVariable('semana');

		self.usuario = $sesion.getVariable('data_user');

		self.list = function (isLoading) {//parametro para activar el loading: true o false
			self.isLoading = isLoading;
			$crud.post('operador/withContenedores/' + self.usuario.id_cliente_operador, self.model).then(function (res) {
				self.contenedores = (res.data.info);
				self.isLoading = false;
			});
		};


		self.listAnio = function () {
			$crud.get('anio/give_semanas').then(function (res) {
				self.anios = res.data.info;
				self.model.id_anio = self.anios[0].id;					
				self.listSemana(self.model.id_anio);							
			});
		};
		self.listAnio();

		self.listSemana = function (id_anio) {
			$crud.get('anio/withSemana/' + id_anio, null).then(function (res) {
				if (res.data.success) {
					self.semanas = res.data.info;
					if (self.semana) {
						self.model.id_semana = self.semana.id;
					}
					else
					{
						self.model.id_semana = self.semanas[0].id;	
					}					
					self.list(true);
				}
			});
		};

		self.listLineaNaviera = function(){
			$crud.list('linea_naviera').then(function(res){
				self.parametrosModal.lineas = (res.data.info);
			});
		};

		self.listLineaNaviera();

		self.listPuerto = function(){
			$crud.list('puerto_destino').then(function(res){
				self.parametrosModal.puertos = (res.data.info);
			});
		};

		self.listPuerto();

		self.redirectTo = function()
		{
			angular.forEach(self.semanas, (value, key) => {
				if(value.id === self.model.id_semana)
				{
					$sesion.setVariable('semana', value);
					return;
				}
			});
		}

		self.load = function(contenedor)
		{	
			contenedor.isSave                 = false;
			contenedor.fecha_proceso_inicio_t = new Date($generic.filterDate(contenedor.dia_proceso_inicio));
			contenedor.fecha_proceso_fin_t 	  = new Date($generic.filterDate(contenedor.dia_proceso_fin));
			contenedor.fecha_zarpe_t 		  = (contenedor.dia_zarpe != null) ? new Date($generic.filterDate(contenedor.dia_zarpe)) : null ;
			contenedor.fecha_llegada_t 		  = (contenedor.dia_llegada != null) ? new Date($generic.filterDate(contenedor.dia_llegada)) : null ;
			self.obj = angular.copy(contenedor);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true, id_semana : self.semana.id_semana };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'contenedor_modal.html',
				controller: 'ListaOperadorControllerModal',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

	}

	function ListaOperadorControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal, $generic, toastr)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.lineas = PARAMS.lineas;
		self.puertos = PARAMS.puertos;
		self.id_contenedor = OBJ.id_contenedor;
		
		self.createEdit = function()
		{
			self.obj.fecha_llegada 			= $generic.filterDate(self.obj.fecha_llegada_t);
			self.obj.fecha_zarpe 			= $generic.filterDate(self.obj.fecha_zarpe_t);
			self.obj.id 					= self.id_contenedor;
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				$crud.post('contenedor/update_operador/'+ self.obj.id, self.obj).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()