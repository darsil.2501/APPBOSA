(function(){
	'use strict';
	angular.module('app')
	.controller('ArchivoOperadorController',ArchivoOperadorController)
	.controller('ArchivoOperadorControllerModal',ArchivoOperadorControllerModal);

	function ArchivoOperadorController($scope, $crud, $swal,$permiso, $uibModal, $sesion, Upload, $location, $file, toastr, $generic)
	{

		var self = $scope;

		self.isUpload = true;
		self.isSave = false;

		self.isLoading = true;
		self.facturas = [];
		self.packings = [];
		self.certificados = [];
		self.valijas = [];
		self.isLoadingOtros = true;
		self.otros_archivos = [];

		self.files = [];

		self.contenedores = [];

		self.data = {};

		self.semana = $sesion.getVariable('semana');
		self.usuario = $sesion.getVariable('data_user');

		self.obj = { id_semana : self.semana.id };

		//objeto para mostrar lista de clientes, lista de operadores, descripcion
		self.viewElementForm = { clientes : false, operadores : false, descripcion : false };

		self.parametrosModal = {};

		//validar si existe un semana seleccionado
		if(self.semana === null || angular.isUndefined(self.semana)){
			$location.path('home/container/lista_operador');
		}

		//start listar archivos
		self.listArchivo = function()
		{
			self.isLoading = true;
			$crud.post('operador/withArchivos/' + self.usuario.id_cliente_operador, {id_semana: self.semana.id}).then((res) => {
				self.isLoading = false;
				self.facturas = res.data.facturas;
				self.packings = res.data.packings;
				self.certificados = res.data.certificados;
				self.valijas = res.data.valijas;
			});
		}

		self.listArchivo();

		self.listOtros = function()
		{
			self.isLoadingOtros = true;
			$crud.show('otros_archivos', self.semana.id).then((res) => {
				self.otros_archivos = res.data.info;
				self.isLoadingOtros = false;
			});
		}

		self.listOtros();
		
		//begin Listar clientes
		self.listCliente = function(){
			$crud.list('cliente').then(function(res){
				self.clientes = res.data.info;
			});
		};
		self.listCliente();

		

		//cargar archivos seleccionados

		self.load = function(archivo, tipo)//1->factura, 2 ->packing, 3 -> certificado,4->bl,5->valija,6->otros
		{	
			archivo.isSave = false;
			switch (tipo) {
    			case 1:
    				archivo.ruta = 'factura';
    				archivo.file = 2;
    				self.viewElementForm = { clientes : true, operadores : true, descripcion : false };
    				break;
    			case 2:
    				archivo.ruta = 'packing';
    				archivo.file = 1;
    				self.viewElementForm = { clientes : true, operadores : false, descripcion : false };
    				break;
    			case 3:
    				archivo.ruta = 'certificado';
    				archivo.file = 2;
    				self.viewElementForm = { clientes : true, operadores : false, descripcion : true };
    				break;    			
    			case 4:
    				archivo.ruta = 'vl';
    				archivo.file = 2;
    				self.viewElementForm = { clientes : true, operadores : false, descripcion : false };
    				break;
    			case 5:
    				archivo.ruta = 'valija';
    				archivo.file = 2;
    				self.viewElementForm = { clientes : true, operadores : false, descripcion : false };
    				break;
    			case 6:
    				archivo.ruta = 'otros_archivos';
    				self.viewElementForm = { clientes : false, operadores : false, descripcion : false };
    				switch(archivo.tipo)
    				{
    					case 1:
    					 	archivo.file = 3;
    						break;
    					case 2: 
	    					archivo.file = 1;
	    					break;
    					case 3:
	    					archivo.file = 2; 
	    					break;
    				};
    				break;
    		}

			self.obj = angular.copy(archivo);
		}

		//descargar archivos

		self.download = function ()
		{
			self.data = { nameFile : self.obj.nombre, file : self.obj.file }
			$file.download(self.obj.ruta + '/download/' + self.obj.id, self.data);
		}

		// eliminar item de ka tabla de files seleccionados
		self.deleteItemFiles = function(index){
	    	self.filest = [];
			for (var i in self.files) {
				if(i != index){
					self.filest.push(self.files[i]);
				}
			}
			self.files = (self.filest);
		}

		//agregar contenedor a certificado o valija

		self.addContenedor = function(event)
		{
			self.parametrosModal = {id_cliente : self.obj.id_cliente, id_semana : self.semana.id};
			self.openModalSelect(event);	
		}

		//vista previa archivos

		self.preview = function(event){
			if (self.obj.file != 2) {
				self.rutaOtros = 'https://view.officeapps.live.com/op/view.aspx?src=http://www.appbosa.com.pe/Server/public/archivos/'+ self.obj.ruta + '/' + self.obj.url;
				var win = window.open(self.rutaOtros, '_blank');
				win.focus();
			}
			if (self.obj.file === 2) {
				self.openModalPreview(event);
			}
		}

		self.clear = function ()
		{
			self.obj = {};
        	self.files = [];
        	self.data = {};
        	self.progress = 0;
		}

		self.new = function (tipo)
		{
			self.load({},tipo);
			self.isUpload = false;			
		}

		self.select = function(files)
		{
			self.files = files;
		}

		self.cancelar = function()
		{
			self.isUpload = true;
			self.clear();
		}

		self.changeCliente = function (event, id_cliente)
		{
			if(self.obj.ruta === 'valija' || self.obj.ruta === 'certificado'){
				self.parametrosModal = {id_cliente : id_cliente, id_semana : self.semana.id};
				self.openModalSelect(event);
			}
		}

		self.save = function()
		{
    		if (self.files && self.files.length) {
    			self.isSave = true;
	            Upload.upload({
	                url: '../Server/public/index.php/'+ self.obj.ruta +'/create',
	                data: {
	                	id_semana : self.semana.id,
	                	id_cliente : self.data.id_cliente,
	                	id_operador : self.data.id_operador,
	                	descripcion : self.data.descripcion,
	                    files: self.files,
	                    contenedores : self.contenedores
	                }
	            }).then(function (response) {
	                if($swal.open(response)){	                	
	                	self.listArchivo();
	                	self.clear();
	                	self.cancelar();
	                }
	                self.progress = 0;
	                self.isSave = false;
	            }, function (response) {
	                if (response.status > 0) {
	                    $scope.errorMsg = response.status + ': ' + response.data;
	                }
	            }, function (evt) {
	                self.progress = 
	                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
	            });
	        }
	    }

	    self.deleteContenedor = function (contenedor)
	    {
	    	self.data = {
				id_contenedor : contenedor.id_contenedor
			}

			switch(self.obj.ruta)
			{
				case 'certificado':
				 	self.data.id_certificado = self.obj.id;
					break;
				case 'valija': 
					self.data.id_valija = self.obj.id;
					break;
			};

			$crud.post(self.obj.ruta + '/eliminar_contenedor', self.data).then(res=>{
				if(res.data.success){
					toastr.success(res.data.message);
					self.listArchivo();
				}
				else{
					toastr.error(res.data.message);
				}
			})

	    }

		//validar permisos

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}

		self.openModalPreview = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'archivo_preview_modal.html',
				controller: 'ArchivoOperadorControllerModal',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
						
			}, function () {

			});
	    };

	    self.openModalSelect = function(event, size) 
		{
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'select_contenedor_modal.html',
				controller: 'ArchivoOperadorControllerModal',
				size: size,
				backdropClass: 'splash splash-2 splash-ef-14',
				windowClass: 'splash splash-2 splash-ef-14',
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {				
				
				if (obj) {
					self.parametrosModal = {};
					self.contenedores = obj;
				}
				else
				{
					self.listArchivo();
					self.clear();
				}
								

			}, function () {

			});
	    };


		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'archivo_delete_modal.html',
				controller: 'ArchivoOperadorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.listArchivo();

			}, function () {
				
			});
	    };

	}

	function ArchivoOperadorControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal, $generic, toastr)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.contenedores = [];
		self.contenedoresTemp = [];
		self.isLoading = false;

		self.ruta = '../Server/public/archivos/' + self.obj.ruta + '/' + self.obj.url;

		self.listContenedor = function()
		{
			self.isLoading = true;
			$crud.post('contenedor/of' + self.obj.ruta +'ClienteAndSemana', PARAMS).then(res => {
  				self.contenedores = (res.data.info);
  				self.isLoading = false;
  			});
		}

		if(PARAMS.id_cliente)
		{
			self.listContenedor();
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete(self.obj.ruta ,self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}



		self.asignContenedor = function ()
		{
			var data = {
				contenedores : self.contenedoresTemp,
				id_valija  : self.obj.id,
				id_certificado : self.obj.id
			};
			$crud.post(self.obj.ruta + '/agregar_contenedor', data).then(res=>{
				if(res.data.success){
					toastr.success(res.data.message);
					self.ok();
				}
				else{
					toastr.error(res.data.message);
				}
			});
		}

		self.saveContenedor = function ()
		{
			angular.forEach(self.contenedores,(value , key)=>{
				if(value.enable)
				{
					self.contenedoresTemp.push(value);
				}
			});
			if(self.obj.id)
			{
				if (self.contenedoresTemp.length > 0) {
					self.asignContenedor();
				}
				else
				{
					toastr.info('No Hay contenedores seleccionados');
					self.ok();
				}
			}
			else
			{
				$uibModalInstance.close(self.contenedoresTemp);
			}
			
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()