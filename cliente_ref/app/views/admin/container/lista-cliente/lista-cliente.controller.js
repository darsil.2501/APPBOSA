(function () {
	'use strict';
	angular.module('app')
		.controller('ListaClienteController', ListaClienteController);

	function ListaClienteController($scope, $crud, $timeout, $sesion) {

		var self = $scope;

		var timer = null;

		self.isLoading = true;
		self.obj = {};

		self.usuario = $sesion.getVariable('data_user');
		self.semana = $sesion.getVariable('semana');

		self.list = function (isLoading) 
		{//parametro para activar el loading: true o false
			self.isLoading = isLoading;
			$crud.post('cliente/withContenedores/' + self.usuario.id_cliente_operador, self.obj).then(function (res) {
				self.containers = (res.data.info);
				self.isLoading = false;
			});
		};


		self.listAnio = function () 
		{
			$crud.get('anio/give_semanas').then(function (res) {
				self.anios = res.data.info;
				self.obj.id_anio = self.anios[0].id;
				self.listSemana(self.obj.id_anio);
			});
		};
		self.listAnio();

		self.listSemana = function (id_anio) 
		{
			$crud.get('anio/withSemana/' + id_anio, null).then(function (res) {
				if (res.data.success) {
					self.semanas = res.data.info;
					if (self.semana) {
						self.obj.id_semana = self.semana.id;
					}
					else
					{
						self.obj.id_semana = self.semanas[0].id;	
					}
					self.list(true);
				}
			});
		};

		self.redirectTo = function()
		{
			angular.forEach(self.semanas, (value, key) => {
				if(value.id === self.obj.id_semana)
				{
					$sesion.setVariable('semana', value);
					return;
				}
			});
		}

		self.periodicQuery = function () {
			timer = $timeout(function () {
				self.periodicQuery();
				self.list(false);
			}, 10000);
		};

		self.periodicQuery();

		self.$on('$stateChangeStart', function(){
		    $timeout.cancel(timer);
		});

	}

})()