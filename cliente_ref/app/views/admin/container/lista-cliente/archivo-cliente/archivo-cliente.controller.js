(function(){
	'use strict';
	angular.module('app')
	.controller('ArchivoClienteController',ArchivoClienteController)
	.controller('ArchivoClienteControllerModal',ArchivoClienteControllerModal);

	function ArchivoClienteController($scope, $crud, $swal,$permiso, $uibModal, $sesion, Upload, $location, $file, toastr, $generic)
	{

		var self = $scope;

		self.isLoading = true;
		self.facturas = [];
		self.packings = [];
		self.certificados = [];
		self.valijas = [];
		self.isLoadingOtros = true;
		self.otros_archivos = [];

		self.semana = $sesion.getVariable('semana');
		self.usuario = $sesion.getVariable('data_user');

		self.obj = { id_semana : self.semana.id };

		//validar si existe un semana seleccionado
		if(self.semana === null || angular.isUndefined(self.semana)){
			$location.path('admin/container/lista_cliente');
		}

		//start listar archivos
		self.listArchivo = function()
		{
			self.isLoading = true;
			$crud.post('cliente/withArchivos/' + self.usuario.id_cliente_operador, {id_semana: self.semana.id}).then((res) => {
				self.isLoading = false;
				self.facturas = res.data.facturas;
				self.packings = res.data.packings;
				self.certificados = res.data.certificados;
				self.valijas = res.data.valijas;
			});
		}

		self.listArchivo();

		self.listOtros = function()
		{
			self.isLoadingOtros = true;
			$crud.show('otros_archivos', self.semana.id).then((res) => {
				self.otros_archivos = res.data.info;
				self.isLoadingOtros = false;
			});
		}

		self.listOtros();

		//cargar archivos seleccionados

		self.load = function(archivo, tipo)//1->factura, 2 ->packing, 3 -> certificado,4->bl,5->valija,6->otros
		{	
			archivo.isSave = false;
			switch (tipo) {
    			case 1:
    				archivo.ruta = 'factura';
    				archivo.file = 2;
    				self.viewElementForm = { clientes : true, operadores : true, descripcion : false };
    				break;
    			case 2:
    				archivo.ruta = 'packing';
    				archivo.file = 1;
    				self.viewElementForm = { clientes : true, operadores : false, descripcion : false };
    				break;
    			case 3:
    				archivo.ruta = 'certificado';
    				archivo.file = 2;
    				self.viewElementForm = { clientes : true, operadores : false, descripcion : true };
    				break;    			
    			case 4:
    				archivo.ruta = 'vl';
    				archivo.file = 2;
    				self.viewElementForm = { clientes : true, operadores : false, descripcion : false };
    				break;
    			case 5:
    				archivo.ruta = 'valija';
    				archivo.file = 2;
    				self.viewElementForm = { clientes : true, operadores : false, descripcion : false };
    				break;
    			case 6:
    				archivo.ruta = 'otros_archivos';
    				self.viewElementForm = { clientes : false, operadores : false, descripcion : false };
    				switch(archivo.tipo)
    				{
    					case 1:
    					 	archivo.file = 3;
    						break;
    					case 2: 
	    					archivo.file = 1;
	    					break;
    					case 3:
	    					archivo.file = 2; 
	    					break;
    				};
    				break;
    		}

			self.obj = angular.copy(archivo);
		}

		//descargar archivos

		self.download = function ()
		{
			self.data = { nameFile : self.obj.nombre, file : self.obj.file }
			$file.download(self.obj.ruta + '/download/' + self.obj.id, self.data);
		}

		//vista previa archivos

		self.preview = function(event){
			if (self.obj.file != 2) {
				self.rutaOtros = 'https://view.officeapps.live.com/op/view.aspx?src=http://www.appbosa.com.pe/Server/public/archivos/'+ self.obj.ruta + '/' + self.obj.url;
				var win = window.open(self.rutaOtros, '_blank');
				win.focus();
			}
			if (self.obj.file === 2) {
				self.openModalPreview(event);
			}
		}

		self.clear = function ()
		{
			self.obj = {};
		}

		self.openModalPreview = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'archivo_preview_modal.html',
				controller: 'ArchivoClienteControllerModal',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {
						
			}, function () {

			});
	    };

	}

	function ArchivoClienteControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal, $generic, toastr)
	{
		var self = $scope;

		self.obj = OBJ;

		self.ruta = '../Server/public/archivos/' + self.obj.ruta + '/' + self.obj.url;		

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()