(function(){
	'use strict';
	angular.module('app')
	.controller('InspectorController',InspectorController)
	.controller('InspectorControllerModal',InspectorControllerModal);

	function InspectorController($scope,$crud,$swal,$permiso, $uibModal, $sesion, toastr)
	{

		var self = $scope;

		self.isLoading = true;
		self.inspectores = [];
		self.obj = { isSave : true };

		self.list = function(isLoading)
		{
			self.isLoading = isLoading;
			$crud.list('inspector').then((res) => {
				self.inspectores = res.data.info;
				self.isLoading = false;
			});
		}

		self.list(true);

		self.load = function(inspector)
		{	
			inspector.isSave = false;
			self.obj = angular.copy(inspector);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.redirectTo = function(inspector)
	    {
	    	$sesion.setVariable('inspector',inspector);
	    }
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'inspector_modal.html',
				controller: 'InspectorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(false);
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'inspector_delete_modal.html',
				controller: 'InspectorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(false);

			}, function () {
				
			});
	    };

	}

	function InspectorControllerModal($scope, $crud, $uibModalInstance, OBJ, $swal, $generic, URL_VIEW_PRODUCTOR)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				self.obj.fecha_ingreso = $generic.filterDate(self.obj.fecha_ingreso_t);
				if (self.obj.isSave) 
				{
					$crud.create('inspector',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('inspector',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('inspector',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()