(function(){
	'use strict';
	angular.module('app')
	.controller('VisitaFileController',VisitaFileController)
	.controller('VisitaFileControllerModal',VisitaFileControllerModal);

	function VisitaFileController($scope, $crud, $swal, $permiso, URL_IMAGE_VISITA, $uibModal, Upload, $generic, $sesion)
	{

		var self = $scope;

		self.isLoading = true;
		self.isUpload = true;
		self.isSave = false;
		self.imagenes = [];

		self.ruta = URL_IMAGE_VISITA;

		self.visita = $sesion.getVariable('visita');
		self.inspector = $sesion.getVariable('inspector');

		self.obj = {};

		self.list = function()
		{
			self.isLoading = true;
			$crud.get('galeria_visita/getdata/' + self.visita.id).then((res) => {
				self.imagenes = res.data.info;
				self.isLoading = false;
			});
		}

		self.list();

		self.load = function(imagen)
		{	
			self.obj = angular.copy(imagen);
			self.obj.url = self.ruta + imagen.url;			
		}

		self.clear = function ()
		{
			self.obj = {};
			self.files = [];
        	self.data = {};
        	self.progress = 0;
			self.isSave = false;
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}

		self.new = function()
		{
			self.isUpload = false;
		}

		self.select = function(files)
		{
			self.files = files;
		}

		self.cancelar = function()
		{
			self.isUpload = true;
			self.clear();
		}

		self.save = function()
		{
    		if (self.files && self.files.length) {
    			self.isSave = true;
	            Upload.upload({
	                url: '../Server/public/index.php/galeria_visita/create',
	                data: {
	                	id_visita : self.visita.id,
	                    files 	  : self.files
	                }
	            }).then(function (response) {
	                if($swal.open(response)){	                	
	                	self.list();
	                	self.clear();
	                	self.cancelar();
	                }
	                self.progress = 0;
	                self.isSave = false;
	            }, function (response) {
	                if (response.status > 0) {
	                    $scope.errorMsg = response.status + ': ' + response.data;
	                }
	            }, function (evt) {
	                self.progress = 
	                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
	            });
	        }
	    }

		// eliminar item de ka tabla de files seleccionados
		self.deleteItemFiles = function(index){
	    	self.filest = [];
			for (var i in self.files) {
				if(i != index){
					self.filest.push(self.files[i]);
				}
			}
			self.files = (self.filest);
		}		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'imagen_modal.html',
				controller: 'VisitaFileControllerModal',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  IMGS: function () {
				    return self.imagenes;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'imagen_delete_modal.html',
				controller: 'VisitaFileControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function VisitaFileControllerModal($scope, $crud, $uibModalInstance, OBJ, IMGS, URL_IMAGE_VISITA, $swal,$generic)
	{
		var self = $scope;

		self.obj = OBJ;
		self.imagenes = IMGS;
		self.ruta = URL_IMAGE_VISITA;
		self.httpRequest = false;

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('galeria_visita',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()