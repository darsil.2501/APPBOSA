(function(){
	'use strict';
	angular.module('app')
	.controller('InspectorParcelaController',InspectorParcelaController)
	.controller('InspectorParcelaControllerModal',InspectorParcelaControllerModal);

	function InspectorParcelaController($scope, $crud, $file, $swal, $permiso, $uibModal, $sesion, $location)
	{

		var self = $scope;

		self.isLoading = true;
		self.isExport = false;
		self.productores = [];
		self.sectores = [];

		self.inspector = $sesion.getVariable('inspector');

		self.obj = {};
		self.request = {};
		self.parametrosModal = {};

		//validar si existe un inspector seleccionado
		if(self.inspector === null && angular.isUndefined(self.inspector)){
			$location.path('admin/inspector/lista');
		}

		if(self.inspector === []){
			$location.path('admin/inspector/lista');
		}

		self.list = function()
		{
			self.isLoading = true;
			self.request = { id_sector : self.id_sector, id_inspector :  self.inspector.id};
			$crud.post('inspector/getparcelas', self.request).then((res) => {
				self.productores = res.data.info;
				self.isLoading = false;
			});
		}

		//self.list();

		self.listSector = function()
		{
			self.isLoading = true;
			$crud.list('sector').then((res) => {
				self.sectores = res.data.info;
				self.id_sector = self.sectores[0].id;
				self.list();
			});
		}

		self.listSector();

		self.listInspector = function()
		{
			$crud.list('inspector').then((res) => {
				self.parametrosModal.inspectores = res.data.info;
			});
		}

		self.listInspector();

		self.redirectTo = function(parcela)
		{
			$sesion.setVariable('parcela',parcela);
		}

		self.load = function(parcela)
		{	
			parcela.isSave = false;
			parcela.id_inspector = self.inspector.id;
			self.obj = angular.copy(parcela);
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}

		self.routesHas = function()
		{
			return ($permiso.validateRoute('admin.inspector.lista'));
		}	

		self.download = function()
		{
			self.isExport = true;
			self.request = { id_sector : self.id_sector, id_inspector :  self.inspector.id};
			self.request.file = 1;//especificar que es excel con 1
			self.request.nameFile = 'parcelas_inspector';
			$file.download('excel/inspector_with_parcelas',self.request)
			.then((res) => {
				self.isExport = false;
				self.request = {};
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				self.isExport = false;
			});
		};

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'parcela_modal.html',
				controller: 'InspectorParcelaControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'parcela_delete_modal.html',
				controller: 'InspectorParcelaControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function InspectorParcelaControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal, $generic)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.inspectores = PARAMS.inspectores;

		console.log(self.obj);

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('parcela',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('parcela',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('parcela',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()