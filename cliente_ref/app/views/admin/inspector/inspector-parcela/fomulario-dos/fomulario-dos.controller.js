(function(){
	'use strict';
	angular.module('app')
	.controller('FormularioDosControllerMantenimiento',FormularioDosControllerMantenimiento)
	.controller('FormularioDosControllerMantenimientoModal',FormularioDosControllerMantenimientoModal);

	function FormularioDosControllerMantenimiento($scope, $crud, $swal, $permiso, $uibModal, $generic, $sesion, $file)
	{

		var self = $scope;

		self.isLoading = true;
		self.visitas = [];

		self.isExport = false;

		self.parametrosModal = {};

		self.parcela = $sesion.getVariable('parcela');
		self.inspector = $sesion.getVariable('inspector');

		self.obj = { isSave : true, id_parcela : self.parcela.id };

		self.list = function()
		{
			self.isLoading = true;
			$crud.get('visita_formulario_dos/getdata/' + self.parcela.id).then((res) => {
				self.visitas = res.data.info;
				self.isLoading = false;
			});
		}

		self.list();

		self.listFormulario = function()
		{
			$crud.list('item_labor').then((res) => {
				self.parametrosModal.labores = res.data.info;
			});
		}

		self.listFormulario();

		self.load = function(visita)
		{	
			visita.isSave = false;
			visita.fecha_t = new Date($generic.filterDate(visita.fecha));
			self.obj = angular.copy(visita);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true, id_parcela : self.parcela.id };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		
		self.redirectTo = function(visita)
		{
			visita.url_return = 'admin.inspector.visita_form_2';
			$sesion.setVariable('visita',visita);
		}

		self.export = function(visita)
		{
			self.obj = {};
			visita.isExport = true;
			self.obj.file = 2;//especificar que es pdf con 2
			self.obj.nameFile = 'reporte_sic';
			$file.download('visita_formulario_dos/exportPDF/' + visita.id,self.obj)
			.then((res) => {
				self.obj = {};
				visita.isExport = false;
			})
			.catch((error) => {
				$swal.error('Error en Servidor');
				visita.isExport = false;
			});
		};

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'visita_modal.html',
				controller: 'FormularioDosControllerMantenimientoModal',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'visita_delete_modal.html',
				controller: 'FormularioDosControllerMantenimientoModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function FormularioDosControllerMantenimientoModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal,$generic)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.labores = angular.copy(PARAMS.labores);

		self.createEdit = function()
		{
			self.obj.labores = self.labores;
			self.obj.fecha = $generic.filterDate(self.obj.fecha_t);
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('visita_formulario_dos',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('visita_auditoria_interna',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('visita_formulario_dos',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()