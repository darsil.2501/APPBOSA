(function () {
	'use strict';
	angular.module('app')
		.controller('AdminCtrl', AdminCtrl);

	function AdminCtrl($scope, $auth, $sesion, $crud) {
		var self = $scope;

		self.setData = function () {
			self.opciones = $sesion.getVariable('data_menu');
			self.persona = $sesion.getVariable('data_persona');
			self.permisos = $sesion.getVariable('permisos');
		}

		self.setData();

		self.logout = function () {
			$auth.logout();
		};

	}

})()