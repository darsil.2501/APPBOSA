(function () {
	'use strict';
	angular.module('app')
		.controller('ClienteCajaController', ClienteCajaController);

	function ClienteCajaController($scope, $crud, toastr) {

		var self = $scope;

		self.cajas = [];
		self.isLoading = true;

		self.list = function()
		{
			$crud.get('cliente/vercajas/' + self.obj.id).then((res) => {
				self.isLoading = false;
				self.cajas = res.data.info;
			});
	    };

	    self.list();

	    self.add = function(caja){
	    	if (!self.httpRequest) {
	    		self.httpRequest = true;
		    	self.data = {'id_cliente': self.obj.id,'id_caja':caja.id};
				if (caja.enable) {
					$crud.post('cliente/asignarcaja',self.data).then((res) => {
						self.httpRequest = false;
						if(res.data.success){
							toastr.success(res.data.message);
						}
						else{
							toastr.error(res.data.message);
							self.list();
						}
					});
				}
				else{
					$crud.post('cliente/quitarcaja',self.data).then((res) => {
						self.httpRequest = false;
						if(res.data.success){
							toastr.success(res.data.message);
						}
						else{
							toastr.error(res.data.message);
							self.list();
						}
					});
				}
			}
	    }

	}

})()