(function () {
	'use strict';
	angular.module('app')
	.controller('ClienteEmailController', ClienteEmailController);

	function ClienteEmailController($scope, $crud, toastr) {

		var self = $scope;

		self.emails = [];
		self.model = {};
		self.isLoading = true;

		self.list = function()
		{
			$crud.get('cliente/vercorreos/' + self.obj.id).then((res) => {
				self.isLoading = false;
				self.emails = res.data.info;
			});
	    };

	    self.list();

	  	self.add = function(){
	    	self.model.id_cliente = self.obj.id;
	    	$crud.post('cliente/agregarcorreos',self.model).then(res=>{
	    		if(res.data.success){
					toastr.success(res.data.message);
					self.list();
					self.model = {};
				}
				else{
					toastr.error(res.data.message);
				}
	    	});
	    }

	  	self.destroy = function(email)
	  	{

	  		$crud.post('cliente/eliminarcorreos/'+email.id,email).then(res=>{
	    		if(res.data.success){
					toastr.success(res.data.message);
					self.list();
				}
				else{
					toastr.error(res.data.message);
				}
	    	});

	  	}

	}

	// ClienteOperadorController.$inject = [
	//     '$scope'
	// ];

})()