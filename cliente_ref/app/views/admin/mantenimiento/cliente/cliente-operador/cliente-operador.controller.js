(function () {
	'use strict';
	angular.module('app')
	.controller('ClienteOperadorController', ClienteOperadorController);

	function ClienteOperadorController($scope, $crud, toastr) {

		var self = $scope;

		self.operadores = [];
		self.isLoading = true;

		self.list = function()
		{
			$crud.get('cliente/veroperadores/' + self.obj.id).then((res) => {
				self.isLoading = false;
				self.operadores = res.data.info;
			});
	    };

	    self.list();

	    self.add = function(operador){
	    	if (!self.httpRequest) {
	    		self.httpRequest = true;
		    	self.data = {'id_cliente': self.obj.id,'id_operador':operador.id};
				if (operador.enable) {
					$crud.post('cliente/asignaroperador',self.data).then((res) => {
						self.httpRequest = false;
						if(res.data.success){
							toastr.success(res.data.message);
						}
						else{
							toastr.error(res.data.message);
							self.list();
						}
					});
				}
				else{
					$crud.post('cliente/quitaroperador',self.data).then((res) => {
						self.httpRequest = false;
						if(res.data.success){
							toastr.success(res.data.message);
						}
						else{
							toastr.error(res.data.message);
							self.list();
						}
					});
				}
			}
	    }

	}

	// ClienteOperadorController.$inject = [
	//     '$scope'
	// ];

})()