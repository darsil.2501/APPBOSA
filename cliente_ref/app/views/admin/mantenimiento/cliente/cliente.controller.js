(function(){
	'use strict';
	angular.module('app')
	.controller('ClienteController',ClienteController)
	.controller('ClienteControllerModal',ClienteControllerModal);

	function ClienteController($scope,$crud,$swal,$permiso, $uibModal)
	{

		var self = $scope;

		self.isLoading = true;
		self.clientes = [];
		self.obj = { isSave : true };

		self.list = function()
		{
			self.isLoading = true;
			$crud.list('cliente').then((res) => {
				self.clientes = res.data.info;
				self.isLoading = false;
			});
		}

		self.list();

		self.load = function(cliente)
		{	

			cliente.isSave = false;
			self.obj = angular.copy(cliente);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'cliente_modal.html',
				controller: 'ClienteControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'cliente_delete_modal.html',
				controller: 'ClienteControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	    self.openModalGestion = function(event, numberGestion, size) 
		{
			self.obj.numberGestion = numberGestion;
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'cliente_gestion_modal.html',
				controller: 'ClienteControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function ClienteControllerModal($scope, $crud, $uibModalInstance, OBJ, $swal, URL_VIEW_CLIENTE)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

	 	// validar si es gestion de operadores = 2, cajas = 1 o emails = 3
	 	if(self.obj.numberGestion){
		 	switch(self.obj.numberGestion){
		 		case 1 : 
		 		{
		 			self.templateUrl = URL_VIEW_CLIENTE + 'cliente-caja/cliente-caja.html';
		 			break;
		 		};
		 		case 2 : 
		 		{
		 			self.templateUrl = URL_VIEW_CLIENTE + 'cliente-operador/cliente-operador.html';
		 			break;
		 		};
		 		case 3 : 
		 		{
		 			self.templateUrl = URL_VIEW_CLIENTE + 'cliente-email/cliente-email.html';
		 			break;
		 		};
		 	}
		}

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('cliente',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('cliente',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('cliente',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()