(function(){
	'use strict';
	angular.module('app')
	.controller('EstadoController',EstadoController)
	.controller('EstadoControllerModal',EstadoControllerModal);

	function EstadoController($scope,$crud,$swal,$permiso, $uibModal)
	{

		var self = $scope;

		self.isLoading = true;
		self.estados = [];
		self.obj = { isSave : true };

		self.list = function()
		{
			self.isLoading = true;
			$crud.list('estado').then((res) => {
				self.estados = res.data.info;
				self.isLoading = false;
			});
		}

		self.list();

		self.load = function(estado)
		{	
			estado.isSave = false;
			self.obj = angular.copy(estado);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'estado_modal.html',
				controller: 'EstadoControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'estado_delete_modal.html',
				controller: 'EstadoControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function EstadoControllerModal($scope, $crud, $uibModalInstance, OBJ, $swal)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('estado',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('estado',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('estado',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()