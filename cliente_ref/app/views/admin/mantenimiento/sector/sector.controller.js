(function(){
	'use strict';
	angular.module('app')
	.controller('SectorController',SectorController)
	.controller('SectorControllerModal',SectorControllerModal);

	function SectorController($scope,$crud,$swal,$permiso, $uibModal)
	{

		var self = $scope;

		self.isLoading = true;
		self.sectores = [];
		self.obj = { isSave : true };

		self.list = function()
		{
			self.isLoading = true;
			$crud.list('sector').then((res) => {
				self.sectores = res.data.info;
				self.isLoading = false;
			});
		}

		self.list();

		self.load = function(sector)
		{	
			sector.isSave = false;
			self.obj = angular.copy(sector);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}

		self.openModalDelegado = function(event, size) {

	    	self.obj.isDelegado = true;

			var options = angular.element(event.target).data('options');

			var modalInstance = $uibModal.open({
				backdrop: 'static',
	            keyboard: false,
	            templateUrl: 'delegado_modal.html',
				controller: 'SectorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  	OBJ: function () {
				    	return self.obj;
				  	}
				}
			});

			modalInstance.result.then(function (obj) {
							
			}, function () {
				// self.list();
		  //       self.clear();
			});
	    };		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'sector_modal.html',
				controller: 'SectorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'sector_delete_modal.html',
				controller: 'SectorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function SectorControllerModal($scope, $crud, $uibModalInstance, OBJ, $swal, $generic, toastr)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.isAdd = false;
	  	self.isSave = false;

		self.listProductores = function(){
	  		$crud.get('sector/obtener_productores/' + self.obj.id).then(function(res){
				self.productores = res.data.info;
			});
	  	}

	  	self.listDelegados = function(){
	  		self.isLoading = true;
	  		$crud.get('sector/obtener_delegados/' + self.obj.id).then(function(res){
				self.delegados = res.data.info;
				self.isLoading = false;
			});
	  	}

	  	if (self.obj.isDelegado) {
	  		self.isLoading = true;
	  		self.listProductores();
	  		self.listDelegados();
	  	}

	  	self.new = function(){
	    	self.isAdd = true;
	    	self.model = {};
	    }

	    self.clear = function(){
	    	self.isAdd = false;
	    	self.model = {};
	    }

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('sector',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('sector',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.asignarDelegado = function(){
	    	self.isSave = true;
	    	self.model.fecha_inicio_cargo	= $generic.filterDate(self.model.fecha_inicio_t);
	    	self.model.id_sector = self.obj.id;
	    	$crud.post('sector/create_delegado',self.model).then(function(res){
	    		self.isSave = false;
         		if(res.data.success){
					toastr.success(res.data.message);
					self.listDelegados();
					self.clear();
				}
				else{
					toastr.error(res.data.message);
				}
       		});
	    }

	    self.quitarDelegado = function(delegado){
	    	$crud.get('sector/delete_delegado', delegado.id).then(function(res){
         		if(res.data.success){
					toastr.success(res.data.message);
					self.listDelegados();
					self.clear();
				}
				else{
					toastr.error(res.data.message);
				}
       		});
	    }

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('sector',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()