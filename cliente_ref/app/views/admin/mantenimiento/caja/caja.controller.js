(function(){
	'use strict';
	angular.module('app')
	.controller('CajaController',CajaController)
	.controller('CajaControllerModal',CajaControllerModal);

	function CajaController($scope,$crud,$swal,$permiso, $uibModal)
	{

		var self = $scope;

		self.isLoading = true;
		self.cajas = [];
		self.obj = { isSave : true };

		self.list = function()
		{
			self.isLoading = true;
			$crud.list('caja').then((res) => {
				self.cajas = res.data.info;
				self.isLoading = false;
			});
		}

		self.list();

		self.load = function(caja)
		{	
			caja.isSave = false;
			self.obj = angular.copy(caja);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'caja_modal.html',
				controller: 'CajaControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'caja_delete_modal.html',
				controller: 'CajaControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function CajaControllerModal($scope, $crud, $uibModalInstance, OBJ, $swal)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('caja',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('caja',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('caja',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()