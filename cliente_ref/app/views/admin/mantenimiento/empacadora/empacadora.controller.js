(function(){
	'use strict';
	angular.module('app')
	.controller('EmpacadoraController',EmpacadoraController)
	.controller('EmpacadoraControllerModal',EmpacadoraControllerModal);

	function EmpacadoraController($scope,$crud,$swal,$permiso, $uibModal)
	{

		var self = $scope;

		self.isLoading = true;
		self.empacadoras = [];
		self.obj = { isSave : true };

		self.list = function()
		{
			self.isLoading = true;
			$crud.list('empacadora').then((res) => {
				self.empacadoras = res.data.info;
				self.isLoading = false;
			});
		}

		self.list();

		self.load = function(empacadora)
		{	
			empacadora.isSave = false;
			self.obj = angular.copy(empacadora);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'empacadora_modal.html',
				controller: 'EmpacadoraControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'empacadora_delete_modal.html',
				controller: 'EmpacadoraControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function EmpacadoraControllerModal($scope, $crud, $uibModalInstance, OBJ, $swal)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('empacadora',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('empacadora',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('empacadora',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()