(function(){
	'use strict';
	angular.module('app')
	.controller('FormularioController', FormularioController);

	function FormularioController($scope, $sesion, URL_VIEW_FORM)
	{

		var self = $scope;

		self.changeTab = function(tab)
		{
			switch(tab.id)
			{
				case 1:
	                self.currentTab = URL_VIEW_FORM + 'formulario-interno/formulario-interno.html';
	                break;
            	case 2:
	                self.currentTab = URL_VIEW_FORM + 'formulario-dos/formulario-dos.html';
	                break;
			}          
        };

        self.tabs = [
	        {id: 1, name: "Formulario uno",active: true},
	        {id: 2, name: "Formulario dos"}
	    ];

       self.changeTab(self.tabs[0]);
		
	}

})()