(function(){
	'use strict';
	angular.module('app')
	.controller('FormularioDosController',FormularioDosController)
	.controller('FormularioDosControllerModal',FormularioDosControllerModal);

	function FormularioDosController($scope,$crud,$swal,$permiso, $uibModal)
	{

		var self = $scope;

		self.isLoading = true;
		self.labores = [];
		self.obj = { isSave : true };

		self.list = function()
		{
			self.isLoading = true;
			$crud.list('item_labor').then((res) => {
				self.labores = res.data.info;
				self.isLoading = false;
			});
		}

		self.list();

		self.new = function(param,event)
		{
			if(param === 1)
			{
				self.obj.route = 'labor';
			}
			else
			{
				self.obj.route = 'item_labor';
				self.obj.id_labor = self.obj.id;
				self.obj.isSave = true;
				self.obj.nombre = "";
				self.obj.items = [];
			}
			self.openModalSave(event);
		}

		self.load = function(labor,param)
		{	
			if(param === 1)
			{
				labor.route = 'labor';
			}
			else
			{
				labor.route = 'item_labor';
			}
			labor.isSave = false;
			self.obj = angular.copy(labor);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'labor_modal.html',
				controller: 'FormularioDosControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'labor_delete_modal.html',
				controller: 'FormularioDosControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function FormularioDosControllerModal($scope, $crud, $uibModalInstance, OBJ, $swal)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create(self.obj.route,self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit(self.obj.route,self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete(self.obj.route,self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()