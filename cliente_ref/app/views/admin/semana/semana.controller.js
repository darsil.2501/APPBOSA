(function(){
	'use strict';
	angular.module('app')
	.controller('SemanaController',SemanaController)
	.controller('SemanaControllerModal',SemanaControllerModal);

	function SemanaController($scope, $crud, $swal, $permiso, $file, $sesion, $uibModal)
	{

		var self = $scope;

		self.isLoading = true;
		self.semanas = [];
		self.obj = { isSave : true };
		self.modalParams = {};

		self.list = function(id_anio)
		{
			self.isLoading = true;
			$crud.get('semana/getdata/' + id_anio).then((res) => {
				self.semanas = res.data.info;
				self.isLoading = false;
			});
		}

		self.listAnios = function(){
			self.isLoading = true;
			$crud.get('anio/give_semanas').then(function(res){
				self.anios = res.data.info;
				self.anio_id = (self.anios[self.anios.length-1].id);
				self.list(self.anio_id);
			});
		};

		self.listAnios();

		self.listCinta = function(){
			$crud.list('cinta').then(function(res){
				self.modalParams.cintas = res.data.info;
			});
		};

		self.listCinta();

		self.load = function(semana)
		{	
			semana.isSave = false;
			self.obj = angular.copy(semana);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		
		// para exportar a excel los contenedores de una semana
	    self.export = function(semana)
	    {
	    	semana.isDownload = true;
	    	semana.file = 1;
	    	semana.nameFile = 'contenedores';
	    	$file.download('excel/contenedores/' + semana.id_semana, semana).then(function(){
	    		semana.isDownload = false;
	    	});
	    }

	    self.redirect = function(semana)
	    {
			$sesion.setVariable('semana', semana);
		}

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'semana_modal.html',
				controller: 'SemanaControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.modalParams;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(self.anio_id);
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'semana_delete_modal.html',
				controller: 'SemanaControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.modalParams;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(self.anio_id);

			}, function () {
				
			});
	    };

	}

	function SemanaControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;
		self.cintas = PARAMS.cintas;

		self.semanasAnio = [];
		for (var i = 1; i < 54; i++) {
			self.semanasAnio.push({'id':i,'nombre':i});
		}

		self.listAnio = function(){
			$crud.list('anio').then(function(res){
				self.anios = (res.data.info);
			});
		};

		self.listAnio();

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('semana',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('semana',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('semana',self.obj.id_semana).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()