(function(){
	'use strict';
	angular.module('app')
	.controller('SemanaContenedorController',SemanaContenedorController)
	.controller('SemanaContenedorControllerModal',SemanaContenedorControllerModal);

	function SemanaContenedorController($scope, $crud, $swal,$permiso, $uibModal, $sesion, $location, toastr, $generic)
	{

		var self = $scope;

		self.isLoading = true;
		self.contenedores = [];

		self.semana = $sesion.getVariable('semana');

		self.obj = { isSave : true, id_semana : self.semana.id_semana };

		self.parametrosModal = {};

		//validar si existe un productor seleccionado
		if(self.semana === null || angular.isUndefined(self.semana)){
			$location.path('admin/semana/lista');
		}

		self.list = function()
		{
			self.isLoading = true;
			$crud.show('contenedor', self.semana.id_semana).then((res) => {
				self.contenedores = res.data.info;
				self.isLoading = false;
			});
		}

		self.list();

		self.listEstado = function()
		{
			$crud.list('estado').then(function(res){
				self.estados = (res.data.info);
			});
		};

		self.listEstado();

		self.listCliente = function(){
			$crud.list('cliente').then(function(res){
				self.parametrosModal.clientes = (res.data.info);
			});
		};

		self.listCliente();

		self.listLineaNaviera = function(){
			$crud.list('linea_naviera').then(function(res){
				self.parametrosModal.lineas = (res.data.info);
			});
		};

		self.listLineaNaviera();

		self.listPuerto = function(){
			$crud.list('puerto_destino').then(function(res){
				self.parametrosModal.puertos = (res.data.info);
			});
		};

		self.listPuerto();

		self.load = function(contenedor)
		{	
			contenedor.isSave                 = false;
			contenedor.fecha_proceso_inicio_t = new Date($generic.filterDate(contenedor.dia_proceso_inicio));
			contenedor.fecha_proceso_fin_t 	  = new Date($generic.filterDate(contenedor.dia_proceso_fin));
			contenedor.fecha_zarpe_t 		  = (contenedor.dia_zarpe != null) ? new Date($generic.filterDate(contenedor.dia_zarpe)) : null ;
			contenedor.fecha_llegada_t 		  = (contenedor.dia_llegada != null) ? new Date($generic.filterDate(contenedor.dia_llegada)) : null ;
			self.obj = angular.copy(contenedor);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true, id_semana : self.semana.id_semana };
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		
		self.changeEstado = function(contenedor){
	    	var data = {
					'id_contenedor' : contenedor.id_contenedor,
					'id_estado' : contenedor.id_estado
				}
	    	$crud.post('contenedor/cambiar_estado',data).then(function(res){
	    		if (res.data.success) {
	    			toastr.info(res.data.message);
	    		}else{
	    			toastr.error(res.data.message);
	    		}
				
				self.list();
			});
	    };

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'contenedor_modal.html',
				controller: 'SemanaContenedorControllerModal',
				size: 'lg',
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'contenedor_delete_modal.html',
				controller: 'SemanaContenedorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function SemanaContenedorControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal, $generic, toastr)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.clientes = PARAMS.clientes;
		self.lineas = PARAMS.lineas;
		self.puertos = PARAMS.puertos;

		self.cajasAdd = [];
		self.id_contenedor = OBJ.id_contenedor;

		if (OBJ.cajas) {
			self.cajasAdd = angular.copy(OBJ.cajas);
		}

		self.selectCliente = function(id){
			$crud.post('cliente/operadoresAsigned/'+id,{}).then(function(res){
				self.operadores = (res.data.info);
			});
			$crud.post('cliente/cajasAsigned/'+id,{}).then(function(res){
				self.cajas = (res.data.info);
			});
		};

		if(!self.obj.isSave)
		{
			self.selectCliente(self.obj.id_cliente);
		}

		self.searchCaja = function(id){
			for (var i = 0; i < self.cajas.length; i++) {
		        if (self.cajas[i].id === id) {
		            return self.cajas[i];
		        }
		    }
		    return null;
		};

		self.changeCajas = function(){
			self.caja = (self.searchCaja(self.objCajas.id_caja));
		}

		self.addCajas = function(){
			self.caja.cantidad_cajas = self.objCajas.total;
			if(self.cajasAdd.length <= 3){
				self.flag = false;
				for (var i = 0; i < self.cajasAdd.length; i++) {
					if (self.cajasAdd[i].id === self.caja.id) {
						self.flag = true;
						break;
					}
				}
				if (self.flag) {
					toastr.error('Tipo de caja ya esta agregada');
				}else{
					self.cajasAdd.push(angular.copy(self.caja));
				}
			}
			self.getTotalCajas();
			self.objCajas = {};
		}

		self.deleteArray = function(param){
			self.cajasTemp = [];
			for (var i = 0; i < self.cajasAdd.length; i++) {
	    		if (i != param) {
	    			self.cajasTemp.push(self.cajasAdd[i]);
	    		}
	    		else if (self.cajasAdd[param].save === true)
	    		{
	    			var data = {
					'id_contenedor' : self.id_contenedor,
					'id_caja' : self.cajasAdd[param].id
					}
					$crud.post('contenedor/eliminar_caja',data).then(function(res){
						toastr.success(res.data.message);
						self.getTotalCajas();
					});
	    		}    	
	    	}
	    	self.getTotalCajas();
	    	self.cajasAdd = angular.copy(self.cajasTemp);
	    }

	    self.totalCajas = 0;

	    self.getTotalCajas = function(){
	    	self.totalCajas = 0;
	    	for (var i = 0; i < self.cajasAdd.length; i++) {
	    		self.totalCajas += self.cajasAdd[i].cantidad_cajas;	    	
	    	}
	    }

	    self.getTotalCajas();


		self.createEdit = function()
		{
			self.obj.cajas 					= self.cajasAdd;
			self.obj.fecha_llegada 			= $generic.filterDate(self.obj.fecha_llegada_t);
			self.obj.fecha_zarpe 			= $generic.filterDate(self.obj.fecha_zarpe_t);
			self.obj.fecha_proceso_inicio 	= $generic.filterDate(self.obj.fecha_proceso_inicio_t);
			self.obj.fecha_proceso_fin 		= $generic.filterDate(self.obj.fecha_proceso_fin_t);
			self.obj.id 					= self.id_contenedor;
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('contenedor',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('contenedor',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('contenedor',self.obj.id_contenedor).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()