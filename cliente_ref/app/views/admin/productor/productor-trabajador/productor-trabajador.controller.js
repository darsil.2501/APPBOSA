(function(){
	'use strict';
	angular.module('app')
	.controller('ProductorTrabajadorController',ProductorTrabajadorController)
	.controller('ProductorTrabajadorControllerModal',ProductorTrabajadorControllerModal);

	function ProductorTrabajadorController($scope,$crud,$swal,$permiso, $uibModal, $sesion, $location)
	{

		var self = $scope;

		self.isLoading = true;
		self.trabajadores = [];

		self.productor = $sesion.getVariable('productor');

		self.obj = { isSave : true, id_productor : self.productor.id};

		self.parametrosModal = {};

		//validar si existe un productor seleccionado
		if(self.productor === null || angular.isUndefined(self.productor)){
			$location.path('admin/productor/productor');
		}

		self.list = function(isLoading)
		{
			self.isLoading = true;
			$crud.get('productor/obtener_trabajadores/' + self.productor.id).then((res) => {
				self.trabajadores = res.data.info;
				if (isLoading) {
					self.isLoading = false;
				}
			});
		}

		self.list(false);

		self.listProductor = function()
		{
			self.isLoading = true;
			$crud.list('productor').then((res) => {
				self.parametrosModal.productores = res.data.info;
				self.isLoading = false;
			});
		}

		self.listProductor();

		self.listParentesco = function()
		{
			self.isLoading = true;
			$crud.list('parentesco').then((res) => {
				self.parametrosModal.parentescos = res.data.info;
			});
		}

		self.listParentesco();

		self.load = function(trabajador)
		{	
			trabajador.isSave = false;
			trabajador.id_productor = self.productor.id;
			self.obj = angular.copy(trabajador);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true, id_productor : self.productor.id};
		}
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'trabajador_modal.html',
				controller: 'ProductorTrabajadorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(true);
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'trabajador_delete_modal.html',
				controller: 'ProductorTrabajadorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(true);

			}, function () {
				
			});
	    };

	}

	function ProductorTrabajadorControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal, $generic)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.parentescos = PARAMS.parentescos;
		self.productores = PARAMS.productores;

		self.listTrabajador = function()
		{
			$crud.list('trabajador').then((res) => {
				self.trabajadores = res.data.info;
			});
		}

		self.listTrabajador();

		self.operaciones = [
			{'id':1,'nombre':'NUEVO TRABAJADOR'},
			{'id':2,'nombre':'TRABAJADOR REGISTRADO'},
			{'id':3,'nombre':'PRODUCTOR REGISTRADO'},
			{'id':4,'nombre':'MISMO PRODUCTOR'}
		];

		self.condiciones = [
			{'id':0,'nombre':'EVENTUAL'},
			{'id':1,'nombre':'PERMANENTE'}
		];

		if(self.obj.isSave){
			self.obj.operacion = 1;
			self.obj.id_parentesco = 1;
		}

		self.selectPersona = function (array , id)
		{
			for (var i = 0; i < array.length; i++) {
				if (array[i].id === id) {
					self.obj.nombre = array[i].nombre;
					self.obj.apellido_paterno = array[i].apellido_paterno;
					self.obj.apellido_materno = array[i].apellido_materno;
					self.obj.dni = array[i].dni;
					self.obj.telefono = array[i].telefono;
					self.obj.direccion = array[i].direccion;
                    break;
				}
			}
		}

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('trabajador',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('trabajador',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('trabajador',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.clear = function ()
		{
			self.obj.nombre = '';
			self.obj.apellido_paterno = '';
			self.obj.apellido_materno = '';
			self.obj.dni = '';
			self.obj.telefono = '';
			self.obj.direccion = '';
			self.obj.id_parentesco = 1;
			self.obj.id_trabajador = 0;

			if (self.obj.operacion === 4) {
				self.selectPersona(self.productores, self.obj.id_productor);
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()