(function () {
	'use strict';
	angular.module('app')
		.controller('ParcelaModalController', ParcelaModalController);

	function ParcelaModalController($scope, $crud, toastr, $sesion) {

		var self = $scope;

		self.parcelas = [];
		self.isLoading = true;

		self.productor = $sesion.getVariable('productor');

		self.list = function(isLoading)
		{
			self.isLoading = isLoading;
			$crud.post('productor/obtener_parcelas/' + self.productor.id,null).then((res) => {
				self.parcelas = res.data.info;
				self.isLoading = false;
			});
	    };

	    self.list(true);

	    self.change = function(parcela){
	    	if (!self.httpRequest) {
	    		self.httpRequest = true;
		    	self.data = { id_parcela : parcela.id, id_condicion : self.productor.id_condicion, 
		    				  id_productor : self.productor.id };
				if (parcela.enable) {
					$crud.post('parcela/enabled',self.data).then((res) => {
						self.httpRequest = false;
						if(res.data.success){
							toastr.success(res.data.message);
							self.list(false);
						}
						else{
							toastr.error(res.data.message);
							self.list(false);
						}
					});
				}
				else{
					$crud.post('parcela/disabled',self.data).then((res) => {
						self.httpRequest = false;
						if(res.data.success){
							toastr.success(res.data.message);
							self.list(false);
						}
						else{
							toastr.error(res.data.message);
							self.list(false);
						}
					})
					.catch((error) => {
						toastr.error('Sucedio un inconveniente, intente nuevamente.');
					});
				}
			}
	    }

	}

})()