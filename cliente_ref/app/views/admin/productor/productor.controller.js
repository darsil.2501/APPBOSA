(function(){
	'use strict';
	angular.module('app')
	.controller('ProductorController',ProductorController)
	.controller('ProductorControllerModal',ProductorControllerModal);

	function ProductorController($scope,$crud,$swal,$permiso, $uibModal, $sesion, toastr)
	{

		var self = $scope;

		self.isLoading = true;
		self.productores = [];
		self.obj = { isSave : true };

		self.list = function(isLoading)
		{
			self.isLoading = isLoading;
			$crud.list('productor').then((res) => {
				self.productores = res.data.info;
				self.isLoading = false;
			});
		}

		self.list(true);

		self.listCondicion = function()
		{
			$crud.list('condicion_productor').then((res) => {
				self.condiciones = res.data.info;
			});
		}

		self.listCondicion();

		self.load = function(productor)
		{	
			productor.isSave = false;
			if(productor.fecha_ingreso){
	 	   		productor.fecha_ingreso_t = new Date(productor.fecha_ingreso);
	 		}
			self.obj = angular.copy(productor);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true };
		}

		self.selectEstado = function(productor, event)
		{	
			//sancinar parcial
			if (productor.id_condicion === 3) {
				self.redirectTo(productor);
				self.openModalParcela();
			}

			//sancionar total
			else {
				$crud.post('productor/cambiar_condicion', productor).then((res) => {
					self.httpRequest = false;
					if(res.data.success){
						toastr.success(res.data.message);
						self.list(false);
					}
					else{
						toastr.error(res.data.message);
						self.list(false);
					}
				})
				.catch((error) => {
					toastr.error('Sucedio un inconveniente, intente nuevamente.');
				});
			}


		}

		self.redirectTo = function(productor)
	    {
	    	$sesion.setVariable('productor',productor);
	    }
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'productor_modal.html',
				controller: 'ProductorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(false);
						
			}, function () {

			});
	    };

	    self.openModalParcela = function(event, size) 
		{

			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'parcela_modal.html',
				controller: 'ProductorControllerModal',
				size: size,
				backdropClass: 'splash splash-2 splash-ef-14',
				windowClass: 'splash splash-2 splash-ef-14',
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(false);
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'productor_delete_modal.html',
				controller: 'ProductorControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list(false);

			}, function () {
				
			});
	    };

	}

	function ProductorControllerModal($scope, $crud, $uibModalInstance, OBJ, $swal, $generic, URL_VIEW_PRODUCTOR)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.sociosEstado = [
			{'id':1,'nombre':'SI'},
			{'id':0,'nombre':'NO'}
		];

		self.templateUrl = URL_VIEW_PRODUCTOR + 'parcela-modal/parcela-modal.html';

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				self.obj.fecha_ingreso = $generic.filterDate(self.obj.fecha_ingreso_t);
				if (self.obj.isSave) 
				{
					$crud.create('productor',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('productor',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('productor',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()