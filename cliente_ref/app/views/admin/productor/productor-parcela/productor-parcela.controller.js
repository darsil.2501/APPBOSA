(function(){
	'use strict';
	angular.module('app')
	.controller('ProductorParcelaController',ProductorParcelaController)
	.controller('ProductorParcelaControllerModal',ProductorParcelaControllerModal);

	function ProductorParcelaController($scope,$crud,$swal,$permiso, $uibModal, $sesion, $location)
	{

		var self = $scope;

		self.isLoading = true;
		self.parcelas = [];

		self.productor = $sesion.getVariable('productor');

		self.obj = { isSave : true, id_productor : self.productor.id};

		self.parametrosModal = {};

		//validar si existe un productor seleccionado
		if(self.productor === null || angular.isUndefined(self.productor)){
			$location.path('admin/productor/productor');
		}

		self.list = function()
		{
			self.isLoading = true;
			$crud.post('productor/obtener_parcelas/' + self.productor.id,null).then((res) => {
				self.parcelas = res.data.info;
				self.isLoading = false;
			});
		}

		self.list();

		self.listSector = function(){
			$crud.list('sector').then(function(res){
				self.parametrosModal.sectores = res.data.info;
			});
		};
		self.listSector();

		self.listEmpacadora = function(){
			$crud.list('empacadora').then(function(res){
				self.parametrosModal.empacadoras = res.data.info;
			});
		};
		self.listEmpacadora();

		self.listInspector = function(){
			$crud.list('inspector').then(function(res){
				self.parametrosModal.inspectores = res.data.info;
			});
		};
		self.listInspector();

		self.load = function(parcela)
		{	
			parcela.isSave = false;
			parcela.id_productor = self.productor.id;
			self.obj = angular.copy(parcela);
		}

		self.clear = function ()
		{
			self.obj = { isSave : true, id_productor : self.productor.id};
		}

		self.permisosHas = function(permiso)
		{
			return ($permiso.validate(permiso));
		}
		

		self.openModalSave = function(event, size) 
		{

			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'parcela_modal.html',
				controller: 'ProductorParcelaControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();
						
			}, function () {

			});
	    };

		self.openModalDelete = function(event, size) 
		{
			var options = angular.element(event.target).data('options');
			var modalInstance = $uibModal.open({
				backdrop: 'static',
                keyboard: false,
                templateUrl: 'parcela_delete_modal.html',
				controller: 'ProductorParcelaControllerModal',
				size: size,
				backdropClass: 'splash' + ' ' + options,
				windowClass: 'splash' + ' ' + options,
				resolve: {
				  OBJ: function () {
				    return self.obj;
				  },
				  PARAMS: function () {
				    return self.parametrosModal;
				  }
				}
			});

			modalInstance.result.then(function (obj) {

				self.list();

			}, function () {
				
			});
	    };

	}

	function ProductorParcelaControllerModal($scope, $crud, $uibModalInstance, OBJ, PARAMS, $swal, $generic)
	{
		var self = $scope;

		self.obj = OBJ;
		self.httpRequest = false;

		self.sectores = PARAMS.sectores;
		self.empacadoras = PARAMS.empacadoras;
		self.inspectores = PARAMS.inspectores;

		self.tiposPropiedad = [
			{'id':1,'nombre':'PROPIA'},
			{'id':0,'nombre':'ARRENDADA'}
		];

		self.estados = [
			{'id':1,'nombre':'SI'},
			{'id':0,'nombre':'NO'}
		];

		if(self.obj.isSave){
			self.obj.tipo_propiedad = 1;
		}

		self.createEdit = function()
		{
			if(!self.httpRequest)
			{
				self.httpRequest = true;
				if (self.obj.isSave) 
				{
					$crud.create('parcela',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
				else{
					$crud.edit('parcela',self.obj).then(function(res){
						self.httpRequest = false;
						if($swal.open(res)){
							self.ok();
						}
					});
				}
			}
		}

		self.delete = function()
		{
			if(!self.httpRequest){
				self.httpRequest = true;
				$crud.delete('parcela',self.obj.id).then(function(res){
					self.httpRequest = false;
					if($swal.open(res)){
						self.ok();
					}
				});
			}
		}

		self.ok = function () 
		{
	      	$uibModalInstance.close();
	    };

	    self.cancel = function () 
	    {
	     	$uibModalInstance.dismiss('cancel');
	    };
	}

})()