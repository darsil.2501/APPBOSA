(function () {
	'use strict';
	angular.module('app')
		.controller('LoginCtrl', LoginCtrl);

	function LoginCtrl($scope, $auth, toastr) {

		var self = $scope;

		self.anio = new Date().getFullYear();
		self.flag = false;

		self.logueo = function () {
			self.flag = true;
			if (self.user) {
				$auth.login(self.user).then(function (res) {
					if (!res.data.success) {
						self.flag = false;
						toastr.error(res.data.message);
					}
				});
			}
		};

	};

})()