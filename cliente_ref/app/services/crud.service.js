(function () {
	'use strict';
	angular.module('app')
		.factory('$crud', $crud)
		.service('sCrud', scrud);

	function $crud(sCrud) {
		return {

			list: function (route) {
				return sCrud.list(route);
			},

			edit: function (route, data) {
				return sCrud.edit(route, data);
			},
			create: function (route, data) {
				return sCrud.create(route, data);
			},
			delete: function (route, id) {
				return sCrud.delete(route, id);
			},
			show: function (route, id) {
				return sCrud.show(route, id);
			},
			get: function (route) {
				return sCrud.get(route);
			},
			post: function (route, data) {
				return sCrud.post(route, data);
			}

		}
	}

	function scrud($http, URL_HTTP) {
		this.list = function (route) {
			return $http.get(URL_HTTP + route + '/getdata');
		};

		this.edit = function (route, data) {
			return $http.post(URL_HTTP + route + '/update/' + data.id, JSON.stringify(data));
		};

		this.create = function (route, data) {
			return $http.post(URL_HTTP + route + '/create', JSON.stringify(data));
		};

		this.delete = function (route, id) {
			return $http.delete(URL_HTTP + route + '/delete/' + id);
		};

		this.show = function (route, id) {
			return $http.post(URL_HTTP + route + '/show/' + id);
		};

		this.get = function (route) {
			return $http.get(URL_HTTP + route);
		};

		this.post = function (route, data) {
			return $http.post(URL_HTTP + route, JSON.stringify(data));
		};
	}

})()