(function () {
    'use strict';
    angular.module('app')
        .factory('$auth', $auth)
        .service('sAuth', sAuth);

    function $auth(sAuth) {
        return {

            login: function (usuario) {
                return sAuth.login(usuario);
            },
            logout: function () {
                return sAuth.logout();
            }

        }
    }

    function sAuth($q, $http, $sesion, $location, URL_HTTP, sPermiso) {
        var defer;

        this.login = function (usuario) {
            defer = $q.defer();
            $http.post(URL_HTTP + 'usuario/auth', JSON.stringify(usuario))
                .then(
                function (res) {
                    if (res.status >= 200 && res.status <= 299) {
                        if (res.data.success) {
                            $sesion.setVariable('data_menu', res.data.data_menu);
                            $sesion.setVariable('data_persona', res.data.data_persona);
                            $sesion.setVariable('data_user', res.data.data_usuario);
                            $sesion.setVariable('permisos', res.data.permisos);
                            $sesion.setVariable('inspector', res.data.inspector);
                            $sesion.setVariable('data_subopciones', res.data.data_subopciones);
                            sPermiso.arrayPermisos = $sesion.getVariable('permisos');
                            sPermiso.arraySubopciones = $sesion.getVariable('data_subopciones');
                            $location.url('admin/dashboard');
                        }
                    } else if (res.status >= 400) {
                        //console.log('Error en la petición.');
                    }
                    defer.resolve(res);
                }
                );
            return defer.promise;
        };

        this.logout = function () {

            $sesion.destroySession();

            $http.get(URL_HTTP + 'usuario/logout').then(
                function (res) {
                    if (res.status >= 200 && res.status <= 299) {
                        $location.url('auth/login');
                    } else if (res.status >= 400) {
                        //console.log('Error en la petición.');
                    }
                }
            );
        }
    }

})()