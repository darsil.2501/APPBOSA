(function () {
  'use strict';
  angular.module('app')
    .factory('$swal', $swal)
    .service('sSwal', sSwal);

  function $swal(sSwal) {
    return {

      open: (res) => {
        return sSwal.open(res);
      },
      confirm: (res) => {
        return sSwal.confirm(res);
      },
      success : (message) => {
        return sSwal.success(message);
      },
      error : (message) => {
        return sSwal.error(message);
      }

    }
  }

  function sSwal($crud) {
    var self = this;

    self.message = 'This message is Empty!';

    this.open = (res) => {
      if (res.data.success) {
        if (!angular.isUndefined(res.data.message)) {
          swal("Éxito!", res.data.message, "success");
        } else {
          swal("Éxito!", 'Todo correcto!', "success");
        }
        return true;
      } else {
        if (angular.isUndefined(res.data.messages)) {
          swal("Advertencia!", res.data.message, "warning");
        } else {
          var cadena = '';
          angular.forEach(res.data.messages, function (p, k) {
            cadena += p + ' \n ';
          });
          swal("Advertencia!", cadena, "warning");
        }

        return false;
      }
    };

    this.confirm = (res) => {
      swal({
        title: "",
        text: "¿Desea Enviar a Correo?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: false
      },
        res);
    }

    this.success = (message) => {
      swal("Mensaje", message , "success");
    }

    this.error = (message) => {
      swal("Mensaje", message , "warning");
    }

  }

})()