(function () {
	'use strict';
	angular.module('app')
		.factory('$permiso', $permiso)
		.service('sPermiso', sPermiso);

	function $permiso(sPermiso) {
		return {

			validate: function (param) {
				return sPermiso.validate(param);
			},
			validateRoute: function (route) {
				return sPermiso.validateRoute(route);
			}
		}
	}

	function sPermiso($filter, $sesion) {

		this.arrayPermisos = $sesion.getVariable('permisos');

		this.arraySubopciones = $sesion.getVariable('data_subopciones');

		this.validate = function (param) {
			return (this.arrayPermisos.includes(param));
		};

		this.validateRoute = function (route) {
			return (this.arraySubopciones.includes(route));
		};
	}

})()