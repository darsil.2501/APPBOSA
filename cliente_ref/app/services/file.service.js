(function(){
	'use strict';
	angular.module('app')
	.factory('$file',$file)
	.service('sFile',sFile);

	function $file(sFile){
		return{

			upload : function(route,data){
                return sFile.uploadFile(route,data);
            },
			download : function(route, data){
				return sFile.downloadFile(route, data);
			},
            destroy : function(route,data){
                return sFile.destroyFile(route,data);
            }

		}
	}

	function sFile($http,$q){

		this.uploadFile = function(route,data){
            var deferred = $q.defer();
            var formdata = new FormData();
            angular.forEach(data.flow.files, function (value, key) {
                formdata.append(key, value.file);
            });

            var request = {
                method: 'POST',
                url: '../Server/public/index.php/'+route+'/create/'+data.id,
                data: formdata,
                headers: {
                    'Content-Type': undefined
                }
            };
            
            return $http(request)
            .success(function (res) {
                deferred.resolve(res);
            })
            .error(function (msg, code) {
                deferred.reject(msg);
            });
            return deferred.promise;
        }

		this.downloadFile = function(route, params){

            if(params.file === 1){
                return $http({
                    url: '../Server/public/index.php/'+route,
                    method: 'POST',
                    params: params,
                    headers: {
                        'Content-type': 'application/xls',
                    },
                    responseType: 'arraybuffer'
                }).success(function (data, status, headers, config) {
                    var file = new Blob([data], {
                        type: 'application/xls'
                    });
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = params.nameFile + '.xls';
                    document.body.appendChild(a);
                    a.click();
                }).error(function (data, status, headers, config) {

                });
            }
            if(params.file === 2)
            {
                return $http({
                    url : '../Server/public/index.php/'+route,
                    method : 'POST',
                    params : params,
                    headers : {
                        'Content-type' : 'application/pdf',
                    },
                    responseType : 'arraybuffer'
                }).success(function(data, status, headers, config) {
                    var file = new Blob([ data ], {
                        type : 'application/pdf'
                    });
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    var a         = document.createElement('a');
                    a.href        = fileURL; 
                    a.target      = '_blank';
                    a.download    = params.nameFile + '.pdf';
                    document.body.appendChild(a);
                    a.click();
                }).error(function(data, status, headers, config) {

                });
            }
            if (params.file === 3)
            {
                return $http({
                    url : '../Server/public/index.php/'+route,
                    method : 'POST',
                    params : params,
                    headers : {
                        'Content-type' : 'application/docx',
                    },
                    responseType : 'arraybuffer'
                }).success(function(data, status, headers, config) {
                    var file = new Blob([ data ], {
                        type : 'application/docx'
                    });
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    var a         = document.createElement('a');
                    a.href        = fileURL; 
                    a.target      = '_blank';
                    a.download    = params.nameFile + '.docx';
                    document.body.appendChild(a);
                    a.click();
                }).error(function(data, status, headers, config) {

                });
            }
		};


        this.destroyFile = function(route,id){
            return $http.delete('../Server/public/index.php/'+route+'/delete/'+id);
        }
	}

})()