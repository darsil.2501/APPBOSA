(function () {
    'use strict';
    angular.module('app')
        .factory('$sesion', $sesion)
        .service('sSession', sSession);

    function $sesion(sSession) {
        return {

            getVariable: function (name) {
                return sSession.getVariable(name);
            },
            setVariable: function (name, param) {
                return sSession.setVariable(name, param);
            },
            destroyVariable: function (name) {
                return sSession.destroyVariable(name);
            },
            getSession: function () {
                return sSession.getSessionStorage();
            },
            destroySession: function () {
                return sSession.destroySessionStorage();
            },
            isLogged : function () {
                return sSession.isLogged();
            }

        }
    }

    function sSession() {

        this.getVariable = function (name) {
            return angular.fromJson(sessionStorage.getItem(name));
        };

        this.setVariable = function (name, param) {
            return sessionStorage.setItem(name, JSON.stringify(param));
        };

        this.destroyVariable = function (name) {
            return sessionStorage.removeItem(name);
        };

        this.getSessionStorage = function () {
            return sessionStorage;
        };

        this.destroySessionStorage = function () {
            return sessionStorage.clear();
        };

        this.isLogged = function(){
            return (sessionStorage.length > 0) ? true : false;
        }
    }

})()