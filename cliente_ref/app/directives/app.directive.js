(function () {
    'use strict';
    angular.module('app')

        .directive('loading', loading)
        .directive('titleModal', titleModal)
        .directive('buttonSpin', buttonSpin)
        .directive('buttonSave', buttonSave)
        .directive('formDelete', formDelete)
        .directive('buttonCancel', buttonCancel);

		function loading () 
		{
            return {
                restrict: 'E',
                template: '<div class="loading-container"><div class="loading"></div><div id="loading-text">Cargando</div></div>'
            };
        }

        function titleModal () 
        {
            return {
                restrict: 'E',
		        link: function(scope, element, attrs) {
		            scope.name = attrs.name;
		            scope.crud = scope.obj.isSave;
		        },
                template: '<h3 class="modal-title font-ligth"><i class="icon-login" ng-if="crud"></i> <span ng-if="crud">Nuevo</span> <i class="icon-pencil" ng-if="!crud"></i> <span ng-if="!crud">Editar</span> {{name}}</h3>'
            };
        }

        function buttonSpin () 
        {
            return {
                restrict: 'E',
		        link: function(scope, element, attrs) {
		            scope.crud = (scope.obj) ? scope.obj.isSave : true;
		        },
                template: `<button class="btn btn-border btn-rounded-10" ng-class="{'btn-primary' : crud,'btn-info' : !crud}"><i class="fa fa-spinner fa-spin"></i> <span ng-if="crud">Guardando...</span><span ng-if="!crud">Actualizando...</span></button>`
            };
        }

        function buttonSave () 
        {
            return {
                restrict: 'E',
		        link: function(scope, element, attrs) {
		            scope.crud = scope.obj.isSave;
		        },
                template: `<button type="submit" class="btn btn-border btn-rounded-10" ng-class="{'btn-primary' : crud,'btn-info' : !crud}"><i class="fa fa-save" ng-if="crud"></i> <span ng-if="crud">Guardar</span> <i class="fa fa-refresh" ng-if="!crud"></i> <span ng-if="!crud">Actualizar</span></button>`
            };
        }

        function formDelete () 
        {
            return {
                restrict: 'E',
		        link: function(scope, element, attrs) {
		            
		        },
                template: `
                <div class="text-center">
		          <i style="font-size: 40px;" class="icon-info"></i>
		          <h4>¿Desea Eliminar Registro?</h4>
		        </div>                
		        <div class="text-center">
		          <button ng-if="!httpRequest" type="submit" class="btn btn-rounded-10 btn-danger"><i class="fa fa-check"></i> Aceptar</button>
		          <button ng-if="httpRequest" class="btn btn-border btn-rounded-10 btn-danger"><i class="fa fa-spinner fa-spin"></i> Eliminando...</button>
		          <button-cancel></button-cancel>
		        </div>                
                `
            };
        }

        function buttonCancel () 
        {
            return {
                restrict: 'E',
		        link: function(scope, element, attrs) {
		            scope.crud = scope.obj.isSave;
		        },
                template: '<button type="button" class="btn btn-default btn-border btn-rounded-10" ng-click="cancel()"><i class="fa fa-close"></i> Cancelar</button>'
            };
        }

})()