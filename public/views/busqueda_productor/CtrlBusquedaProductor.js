(function(){
	'use strict';
	angular.module('app')
	.controller('BusquedaProductorCtrl', BusquedaProductorCtrl);

	function BusquedaProductorCtrl($scope,$crud,vcRecaptchaService,$file){
		var vm = $scope;

		vm.obj = {
			nombres : null,
			dni : null,
			recaptcha : ""
		}

		var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
		var f=new Date();
		vm.anio = (diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());

		vm.validar = function(){
			if(vm.obj.recaptcha !== null && vm.obj.recaptcha !== ''){
				$crud.post('productor/validar_captcha',vm.obj).then(res=>{
					if(res.data.success){
						vm.search();					
					}
					else{
						vm.clear(); 
					}					
				});
			}	
		}
		
		vm.search = function(){
			if((vm.obj.nombres !== null && vm.obj.nombres !== '') || (vm.obj.dni !== null && vm.obj.dni !== '')){
				$crud.post('productor/buscar',vm.obj).then(res=>{
					vm.socios = (res.data.info);
					// vm.clear(); lo he comentado por que si borra el campo no se envía la cadena a buscar			
				});
			}			
		}

		vm.clear = function(){
			vm.obj = {
				nombres : null,
				dni : null,
				recaptcha : ""
			};
			vcRecaptchaService.reload(0);
		}

		vm.downloadPDF = function (){
			vm.obj.file = 2;
			vm.obj.nameFile = 'pdf_consulta_publica';
			$file.download('download/pdf_consulta_publica',vm.obj);
		};

	};

})()