(function () {

    'use strict';

    angular.module('ng-Reporte',[])
            .factory('$reporte', $reporte)
            .service('sReporte', sReporte);

            function $reporte(sReporte){
                return{

                    excel : function(route){
                        return sReporte.excel(route);
                    },
                    excelId : function(route,id){
                        return sReporte.excelId(route,id);
                    },
                    excelData : function(route,data){
                        return sReporte.excelData(route,data);
                    },
                    excelDetalleContenedor : function(route,id){
                        return sReporte.excelDetalleContenedor(route,id);
                    },
                    downloadPDF : function(route,valor){
                        return sReporte.downloadPDF(route,valor);
                    },
                }
            }

            function sReporte($http) {

                this.excel = function (route) {
                    return $http({
                        url: '../Server/public/index.php/excel/'+route,
                        method: 'GET',
                        headers: {
                            'Content-type': 'application/xls',
                        },
                        responseType: 'arraybuffer'
                    }).success(function (data, status, headers, config) {
                        var file = new Blob([data], {
                            type: 'application/xls'
                        });
                        //trick to download store a file having its URL
                        var fileURL = URL.createObjectURL(file);
                        var a = document.createElement('a');
                        a.href = fileURL;
                        a.target = '_blank';
                        a.download = 'reporte_'+route+'.xls';
                        document.body.appendChild(a);
                        a.click();
                    }).error(function (data, status, headers, config) {

                    });
                };

                this.excelId = function(route,id){
                    return $http({
                        url: '../Server/public/index.php/excel/'+route+'/'+id,
                        method: 'POST',
                        params: id,
                        headers: {
                            'Content-type': 'application/xls',
                        },
                        responseType: 'arraybuffer'
                    }).success(function (data, status, headers, config) {
                        var file = new Blob([data], {
                            type: 'application/xls'
                        });
                        //trick to download store a file having its URL
                        var fileURL = URL.createObjectURL(file);
                        var a = document.createElement('a');
                        a.href = fileURL;
                        a.target = '_blank';
                        a.download = 'reporte_'+route+'.xls';
                        document.body.appendChild(a);
                        a.click();
                    }).error(function (data, status, headers, config) {

                    });
                };

                this.excelData = function(route,data){
                    return $http({
                        url: '../Server/public/index.php/excel/'+route,
                        method: 'POST',
                        params: data,
                        headers: {
                            'Content-type': 'application/xls',
                        },
                        responseType: 'arraybuffer'
                    }).success(function (data, status, headers, config) {
                        var file = new Blob([data], {
                            type: 'application/xls'
                        });
                        //trick to download store a file having its URL
                        var fileURL = URL.createObjectURL(file);
                        var a = document.createElement('a');
                        a.href = fileURL;
                        a.target = '_blank';
                        a.download = 'reporte_'+route+'.xls';
                        document.body.appendChild(a);
                        a.click();
                    }).error(function (data, status, headers, config) {

                    });
                };

                this.excelDetalleContenedor = function(route,id){
                    return $http({
                        url: '../Server/public/index.php/excel/'+route+'/'+id,
                        method: 'POST',
                        params: id,
                        headers: {
                            'Content-type': 'application/xls',
                        },
                        responseType: 'arraybuffer'
                    }).success(function (data, status, headers, config) {
                        var file = new Blob([data], {
                            type: 'application/xls'
                        });
                        //trick to download store a file having its URL
                        var fileURL = URL.createObjectURL(file);
                        var a = document.createElement('a');
                        a.href = fileURL;
                        a.target = '_blank';
                        a.download = 'detalle_contenedor.xls';
                        document.body.appendChild(a);
                        a.click();
                    }).error(function (data, status, headers, config) {

                    });
                };

                this.downloadPDF = function(route,valor){
                    return $http({
                        url: '../Server/public/index.php/download/' + route,
                        method : 'POST',
                        params : valor,
                        headers : {
                            'Content-type' : 'application/pdf',
                        },
                        responseType : 'arraybuffer'
                    }).success(function(data, status, headers, config) {
                        var file = new Blob([ data ], {
                            type : 'application/pdf'
                        });
                        //trick to download store a file having its URL
                        var fileURL = URL.createObjectURL(file);
                        var a         = document.createElement('a');
                        a.href        = fileURL; 
                        a.target      = '_blank';
                        a.download    = 'consulta.pdf';
                        document.body.appendChild(a);
                        a.click();
                    }).error(function(data, status, headers, config) {

                    });
                };
            }
})();