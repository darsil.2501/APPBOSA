(function(){
	'use strict';
	angular.module('ng-Crud',[])
	.factory('$crud',$crud)
	.service('sCrud',scrud);

	function $crud(sCrud){
		return{

			get : function(route){
				return sCrud.get(route);
			},
			post : function(route,data){
				return sCrud.post(route,data);
			},
			delete : function(route){
				return sCrud.delete(route);
			}
		}
	}

	function scrud($http, url){

		this.get = function(route){
			return $http.get(url + route);
		};

		this.post = function(route,data){
			return $http.post(url + route, JSON.stringify(data));
		};

		this.delete = function(route){
			return $http.delete(url + route);
		};
	}

})()