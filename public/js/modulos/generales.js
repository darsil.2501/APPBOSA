(function () {

    'use strict';

    angular.module('app')
            .factory('$generic', $generic)
            .service('sGeneric', sGeneric);

            function $generic(sGeneric){
                return{

                    filterDate : function(date){
                        return sGeneric.filterDate(date);
                    },
                    validatePassword : function(obj){
                        return sGeneric.compareString(obj);
                    }

                }
            }

            function sGeneric($filter,sSession,$q,$http) {

                this.filterDate = function (date) {
                    return $filter('date')(date, 'yyyy/MM/dd');
                };

                this.compareString = function(obj){
                    return (obj.password_nuevo === obj.password_repeat);
                };

            }
})();