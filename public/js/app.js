(function(){
	'use strict';
	angular.module('app',['ui.router','oc.lazyLoad','angularUtils.directives.dirPagination','ng-Crud'
				   ,'toastr','ng-SweetAlert','angular-loading-bar','ngAnimate','ngLoadingSpinner',
				   'vcRecaptcha','ng-File'])
	
	.config(mainConfig)
	.run(mainRun);
	//.constant('url', '../Server/public/index.php/');

	 
	function mainConfig($stateProvider, $urlRouterProvider) {

		$stateProvider

		//app core pages (errors, login,signup)
		.state('public', {
		  abstract: true,
		  url: '/public',
		  template: '<div ui-view></div>'
		})
		//login
		.state('public.busqueda_productor', {
		  url: '/busqueda_productor',
		  controller: 'BusquedaProductorCtrl',
		  templateUrl: 'views/busqueda_productor/busqueda_productor.html',
		  resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
		    loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
		      // you can lazy load files for an existing module
		             return $ocLazyLoad.load('views/busqueda_productor/CtrlBusquedaProductor.js');
		    }]
		  }
		})

	
	    $urlRouterProvider.otherwise('/public/busqueda_productor');
		
	};

	function mainRun($transitions){

		
	 //  	$transitions.onStart({ to: 'home.**' }, function(trans) {
		//     var $session = trans.injector().get('$session');
		//     if ($session.getSesion().length === 0) {
		//       // User isn't authenticated. Redirect to a new Target State
		//       return trans.router.stateService.target('api.login');
		//     }
		// });

    }

})()