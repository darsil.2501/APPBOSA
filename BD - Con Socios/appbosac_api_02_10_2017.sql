-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-10-2017 a las 22:36:49
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `appbosac_api`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anio`
--

CREATE TABLE `anio` (
  `id` int(11) NOT NULL,
  `anio` year(4) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `anio`
--

INSERT INTO `anio` (`id`, `anio`, `activo`) VALUES
(1, 2017, 1),
(2, 2018, 1),
(3, 2019, 1),
(4, 2020, 1),
(5, 2021, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_caja`
--

CREATE TABLE `audit_caja` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_caja`
--

INSERT INTO `audit_caja` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(1, 'APPBOSA SAMAN SAMAN', 'SAMAN ORGANICO FT', 'ACTUALIZAR', '2017-07-27', '08:11:47', '190.42.206.197', '190.42.206.197'),
(2, 'APPBOSA SAMAN SAMAN', 'SAMAN ORGANICO SPP', 'ACTUALIZAR', '2017-07-27', '08:12:54', '190.42.206.197', '190.42.206.197'),
(3, 'Juan Calderón More', 'KOREA 13 KG', 'ACTUALIZAR', '2017-08-10', '10:17:55', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(4, 'APPBOSA SAMAN SAMAN', 'ALBERT HEIJN BANDED', 'ACTUALIZAR', '2017-08-26', '08:25:53', '190.42.208.254', '190.42.208.254'),
(5, 'APPBOSA SAMAN SAMAN', 'ALL SELET ALTOMERCATO (ITALIA)', 'ACTUALIZAR', '2017-08-26', '08:29:13', '190.42.208.254', '190.42.208.254'),
(6, 'APPBOSA SAMAN SAMAN', 'EDEKA BIO BIO FT', 'ACTUALIZAR', '2017-08-26', '08:29:57', '190.42.208.254', '190.42.208.254'),
(7, 'APPBOSA SAMAN SAMAN', 'CB CLUSTERBAGS BABG CARRES', 'ACTUALIZAR', '2017-08-26', '08:30:58', '190.42.208.254', '190.42.208.254'),
(8, 'APPBOSA SAMAN SAMAN', 'EDEKA BIO FT', 'ACTUALIZAR', '2017-08-26', '08:31:26', '190.42.208.254', '190.42.208.254'),
(9, 'APPBOSA SAMAN SAMAN', 'EDEKA BIO ORGANIC', 'ACTUALIZAR', '2017-08-26', '08:31:46', '190.42.208.254', '190.42.208.254'),
(10, 'APPBOSA SAMAN SAMAN', 'EKOOKE FT', 'ACTUALIZAR', '2017-08-26', '08:32:17', '190.42.208.254', '190.42.208.254'),
(11, 'APPBOSA SAMAN SAMAN', 'EQUICOSTA', 'ACTUALIZAR', '2017-08-26', '08:33:10', '190.42.208.254', '190.42.208.254'),
(12, 'APPBOSA SAMAN SAMAN', 'EKOOKE ORGANIC (VERDE)', 'ACTUALIZAR', '2017-08-26', '08:33:42', '190.42.208.254', '190.42.208.254'),
(13, 'APPBOSA SAMAN SAMAN', 'SAMAN ORGANIC FT', 'ACTUALIZAR', '2017-08-26', '08:34:18', '190.42.208.254', '190.42.208.254'),
(14, 'APPBOSA SAMAN SAMAN', 'SAMAN ORGANIC SPP', 'ACTUALIZAR', '2017-08-26', '08:34:45', '190.42.208.254', '190.42.208.254'),
(15, 'APPBOSA SAMAN SAMAN', 'FT KOREA CLOUSTER', 'ACTUALIZAR', '2017-08-26', '08:38:44', '190.42.208.254', '190.42.208.254'),
(16, 'APPBOSA SAMAN SAMAN', 'FT COREA CLOUSTER', 'ACTUALIZAR', '2017-08-26', '08:41:08', '190.42.208.254', '190.42.208.254'),
(17, 'APPBOSA SAMAN SAMAN', 'JAPON 13 KG', 'ACTUALIZAR', '2017-08-26', '08:43:42', '190.42.208.254', '190.42.208.254'),
(18, 'APPBOSA SAMAN SAMAN', 'CHIQUITA ORGANIC', 'ACTUALIZAR', '2017-08-26', '08:44:24', '190.42.208.254', '190.42.208.254'),
(19, 'APPBOSA SAMAN SAMAN', 'CHIQUITA ORGANIC 2LB', 'ACTUALIZAR', '2017-08-26', '08:50:58', '190.42.208.254', '190.42.208.254'),
(20, 'APPBOSA SAMAN SAMAN', 'CHIQUITA ORGANIC', 'ACTUALIZAR', '2017-08-26', '08:51:38', '190.42.208.254', '190.42.208.254'),
(21, 'APPBOSA SAMAN SAMAN', 'CHIQUITA ORGANIC 2LB', 'ACTUALIZAR', '2017-08-26', '08:51:58', '190.42.208.254', '190.42.208.254'),
(22, 'APPBOSA SAMAN SAMAN', 'CHIQUITA EUROPA FT', 'ACTUALIZAR', '2017-08-26', '08:52:43', '190.42.208.254', '190.42.208.254'),
(23, 'APPBOSA SAMAN SAMAN', 'YC ALBERTH HEIJING BANDED', 'ACTUALIZAR', '2017-08-26', '08:54:29', '190.42.208.254', '190.42.208.254'),
(24, 'APPBOSA SAMAN SAMAN', 'MIGROS FT', 'INSERTAR', '2017-08-26', '08:55:45', '190.42.208.254', '190.42.208.254'),
(25, 'APPBOSA SAMAN SAMAN', 'FT COREA CLOUSTER 13KG', 'ACTUALIZAR', '2017-08-26', '10:02:12', '190.42.208.254', '190.42.208.254'),
(26, 'APPBOSA SAMAN SAMAN', 'JAPON 13KG (DOLE)', 'ACTUALIZAR', '2017-08-26', '10:02:56', '190.42.208.254', '190.42.208.254'),
(27, 'APPBOSA SAMAN SAMAN', 'EQUIFRUIT', 'ACTUALIZAR', '2017-08-26', '10:03:55', '190.42.208.254', '190.42.208.254'),
(28, 'APPBOSA SAMAN SAMAN', 'ALBERTH HEIJIN BANDED', 'ACTUALIZAR', '2017-08-26', '10:24:34', '190.42.208.254', '190.42.208.254'),
(29, 'APPBOSA SAMAN SAMAN', 'ALBERTH HEIJING BANDED', 'ACTUALIZAR', '2017-08-26', '10:24:55', '190.42.208.254', '190.42.208.254'),
(30, 'APPBOSA SAMAN SAMAN', 'JAPON 13KG', 'ACTUALIZAR', '2017-08-26', '10:29:37', '190.42.208.254', '190.42.208.254'),
(31, 'APPBOSA SAMAN SAMAN', 'JAPON 13KG - (Dole)', 'ACTUALIZAR', '2017-09-11', '11:18:39', '190.237.236.83', '190.237.236.83');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_cargo`
--

CREATE TABLE `audit_cargo` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_cargo`
--

INSERT INTO `audit_cargo` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(1, 'APPBOSA SAMAN SAMAN', 'Jefe Trazabilidad', 'INSERTAR', '2017-05-23', '15:37:19', '179.7.139.187', '179.7.139.187'),
(2, 'APPBOSA SAMAN SAMAN', 'Auxiliar Trazabilidad', 'INSERTAR', '2017-05-23', '15:38:05', '179.7.139.187', '179.7.139.187'),
(3, 'Juan Calderón More', 'Jefe Trazabilidad', 'ACTUALIZAR', '2017-05-30', '08:16:10', '179.7.132.187', '179.7.132.187'),
(4, 'APPBOSA SAMAN SAMAN', 'Operador Logístico', 'INSERTAR', '2017-07-08', '12:16:51', '190.235.130.55', '190.235.130.55'),
(5, 'APPBOSA SAMAN SAMAN', 'Ing. Agronoma', 'INSERTAR', '2017-07-25', '11:48:59', '181.176.84.75', '181.176.84.75'),
(6, 'APPBOSA SAMAN SAMAN', 'Ing. Agrónoma', 'ACTUALIZAR', '2017-07-25', '11:49:20', '181.176.84.75', '181.176.84.75'),
(7, 'APPBOSA SAMAN SAMAN', 'Otros', 'INSERTAR', '2017-09-02', '08:08:35', '181.64.26.88', '181.64.26.88');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_certificado`
--

CREATE TABLE `audit_certificado` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_certificado`
--

INSERT INTO `audit_certificado` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(3, 'APPBOSA SAMAN SAMAN', '1_26_2017_(1)1_24_2017_(1)1_24_2017_(2)AGROFAIR2.pdf', 'INSERTAR', '2017-07-24', '20:18:04', '190.117.166.176', '190.117.166.176'),
(4, 'APPBOSA SAMAN SAMAN', '1_26_2017_(1)1_24_2017_(1)1_24_2017_(2)AGROFAIR2.pdf', 'ELIMINAR', '2017-07-24', '20:18:43', '190.42.97.91', '190.42.97.91'),
(5, 'APPBOSA SAMAN SAMAN', '1_30_2017_(1)PRIMA ABRIL 2017.pdf', 'INSERTAR', '2017-08-07', '11:37:57', '181.64.26.246', '181.64.26.246'),
(6, 'APPBOSA SAMAN SAMAN', '1_1_2017_(2)MVC.pdf', 'INSERTAR', '2017-08-22', '22:58:20', '181.176.78.33', '181.176.78.33'),
(7, 'APPBOSA SAMAN SAMAN', '1_1_2017_(2)MVC.pdf', 'ELIMINAR', '2017-08-22', '23:03:19', '181.176.78.33', '181.176.78.33'),
(8, 'APPBOSA SAMAN SAMAN', '1_1_2017_(2)MVC - copia.pdf', 'INSERTAR', '2017-08-22', '23:14:41', '181.176.78.33', '181.176.78.33'),
(9, 'APPBOSA SAMAN SAMAN', '1_1_2017_(2)MVC - copia.pdf-20387', 'AGREGAR CONTENEDOR', '2017-08-22', '23:14:59', '181.176.78.33', '181.176.78.33'),
(10, 'APPBOSA SAMAN SAMAN', '1_1_2017_(2)MVC - copia.pdf-20387', 'ELIMINAR CONTENEDOR', '2017-08-22', '23:15:03', '181.176.78.33', '181.176.78.33'),
(11, 'APPBOSA SAMAN SAMAN', '1_1_2017_(2)MVC - copia.pdf', 'ELIMINAR', '2017-08-22', '23:17:35', '181.176.78.33', '181.176.78.33'),
(12, 'Marcia Ninoska Herrera Reto', '3_34_2017_(1)DOLE-34BEL.PDF', 'INSERTAR', '2017-09-08', '07:51:58', '181.67.154.141', '181.67.154.141'),
(13, 'Marcia Ninoska Herrera Reto', '2_34_2017_(1)BD-34.pdf', 'INSERTAR', '2017-09-08', '07:59:28', '181.67.154.141', '181.67.154.141'),
(14, 'Marcia Ninoska Herrera Reto', '1_34_2017_(2)AGF-34RTT.pdf', 'INSERTAR', '2017-09-08', '08:01:50', '181.67.154.141', '181.67.154.141'),
(15, 'Marcia Ninoska Herrera Reto', '1_34_2017_(3)AGF-34HAM.pdf', 'INSERTAR', '2017-09-08', '08:03:17', '181.67.154.141', '181.67.154.141'),
(16, 'Marcia Ninoska Herrera Reto', '2_35_2017_(2)W-35BD.pdf', 'INSERTAR', '2017-09-08', '13:50:19', '181.67.154.141', '181.67.154.141'),
(17, 'Marcia Ninoska Herrera Reto', '1_35_2017_(4)W-35AGFRTT.pdf', 'INSERTAR', '2017-09-08', '13:55:24', '181.67.154.141', '181.67.154.141'),
(18, 'Marcia Ninoska Herrera Reto', '3_35_2017_(2)W-35DOLE.PDF', 'INSERTAR', '2017-09-09', '07:48:34', '181.67.154.141', '181.67.154.141'),
(19, 'Marcia Ninoska Herrera Reto', '4_35_2017_(1)W-35TA.PDF', 'INSERTAR', '2017-09-09', '07:49:45', '181.67.154.141', '181.67.154.141');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_ciudad`
--

CREATE TABLE `audit_ciudad` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_cliente`
--

CREATE TABLE `audit_cliente` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(255) NOT NULL,
  `registro` varchar(255) NOT NULL,
  `operacion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_contenedor`
--

CREATE TABLE `audit_contenedor` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(255) NOT NULL,
  `semana` varchar(255) NOT NULL,
  `numero_contenedor` varchar(255) NOT NULL,
  `referencia` varchar(255) NOT NULL,
  `operacion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time DEFAULT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_contenedor`
--

INSERT INTO `audit_contenedor` (`id`, `usuario_responsable`, `semana`, `numero_contenedor`, `referencia`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(28, 'Juan Calderón More', '22', 'PONU 481624-0', '22513', 'INSERTAR', '2017-05-29', '10:15:54', '179.7.132.187', '179.7.132.187'),
(29, 'Juan Calderón More', '22', 'PONU 481624-0', '22513', 'ACTUALIZAR', '2017-05-29', '10:17:27', '179.7.132.187', '179.7.132.187'),
(30, 'Juan Calderón More', '22', 'PONU 481624-0', '22513', 'ACTUALIZAR', '2017-05-29', '12:18:16', '179.7.132.187', '179.7.132.187'),
(31, 'Juan Calderón More', '22', 'PONU 481624-0', '22513', 'ACTUALIZAR', '2017-05-29', '12:20:17', '179.7.132.187', '179.7.132.187'),
(32, 'Juan Calderón More', '22', 'PONU 481624-0', '22513', 'ELIMINAR', '2017-05-29', '12:20:17', '179.7.132.187', '179.7.132.187'),
(33, 'Juan Calderón More', '21', 'PONU 497834-4', '22595', 'INSERTAR', '2017-05-29', '12:33:50', '179.7.132.187', '179.7.132.187'),
(34, 'Juan Calderón More', '21', 'MNBU 315895-0', '22595', 'INSERTAR', '2017-05-29', '12:42:51', '179.7.132.187', '179.7.132.187'),
(35, 'Juan Calderón More', '21', 'MNBU 315895-0', '22595', 'ACTUALIZAR', '2017-05-29', '12:43:07', '179.7.132.187', '179.7.132.187'),
(36, 'Juan Calderón More', '21', 'PONU 497834-4', '22595', 'ACTUALIZAR', '2017-05-29', '12:43:37', '179.7.132.187', '179.7.132.187'),
(37, 'Juan Calderón More', '21', 'PONU 480767-6', '22597', 'INSERTAR', '2017-05-29', '12:49:33', '179.7.132.187', '179.7.132.187'),
(38, 'Juan Calderón More', '21', 'PONU 482052-8', '22598', 'INSERTAR', '2017-05-29', '12:52:50', '179.7.132.187', '179.7.132.187'),
(39, 'Juan Calderón More', '21', 'MMAU 107222-6', '22599', 'INSERTAR', '2017-05-29', '12:56:27', '179.7.132.187', '179.7.132.187'),
(40, 'Juan Calderón More', '21', 'BMOU 965137-1', '22588', 'INSERTAR', '2017-05-29', '15:11:35', '179.7.132.187', '179.7.132.187'),
(41, 'Juan Calderón More', '21', 'TTNU 896246-2', '22589', 'INSERTAR', '2017-05-29', '15:17:08', '179.7.132.187', '179.7.132.187'),
(42, 'Juan Calderón More', '21', 'BMOU 961635-0', '22590', 'INSERTAR', '2017-05-29', '15:19:48', '179.7.132.187', '179.7.132.187'),
(43, 'Juan Calderón More', '21', 'SUDU 605016-3', '22591', 'INSERTAR', '2017-05-29', '16:13:40', '179.7.132.187', '179.7.132.187'),
(44, 'Juan Calderón More', '21', 'SUDU 605417-4', '22592', 'INSERTAR', '2017-05-29', '16:15:15', '179.7.132.187', '179.7.132.187'),
(45, 'Juan Calderón More', '21', 'SUDU 604948-1', '22593', 'INSERTAR', '2017-05-29', '16:17:18', '179.7.132.187', '179.7.132.187'),
(46, 'Juan Calderón More', '21', 'SUDU 529208-0', '22594', 'INSERTAR', '2017-05-29', '16:20:48', '179.7.132.187', '179.7.132.187'),
(47, 'Juan Calderón More', '21', 'HLXU 876009-0', '1', 'INSERTAR', '2017-05-29', '16:24:37', '179.7.132.187', '179.7.132.187'),
(48, 'Juan Calderón More', '21', 'HLBU 900451-0', '2', 'INSERTAR', '2017-05-29', '16:27:25', '179.7.132.187', '179.7.132.187'),
(49, 'Juan Calderón More', '21', 'TCLU 102844-3', '1', 'INSERTAR', '2017-05-29', '16:29:35', '179.7.132.187', '179.7.132.187'),
(50, 'Juan Calderón More', '21', 'CRLU 137750-9', '2', 'INSERTAR', '2017-05-29', '16:31:34', '179.7.132.187', '179.7.132.187'),
(51, 'Juan Calderón More', '21', 'TLLU 106897-4', '1', 'INSERTAR', '2017-05-29', '16:40:33', '179.7.132.187', '179.7.132.187'),
(52, 'Juan Calderón More', '21', 'SUDU 823613-6', '1', 'INSERTAR', '2017-05-29', '16:42:35', '179.7.132.187', '179.7.132.187'),
(53, 'Juan Calderón More', '21', 'SUDU  523335-0', '2', 'INSERTAR', '2017-05-29', '16:44:06', '179.7.132.187', '179.7.132.187'),
(54, 'Juan Calderón More', '21', 'CXRU 161009-2', '2', 'INSERTAR', '2017-05-29', '16:48:31', '179.7.132.187', '179.7.132.187'),
(55, 'Juan Calderón More', '20', 'PONU 481624-0', '22513', 'INSERTAR', '2017-05-30', '10:52:17', '179.7.132.187', '179.7.132.187'),
(56, 'Juan Calderón More', '21', 'MNBU 315895-0', '22596', 'ACTUALIZAR', '2017-06-02', '09:16:42', '190.237.186.168', '190.237.186.168'),
(57, 'Juan Calderón More', '22', 'SUDU 616397-7', '22692', 'ACTUALIZAR', '2017-06-06', '10:58:47', '190.237.186.168', '190.237.186.168'),
(58, 'Juan Calderón More', '22', 'CSVU 750614-3', '2', 'ACTUALIZAR', '2017-06-06', '10:59:53', '190.237.186.168', '190.237.186.168'),
(59, 'Juan Calderón More', '22', 'SUDU 621885-3', '1', 'ACTUALIZAR', '2017-06-06', '11:00:37', '190.237.186.168', '190.237.186.168'),
(60, 'Juan Calderón More', '22', 'DFIU 215125-2', '1', 'ACTUALIZAR', '2017-06-06', '11:01:55', '190.237.186.168', '190.237.186.168'),
(61, 'Juan Calderón More', '22', 'MSCU 7433652', '1', 'ACTUALIZAR', '2017-06-06', '11:02:25', '190.237.186.168', '190.237.186.168'),
(62, 'Juan Calderón More', '22', 'HXLU 877232-1', '1', 'ACTUALIZAR', '2017-06-06', '11:02:53', '190.237.186.168', '190.237.186.168'),
(63, 'Juan Calderón More', '22', 'TEMU 906017-7', '22693', 'ACTUALIZAR', '2017-06-06', '11:03:16', '190.237.186.168', '190.237.186.168'),
(64, 'Juan Calderón More', '22', 'SUDU 600809-7', '22691', 'ACTUALIZAR', '2017-06-06', '11:03:51', '190.237.186.168', '190.237.186.168'),
(65, 'Juan Calderón More', '22', 'CNIU 222198-8', '22690', 'ACTUALIZAR', '2017-06-06', '11:04:21', '190.237.186.168', '190.237.186.168'),
(66, 'Juan Calderón More', '22', 'RRSU 100351-4', '22689', 'ACTUALIZAR', '2017-06-06', '11:04:47', '190.237.186.168', '190.237.186.168'),
(67, 'Juan Calderón More', '22', 'SZLU 980331-0', '22688', 'ACTUALIZAR', '2017-06-06', '11:05:29', '190.237.186.168', '190.237.186.168'),
(68, 'Juan Calderón More', '22', 'TCLU 138551-7', '22687', 'ACTUALIZAR', '2017-06-06', '11:05:52', '190.237.186.168', '190.237.186.168'),
(69, 'Juan Calderón More', '22', 'MSWU 102357-9', '22696', 'ACTUALIZAR', '2017-06-06', '11:06:22', '190.237.186.168', '190.237.186.168'),
(70, 'Juan Calderón More', '22', 'MMAU 107723-3', '22698', 'ACTUALIZAR', '2017-06-06', '11:06:47', '190.237.186.168', '190.237.186.168'),
(71, 'Juan Calderón More', '22', 'DAYU 670586-6', '22695', 'ACTUALIZAR', '2017-06-06', '11:07:03', '190.237.186.168', '190.237.186.168'),
(72, 'Juan Calderón More', '22', 'MWCU 657259-1', '22694', 'ACTUALIZAR', '2017-06-06', '11:07:32', '190.237.186.168', '190.237.186.168'),
(73, 'Juan Calderón More', '23', 'MNBU 308760-4', '22796', 'ACTUALIZAR', '2017-06-13', '07:19:10', '190.237.245.136', '190.237.245.136'),
(74, 'Juan Calderón More', '23', 'MMAU 124524-0', '22798', 'ACTUALIZAR', '2017-06-13', '07:46:42', '190.237.245.136', '190.237.245.136'),
(75, 'Juan Calderón More', '23', 'MNBU 041418-0', '22797', 'ACTUALIZAR', '2017-06-13', '07:48:40', '190.237.245.136', '190.237.245.136'),
(76, 'Juan Calderón More', '23', 'SUDU 823377-5', '22794', 'ACTUALIZAR', '2017-06-13', '07:51:51', '190.237.245.136', '190.237.245.136'),
(77, 'Juan Calderón More', '23', 'SUDU 801296-4', '22792', 'ACTUALIZAR', '2017-06-13', '07:53:25', '190.237.245.136', '190.237.245.136'),
(78, 'Juan Calderón More', '23', 'SUDU 800455-2', '22791', 'ACTUALIZAR', '2017-06-13', '07:55:12', '190.237.245.136', '190.237.245.136'),
(79, 'Juan Calderón More', '23', 'SUDU 625143-0', '22790', 'ACTUALIZAR', '2017-06-13', '07:56:41', '190.237.245.136', '190.237.245.136'),
(80, 'Juan Calderón More', '23', 'SUDU 823377-5', '22794', 'ACTUALIZAR', '2017-06-13', '07:58:03', '190.237.245.136', '190.237.245.136'),
(81, 'Juan Calderón More', '23', 'CAIU 554907-9', '22789', 'ACTUALIZAR', '2017-06-13', '08:00:01', '190.237.245.136', '190.237.245.136'),
(82, 'Juan Calderón More', '23', 'CAIU 554907-9', '22789', 'ACTUALIZAR', '2017-06-13', '08:01:20', '190.237.245.136', '190.237.245.136'),
(83, 'Juan Calderón More', '23', 'TRIU 811487-2', '22788', 'ACTUALIZAR', '2017-06-13', '08:02:16', '190.237.245.136', '190.237.245.136'),
(84, 'Juan Calderón More', '23', 'TRIU 835537-6', '22787', 'ACTUALIZAR', '2017-06-13', '08:03:50', '190.237.245.136', '190.237.245.136'),
(85, 'Juan Calderón More', '23', 'TRIU 835537-6', '22787', 'ACTUALIZAR', '2017-06-13', '08:03:51', '190.237.245.136', '190.237.245.136'),
(86, 'Juan Calderón More', '23', 'HLXU 877275-9', '1', 'ACTUALIZAR', '2017-06-13', '08:06:41', '190.237.245.136', '190.237.245.136'),
(87, 'Juan Calderón More', '23', 'HLXU 877275-9', '1', 'ACTUALIZAR', '2017-06-13', '08:06:41', '190.237.245.136', '190.237.245.136'),
(88, 'Juan Calderón More', '23', 'HLXU 877275-9', '1', 'ACTUALIZAR', '2017-06-13', '08:07:43', '190.237.245.136', '190.237.245.136'),
(89, 'Juan Calderón More', '23', 'HLXU 876189-9', '2', 'INSERTAR', '2017-06-13', '08:10:42', '190.237.245.136', '190.237.245.136'),
(90, 'Juan Calderón More', '23', 'HLXU 876189-9', '2', 'ACTUALIZAR', '2017-06-13', '08:12:31', '190.237.245.136', '190.237.245.136'),
(91, 'Juan Calderón More', '23', 'CPSU 515501-3', '1', 'ACTUALIZAR', '2017-06-13', '08:16:19', '190.237.245.136', '190.237.245.136'),
(92, 'Juan Calderón More', '23', 'HLXU 874770-9', '2', 'INSERTAR', '2017-06-13', '08:19:20', '190.237.245.136', '190.237.245.136'),
(93, 'Juan Calderón More', '23', 'HLXU 874770-9', '2', 'ACTUALIZAR', '2017-06-13', '08:19:48', '190.237.245.136', '190.237.245.136'),
(94, 'Juan Calderón More', '23', 'HLXU 876189-9', '1', 'ACTUALIZAR', '2017-06-13', '08:20:42', '190.237.245.136', '190.237.245.136'),
(95, 'Juan Calderón More', '23', 'HLXU 876189-9', '2', 'ACTUALIZAR', '2017-06-13', '08:22:01', '190.237.245.136', '190.237.245.136'),
(96, 'Juan Calderón More', '23', 'CPSU 515501-3', '1', 'INSERTAR', '2017-06-13', '08:24:20', '190.237.245.136', '190.237.245.136'),
(97, 'Juan Calderón More', '23', 'TLLU 104789-0', '1', 'INSERTAR', '2017-06-13', '08:27:25', '190.237.245.136', '190.237.245.136'),
(98, 'Juan Calderón More', '23', 'TLLU 104789-0', '1', 'INSERTAR', '2017-06-13', '08:27:25', '190.237.245.136', '190.237.245.136'),
(99, 'Juan Calderón More', '23', 'DFIU 811104-6', '2', 'ACTUALIZAR', '2017-06-13', '08:29:56', '190.237.245.136', '190.237.245.136'),
(100, 'Juan Calderón More', '23', 'SUDU 623810-3', '1', 'INSERTAR', '2017-06-13', '08:32:49', '190.237.245.136', '190.237.245.136'),
(101, 'Juan Calderón More', '23', 'SUDU 623810-3', '1', 'INSERTAR', '2017-06-13', '08:32:49', '190.237.245.136', '190.237.245.136'),
(102, 'Juan Calderón More', '23', 'SUDU 627365-5', '2', 'ACTUALIZAR', '2017-06-13', '08:34:10', '190.237.245.136', '190.237.245.136'),
(103, 'Juan Calderón More', '23', 'CPSU 515501-3', '1', 'ACTUALIZAR', '2017-06-13', '08:34:51', '190.237.245.136', '190.237.245.136'),
(104, 'Juan Calderón More', '23', 'HLXU 874770-9', '2', 'ACTUALIZAR', '2017-06-13', '08:35:12', '190.237.245.136', '190.237.245.136'),
(105, 'Juan Calderón More', '23', 'SUDU 625143-0', '22790', 'ACTUALIZAR', '2017-06-13', '08:36:22', '190.237.245.136', '190.237.245.136'),
(106, 'Juan Calderón More', '23', 'TRIU 835537-6', '22787', 'ACTUALIZAR', '2017-06-13', '09:22:37', '190.237.245.136', '190.237.245.136'),
(107, 'Juan Calderón More', '23', 'CPSU 515501-3', '1', 'ACTUALIZAR', '2017-06-13', '09:24:12', '190.237.245.136', '190.237.245.136'),
(108, 'Juan Calderón More', '23', 'CPSU 515501-3', '1', 'ELIMINAR', '2017-06-13', '09:24:15', '190.237.245.136', '190.237.245.136'),
(109, 'Juan Calderón More', '23', 'MMAU 124524-0', '22798', 'ACTUALIZAR', '2017-06-13', '09:25:22', '190.237.245.136', '190.237.245.136'),
(110, 'Juan Calderón More', '24', 'MWCU 678940-0', '22904', 'ACTUALIZAR', '2017-06-19', '11:09:33', '190.239.128.226', '190.239.128.226'),
(111, 'Juan Calderón More', '24', 'MNBU 365704-0', '22903', 'ACTUALIZAR', '2017-06-19', '11:11:04', '190.239.128.226', '190.239.128.226'),
(112, 'Juan Calderón More', '24', 'MMAU 111927-8', '22905', 'ACTUALIZAR', '2017-06-19', '11:15:27', '190.239.128.226', '190.239.128.226'),
(113, 'Juan Calderón More', '24', 'SUDU 803505-0', '22898', 'ACTUALIZAR', '2017-06-19', '11:17:19', '190.239.128.226', '190.239.128.226'),
(114, 'Juan Calderón More', '24', 'SUDU 801152-5', '22899', 'ACTUALIZAR', '2017-06-19', '11:19:12', '190.239.128.226', '190.239.128.226'),
(115, 'Juan Calderón More', '24', 'SUDU 628780-7', '22900', 'ACTUALIZAR', '2017-06-19', '11:20:36', '190.239.128.226', '190.239.128.226'),
(116, 'Juan Calderón More', '24', 'SUDU 629428-3', '22901', 'ACTUALIZAR', '2017-06-19', '11:22:16', '190.239.128.226', '190.239.128.226'),
(117, 'Juan Calderón More', '24', 'SUDU 801981-9', '22902', 'ACTUALIZAR', '2017-06-19', '11:24:49', '190.239.128.226', '190.239.128.226'),
(118, 'Juan Calderón More', '24', 'CAIU 556413-4', '22896', 'ACTUALIZAR', '2017-06-19', '11:26:41', '190.239.128.226', '190.239.128.226'),
(119, 'Juan Calderón More', '24', 'CAIU 556259-5', '22895', 'ACTUALIZAR', '2017-06-19', '11:27:55', '190.239.128.226', '190.239.128.226'),
(120, 'Juan Calderón More', '24', 'SZLU 980178-6', '22897', 'ACTUALIZAR', '2017-06-19', '11:29:50', '190.239.128.226', '190.239.128.226'),
(121, 'Juan Calderón More', '24', 'HLXU 875946-4', '1', 'INSERTAR', '2017-06-19', '11:32:44', '190.239.128.226', '190.239.128.226'),
(122, 'Juan Calderón More', '24', 'HLXU 875946-4', '1', 'INSERTAR', '2017-06-19', '11:32:44', '190.239.128.226', '190.239.128.226'),
(123, 'Juan Calderón More', '24', 'HLXU 875946-4', '2', 'ACTUALIZAR', '2017-06-19', '11:33:38', '190.239.128.226', '190.239.128.226'),
(124, 'Juan Calderón More', '24', 'TCLU 117750-8', '1', 'INSERTAR', '2017-06-19', '11:35:56', '190.239.128.226', '190.239.128.226'),
(125, 'Juan Calderón More', '24', 'TCLU 117750-8', '1', 'INSERTAR', '2017-06-19', '11:35:57', '190.239.128.226', '190.239.128.226'),
(126, 'Juan Calderón More', '24', 'HLBU 900823-9', '2', 'ACTUALIZAR', '2017-06-19', '11:36:24', '190.239.128.226', '190.239.128.226'),
(127, 'Juan Calderón More', '24', 'HLBU 902370-0', '2', 'ACTUALIZAR', '2017-06-19', '11:37:19', '190.239.128.226', '190.239.128.226'),
(128, 'Juan Calderón More', '24', 'SUDU 810704-1', '2', 'INSERTAR', '2017-06-19', '11:39:47', '190.239.128.226', '190.239.128.226'),
(129, 'Juan Calderón More', '24', 'SUDU 803122-3', '2', 'INSERTAR', '2017-06-19', '11:41:29', '190.239.128.226', '190.239.128.226'),
(130, 'Juan Calderón More', '24', 'TLLU 105911-8', '1', 'INSERTAR', '2017-06-19', '11:44:01', '190.239.128.226', '190.239.128.226'),
(131, 'Juan Calderón More', '24', 'TLLU 105911-8', '1', 'INSERTAR', '2017-06-19', '11:44:02', '190.239.128.226', '190.239.128.226'),
(132, 'Juan Calderón More', '24', 'BMOU 964121-8', '1', 'ACTUALIZAR', '2017-06-19', '11:44:48', '190.239.128.226', '190.239.128.226'),
(133, 'Juan Calderón More', '24', 'BMOU 964121-8', '2', 'ACTUALIZAR', '2017-06-19', '11:45:15', '190.239.128.226', '190.239.128.226'),
(134, 'Juan Calderón More', '25', 'PONU 483173-3', '23004', 'ACTUALIZAR', '2017-06-26', '10:09:55', '190.232.101.51', '190.232.101.51'),
(135, 'Juan Calderón More', '25', 'PONU 484117-7', '23005', 'ACTUALIZAR', '2017-06-26', '10:11:55', '190.232.101.51', '190.232.101.51'),
(136, 'Juan Calderón More', '25', 'MMAU 101782-5', '23006', 'ACTUALIZAR', '2017-06-26', '10:13:23', '190.232.101.51', '190.232.101.51'),
(137, 'Juan Calderón More', '25', 'SUDU 817060-9', '23003', 'ACTUALIZAR', '2017-06-26', '10:15:16', '190.232.101.51', '190.232.101.51'),
(138, 'Juan Calderón More', '25', 'SUDU 811441-5', '23000', 'ACTUALIZAR', '2017-06-26', '10:17:36', '190.232.101.51', '190.232.101.51'),
(139, 'Juan Calderón More', '25', 'SUDU 821029-7', '23001', 'ACTUALIZAR', '2017-06-26', '10:23:03', '190.232.101.51', '190.232.101.51'),
(140, 'Juan Calderón More', '25', 'SUDU 805116-9', '23002', 'ACTUALIZAR', '2017-06-26', '10:25:04', '190.232.101.51', '190.232.101.51'),
(141, 'Juan Calderón More', '25', 'TRIU 859069-4', '22998', 'ACTUALIZAR', '2017-06-26', '10:26:55', '190.232.101.51', '190.232.101.51'),
(142, 'Juan Calderón More', '25', 'CXRU 152387-1', '22999', 'ACTUALIZAR', '2017-06-26', '10:29:43', '190.232.101.51', '190.232.101.51'),
(143, 'Juan Calderón More', '25', 'CXRU 111677-3', '22997', 'ACTUALIZAR', '2017-06-26', '10:31:08', '190.232.101.51', '190.232.101.51'),
(144, 'Juan Calderón More', '25', 'HLBU 904294-8', '1', 'INSERTAR', '2017-06-26', '10:33:31', '190.232.101.51', '190.232.101.51'),
(145, 'Juan Calderón More', '25', 'HLBU 904294-8', '1', 'INSERTAR', '2017-06-26', '10:33:32', '190.232.101.51', '190.232.101.51'),
(146, 'Juan Calderón More', '25', 'HLBU 904186-0', '2', 'ACTUALIZAR', '2017-06-26', '10:35:25', '190.232.101.51', '190.232.101.51'),
(147, 'Juan Calderón More', '25', 'HLBU 904294-8', '1', 'ACTUALIZAR', '2017-06-26', '10:35:46', '190.232.101.51', '190.232.101.51'),
(148, 'Juan Calderón More', '25', 'HLBU 904294-8', '1', 'ACTUALIZAR', '2017-06-26', '10:36:07', '190.232.101.51', '190.232.101.51'),
(149, 'Juan Calderón More', '25', 'HLBU 904186-0', '2', 'ACTUALIZAR', '2017-06-26', '10:36:37', '190.232.101.51', '190.232.101.51'),
(150, 'Juan Calderón More', '25', 'HLBU 904186-0', '2', 'ELIMINAR', '2017-06-26', '10:36:38', '190.232.101.51', '190.232.101.51'),
(151, 'Juan Calderón More', '25', 'HLBU 904186-0', '2', 'INSERTAR', '2017-06-26', '10:38:43', '190.232.101.51', '190.232.101.51'),
(152, 'Juan Calderón More', '25', 'LNXU 845859-8', '1', 'INSERTAR', '2017-06-26', '11:02:02', '190.232.101.51', '190.232.101.51'),
(153, 'Juan Calderón More', '25', 'LNXU 845859-8', '1', 'INSERTAR', '2017-06-26', '11:02:02', '190.232.101.51', '190.232.101.51'),
(154, 'Juan Calderón More', '25', 'CPSU 511286-0', '2', 'ACTUALIZAR', '2017-06-26', '11:02:56', '190.232.101.51', '190.232.101.51'),
(155, 'Juan Calderón More', '25', 'CAIU 558762-8', '1', 'INSERTAR', '2017-06-26', '11:05:59', '190.232.101.51', '190.232.101.51'),
(156, 'Juan Calderón More', '25', 'CAIU 558762-8', '1', 'INSERTAR', '2017-06-26', '11:05:59', '190.232.101.51', '190.232.101.51'),
(157, 'Juan Calderón More', '25', 'DFIU 723082-1', '2', 'ACTUALIZAR', '2017-06-26', '11:08:38', '190.232.101.51', '190.232.101.51'),
(158, 'Juan Calderón More', '25', 'SUDU 803934-8', '1', 'INSERTAR', '2017-06-26', '11:10:53', '190.232.101.51', '190.232.101.51'),
(159, 'Juan Calderón More', '25', 'SUDU 803934-8', '1', 'INSERTAR', '2017-06-26', '11:10:54', '190.232.101.51', '190.232.101.51'),
(160, 'Juan Calderón More', '25', 'SUDU 810355-5', '2', 'ACTUALIZAR', '2017-06-26', '11:12:05', '190.232.101.51', '190.232.101.51'),
(161, 'Juan Calderón More', '25', 'HLBU 904186-0', '2', 'ACTUALIZAR', '2017-06-26', '11:12:34', '190.232.101.51', '190.232.101.51'),
(162, 'Juan Calderón More', '25', 'HLBU 904294-8', '1', 'ACTUALIZAR', '2017-06-26', '11:12:45', '190.232.101.51', '190.232.101.51'),
(163, 'Juan Calderón More', '25', 'SUDU 817060-9', '23003', 'ACTUALIZAR', '2017-06-26', '11:22:34', '190.232.101.51', '190.232.101.51'),
(164, 'Juan Calderón More', '25', 'SUDU 811441-5', '23000', 'ACTUALIZAR', '2017-06-26', '11:22:44', '190.232.101.51', '190.232.101.51'),
(165, 'Juan Calderón More', '25', 'PONU 484117-7', '23005', 'ACTUALIZAR', '2017-06-27', '09:41:46', '190.232.101.51', '190.232.101.51'),
(166, 'Juan Calderón More', '25', 'MMAU 101782-5', '23006', 'ACTUALIZAR', '2017-06-27', '09:42:26', '190.232.101.51', '190.232.101.51'),
(167, 'Juan Calderón More', '25', 'DFIU 723082-1', '2', 'ACTUALIZAR', '2017-06-27', '09:47:49', '190.232.101.51', '190.232.101.51'),
(168, 'Juan Calderón More', '25', 'CAIU 558762-8', '1', 'ACTUALIZAR', '2017-06-27', '09:48:02', '190.232.101.51', '190.232.101.51'),
(169, 'Juan Calderón More', '26', 'RRSU 168117-3', '23109', 'ACTUALIZAR', '2017-07-03', '10:35:04', '181.67.155.132', '181.67.155.132'),
(170, 'Juan Calderón More', '26', 'MMAU 125925-9', '23119', 'ACTUALIZAR', '2017-07-03', '10:36:44', '181.67.155.132', '181.67.155.132'),
(171, 'Juan Calderón More', '26', 'MSWU 001523-9', '23117', 'ACTUALIZAR', '2017-07-03', '10:38:06', '181.67.155.132', '181.67.155.132'),
(172, 'Juan Calderón More', '26', 'SUDU 623352-3', '23112', 'ACTUALIZAR', '2017-07-03', '10:40:06', '181.67.155.132', '181.67.155.132'),
(173, 'Juan Calderón More', '26', 'SUDU 620640-4', '23113', 'ACTUALIZAR', '2017-07-03', '10:42:02', '181.67.155.132', '181.67.155.132'),
(174, 'Juan Calderón More', '26', 'SUDU 813105-3', '23114', 'ACTUALIZAR', '2017-07-03', '10:47:53', '181.67.155.132', '181.67.155.132'),
(175, 'Juan Calderón More', '26', 'SUDU 801828-4', '23115', 'ACTUALIZAR', '2017-07-03', '11:05:04', '181.67.155.132', '181.67.155.132'),
(176, 'Juan Calderón More', '26', 'CXRU 152647-0', '23110', 'ACTUALIZAR', '2017-07-03', '11:06:17', '181.67.155.132', '181.67.155.132'),
(177, 'Juan Calderón More', '26', 'SUDU 801096-1', '23116', 'ACTUALIZAR', '2017-07-03', '11:08:14', '181.67.155.132', '181.67.155.132'),
(178, 'Juan Calderón More', '26', 'TEMU 937220-0', '23137', 'ACTUALIZAR', '2017-07-03', '11:10:10', '181.67.155.132', '181.67.155.132'),
(179, 'Juan Calderón More', '26', 'PONU 498186-2', '23118', 'ACTUALIZAR', '2017-07-03', '11:11:49', '181.67.155.132', '181.67.155.132'),
(180, 'Juan Calderón More', '26', 'PONU 498186-2', '23118', 'ACTUALIZAR', '2017-07-03', '11:12:03', '181.67.155.132', '181.67.155.132'),
(181, 'Juan Calderón More', '26', 'PONU 498186-2', '23118', 'ACTUALIZAR', '2017-07-03', '11:13:12', '181.67.155.132', '181.67.155.132'),
(182, 'Juan Calderón More', '26', 'HLBU 904194-1', '1', 'INSERTAR', '2017-07-03', '11:18:06', '181.67.155.132', '181.67.155.132'),
(183, 'Juan Calderón More', '26', 'HLBU 900822-3', '2', 'INSERTAR', '2017-07-03', '11:22:43', '181.67.155.132', '181.67.155.132'),
(184, 'Juan Calderón More', '26', 'GESU 950668-0', '1', 'INSERTAR', '2017-07-03', '11:27:00', '181.67.155.132', '181.67.155.132'),
(185, 'Juan Calderón More', '26', 'GESU 950668-0', '1', 'INSERTAR', '2017-07-03', '11:27:01', '181.67.155.132', '181.67.155.132'),
(186, 'Juan Calderón More', '26', 'GESU 950668-0', '2', 'ACTUALIZAR', '2017-07-03', '11:27:50', '181.67.155.132', '181.67.155.132'),
(187, 'Juan Calderón More', '26', 'BMOU 977317-4', '2', 'ACTUALIZAR', '2017-07-03', '11:28:25', '181.67.155.132', '181.67.155.132'),
(188, 'Juan Calderón More', '26', 'TLLU 105619-2', '1', 'INSERTAR', '2017-07-03', '11:31:51', '181.67.155.132', '181.67.155.132'),
(189, 'Juan Calderón More', '26', 'TLLU 105619-2', '1', 'INSERTAR', '2017-07-03', '11:31:51', '181.67.155.132', '181.67.155.132'),
(190, 'Juan Calderón More', '26', 'DFIU 723001-4', '2', 'ACTUALIZAR', '2017-07-03', '11:33:43', '181.67.155.132', '181.67.155.132'),
(191, 'Juan Calderón More', '26', 'SUDU 627430-6', '1', 'INSERTAR', '2017-07-03', '11:36:01', '181.67.155.132', '181.67.155.132'),
(192, 'Juan Calderón More', '26', 'SUDU 627430-6', '1', 'INSERTAR', '2017-07-03', '11:36:10', '181.67.155.132', '181.67.155.132'),
(193, 'Juan Calderón More', '26', 'SUDU 627430-6', '1', 'ACTUALIZAR', '2017-07-03', '11:37:04', '181.67.155.132', '181.67.155.132'),
(194, 'Juan Calderón More', '26', 'SUDU 627430-6', '1', 'ELIMINAR', '2017-07-03', '11:37:04', '181.67.155.132', '181.67.155.132'),
(195, 'APPBOSA SAMAN SAMAN', '26', 'PONU 498186-2', '23118', 'ACTUALIZAR', '2017-07-05', '08:25:57', '190.237.180.127', '190.237.180.127'),
(196, 'APPBOSA SAMAN SAMAN', '26', 'MSWU 001523-9', '23117', 'ACTUALIZAR', '2017-07-05', '08:26:33', '190.237.180.127', '190.237.180.127'),
(197, 'APPBOSA SAMAN SAMAN', '26', 'RRSU 168117-3', '23109', 'ACTUALIZAR', '2017-07-05', '08:27:20', '190.237.180.127', '190.237.180.127'),
(198, 'APPBOSA SAMAN SAMAN', '26', 'MMAU 125925-9', '23119', 'ACTUALIZAR', '2017-07-05', '08:28:35', '190.237.180.127', '190.237.180.127'),
(199, 'APPBOSA SAMAN SAMAN', '26', 'SUDU 813105-3', '23114', 'ACTUALIZAR', '2017-07-05', '08:29:21', '190.237.180.127', '190.237.180.127'),
(200, 'APPBOSA SAMAN SAMAN', '26', 'SUDU 620640-4', '23113', 'ACTUALIZAR', '2017-07-05', '08:29:48', '190.237.180.127', '190.237.180.127'),
(201, 'APPBOSA SAMAN SAMAN', '26', 'CXRU 152647-0', '23110', 'ACTUALIZAR', '2017-07-05', '08:30:19', '190.237.180.127', '190.237.180.127'),
(202, 'APPBOSA SAMAN SAMAN', '26', 'SUDU 801828-4', '23115', 'ACTUALIZAR', '2017-07-05', '08:30:44', '190.237.180.127', '190.237.180.127'),
(203, 'APPBOSA SAMAN SAMAN', '26', 'HLBU 904194-1', '1', 'ACTUALIZAR', '2017-07-05', '08:31:15', '190.237.180.127', '190.237.180.127'),
(204, 'APPBOSA SAMAN SAMAN', '26', 'TEMU 937220-0', '23137', 'ACTUALIZAR', '2017-07-05', '08:31:43', '190.237.180.127', '190.237.180.127'),
(205, 'APPBOSA SAMAN SAMAN', '26', 'SUDU 623352-3', '23112', 'ACTUALIZAR', '2017-07-05', '08:32:18', '190.237.180.127', '190.237.180.127'),
(206, 'APPBOSA SAMAN SAMAN', '26', 'SUDU 801096-1', '23116', 'ACTUALIZAR', '2017-07-05', '08:32:43', '190.237.180.127', '190.237.180.127'),
(207, 'APPBOSA SAMAN SAMAN', '26', 'HLBU 900822-3', '2', 'ACTUALIZAR', '2017-07-05', '08:33:37', '190.237.180.127', '190.237.180.127'),
(208, 'APPBOSA SAMAN SAMAN', '26', 'GESU 950668-0', '1', 'ACTUALIZAR', '2017-07-05', '08:35:28', '190.237.180.127', '190.237.180.127'),
(209, 'APPBOSA SAMAN SAMAN', '26', 'SUDU 627430-6', '1', 'ACTUALIZAR', '2017-07-05', '08:35:58', '190.237.180.127', '190.237.180.127'),
(210, 'APPBOSA SAMAN SAMAN', '26', 'BMOU 977317-4', '2', 'ACTUALIZAR', '2017-07-05', '08:36:41', '190.237.180.127', '190.237.180.127'),
(211, 'APPBOSA SAMAN SAMAN', '26', 'DFIU 723001-4', '2', 'ACTUALIZAR', '2017-07-05', '08:37:20', '190.237.180.127', '190.237.180.127'),
(212, 'APPBOSA SAMAN SAMAN', '26', 'TLLU 105619-2', '1', 'ACTUALIZAR', '2017-07-05', '08:39:37', '190.237.180.127', '190.237.180.127'),
(213, 'Juan Calderón More', '27', 'MMAU 113307-0', '23157', 'ACTUALIZAR', '2017-07-10', '08:29:20', '190.43.32.252', '190.43.32.252'),
(214, 'Juan Calderón More', '27', 'MMAU 113307-0', '23157', 'ACTUALIZAR', '2017-07-10', '08:29:20', '190.43.32.252', '190.43.32.252'),
(215, 'Juan Calderón More', '27', 'SUDU 818007-9', '23153', 'ACTUALIZAR', '2017-07-10', '08:40:35', '190.43.32.252', '190.43.32.252'),
(216, 'Juan Calderón More', '27', 'MWCU 685179-1', '23155', 'ACTUALIZAR', '2017-07-10', '09:03:40', '190.43.32.252', '190.43.32.252'),
(217, 'Juan Calderón More', '27', 'CXRU 135069-4', '23149', 'ACTUALIZAR', '2017-07-10', '09:07:34', '190.43.32.252', '190.43.32.252'),
(218, 'Juan Calderón More', '27', 'CXRU 135069-4', '23149', 'ACTUALIZAR', '2017-07-10', '09:07:35', '190.43.32.252', '190.43.32.252'),
(219, 'Juan Calderón More', '27', 'PONU481197-4', '23154', 'ACTUALIZAR', '2017-07-10', '09:10:30', '190.43.32.252', '190.43.32.252'),
(220, 'Juan Calderón More', '27', 'MWCU 685179-1', '23155', 'ACTUALIZAR', '2017-07-10', '09:11:07', '190.43.32.252', '190.43.32.252'),
(221, 'Juan Calderón More', '27', 'MWCU 685179-1', '23155', 'ACTUALIZAR', '2017-07-10', '09:11:07', '190.43.32.252', '190.43.32.252'),
(222, 'Juan Calderón More', '27', 'CAIU541966-6', '23148', 'ACTUALIZAR', '2017-07-10', '09:13:33', '190.43.32.252', '190.43.32.252'),
(223, 'Juan Calderón More', '27', 'CAIU541966-6', '23148', 'ACTUALIZAR', '2017-07-10', '09:13:33', '190.43.32.252', '190.43.32.252'),
(224, 'Juan Calderón More', '27', 'MWCU670752-6', '23156', 'ACTUALIZAR', '2017-07-10', '09:16:39', '190.43.32.252', '190.43.32.252'),
(225, 'Juan Calderón More', '27', 'SUDU628863-4', '23152', 'ACTUALIZAR', '2017-07-10', '09:25:24', '190.43.32.252', '190.43.32.252'),
(226, 'Juan Calderón More', '27', 'SUDU628863-4', '23152', 'ACTUALIZAR', '2017-07-10', '09:25:25', '190.43.32.252', '190.43.32.252'),
(227, 'Juan Calderón More', '27', 'SUDU 810435-6', '23151', 'ACTUALIZAR', '2017-07-10', '09:28:45', '190.43.32.252', '190.43.32.252'),
(228, 'Juan Calderón More', '27', 'SUDU 804300-8', '23150', 'ACTUALIZAR', '2017-07-10', '09:30:13', '190.43.32.252', '190.43.32.252'),
(229, 'Juan Calderón More', '27', 'PONU481197-4', '23154', 'ACTUALIZAR', '2017-07-10', '09:30:54', '190.43.32.252', '190.43.32.252'),
(230, 'Juan Calderón More', '27', 'MWCU670752-6', '23156', 'ACTUALIZAR', '2017-07-10', '09:31:44', '190.43.32.252', '190.43.32.252'),
(231, 'Juan Calderón More', '27', 'SUDU 818007-9', '23153', 'ACTUALIZAR', '2017-07-10', '09:32:36', '190.43.32.252', '190.43.32.252'),
(232, 'Juan Calderón More', '27', 'SUDU 810435-6', '23151', 'ACTUALIZAR', '2017-07-10', '09:33:25', '190.43.32.252', '190.43.32.252'),
(233, 'Juan Calderón More', '27', 'MMAU 113307-0', '23157', 'ACTUALIZAR', '2017-07-10', '09:33:55', '190.43.32.252', '190.43.32.252'),
(234, 'Juan Calderón More', '27', 'SUDU 803239-0', '23228', 'INSERTAR', '2017-07-10', '09:44:27', '190.43.32.252', '190.43.32.252'),
(235, 'Juan Calderón More', '27', 'SUDU 803239-0', '23228', 'INSERTAR', '2017-07-10', '09:44:27', '190.43.32.252', '190.43.32.252'),
(236, 'Juan Calderón More', '27', 'SUDU 803239-0', '23228', 'INSERTAR', '2017-07-10', '09:44:27', '190.43.32.252', '190.43.32.252'),
(237, 'Juan Calderón More', '27', 'MWCU 524004-1', '23227', 'ACTUALIZAR', '2017-07-10', '09:46:55', '190.43.32.252', '190.43.32.252'),
(238, 'Juan Calderón More', '27', 'MWCU 524004-1', '23227', 'ACTUALIZAR', '2017-07-10', '09:46:55', '190.43.32.252', '190.43.32.252'),
(239, 'Juan Calderón More', '27', 'MWCU670752-6', '23156', 'ACTUALIZAR', '2017-07-10', '09:48:31', '190.43.32.252', '190.43.32.252'),
(240, 'Juan Calderón More', '27', 'MWCU670752-6', '23156', 'ACTUALIZAR', '2017-07-10', '09:48:32', '190.43.32.252', '190.43.32.252'),
(241, 'Juan Calderón More', '27', 'MWCU670752-6', '23156', 'ACTUALIZAR', '2017-07-10', '09:48:32', '190.43.32.252', '190.43.32.252'),
(242, 'Juan Calderón More', '27', 'MWCU 685179-1', '23155', 'ACTUALIZAR', '2017-07-10', '09:48:54', '190.43.32.252', '190.43.32.252'),
(243, 'Juan Calderón More', '27', 'MWCU 685179-1', '23155', 'ACTUALIZAR', '2017-07-10', '09:48:54', '190.43.32.252', '190.43.32.252'),
(244, 'Juan Calderón More', '27', 'CAIU541966-6', '23148', 'ACTUALIZAR', '2017-07-10', '09:49:44', '190.43.32.252', '190.43.32.252'),
(245, 'Juan Calderón More', '27', 'CAIU541966-6', '23148', 'ACTUALIZAR', '2017-07-10', '09:49:44', '190.43.32.252', '190.43.32.252'),
(246, 'Juan Calderón More', '27', 'CXRU 135069-4', '23149', 'ACTUALIZAR', '2017-07-10', '09:50:05', '190.43.32.252', '190.43.32.252'),
(247, 'Juan Calderón More', '27', 'HLBU 900891-7', '1', 'INSERTAR', '2017-07-10', '09:53:21', '190.43.32.252', '190.43.32.252'),
(248, 'Juan Calderón More', '27', 'HLBU 900891-7', '1', 'INSERTAR', '2017-07-10', '09:53:22', '190.43.32.252', '190.43.32.252'),
(249, 'Juan Calderón More', '27', 'HLBU 900891-7', '1', 'INSERTAR', '2017-07-10', '09:53:22', '190.43.32.252', '190.43.32.252'),
(250, 'Juan Calderón More', '27', 'HLBU 904065-2', '2', 'ACTUALIZAR', '2017-07-10', '09:54:38', '190.43.32.252', '190.43.32.252'),
(251, 'Juan Calderón More', '27', 'HLBU904522-7', '3', 'ACTUALIZAR', '2017-07-10', '09:55:44', '190.43.32.252', '190.43.32.252'),
(252, 'Juan Calderón More', '27', 'LNXU 755981-3', '1', 'INSERTAR', '2017-07-10', '09:59:05', '190.43.32.252', '190.43.32.252'),
(253, 'Juan Calderón More', '27', 'LNXU 755981-3', '1', 'INSERTAR', '2017-07-10', '09:59:05', '190.43.32.252', '190.43.32.252'),
(254, 'Juan Calderón More', '27', 'BMOU 921454-0', '2', 'ACTUALIZAR', '2017-07-10', '10:00:25', '190.43.32.252', '190.43.32.252'),
(255, 'Juan Calderón More', '27', 'BMOU 921454-0', '2', 'ACTUALIZAR', '2017-07-10', '10:00:25', '190.43.32.252', '190.43.32.252'),
(256, 'Juan Calderón More', '27', 'SUDU 804227-5', '1', 'INSERTAR', '2017-07-10', '10:02:49', '190.43.32.252', '190.43.32.252'),
(257, 'Juan Calderón More', '27', 'TLLU 106743-2', '1', 'INSERTAR', '2017-07-10', '10:05:40', '190.43.32.252', '190.43.32.252'),
(258, 'Juan Calderón More', '27', 'TLLU 106743-2', '1', 'INSERTAR', '2017-07-10', '10:05:40', '190.43.32.252', '190.43.32.252'),
(259, 'Juan Calderón More', '27', 'SUDU 811934-0', '2', 'ACTUALIZAR', '2017-07-10', '10:08:41', '190.43.32.252', '190.43.32.252'),
(260, 'Juan Calderón More', '27', 'SUDU 803239-0', '23228', 'ACTUALIZAR', '2017-07-10', '10:11:54', '190.43.32.252', '190.43.32.252'),
(261, 'Juan Calderón More', '27', 'SUDU 803239-0', '23228', 'ELIMINAR', '2017-07-10', '10:11:55', '190.43.32.252', '190.43.32.252'),
(262, 'Juan Calderón More', '28', 'SUDU 817614-5', '23274', 'ACTUALIZAR', '2017-07-17', '12:11:38', '190.42.204.119', '190.42.204.119'),
(263, 'Juan Calderón More', '28', 'BMOU 965124-2', '23272', 'ACTUALIZAR', '2017-07-17', '12:12:59', '190.42.204.119', '190.42.204.119'),
(264, 'Juan Calderón More', '28', 'SUDU 821453-8', '23275', 'ACTUALIZAR', '2017-07-17', '12:14:52', '190.42.204.119', '190.42.204.119'),
(265, 'Juan Calderón More', '28', 'SUDU 821453-8', '23275', 'ACTUALIZAR', '2017-07-17', '12:14:56', '190.42.204.119', '190.42.204.119'),
(266, 'Juan Calderón More', '28', 'SUDU 806707-8', '23276', 'ACTUALIZAR', '2017-07-17', '12:17:38', '190.42.204.119', '190.42.204.119'),
(267, 'Juan Calderón More', '28', 'SUDU 803304-1', '23273', 'ACTUALIZAR', '2017-07-17', '12:18:26', '190.42.204.119', '190.42.204.119'),
(268, 'Juan Calderón More', '28', 'MWCU 683473-1', '23279', 'ACTUALIZAR', '2017-07-17', '12:29:05', '190.42.204.119', '190.42.204.119'),
(269, 'Juan Calderón More', '28', 'MSWU 900991-5', '23278', 'ACTUALIZAR', '2017-07-17', '12:30:45', '190.42.204.119', '190.42.204.119'),
(270, 'Juan Calderón More', '28', 'MMAU 106056-5', '23280', 'ACTUALIZAR', '2017-07-17', '12:31:48', '190.42.204.119', '190.42.204.119'),
(271, 'Juan Calderón More', '28', 'MWCU 676530-6', '23277', 'ACTUALIZAR', '2017-07-17', '12:33:06', '190.42.204.119', '190.42.204.119'),
(272, 'Juan Calderón More', '28', 'CAIU 565696-6', '23271', 'ACTUALIZAR', '2017-07-17', '12:35:15', '190.42.204.119', '190.42.204.119'),
(273, 'Juan Calderón More', '28', 'SUDU 628821-2', '23317', 'INSERTAR', '2017-07-17', '12:39:59', '190.42.204.119', '190.42.204.119'),
(274, 'Juan Calderón More', '28', 'HLBU 907719-0', '1', 'INSERTAR', '2017-07-17', '12:48:29', '190.42.204.119', '190.42.204.119'),
(275, 'Juan Calderón More', '28', 'HLBU 907719-0', '1', 'INSERTAR', '2017-07-17', '12:48:29', '190.42.204.119', '190.42.204.119'),
(276, 'Juan Calderón More', '28', 'HLBU 907720-3', '2', 'ACTUALIZAR', '2017-07-17', '12:49:44', '190.42.204.119', '190.42.204.119'),
(277, 'Juan Calderón More', '28', 'TCLU 118219-2', '1', 'INSERTAR', '2017-07-17', '12:52:06', '190.42.204.119', '190.42.204.119'),
(278, 'Juan Calderón More', '28', 'TCLU 118219-2', '1', 'INSERTAR', '2017-07-17', '12:52:06', '190.42.204.119', '190.42.204.119'),
(279, 'Juan Calderón More', '28', 'TCLU 102065-3', '2', 'ACTUALIZAR', '2017-07-17', '12:53:22', '190.42.204.119', '190.42.204.119'),
(280, 'Juan Calderón More', '28', 'SUDU 820004-6', '1', 'INSERTAR', '2017-07-17', '12:55:26', '190.42.204.119', '190.42.204.119'),
(281, 'Juan Calderón More', '28', 'TLLU 105065-6', '1', 'INSERTAR', '2017-07-17', '12:57:30', '190.42.204.119', '190.42.204.119'),
(282, 'Juan Calderón More', '28', 'TLLU 105065-6', '1', 'INSERTAR', '2017-07-17', '12:57:30', '190.42.204.119', '190.42.204.119'),
(283, 'Juan Calderón More', '28', 'DFIU 811154-0', '2', 'ACTUALIZAR', '2017-07-17', '12:59:21', '190.42.204.119', '190.42.204.119'),
(284, 'Juan Calderón More', '28', 'TLLU 105065-6', '1', 'ACTUALIZAR', '2017-07-17', '12:59:53', '190.42.204.119', '190.42.204.119'),
(285, 'APPBOSA SAMAN SAMAN', '27', 'MMAU 113307-0', '23157', 'ACTUALIZAR', '2017-07-17', '14:47:06', '190.42.204.119', '190.42.204.119'),
(286, 'APPBOSA SAMAN SAMAN', '27', 'MWCU670752-6', '23156', 'ACTUALIZAR', '2017-07-17', '14:48:46', '190.42.204.119', '190.42.204.119'),
(287, 'APPBOSA SAMAN SAMAN', '27', 'MWCU 524004-1', '23227', 'ACTUALIZAR', '2017-07-17', '14:50:16', '190.42.204.119', '190.42.204.119'),
(288, 'APPBOSA SAMAN SAMAN', '27', 'MWCU 524004-1', '23227', 'ACTUALIZAR', '2017-07-17', '14:51:00', '190.42.204.119', '190.42.204.119'),
(289, 'APPBOSA SAMAN SAMAN', '27', 'PONU481197-4', '23154', 'ACTUALIZAR', '2017-07-17', '14:51:50', '190.42.204.119', '190.42.204.119'),
(290, 'APPBOSA SAMAN SAMAN', '27', 'SUDU 803239-0', '23228', 'ACTUALIZAR', '2017-07-17', '14:52:36', '190.42.204.119', '190.42.204.119'),
(291, 'APPBOSA SAMAN SAMAN', '27', 'CAIU541966-6', '23148', 'ACTUALIZAR', '2017-07-17', '14:53:20', '190.42.204.119', '190.42.204.119'),
(292, 'APPBOSA SAMAN SAMAN', '27', 'MWCU 685179-1', '23155', 'ACTUALIZAR', '2017-07-17', '14:53:43', '190.42.204.119', '190.42.204.119'),
(293, 'APPBOSA SAMAN SAMAN', '27', 'SUDU 804300-8', '23150', 'ACTUALIZAR', '2017-07-17', '14:54:38', '190.42.204.119', '190.42.204.119'),
(294, 'APPBOSA SAMAN SAMAN', '27', 'CXRU 135069-4', '23149', 'ACTUALIZAR', '2017-07-17', '14:55:15', '190.42.204.119', '190.42.204.119'),
(295, 'APPBOSA SAMAN SAMAN', '27', 'SUDU 818007-9', '23153', 'ACTUALIZAR', '2017-07-17', '14:55:44', '190.42.204.119', '190.42.204.119'),
(296, 'APPBOSA SAMAN SAMAN', '27', 'SUDU 810435-6', '23151', 'ACTUALIZAR', '2017-07-17', '14:56:25', '190.42.204.119', '190.42.204.119'),
(297, 'APPBOSA SAMAN SAMAN', '27', 'HLBU 900891-7', '1', 'ACTUALIZAR', '2017-07-17', '14:56:55', '190.42.204.119', '190.42.204.119'),
(298, 'APPBOSA SAMAN SAMAN', '27', 'SUDU 804227-5', '1', 'ACTUALIZAR', '2017-07-17', '14:57:22', '190.42.204.119', '190.42.204.119'),
(299, 'APPBOSA SAMAN SAMAN', '27', 'BMOU 921454-0', '2', 'ACTUALIZAR', '2017-07-17', '14:58:09', '190.42.204.119', '190.42.204.119'),
(300, 'APPBOSA SAMAN SAMAN', '27', 'LNXU 755981-3', '1', 'ACTUALIZAR', '2017-07-17', '14:58:36', '190.42.204.119', '190.42.204.119'),
(301, 'APPBOSA SAMAN SAMAN', '27', 'HLBU904522-7', '3', 'ACTUALIZAR', '2017-07-17', '14:59:11', '190.42.204.119', '190.42.204.119'),
(302, 'APPBOSA SAMAN SAMAN', '27', 'HLBU 904065-2', '2', 'ACTUALIZAR', '2017-07-17', '14:59:37', '190.42.204.119', '190.42.204.119'),
(303, 'APPBOSA SAMAN SAMAN', '27', 'SUDU628863-4', '23152', 'ACTUALIZAR', '2017-07-17', '15:00:02', '190.42.204.119', '190.42.204.119'),
(304, 'APPBOSA SAMAN SAMAN', '27', 'TLLU 106743-2', '1', 'ACTUALIZAR', '2017-07-17', '15:01:00', '190.42.204.119', '190.42.204.119'),
(305, 'APPBOSA SAMAN SAMAN', '27', 'SUDU 811934-0', '2', 'ACTUALIZAR', '2017-07-17', '15:01:25', '190.42.204.119', '190.42.204.119'),
(306, 'APPBOSA SAMAN SAMAN', '27', 'MMAU 113307-0', '23157', 'ACTUALIZAR', '2017-07-17', '15:09:28', '190.42.204.119', '190.42.204.119'),
(307, 'APPBOSA SAMAN SAMAN', '27', 'MWCU 685179-1', '23155', 'ACTUALIZAR', '2017-07-17', '15:10:24', '190.42.204.119', '190.42.204.119'),
(308, 'APPBOSA SAMAN SAMAN', '27', 'MWCU670752-6', '23156', 'ACTUALIZAR', '2017-07-17', '15:11:22', '190.42.204.119', '190.42.204.119'),
(309, 'APPBOSA SAMAN SAMAN', '27', 'PONU481197-4', '23154', 'ACTUALIZAR', '2017-07-17', '15:11:51', '190.42.204.119', '190.42.204.119'),
(310, 'APPBOSA SAMAN SAMAN', '27', 'MWCU 524004-1', '23227', 'ACTUALIZAR', '2017-07-17', '15:12:28', '190.42.204.119', '190.42.204.119'),
(311, 'APPBOSA SAMAN SAMAN', '27', 'CAIU541966-6', '23148', 'ACTUALIZAR', '2017-07-17', '15:14:09', '190.42.204.119', '190.42.204.119'),
(312, 'APPBOSA SAMAN SAMAN', '27', 'CXRU 135069-4', '23149', 'ACTUALIZAR', '2017-07-17', '15:14:27', '190.42.204.119', '190.42.204.119'),
(313, 'APPBOSA SAMAN SAMAN', '27', 'SUDU 804300-8', '23150', 'ACTUALIZAR', '2017-07-17', '15:14:48', '190.42.204.119', '190.42.204.119'),
(314, 'APPBOSA SAMAN SAMAN', '27', 'SUDU 803239-0', '23228', 'ACTUALIZAR', '2017-07-17', '15:15:35', '190.42.204.119', '190.42.204.119'),
(315, 'APPBOSA SAMAN SAMAN', '27', 'SUDU 811934-0', '2', 'ACTUALIZAR', '2017-07-17', '15:17:18', '190.42.204.119', '190.42.204.119'),
(316, 'Juan Calderón More', '29', 'PONU 489829-6', '23348', 'ACTUALIZAR', '2017-07-24', '07:55:22', '190.42.208.16', '190.42.208.16'),
(317, 'Juan Calderón More', '29', 'PONU 489353-0', '23349', 'ACTUALIZAR', '2017-07-24', '07:56:41', '190.42.208.16', '190.42.208.16'),
(318, 'Juan Calderón More', '29', 'MWCU 530573-3', '23350', 'ACTUALIZAR', '2017-07-24', '07:59:39', '190.42.208.16', '190.42.208.16'),
(319, 'Juan Calderón More', '29', 'TTNU 845055-3', '23351', 'ACTUALIZAR', '2017-07-24', '08:03:46', '190.42.208.16', '190.42.208.16'),
(320, 'Juan Calderón More', '29', 'SUDU 627879-1', '23354', 'ACTUALIZAR', '2017-07-24', '08:07:27', '74.82.35.40', '74.82.35.40'),
(321, 'Juan Calderón More', '29', 'SUDU 803258-0', '23357', 'ACTUALIZAR', '2017-07-24', '08:08:26', '74.82.35.40', '74.82.35.40'),
(322, 'Juan Calderón More', '29', 'CXRU 152421-9', '23352', 'ACTUALIZAR', '2017-07-24', '08:09:41', '74.82.35.40', '74.82.35.40'),
(323, 'Juan Calderón More', '29', 'SUDU 624583-8', '23355', 'ACTUALIZAR', '2017-07-24', '08:10:40', '74.82.35.40', '74.82.35.40'),
(324, 'Juan Calderón More', '29', 'SUDU 804642-9', '23353', 'ACTUALIZAR', '2017-07-24', '08:11:34', '74.82.35.40', '74.82.35.40'),
(325, 'Juan Calderón More', '29', 'SUDU 628546-6', '23356', 'ACTUALIZAR', '2017-07-24', '08:12:22', '74.82.35.40', '74.82.35.40'),
(326, 'Juan Calderón More', '29', 'MMAU 110134-5', '23347', 'ACTUALIZAR', '2017-07-24', '08:13:29', '74.82.35.40', '74.82.35.40'),
(327, 'Juan Calderón More', '29', 'HLBU 904788-9', '1', 'INSERTAR', '2017-07-24', '08:17:20', '74.82.35.40', '74.82.35.40'),
(328, 'Juan Calderón More', '29', 'HLBU 904788-9', '1', 'INSERTAR', '2017-07-24', '08:17:20', '74.82.35.40', '74.82.35.40'),
(329, 'Juan Calderón More', '29', 'HLBU 904788-9', '1', 'INSERTAR', '2017-07-24', '08:17:21', '74.82.35.40', '74.82.35.40'),
(330, 'Juan Calderón More', '29', 'HLBU 907963-3', '2', 'ACTUALIZAR', '2017-07-24', '08:17:59', '74.82.35.40', '74.82.35.40'),
(331, 'Juan Calderón More', '29', 'HLBU 904788-9', '3', 'ACTUALIZAR', '2017-07-24', '08:18:48', '74.82.35.40', '74.82.35.40'),
(332, 'Juan Calderón More', '29', 'CRLU 183918-7', '1', 'INSERTAR', '2017-07-24', '08:21:26', '74.82.35.40', '74.82.35.40'),
(333, 'Juan Calderón More', '29', 'CRLU 183918-7', '1', 'INSERTAR', '2017-07-24', '08:21:27', '74.82.35.40', '74.82.35.40'),
(334, 'Juan Calderón More', '29', 'GESU 950621-0', '2', 'ACTUALIZAR', '2017-07-24', '08:22:08', '74.82.35.40', '74.82.35.40'),
(335, 'Juan Calderón More', '29', 'HLBU 907171-4', '3', 'ACTUALIZAR', '2017-07-24', '08:22:41', '74.82.35.40', '74.82.35.40'),
(336, 'Juan Calderón More', '29', 'SUDU 807093-4', '1', 'INSERTAR', '2017-07-24', '08:24:39', '74.82.35.40', '74.82.35.40'),
(337, 'Juan Calderón More', '29', 'TLLU 104530-4', '1', 'INSERTAR', '2017-07-24', '08:26:49', '74.82.35.40', '74.82.35.40'),
(338, 'Juan Calderón More', '29', 'TLLU 104530-4', '1', 'INSERTAR', '2017-07-24', '08:26:49', '74.82.35.40', '74.82.35.40'),
(339, 'Juan Calderón More', '29', 'DFIU 722128-6', '2', 'ACTUALIZAR', '2017-07-24', '08:28:29', '74.82.35.40', '74.82.35.40'),
(340, 'Juan Calderón More', '29', 'TLLU 104530-4', '1', 'ACTUALIZAR', '2017-07-24', '11:51:43', '190.42.208.16', '190.42.208.16'),
(341, 'Juan Calderón More', '29', 'SUDU 807093-4', '1', 'ACTUALIZAR', '2017-07-24', '11:52:09', '190.42.208.16', '190.42.208.16'),
(342, 'Juan Calderón More', '29', 'SUDU 807093-4', '1', 'ACTUALIZAR', '2017-07-24', '11:52:10', '190.42.208.16', '190.42.208.16'),
(343, 'Juan Calderón More', '30', 'PONU 489371-4', '23422', 'ACTUALIZAR', '2017-08-01', '08:50:25', '190.42.203.13', '190.42.203.13'),
(344, 'Juan Calderón More', '30', 'MNBU 307774-0', '23420', 'ACTUALIZAR', '2017-08-01', '08:52:52', '190.42.203.13', '190.42.203.13'),
(345, 'Juan Calderón More', '30', 'MNBU 307774-0', '23422', 'ACTUALIZAR', '2017-08-01', '08:54:58', '190.42.203.13', '190.42.203.13'),
(346, 'Juan Calderón More', '30', 'MNBU 339827-3', '23420', 'ACTUALIZAR', '2017-08-01', '08:56:04', '190.42.203.13', '190.42.203.13'),
(347, 'Juan Calderón More', '30', 'PONU 489371-4', '23421', 'ACTUALIZAR', '2017-08-01', '08:58:38', '190.42.203.13', '190.42.203.13'),
(348, 'Juan Calderón More', '30', 'PONU 450426-3', '23423', 'ACTUALIZAR', '2017-08-01', '09:10:01', '190.42.203.13', '190.42.203.13'),
(349, 'Juan Calderón More', '30', 'MMAU 115229-7', '23424', 'ACTUALIZAR', '2017-08-01', '09:12:45', '190.42.203.13', '190.42.203.13'),
(350, 'Juan Calderón More', '30', 'BMOU 965241-8', '23415', 'ACTUALIZAR', '2017-08-01', '09:20:54', '190.42.203.13', '190.42.203.13'),
(351, 'Juan Calderón More', '30', 'BMOU 961803-3', '23414', 'ACTUALIZAR', '2017-08-01', '09:22:06', '190.42.203.13', '190.42.203.13'),
(352, 'Juan Calderón More', '30', 'SUDU', '23419', 'ACTUALIZAR', '2017-08-01', '09:23:43', '190.42.203.13', '190.42.203.13'),
(353, 'Juan Calderón More', '30', 'SUDU 834675-1', '23419', 'ACTUALIZAR', '2017-08-01', '09:25:06', '190.42.203.13', '190.42.203.13'),
(354, 'Juan Calderón More', '30', 'SUDU 813990-1', '23418', 'ACTUALIZAR', '2017-08-01', '09:28:35', '190.42.203.13', '190.42.203.13'),
(355, 'Juan Calderón More', '30', 'SUDU 610980-5', '23416', 'ACTUALIZAR', '2017-08-01', '09:29:51', '190.42.203.13', '190.42.203.13'),
(356, 'Juan Calderón More', '30', 'SUDU 812206-7', '23417', 'ACTUALIZAR', '2017-08-01', '09:31:15', '190.42.203.13', '190.42.203.13'),
(357, 'Juan Calderón More', '30', 'LNXU 965478-5', '1', 'ACTUALIZAR', '2017-08-01', '09:36:42', '190.42.203.13', '190.42.203.13'),
(358, 'Juan Calderón More', '30', 'FSCU 567276-9', '1', 'ACTUALIZAR', '2017-08-01', '09:39:43', '190.42.203.13', '190.42.203.13'),
(359, 'Juan Calderón More', '30', 'CAIU 565452-0', '1', 'ACTUALIZAR', '2017-08-01', '09:41:53', '190.42.203.13', '190.42.203.13'),
(360, 'Juan Calderón More', '30', 'HLXU 877281-0', '2', 'ACTUALIZAR', '2017-08-01', '09:47:00', '190.42.203.13', '190.42.203.13'),
(361, 'Juan Calderón More', '30', 'SUDU 813474-6', '1', 'ACTUALIZAR', '2017-08-01', '09:49:11', '190.42.203.13', '190.42.203.13'),
(362, 'Juan Calderón More', '30', 'HLBU 900879-5', '1', 'ACTUALIZAR', '2017-08-01', '09:50:45', '190.42.203.13', '190.42.203.13'),
(363, 'Juan Calderón More', '31', '1', '1', 'INSERTAR', '2017-08-01', '10:55:43', '190.42.203.13', '190.42.203.13'),
(364, 'Juan Calderón More', '31', '1', '1', 'INSERTAR', '2017-08-01', '10:56:47', '190.42.203.13', '190.42.203.13'),
(365, 'Juan Calderón More', '31', '1', '1', 'INSERTAR', '2017-08-01', '10:57:37', '190.42.203.13', '190.42.203.13'),
(366, 'Juan Calderón More', '31', '1', '1', 'ACTUALIZAR', '2017-08-01', '10:57:59', '190.42.203.13', '190.42.203.13'),
(367, 'Juan Calderón More', '31', '1', '1', 'INSERTAR', '2017-08-01', '10:58:59', '190.42.203.13', '190.42.203.13'),
(368, 'Juan Calderón More', '31', '1', '1', 'INSERTAR', '2017-08-01', '10:59:36', '190.42.203.13', '190.42.203.13'),
(369, 'Juan Calderón More', '31', '1', '1', 'INSERTAR', '2017-08-01', '11:00:48', '190.42.203.13', '190.42.203.13'),
(370, 'Juan Calderón More', '31', 'MNBU 309738-8', '23508', 'ACTUALIZAR', '2017-08-07', '07:32:33', '181.64.26.246', '181.64.26.246'),
(371, 'Juan Calderón More', '31', 'SUDU 8080594', '23510', 'ACTUALIZAR', '2017-08-07', '07:34:56', '181.64.26.246', '181.64.26.246'),
(372, 'Juan Calderón More', '31', 'SUDU 823497-7', '23512', 'ACTUALIZAR', '2017-08-07', '07:36:30', '181.64.26.246', '181.64.26.246'),
(373, 'Juan Calderón More', '31', 'SUDU 808059 4', '23510', 'ACTUALIZAR', '2017-08-07', '07:37:13', '181.64.26.246', '181.64.26.246'),
(374, 'Juan Calderón More', '31', 'SUDU 822107-5', '23513', 'ACTUALIZAR', '2017-08-07', '07:39:18', '181.64.26.246', '181.64.26.246'),
(375, 'Juan Calderón More', '31', 'SUDU 822107-5', '23511', 'ACTUALIZAR', '2017-08-07', '07:42:23', '181.64.26.246', '181.64.26.246'),
(376, 'Juan Calderón More', '31', 'SUDU 822107-5', '23511', 'ACTUALIZAR', '2017-08-07', '07:42:50', '181.64.26.246', '181.64.26.246'),
(377, 'Juan Calderón More', '31', 'MWCU 663807-1', '23517', 'ACTUALIZAR', '2017-08-07', '07:51:22', '181.64.26.246', '181.64.26.246'),
(378, 'Juan Calderón More', '31', 'SUDU 808059 4', '23510', 'ACTUALIZAR', '2017-08-07', '07:52:12', '181.64.26.246', '181.64.26.246'),
(379, 'Juan Calderón More', '31', 'SUDU 823497-7', '23512', 'ACTUALIZAR', '2017-08-07', '07:52:47', '181.64.26.246', '181.64.26.246'),
(380, 'Juan Calderón More', '31', 'PONU 497285-5', '23515', 'ACTUALIZAR', '2017-08-07', '07:54:12', '181.64.26.246', '181.64.26.246'),
(381, 'Juan Calderón More', '31', 'SUDU 822107-5', '23513', 'ACTUALIZAR', '2017-08-07', '07:54:43', '181.64.26.246', '181.64.26.246'),
(382, 'Juan Calderón More', '31', 'SUDU 822107-5', '23511', 'ACTUALIZAR', '2017-08-07', '07:55:08', '181.64.26.246', '181.64.26.246'),
(383, 'Juan Calderón More', '31', 'MNBU 309738-8', '23508', 'ACTUALIZAR', '2017-08-07', '07:56:35', '181.64.26.246', '181.64.26.246'),
(384, 'Juan Calderón More', '31', 'MWMU 633834-6', '23516', 'ACTUALIZAR', '2017-08-07', '08:00:54', '181.64.26.246', '181.64.26.246'),
(385, 'Juan Calderón More', '31', 'MWMU 633834-6', '23516', 'ACTUALIZAR', '2017-08-07', '08:14:29', '181.64.26.246', '181.64.26.246'),
(386, 'Juan Calderón More', '31', 'CAIU 542117-5', '23518', 'ACTUALIZAR', '2017-08-07', '08:23:21', '181.64.26.246', '181.64.26.246'),
(387, 'Juan Calderón More', '31', 'MMAU 121545-6', '23519', 'ACTUALIZAR', '2017-08-07', '08:26:20', '181.64.26.246', '181.64.26.246'),
(388, 'Juan Calderón More', '31', 'CXRU 111430-1', '23509', 'ACTUALIZAR', '2017-08-07', '08:30:23', '181.64.26.246', '181.64.26.246'),
(389, 'Juan Calderón More', '31', 'PONU 497285-5', '23515', 'ACTUALIZAR', '2017-08-07', '08:31:12', '181.64.26.246', '181.64.26.246'),
(390, 'Juan Calderón More', '31', '1', '1', 'ACTUALIZAR', '2017-08-07', '08:32:20', '181.64.26.246', '181.64.26.246'),
(391, 'Juan Calderón More', '31', 'MWMU 633834-6', '23516', 'ACTUALIZAR', '2017-08-07', '08:32:50', '181.64.26.246', '181.64.26.246'),
(392, 'Juan Calderón More', '31', 'MMAU 121545-6', '23519', 'ACTUALIZAR', '2017-08-07', '08:33:21', '181.64.26.246', '181.64.26.246'),
(393, 'Juan Calderón More', '31', 'MWCU 663807-1', '23517', 'ACTUALIZAR', '2017-08-07', '08:33:40', '181.64.26.246', '181.64.26.246'),
(394, 'Juan Calderón More', '31', 'CAIU 542117-5', '23518', 'ACTUALIZAR', '2017-08-07', '08:34:12', '181.64.26.246', '181.64.26.246'),
(395, 'Juan Calderón More', '31', 'SUDU 822107-5', '23513', 'ACTUALIZAR', '2017-08-07', '08:34:32', '181.64.26.246', '181.64.26.246'),
(396, 'Juan Calderón More', '31', 'SUDU 823497-7', '23512', 'ACTUALIZAR', '2017-08-07', '08:35:03', '181.64.26.246', '181.64.26.246'),
(397, 'Juan Calderón More', '31', 'MNBU 309738-8', '23508', 'ACTUALIZAR', '2017-08-07', '08:35:35', '181.64.26.246', '181.64.26.246'),
(398, 'Juan Calderón More', '31', 'CXRU 111430-1', '23509', 'ACTUALIZAR', '2017-08-07', '08:36:02', '181.64.26.246', '181.64.26.246'),
(399, 'Juan Calderón More', '31', '1', '1', 'ACTUALIZAR', '2017-08-07', '08:36:44', '181.64.26.246', '181.64.26.246'),
(400, 'Juan Calderón More', '31', 'SUDU 808059-4', '23510', 'ACTUALIZAR', '2017-08-07', '08:41:50', '181.64.26.246', '181.64.26.246'),
(401, 'Juan Calderón More', '31', 'SUDU 822107-5', '23511', 'ACTUALIZAR', '2017-08-07', '08:43:31', '181.64.26.246', '181.64.26.246'),
(402, 'Juan Calderón More', '31', 'MNBU 309738-8', '23508', 'ACTUALIZAR', '2017-08-07', '08:44:40', '181.64.26.246', '181.64.26.246'),
(403, 'Juan Calderón More', '31', 'SUDU 808279-2', '1', 'ACTUALIZAR', '2017-08-07', '08:51:35', '181.64.26.246', '181.64.26.246'),
(404, 'Juan Calderón More', '31', 'SUDU 822107-5', '23513', 'ACTUALIZAR', '2017-08-07', '08:51:57', '181.64.26.246', '181.64.26.246');
INSERT INTO `audit_contenedor` (`id`, `usuario_responsable`, `semana`, `numero_contenedor`, `referencia`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(405, 'Juan Calderón More', '31', 'SUDU 823041-5', '1', 'ACTUALIZAR', '2017-08-07', '08:54:07', '181.64.26.246', '181.64.26.246'),
(406, 'Juan Calderón More', '31', 'SUDU 808279-2', '1', 'ACTUALIZAR', '2017-08-07', '08:55:17', '181.64.26.246', '181.64.26.246'),
(407, 'Juan Calderón More', '31', 'HLXU 876094-8', '1', 'ACTUALIZAR', '2017-08-07', '08:58:31', '181.64.26.246', '181.64.26.246'),
(408, 'Juan Calderón More', '31', 'HLXU 875896-1', '1', 'ACTUALIZAR', '2017-08-07', '09:00:33', '181.64.26.246', '181.64.26.246'),
(409, 'Juan Calderón More', '31', 'HLXU 876069-7', '1', 'ACTUALIZAR', '2017-08-07', '09:02:13', '181.64.26.246', '181.64.26.246'),
(410, 'Juan Calderón More', '31', 'TLLU 105275-1', '1', 'ACTUALIZAR', '2017-08-07', '09:04:22', '181.64.26.246', '181.64.26.246'),
(411, 'Juan Calderón More', '31', 'CRLU 127214-9', '1', 'INSERTAR', '2017-08-07', '09:08:10', '181.64.26.246', '181.64.26.246'),
(412, 'Juan Calderón More', '31', 'HLXU 875124-7', '1', 'INSERTAR', '2017-08-07', '09:10:59', '181.64.26.246', '181.64.26.246'),
(413, 'Juan Calderón More', '32', '1', '23683', 'INSERTAR', '2017-08-10', '08:27:18', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(414, 'Juan Calderón More', '32', '1', '23684', 'INSERTAR', '2017-08-10', '08:28:46', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(415, 'Juan Calderón More', '32', '1', '23685', 'INSERTAR', '2017-08-10', '08:44:35', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(416, 'Juan Calderón More', '32', '1', '23686', 'INSERTAR', '2017-08-10', '09:33:58', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(417, 'Juan Calderón More', '32', '1', '23687', 'INSERTAR', '2017-08-10', '09:35:18', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(418, 'Juan Calderón More', '32', '1', '23688', 'INSERTAR', '2017-08-10', '09:37:17', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(419, 'Juan Calderón More', '32', '1', '23696', 'INSERTAR', '2017-08-10', '09:38:28', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(420, 'Juan Calderón More', '32', '1', '23737', 'INSERTAR', '2017-08-10', '09:39:37', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(421, 'Juan Calderón More', '32', '1', '23737', 'ACTUALIZAR', '2017-08-10', '09:40:14', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(422, 'Juan Calderón More', '32', '1', '23683', 'ACTUALIZAR', '2017-08-10', '09:40:34', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(423, 'Juan Calderón More', '32', '1', '23689', 'INSERTAR', '2017-08-10', '09:42:35', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(424, 'Juan Calderón More', '32', '1', '23690', 'INSERTAR', '2017-08-10', '09:43:37', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(425, 'Juan Calderón More', '32', '1', '23691', 'INSERTAR', '2017-08-10', '09:44:43', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(426, 'Juan Calderón More', '32', '1', '23692', 'INSERTAR', '2017-08-10', '09:46:52', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(427, 'Juan Calderón More', '32', '1', '1', 'INSERTAR', '2017-08-10', '10:03:16', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(428, 'Juan Calderón More', '32', '1', '2', 'INSERTAR', '2017-08-10', '10:04:25', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(429, 'Juan Calderón More', '32', '1', '1', 'INSERTAR', '2017-08-10', '10:06:01', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(430, 'Juan Calderón More', '32', '1', '1', 'INSERTAR', '2017-08-10', '10:07:14', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(431, 'Juan Calderón More', '32', '1', '1', 'INSERTAR', '2017-08-10', '10:08:38', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(432, 'Juan Calderón More', '32', '1', '1', 'INSERTAR', '2017-08-10', '10:10:31', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(433, 'Juan Calderón More', '32', '1', '1', 'INSERTAR', '2017-08-10', '10:11:22', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(434, 'Juan Calderón More', '32', 'CXRU 111139-1', '23683', 'ACTUALIZAR', '2017-08-14', '14:30:26', '190.237.236.58', '190.237.236.58'),
(435, 'Juan Calderón More', '32', '1', '1', 'ACTUALIZAR', '2017-08-14', '14:30:54', '190.237.236.58', '190.237.236.58'),
(436, 'Juan Calderón More', '32', 'MMAU 112342-6', '23692', 'ACTUALIZAR', '2017-08-14', '14:32:41', '190.237.236.58', '190.237.236.58'),
(437, 'Juan Calderón More', '32', 'MWCU 532834-3', '23691', 'ACTUALIZAR', '2017-08-14', '14:34:32', '190.237.236.58', '190.237.236.58'),
(438, 'Juan Calderón More', '32', 'MWCU 530648-9', '23690', 'ACTUALIZAR', '2017-08-14', '14:37:11', '190.237.236.58', '190.237.236.58'),
(439, 'Juan Calderón More', '32', 'MWCU 530648-9', '23690', 'ACTUALIZAR', '2017-08-14', '14:37:28', '190.237.236.58', '190.237.236.58'),
(440, 'Juan Calderón More', '32', 'MMAU 112342-6', '23692', 'ACTUALIZAR', '2017-08-14', '14:37:47', '190.237.236.58', '190.237.236.58'),
(441, 'Juan Calderón More', '32', 'MWCU 532834-3', '23691', 'ACTUALIZAR', '2017-08-14', '14:38:11', '190.237.236.58', '190.237.236.58'),
(442, 'Juan Calderón More', '32', 'CXRU 111139-1', '23683', 'ACTUALIZAR', '2017-08-14', '14:38:39', '190.237.236.58', '190.237.236.58'),
(443, 'Juan Calderón More', '32', 'SUDU 812231-8', '23687', 'ACTUALIZAR', '2017-08-14', '14:40:20', '190.237.236.58', '190.237.236.58'),
(444, 'Juan Calderón More', '32', 'TCLU 138554-3', '23684', 'ACTUALIZAR', '2017-08-14', '14:43:43', '190.237.236.58', '190.237.236.58'),
(445, 'Juan Calderón More', '32', 'SUDU 610246-2', '23696', 'ACTUALIZAR', '2017-08-14', '14:45:28', '190.237.236.58', '190.237.236.58'),
(446, 'Juan Calderón More', '32', '1', '1', 'ACTUALIZAR', '2017-08-14', '14:46:02', '190.237.236.58', '190.237.236.58'),
(447, 'Juan Calderón More', '32', 'SUDU 804382-0', '23688', 'ACTUALIZAR', '2017-08-14', '14:47:35', '190.237.236.58', '190.237.236.58'),
(448, 'Juan Calderón More', '32', 'CNIU 223155-9', '23685', 'ACTUALIZAR', '2017-08-14', '14:51:52', '190.237.236.58', '190.237.236.58'),
(449, 'Juan Calderón More', '32', 'SUDU 511341-5', '23737', 'ACTUALIZAR', '2017-08-14', '14:57:45', '190.237.236.58', '190.237.236.58'),
(450, 'Juan Calderón More', '32', 'SUDU 6282717-4', '23686', 'ACTUALIZAR', '2017-08-14', '14:59:30', '190.237.236.58', '190.237.236.58'),
(451, 'Juan Calderón More', '32', 'SUDU 610246-2', '23696', 'ACTUALIZAR', '2017-08-14', '15:00:12', '190.237.236.58', '190.237.236.58'),
(452, 'Juan Calderón More', '32', 'SUDU 6282717-4', '23686', 'ACTUALIZAR', '2017-08-14', '15:01:02', '190.237.236.58', '190.237.236.58'),
(453, 'Juan Calderón More', '32', 'PONU 480451-1', '23689', 'ACTUALIZAR', '2017-08-14', '15:02:50', '190.237.236.58', '190.237.236.58'),
(454, 'Juan Calderón More', '32', 'HLBU 900930-1', '1', 'ACTUALIZAR', '2017-08-14', '15:06:49', '190.237.236.58', '190.237.236.58'),
(455, 'Juan Calderón More', '32', 'HLBU 900318-1', '1', 'ACTUALIZAR', '2017-08-14', '15:12:02', '190.237.236.58', '190.237.236.58'),
(456, 'Juan Calderón More', '32', 'SEGU 937696-0', '1', 'ACTUALIZAR', '2017-08-14', '15:14:52', '190.237.236.58', '190.237.236.58'),
(457, 'Juan Calderón More', '32', 'LNXU 755955-7', '1', 'ACTUALIZAR', '2017-08-14', '15:17:14', '190.237.236.58', '190.237.236.58'),
(458, 'Juan Calderón More', '32', 'SUDU 602390-7', '1', 'ACTUALIZAR', '2017-08-14', '15:19:07', '190.237.236.58', '190.237.236.58'),
(459, 'Juan Calderón More', '32', 'SUDU 310908-6', '1', 'ACTUALIZAR', '2017-08-14', '15:21:17', '190.237.236.58', '190.237.236.58'),
(460, 'Juan Calderón More', '32', 'APRU 614231-2', '1', 'ACTUALIZAR', '2017-08-14', '15:22:49', '190.237.236.58', '190.237.236.58'),
(461, 'APPBOSA SAMAN SAMAN', '28', 'MSWU 900991-5', '23278', 'ACTUALIZAR', '2017-08-18', '14:27:46', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(462, 'APPBOSA SAMAN SAMAN', '28', 'MSWU 900991-5', '23278', 'ACTUALIZAR', '2017-08-18', '14:35:20', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(463, 'APPBOSA SAMAN SAMAN', '28', 'CAIU 565696-6', '23271', 'ACTUALIZAR', '2017-08-18', '14:36:20', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(464, 'APPBOSA SAMAN SAMAN', '28', 'MMAU 106056-5', '23280', 'ACTUALIZAR', '2017-08-18', '14:37:05', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(465, 'APPBOSA SAMAN SAMAN', '28', 'MWCU 683473-1', '23279', 'ACTUALIZAR', '2017-08-18', '14:37:37', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(466, 'APPBOSA SAMAN SAMAN', '28', 'MWCU 676530-6', '23277', 'ACTUALIZAR', '2017-08-18', '14:38:05', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(467, 'APPBOSA SAMAN SAMAN', '28', 'SUDU 821453-8', '23275', 'ACTUALIZAR', '2017-08-18', '14:38:54', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(468, 'APPBOSA SAMAN SAMAN', '28', 'SUDU 817614-5', '23274', 'ACTUALIZAR', '2017-08-18', '14:39:29', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(469, 'APPBOSA SAMAN SAMAN', '28', 'BMOU 965124-2', '23272', 'ACTUALIZAR', '2017-08-18', '14:40:11', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(470, 'APPBOSA SAMAN SAMAN', '28', 'SUDU 628821-2', '23317', 'ACTUALIZAR', '2017-08-18', '14:40:45', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(471, 'APPBOSA SAMAN SAMAN', '28', 'SUDU 803304-1', '23273', 'ACTUALIZAR', '2017-08-18', '14:41:13', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(472, 'APPBOSA SAMAN SAMAN', '28', 'HLBU 907719-0', '1', 'ACTUALIZAR', '2017-08-18', '14:43:48', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(473, 'APPBOSA SAMAN SAMAN', '28', 'TCLU 102065-3', '2', 'ACTUALIZAR', '2017-08-18', '14:44:34', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(474, 'APPBOSA SAMAN SAMAN', '28', 'HLBU 907720-3', '2', 'ACTUALIZAR', '2017-08-18', '14:45:15', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(475, 'APPBOSA SAMAN SAMAN', '28', 'TCLU 118219-2', '1', 'ACTUALIZAR', '2017-08-18', '15:49:47', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(476, 'APPBOSA SAMAN SAMAN', '28', 'TLLU 105065-6', '1', 'ACTUALIZAR', '2017-08-18', '15:50:17', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(477, 'APPBOSA SAMAN SAMAN', '28', 'SUDU 820004-6', '1', 'ACTUALIZAR', '2017-08-18', '15:51:21', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(478, 'APPBOSA SAMAN SAMAN', '28', 'SUDU 806707-8', '23276', 'ACTUALIZAR', '2017-08-18', '15:51:53', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(479, 'APPBOSA SAMAN SAMAN', '28', 'DFIU 811154-0', '2', 'ACTUALIZAR', '2017-08-18', '15:52:21', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(480, 'APPBOSA SAMAN SAMAN', '29', 'PONU 489829-6', '23348', 'ACTUALIZAR', '2017-08-21', '07:45:17', '190.238.17.74', '190.238.17.74'),
(481, 'APPBOSA SAMAN SAMAN', '29', 'PONU 489353-0', '23349', 'ACTUALIZAR', '2017-08-21', '07:45:37', '190.238.17.74', '190.238.17.74'),
(482, 'APPBOSA SAMAN SAMAN', '29', 'MMAU 110134-5', '23347', 'ACTUALIZAR', '2017-08-21', '07:45:55', '190.238.17.74', '190.238.17.74'),
(483, 'APPBOSA SAMAN SAMAN', '29', 'TTNU 845055-3', '23351', 'ACTUALIZAR', '2017-08-21', '07:47:22', '190.238.17.74', '190.238.17.74'),
(484, 'APPBOSA SAMAN SAMAN', '29', 'SUDU 624583-8', '23355', 'ACTUALIZAR', '2017-08-21', '07:48:16', '190.238.17.74', '190.238.17.74'),
(485, 'APPBOSA SAMAN SAMAN', '29', 'CXRU 152421-9', '23352', 'ACTUALIZAR', '2017-08-21', '07:50:53', '190.238.17.74', '190.238.17.74'),
(486, 'APPBOSA SAMAN SAMAN', '29', 'SUDU 804642-9', '23353', 'ACTUALIZAR', '2017-08-21', '07:54:23', '190.238.17.74', '190.238.17.74'),
(487, 'APPBOSA SAMAN SAMAN', '29', 'SUDU 803258-0', '23357', 'ACTUALIZAR', '2017-08-21', '07:57:09', '190.238.17.74', '190.238.17.74'),
(488, 'APPBOSA SAMAN SAMAN', '29', 'SUDU 627879-1', '23354', 'ACTUALIZAR', '2017-08-21', '07:57:58', '190.238.17.74', '190.238.17.74'),
(489, 'APPBOSA SAMAN SAMAN', '29', 'SUDU 627879-1', '23354', 'ACTUALIZAR', '2017-08-21', '07:58:30', '190.238.17.74', '190.238.17.74'),
(490, 'APPBOSA SAMAN SAMAN', '29', 'MWCU 530573-3', '23350', 'ACTUALIZAR', '2017-08-21', '07:59:27', '190.238.17.74', '190.238.17.74'),
(491, 'APPBOSA SAMAN SAMAN', '29', 'HLBU 907171-4', '3', 'ACTUALIZAR', '2017-08-21', '07:59:53', '190.238.17.74', '190.238.17.74'),
(492, 'APPBOSA SAMAN SAMAN', '29', 'HLBU 907963-3', '2', 'ACTUALIZAR', '2017-08-21', '08:00:41', '190.238.17.74', '190.238.17.74'),
(493, 'APPBOSA SAMAN SAMAN', '29', 'HLBU 904788-9', '1', 'ACTUALIZAR', '2017-08-21', '08:03:42', '190.238.17.74', '190.238.17.74'),
(494, 'APPBOSA SAMAN SAMAN', '29', 'CRLU 183918-7', '1', 'ACTUALIZAR', '2017-08-21', '08:04:18', '190.238.17.74', '190.238.17.74'),
(495, 'APPBOSA SAMAN SAMAN', '29', 'GESU 950621-0', '2', 'ACTUALIZAR', '2017-08-21', '08:05:57', '190.238.17.74', '190.238.17.74'),
(496, 'APPBOSA SAMAN SAMAN', '29', 'TLLU 104530-4', '1', 'ACTUALIZAR', '2017-08-21', '08:06:32', '190.238.17.74', '190.238.17.74'),
(497, 'APPBOSA SAMAN SAMAN', '29', 'SUDU 807093-4', '1', 'ACTUALIZAR', '2017-08-21', '08:07:03', '190.238.17.74', '190.238.17.74'),
(498, 'APPBOSA SAMAN SAMAN', '29', 'DFIU 722128-6', '2', 'ACTUALIZAR', '2017-08-21', '08:07:24', '190.238.17.74', '190.238.17.74'),
(499, 'APPBOSA SAMAN SAMAN', '30', 'MNBU 307774-0', '23422', 'ACTUALIZAR', '2017-08-21', '09:01:50', '190.238.17.74', '190.238.17.74'),
(500, 'APPBOSA SAMAN SAMAN', '30', 'PONU 489371-4', '23421', 'ACTUALIZAR', '2017-08-21', '09:02:17', '190.238.17.74', '190.238.17.74'),
(501, 'APPBOSA SAMAN SAMAN', '30', 'PONU 450426-3', '23423', 'ACTUALIZAR', '2017-08-21', '09:02:40', '190.238.17.74', '190.238.17.74'),
(502, 'APPBOSA SAMAN SAMAN', '30', 'BMOU 961803-3', '23414', 'ACTUALIZAR', '2017-08-21', '09:03:27', '190.238.17.74', '190.238.17.74'),
(503, 'APPBOSA SAMAN SAMAN', '30', 'MNBU 339827-3', '23420', 'ACTUALIZAR', '2017-08-21', '09:03:58', '190.238.17.74', '190.238.17.74'),
(504, 'APPBOSA SAMAN SAMAN', '30', 'MMAU 115229-7', '23424', 'ACTUALIZAR', '2017-08-21', '09:04:36', '190.238.17.74', '190.238.17.74'),
(505, 'APPBOSA SAMAN SAMAN', '30', 'BMOU 965241-8', '23415', 'ACTUALIZAR', '2017-08-21', '09:05:18', '190.238.17.74', '190.238.17.74'),
(506, 'APPBOSA SAMAN SAMAN', '30', 'SUDU 824675-1', '23419', 'ACTUALIZAR', '2017-08-21', '09:06:01', '190.238.17.74', '190.238.17.74'),
(507, 'APPBOSA SAMAN SAMAN', '30', 'SUDU 610980-5', '23416', 'ACTUALIZAR', '2017-08-21', '09:07:01', '190.238.17.74', '190.238.17.74'),
(508, 'APPBOSA SAMAN SAMAN', '30', 'SUDU 813990-1', '23418', 'ACTUALIZAR', '2017-08-21', '09:07:36', '190.238.17.74', '190.238.17.74'),
(509, 'APPBOSA SAMAN SAMAN', '30', 'SUDU 812206-7', '23417', 'ACTUALIZAR', '2017-08-21', '09:08:27', '190.238.17.74', '190.238.17.74'),
(510, 'APPBOSA SAMAN SAMAN', '30', 'FSCU 567276-9', '1', 'ACTUALIZAR', '2017-08-21', '09:10:30', '190.238.17.74', '190.238.17.74'),
(511, 'APPBOSA SAMAN SAMAN', '30', 'HLXU 877281-0', '2', 'ACTUALIZAR', '2017-08-21', '09:11:21', '190.238.17.74', '190.238.17.74'),
(512, 'APPBOSA SAMAN SAMAN', '30', 'HLBU 900879-5', '1', 'ACTUALIZAR', '2017-08-21', '09:14:38', '190.238.17.74', '190.238.17.74'),
(513, 'APPBOSA SAMAN SAMAN', '30', 'CAIU 565452-0', '1', 'ACTUALIZAR', '2017-08-21', '09:15:18', '190.238.17.74', '190.238.17.74'),
(514, 'Juan Calderón More', '33', 'MNBU 355141-7', '23752', 'INSERTAR', '2017-08-21', '09:17:45', '190.238.17.74', '190.238.17.74'),
(515, 'Juan Calderón More', '33', 'MNBU 901425-1', '23753', 'INSERTAR', '2017-08-21', '09:19:55', '190.238.17.74', '190.238.17.74'),
(516, 'APPBOSA SAMAN SAMAN', '30', 'LNXU 965478-5', '1', 'ACTUALIZAR', '2017-08-21', '09:21:37', '190.238.17.74', '190.238.17.74'),
(517, 'APPBOSA SAMAN SAMAN', '30', 'SUDU 813474-6', '1', 'ACTUALIZAR', '2017-08-21', '09:22:25', '190.238.17.74', '190.238.17.74'),
(518, 'Juan Calderón More', '33', 'PONU 484667-2', '23750', 'INSERTAR', '2017-08-21', '09:23:07', '190.238.17.74', '190.238.17.74'),
(519, 'Juan Calderón More', '33', 'PONU 451898-7', '23751', 'INSERTAR', '2017-08-21', '09:25:03', '190.238.17.74', '190.238.17.74'),
(520, 'Juan Calderón More', '33', 'PONU 484667-2', '23750', 'ACTUALIZAR', '2017-08-21', '09:25:50', '190.238.17.74', '190.238.17.74'),
(521, 'Juan Calderón More', '33', 'SUDU 601834-6', '23746', 'INSERTAR', '2017-08-21', '09:28:45', '190.238.17.74', '190.238.17.74'),
(522, 'Juan Calderón More', '33', 'MMAU 110979-4', '23754', 'INSERTAR', '2017-08-21', '09:31:40', '190.238.17.74', '190.238.17.74'),
(523, 'Juan Calderón More', '33', 'CXRU 127908-7', '23744', 'INSERTAR', '2017-08-21', '09:34:54', '190.238.17.74', '190.238.17.74'),
(524, 'Juan Calderón More', '33', 'RRSU 170259-5', '23745', 'INSERTAR', '2017-08-21', '09:37:21', '190.238.17.74', '190.238.17.74'),
(525, 'Juan Calderón More', '33', 'SUDU 514081-1', '23717', 'INSERTAR', '2017-08-21', '09:39:30', '190.238.17.74', '190.238.17.74'),
(526, 'Juan Calderón More', '33', 'SUDU 809810-3', '23748', 'INSERTAR', '2017-08-21', '10:10:04', '190.238.17.74', '190.238.17.74'),
(527, 'Juan Calderón More', '33', 'HLBU 900576-0', '1', 'INSERTAR', '2017-08-21', '10:23:02', '190.238.17.74', '190.238.17.74'),
(528, 'Juan Calderón More', '33', 'HLBU 876210-7', '1', 'INSERTAR', '2017-08-21', '10:26:03', '190.238.17.74', '190.238.17.74'),
(529, 'Juan Calderón More', '33', 'HLBU 900576-0', '1', 'ACTUALIZAR', '2017-08-21', '10:26:36', '190.238.17.74', '190.238.17.74'),
(530, 'Juan Calderón More', '33', 'TRIU 881644-1', '1', 'INSERTAR', '2017-08-21', '10:30:30', '190.238.17.74', '190.238.17.74'),
(531, 'Juan Calderón More', '33', 'TRLU 166544-0', '1', 'INSERTAR', '2017-08-21', '10:32:41', '190.238.17.74', '190.238.17.74'),
(532, 'Juan Calderón More', '33', 'SUDU 801041-0', '23749', 'INSERTAR', '2017-08-21', '10:35:23', '190.238.17.74', '190.238.17.74'),
(533, 'Juan Calderón More', '33', 'DFIU 812177-0', '1', 'INSERTAR', '2017-08-21', '10:46:51', '190.238.17.74', '190.238.17.74'),
(534, 'Juan Calderón More', '33', 'TRIU 865904-4', '1', 'INSERTAR', '2017-08-21', '10:49:10', '190.238.17.74', '190.238.17.74'),
(535, 'Juan Calderón More', '33', 'HLBU 900704-2', '1', 'INSERTAR', '2017-08-21', '10:53:43', '190.238.17.74', '190.238.17.74'),
(536, 'Juan Calderón More', '33', 'SUDU', '1', 'INSERTAR', '2017-08-21', '10:56:37', '190.238.17.74', '190.238.17.74'),
(537, 'Juan Calderón More', '33', 'SUDU 605321-8', '1', 'INSERTAR', '2017-08-21', '10:58:11', '190.238.17.74', '190.238.17.74'),
(538, 'Juan Calderón More', '31', 'CXRU 111430-1', '23509', 'ACTUALIZAR', '2017-08-23', '08:02:35', '190.233.185.137', '190.233.185.137'),
(539, 'APPBOSA SAMAN SAMAN', '33', 'MNBU 355141-7', '23752', 'ACTUALIZAR', '2017-08-24', '11:54:26', '190.42.208.254', '190.42.208.254'),
(540, 'APPBOSA SAMAN SAMAN', '33', 'MNBU 901425-1', '23753', 'ACTUALIZAR', '2017-08-24', '11:55:04', '190.42.208.254', '190.42.208.254'),
(541, 'APPBOSA SAMAN SAMAN', '33', 'PONU 484667-2', '23750', 'ACTUALIZAR', '2017-08-24', '11:57:11', '190.42.208.254', '190.42.208.254'),
(542, 'APPBOSA SAMAN SAMAN', '33', 'PONU 451898-7', '23751', 'ACTUALIZAR', '2017-08-24', '11:57:33', '190.42.208.254', '190.42.208.254'),
(543, 'APPBOSA SAMAN SAMAN', '33', 'SUDU 601834-6', '23746', 'ACTUALIZAR', '2017-08-24', '11:58:43', '190.42.208.254', '190.42.208.254'),
(544, 'APPBOSA SAMAN SAMAN', '33', 'MMAU 110979-4', '23754', 'ACTUALIZAR', '2017-08-24', '11:59:06', '190.42.208.254', '190.42.208.254'),
(545, 'APPBOSA SAMAN SAMAN', '33', 'SUDU 809810-3', '23748', 'ACTUALIZAR', '2017-08-24', '12:15:32', '190.42.208.254', '190.42.208.254'),
(546, 'APPBOSA SAMAN SAMAN', '33', 'SUDU 514081-1', '23717', 'ACTUALIZAR', '2017-08-24', '12:16:17', '190.42.208.254', '190.42.208.254'),
(547, 'APPBOSA SAMAN SAMAN', '33', 'RRSU 170259-5', '23745', 'ACTUALIZAR', '2017-08-24', '12:16:50', '190.42.208.254', '190.42.208.254'),
(548, 'APPBOSA SAMAN SAMAN', '33', 'CXRU 127908-7', '23744', 'ACTUALIZAR', '2017-08-24', '12:17:43', '190.42.208.254', '190.42.208.254'),
(549, 'APPBOSA SAMAN SAMAN', '33', 'TRLU 166544-0', '1', 'ACTUALIZAR', '2017-08-24', '12:18:26', '190.42.208.254', '190.42.208.254'),
(550, 'APPBOSA SAMAN SAMAN', '33', 'TRIU 881644-1', '1', 'ACTUALIZAR', '2017-08-24', '12:19:05', '190.42.208.254', '190.42.208.254'),
(551, 'APPBOSA SAMAN SAMAN', '33', 'HLXU 876210-7', '1', 'ACTUALIZAR', '2017-08-24', '12:19:43', '190.42.208.254', '190.42.208.254'),
(552, 'APPBOSA SAMAN SAMAN', '33', 'HLBU 900576-0', '1', 'ACTUALIZAR', '2017-08-24', '12:22:12', '190.42.208.254', '190.42.208.254'),
(553, 'APPBOSA SAMAN SAMAN', '33', 'SUDU 801041-0', '23749', 'ACTUALIZAR', '2017-08-24', '12:22:38', '190.42.208.254', '190.42.208.254'),
(554, 'APPBOSA SAMAN SAMAN', '33', 'DFIU 812177-0', '1', 'ACTUALIZAR', '2017-08-24', '12:23:08', '190.42.208.254', '190.42.208.254'),
(555, 'APPBOSA SAMAN SAMAN', '33', 'TRIU 865904-4', '1', 'ACTUALIZAR', '2017-08-24', '12:24:45', '190.42.208.254', '190.42.208.254'),
(556, 'APPBOSA SAMAN SAMAN', '33', 'HLBU 900704-2', '1', 'ACTUALIZAR', '2017-08-24', '12:25:11', '190.42.208.254', '190.42.208.254'),
(557, 'APPBOSA SAMAN SAMAN', '33', 'SUDU 602126-8', '1', 'ACTUALIZAR', '2017-08-24', '12:25:54', '190.42.208.254', '190.42.208.254'),
(558, 'APPBOSA SAMAN SAMAN', '33', 'SUDU 605321-8', '1', 'ACTUALIZAR', '2017-08-24', '12:30:17', '190.42.208.254', '190.42.208.254'),
(559, 'Juan Calderón More', '34', '1', '23839', 'INSERTAR', '2017-08-26', '09:26:15', '190.42.208.254', '190.42.208.254'),
(560, 'Juan Calderón More', '35', '1', '23963', 'INSERTAR', '2017-08-26', '09:29:30', '190.42.208.254', '190.42.208.254'),
(561, 'Juan Calderón More', '35', '1', '23966', 'INSERTAR', '2017-08-26', '09:30:48', '190.42.208.254', '190.42.208.254'),
(562, 'Juan Calderón More', '35', '1', '23967', 'INSERTAR', '2017-08-26', '09:32:07', '190.42.208.254', '190.42.208.254'),
(563, 'Juan Calderón More', '35', '1', '23968', 'INSERTAR', '2017-08-26', '09:35:32', '190.42.208.254', '190.42.208.254'),
(564, 'Juan Calderón More', '35', '1', '23969', 'INSERTAR', '2017-08-26', '09:39:26', '190.42.208.254', '190.42.208.254'),
(565, 'Juan Calderón More', '35', '1', '23970', 'INSERTAR', '2017-08-26', '10:00:19', '190.42.208.254', '190.42.208.254'),
(566, 'Juan Calderón More', '35', '1', '23971', 'INSERTAR', '2017-08-26', '10:01:32', '190.42.208.254', '190.42.208.254'),
(567, 'Juan Calderón More', '35', '1', '23972', 'INSERTAR', '2017-08-26', '10:03:47', '190.42.208.254', '190.42.208.254'),
(568, 'Juan Calderón More', '35', '1', '23980', 'INSERTAR', '2017-08-26', '10:05:49', '190.42.208.254', '190.42.208.254'),
(569, 'Juan Calderón More', '35', '1', '23973', 'INSERTAR', '2017-08-26', '10:09:50', '190.42.208.254', '190.42.208.254'),
(570, 'Juan Calderón More', '35', '1', '23974', 'INSERTAR', '2017-08-26', '10:11:27', '190.42.208.254', '190.42.208.254'),
(571, 'Juan Calderón More', '35', '1', '23975', 'INSERTAR', '2017-08-26', '10:12:25', '190.42.208.254', '190.42.208.254'),
(572, 'Juan Calderón More', '35', '1', '23976', 'INSERTAR', '2017-08-26', '10:13:46', '190.42.208.254', '190.42.208.254'),
(573, 'APPBOSA SAMAN SAMAN', '31', 'PONU 497285-5', '23515', 'ACTUALIZAR', '2017-08-26', '10:47:10', '190.42.208.254', '190.42.208.254'),
(574, 'APPBOSA SAMAN SAMAN', '31', 'MWMU 633834-6', '23516', 'ACTUALIZAR', '2017-08-26', '10:48:49', '190.42.208.254', '190.42.208.254'),
(575, 'APPBOSA SAMAN SAMAN', '31', 'MMAU 121545-6', '23519', 'ACTUALIZAR', '2017-08-26', '10:49:26', '190.42.208.254', '190.42.208.254'),
(576, 'APPBOSA SAMAN SAMAN', '31', 'MMAU 121545-6', '23519', 'ACTUALIZAR', '2017-08-26', '10:58:40', '190.42.208.254', '190.42.208.254'),
(577, 'APPBOSA SAMAN SAMAN', '31', 'CAIU 542117-5', '23518', 'ACTUALIZAR', '2017-08-26', '11:01:01', '190.42.208.254', '190.42.208.254'),
(578, 'APPBOSA SAMAN SAMAN', '31', 'MWCU 663807-1', '23517', 'ACTUALIZAR', '2017-08-26', '11:01:25', '190.42.208.254', '190.42.208.254'),
(579, 'APPBOSA SAMAN SAMAN', '31', 'SUDU 821297-8', '23513', 'ACTUALIZAR', '2017-08-26', '11:03:25', '190.42.208.254', '190.42.208.254'),
(580, 'APPBOSA SAMAN SAMAN', '31', 'MNBU 309738-8', '23508', 'ACTUALIZAR', '2017-08-26', '11:03:55', '190.42.208.254', '190.42.208.254'),
(581, 'APPBOSA SAMAN SAMAN', '31', 'MNBU 309738-8', '23508', 'ACTUALIZAR', '2017-08-26', '11:04:37', '190.42.208.254', '190.42.208.254'),
(582, 'APPBOSA SAMAN SAMAN', '31', 'SUDU 823497-7', '23512', 'ACTUALIZAR', '2017-08-26', '11:04:58', '190.42.208.254', '190.42.208.254'),
(583, 'APPBOSA SAMAN SAMAN', '31', 'SUDU 823497-7', '23512', 'ACTUALIZAR', '2017-08-26', '11:05:33', '190.42.208.254', '190.42.208.254'),
(584, 'APPBOSA SAMAN SAMAN', '31', 'CXRU 111430-1', '23509', 'ACTUALIZAR', '2017-08-26', '11:06:08', '190.42.208.254', '190.42.208.254'),
(585, 'APPBOSA SAMAN SAMAN', '31', 'HLXU 875896-1', '1', 'ACTUALIZAR', '2017-08-26', '11:06:55', '190.42.208.254', '190.42.208.254'),
(586, 'APPBOSA SAMAN SAMAN', '31', 'HLXU 876069-7', '1', 'ACTUALIZAR', '2017-08-26', '11:07:38', '190.42.208.254', '190.42.208.254'),
(587, 'APPBOSA SAMAN SAMAN', '31', 'SUDU 808059-4', '23510', 'ACTUALIZAR', '2017-08-26', '11:08:13', '190.42.208.254', '190.42.208.254'),
(588, 'APPBOSA SAMAN SAMAN', '31', 'SUDU 822107-5', '23511', 'ACTUALIZAR', '2017-08-26', '11:08:39', '190.42.208.254', '190.42.208.254'),
(589, 'APPBOSA SAMAN SAMAN', '31', 'TLLU 105275-1', '1', 'ACTUALIZAR', '2017-08-26', '11:09:17', '190.42.208.254', '190.42.208.254'),
(590, 'APPBOSA SAMAN SAMAN', '31', 'CRLU 127214-9', '1', 'ACTUALIZAR', '2017-08-26', '11:12:27', '190.42.208.254', '190.42.208.254'),
(591, 'APPBOSA SAMAN SAMAN', '31', 'HLXU 875124-7', '1', 'ACTUALIZAR', '2017-08-26', '11:47:22', '190.42.208.254', '190.42.208.254'),
(592, 'APPBOSA SAMAN SAMAN', '31', 'SUDU 823041-5', '1', 'ACTUALIZAR', '2017-08-26', '11:48:01', '190.42.208.254', '190.42.208.254'),
(593, 'APPBOSA SAMAN SAMAN', '31', 'SUDU 808279-2', '1', 'ACTUALIZAR', '2017-08-26', '11:48:37', '190.42.208.254', '190.42.208.254'),
(594, 'APPBOSA SAMAN SAMAN', '31', 'HLXU 876094-8', '1', 'ACTUALIZAR', '2017-08-26', '11:57:52', '190.42.208.254', '190.42.208.254'),
(595, 'APPBOSA SAMAN SAMAN', '31', 'HLXU 876094-8', '1', 'ACTUALIZAR', '2017-08-26', '12:15:43', '190.42.208.254', '190.42.208.254'),
(596, 'APPBOSA SAMAN SAMAN', '31', 'HLXU 876094-8', '1', 'ACTUALIZAR', '2017-08-26', '12:15:59', '190.42.208.254', '190.42.208.254'),
(597, 'Juan Calderón More', '34', 'TGHU991684-0', '23839', 'ACTUALIZAR', '2017-08-28', '09:47:03', '190.40.249.208', '190.40.249.208'),
(598, 'Juan Calderón More', '34', 'PONU 494585-0', '23849', 'INSERTAR', '2017-08-28', '09:50:18', '190.40.249.208', '190.40.249.208'),
(599, 'Juan Calderón More', '34', 'MNBU 331586-0', '23850', 'INSERTAR', '2017-08-28', '10:55:38', '190.40.249.208', '190.40.249.208'),
(600, 'Juan Calderón More', '34', 'MSWU 006006-9', '23848', 'INSERTAR', '2017-08-28', '11:18:32', '190.40.249.208', '190.40.249.208'),
(601, 'Juan Calderón More', '34', 'ATKU 411267-8', '23840', 'INSERTAR', '2017-08-28', '11:43:20', '190.40.249.208', '190.40.249.208'),
(602, 'Juan Calderón More', '34', 'SZLU 913378-0', '23841', 'INSERTAR', '2017-08-28', '11:50:33', '190.40.249.208', '190.40.249.208'),
(603, 'Juan Calderón More', '34', 'SUDU 612457-0', '23842', 'INSERTAR', '2017-08-28', '11:53:36', '190.40.249.208', '190.40.249.208'),
(604, 'Juan Calderón More', '34', 'SUDU 605195-6', '23843', 'INSERTAR', '2017-08-28', '11:57:21', '190.40.249.208', '190.40.249.208'),
(605, 'Juan Calderón More', '34', 'SUDU 819760-0', '23847', 'INSERTAR', '2017-08-28', '12:00:27', '190.40.249.208', '190.40.249.208'),
(606, 'Juan Calderón More', '34', 'SUDU 817353-1', '23846', 'INSERTAR', '2017-08-28', '12:03:40', '190.40.249.208', '190.40.249.208'),
(607, 'Juan Calderón More', '34', 'CPSU 516640-3', '1', 'INSERTAR', '2017-08-28', '12:06:48', '190.40.249.208', '190.40.249.208'),
(608, 'Juan Calderón More', '34', 'HLXU 673459-6', '1', 'INSERTAR', '2017-08-28', '12:08:51', '190.40.249.208', '190.40.249.208'),
(609, 'Juan Calderón More', '34', 'CRLU 721320-4', '1', 'INSERTAR', '2017-08-28', '12:20:08', '190.40.249.208', '190.40.249.208'),
(610, 'Juan Calderón More', '34', 'TCLU 118605-3', '1', 'INSERTAR', '2017-08-28', '12:22:59', '190.40.249.208', '190.40.249.208'),
(611, 'Juan Calderón More', '34', 'SUDU 515015-2', '23845', 'INSERTAR', '2017-08-28', '12:26:48', '190.40.249.208', '190.40.249.208'),
(612, 'Juan Calderón More', '34', 'CPSU 512120-3', '1', 'INSERTAR', '2017-08-28', '12:28:24', '190.40.249.208', '190.40.249.208'),
(613, 'Juan Calderón More', '34', 'TRIU 897756-0', '1', 'INSERTAR', '2017-08-28', '12:40:43', '190.40.249.208', '190.40.249.208'),
(614, 'Juan Calderón More', '34', 'SUDU 519274-9', '23844', 'INSERTAR', '2017-08-28', '12:43:22', '190.40.249.208', '190.40.249.208'),
(615, 'Juan Calderón More', '34', 'SUDU 620400-0', '1', 'INSERTAR', '2017-08-28', '12:45:59', '190.40.249.208', '190.40.249.208'),
(616, 'Juan Calderón More', '34', 'SUDU 623091-0', '1', 'INSERTAR', '2017-08-28', '12:47:03', '190.40.249.208', '190.40.249.208'),
(617, 'Juan Calderón More', '34', 'MNBU 331586-0', '23850', 'ACTUALIZAR', '2017-08-29', '14:22:00', '190.40.249.208', '190.40.249.208'),
(618, 'APPBOSA SAMAN SAMAN', '34', 'PONU 494585-0', '23849', 'ACTUALIZAR', '2017-08-29', '14:47:19', '190.40.249.208', '190.40.249.208'),
(619, 'APPBOSA SAMAN SAMAN', '34', 'MNBU 331586-0', '23850', 'ACTUALIZAR', '2017-08-29', '14:47:54', '190.40.249.208', '190.40.249.208'),
(620, 'APPBOSA SAMAN SAMAN', '34', 'PONU 494585-0', '23849', 'ACTUALIZAR', '2017-08-29', '14:48:09', '190.40.249.208', '190.40.249.208'),
(621, 'APPBOSA SAMAN SAMAN', '34', 'MSWU 006006-9', '23848', 'ACTUALIZAR', '2017-08-29', '14:48:25', '190.40.249.208', '190.40.249.208'),
(622, 'APPBOSA SAMAN SAMAN', '34', 'TGHU991684-0', '23839', 'ACTUALIZAR', '2017-08-29', '14:48:52', '190.40.249.208', '190.40.249.208'),
(623, 'APPBOSA SAMAN SAMAN', '34', 'ATKU 411267-8', '23840', 'ACTUALIZAR', '2017-08-29', '14:49:15', '190.40.249.208', '190.40.249.208'),
(624, 'APPBOSA SAMAN SAMAN', '34', 'SZLU 913378-0', '23841', 'ACTUALIZAR', '2017-08-29', '14:49:36', '190.40.249.208', '190.40.249.208'),
(625, 'APPBOSA SAMAN SAMAN', '34', 'SUDU 817353-1', '23846', 'ACTUALIZAR', '2017-08-29', '14:50:00', '190.40.249.208', '190.40.249.208'),
(626, 'APPBOSA SAMAN SAMAN', '34', 'SUDU 819760-0', '23847', 'ACTUALIZAR', '2017-08-29', '14:50:25', '190.40.249.208', '190.40.249.208'),
(627, 'APPBOSA SAMAN SAMAN', '34', 'SUDU 605195-6', '23843', 'ACTUALIZAR', '2017-08-29', '14:50:44', '190.40.249.208', '190.40.249.208'),
(628, 'APPBOSA SAMAN SAMAN', '34', 'SUDU 612457-0', '23842', 'ACTUALIZAR', '2017-08-29', '14:51:04', '190.40.249.208', '190.40.249.208'),
(629, 'APPBOSA SAMAN SAMAN', '34', 'TCLU 118605-3', '1', 'ACTUALIZAR', '2017-08-29', '14:51:28', '190.40.249.208', '190.40.249.208'),
(630, 'APPBOSA SAMAN SAMAN', '34', 'CRLU 721320-4', '1', 'ACTUALIZAR', '2017-08-29', '14:51:54', '190.40.249.208', '190.40.249.208'),
(631, 'APPBOSA SAMAN SAMAN', '34', 'HLXU 673459-6', '1', 'ACTUALIZAR', '2017-08-29', '14:52:24', '190.40.249.208', '190.40.249.208'),
(632, 'APPBOSA SAMAN SAMAN', '34', 'CPSU 516640-3', '1', 'ACTUALIZAR', '2017-08-29', '14:52:55', '190.40.249.208', '190.40.249.208'),
(633, 'APPBOSA SAMAN SAMAN', '34', 'SUDU 515015-2', '23845', 'ACTUALIZAR', '2017-08-29', '14:53:22', '190.40.249.208', '190.40.249.208'),
(634, 'APPBOSA SAMAN SAMAN', '34', 'CPSU 512120-3', '1', 'ACTUALIZAR', '2017-08-29', '14:53:50', '190.40.249.208', '190.40.249.208'),
(635, 'APPBOSA SAMAN SAMAN', '34', 'TRIU 897756-0', '1', 'ACTUALIZAR', '2017-08-29', '14:54:26', '190.40.249.208', '190.40.249.208'),
(636, 'APPBOSA SAMAN SAMAN', '34', 'SUDU 519274-9', '23844', 'ACTUALIZAR', '2017-08-29', '14:54:51', '190.40.249.208', '190.40.249.208'),
(637, 'APPBOSA SAMAN SAMAN', '34', 'SUDU 620400-0', '1', 'ACTUALIZAR', '2017-08-29', '14:55:10', '190.40.249.208', '190.40.249.208'),
(638, 'APPBOSA SAMAN SAMAN', '34', 'SUDU 623091-0', '1', 'ACTUALIZAR', '2017-08-29', '14:55:52', '190.40.249.208', '190.40.249.208'),
(639, 'Juan Calderón More', '36', '1', '24037', 'INSERTAR', '2017-09-01', '12:44:33', '190.239.255.203', '190.239.255.203'),
(640, 'Juan Calderón More', '36', '1', '24037', 'ACTUALIZAR', '2017-09-01', '12:45:08', '190.239.255.203', '190.239.255.203'),
(641, 'Juan Calderón More', '36', '1', '24038', 'INSERTAR', '2017-09-01', '12:46:12', '190.239.255.203', '190.239.255.203'),
(642, 'Juan Calderón More', '36', '1', '24039', 'INSERTAR', '2017-09-01', '12:47:08', '190.239.255.203', '190.239.255.203'),
(643, 'Juan Calderón More', '36', '|', '24040', 'INSERTAR', '2017-09-01', '12:48:12', '190.239.255.203', '190.239.255.203'),
(644, 'Juan Calderón More', '36', '1', '24041', 'INSERTAR', '2017-09-01', '12:50:22', '190.239.255.203', '190.239.255.203'),
(645, 'Juan Calderón More', '36', '|', '24040', 'ACTUALIZAR', '2017-09-01', '12:50:48', '190.239.255.203', '190.239.255.203'),
(646, 'Juan Calderón More', '36', '1', '24042', 'INSERTAR', '2017-09-01', '12:52:27', '190.239.255.203', '190.239.255.203'),
(647, 'Juan Calderón More', '36', '1', '24102', 'INSERTAR', '2017-09-01', '12:55:04', '190.239.255.203', '190.239.255.203'),
(648, 'Juan Calderón More', '36', '1', '24103', 'INSERTAR', '2017-09-01', '12:57:09', '190.239.255.203', '190.239.255.203'),
(649, 'Juan Calderón More', '36', '1', '24043', 'INSERTAR', '2017-09-01', '13:02:48', '190.239.255.203', '190.239.255.203'),
(650, 'Juan Calderón More', '36', '1', '24044', 'INSERTAR', '2017-09-01', '13:04:10', '190.239.255.203', '190.239.255.203'),
(651, 'Juan Calderón More', '36', '1', '24045', 'INSERTAR', '2017-09-01', '13:05:25', '190.239.255.203', '190.239.255.203'),
(652, 'Juan Calderón More', '36', '1', '24046', 'INSERTAR', '2017-09-01', '13:06:13', '190.239.255.203', '190.239.255.203'),
(653, 'Juan Calderón More', '36', '1', '24047', 'INSERTAR', '2017-09-01', '13:07:27', '190.239.255.203', '190.239.255.203'),
(654, 'Juan Calderón More', '36', '1', '1', 'INSERTAR', '2017-09-01', '14:46:17', '190.239.255.203', '190.239.255.203'),
(655, 'Juan Calderón More', '36', '1', '1', 'INSERTAR', '2017-09-01', '14:47:26', '190.239.255.203', '190.239.255.203'),
(656, 'Juan Calderón More', '36', '1', '1', 'ACTUALIZAR', '2017-09-01', '14:48:24', '190.239.255.203', '190.239.255.203'),
(657, 'Juan Calderón More', '36', '1', '1', 'INSERTAR', '2017-09-01', '14:49:21', '190.239.255.203', '190.239.255.203'),
(658, 'Juan Calderón More', '36', '1', '1', 'INSERTAR', '2017-09-01', '14:50:12', '190.239.255.203', '190.239.255.203'),
(659, 'Juan Calderón More', '36', '1', '1', 'INSERTAR', '2017-09-01', '14:51:20', '190.239.255.203', '190.239.255.203'),
(660, 'Juan Calderón More', '36', '1', '1', 'INSERTAR', '2017-09-01', '14:52:01', '190.239.255.203', '190.239.255.203'),
(661, 'Juan Calderón More', '36', '1', '1', 'INSERTAR', '2017-09-01', '14:53:16', '190.239.255.203', '190.239.255.203'),
(662, 'Juan Calderón More', '36', '1', '1', 'INSERTAR', '2017-09-01', '14:54:04', '190.239.255.203', '190.239.255.203'),
(663, 'APPBOSA SAMAN SAMAN', '35', '1', '23967', 'ACTUALIZAR', '2017-09-02', '10:38:27', '181.176.82.110', '181.176.82.110'),
(664, 'APPBOSA SAMAN SAMAN', '6', 'TEMU 943002-9', '1', 'INSERTAR', '2017-09-02', '11:54:33', '181.64.26.88', '181.64.26.88'),
(665, 'APPBOSA SAMAN SAMAN', '6', 'GESU 938471-9', '1', 'INSERTAR', '2017-09-02', '11:57:33', '181.64.26.88', '181.64.26.88'),
(666, 'APPBOSA SAMAN SAMAN', '12', 'DFIU 260060-4', '1', 'INSERTAR', '2017-09-03', '08:35:12', '181.64.26.88', '181.64.26.88'),
(667, 'APPBOSA SAMAN SAMAN', '21', 'HLXU 876009-0', '1', 'ACTUALIZAR', '2017-09-03', '09:46:34', '181.64.26.88', '181.64.26.88'),
(668, 'APPBOSA SAMAN SAMAN', '21', 'HLBU 900451-0', '2', 'ACTUALIZAR', '2017-09-03', '09:47:12', '181.64.26.88', '181.64.26.88'),
(669, 'APPBOSA SAMAN SAMAN', '22', 'TCLU 138551-7', '22687', 'ACTUALIZAR', '2017-09-03', '09:55:11', '181.64.26.88', '181.64.26.88'),
(670, 'APPBOSA SAMAN SAMAN', '23', 'HLXU 877275-9', '1', 'ACTUALIZAR', '2017-09-03', '10:04:13', '181.64.26.88', '181.64.26.88'),
(671, 'APPBOSA SAMAN SAMAN', '30', 'HLXU 877281-0', '2', 'ACTUALIZAR', '2017-09-03', '10:41:04', '181.64.26.88', '181.64.26.88'),
(672, 'APPBOSA SAMAN SAMAN', '31', 'HLXU 875896-1', '1', 'ACTUALIZAR', '2017-09-03', '10:45:41', '181.64.26.88', '181.64.26.88'),
(673, 'APPBOSA SAMAN SAMAN', '31', 'HLXU 876069-7', '1', 'ACTUALIZAR', '2017-09-03', '10:46:02', '181.64.26.88', '181.64.26.88'),
(674, 'APPBOSA SAMAN SAMAN', '32', 'HLBU 900930-1', '1', 'ACTUALIZAR', '2017-09-03', '10:54:54', '181.64.26.88', '181.64.26.88'),
(675, 'APPBOSA SAMAN SAMAN', '33', 'HLXU 876210-7', '1', 'ACTUALIZAR', '2017-09-03', '11:11:54', '181.64.26.88', '181.64.26.88'),
(676, 'APPBOSA SAMAN SAMAN', '33', 'HLBU 900576-0', '1', 'ACTUALIZAR', '2017-09-03', '11:13:27', '181.64.26.88', '181.64.26.88'),
(677, 'Juan Calderón More', '35', '1', '23963', 'ACTUALIZAR', '2017-09-04', '08:05:08', '181.64.216.195', '181.64.216.195'),
(678, 'Juan Calderón More', '35', 'CXRU 111338-9', '23963', 'ACTUALIZAR', '2017-09-04', '09:01:41', '181.64.216.195', '181.64.216.195'),
(679, 'Juan Calderón More', '35', 'TRIU 859121-6', '23966', 'ACTUALIZAR', '2017-09-04', '09:04:09', '181.64.216.195', '181.64.216.195'),
(680, 'Juan Calderón More', '35', 'CAIU 542161-6', '23967', 'ACTUALIZAR', '2017-09-04', '09:06:10', '181.64.216.195', '181.64.216.195'),
(681, 'Juan Calderón More', '35', 'SUDU 627006-5', '23968', 'ACTUALIZAR', '2017-09-04', '09:11:40', '181.64.216.195', '181.64.216.195'),
(682, 'Juan Calderón More', '35', 'SUDU 628288-9', '23969', 'ACTUALIZAR', '2017-09-04', '09:13:55', '181.64.216.195', '181.64.216.195'),
(683, 'Juan Calderón More', '35', 'MMAU 111331-0', '23976', 'ACTUALIZAR', '2017-09-04', '09:16:21', '181.64.216.195', '181.64.216.195'),
(684, 'APPBOSA SAMAN SAMAN', '1', 'MNBU 900997-5', '20386', 'ACTUALIZAR', '2017-09-04', '09:25:50', '181.64.216.195', '181.64.216.195'),
(685, 'Juan Calderón More', '35', 'MWCU 523233-9', '23975', 'ACTUALIZAR', '2017-09-04', '09:27:22', '181.64.216.195', '181.64.216.195'),
(686, 'Juan Calderón More', '35', 'MNBU 365450-2', '23974', 'ACTUALIZAR', '2017-09-04', '09:34:55', '181.64.216.195', '181.64.216.195'),
(687, 'Juan Calderón More', '35', 'PONU 483751-5', '23973', 'ACTUALIZAR', '2017-09-04', '09:36:31', '181.64.216.195', '181.64.216.195'),
(688, 'Juan Calderón More', '35', 'SUDU 608761-9', '23980', 'ACTUALIZAR', '2017-09-04', '09:38:50', '181.64.216.195', '181.64.216.195'),
(689, 'Juan Calderón More', '35', 'SUDU 608150-2', '23972', 'ACTUALIZAR', '2017-09-04', '09:40:46', '181.64.216.195', '181.64.216.195'),
(690, 'Juan Calderón More', '35', 'SUDU 621344-5', '23971', 'ACTUALIZAR', '2017-09-04', '09:42:08', '181.64.216.195', '181.64.216.195'),
(691, 'Juan Calderón More', '35', 'SUDU 615306-9', '23970', 'ACTUALIZAR', '2017-09-04', '09:44:13', '181.64.216.195', '181.64.216.195'),
(692, 'APPBOSA SAMAN SAMAN', '1', 'MNBU 900997-5', '20386', 'ACTUALIZAR', '2017-09-04', '09:48:21', '181.64.216.195', '181.64.216.195'),
(693, 'Juan Calderón More', '35', 'TLLU 105927-3', '1', 'INSERTAR', '2017-09-04', '09:49:55', '181.64.216.195', '181.64.216.195'),
(694, 'Juan Calderón More', '35', 'TLLU 105716-2', '1', 'INSERTAR', '2017-09-04', '09:52:35', '181.64.216.195', '181.64.216.195'),
(695, 'APPBOSA SAMAN SAMAN', '1', 'MNBU 903004-4', '20387', 'ACTUALIZAR', '2017-09-04', '09:53:07', '181.64.216.195', '181.64.216.195'),
(696, 'Juan Calderón More', '35', 'CRLU 130221-7', '1', 'INSERTAR', '2017-09-04', '09:56:32', '181.64.216.195', '181.64.216.195'),
(697, 'APPBOSA SAMAN SAMAN', '1', 'MNBU 903004-4', '20387', 'ACTUALIZAR', '2017-09-04', '09:58:08', '181.64.216.195', '181.64.216.195'),
(698, 'Juan Calderón More', '35', 'CRLU 150303-7', '1', 'INSERTAR', '2017-09-04', '09:58:43', '181.64.216.195', '181.64.216.195'),
(699, 'APPBOSA SAMAN SAMAN', '1', 'MNBU 903004-4', '20387', 'ACTUALIZAR', '2017-09-04', '09:59:15', '181.64.216.195', '181.64.216.195'),
(700, 'Juan Calderón More', '35', 'CRLU 126164-8', '1', 'INSERTAR', '2017-09-04', '10:02:26', '181.64.216.195', '181.64.216.195'),
(701, 'APPBOSA SAMAN SAMAN', '1', 'MNBU 360791', '20380', 'ACTUALIZAR', '2017-09-04', '10:04:06', '181.64.216.195', '181.64.216.195'),
(702, 'Juan Calderón More', '35', 'HLXU 875617-2', '1', 'INSERTAR', '2017-09-04', '10:04:28', '181.64.216.195', '181.64.216.195'),
(703, 'APPBOSA SAMAN SAMAN', '1', 'MNBU 903004-4', '20387', 'ACTUALIZAR', '2017-09-04', '10:04:30', '181.64.216.195', '181.64.216.195'),
(704, 'Juan Calderón More', '35', 'SUDU 629572-0', '1', 'INSERTAR', '2017-09-04', '10:06:39', '181.64.216.195', '181.64.216.195'),
(705, 'APPBOSA SAMAN SAMAN', '1', 'MNWU 005645-4', '20381', 'ACTUALIZAR', '2017-09-04', '10:07:21', '181.64.216.195', '181.64.216.195'),
(706, 'Juan Calderón More', '35', 'SUDU 620492-6', '1', 'INSERTAR', '2017-09-04', '10:09:08', '181.64.216.195', '181.64.216.195'),
(707, 'APPBOSA SAMAN SAMAN', '1', 'MWCU 670330-4', '20382', 'ACTUALIZAR', '2017-09-04', '10:13:34', '181.64.216.195', '181.64.216.195'),
(708, 'APPBOSA SAMAN SAMAN', '1', 'MWCU 670330-4', '20382', 'ACTUALIZAR', '2017-09-04', '10:15:11', '181.64.216.195', '181.64.216.195'),
(709, 'APPBOSA SAMAN SAMAN', '1', 'MNBU 307581-4', '20383', 'ACTUALIZAR', '2017-09-04', '10:17:33', '181.64.216.195', '181.64.216.195'),
(710, 'APPBOSA SAMAN SAMAN', '1', 'MWCU 682680-2', '20384', 'ACTUALIZAR', '2017-09-04', '10:19:49', '181.64.216.195', '181.64.216.195'),
(711, 'APPBOSA SAMAN SAMAN', '1', 'MNWU 005645-4', '20381', 'ACTUALIZAR', '2017-09-04', '10:21:02', '181.64.216.195', '181.64.216.195'),
(712, 'APPBOSA SAMAN SAMAN', '1', 'MWCU 682680-2', '20384', 'ACTUALIZAR', '2017-09-04', '10:22:50', '181.64.216.195', '181.64.216.195'),
(713, 'APPBOSA SAMAN SAMAN', '1', 'MWCU 694362-4', '20385', 'ACTUALIZAR', '2017-09-04', '10:24:59', '181.64.216.195', '181.64.216.195'),
(714, 'APPBOSA SAMAN SAMAN', '1', 'MWCU 694362-4', '20385', 'ACTUALIZAR', '2017-09-04', '10:27:28', '181.64.216.195', '181.64.216.195'),
(715, 'APPBOSA SAMAN SAMAN', '1', 'TRIU 8591140-0', '20374', 'ACTUALIZAR', '2017-09-04', '10:32:29', '181.64.216.195', '181.64.216.195'),
(716, 'APPBOSA SAMAN SAMAN', '1', 'CXRU 110968-7', '20376', 'ACTUALIZAR', '2017-09-04', '10:35:00', '181.64.216.195', '181.64.216.195'),
(717, 'APPBOSA SAMAN SAMAN', '1', 'TRIU 8591140-0', '20374', 'ACTUALIZAR', '2017-09-04', '10:35:26', '181.64.216.195', '181.64.216.195'),
(718, 'APPBOSA SAMAN SAMAN', '1', 'SIDU 625910-6', '20377', 'ACTUALIZAR', '2017-09-04', '10:39:12', '181.64.216.195', '181.64.216.195'),
(719, 'APPBOSA SAMAN SAMAN', '1', 'SUDU 615755-2', '20378', 'ACTUALIZAR', '2017-09-04', '10:41:49', '181.64.216.195', '181.64.216.195'),
(720, 'APPBOSA SAMAN SAMAN', '1', 'SUDU 808629-4', '20379', 'ACTUALIZAR', '2017-09-04', '10:44:10', '181.64.216.195', '181.64.216.195'),
(721, 'APPBOSA SAMAN SAMAN', '1', 'CRLU 725353-1', '1', 'ACTUALIZAR', '2017-09-04', '10:48:59', '181.64.216.195', '181.64.216.195'),
(722, 'APPBOSA SAMAN SAMAN', '1', 'CRLU 721799-8', '2', 'ACTUALIZAR', '2017-09-04', '10:51:44', '181.64.216.195', '181.64.216.195'),
(723, 'APPBOSA SAMAN SAMAN', '1', 'TRIU 834400-5', '3', 'ACTUALIZAR', '2017-09-04', '10:55:15', '181.64.216.195', '181.64.216.195'),
(724, 'APPBOSA SAMAN SAMAN', '1', 'TEMU 919858-8', '1', 'ACTUALIZAR', '2017-09-04', '10:58:20', '181.64.216.195', '181.64.216.195'),
(725, 'APPBOSA SAMAN SAMAN', '1', 'SEGU 910535-7', '4', 'ACTUALIZAR', '2017-09-04', '11:02:28', '181.64.216.195', '181.64.216.195'),
(726, 'APPBOSA SAMAN SAMAN', '1', 'SEGU 942085-2', '1', 'ACTUALIZAR', '2017-09-04', '11:07:43', '181.64.216.195', '181.64.216.195'),
(727, 'APPBOSA SAMAN SAMAN', '1', 'SEGU 935356-4', '2', 'ACTUALIZAR', '2017-09-04', '11:10:22', '181.64.216.195', '181.64.216.195'),
(728, 'APPBOSA SAMAN SAMAN', '1', 'DTPU 2115-2', '1', 'ACTUALIZAR', '2017-09-04', '11:12:38', '181.64.216.195', '181.64.216.195'),
(729, 'APPBOSA SAMAN SAMAN', '1', 'DTPU 213315-2', '2', 'ACTUALIZAR', '2017-09-04', '11:14:14', '181.64.216.195', '181.64.216.195'),
(730, 'APPBOSA SAMAN SAMAN', '2', 'PONU 494233-6', '20466', 'ACTUALIZAR', '2017-09-04', '11:19:23', '181.64.216.195', '181.64.216.195'),
(731, 'APPBOSA SAMAN SAMAN', '2', 'MNBU 305438-6', '20467', 'ACTUALIZAR', '2017-09-04', '11:21:43', '181.64.216.195', '181.64.216.195'),
(732, 'APPBOSA SAMAN SAMAN', '2', 'PONU 494233-6', '20466', 'ACTUALIZAR', '2017-09-04', '11:22:27', '181.64.216.195', '181.64.216.195'),
(733, 'APPBOSA SAMAN SAMAN', '2', 'MNBU 305438-6', '20467', 'ACTUALIZAR', '2017-09-04', '11:22:40', '181.64.216.195', '181.64.216.195'),
(734, 'APPBOSA SAMAN SAMAN', '2', 'MWCU 629931-6', '20473', 'ACTUALIZAR', '2017-09-04', '11:24:36', '181.64.216.195', '181.64.216.195'),
(735, 'APPBOSA SAMAN SAMAN', '2', 'PONE 499461-7', '20473', 'ACTUALIZAR', '2017-09-04', '11:33:41', '181.64.216.195', '181.64.216.195'),
(736, 'APPBOSA SAMAN SAMAN', '2', 'MNBU 345494-7', '20472', 'ACTUALIZAR', '2017-09-04', '11:35:15', '181.64.216.195', '181.64.216.195'),
(737, 'APPBOSA SAMAN SAMAN', '2', 'MWCU 629931-6', '20471', 'ACTUALIZAR', '2017-09-04', '11:38:41', '181.64.216.195', '181.64.216.195'),
(738, 'APPBOSA SAMAN SAMAN', '2', 'MNBU 309499-0', '20474', 'ACTUALIZAR', '2017-09-04', '11:42:41', '181.64.216.195', '181.64.216.195'),
(739, 'APPBOSA SAMAN SAMAN', '2', 'MNBU 313510-6', '20470', 'ACTUALIZAR', '2017-09-04', '11:44:00', '181.64.216.195', '181.64.216.195'),
(740, 'APPBOSA SAMAN SAMAN', '2', 'MNBU 337325-2', '20469', 'ACTUALIZAR', '2017-09-04', '11:45:05', '181.64.216.195', '181.64.216.195'),
(741, 'APPBOSA SAMAN SAMAN', '2', 'MSWU 905159-8', '20468', 'ACTUALIZAR', '2017-09-04', '11:46:35', '181.64.216.195', '181.64.216.195'),
(742, 'APPBOSA SAMAN SAMAN', '2', 'TTNU 895542-1', '20466', 'ACTUALIZAR', '2017-09-04', '11:49:06', '181.64.216.195', '181.64.216.195'),
(743, 'APPBOSA SAMAN SAMAN', '2', 'TTNU 895541-6', '20463', 'ACTUALIZAR', '2017-09-04', '11:51:15', '181.64.216.195', '181.64.216.195'),
(744, 'APPBOSA SAMAN SAMAN', '2', 'TTNU 896066-5', '20460', 'ACTUALIZAR', '2017-09-04', '11:52:51', '181.64.216.195', '181.64.216.195'),
(745, 'APPBOSA SAMAN SAMAN', '2', 'CXRU 153754-0', '20461', 'ACTUALIZAR', '2017-09-04', '11:54:40', '181.64.216.195', '181.64.216.195'),
(746, 'APPBOSA SAMAN SAMAN', '2', 'SUDU 506496-4', '20465', 'ACTUALIZAR', '2017-09-04', '11:56:20', '181.64.216.195', '181.64.216.195'),
(747, 'APPBOSA SAMAN SAMAN', '2', 'SUDU 505287-6', '20464', 'ACTUALIZAR', '2017-09-04', '11:57:31', '181.64.216.195', '181.64.216.195'),
(748, 'APPBOSA SAMAN SAMAN', '2', 'TTNU 896082-9', '20459', 'ACTUALIZAR', '2017-09-04', '11:59:38', '181.64.216.195', '181.64.216.195'),
(749, 'APPBOSA SAMAN SAMAN', '2', 'CRLU 726811-0', '2', 'ACTUALIZAR', '2017-09-04', '12:03:17', '181.64.216.195', '181.64.216.195'),
(750, 'APPBOSA SAMAN SAMAN', '2', 'CRLU 726872-1', '1', 'ACTUALIZAR', '2017-09-04', '12:04:43', '181.64.216.195', '181.64.216.195'),
(751, 'APPBOSA SAMAN SAMAN', '2', 'HLXU 872039-6', '3', 'ACTUALIZAR', '2017-09-04', '12:08:33', '181.64.216.195', '181.64.216.195'),
(752, 'APPBOSA SAMAN SAMAN', '2', 'CPSU 511579-3', '4', 'ACTUALIZAR', '2017-09-04', '12:09:38', '181.64.216.195', '181.64.216.195'),
(753, 'APPBOSA SAMAN SAMAN', '2', 'CGMU 514920-0', '1', 'ACTUALIZAR', '2017-09-04', '12:12:42', '181.64.216.195', '181.64.216.195'),
(754, 'APPBOSA SAMAN SAMAN', '2', 'CGMU 514920-0', '1', 'ACTUALIZAR', '2017-09-04', '12:13:19', '181.64.216.195', '181.64.216.195'),
(755, 'APPBOSA SAMAN SAMAN', '2', 'DFIU 331303-9', '2', 'ACTUALIZAR', '2017-09-04', '12:15:41', '181.64.216.195', '181.64.216.195'),
(756, 'APPBOSA SAMAN SAMAN', '2', 'HLXU 870752-1', '1', 'ACTUALIZAR', '2017-09-04', '12:19:37', '181.64.216.195', '181.64.216.195'),
(757, 'APPBOSA SAMAN SAMAN', '2', 'LNXU 755192-0', '2', 'ACTUALIZAR', '2017-09-04', '12:22:37', '181.64.216.195', '181.64.216.195'),
(758, 'APPBOSA SAMAN SAMAN', '2', 'MORU 580799-1', '20465', 'ACTUALIZAR', '2017-09-04', '12:23:37', '181.64.216.195', '181.64.216.195'),
(759, 'APPBOSA SAMAN SAMAN', '3', 'MNBU 304147-6', '20597', 'ACTUALIZAR', '2017-09-04', '12:30:48', '181.64.216.195', '181.64.216.195'),
(760, 'APPBOSA SAMAN SAMAN', '3', 'PONU 48312-3', '20600', 'ACTUALIZAR', '2017-09-04', '12:32:37', '181.64.216.195', '181.64.216.195'),
(761, 'APPBOSA SAMAN SAMAN', '3', 'MSWU 959266652', '20599', 'ACTUALIZAR', '2017-09-04', '12:33:55', '181.64.216.195', '181.64.216.195'),
(762, 'APPBOSA SAMAN SAMAN', '3', 'MSWU 901933-8', '20598', 'ACTUALIZAR', '2017-09-04', '12:35:04', '181.64.216.195', '181.64.216.195'),
(763, 'APPBOSA SAMAN SAMAN', '3', 'MNBU 003905-3', '20603', 'ACTUALIZAR', '2017-09-04', '12:43:10', '181.64.216.195', '181.64.216.195'),
(764, 'APPBOSA SAMAN SAMAN', '3', 'MNBU 003905-3', '20603', 'ACTUALIZAR', '2017-09-04', '12:44:17', '181.64.216.195', '181.64.216.195'),
(765, 'APPBOSA SAMAN SAMAN', '3', 'MWCU 6677867-0', '20604', 'ACTUALIZAR', '2017-09-04', '12:45:59', '181.64.216.195', '181.64.216.195'),
(766, 'APPBOSA SAMAN SAMAN', '3', 'MWSU 901692-0', '20601', 'ACTUALIZAR', '2017-09-04', '12:47:02', '181.64.216.195', '181.64.216.195'),
(767, 'APPBOSA SAMAN SAMAN', '3', 'MNBU 003905-3', '20602', 'ACTUALIZAR', '2017-09-04', '12:49:29', '181.64.216.195', '181.64.216.195'),
(768, 'APPBOSA SAMAN SAMAN', '3', 'TTNU 895753-2', '20589', 'ACTUALIZAR', '2017-09-04', '12:50:33', '181.64.216.195', '181.64.216.195'),
(769, 'APPBOSA SAMAN SAMAN', '3', 'TTNU 895899-2', '20590', 'ACTUALIZAR', '2017-09-04', '12:51:56', '181.64.216.195', '181.64.216.195'),
(770, 'APPBOSA SAMAN SAMAN', '3', 'TTNU 895519-1', '20591', 'ACTUALIZAR', '2017-09-04', '12:55:32', '181.64.216.195', '181.64.216.195'),
(771, 'APPBOSA SAMAN SAMAN', '3', 'TTNU 896202-0', '20592', 'ACTUALIZAR', '2017-09-04', '12:56:47', '181.64.216.195', '181.64.216.195'),
(772, 'APPBOSA SAMAN SAMAN', '3', 'TTNU 895842-0', '20593', 'ACTUALIZAR', '2017-09-04', '12:57:59', '181.64.216.195', '181.64.216.195'),
(773, 'APPBOSA SAMAN SAMAN', '3', 'SUDU 816039-1', '20594', 'ACTUALIZAR', '2017-09-04', '12:59:18', '181.64.216.195', '181.64.216.195'),
(774, 'APPBOSA SAMAN SAMAN', '3', 'SUDU 803220-9', '20591', 'ACTUALIZAR', '2017-09-04', '13:00:28', '181.64.216.195', '181.64.216.195'),
(775, 'APPBOSA SAMAN SAMAN', '3', 'SUDU 818251-2', '20596', 'ACTUALIZAR', '2017-09-05', '07:28:37', '181.64.216.195', '181.64.216.195'),
(776, 'APPBOSA SAMAN SAMAN', '3', 'CRLU 12743-2', '1', 'ACTUALIZAR', '2017-09-05', '07:31:36', '181.64.216.195', '181.64.216.195'),
(777, 'APPBOSA SAMAN SAMAN', '3', 'FSCU 56150-4', '2', 'ACTUALIZAR', '2017-09-05', '07:32:52', '181.64.216.195', '181.64.216.195'),
(778, 'APPBOSA SAMAN SAMAN', '3', 'BMOU 975255-1', '1', 'ACTUALIZAR', '2017-09-05', '07:34:24', '181.64.216.195', '181.64.216.195');
INSERT INTO `audit_contenedor` (`id`, `usuario_responsable`, `semana`, `numero_contenedor`, `referencia`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(779, 'APPBOSA SAMAN SAMAN', '3', 'DFIU 710073-0', '1', 'ACTUALIZAR', '2017-09-05', '07:36:02', '181.64.216.195', '181.64.216.195'),
(780, 'APPBOSA SAMAN SAMAN', '3', 'HLXU 871903-4', '1', 'ACTUALIZAR', '2017-09-05', '07:44:06', '181.64.216.195', '181.64.216.195'),
(781, 'APPBOSA SAMAN SAMAN', '3', 'HLXU 871473-1', '2', 'ACTUALIZAR', '2017-09-05', '07:46:05', '181.64.216.195', '181.64.216.195'),
(782, 'APPBOSA SAMAN SAMAN', '3', 'CPSU 517265-9', '3', 'ACTUALIZAR', '2017-09-05', '07:47:32', '181.64.216.195', '181.64.216.195'),
(783, 'APPBOSA SAMAN SAMAN', '4', 'PONU 483501-9', '20710', 'ACTUALIZAR', '2017-09-05', '07:51:57', '181.64.216.195', '181.64.216.195'),
(784, 'APPBOSA SAMAN SAMAN', '4', 'MWCU 663391-1', '20712', 'ACTUALIZAR', '2017-09-05', '07:52:53', '181.64.216.195', '181.64.216.195'),
(785, 'APPBOSA SAMAN SAMAN', '4', 'MWMU 641236-1', '20712', 'ACTUALIZAR', '2017-09-05', '07:53:40', '181.64.216.195', '181.64.216.195'),
(786, 'APPBOSA SAMAN SAMAN', '4', 'MWCU 663391-1', '20711', 'ACTUALIZAR', '2017-09-05', '07:54:12', '181.64.216.195', '181.64.216.195'),
(787, 'APPBOSA SAMAN SAMAN', '4', 'PONU 450833-5', '20701', 'ACTUALIZAR', '2017-09-05', '07:56:01', '181.64.216.195', '181.64.216.195'),
(788, 'APPBOSA SAMAN SAMAN', '4', 'MWMU 641236-1', '20712', 'ACTUALIZAR', '2017-09-05', '07:56:28', '181.64.216.195', '181.64.216.195'),
(789, 'APPBOSA SAMAN SAMAN', '4', 'PONU 483501-9', '20710', 'ACTUALIZAR', '2017-09-05', '07:56:35', '181.64.216.195', '181.64.216.195'),
(790, 'APPBOSA SAMAN SAMAN', '4', 'PONU 450833-5', '20701', 'ACTUALIZAR', '2017-09-05', '07:56:41', '181.64.216.195', '181.64.216.195'),
(791, 'APPBOSA SAMAN SAMAN', '4', 'PONU 450833-5', '20701', 'ACTUALIZAR', '2017-09-05', '07:56:47', '181.64.216.195', '181.64.216.195'),
(792, 'APPBOSA SAMAN SAMAN', '4', 'TTNU 895955-6', '20703', 'ACTUALIZAR', '2017-09-05', '07:59:45', '181.64.216.195', '181.64.216.195'),
(793, 'APPBOSA SAMAN SAMAN', '4', 'TTNU 895553-0', '20694', 'ACTUALIZAR', '2017-09-05', '08:01:04', '181.64.216.195', '181.64.216.195'),
(794, 'APPBOSA SAMAN SAMAN', '4', 'TTNU 896088-1', '20704', 'ACTUALIZAR', '2017-09-05', '08:02:36', '181.64.216.195', '181.64.216.195'),
(795, 'APPBOSA SAMAN SAMAN', '4', 'MNBU 007605-7', '20777', 'ACTUALIZAR', '2017-09-05', '08:04:03', '181.64.216.195', '181.64.216.195'),
(796, 'APPBOSA SAMAN SAMAN', '4', 'PONU 499225-5', '20714', 'ACTUALIZAR', '2017-09-05', '08:05:10', '181.64.216.195', '181.64.216.195'),
(797, 'APPBOSA SAMAN SAMAN', '4', 'TTNU 895539-7', '20701', 'ACTUALIZAR', '2017-09-05', '08:07:37', '181.64.216.195', '181.64.216.195'),
(798, 'APPBOSA SAMAN SAMAN', '4', 'PONU 450833-5', '20713', 'ACTUALIZAR', '2017-09-05', '08:08:48', '181.64.216.195', '181.64.216.195'),
(799, 'APPBOSA SAMAN SAMAN', '4', 'TTNU 895539-7', '20701', 'ACTUALIZAR', '2017-09-05', '08:10:37', '181.64.216.195', '181.64.216.195'),
(800, 'APPBOSA SAMAN SAMAN', '4', 'TTNU 895955-6', '20703', 'ACTUALIZAR', '2017-09-05', '08:11:43', '181.64.216.195', '181.64.216.195'),
(801, 'APPBOSA SAMAN SAMAN', '4', 'TTNU 895553-0', '20694', 'ACTUALIZAR', '2017-09-05', '08:12:14', '181.64.216.195', '181.64.216.195'),
(802, 'APPBOSA SAMAN SAMAN', '4', 'TTNU 896088-1', '20704', 'ACTUALIZAR', '2017-09-05', '08:13:57', '181.64.216.195', '181.64.216.195'),
(803, 'APPBOSA SAMAN SAMAN', '4', 'MNBU 007605-7', '20777', 'ACTUALIZAR', '2017-09-05', '08:14:36', '181.64.216.195', '181.64.216.195'),
(804, 'APPBOSA SAMAN SAMAN', '4', 'PONU 499225-5', '20714', 'ACTUALIZAR', '2017-09-05', '08:15:15', '181.64.216.195', '181.64.216.195'),
(805, 'APPBOSA SAMAN SAMAN', '4', 'TTNU 896073-1', '20702', 'ACTUALIZAR', '2017-09-05', '08:18:21', '181.64.216.195', '181.64.216.195'),
(806, 'APPBOSA SAMAN SAMAN', '4', 'SUDU 607249-7', '20708', 'ACTUALIZAR', '2017-09-05', '08:20:24', '181.64.216.195', '181.64.216.195'),
(807, 'APPBOSA SAMAN SAMAN', '4', 'SUDU 607249-7', '20708', 'ACTUALIZAR', '2017-09-05', '08:22:15', '181.64.216.195', '181.64.216.195'),
(808, 'APPBOSA SAMAN SAMAN', '2', 'HLXU 870752-1', '1', 'ACTUALIZAR', '2017-09-05', '08:23:09', '181.64.216.195', '181.64.216.195'),
(809, 'APPBOSA SAMAN SAMAN', '2', 'LNXU 755192-0', '2', 'ACTUALIZAR', '2017-09-05', '08:24:14', '181.64.216.195', '181.64.216.195'),
(810, 'APPBOSA SAMAN SAMAN', '4', 'SUDU  824006-0', '20709', 'ACTUALIZAR', '2017-09-05', '08:24:16', '181.64.216.195', '181.64.216.195'),
(811, 'APPBOSA SAMAN SAMAN', '4', 'SUDU 822334-0', '20705', 'ACTUALIZAR', '2017-09-05', '08:25:40', '181.64.216.195', '181.64.216.195'),
(812, 'APPBOSA SAMAN SAMAN', '2', 'HLXU 872039-6', '3', 'ACTUALIZAR', '2017-09-05', '08:26:57', '181.64.216.195', '181.64.216.195'),
(813, 'APPBOSA SAMAN SAMAN', '4', 'SUDU 822379-8', '20707', 'ACTUALIZAR', '2017-09-05', '08:31:01', '181.64.216.195', '181.64.216.195'),
(814, 'APPBOSA SAMAN SAMAN', '4', 'SUDU 822883-0', '20706', 'ACTUALIZAR', '2017-09-05', '08:33:28', '181.64.216.195', '181.64.216.195'),
(815, 'APPBOSA SAMAN SAMAN', '2', 'CPSU 511579-3', '4', 'ACTUALIZAR', '2017-09-05', '08:35:09', '181.64.216.195', '181.64.216.195'),
(816, 'APPBOSA SAMAN SAMAN', '4', 'CPSU 510153-1', '1', 'ACTUALIZAR', '2017-09-05', '08:35:12', '181.64.216.195', '181.64.216.195'),
(817, 'APPBOSA SAMAN SAMAN', '4', 'CRLU 723221-0', '2', 'ACTUALIZAR', '2017-09-05', '08:36:45', '181.64.216.195', '181.64.216.195'),
(818, 'APPBOSA SAMAN SAMAN', '4', 'GESU 954909-3', '3', 'ACTUALIZAR', '2017-09-05', '08:38:47', '181.64.216.195', '181.64.216.195'),
(819, 'APPBOSA SAMAN SAMAN', '4', 'TCLU 124255-8', '1', 'ACTUALIZAR', '2017-09-05', '08:40:05', '181.64.216.195', '181.64.216.195'),
(820, 'APPBOSA SAMAN SAMAN', '4', 'CRLU 162668-5', '2', 'ACTUALIZAR', '2017-09-05', '08:41:06', '181.64.216.195', '181.64.216.195'),
(821, 'APPBOSA SAMAN SAMAN', '5', 'PUNU 485432-2', '20812', 'ACTUALIZAR', '2017-09-05', '08:58:50', '181.64.216.195', '181.64.216.195'),
(822, 'APPBOSA SAMAN SAMAN', '5', 'TTNU 895573-5', '20797', 'ACTUALIZAR', '2017-09-05', '09:00:00', '181.64.216.195', '181.64.216.195'),
(823, 'APPBOSA SAMAN SAMAN', '5', 'TTNU 895009-7', '20798', 'ACTUALIZAR', '2017-09-05', '09:01:08', '181.64.216.195', '181.64.216.195'),
(824, 'APPBOSA SAMAN SAMAN', '5', 'TTNU 895928-4', '20800', 'ACTUALIZAR', '2017-09-05', '09:02:44', '181.64.216.195', '181.64.216.195'),
(825, 'APPBOSA SAMAN SAMAN', '5', 'TTNU 895345-5', '20799', 'ACTUALIZAR', '2017-09-05', '09:03:28', '181.64.216.195', '181.64.216.195'),
(826, 'APPBOSA SAMAN SAMAN', '5', 'PONU 486136-3', '20811', 'ACTUALIZAR', '2017-09-05', '09:05:09', '181.64.216.195', '181.64.216.195'),
(827, 'APPBOSA SAMAN SAMAN', '5', 'PONU 486364-3', '20808', 'ACTUALIZAR', '2017-09-05', '09:06:27', '181.64.216.195', '181.64.216.195'),
(828, 'APPBOSA SAMAN SAMAN', '5', 'PONU 496849-6', '20807', 'ACTUALIZAR', '2017-09-05', '09:07:58', '181.64.216.195', '181.64.216.195'),
(829, 'APPBOSA SAMAN SAMAN', '5', 'PUNU 485432-2', '20812', 'ACTUALIZAR', '2017-09-05', '09:08:29', '181.64.216.195', '181.64.216.195'),
(830, 'APPBOSA SAMAN SAMAN', '5', 'PONU 486136-3', '20811', 'ACTUALIZAR', '2017-09-05', '09:10:26', '181.64.216.195', '181.64.216.195'),
(831, 'APPBOSA SAMAN SAMAN', '5', 'PONU 486364-3', '20808', 'ACTUALIZAR', '2017-09-05', '09:11:36', '181.64.216.195', '181.64.216.195'),
(832, 'APPBOSA SAMAN SAMAN', '5', 'MNBU 012042-1', '20810', 'ACTUALIZAR', '2017-09-05', '09:13:43', '181.64.216.195', '181.64.216.195'),
(833, 'APPBOSA SAMAN SAMAN', '5', 'MNBU 012042-1', '20809', 'ACTUALIZAR', '2017-09-05', '09:15:13', '181.64.216.195', '181.64.216.195'),
(834, 'APPBOSA SAMAN SAMAN', '5', 'SEGU 945919-7', '1', 'ACTUALIZAR', '2017-09-05', '09:17:38', '181.64.216.195', '181.64.216.195'),
(835, 'APPBOSA SAMAN SAMAN', '5', 'MEDU 913599-3', '2', 'ACTUALIZAR', '2017-09-05', '09:18:35', '181.64.216.195', '181.64.216.195'),
(836, 'APPBOSA SAMAN SAMAN', '5', 'CPSU 514094-4', '3', 'ACTUALIZAR', '2017-09-05', '09:19:26', '181.64.216.195', '181.64.216.195'),
(837, 'APPBOSA SAMAN SAMAN', '5', 'CPSU 515779-9', '4', 'ACTUALIZAR', '2017-09-05', '09:20:20', '181.64.216.195', '181.64.216.195'),
(838, 'APPBOSA SAMAN SAMAN', '5', 'CPSU 510241-4', '1', 'ACTUALIZAR', '2017-09-05', '09:31:27', '181.64.216.195', '181.64.216.195'),
(839, 'APPBOSA SAMAN SAMAN', '5', 'CPSU 510241-4', '2', 'ACTUALIZAR', '2017-09-05', '09:32:06', '181.64.216.195', '181.64.216.195'),
(840, 'APPBOSA SAMAN SAMAN', '5', 'CPSU 510241-4', '2', 'ACTUALIZAR', '2017-09-05', '09:32:28', '181.64.216.195', '181.64.216.195'),
(841, 'APPBOSA SAMAN SAMAN', '5', 'SEGU 925356-5', '1', 'ACTUALIZAR', '2017-09-05', '09:35:02', '181.64.216.195', '181.64.216.195'),
(842, 'APPBOSA SAMAN SAMAN', '5', 'DIPU 429184-0', '1', 'ACTUALIZAR', '2017-09-05', '09:43:59', '181.64.216.195', '181.64.216.195'),
(843, 'APPBOSA SAMAN SAMAN', '5', 'TTNU 895408-7', '20801', 'INSERTAR', '2017-09-05', '09:49:47', '181.64.216.195', '181.64.216.195'),
(844, 'APPBOSA SAMAN SAMAN', '6', 'MNBU 333660-4', '20915', 'ACTUALIZAR', '2017-09-05', '09:53:48', '181.64.216.195', '181.64.216.195'),
(845, 'APPBOSA SAMAN SAMAN', '6', 'MNBU 333335-4', '20916', 'ACTUALIZAR', '2017-09-05', '09:54:42', '181.64.216.195', '181.64.216.195'),
(846, 'APPBOSA SAMAN SAMAN', '6', 'MWCU 531764-7', '20918', 'ACTUALIZAR', '2017-09-05', '09:55:43', '181.64.216.195', '181.64.216.195'),
(847, 'APPBOSA SAMAN SAMAN', '6', 'MNBU 324198-3', '20917', 'ACTUALIZAR', '2017-09-05', '09:56:40', '181.64.216.195', '181.64.216.195'),
(848, 'APPBOSA SAMAN SAMAN', '6', 'CXRU 156856-2', '20805', 'ACTUALIZAR', '2017-09-05', '09:57:45', '181.64.216.195', '181.64.216.195'),
(849, 'APPBOSA SAMAN SAMAN', '6', 'TTNU 845381-9', '20803', 'ACTUALIZAR', '2017-09-05', '09:58:46', '181.64.216.195', '181.64.216.195'),
(850, 'APPBOSA SAMAN SAMAN', '6', 'TTNU 840858-0', '20804', 'ACTUALIZAR', '2017-09-05', '10:01:20', '181.64.216.195', '181.64.216.195'),
(851, 'APPBOSA SAMAN SAMAN', '6', 'TTNU 896177-0', '20907', 'ACTUALIZAR', '2017-09-05', '10:02:56', '181.64.216.195', '181.64.216.195'),
(852, 'APPBOSA SAMAN SAMAN', '6', 'TTNU 896156-9', '20904', 'ACTUALIZAR', '2017-09-05', '10:03:34', '181.64.216.195', '181.64.216.195'),
(853, 'APPBOSA SAMAN SAMAN', '6', 'GESU 934390-0', '20806', 'ACTUALIZAR', '2017-09-05', '10:07:52', '181.64.216.195', '181.64.216.195'),
(854, 'APPBOSA SAMAN SAMAN', '6', 'PONU 497181-7', '20919', 'ACTUALIZAR', '2017-09-05', '10:08:43', '181.64.216.195', '181.64.216.195'),
(855, 'APPBOSA SAMAN SAMAN', '6', 'TTNU 896181-0', '20906', 'ACTUALIZAR', '2017-09-05', '10:09:54', '181.64.216.195', '181.64.216.195'),
(856, 'APPBOSA SAMAN SAMAN', '6', 'TTNU 895868-9', '20908', 'ACTUALIZAR', '2017-09-05', '10:12:00', '181.64.216.195', '181.64.216.195'),
(857, 'APPBOSA SAMAN SAMAN', '6', 'TTNU 895804-0', '20905', 'ACTUALIZAR', '2017-09-05', '10:13:18', '181.64.216.195', '181.64.216.195'),
(858, 'APPBOSA SAMAN SAMAN', '6', 'TRIU 839010-3', '20909', 'ACTUALIZAR', '2017-09-05', '10:14:27', '181.64.216.195', '181.64.216.195'),
(859, 'APPBOSA SAMAN SAMAN', '6', 'TTNU 896156-9', '20904', 'ACTUALIZAR', '2017-09-05', '10:15:27', '181.64.216.195', '181.64.216.195'),
(860, 'APPBOSA SAMAN SAMAN', '6', 'TTNU 896181-0', '20906', 'ACTUALIZAR', '2017-09-05', '10:16:24', '181.64.216.195', '181.64.216.195'),
(861, 'APPBOSA SAMAN SAMAN', '6', 'TTNU 895804-0', '20905', 'ACTUALIZAR', '2017-09-05', '10:16:39', '181.64.216.195', '181.64.216.195'),
(862, 'APPBOSA SAMAN SAMAN', '6', 'LNXU 755883-8', '1', 'ACTUALIZAR', '2017-09-05', '10:17:50', '181.64.216.195', '181.64.216.195'),
(863, 'APPBOSA SAMAN SAMAN', '6', 'CPSU 517098-0', '2', 'ACTUALIZAR', '2017-09-05', '10:18:38', '181.64.216.195', '181.64.216.195'),
(864, 'APPBOSA SAMAN SAMAN', '6', 'MORU 110221-5', '1', 'ACTUALIZAR', '2017-09-05', '10:19:37', '181.64.216.195', '181.64.216.195'),
(865, 'APPBOSA SAMAN SAMAN', '6', 'GESU 938471-9', '1', 'ACTUALIZAR', '2017-09-05', '10:21:45', '181.64.216.195', '181.64.216.195'),
(866, 'APPBOSA SAMAN SAMAN', '6', 'TEMU 943002-9', '1', 'ACTUALIZAR', '2017-09-05', '10:22:00', '181.64.216.195', '181.64.216.195'),
(867, 'APPBOSA SAMAN SAMAN', '7', 'MEDU 913147-3', '20910', 'ACTUALIZAR', '2017-09-05', '10:28:31', '181.64.216.195', '181.64.216.195'),
(868, 'APPBOSA SAMAN SAMAN', '7', 'MWCU 664404-8', '21051', 'ACTUALIZAR', '2017-09-05', '10:30:24', '181.64.216.195', '181.64.216.195'),
(869, 'APPBOSA SAMAN SAMAN', '7', 'MNBU 361098-9', '21050', 'ACTUALIZAR', '2017-09-05', '10:33:13', '181.64.216.195', '181.64.216.195'),
(870, 'APPBOSA SAMAN SAMAN', '7', 'MSCU 745022-2', '20913', 'ACTUALIZAR', '2017-09-05', '10:34:42', '181.64.216.195', '181.64.216.195'),
(871, 'APPBOSA SAMAN SAMAN', '7', 'TEMU 900898-6', '20911', 'ACTUALIZAR', '2017-09-05', '10:35:25', '181.64.216.195', '181.64.216.195'),
(872, 'APPBOSA SAMAN SAMAN', '7', 'CRLU 141145-5', '20914', 'ACTUALIZAR', '2017-09-05', '10:36:04', '181.64.216.195', '181.64.216.195'),
(873, 'APPBOSA SAMAN SAMAN', '7', 'TTNU 814654-0', '21046', 'ACTUALIZAR', '2017-09-05', '10:36:39', '181.64.216.195', '181.64.216.195'),
(874, 'APPBOSA SAMAN SAMAN', '7', 'MEDU 905506-0', '21045', 'ACTUALIZAR', '2017-09-05', '10:37:10', '181.64.216.195', '181.64.216.195'),
(875, 'APPBOSA SAMAN SAMAN', '7', 'CXRU 118701-5', '21043', 'ACTUALIZAR', '2017-09-05', '10:37:47', '181.64.216.195', '181.64.216.195'),
(876, 'APPBOSA SAMAN SAMAN', '7', 'TRIU 859164-3', '21045', 'ACTUALIZAR', '2017-09-05', '10:38:33', '181.64.216.195', '181.64.216.195'),
(877, 'APPBOSA SAMAN SAMAN', '7', 'CXRU 112006-9', '21041', 'ACTUALIZAR', '2017-09-05', '10:39:37', '181.64.216.195', '181.64.216.195'),
(878, 'APPBOSA SAMAN SAMAN', '7', 'CXRU 128234-7', '21044', 'ACTUALIZAR', '2017-09-05', '10:40:22', '181.64.216.195', '181.64.216.195'),
(879, 'APPBOSA SAMAN SAMAN', '4', 'CPSU 510153-1', '1', 'ACTUALIZAR', '2017-09-05', '10:41:30', '181.64.216.195', '181.64.216.195'),
(880, 'APPBOSA SAMAN SAMAN', '7', 'CRLU 126978-3', '1', 'ACTUALIZAR', '2017-09-05', '10:41:36', '181.64.216.195', '181.64.216.195'),
(881, 'APPBOSA SAMAN SAMAN', '7', 'CRLU 723499-5', '1', 'ACTUALIZAR', '2017-09-05', '10:43:41', '181.64.216.195', '181.64.216.195'),
(882, 'APPBOSA SAMAN SAMAN', '7', 'CPSU', '1', 'ACTUALIZAR', '2017-09-05', '10:44:57', '181.64.216.195', '181.64.216.195'),
(883, 'APPBOSA SAMAN SAMAN', '7', 'BMOU 922128-3', '2', 'ACTUALIZAR', '2017-09-05', '10:45:38', '181.64.216.195', '181.64.216.195'),
(884, 'APPBOSA SAMAN SAMAN', '4', 'CRLU 723221-0', '2', 'ACTUALIZAR', '2017-09-05', '10:46:51', '181.64.216.195', '181.64.216.195'),
(885, 'APPBOSA SAMAN SAMAN', '7', 'CPSU 512069-7', '1', 'ACTUALIZAR', '2017-09-05', '10:48:02', '181.64.216.195', '181.64.216.195'),
(886, 'APPBOSA SAMAN SAMAN', '7', 'SEGU 946177-0', '1', 'ACTUALIZAR', '2017-09-05', '10:50:05', '181.64.216.195', '181.64.216.195'),
(887, 'APPBOSA SAMAN SAMAN', '7', 'TRIU 890632-5', '2', 'ACTUALIZAR', '2017-09-05', '10:50:47', '181.64.216.195', '181.64.216.195'),
(888, 'APPBOSA SAMAN SAMAN', '7', 'SEGU 946177-0', '1', 'ACTUALIZAR', '2017-09-05', '10:51:10', '181.64.216.195', '181.64.216.195'),
(889, 'APPBOSA SAMAN SAMAN', '7', 'DFIU 812371-0', '1', 'ACTUALIZAR', '2017-09-05', '10:52:06', '181.64.216.195', '181.64.216.195'),
(890, 'APPBOSA SAMAN SAMAN', '7', 'DFIU 215252-0', '2', 'ACTUALIZAR', '2017-09-05', '10:52:53', '181.64.216.195', '181.64.216.195'),
(891, 'APPBOSA SAMAN SAMAN', '7', 'GESU 924539-6', '3', 'ACTUALIZAR', '2017-09-05', '10:54:41', '181.64.216.195', '181.64.216.195'),
(892, 'APPBOSA SAMAN SAMAN', '8', 'MSWU 003600-0', '21132', 'ACTUALIZAR', '2017-09-05', '10:57:33', '181.64.216.195', '181.64.216.195'),
(893, 'APPBOSA SAMAN SAMAN', '8', 'MWMU 640056-6', '21131', 'ACTUALIZAR', '2017-09-05', '10:58:52', '181.64.216.195', '181.64.216.195'),
(894, 'APPBOSA SAMAN SAMAN', '8', 'MNBU 311496-8', '21131', 'ACTUALIZAR', '2017-09-05', '10:59:58', '181.64.216.195', '181.64.216.195'),
(895, 'APPBOSA SAMAN SAMAN', '8', 'MNBU 311496-8', '21133', 'ACTUALIZAR', '2017-09-05', '11:02:08', '181.64.216.195', '181.64.216.195'),
(896, 'APPBOSA SAMAN SAMAN', '8', 'MWCU 690649-3', '21134', 'ACTUALIZAR', '2017-09-05', '11:03:03', '181.64.216.195', '181.64.216.195'),
(897, 'APPBOSA SAMAN SAMAN', '8', 'MNBU 023386-0', '21135', 'ACTUALIZAR', '2017-09-05', '11:04:49', '181.64.216.195', '181.64.216.195'),
(898, 'APPBOSA SAMAN SAMAN', '8', 'CAIU 556541-8', '21126', 'ACTUALIZAR', '2017-09-05', '11:09:53', '181.64.216.195', '181.64.216.195'),
(899, 'APPBOSA SAMAN SAMAN', '8', 'CAIU', '21127', 'ACTUALIZAR', '2017-09-05', '11:11:38', '181.64.216.195', '181.64.216.195'),
(900, 'APPBOSA SAMAN SAMAN', '8', 'SUDU 605546-3', '21047', 'ACTUALIZAR', '2017-09-05', '11:15:44', '181.64.216.195', '181.64.216.195'),
(901, 'APPBOSA SAMAN SAMAN', '5', 'CPSU 510241-4', '1', 'ACTUALIZAR', '2017-09-05', '11:21:39', '181.64.216.195', '181.64.216.195'),
(902, 'APPBOSA SAMAN SAMAN', '8', 'SUDU 629158-2', '21049', 'ACTUALIZAR', '2017-09-05', '11:21:50', '181.64.216.195', '181.64.216.195'),
(903, 'APPBOSA SAMAN SAMAN', '5', 'CPSU 510241-4', '2', 'ACTUALIZAR', '2017-09-05', '11:22:23', '181.64.216.195', '181.64.216.195'),
(904, 'APPBOSA SAMAN SAMAN', '8', 'CAIU 556284-6', '21125', 'ACTUALIZAR', '2017-09-05', '11:22:44', '181.64.216.195', '181.64.216.195'),
(905, 'APPBOSA SAMAN SAMAN', '8', 'SUDU 624722-9', '21048', 'ACTUALIZAR', '2017-09-05', '11:23:31', '181.64.216.195', '181.64.216.195'),
(906, 'APPBOSA SAMAN SAMAN', '8', 'SUDU 624722-9', '21130', 'ACTUALIZAR', '2017-09-05', '11:26:25', '181.64.216.195', '181.64.216.195'),
(907, 'APPBOSA SAMAN SAMAN', '8', 'MEDU 916199-2', '21130', 'ACTUALIZAR', '2017-09-05', '11:27:26', '181.64.216.195', '181.64.216.195'),
(908, 'APPBOSA SAMAN SAMAN', '8', 'TRIU 815586-6', '21129', 'ACTUALIZAR', '2017-09-05', '11:28:49', '181.64.216.195', '181.64.216.195'),
(909, 'APPBOSA SAMAN SAMAN', '8', 'SZLU 915151-0', '21128', 'ACTUALIZAR', '2017-09-05', '11:29:46', '181.64.216.195', '181.64.216.195'),
(910, 'APPBOSA SAMAN SAMAN', '8', 'CRLU 126171-4', '1', 'ACTUALIZAR', '2017-09-05', '11:31:30', '181.64.216.195', '181.64.216.195'),
(911, 'APPBOSA SAMAN SAMAN', '8', 'CPSU 512134-8', '2', 'ACTUALIZAR', '2017-09-05', '11:34:15', '181.64.216.195', '181.64.216.195'),
(912, 'APPBOSA SAMAN SAMAN', '8', 'CRLU 727352-2', '1', 'ACTUALIZAR', '2017-09-05', '11:37:43', '181.64.216.195', '181.64.216.195'),
(913, 'APPBOSA SAMAN SAMAN', '8', 'CRLU 727559-3', '2', 'ACTUALIZAR', '2017-09-05', '11:39:17', '181.64.216.195', '181.64.216.195'),
(914, 'APPBOSA SAMAN SAMAN', '8', 'GESU 948741-9', '3', 'ACTUALIZAR', '2017-09-05', '11:40:13', '181.64.216.195', '181.64.216.195'),
(915, 'APPBOSA SAMAN SAMAN', '8', 'CRLU 727559-3', '2', 'ACTUALIZAR', '2017-09-05', '11:41:09', '181.64.216.195', '181.64.216.195'),
(916, 'APPBOSA SAMAN SAMAN', '8', 'DFIU 723027-2', '1', 'ACTUALIZAR', '2017-09-05', '11:42:03', '181.64.216.195', '181.64.216.195'),
(917, 'APPBOSA SAMAN SAMAN', '8', 'DTPU 721093-0', '2', 'ACTUALIZAR', '2017-09-05', '11:46:02', '181.64.216.195', '181.64.216.195'),
(918, 'APPBOSA SAMAN SAMAN', '8', 'FSCU 568365-5', '1', 'ACTUALIZAR', '2017-09-05', '11:47:43', '181.64.216.195', '181.64.216.195'),
(919, 'APPBOSA SAMAN SAMAN', '8', 'FSCU 568365-5', '1', 'ACTUALIZAR', '2017-09-05', '11:47:57', '181.64.216.195', '181.64.216.195'),
(920, 'APPBOSA SAMAN SAMAN', '9', 'PUNU 496304-6', '21262', 'ACTUALIZAR', '2017-09-05', '11:53:57', '181.64.216.195', '181.64.216.195'),
(921, 'APPBOSA SAMAN SAMAN', '7', 'CRLU 723499-5', '1', 'ACTUALIZAR', '2017-09-05', '11:57:27', '181.64.216.195', '181.64.216.195'),
(922, 'APPBOSA SAMAN SAMAN', '9', 'MNBU 018684-0', '21322', 'ACTUALIZAR', '2017-09-05', '11:57:51', '181.64.216.195', '181.64.216.195'),
(923, 'APPBOSA SAMAN SAMAN', '9', 'MSWU 905019-0', '21322', 'ACTUALIZAR', '2017-09-05', '11:58:36', '181.64.216.195', '181.64.216.195'),
(924, 'APPBOSA SAMAN SAMAN', '9', 'TEMU 921567-0', '21264', 'ACTUALIZAR', '2017-09-05', '12:12:47', '181.64.216.195', '181.64.216.195'),
(925, 'APPBOSA SAMAN SAMAN', '8', 'CRLU 727559-3', '2', 'ACTUALIZAR', '2017-09-05', '12:15:24', '181.64.216.195', '181.64.216.195'),
(926, 'APPBOSA SAMAN SAMAN', '9', 'TEMU 921177-7', '21263', 'ACTUALIZAR', '2017-09-05', '12:15:27', '181.64.216.195', '181.64.216.195'),
(927, 'APPBOSA SAMAN SAMAN', '8', 'GESU 948741-9', '3', 'ACTUALIZAR', '2017-09-05', '12:15:51', '181.64.216.195', '181.64.216.195'),
(928, 'APPBOSA SAMAN SAMAN', '9', 'TTNU 807064-0', '21266', 'ACTUALIZAR', '2017-09-05', '12:17:55', '181.64.216.195', '181.64.216.195'),
(929, 'APPBOSA SAMAN SAMAN', '9', 'SEGU 929466-7', '21256', 'ACTUALIZAR', '2017-09-05', '12:18:49', '181.64.216.195', '181.64.216.195'),
(930, 'APPBOSA SAMAN SAMAN', '9', 'SEGU 929466-7', '21256', 'ACTUALIZAR', '2017-09-05', '12:19:07', '181.64.216.195', '181.64.216.195'),
(931, 'APPBOSA SAMAN SAMAN', '8', 'CRLU 727352-2', '1', 'ACTUALIZAR', '2017-09-05', '12:19:40', '181.64.216.195', '181.64.216.195'),
(932, 'APPBOSA SAMAN SAMAN', '9', 'TTNU 814495-4', '21259', 'ACTUALIZAR', '2017-09-05', '12:22:50', '181.64.216.195', '181.64.216.195'),
(933, 'APPBOSA SAMAN SAMAN', '9', 'MNBU 018684-0', '21321', 'ACTUALIZAR', '2017-09-05', '12:25:32', '181.64.216.195', '181.64.216.195'),
(934, 'APPBOSA SAMAN SAMAN', '9', 'CXRU 112030-4', '21265', 'ACTUALIZAR', '2017-09-05', '12:27:04', '181.64.216.195', '181.64.216.195'),
(935, 'APPBOSA SAMAN SAMAN', '9', 'CRSU 614410-4', '21261', 'ACTUALIZAR', '2017-09-05', '12:28:57', '181.64.216.195', '181.64.216.195'),
(936, 'APPBOSA SAMAN SAMAN', '9', 'GESU 614410-4', '21260', 'ACTUALIZAR', '2017-09-05', '12:29:35', '181.64.216.195', '181.64.216.195'),
(937, 'APPBOSA SAMAN SAMAN', '9', 'MORU 113727-4', '1', 'ACTUALIZAR', '2017-09-05', '12:32:33', '181.64.216.195', '181.64.216.195'),
(938, 'APPBOSA SAMAN SAMAN', '9', 'BMOU 972253-6', '2', 'ACTUALIZAR', '2017-09-05', '12:33:31', '181.64.216.195', '181.64.216.195'),
(939, 'APPBOSA SAMAN SAMAN', '9', 'BMOU 920720-1', '1', 'ACTUALIZAR', '2017-09-05', '12:35:15', '181.64.216.195', '181.64.216.195'),
(940, 'APPBOSA SAMAN SAMAN', '9', 'CRLU 126701-3', '2', 'ACTUALIZAR', '2017-09-05', '12:36:14', '181.64.216.195', '181.64.216.195'),
(941, 'APPBOSA SAMAN SAMAN', '9', 'CXRU 140098-5', '3', 'ACTUALIZAR', '2017-09-05', '12:37:07', '181.64.216.195', '181.64.216.195'),
(942, 'APPBOSA SAMAN SAMAN', '9', 'HLXU 873679-6', '4', 'ACTUALIZAR', '2017-09-05', '12:38:33', '181.64.216.195', '181.64.216.195'),
(943, 'APPBOSA SAMAN SAMAN', '9', 'CGMU 652702-8', '1', 'ACTUALIZAR', '2017-09-05', '12:39:30', '181.64.216.195', '181.64.216.195'),
(944, 'APPBOSA SAMAN SAMAN', '9', 'AMWCU 929952-8', '2', 'ACTUALIZAR', '2017-09-05', '12:40:32', '181.64.216.195', '181.64.216.195'),
(945, 'APPBOSA SAMAN SAMAN', '9', 'GESU 935288-2', '1', 'ACTUALIZAR', '2017-09-05', '12:42:16', '181.64.216.195', '181.64.216.195'),
(946, 'APPBOSA SAMAN SAMAN', '9', 'GESU 950598-1', '2', 'ACTUALIZAR', '2017-09-05', '12:43:13', '181.64.216.195', '181.64.216.195'),
(947, 'APPBOSA SAMAN SAMAN', '10', 'MECU 670019-9', '21354', 'ACTUALIZAR', '2017-09-05', '12:51:22', '181.64.216.195', '181.64.216.195'),
(948, 'APPBOSA SAMAN SAMAN', '10', 'MECU 670019-9', '21354', 'ACTUALIZAR', '2017-09-05', '12:54:19', '181.64.216.195', '181.64.216.195'),
(949, 'APPBOSA SAMAN SAMAN', '10', 'MNBU 335774-1', '21356', 'ACTUALIZAR', '2017-09-05', '12:57:51', '181.64.216.195', '181.64.216.195'),
(950, 'APPBOSA SAMAN SAMAN', '10', 'PONU 487963-4', '21355', 'ACTUALIZAR', '2017-09-05', '12:58:36', '181.64.216.195', '181.64.216.195'),
(951, 'APPBOSA SAMAN SAMAN', '10', 'ATKU 411084-4', '21348', 'ACTUALIZAR', '2017-09-05', '12:59:49', '181.64.216.195', '181.64.216.195'),
(952, 'APPBOSA SAMAN SAMAN', '10', 'TCLU 138380-7', '21349', 'ACTUALIZAR', '2017-09-06', '07:18:36', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(953, 'APPBOSA SAMAN SAMAN', '10', 'TEMU 937082-4', '21347', 'ACTUALIZAR', '2017-09-06', '07:19:52', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(954, 'APPBOSA SAMAN SAMAN', '10', 'TCLU 138380-7', '21349', 'ACTUALIZAR', '2017-09-06', '07:20:37', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(955, 'APPBOSA SAMAN SAMAN', '10', 'CXRU 147615-2', '21350', 'ACTUALIZAR', '2017-09-06', '07:22:41', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(956, 'APPBOSA SAMAN SAMAN', '10', 'MSCU 732041-9', '21351', 'ACTUALIZAR', '2017-09-06', '07:23:38', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(957, 'APPBOSA SAMAN SAMAN', '10', 'TTNU 806981-9', '21352', 'ACTUALIZAR', '2017-09-06', '07:24:18', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(958, 'APPBOSA SAMAN SAMAN', '10', 'TRLU 164636-9', '21353', 'ACTUALIZAR', '2017-09-06', '07:25:04', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(959, 'APPBOSA SAMAN SAMAN', '10', 'TRIU 815100-6', '21357', 'ACTUALIZAR', '2017-09-06', '07:27:44', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(960, 'APPBOSA SAMAN SAMAN', '10', 'CGMU 515225-1', '21491', 'ACTUALIZAR', '2017-09-06', '07:28:46', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(961, 'APPBOSA SAMAN SAMAN', '10', 'CGMU 934441-6', '21492', 'ACTUALIZAR', '2017-09-06', '07:29:45', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(962, 'APPBOSA SAMAN SAMAN', '10', 'CGMU 515225-1', '21491', 'ACTUALIZAR', '2017-09-06', '07:30:14', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(963, 'APPBOSA SAMAN SAMAN', '10', 'TRIU 815100-6', '21357', 'ACTUALIZAR', '2017-09-06', '07:31:08', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(964, 'APPBOSA SAMAN SAMAN', '10', 'MORU 113458-9', '1', 'ACTUALIZAR', '2017-09-06', '07:56:50', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(965, 'APPBOSA SAMAN SAMAN', '10', 'HLXU 872841-6', '1', 'ACTUALIZAR', '2017-09-06', '07:58:01', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(966, 'APPBOSA SAMAN SAMAN', '10', 'GESU 933551-9', '2', 'ACTUALIZAR', '2017-09-06', '07:59:06', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(967, 'APPBOSA SAMAN SAMAN', '10', 'TRIU 89434-5', '1', 'ACTUALIZAR', '2017-09-06', '08:00:16', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(968, 'APPBOSA SAMAN SAMAN', '10', 'CRLU 722638-8', '2', 'ACTUALIZAR', '2017-09-06', '08:01:23', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(969, 'APPBOSA SAMAN SAMAN', '10', 'SZW901253-5', '1', 'ACTUALIZAR', '2017-09-06', '08:03:29', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(970, 'APPBOSA SAMAN SAMAN', '10', 'SZW 902443-3', '2', 'ACTUALIZAR', '2017-09-06', '08:04:14', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(971, 'APPBOSA SAMAN SAMAN', '11', 'MWCU 524508-5', '21473', 'ACTUALIZAR', '2017-09-06', '08:07:47', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(972, 'APPBOSA SAMAN SAMAN', '11', 'MBM 160065011', '21466', 'ACTUALIZAR', '2017-09-06', '08:09:10', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(973, 'APPBOSA SAMAN SAMAN', '11', 'SUDU 600697-8', '21468', 'ACTUALIZAR', '2017-09-06', '08:19:35', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(974, 'APPBOSA SAMAN SAMAN', '11', 'SUDU 602313-1', '21469', 'ACTUALIZAR', '2017-09-06', '08:20:21', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(975, 'APPBOSA SAMAN SAMAN', '11', 'SZLU 980398-4', '21465', 'ACTUALIZAR', '2017-09-06', '08:21:27', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(976, 'APPBOSA SAMAN SAMAN', '11', 'SUDU 817546-8', '21472', 'ACTUALIZAR', '2017-09-06', '08:22:36', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(977, 'APPBOSA SAMAN SAMAN', '11', 'SUDU 627315-1', '21471', 'ACTUALIZAR', '2017-09-06', '08:23:41', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(978, 'APPBOSA SAMAN SAMAN', '11', 'MWCU 524508-5', '21473', 'ACTUALIZAR', '2017-09-06', '08:24:02', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(979, 'APPBOSA SAMAN SAMAN', '11', 'SUDU 602313-1', '21469', 'ACTUALIZAR', '2017-09-06', '08:24:13', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(980, 'APPBOSA SAMAN SAMAN', '11', 'SUDU 600697-8', '21468', 'ACTUALIZAR', '2017-09-06', '08:24:27', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(981, 'APPBOSA SAMAN SAMAN', '11', 'MBM 160065011', '21466', 'ACTUALIZAR', '2017-09-06', '08:24:49', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(982, 'APPBOSA SAMAN SAMAN', '11', 'SUDU 817546-8', '712', 'ACTUALIZAR', '2017-09-06', '08:25:00', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(983, 'APPBOSA SAMAN SAMAN', '11', 'SUDU 817546-8', '21472', 'ACTUALIZAR', '2017-09-06', '08:25:50', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(984, 'APPBOSA SAMAN SAMAN', '11', 'SZLU 980398-4', '21465', 'ACTUALIZAR', '2017-09-06', '08:26:35', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(985, 'APPBOSA SAMAN SAMAN', '11', 'CPSU 510279-6', '1', 'ACTUALIZAR', '2017-09-06', '08:27:54', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(986, 'APPBOSA SAMAN SAMAN', '11', 'HLXU 879182-5', '2', 'ACTUALIZAR', '2017-09-06', '08:30:56', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(987, 'APPBOSA SAMAN SAMAN', '11', 'HLXU 879182-5', '2', 'ACTUALIZAR', '2017-09-06', '08:31:17', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(988, 'APPBOSA SAMAN SAMAN', '11', 'CPSU 510299-1', '3', 'ACTUALIZAR', '2017-09-06', '08:32:53', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(989, 'APPBOSA SAMAN SAMAN', '11', 'CRSU 613956-1', '4', 'ACTUALIZAR', '2017-09-06', '08:33:40', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(990, 'APPBOSA SAMAN SAMAN', '11', 'TRIU 868746-8', '1', 'ACTUALIZAR', '2017-09-06', '08:34:37', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(991, 'APPBOSA SAMAN SAMAN', '11', 'HLXU 676567-9', '1', 'ACTUALIZAR', '2017-09-06', '08:36:23', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(992, 'APPBOSA SAMAN SAMAN', '11', 'CPSU 514390-1', '2', 'ACTUALIZAR', '2017-09-06', '08:37:16', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(993, 'APPBOSA SAMAN SAMAN', '11', 'TCLU 124783-7', '1', 'ACTUALIZAR', '2017-09-06', '08:38:03', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(994, 'APPBOSA SAMAN SAMAN', '11', 'TCLU 124783-7', '1', 'ACTUALIZAR', '2017-09-06', '08:38:34', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(995, 'APPBOSA SAMAN SAMAN', '11', 'CPSU 514390-1', '2', 'ACTUALIZAR', '2017-09-06', '08:38:53', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(996, 'APPBOSA SAMAN SAMAN', '11', 'TCLU 124473--5', '2', 'ACTUALIZAR', '2017-09-06', '08:39:35', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(997, 'APPBOSA SAMAN SAMAN', '11', 'MORU 11618-9', '3', 'ACTUALIZAR', '2017-09-06', '08:40:48', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(998, 'APPBOSA SAMAN SAMAN', '12', 'MWCU 691646-5', '21606', 'ACTUALIZAR', '2017-09-06', '08:43:03', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(999, 'APPBOSA SAMAN SAMAN', '12', 'MSWU 907745-8', '21475', 'ACTUALIZAR', '2017-09-06', '08:43:55', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1000, 'APPBOSA SAMAN SAMAN', '12', 'MWCU 670039-4', '21474', 'ACTUALIZAR', '2017-09-06', '08:45:20', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1001, 'APPBOSA SAMAN SAMAN', '12', 'TEMU 921294-2', '21257', 'ACTUALIZAR', '2017-09-06', '08:48:59', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1002, 'APPBOSA SAMAN SAMAN', '12', 'CAIU 557707-0', '21598', 'ACTUALIZAR', '2017-09-06', '08:49:55', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1003, 'APPBOSA SAMAN SAMAN', '12', 'MSWU 900139-1', '21676', 'ACTUALIZAR', '2017-09-06', '08:50:36', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1004, 'APPBOSA SAMAN SAMAN', '12', 'CXRU 16199-7', '21258', 'ACTUALIZAR', '2017-09-06', '08:51:46', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1005, 'APPBOSA SAMAN SAMAN', '12', 'SUDU 520520-8', '21601', 'ACTUALIZAR', '2017-09-06', '08:52:21', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1006, 'APPBOSA SAMAN SAMAN', '12', 'SUDU 607032-3', '21603', 'ACTUALIZAR', '2017-09-06', '08:53:01', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1007, 'APPBOSA SAMAN SAMAN', '12', 'SUDU 606248-3', '21605', 'ACTUALIZAR', '2017-09-06', '08:54:11', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1008, 'APPBOSA SAMAN SAMAN', '12', 'SUDU 603038-3', '21604', 'ACTUALIZAR', '2017-09-06', '08:54:56', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1009, 'APPBOSA SAMAN SAMAN', '12', 'CPSU 512140-9', '1', 'ACTUALIZAR', '2017-09-06', '08:56:03', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1010, 'APPBOSA SAMAN SAMAN', '12', 'BMOU 920790-0', '2', 'ACTUALIZAR', '2017-09-06', '08:56:56', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1011, 'APPBOSA SAMAN SAMAN', '12', 'LNXU 755914-0', '1', 'ACTUALIZAR', '2017-09-06', '08:57:41', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1012, 'APPBOSA SAMAN SAMAN', '12', 'DFIU 260060-4', '1', 'ACTUALIZAR', '2017-09-06', '09:01:08', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1013, 'APPBOSA SAMAN SAMAN', '12', 'CXRU 116665-0', '2', 'ACTUALIZAR', '2017-09-06', '09:02:10', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1014, 'APPBOSA SAMAN SAMAN', '12', 'MORU 590143-6', '1', 'ACTUALIZAR', '2017-09-06', '09:03:10', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1015, 'APPBOSA SAMAN SAMAN', '12', 'SEGU 9392940', '2', 'ACTUALIZAR', '2017-09-06', '09:03:50', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1016, 'APPBOSA SAMAN SAMAN', '12', 'DTPU 429144-0', '2', 'ACTUALIZAR', '2017-09-06', '09:05:17', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1017, 'APPBOSA SAMAN SAMAN', '12', 'DFIU 720139-8', '3', 'ACTUALIZAR', '2017-09-06', '09:07:01', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1018, 'APPBOSA SAMAN SAMAN', '13', 'SUDU 803838-3', '21733', 'ACTUALIZAR', '2017-09-06', '09:18:01', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1019, 'APPBOSA SAMAN SAMAN', '13', 'CXRU 11095-6', '21257', 'ACTUALIZAR', '2017-09-06', '09:27:58', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1020, 'APPBOSA SAMAN SAMAN', '13', 'SUDU 601706-2', '21732', 'ACTUALIZAR', '2017-09-06', '09:29:55', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1021, 'APPBOSA SAMAN SAMAN', '13', 'SUDU 625261-0', '21731', 'ACTUALIZAR', '2017-09-06', '09:30:40', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1022, 'APPBOSA SAMAN SAMAN', '13', 'TLLU 104334-3', '21258', 'ACTUALIZAR', '2017-09-06', '09:31:46', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1023, 'APPBOSA SAMAN SAMAN', '13', 'MNBU 039996-0', '21735', 'ACTUALIZAR', '2017-09-06', '09:33:40', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1024, 'APPBOSA SAMAN SAMAN', '13', 'TCLU 138407-0', '21736', 'ACTUALIZAR', '2017-09-06', '09:34:39', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1025, 'APPBOSA SAMAN SAMAN', '13', 'CAIU 557990-0', '21737', 'ACTUALIZAR', '2017-09-06', '09:35:28', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1026, 'APPBOSA SAMAN SAMAN', '13', 'BMOU 920417-8', '21738', 'ACTUALIZAR', '2017-09-06', '09:36:27', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1027, 'APPBOSA SAMAN SAMAN', '13', 'MORU 070911-7', '1', 'ACTUALIZAR', '2017-09-06', '09:39:14', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1028, 'APPBOSA SAMAN SAMAN', '13', 'HLBU 908649-0', '1', 'ACTUALIZAR', '2017-09-06', '09:55:07', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1029, 'APPBOSA SAMAN SAMAN', '13', 'CRLU 724442-1', '1', 'ACTUALIZAR', '2017-09-06', '09:55:59', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1030, 'APPBOSA SAMAN SAMAN', '13', 'BMOU 921363-1', '2', 'ACTUALIZAR', '2017-09-06', '09:56:33', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1031, 'APPBOSA SAMAN SAMAN', '13', 'HLXU 876491-7', '1', 'ACTUALIZAR', '2017-09-06', '09:57:27', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1032, 'APPBOSA SAMAN SAMAN', '13', 'BMOU 975429-8', '2', 'ACTUALIZAR', '2017-09-06', '09:58:55', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1033, 'APPBOSA SAMAN SAMAN', '13', 'BMOU 982800-3', '1', 'ACTUALIZAR', '2017-09-06', '09:59:47', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1034, 'APPBOSA SAMAN SAMAN', '13', 'CRXU 161091-3', '2', 'ACTUALIZAR', '2017-09-06', '10:00:30', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1035, 'APPBOSA SAMAN SAMAN', '13', 'DFIU 812151-1', '3', 'ACTUALIZAR', '2017-09-06', '10:01:26', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1036, 'APPBOSA SAMAN SAMAN', '14', 'NSWU 906402-3', '21823', 'ACTUALIZAR', '2017-09-06', '10:04:30', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1037, 'APPBOSA SAMAN SAMAN', '14', 'TRIU 803714-3', '21824', 'ACTUALIZAR', '2017-09-06', '10:05:14', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1038, 'APPBOSA SAMAN SAMAN', '14', 'MMAU 100216-8', '21740', 'ACTUALIZAR', '2017-09-06', '10:05:51', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1039, 'APPBOSA SAMAN SAMAN', '14', 'SUDU 626354-9', '21817', 'ACTUALIZAR', '2017-09-06', '10:06:55', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1040, 'APPBOSA SAMAN SAMAN', '14', 'CAIU 552897-0', '21816', 'ACTUALIZAR', '2017-09-06', '10:07:50', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1041, 'APPBOSA SAMAN SAMAN', '14', 'CAIU 552897-0', '21816', 'ACTUALIZAR', '2017-09-06', '10:08:28', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1042, 'APPBOSA SAMAN SAMAN', '14', 'CXRU 162047-0', '21815', 'ACTUALIZAR', '2017-09-06', '10:16:48', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1043, 'APPBOSA SAMAN SAMAN', '14', 'CXRU 162047-0', '21815', 'ACTUALIZAR', '2017-09-06', '10:17:22', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1044, 'APPBOSA SAMAN SAMAN', '14', 'SUDU 624794-9', '21818', 'ACTUALIZAR', '2017-09-06', '10:20:19', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1045, 'APPBOSA SAMAN SAMAN', '14', 'SUDU 820917-2', '21819', 'ACTUALIZAR', '2017-09-06', '10:21:15', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1046, 'APPBOSA SAMAN SAMAN', '14', 'SUDU 607748-3', '21820', 'ACTUALIZAR', '2017-09-06', '10:22:05', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1047, 'APPBOSA SAMAN SAMAN', '14', 'SUDU 811312-6', '21822', 'ACTUALIZAR', '2017-09-06', '10:22:56', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1048, 'APPBOSA SAMAN SAMAN', '14', 'SUDU 623521-2', '21821', 'ACTUALIZAR', '2017-09-06', '10:23:42', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1049, 'APPBOSA SAMAN SAMAN', '14', 'HXLU 876200-4', '2', 'ACTUALIZAR', '2017-09-06', '10:25:01', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1050, 'APPBOSA SAMAN SAMAN', '14', 'HLBU 900241-5', '1', 'ACTUALIZAR', '2017-09-06', '10:25:59', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1051, 'APPBOSA SAMAN SAMAN', '14', 'TRLU 166726-9', '1', 'ACTUALIZAR', '2017-09-06', '10:28:58', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1052, 'APPBOSA SAMAN SAMAN', '14', 'CPSU 511506-8', '2', 'ACTUALIZAR', '2017-09-06', '10:30:27', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1053, 'APPBOSA SAMAN SAMAN', '14', 'CGMU 929863-0', '1', 'ACTUALIZAR', '2017-09-06', '10:31:12', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1054, 'APPBOSA SAMAN SAMAN', '14', 'CXRU 113270-6', '2', 'ACTUALIZAR', '2017-09-06', '10:32:48', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1055, 'APPBOSA SAMAN SAMAN', '14', 'CXRU 113270-6', '1', 'ACTUALIZAR', '2017-09-06', '10:37:02', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1056, 'APPBOSA SAMAN SAMAN', '14', 'DFIU 426376-0', '2', 'ACTUALIZAR', '2017-09-06', '10:38:30', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1057, 'APPBOSA SAMAN SAMAN', '15', 'MSWU 908240-7', '21943', 'ACTUALIZAR', '2017-09-06', '10:39:54', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1058, 'APPBOSA SAMAN SAMAN', '15', 'MNBU 309103-4', '21944', 'ACTUALIZAR', '2017-09-06', '10:40:34', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1059, 'APPBOSA SAMAN SAMAN', '15', 'MMAU 114340-1', '21945', 'ACTUALIZAR', '2017-09-06', '10:42:53', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1060, 'APPBOSA SAMAN SAMAN', '15', 'TEMU 952754-9', '21935', 'ACTUALIZAR', '2017-09-06', '10:43:35', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1061, 'APPBOSA SAMAN SAMAN', '15', 'TEMU 940125-2', '21936', 'ACTUALIZAR', '2017-09-06', '10:44:07', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1062, 'APPBOSA SAMAN SAMAN', '15', 'SUDU 519611-1', '21937', 'ACTUALIZAR', '2017-09-06', '10:44:45', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1063, 'APPBOSA SAMAN SAMAN', '15', 'SUDU 823262-9', '21938', 'ACTUALIZAR', '2017-09-06', '10:45:42', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1064, 'APPBOSA SAMAN SAMAN', '15', 'SUDU 519042-7', '21941', 'ACTUALIZAR', '2017-09-06', '10:46:41', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1065, 'APPBOSA SAMAN SAMAN', '15', 'TTNU 895700-2', '21940', 'ACTUALIZAR', '2017-09-06', '10:47:32', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1066, 'APPBOSA SAMAN SAMAN', '15', 'TTNU 896407-0', '21939', 'ACTUALIZAR', '2017-09-06', '10:48:09', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1067, 'APPBOSA SAMAN SAMAN', '15', 'CGMU 514892-4', '1', 'ACTUALIZAR', '2017-09-06', '10:50:45', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1068, 'APPBOSA SAMAN SAMAN', '15', 'BMOU 982642-2', '2', 'ACTUALIZAR', '2017-09-06', '10:51:34', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1069, 'APPBOSA SAMAN SAMAN', '15', 'DFIU 812402-2', '3', 'ACTUALIZAR', '2017-09-06', '10:52:23', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1070, 'APPBOSA SAMAN SAMAN', '15', 'TEMU 905696-3', '1', 'ACTUALIZAR', '2017-09-06', '10:53:45', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1071, 'APPBOSA SAMAN SAMAN', '15', 'HLXU 873258-7', '1', 'ACTUALIZAR', '2017-09-06', '10:55:11', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1072, 'APPBOSA SAMAN SAMAN', '15', 'HLBU 901498-8', '2', 'ACTUALIZAR', '2017-09-06', '10:56:01', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1073, 'APPBOSA SAMAN SAMAN', '15', 'CRLU 129625-9', '1', 'ACTUALIZAR', '2017-09-06', '10:57:09', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1074, 'APPBOSA SAMAN SAMAN', '15', 'SUDU 616311-2', '21822', 'ACTUALIZAR', '2017-09-06', '10:59:10', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1075, 'APPBOSA SAMAN SAMAN', '16', 'MWCU  530467-6', '22079', 'ACTUALIZAR', '2017-09-06', '11:02:03', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1076, 'APPBOSA SAMAN SAMAN', '16', 'MNBU 039353-4', '22080', 'ACTUALIZAR', '2017-09-06', '11:04:43', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1077, 'APPBOSA SAMAN SAMAN', '16', 'MMAU 105387-0', '22081', 'ACTUALIZAR', '2017-09-06', '11:05:22', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1078, 'APPBOSA SAMAN SAMAN', '16', 'MNBU 040856-8', '22089', 'ACTUALIZAR', '2017-09-06', '11:05:56', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1079, 'APPBOSA SAMAN SAMAN', '16', 'CAIU 552887-8', '22082', 'ACTUALIZAR', '2017-09-06', '11:14:28', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1080, 'APPBOSA SAMAN SAMAN', '16', 'IRNU 751068-0', '22083', 'ACTUALIZAR', '2017-09-06', '11:15:25', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1081, 'APPBOSA SAMAN SAMAN', '16', 'SUDU  900272-3', '22086', 'ACTUALIZAR', '2017-09-06', '11:26:28', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1082, 'APPBOSA SAMAN SAMAN', '16', 'TCLU 113928-3', '22084', 'ACTUALIZAR', '2017-09-06', '11:27:42', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1083, 'APPBOSA SAMAN SAMAN', '16', 'SUDU 522746-5', '22085', 'ACTUALIZAR', '2017-09-06', '11:30:14', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1084, 'APPBOSA SAMAN SAMAN', '16', 'SUDU 607649-2', '22087', 'ACTUALIZAR', '2017-09-06', '11:31:28', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1085, 'APPBOSA SAMAN SAMAN', '16', 'SUDU 616219-0', '22088', 'ACTUALIZAR', '2017-09-06', '11:32:05', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1086, 'APPBOSA SAMAN SAMAN', '16', 'CPSU 516485-9', '2', 'ACTUALIZAR', '2017-09-06', '11:32:45', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1087, 'APPBOSA SAMAN SAMAN', '16', 'CPSU 517118-5', '1', 'ACTUALIZAR', '2017-09-06', '11:33:43', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1088, 'APPBOSA SAMAN SAMAN', '16', 'SUDU 620449-0', '1', 'ACTUALIZAR', '2017-09-06', '11:34:45', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1089, 'APPBOSA SAMAN SAMAN', '16', 'SUDU 621297-9', '2', 'ACTUALIZAR', '2017-09-06', '11:35:34', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1090, 'APPBOSA SAMAN SAMAN', '16', 'TLLU 106936-9', '1', 'ACTUALIZAR', '2017-09-06', '11:36:22', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1091, 'APPBOSA SAMAN SAMAN', '16', 'TLLU 107086-3', '2', 'ACTUALIZAR', '2017-09-06', '11:37:06', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1092, 'APPBOSA SAMAN SAMAN', '16', 'LNXU 965912-8', '1', 'ACTUALIZAR', '2017-09-06', '11:38:06', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1093, 'APPBOSA SAMAN SAMAN', '16', 'HLBU 903450-0', '2', 'ACTUALIZAR', '2017-09-06', '11:39:06', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1094, 'APPBOSA SAMAN SAMAN', '16', 'LNXU 845797-1', '3', 'ACTUALIZAR', '2017-09-06', '11:39:44', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1095, 'APPBOSA SAMAN SAMAN', '17', 'MSWU 102450-7', '22182', 'ACTUALIZAR', '2017-09-06', '11:50:29', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1096, 'APPBOSA SAMAN SAMAN', '17', 'MNBU 039856-2', '22185', 'ACTUALIZAR', '2017-09-06', '11:52:03', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1097, 'APPBOSA SAMAN SAMAN', '17', 'TRIU 886482-0', '22186', 'ACTUALIZAR', '2017-09-06', '12:00:57', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1098, 'APPBOSA SAMAN SAMAN', '17', 'SUDU 513868-7', '22179', 'ACTUALIZAR', '2017-09-06', '12:08:25', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1099, 'APPBOSA SAMAN SAMAN', '17', 'SUDU 617838-6', '22178', 'ACTUALIZAR', '2017-09-06', '12:10:05', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1100, 'APPBOSA SAMAN SAMAN', '17', 'TEMU 921320-8', '22176', 'ACTUALIZAR', '2017-09-06', '12:11:38', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1101, 'APPBOSA SAMAN SAMAN', '17', 'MMAU 102304-7', '22187', 'ACTUALIZAR', '2017-09-06', '12:15:46', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1102, 'APPBOSA SAMAN SAMAN', '17', 'SUDU 618302-1', '22180', 'ACTUALIZAR', '2017-09-06', '12:18:31', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1103, 'APPBOSA SAMAN SAMAN', '17', 'CXRU 111249-0', '22175', 'ACTUALIZAR', '2017-09-06', '12:19:52', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1104, 'APPBOSA SAMAN SAMAN', '17', 'TTNU 895755-3', '22177', 'ACTUALIZAR', '2017-09-06', '12:20:51', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1105, 'APPBOSA SAMAN SAMAN', '17', 'SUDU 629540-1', '22181', 'ACTUALIZAR', '2017-09-06', '12:21:53', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1106, 'APPBOSA SAMAN SAMAN', '17', 'LNXU 755037-5', '1', 'ACTUALIZAR', '2017-09-06', '12:24:44', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1107, 'APPBOSA SAMAN SAMAN', '17', 'HLXU 875724-5', '2', 'ACTUALIZAR', '2017-09-06', '12:25:40', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1108, 'APPBOSA SAMAN SAMAN', '17', 'FSCU 565933-0', '1', 'ACTUALIZAR', '2017-09-06', '12:26:57', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1109, 'APPBOSA SAMAN SAMAN', '17', 'CRLU 7270950', '2', 'ACTUALIZAR', '2017-09-06', '12:27:54', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1110, 'APPBOSA SAMAN SAMAN', '17', 'FSCU 565933-0', '1', 'ACTUALIZAR', '2017-09-06', '12:28:21', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1111, 'APPBOSA SAMAN SAMAN', '17', 'SUDU 816408-3', '2', 'ACTUALIZAR', '2017-09-06', '12:29:09', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(1112, 'APPBOSA SAMAN SAMAN', '17', 'SUDU 620537-3', '1', 'ACTUALIZAR', '2017-09-07', '07:23:34', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1113, 'APPBOSA SAMAN SAMAN', '17', 'SUDU 816408-4', '3', 'ACTUALIZAR', '2017-09-07', '07:24:33', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1114, 'APPBOSA SAMAN SAMAN', '17', 'LMM0213991', '1', 'ACTUALIZAR', '2017-09-07', '07:25:34', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1115, 'APPBOSA SAMAN SAMAN', '17', 'IKSU 400955-4', '2', 'ACTUALIZAR', '2017-09-07', '07:28:06', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1116, 'APPBOSA SAMAN SAMAN', '18', 'MWCU 531710-1', '22312', 'ACTUALIZAR', '2017-09-07', '07:38:52', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1117, 'APPBOSA SAMAN SAMAN', '18', 'MWCU 676726-9', '22313', 'ACTUALIZAR', '2017-09-07', '08:06:34', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1118, 'APPBOSA SAMAN SAMAN', '18', 'MNBU 315979-3', '22314', 'ACTUALIZAR', '2017-09-07', '08:07:19', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1119, 'APPBOSA SAMAN SAMAN', '18', 'MMAU 102717-1', '22307', 'ACTUALIZAR', '2017-09-07', '08:08:07', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1120, 'APPBOSA SAMAN SAMAN', '18', 'PONU 482405-6', '22315', 'ACTUALIZAR', '2017-09-07', '08:08:38', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe');
INSERT INTO `audit_contenedor` (`id`, `usuario_responsable`, `semana`, `numero_contenedor`, `referencia`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(1121, 'APPBOSA SAMAN SAMAN', '18', 'CAIU 541972-7', '22307', 'ACTUALIZAR', '2017-09-07', '08:09:49', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1122, 'APPBOSA SAMAN SAMAN', '18', 'MMAU 102717-1', '22316', 'ACTUALIZAR', '2017-09-07', '08:10:39', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1123, 'APPBOSA SAMAN SAMAN', '18', 'SUDU 620305-14', '22309', 'ACTUALIZAR', '2017-09-07', '08:11:18', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1124, 'APPBOSA SAMAN SAMAN', '18', 'SUDU 607630-0', '22310', 'ACTUALIZAR', '2017-09-07', '08:11:48', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1125, 'APPBOSA SAMAN SAMAN', '18', 'SUDU 527492-9', '22317', 'ACTUALIZAR', '2017-09-07', '08:12:28', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1126, 'APPBOSA SAMAN SAMAN', '18', 'SUDU 621605-9', '22311', 'ACTUALIZAR', '2017-09-07', '08:13:08', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1127, 'APPBOSA SAMAN SAMAN', '18', 'TEMU 936989-1', '22355', 'ACTUALIZAR', '2017-09-07', '08:13:40', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1128, 'APPBOSA SAMAN SAMAN', '18', 'HLBU 900209-8', '1', 'ACTUALIZAR', '2017-09-07', '08:21:42', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1129, 'APPBOSA SAMAN SAMAN', '18', 'CAIU 558886-1', '1', 'ACTUALIZAR', '2017-09-07', '08:22:41', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1130, 'APPBOSA SAMAN SAMAN', '18', 'CAIU 558886-1', '1', 'ACTUALIZAR', '2017-09-07', '08:23:00', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1131, 'APPBOSA SAMAN SAMAN', '18', 'CAIU 553414-5', '2', 'ACTUALIZAR', '2017-09-07', '08:23:32', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1132, 'APPBOSA SAMAN SAMAN', '18', 'SUDU 509979-1', '1', 'ACTUALIZAR', '2017-09-07', '08:34:54', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1133, 'APPBOSA SAMAN SAMAN', '18', 'SUDU 522861-0', '2', 'ACTUALIZAR', '2017-09-07', '08:35:59', '200.121.222.76', 'client-200.121.222.76.speedy.net.pe'),
(1134, 'APPBOSA SAMAN SAMAN', '18', 'CPSU 511195-1', '2', 'ACTUALIZAR', '2017-09-07', '08:39:11', '190.237.236.156', '190.237.236.156'),
(1135, 'APPBOSA SAMAN SAMAN', '18', 'CPSU 511905-8', '1', 'ACTUALIZAR', '2017-09-07', '08:39:43', '190.237.236.156', '190.237.236.156'),
(1136, 'APPBOSA SAMAN SAMAN', '18', 'HLBU 902434-8', '2', 'ACTUALIZAR', '2017-09-07', '08:40:23', '190.237.236.156', '190.237.236.156'),
(1137, 'APPBOSA SAMAN SAMAN', '19', 'MSWU 100272-4', '22383', 'ACTUALIZAR', '2017-09-07', '08:52:15', '190.237.236.156', '190.237.236.156'),
(1138, 'APPBOSA SAMAN SAMAN', '19', 'MNBU 041511-9', '22384', 'ACTUALIZAR', '2017-09-07', '08:53:01', '190.237.236.156', '190.237.236.156'),
(1139, 'APPBOSA SAMAN SAMAN', '19', 'MNBU 018979-4', '22385', 'ACTUALIZAR', '2017-09-07', '08:53:53', '190.237.236.156', '190.237.236.156'),
(1140, 'APPBOSA SAMAN SAMAN', '19', 'TRIU 886892-8', '22386', 'ACTUALIZAR', '2017-09-07', '08:54:26', '190.237.236.156', '190.237.236.156'),
(1141, 'APPBOSA SAMAN SAMAN', '19', 'TRIU 886892-8', '22386', 'ACTUALIZAR', '2017-09-07', '08:54:37', '190.237.236.156', '190.237.236.156'),
(1142, 'APPBOSA SAMAN SAMAN', '19', 'MMAU 108076-7', '22387', 'ACTUALIZAR', '2017-09-07', '08:55:19', '190.237.236.156', '190.237.236.156'),
(1143, 'APPBOSA SAMAN SAMAN', '19', 'TCLU 138506-0', '22378', 'ACTUALIZAR', '2017-09-07', '08:56:03', '190.237.236.156', '190.237.236.156'),
(1144, 'APPBOSA SAMAN SAMAN', '19', 'CAIU 555044-4', '22308', 'ACTUALIZAR', '2017-09-07', '09:02:11', '190.237.236.156', '190.237.236.156'),
(1145, 'APPBOSA SAMAN SAMAN', '19', 'SUDU 815048-4', '22380', 'ACTUALIZAR', '2017-09-07', '09:03:14', '190.237.236.156', '190.237.236.156'),
(1146, 'APPBOSA SAMAN SAMAN', '19', 'TEMU 921254-1', '22379', 'ACTUALIZAR', '2017-09-07', '09:04:17', '190.237.236.156', '190.237.236.156'),
(1147, 'APPBOSA SAMAN SAMAN', '19', 'SUDU 613373-5', '22381', 'ACTUALIZAR', '2017-09-07', '09:09:16', '190.237.236.156', '190.237.236.156'),
(1148, 'APPBOSA SAMAN SAMAN', '19', 'SUDU 603298-2', '22382', 'ACTUALIZAR', '2017-09-07', '09:10:06', '190.237.236.156', '190.237.236.156'),
(1149, 'APPBOSA SAMAN SAMAN', '19', 'CRLU 722620-1', '1', 'ACTUALIZAR', '2017-09-07', '09:10:57', '190.237.236.156', '190.237.236.156'),
(1150, 'APPBOSA SAMAN SAMAN', '19', 'HXLU 873973-0', '2', 'ACTUALIZAR', '2017-09-07', '09:11:39', '190.237.236.156', '190.237.236.156'),
(1151, 'APPBOSA SAMAN SAMAN', '19', 'HXLU 8725942-8', '1', 'ACTUALIZAR', '2017-09-07', '09:12:28', '190.237.236.156', '190.237.236.156'),
(1152, 'APPBOSA SAMAN SAMAN', '19', 'CPSU 515178-5', '2', 'ACTUALIZAR', '2017-09-07', '09:13:08', '190.237.236.156', '190.237.236.156'),
(1153, 'APPBOSA SAMAN SAMAN', '19', 'DFIU 215285-5', '2', 'ACTUALIZAR', '2017-09-07', '09:14:21', '190.237.236.156', '190.237.236.156'),
(1154, 'APPBOSA SAMAN SAMAN', '19', 'SUDU 519096-2', '1', 'ACTUALIZAR', '2017-09-07', '09:14:56', '190.237.236.156', '190.237.236.156'),
(1155, 'APPBOSA SAMAN SAMAN', '19', 'SUDU 519096-2', '1', 'ACTUALIZAR', '2017-09-07', '09:15:21', '190.237.236.156', '190.237.236.156'),
(1156, 'APPBOSA SAMAN SAMAN', '19', 'SUDU 608651-0', '2', 'ACTUALIZAR', '2017-09-07', '09:15:56', '190.237.236.156', '190.237.236.156'),
(1157, 'APPBOSA SAMAN SAMAN', '19', 'SUDU 616035-0', '3', 'ACTUALIZAR', '2017-09-07', '09:16:34', '190.237.236.156', '190.237.236.156'),
(1158, 'APPBOSA SAMAN SAMAN', '19', 'DFIU 210486-2', '1', 'ACTUALIZAR', '2017-09-07', '09:17:16', '190.237.236.156', '190.237.236.156'),
(1159, 'APPBOSA SAMAN SAMAN', '20', 'PONU 481624-0', '22513', 'ACTUALIZAR', '2017-09-07', '09:19:40', '190.237.236.156', '190.237.236.156'),
(1160, 'APPBOSA SAMAN SAMAN', '20', 'MNBU 040678-1', '22514', 'ACTUALIZAR', '2017-09-07', '09:20:26', '190.237.236.156', '190.237.236.156'),
(1161, 'APPBOSA SAMAN SAMAN', '20', 'MMAU 119589-5', '22515', 'ACTUALIZAR', '2017-09-07', '09:21:13', '190.237.236.156', '190.237.236.156'),
(1162, 'APPBOSA SAMAN SAMAN', '20', 'BMOU 961456-8', '22507', 'ACTUALIZAR', '2017-09-07', '09:22:06', '190.237.236.156', '190.237.236.156'),
(1163, 'APPBOSA SAMAN SAMAN', '20', 'BMOU 961479-0', '22506', 'ACTUALIZAR', '2017-09-07', '09:22:39', '190.237.236.156', '190.237.236.156'),
(1164, 'APPBOSA SAMAN SAMAN', '20', 'BMOU 961408-5', '22505', 'ACTUALIZAR', '2017-09-07', '09:23:40', '190.237.236.156', '190.237.236.156'),
(1165, 'APPBOSA SAMAN SAMAN', '20', 'CXRU 153116-2', '22508', 'ACTUALIZAR', '2017-09-07', '09:24:35', '190.237.236.156', '190.237.236.156'),
(1166, 'APPBOSA SAMAN SAMAN', '20', 'SUDU 603547-2', '22509', 'ACTUALIZAR', '2017-09-07', '09:25:10', '190.237.236.156', '190.237.236.156'),
(1167, 'APPBOSA SAMAN SAMAN', '20', 'SUDU 527211-9', '22510', 'ACTUALIZAR', '2017-09-07', '09:25:35', '190.237.236.156', '190.237.236.156'),
(1168, 'APPBOSA SAMAN SAMAN', '20', 'SUDU 514549-6', '22512', 'ACTUALIZAR', '2017-09-07', '09:26:16', '190.237.236.156', '190.237.236.156'),
(1169, 'APPBOSA SAMAN SAMAN', '20', 'SUDU 610493-2', '22574', 'ACTUALIZAR', '2017-09-07', '09:26:57', '190.237.236.156', '190.237.236.156'),
(1170, 'APPBOSA SAMAN SAMAN', '20', 'SUDU 622437-3', '22511', 'ACTUALIZAR', '2017-09-07', '09:27:37', '190.237.236.156', '190.237.236.156'),
(1171, 'APPBOSA SAMAN SAMAN', '20', 'HLXU 876061-3', '2', 'ACTUALIZAR', '2017-09-07', '09:29:53', '190.237.236.156', '190.237.236.156'),
(1172, 'APPBOSA SAMAN SAMAN', '20', 'DTPU 721041-5', '2', 'ACTUALIZAR', '2017-09-07', '09:32:08', '190.237.236.156', '190.237.236.156'),
(1173, 'APPBOSA SAMAN SAMAN', '20', 'DFIU 722043-8', '1', 'ACTUALIZAR', '2017-09-07', '09:34:02', '190.237.236.156', '190.237.236.156'),
(1174, 'APPBOSA SAMAN SAMAN', '20', 'HLBU 900941-0', '1', 'ACTUALIZAR', '2017-09-07', '09:34:57', '190.237.236.156', '190.237.236.156'),
(1175, 'APPBOSA SAMAN SAMAN', '20', 'FSCU 565916-0', '1', 'ACTUALIZAR', '2017-09-07', '09:35:42', '190.237.236.156', '190.237.236.156'),
(1176, 'APPBOSA SAMAN SAMAN', '20', 'SUDU 626452-4', '1', 'ACTUALIZAR', '2017-09-07', '09:36:14', '190.237.236.156', '190.237.236.156'),
(1177, 'APPBOSA SAMAN SAMAN', '20', 'CPSU 510199-5', '2', 'INSERTAR', '2017-09-07', '09:38:55', '190.237.236.156', '190.237.236.156'),
(1178, 'APPBOSA SAMAN SAMAN', '21', 'PONU 497834-4', '22595', 'ACTUALIZAR', '2017-09-07', '09:40:51', '190.237.236.156', '190.237.236.156'),
(1179, 'APPBOSA SAMAN SAMAN', '21', 'MNBU 315895-0', '22596', 'ACTUALIZAR', '2017-09-07', '09:41:09', '190.237.236.156', '190.237.236.156'),
(1180, 'APPBOSA SAMAN SAMAN', '21', 'PONU 480767-6', '22597', 'ACTUALIZAR', '2017-09-07', '09:41:26', '190.237.236.156', '190.237.236.156'),
(1181, 'APPBOSA SAMAN SAMAN', '21', 'PONU 482052-8', '22598', 'ACTUALIZAR', '2017-09-07', '09:41:42', '190.237.236.156', '190.237.236.156'),
(1182, 'APPBOSA SAMAN SAMAN', '21', 'MMAU 107222-6', '22599', 'ACTUALIZAR', '2017-09-07', '09:42:07', '190.237.236.156', '190.237.236.156'),
(1183, 'APPBOSA SAMAN SAMAN', '21', 'SUDU 605016-3', '22591', 'ACTUALIZAR', '2017-09-07', '09:42:37', '190.237.236.156', '190.237.236.156'),
(1184, 'APPBOSA SAMAN SAMAN', '21', 'BMOU 961635-0', '22590', 'ACTUALIZAR', '2017-09-07', '09:54:31', '181.67.154.141', '181.67.154.141'),
(1185, 'APPBOSA SAMAN SAMAN', '21', 'TTNU 896246-2', '22589', 'ACTUALIZAR', '2017-09-07', '09:54:46', '181.67.154.141', '181.67.154.141'),
(1186, 'APPBOSA SAMAN SAMAN', '21', 'BMOU 965137-1', '22588', 'ACTUALIZAR', '2017-09-07', '09:54:59', '181.67.154.141', '181.67.154.141'),
(1187, 'APPBOSA SAMAN SAMAN', '21', 'SUDU 529208-0', '22594', 'ACTUALIZAR', '2017-09-07', '09:55:18', '181.67.154.141', '181.67.154.141'),
(1188, 'APPBOSA SAMAN SAMAN', '21', 'SUDU 604948-1', '22593', 'ACTUALIZAR', '2017-09-07', '09:55:38', '181.67.154.141', '181.67.154.141'),
(1189, 'APPBOSA SAMAN SAMAN', '21', 'SUDU 605417-4', '22592', 'ACTUALIZAR', '2017-09-07', '09:55:58', '181.67.154.141', '181.67.154.141'),
(1190, 'APPBOSA SAMAN SAMAN', '21', 'HLXU 876009-0', '1', 'ACTUALIZAR', '2017-09-07', '09:56:29', '181.67.154.141', '181.67.154.141'),
(1191, 'APPBOSA SAMAN SAMAN', '21', 'HLBU 900451-0', '2', 'ACTUALIZAR', '2017-09-07', '09:56:52', '181.67.154.141', '181.67.154.141'),
(1192, 'APPBOSA SAMAN SAMAN', '21', 'TCLU 102844-3', '1', 'ACTUALIZAR', '2017-09-07', '09:57:16', '181.67.154.141', '181.67.154.141'),
(1193, 'APPBOSA SAMAN SAMAN', '21', 'CRLU 137750-9', '2', 'ACTUALIZAR', '2017-09-07', '10:00:50', '181.67.154.141', '181.67.154.141'),
(1194, 'APPBOSA SAMAN SAMAN', '21', 'TLLU 106897-4', '1', 'ACTUALIZAR', '2017-09-07', '10:01:17', '181.67.154.141', '181.67.154.141'),
(1195, 'APPBOSA SAMAN SAMAN', '21', 'SUDU 823613-6', '1', 'ACTUALIZAR', '2017-09-07', '10:01:51', '181.67.154.141', '181.67.154.141'),
(1196, 'APPBOSA SAMAN SAMAN', '21', 'SUDU  523335-0', '2', 'ACTUALIZAR', '2017-09-07', '10:02:05', '181.67.154.141', '181.67.154.141'),
(1197, 'APPBOSA SAMAN SAMAN', '21', 'CXRU 161009-2', '2', 'ACTUALIZAR', '2017-09-07', '10:02:19', '181.67.154.141', '181.67.154.141'),
(1198, 'APPBOSA SAMAN SAMAN', '22', 'MWCU 657259-1', '22694', 'ACTUALIZAR', '2017-09-07', '10:06:45', '181.67.154.141', '181.67.154.141'),
(1199, 'APPBOSA SAMAN SAMAN', '22', 'DAYU 670586-6', '22695', 'ACTUALIZAR', '2017-09-07', '10:07:08', '181.67.154.141', '181.67.154.141'),
(1200, 'APPBOSA SAMAN SAMAN', '22', 'MSWU 102357-9', '22696', 'ACTUALIZAR', '2017-09-07', '10:07:26', '181.67.154.141', '181.67.154.141'),
(1201, 'APPBOSA SAMAN SAMAN', '22', 'MMAU 107723-3', '22698', 'ACTUALIZAR', '2017-09-07', '10:07:52', '181.67.154.141', '181.67.154.141'),
(1202, 'APPBOSA SAMAN SAMAN', '22', 'TCLU 138551-7', '22687', 'ACTUALIZAR', '2017-09-07', '10:08:12', '181.67.154.141', '181.67.154.141'),
(1203, 'APPBOSA SAMAN SAMAN', '22', 'CNIU 222198-8', '22690', 'ACTUALIZAR', '2017-09-07', '10:18:52', '181.67.154.141', '181.67.154.141'),
(1204, 'APPBOSA SAMAN SAMAN', '22', 'RRSU 100351-4', '22689', 'ACTUALIZAR', '2017-09-07', '10:19:08', '181.67.154.141', '181.67.154.141'),
(1205, 'APPBOSA SAMAN SAMAN', '22', 'SZLU 980331-0', '22688', 'ACTUALIZAR', '2017-09-07', '10:19:28', '181.67.154.141', '181.67.154.141'),
(1206, 'APPBOSA SAMAN SAMAN', '22', 'CSVU 750614-3', '2', 'ACTUALIZAR', '2017-09-07', '10:20:13', '181.67.154.141', '181.67.154.141'),
(1207, 'APPBOSA SAMAN SAMAN', '22', 'HXLU 877232-1', '1', 'ACTUALIZAR', '2017-09-07', '10:20:33', '181.67.154.141', '181.67.154.141'),
(1208, 'APPBOSA SAMAN SAMAN', '22', 'TEMU 906017-7', '22693', 'ACTUALIZAR', '2017-09-07', '10:20:55', '181.67.154.141', '181.67.154.141'),
(1209, 'APPBOSA SAMAN SAMAN', '22', 'SUDU 600809-7', '22691', 'ACTUALIZAR', '2017-09-07', '10:21:27', '181.67.154.141', '181.67.154.141'),
(1210, 'APPBOSA SAMAN SAMAN', '22', 'SUDU 616397-7', '22692', 'ACTUALIZAR', '2017-09-07', '10:21:49', '181.67.154.141', '181.67.154.141'),
(1211, 'APPBOSA SAMAN SAMAN', '22', 'DFIU 215125-2', '1', 'ACTUALIZAR', '2017-09-07', '10:22:11', '181.67.154.141', '181.67.154.141'),
(1212, 'APPBOSA SAMAN SAMAN', '22', 'SUDU 621885-3', '1', 'ACTUALIZAR', '2017-09-07', '10:22:31', '181.67.154.141', '181.67.154.141'),
(1213, 'APPBOSA SAMAN SAMAN', '22', 'MSCU 7433652', '1', 'ACTUALIZAR', '2017-09-07', '10:22:54', '181.67.154.141', '181.67.154.141'),
(1214, 'APPBOSA SAMAN SAMAN', '23', 'MNBU 308760-4', '22796', 'ACTUALIZAR', '2017-09-07', '10:24:29', '181.67.154.141', '181.67.154.141'),
(1215, 'APPBOSA SAMAN SAMAN', '23', 'MMAU 124524-0', '22798', 'ACTUALIZAR', '2017-09-07', '10:25:11', '181.67.154.141', '181.67.154.141'),
(1216, 'APPBOSA SAMAN SAMAN', '23', 'TRIU 835537-6', '22787', 'ACTUALIZAR', '2017-09-07', '10:25:38', '181.67.154.141', '181.67.154.141'),
(1217, 'APPBOSA SAMAN SAMAN', '23', 'MNBU 041418-0', '22797', 'ACTUALIZAR', '2017-09-07', '10:26:06', '181.67.154.141', '181.67.154.141'),
(1218, 'APPBOSA SAMAN SAMAN', '23', 'CAIU 554907-9', '22789', 'ACTUALIZAR', '2017-09-07', '10:26:29', '181.67.154.141', '181.67.154.141'),
(1219, 'APPBOSA SAMAN SAMAN', '23', 'CPSU 515501-3', '1', 'ACTUALIZAR', '2017-09-07', '10:26:49', '181.67.154.141', '181.67.154.141'),
(1220, 'APPBOSA SAMAN SAMAN', '23', 'HLXU 877275-9', '1', 'ACTUALIZAR', '2017-09-07', '10:27:26', '181.67.154.141', '181.67.154.141'),
(1221, 'APPBOSA SAMAN SAMAN', '23', 'SUDU 625143-0', '22790', 'ACTUALIZAR', '2017-09-07', '10:27:42', '181.67.154.141', '181.67.154.141'),
(1222, 'APPBOSA SAMAN SAMAN', '23', 'TRIU 811487-2', '22788', 'ACTUALIZAR', '2017-09-07', '10:27:58', '181.67.154.141', '181.67.154.141'),
(1223, 'APPBOSA SAMAN SAMAN', '23', 'SUDU 800455-2', '22791', 'ACTUALIZAR', '2017-09-07', '10:28:29', '181.67.154.141', '181.67.154.141'),
(1224, 'APPBOSA SAMAN SAMAN', '23', 'HLXU 876189-9', '2', 'ACTUALIZAR', '2017-09-07', '10:28:57', '181.67.154.141', '181.67.154.141'),
(1225, 'APPBOSA SAMAN SAMAN', '23', 'SUDU 823377-5', '22794', 'ACTUALIZAR', '2017-09-07', '10:29:35', '181.67.154.141', '181.67.154.141'),
(1226, 'APPBOSA SAMAN SAMAN', '23', 'SUDU 801296-4', '22792', 'ACTUALIZAR', '2017-09-07', '10:29:55', '181.67.154.141', '181.67.154.141'),
(1227, 'APPBOSA SAMAN SAMAN', '23', 'DFIU 811104-6', '2', 'ACTUALIZAR', '2017-09-07', '10:30:18', '181.67.154.141', '181.67.154.141'),
(1228, 'APPBOSA SAMAN SAMAN', '23', 'SUDU 623810-3', '1', 'ACTUALIZAR', '2017-09-07', '10:30:41', '181.67.154.141', '181.67.154.141'),
(1229, 'APPBOSA SAMAN SAMAN', '23', 'SUDU 627365-5', '2', 'ACTUALIZAR', '2017-09-07', '10:31:01', '181.67.154.141', '181.67.154.141'),
(1230, 'APPBOSA SAMAN SAMAN', '23', 'TLLU 104789-0', '1', 'ACTUALIZAR', '2017-09-07', '10:31:22', '181.67.154.141', '181.67.154.141'),
(1231, 'APPBOSA SAMAN SAMAN', '23', 'HLXU 874770-9', '2', 'ACTUALIZAR', '2017-09-07', '10:31:47', '181.67.154.141', '181.67.154.141'),
(1232, 'APPBOSA SAMAN SAMAN', '24', 'MNBU 365704-0', '22903', 'ACTUALIZAR', '2017-09-07', '10:41:21', '181.67.154.141', '181.67.154.141'),
(1233, 'APPBOSA SAMAN SAMAN', '24', 'MWCU 678940-0', '22904', 'ACTUALIZAR', '2017-09-07', '10:41:42', '181.67.154.141', '181.67.154.141'),
(1234, 'APPBOSA SAMAN SAMAN', '24', 'CAIU 556259-5', '22895', 'ACTUALIZAR', '2017-09-07', '10:42:09', '181.67.154.141', '181.67.154.141'),
(1235, 'APPBOSA SAMAN SAMAN', '24', 'SUDU 628780-7', '22900', 'ACTUALIZAR', '2017-09-07', '10:42:23', '181.67.154.141', '181.67.154.141'),
(1236, 'APPBOSA SAMAN SAMAN', '24', 'SUDU 629428-3', '22901', 'ACTUALIZAR', '2017-09-07', '10:42:40', '181.67.154.141', '181.67.154.141'),
(1237, 'APPBOSA SAMAN SAMAN', '24', 'SUDU 801152-5', '22899', 'ACTUALIZAR', '2017-09-07', '10:42:55', '181.67.154.141', '181.67.154.141'),
(1238, 'APPBOSA SAMAN SAMAN', '24', 'SUDU 803505-0', '22898', 'ACTUALIZAR', '2017-09-07', '10:43:15', '181.67.154.141', '181.67.154.141'),
(1239, 'APPBOSA SAMAN SAMAN', '24', 'CAIU 556413-4', '22896', 'ACTUALIZAR', '2017-09-07', '10:43:37', '181.67.154.141', '181.67.154.141'),
(1240, 'APPBOSA SAMAN SAMAN', '24', 'MMAU 111927-8', '22905', 'ACTUALIZAR', '2017-09-07', '10:43:59', '181.67.154.141', '181.67.154.141'),
(1241, 'APPBOSA SAMAN SAMAN', '24', 'SZLU 980178-6', '22897', 'ACTUALIZAR', '2017-09-07', '10:44:28', '181.67.154.141', '181.67.154.141'),
(1242, 'APPBOSA SAMAN SAMAN', '24', 'SUDU 801981-9', '22902', 'ACTUALIZAR', '2017-09-07', '10:44:59', '181.67.154.141', '181.67.154.141'),
(1243, 'APPBOSA SAMAN SAMAN', '24', 'HLXU 875946-4', '1', 'ACTUALIZAR', '2017-09-07', '10:47:05', '181.67.154.141', '181.67.154.141'),
(1244, 'APPBOSA SAMAN SAMAN', '24', 'SUDU 810704-1', '2', 'ACTUALIZAR', '2017-09-07', '10:48:29', '181.67.154.141', '181.67.154.141'),
(1245, 'APPBOSA SAMAN SAMAN', '24', 'BMOU 964121-8', '2', 'ACTUALIZAR', '2017-09-07', '10:49:05', '181.67.154.141', '181.67.154.141'),
(1246, 'APPBOSA SAMAN SAMAN', '24', 'SUDU 803122-3', '2', 'ACTUALIZAR', '2017-09-07', '10:49:52', '181.67.154.141', '181.67.154.141'),
(1247, 'APPBOSA SAMAN SAMAN', '24', 'TLLU 105911-8', '1', 'ACTUALIZAR', '2017-09-07', '10:50:19', '181.67.154.141', '181.67.154.141'),
(1248, 'APPBOSA SAMAN SAMAN', '24', 'HLBU 902370-0', '2', 'ACTUALIZAR', '2017-09-07', '10:50:48', '181.67.154.141', '181.67.154.141'),
(1249, 'APPBOSA SAMAN SAMAN', '24', 'TCLU 117750-8', '1', 'ACTUALIZAR', '2017-09-07', '10:51:21', '181.67.154.141', '181.67.154.141'),
(1250, 'APPBOSA SAMAN SAMAN', '24', 'HLBU 900823-9', '2', 'ACTUALIZAR', '2017-09-07', '10:51:43', '181.67.154.141', '181.67.154.141'),
(1251, 'APPBOSA SAMAN SAMAN', '24', 'TLLU 105911-8', '1', 'ACTUALIZAR', '2017-09-07', '10:52:16', '181.67.154.141', '181.67.154.141'),
(1252, 'APPBOSA SAMAN SAMAN', '25', 'SUDU 821029-7', '23001', 'ACTUALIZAR', '2017-09-07', '11:34:53', '181.67.154.141', '181.67.154.141'),
(1253, 'APPBOSA SAMAN SAMAN', '25', 'PONU 484117-7', '23005', 'ACTUALIZAR', '2017-09-07', '11:35:12', '181.67.154.141', '181.67.154.141'),
(1254, 'APPBOSA SAMAN SAMAN', '25', 'PONU 483173-3', '23004', 'ACTUALIZAR', '2017-09-07', '11:35:28', '181.67.154.141', '181.67.154.141'),
(1255, 'APPBOSA SAMAN SAMAN', '25', 'CXRU 111677-3', '22997', 'ACTUALIZAR', '2017-09-07', '11:35:57', '181.67.154.141', '181.67.154.141'),
(1256, 'APPBOSA SAMAN SAMAN', '25', 'MMAU 101782-5', '23006', 'ACTUALIZAR', '2017-09-07', '11:36:51', '181.67.154.141', '181.67.154.141'),
(1257, 'APPBOSA SAMAN SAMAN', '25', 'TRIU 859069-4', '22998', 'ACTUALIZAR', '2017-09-07', '11:37:09', '181.67.154.141', '181.67.154.141'),
(1258, 'APPBOSA SAMAN SAMAN', '25', 'SUDU 805116-9', '23002', 'ACTUALIZAR', '2017-09-07', '11:37:30', '181.67.154.141', '181.67.154.141'),
(1259, 'APPBOSA SAMAN SAMAN', '25', 'CXRU 152387-1', '22999', 'ACTUALIZAR', '2017-09-07', '11:37:49', '181.67.154.141', '181.67.154.141'),
(1260, 'APPBOSA SAMAN SAMAN', '25', 'SUDU 811441-5', '23000', 'ACTUALIZAR', '2017-09-07', '11:38:10', '181.67.154.141', '181.67.154.141'),
(1261, 'APPBOSA SAMAN SAMAN', '25', 'LNXU 845859-8', '1', 'ACTUALIZAR', '2017-09-07', '11:38:47', '181.67.154.141', '181.67.154.141'),
(1262, 'APPBOSA SAMAN SAMAN', '25', 'HLBU 904294-8', '1', 'ACTUALIZAR', '2017-09-07', '11:39:22', '181.67.154.141', '181.67.154.141'),
(1263, 'APPBOSA SAMAN SAMAN', '25', 'HLBU 904186-0', '2', 'ACTUALIZAR', '2017-09-07', '11:40:53', '181.67.154.141', '181.67.154.141'),
(1264, 'APPBOSA SAMAN SAMAN', '25', 'CPSU 511286-0', '2', 'ACTUALIZAR', '2017-09-07', '11:41:27', '181.67.154.141', '181.67.154.141'),
(1265, 'APPBOSA SAMAN SAMAN', '25', 'DFIU 723082-1', '2', 'ACTUALIZAR', '2017-09-07', '11:41:57', '181.67.154.141', '181.67.154.141'),
(1266, 'APPBOSA SAMAN SAMAN', '25', 'SUDU 810355-5', '2', 'ACTUALIZAR', '2017-09-07', '11:42:17', '181.67.154.141', '181.67.154.141'),
(1267, 'APPBOSA SAMAN SAMAN', '25', 'CAIU 558762-8', '1', 'ACTUALIZAR', '2017-09-07', '11:42:38', '181.67.154.141', '181.67.154.141'),
(1268, 'APPBOSA SAMAN SAMAN', '25', 'SUDU 817060-9', '23003', 'ACTUALIZAR', '2017-09-07', '11:43:09', '181.67.154.141', '181.67.154.141'),
(1269, 'APPBOSA SAMAN SAMAN', '25', 'SUDU 803934-8', '1', 'ACTUALIZAR', '2017-09-07', '11:43:34', '181.67.154.141', '181.67.154.141'),
(1270, 'APPBOSA SAMAN SAMAN', '26', 'PONU 498186-2', '23118', 'ACTUALIZAR', '2017-09-07', '11:45:27', '181.67.154.141', '181.67.154.141'),
(1271, 'APPBOSA SAMAN SAMAN', '27', 'MWCU670752-6', '23156', 'ACTUALIZAR', '2017-09-07', '11:47:47', '181.67.154.141', '181.67.154.141'),
(1272, 'APPBOSA SAMAN SAMAN', '29', 'PONU 489829-6', '23348', 'ACTUALIZAR', '2017-09-07', '11:51:04', '181.67.154.141', '181.67.154.141'),
(1273, 'APPBOSA SAMAN SAMAN', '33', 'SUDU 809810-3', '23748', 'ACTUALIZAR', '2017-09-07', '11:59:10', '181.67.154.141', '181.67.154.141'),
(1274, 'APPBOSA SAMAN SAMAN', '35', 'MNBU 365450-2', '23974', 'ACTUALIZAR', '2017-09-08', '10:55:32', '181.67.154.141', '181.67.154.141'),
(1275, 'APPBOSA SAMAN SAMAN', '35', 'MMAU 111331-0', '23976', 'ACTUALIZAR', '2017-09-08', '10:56:05', '181.67.154.141', '181.67.154.141'),
(1276, 'APPBOSA SAMAN SAMAN', '35', 'MWCU 523233-9', '23975', 'ACTUALIZAR', '2017-09-08', '10:56:26', '181.67.154.141', '181.67.154.141'),
(1277, 'APPBOSA SAMAN SAMAN', '35', 'PONU 483751-5', '23973', 'ACTUALIZAR', '2017-09-08', '10:56:44', '181.67.154.141', '181.67.154.141'),
(1278, 'APPBOSA SAMAN SAMAN', '35', 'CXRU 111338-9', '23963', 'ACTUALIZAR', '2017-09-08', '10:57:15', '181.67.154.141', '181.67.154.141'),
(1279, 'APPBOSA SAMAN SAMAN', '35', 'CAIU 542161-6', '23967', 'ACTUALIZAR', '2017-09-08', '10:57:51', '181.67.154.141', '181.67.154.141'),
(1280, 'APPBOSA SAMAN SAMAN', '35', 'TRIU 859121-6', '23966', 'ACTUALIZAR', '2017-09-08', '10:58:13', '181.67.154.141', '181.67.154.141'),
(1281, 'APPBOSA SAMAN SAMAN', '35', 'SUDU 615306-9', '23970', 'ACTUALIZAR', '2017-09-08', '10:58:32', '181.67.154.141', '181.67.154.141'),
(1282, 'APPBOSA SAMAN SAMAN', '35', 'SUDU 621344-5', '23971', 'ACTUALIZAR', '2017-09-08', '10:58:52', '181.67.154.141', '181.67.154.141'),
(1283, 'APPBOSA SAMAN SAMAN', '35', 'SUDU 608150-2', '23972', 'ACTUALIZAR', '2017-09-08', '10:59:30', '181.67.154.141', '181.67.154.141'),
(1284, 'APPBOSA SAMAN SAMAN', '35', 'SUDU 608761-9', '23980', 'ACTUALIZAR', '2017-09-08', '10:59:47', '181.67.154.141', '181.67.154.141'),
(1285, 'APPBOSA SAMAN SAMAN', '35', 'SUDU 627006-5', '23968', 'ACTUALIZAR', '2017-09-08', '11:00:08', '181.67.154.141', '181.67.154.141'),
(1286, 'APPBOSA SAMAN SAMAN', '35', 'SUDU 628288-9', '23969', 'ACTUALIZAR', '2017-09-08', '11:00:25', '181.67.154.141', '181.67.154.141'),
(1287, 'APPBOSA SAMAN SAMAN', '35', 'CRLU 130221-7', '1', 'ACTUALIZAR', '2017-09-08', '11:00:57', '181.67.154.141', '181.67.154.141'),
(1288, 'APPBOSA SAMAN SAMAN', '35', 'TLLU 105716-2', '1', 'ACTUALIZAR', '2017-09-08', '11:02:22', '181.67.154.141', '181.67.154.141'),
(1289, 'APPBOSA SAMAN SAMAN', '35', 'CRLU 150303-7', '1', 'ACTUALIZAR', '2017-09-08', '11:02:50', '181.67.154.141', '181.67.154.141'),
(1290, 'APPBOSA SAMAN SAMAN', '35', 'CRLU 126164-8', '1', 'ACTUALIZAR', '2017-09-08', '11:03:19', '181.67.154.141', '181.67.154.141'),
(1291, 'APPBOSA SAMAN SAMAN', '35', 'HLXU 875617-2', '1', 'ACTUALIZAR', '2017-09-08', '11:04:37', '181.67.154.141', '181.67.154.141'),
(1292, 'APPBOSA SAMAN SAMAN', '35', 'SUDU 629572-0', '1', 'ACTUALIZAR', '2017-09-08', '11:05:00', '181.67.154.141', '181.67.154.141'),
(1293, 'APPBOSA SAMAN SAMAN', '35', 'SUDU 620492-6', '1', 'ACTUALIZAR', '2017-09-08', '11:05:16', '181.67.154.141', '181.67.154.141'),
(1294, 'APPBOSA SAMAN SAMAN', '35', 'TLLU 105927-3', '1', 'ACTUALIZAR', '2017-09-08', '11:06:02', '181.67.154.141', '181.67.154.141'),
(1295, 'Juan Calderón More', '36', 'CXRU 128422-6', '24037', 'ACTUALIZAR', '2017-09-12', '08:02:43', '190.237.236.83', '190.237.236.83'),
(1296, 'Juan Calderón More', '36', 'MMBU 325216-5', '24043', 'ACTUALIZAR', '2017-09-12', '08:05:45', '190.237.236.83', '190.237.236.83'),
(1297, 'Juan Calderón More', '36', 'SUDU 800714-5', '24103', 'ACTUALIZAR', '2017-09-12', '08:08:44', '190.237.236.83', '190.237.236.83'),
(1298, 'Juan Calderón More', '36', 'SUDU 8000702-1', '24102', 'ACTUALIZAR', '2017-09-12', '08:10:07', '190.237.236.83', '190.237.236.83'),
(1299, 'Juan Calderón More', '36', 'SUDU 824044-0', '24042', 'ACTUALIZAR', '2017-09-12', '08:12:34', '190.237.236.83', '190.237.236.83'),
(1300, 'Juan Calderón More', '36', 'SUDU 816768-9', '24041', 'ACTUALIZAR', '2017-09-12', '08:14:07', '190.237.236.83', '190.237.236.83'),
(1301, 'Juan Calderón More', '36', 'TTNU 836364-3', '24040', 'ACTUALIZAR', '2017-09-12', '08:16:49', '190.237.236.83', '190.237.236.83'),
(1302, 'Juan Calderón More', '36', 'CXRU 152742-9', '24039', 'ACTUALIZAR', '2017-09-12', '08:40:09', '190.237.236.83', '190.237.236.83'),
(1303, 'Juan Calderón More', '36', 'CAIU 542053-8', '24038', 'ACTUALIZAR', '2017-09-12', '08:41:35', '190.237.236.83', '190.237.236.83'),
(1304, 'Juan Calderón More', '36', 'MWCU 692721-7', '24044', 'ACTUALIZAR', '2017-09-12', '08:43:32', '190.237.236.83', '190.237.236.83'),
(1305, 'Juan Calderón More', '36', 'MWCU 695324-2', '24045', 'ACTUALIZAR', '2017-09-12', '08:44:59', '190.237.236.83', '190.237.236.83'),
(1306, 'Juan Calderón More', '36', 'MSWU 901014-0', '24046', 'ACTUALIZAR', '2017-09-12', '08:45:56', '190.237.236.83', '190.237.236.83'),
(1307, 'Juan Calderón More', '36', 'TLLU 106984-1', '1', 'ACTUALIZAR', '2017-09-12', '08:49:00', '190.237.236.83', '190.237.236.83'),
(1308, 'Juan Calderón More', '36', 'TCLU 102664-6', '1', 'ACTUALIZAR', '2017-09-12', '08:50:36', '190.237.236.83', '190.237.236.83'),
(1309, 'Juan Calderón More', '36', 'MMAU 101604-8', '24047', 'ACTUALIZAR', '2017-09-12', '08:53:11', '190.237.236.83', '190.237.236.83'),
(1310, 'Juan Calderón More', '36', 'SUDU 822627-2', '1', 'ACTUALIZAR', '2017-09-12', '08:56:53', '190.237.236.83', '190.237.236.83'),
(1311, 'Juan Calderón More', '36', 'SUDU 814923-7', '1', 'ACTUALIZAR', '2017-09-12', '08:58:07', '190.237.236.83', '190.237.236.83'),
(1312, 'Juan Calderón More', '36', 'HLXU 673488-9', '1', 'ACTUALIZAR', '2017-09-12', '08:59:37', '190.237.236.83', '190.237.236.83'),
(1313, 'Juan Calderón More', '36', 'UACO 472996-0', '1', 'ACTUALIZAR', '2017-09-12', '09:01:10', '190.237.236.83', '190.237.236.83'),
(1314, 'Juan Calderón More', '36', 'CPSU 516498-8', '1', 'ACTUALIZAR', '2017-09-12', '09:02:58', '190.237.236.83', '190.237.236.83'),
(1315, 'Juan Calderón More', '36', 'TCLU 120483-5', '1', 'ACTUALIZAR', '2017-09-12', '09:05:35', '190.237.236.83', '190.237.236.83'),
(1316, 'Juan Calderón More', '36', 'TLLU 106960-4', '1', 'ACTUALIZAR', '2017-09-12', '09:07:19', '190.237.236.83', '190.237.236.83'),
(1317, 'Juan Calderón More', '36', 'SUDU 816768-9', '24135', 'INSERTAR', '2017-09-12', '09:10:51', '190.237.236.83', '190.237.236.83'),
(1318, 'Juan Calderón More', '36', 'UACO 472996-0', '1', 'INSERTAR', '2017-09-12', '09:12:37', '190.237.236.83', '190.237.236.83'),
(1319, 'APPBOSA SAMAN SAMAN', '36', 'MSWU 901014-0', '24046', 'ACTUALIZAR', '2017-09-13', '07:42:21', '190.42.217.195', '190.42.217.195'),
(1320, 'APPBOSA SAMAN SAMAN', '36', 'MWCU 695324-2', '24045', 'ACTUALIZAR', '2017-09-13', '07:42:36', '190.42.217.195', '190.42.217.195'),
(1321, 'APPBOSA SAMAN SAMAN', '36', 'MWCU 692721-7', '24044', 'ACTUALIZAR', '2017-09-13', '07:42:59', '190.42.217.195', '190.42.217.195'),
(1322, 'APPBOSA SAMAN SAMAN', '36', 'MMBU 325216-5', '24043', 'ACTUALIZAR', '2017-09-13', '07:43:15', '190.42.217.195', '190.42.217.195'),
(1323, 'APPBOSA SAMAN SAMAN', '36', 'MMAU 101604-8', '24047', 'ACTUALIZAR', '2017-09-13', '07:43:34', '190.42.217.195', '190.42.217.195'),
(1324, 'APPBOSA SAMAN SAMAN', '36', 'CXRU 128422-6', '24037', 'ACTUALIZAR', '2017-09-13', '07:43:56', '190.42.217.195', '190.42.217.195'),
(1325, 'APPBOSA SAMAN SAMAN', '36', 'CXRU 128422-6', '24037', 'ACTUALIZAR', '2017-09-13', '07:44:05', '190.42.217.195', '190.42.217.195'),
(1326, 'APPBOSA SAMAN SAMAN', '36', 'CAIU 542053-8', '24038', 'ACTUALIZAR', '2017-09-13', '07:44:27', '190.42.217.195', '190.42.217.195'),
(1327, 'APPBOSA SAMAN SAMAN', '36', 'SUDU 800702-1', '24102', 'ACTUALIZAR', '2017-09-13', '07:44:48', '190.42.217.195', '190.42.217.195'),
(1328, 'APPBOSA SAMAN SAMAN', '36', 'SUDU 800714-5', '24103', 'ACTUALIZAR', '2017-09-13', '07:45:07', '190.42.217.195', '190.42.217.195'),
(1329, 'APPBOSA SAMAN SAMAN', '36', 'SUDU 800702-1', '24102', 'ACTUALIZAR', '2017-09-13', '07:45:16', '190.42.217.195', '190.42.217.195'),
(1330, 'APPBOSA SAMAN SAMAN', '36', 'TTNU 896364-3', '24040', 'ACTUALIZAR', '2017-09-13', '07:45:43', '190.42.217.195', '190.42.217.195'),
(1331, 'APPBOSA SAMAN SAMAN', '36', 'HLXU 673488-9', '1', 'ACTUALIZAR', '2017-09-13', '07:46:06', '190.42.217.195', '190.42.217.195'),
(1332, 'APPBOSA SAMAN SAMAN', '36', 'TCLU 102664-6', '1', 'ACTUALIZAR', '2017-09-13', '07:46:21', '190.42.217.195', '190.42.217.195'),
(1333, 'APPBOSA SAMAN SAMAN', '36', 'TCLU 120483-5', '1', 'ACTUALIZAR', '2017-09-13', '07:46:44', '190.42.217.195', '190.42.217.195'),
(1334, 'APPBOSA SAMAN SAMAN', '36', 'CPSU 516498-8', '1', 'ACTUALIZAR', '2017-09-13', '07:47:01', '190.42.217.195', '190.42.217.195'),
(1335, 'APPBOSA SAMAN SAMAN', '36', 'UACU 472996-0', '1', 'ACTUALIZAR', '2017-09-13', '07:47:37', '190.42.217.195', '190.42.217.195'),
(1336, 'APPBOSA SAMAN SAMAN', '36', 'CXRU 152742-9', '24039', 'ACTUALIZAR', '2017-09-13', '07:47:57', '190.42.217.195', '190.42.217.195'),
(1337, 'APPBOSA SAMAN SAMAN', '36', 'SUDU 816768-9', '24041', 'ACTUALIZAR', '2017-09-13', '07:48:38', '190.42.217.195', '190.42.217.195'),
(1338, 'APPBOSA SAMAN SAMAN', '36', 'SUDU 824044-0', '24042', 'ACTUALIZAR', '2017-09-13', '07:48:57', '190.42.217.195', '190.42.217.195'),
(1339, 'APPBOSA SAMAN SAMAN', '36', 'TLLU 106984-1', '1', 'ACTUALIZAR', '2017-09-13', '07:49:20', '190.42.217.195', '190.42.217.195'),
(1340, 'APPBOSA SAMAN SAMAN', '36', 'TLLU 106960-4', '1', 'ACTUALIZAR', '2017-09-13', '07:49:53', '190.42.217.195', '190.42.217.195'),
(1341, 'APPBOSA SAMAN SAMAN', '36', 'SUDU 816768-9', '24135', 'ACTUALIZAR', '2017-09-13', '07:50:11', '190.42.217.195', '190.42.217.195'),
(1342, 'APPBOSA SAMAN SAMAN', '36', 'SUDU 822627-2', '1', 'ACTUALIZAR', '2017-09-13', '07:50:27', '190.42.217.195', '190.42.217.195'),
(1343, 'APPBOSA SAMAN SAMAN', '36', 'SUDU 502329-2', '24135', 'ACTUALIZAR', '2017-09-13', '07:51:02', '190.42.217.195', '190.42.217.195'),
(1344, 'APPBOSA SAMAN SAMAN', '36', 'SUDU 814923-7', '1', 'ACTUALIZAR', '2017-09-13', '07:51:36', '190.42.217.195', '190.42.217.195'),
(1345, 'Juan Calderón More', '37', 'PONU 480756-8', '24179', 'INSERTAR', '2017-09-16', '09:14:39', '190.43.117.238', '190.43.117.238'),
(1346, 'Juan Calderón More', '37', 'MWCU 524826-8', '24180', 'INSERTAR', '2017-09-16', '09:24:46', '190.43.117.238', '190.43.117.238'),
(1347, 'Juan Calderón More', '37', 'PONU 480756-8', '24179', 'ACTUALIZAR', '2017-09-16', '09:25:14', '190.43.117.238', '190.43.117.238'),
(1348, 'Juan Calderón More', '37', 'MWMU 635700-6', '24181', 'INSERTAR', '2017-09-16', '09:31:03', '190.43.117.238', '190.43.117.238'),
(1349, 'APPBOSA SAMAN SAMAN', '37', 'PONU 480756-8', '24179', 'ACTUALIZAR', '2017-10-02', '10:28:04', '::1', 'Luis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_estado`
--

CREATE TABLE `audit_estado` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_estado`
--

INSERT INTO `audit_estado` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(1, 'APPBOSA SAMAN SAMAN', 'CANCELADO', 'INSERTAR', '2017-07-27', '08:35:54', '190.42.206.197', '190.42.206.197'),
(2, 'APPBOSA SAMAN SAMAN', 'Cancelado', 'ACTUALIZAR', '2017-08-16', '15:30:28', '181.176.59.104', '181.176.59.104');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_factura`
--

CREATE TABLE `audit_factura` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_factura`
--

INSERT INTO `audit_factura` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(3, 'APPBOSA SAMAN SAMAN', '1_29_2017_(7)1_24_2017_(1)1_24_2017_(2)AGROFAIR2.pdf', 'INSERTAR', '2017-07-24', '19:34:51', '190.117.166.176', '190.117.166.176'),
(4, 'APPBOSA SAMAN SAMAN', '1_29_2017_(7)1_24_2017_(1)1_24_2017_(2)AGROFAIR2.pdf', 'ELIMINAR', '2017-07-24', '19:36:59', '190.117.166.176', '190.117.166.176'),
(5, 'APPBOSA SAMAN SAMAN', '1_28_2017_(7)AGROFAIR01.pdf', 'INSERTAR', '2017-08-18', '16:16:06', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(6, 'APPBOSA SAMAN SAMAN', '1_28_2017_(8)AGROFAIR02.pdf', 'INSERTAR', '2017-08-18', '16:16:06', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(7, 'APPBOSA SAMAN SAMAN', '2_28_2017_(5)BIODYNAMISKA.pdf', 'INSERTAR', '2017-08-18', '16:18:03', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(8, 'APPBOSA SAMAN SAMAN', '3_28_2017_(4)DOLE D.pdf', 'INSERTAR', '2017-08-18', '16:19:09', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(9, 'APPBOSA SAMAN SAMAN', '5_28_2017_(5)EQUIFRUIT.pdf', 'INSERTAR', '2017-08-18', '16:22:51', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(10, 'APPBOSA SAMAN SAMAN', '4_28_2017_(4)TRANSASTRA.pdf', 'INSERTAR', '2017-08-18', '16:25:16', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(11, 'APPBOSA SAMAN SAMAN', '1_29_2017_(9)AGROFAIR01.pdf', 'INSERTAR', '2017-08-21', '08:38:24', '190.238.17.74', '190.238.17.74'),
(12, 'APPBOSA SAMAN SAMAN', '1_29_2017_(10)AGROFAIR02.pdf', 'INSERTAR', '2017-08-21', '08:38:24', '190.238.17.74', '190.238.17.74'),
(13, 'APPBOSA SAMAN SAMAN', '2_29_2017_(6)BIODYNAMISKA.pdf', 'INSERTAR', '2017-08-21', '08:41:52', '190.238.17.74', '190.238.17.74'),
(14, 'APPBOSA SAMAN SAMAN', '3_29_2017_(5)DOLE.pdf', 'INSERTAR', '2017-08-21', '08:42:59', '190.238.17.74', '190.238.17.74'),
(15, 'APPBOSA SAMAN SAMAN', '5_29_2017_(6)EQUIFRUIT.pdf', 'INSERTAR', '2017-08-21', '08:44:11', '190.238.17.74', '190.238.17.74'),
(16, 'APPBOSA SAMAN SAMAN', '4_29_2017_(5)TRANSASTRA.pdf', 'INSERTAR', '2017-08-21', '08:51:16', '190.238.17.74', '190.238.17.74'),
(17, 'APPBOSA SAMAN SAMAN', '1_30_2017_(11)AGROFAIR 1.pdf', 'INSERTAR', '2017-08-21', '09:26:22', '190.238.17.74', '190.238.17.74'),
(18, 'APPBOSA SAMAN SAMAN', '1_30_2017_(12)AGROFAIR 2.pdf', 'INSERTAR', '2017-08-21', '09:26:22', '190.238.17.74', '190.238.17.74'),
(19, 'APPBOSA SAMAN SAMAN', '1_1_2017_(13)MVC.pdf', 'INSERTAR', '2017-08-22', '23:03:50', '181.176.78.33', '181.176.78.33'),
(20, 'APPBOSA SAMAN SAMAN', '1_1_2017_(13)MVC.pdf', 'ELIMINAR', '2017-08-22', '23:10:40', '181.176.78.33', '181.176.78.33'),
(21, 'APPBOSA SAMAN SAMAN', '1_33_2017_(13)AGROFAIR1.pdf', 'INSERTAR', '2017-08-24', '12:32:48', '190.42.208.254', '190.42.208.254'),
(22, 'APPBOSA SAMAN SAMAN', '1_33_2017_(14)AGROFAIR2.pdf', 'INSERTAR', '2017-08-24', '12:32:48', '190.42.208.254', '190.42.208.254'),
(23, 'APPBOSA SAMAN SAMAN', '2_33_2017_(7)BIODYNAMISKA.pdf', 'INSERTAR', '2017-08-24', '12:33:56', '190.42.208.254', '190.42.208.254'),
(24, 'APPBOSA SAMAN SAMAN', '3_33_2017_(6)DOLE D.pdf', 'INSERTAR', '2017-08-24', '12:34:46', '190.42.208.254', '190.42.208.254'),
(25, 'APPBOSA SAMAN SAMAN', '5_33_2017_(7)EQUIFRUIT.pdf', 'INSERTAR', '2017-08-24', '12:35:41', '190.42.208.254', '190.42.208.254'),
(26, 'APPBOSA SAMAN SAMAN', '4_33_2017_(6)TRANSASTRA.pdf', 'INSERTAR', '2017-08-24', '12:36:29', '190.42.208.254', '190.42.208.254'),
(27, 'APPBOSA SAMAN SAMAN', '3_30_2017_(7)DOLE GERMANY.pdf', 'INSERTAR', '2017-08-26', '10:39:54', '190.42.208.254', '190.42.208.254'),
(28, 'APPBOSA SAMAN SAMAN', '5_30_2017_(8)EQUIFRUIT.pdf', 'INSERTAR', '2017-08-26', '10:40:57', '190.42.208.254', '190.42.208.254'),
(29, 'APPBOSA SAMAN SAMAN', '2_30_2017_(8)STIFTELSEN BIODYNAMISKA.pdf', 'INSERTAR', '2017-08-26', '10:43:06', '190.42.208.254', '190.42.208.254'),
(30, 'APPBOSA SAMAN SAMAN', '4_30_2017_(7)TRANSASTRA.pdf', 'INSERTAR', '2017-08-26', '10:44:08', '190.42.208.254', '190.42.208.254'),
(31, 'APPBOSA SAMAN SAMAN', '1_34_2017_(15)AGROFAIR.pdf', 'INSERTAR', '2017-08-29', '14:57:29', '190.40.249.208', '190.40.249.208'),
(32, 'APPBOSA SAMAN SAMAN', '2_34_2017_(9)BIODYNAMISKA.pdf', 'INSERTAR', '2017-08-29', '14:58:42', '190.40.249.208', '190.40.249.208'),
(33, 'APPBOSA SAMAN SAMAN', '3_34_2017_(8)DOLE.pdf', 'INSERTAR', '2017-08-29', '14:59:16', '190.40.249.208', '190.40.249.208'),
(34, 'APPBOSA SAMAN SAMAN', '5_34_2017_(9)EQUIFRUIT.pdf', 'INSERTAR', '2017-08-29', '15:00:22', '190.40.249.208', '190.40.249.208'),
(35, 'APPBOSA SAMAN SAMAN', '4_34_2017_(8)TRANSASTRA.pdf', 'INSERTAR', '2017-08-29', '15:01:16', '190.40.249.208', '190.40.249.208'),
(36, 'APPBOSA SAMAN SAMAN', '1_35_2017_(16)AGROFAIR1.pdf', 'INSERTAR', '2017-09-12', '10:53:44', '190.237.236.83', '190.237.236.83'),
(37, 'APPBOSA SAMAN SAMAN', '1_35_2017_(17)AGROFAIR2.pdf', 'INSERTAR', '2017-09-12', '10:53:44', '190.237.236.83', '190.237.236.83'),
(38, 'APPBOSA SAMAN SAMAN', '2_35_2017_(10)BIODYNAMISKA.pdf', 'INSERTAR', '2017-09-12', '10:54:51', '190.237.236.83', '190.237.236.83'),
(39, 'APPBOSA SAMAN SAMAN', '3_35_2017_(9)DOLE.pdf', 'INSERTAR', '2017-09-12', '10:55:38', '190.237.236.83', '190.237.236.83'),
(40, 'APPBOSA SAMAN SAMAN', '5_35_2017_(10)EQUIFRUIT.pdf', 'INSERTAR', '2017-09-12', '10:56:26', '190.237.236.83', '190.237.236.83'),
(41, 'APPBOSA SAMAN SAMAN', '4_35_2017_(9)TRANSASTRA.pdf', 'INSERTAR', '2017-09-12', '10:57:51', '190.237.236.83', '190.237.236.83'),
(42, 'APPBOSA SAMAN SAMAN', '2_36_2017_(11)BIODYNAMISKA.pdf', 'INSERTAR', '2017-09-12', '16:16:31', '190.237.236.83', '190.237.236.83'),
(43, 'APPBOSA SAMAN SAMAN', '3_36_2017_(10)DOLE.pdf', 'INSERTAR', '2017-09-12', '16:17:43', '190.237.236.83', '190.237.236.83'),
(44, 'APPBOSA SAMAN SAMAN', '5_36_2017_(11)EQUIFRUIT.pdf', 'INSERTAR', '2017-09-12', '16:18:45', '190.237.236.83', '190.237.236.83'),
(45, 'APPBOSA SAMAN SAMAN', '4_36_2017_(10)TRANSASTRA.pdf', 'INSERTAR', '2017-09-12', '16:19:29', '190.237.236.83', '190.237.236.83'),
(46, 'APPBOSA SAMAN SAMAN', '1_36_2017_(18)AGROFAIR1.pdf', 'INSERTAR', '2017-09-12', '16:26:57', '190.237.236.83', '190.237.236.83'),
(47, 'APPBOSA SAMAN SAMAN', '1_36_2017_(19)AGROFAIR2.pdf', 'INSERTAR', '2017-09-12', '16:26:57', '190.237.236.83', '190.237.236.83');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_linea_naviera`
--

CREATE TABLE `audit_linea_naviera` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_nave`
--

CREATE TABLE `audit_nave` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_operador`
--

CREATE TABLE `audit_operador` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_operador`
--

INSERT INTO `audit_operador` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(14, 'Juan Calderón More', 'TPSAC', 'ELIMINAR', '2017-05-29', '10:34:35', '179.7.132.187', '179.7.132.187'),
(15, 'Juan Calderón More', 'LA HANSEATICA', 'INSERTAR', '2017-07-14', '08:10:36', '190.237.128.174', '190.237.128.174'),
(16, 'APPBOSA SAMAN SAMAN', 'SAN MIGUEL', 'ACTUALIZAR', '2017-07-27', '08:09:33', '190.42.206.197', '190.42.206.197');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_packing`
--

CREATE TABLE `audit_packing` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_packing`
--

INSERT INTO `audit_packing` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(3, 'APPBOSA SAMAN SAMAN', '1_26_2017_(1)529-(1)-detalle_contenedor.xls', 'INSERTAR', '2017-07-24', '20:12:54', '190.117.166.176', '190.117.166.176'),
(4, 'APPBOSA SAMAN SAMAN', '1_26_2017_(1)529-(1)-detalle_contenedor.xls', 'ELIMINAR', '2017-07-24', '20:15:11', '190.42.97.91', '190.42.97.91'),
(5, 'APPBOSA SAMAN SAMAN', '1_28_2017_(1)529-(1)-detalle_contenedor.xls', 'INSERTAR', '2017-07-25', '12:14:24', '181.176.87.187', '181.176.87.187'),
(6, 'APPBOSA SAMAN SAMAN', '1_29_2017_(2)1_28_2017_(1)529-(1)-detalle_contenedor.xls', 'INSERTAR', '2017-07-25', '12:39:28', '181.176.87.187', '181.176.87.187'),
(7, 'APPBOSA SAMAN SAMAN', '1_29_2017_(2)1_28_2017_(1)529-(1)-detalle_contened', 'ELIMINAR', '2017-07-25', '12:55:35', '181.176.87.187', '181.176.87.187'),
(8, 'APPBOSA SAMAN SAMAN', '1_29_2017_(2)reporte_contenedores (1).xls', 'INSERTAR', '2017-07-25', '12:55:48', '181.176.87.187', '181.176.87.187'),
(9, 'APPBOSA SAMAN SAMAN', '1_29_2017_(2)reporte_contenedores (1).xls', 'ELIMINAR', '2017-07-25', '12:56:39', '181.176.87.187', '181.176.87.187'),
(10, 'APPBOSA SAMAN SAMAN', '1_29_2017_(2)reporte_contenedores (18).xls', 'INSERTAR', '2017-07-25', '12:56:53', '181.176.87.187', '181.176.87.187'),
(11, 'APPBOSA SAMAN SAMAN', '1_29_2017_(3)reporte_damnificados.xls', 'INSERTAR', '2017-07-25', '13:08:04', '181.176.87.187', '181.176.87.187'),
(12, 'APPBOSA SAMAN SAMAN', '1_29_2017_(2)reporte_contenedores (18).xls', 'ELIMINAR', '2017-07-25', '13:08:29', '181.176.87.187', '181.176.87.187'),
(13, 'APPBOSA SAMAN SAMAN', '1_29_2017_(3)reporte_damnificados.xls', 'ELIMINAR', '2017-07-25', '13:08:33', '181.176.87.187', '181.176.87.187'),
(14, 'APPBOSA SAMAN SAMAN', '1_29_2017_(2)COMBUSTIBLE.xlsx', 'INSERTAR', '2017-07-25', '14:25:13', '190.237.128.146', '190.237.128.146'),
(15, 'APPBOSA SAMAN SAMAN', '1_29_2017_(2)COMBUSTIBLE.xlsx', 'ELIMINAR', '2017-07-25', '14:26:10', '190.237.128.146', '190.237.128.146'),
(16, 'Nilton Anterio Ancajima Castro', '1_28_2017_(1)529-(1)-detalle_contenedor.xls', 'ELIMINAR', '2017-07-25', '14:37:41', '190.237.128.146', '190.237.128.146'),
(17, 'APPBOSA SAMAN SAMAN', '1_29_2017_(1)DETALLE DE RECLAMOS AGROFAIR.xlsx', 'INSERTAR', '2017-07-27', '09:38:08', '190.42.206.197', '190.42.206.197'),
(18, 'Juan Calderón More', '1_30_2017_(2)agrofair 01.xlsx', 'INSERTAR', '2017-08-01', '16:14:17', '190.42.203.13', '190.42.203.13'),
(19, 'Juan Calderón More', '1_30_2017_(3)agrofair  30.xlsx', 'INSERTAR', '2017-08-02', '16:13:28', '190.233.174.236', '190.233.174.236'),
(20, 'Juan Calderón More', '5_30_2017_(1)equifruit    30.xlsx', 'INSERTAR', '2017-08-02', '16:14:44', '190.233.174.236', '190.233.174.236'),
(21, 'Juan Calderón More', '1_30_2017_(3)agrofair  30.xlsx', 'ELIMINAR', '2017-08-02', '16:15:16', '190.233.174.236', '190.233.174.236'),
(22, 'Juan Calderón More', '3_30_2017_(1)dole    30.xlsx', 'INSERTAR', '2017-08-02', '16:17:15', '190.233.174.236', '190.233.174.236'),
(23, 'Juan Calderón More', '2_30_2017_(1)biodynamiska   30.xlsx', 'INSERTAR', '2017-08-02', '16:18:00', '190.233.174.236', '190.233.174.236'),
(24, 'Juan Calderón More', '4_30_2017_(1)chiquita  30.xlsx', 'INSERTAR', '2017-08-02', '16:18:24', '190.233.174.236', '190.233.174.236'),
(25, 'APPBOSA SAMAN SAMAN', '1_29_2017_(1)DETALLE DE RECLAMOS AGROFAIR.xlsx', 'ELIMINAR', '2017-08-04', '09:06:04', '181.66.45.23', '181.66.45.23'),
(26, 'Juan Calderón More', '1_31_2017_(2)agrofair  31.xlsx', 'INSERTAR', '2017-08-09', '15:44:21', '190.238.204.106', '190.238.204.106'),
(27, 'Juan Calderón More', '2_31_2017_(2)biodynamiska   31.xlsx', 'INSERTAR', '2017-08-09', '15:46:05', '190.238.204.106', '190.238.204.106'),
(28, 'Juan Calderón More', '4_31_2017_(2)chiquita  31.xlsx', 'INSERTAR', '2017-08-09', '15:46:55', '190.238.204.106', '190.238.204.106'),
(29, 'Juan Calderón More', '3_31_2017_(2)dole   31.xlsx', 'INSERTAR', '2017-08-09', '15:48:00', '190.238.204.106', '190.238.204.106'),
(30, 'Juan Calderón More', '5_31_2017_(2)equifruit    31.xlsx', 'INSERTAR', '2017-08-09', '15:48:34', '190.238.204.106', '190.238.204.106'),
(31, 'APPBOSA SAMAN SAMAN', '1_24_2017_(3)cuadro12 (1).xlsx', 'INSERTAR', '2017-08-10', '21:30:39', '181.176.59.133', '181.176.59.133'),
(32, 'APPBOSA SAMAN SAMAN', '1_24_2017_(3)cuadro12 (1).xlsx', 'ELIMINAR', '2017-08-10', '23:36:12', '181.176.59.133', '181.176.59.133'),
(33, 'APPBOSA SAMAN SAMAN', '1_24_2017_(3)diseño.xls', 'INSERTAR', '2017-08-11', '09:42:27', '181.176.59.89', '181.176.59.89'),
(34, 'APPBOSA SAMAN SAMAN', '1_24_2017_(3)diseño.xls', 'ELIMINAR', '2017-08-11', '10:12:25', '181.176.59.89', '181.176.59.89'),
(35, 'APPBOSA SAMAN SAMAN', '1_24_2017_(3)Cosas.xlsx', 'INSERTAR', '2017-08-16', '10:35:48', '181.176.56.39', '181.176.56.39'),
(36, 'APPBOSA SAMAN SAMAN', '1_24_2017_(3)Cosas.xlsx', 'ELIMINAR', '2017-08-16', '10:36:50', '181.176.56.39', '181.176.56.39'),
(37, 'APPBOSA SAMAN SAMAN', '1_31_2017_(3)cap09003.xlsx', 'INSERTAR', '2017-08-18', '13:25:19', '181.176.58.57', '181.176.58.57'),
(38, 'APPBOSA SAMAN SAMAN', '1_31_2017_(3)cap09003.xlsx', 'ELIMINAR', '2017-08-18', '13:33:40', '181.176.58.57', '181.176.58.57'),
(39, 'APPBOSA SAMAN SAMAN', '1_31_2017_(3)cap09003.xlsx', 'INSERTAR', '2017-08-18', '15:26:37', '181.176.58.57', '181.176.58.57'),
(40, 'APPBOSA SAMAN SAMAN', '1_31_2017_(3)cap09003.xlsx', 'ELIMINAR', '2017-08-18', '15:27:42', '181.176.58.57', '181.176.58.57'),
(41, 'APPBOSA SAMAN SAMAN', '1_31_2017_(3)cap09003.xlsx', 'INSERTAR', '2017-08-18', '15:28:52', '181.176.58.57', '181.176.58.57'),
(42, 'APPBOSA SAMAN SAMAN', '1_31_2017_(3)cap09003.xlsx', 'ELIMINAR', '2017-08-18', '15:30:53', '181.176.58.57', '181.176.58.57'),
(43, 'APPBOSA SAMAN SAMAN', '1_31_2017_(3)cap09003.xlsx', 'INSERTAR', '2017-08-18', '15:31:19', '181.176.58.57', '181.176.58.57'),
(44, 'APPBOSA SAMAN SAMAN', '1_31_2017_(3)cap09003.xlsx', 'ELIMINAR', '2017-08-18', '15:32:41', '181.176.58.57', '181.176.58.57'),
(45, 'Juan Calderón More', '1_32_2017_(3)agrofair  30.xlsx', 'INSERTAR', '2017-08-18', '15:47:01', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(46, 'Juan Calderón More', '2_32_2017_(3)biodynamiska   30.xlsx', 'INSERTAR', '2017-08-18', '15:47:23', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(47, 'Juan Calderón More', '4_32_2017_(3)chiquita  30.xlsx', 'INSERTAR', '2017-08-18', '15:47:43', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(48, 'Juan Calderón More', '3_32_2017_(3)dole    30.xlsx', 'INSERTAR', '2017-08-18', '15:48:18', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(49, 'Juan Calderón More', '5_32_2017_(3)equifruit    30.xlsx', 'INSERTAR', '2017-08-18', '15:48:36', '201.230.16.152', 'client-201.230.16.152.speedy.net.pe'),
(50, 'Juan Calderón More', '1_32_2017_(3)agrofair  30.xlsx', 'ELIMINAR', '2017-08-21', '08:07:27', '190.238.17.74', '190.238.17.74'),
(51, 'Juan Calderón More', '2_32_2017_(3)biodynamiska   30.xlsx', 'ELIMINAR', '2017-08-21', '08:07:32', '190.238.17.74', '190.238.17.74'),
(52, 'Juan Calderón More', '4_32_2017_(3)chiquita  30.xlsx', 'ELIMINAR', '2017-08-21', '08:07:36', '190.238.17.74', '190.238.17.74'),
(53, 'Juan Calderón More', '3_32_2017_(3)dole    30.xlsx', 'ELIMINAR', '2017-08-21', '08:07:40', '190.238.17.74', '190.238.17.74'),
(54, 'Juan Calderón More', '5_32_2017_(3)equifruit    30.xlsx', 'ELIMINAR', '2017-08-21', '08:07:44', '190.238.17.74', '190.238.17.74'),
(55, 'Juan Calderón More', '1_32_2017_(3)Packing Agrofair Sem 32.xls', 'INSERTAR', '2017-08-21', '08:10:07', '190.238.17.74', '190.238.17.74'),
(56, 'Juan Calderón More', '2_32_2017_(3)bidynamiska   32.xlsx', 'INSERTAR', '2017-08-21', '08:10:33', '190.238.17.74', '190.238.17.74'),
(57, 'Juan Calderón More', '4_32_2017_(3)chiquita  32.xlsx', 'INSERTAR', '2017-08-21', '08:11:17', '190.238.17.74', '190.238.17.74'),
(58, 'Juan Calderón More', '3_32_2017_(3)dole   32.xlsx', 'INSERTAR', '2017-08-21', '08:11:40', '190.238.17.74', '190.238.17.74'),
(59, 'Juan Calderón More', '5_32_2017_(3)equifruit   32.xlsx', 'INSERTAR', '2017-08-21', '08:12:02', '190.238.17.74', '190.238.17.74'),
(60, 'APPBOSA SAMAN SAMAN', '1_33_2017_(4)cap09003.xlsx', 'INSERTAR', '2017-08-21', '19:27:51', '181.176.66.27', '181.176.66.27'),
(61, 'APPBOSA SAMAN SAMAN', '1_33_2017_(4)cap09003.xlsx', 'ELIMINAR', '2017-08-21', '19:35:07', '181.176.66.27', '181.176.66.27'),
(62, 'APPBOSA SAMAN SAMAN', '1_1_2017_(4)REVISION DE TAREAS.xlsx', 'INSERTAR', '2017-08-22', '23:11:12', '181.176.78.33', '181.176.78.33'),
(63, 'APPBOSA SAMAN SAMAN', '1_1_2017_(4)REVISION DE TAREAS.xlsx', 'ELIMINAR', '2017-08-22', '23:14:03', '181.176.78.33', '181.176.78.33'),
(64, 'Juan Calderón More', '1_33_2017_(4)Packing Agrofair Sem 33.xls', 'INSERTAR', '2017-08-24', '10:58:53', '190.42.208.254', '190.42.208.254'),
(65, 'Juan Calderón More', '1_34_2017_(5)Packing Agrofair Sem 34.xls', 'INSERTAR', '2017-08-31', '12:21:12', '190.42.203.87', '190.42.203.87'),
(66, 'Juan Calderón More', '2_34_2017_(4)biodynamiska   34.xlsx', 'INSERTAR', '2017-08-31', '12:27:02', '190.42.203.87', '190.42.203.87'),
(67, 'Juan Calderón More', '4_34_2017_(4)chiquita    34.xlsx', 'INSERTAR', '2017-08-31', '12:27:29', '190.42.203.87', '190.42.203.87'),
(68, 'Juan Calderón More', '3_34_2017_(4)dole  34.xlsx', 'INSERTAR', '2017-08-31', '12:29:11', '190.42.203.87', '190.42.203.87'),
(69, 'Juan Calderón More', '5_34_2017_(4)equifruit  34.xlsx', 'INSERTAR', '2017-08-31', '12:30:56', '190.42.203.87', '190.42.203.87'),
(70, 'Juan Calderón More', '1_35_2017_(6)Packing Agrofair Sem 35.xls', 'INSERTAR', '2017-09-06', '16:48:41', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(71, 'Juan Calderón More', '2_35_2017_(5)biodynamiska  35.xlsx', 'INSERTAR', '2017-09-06', '16:54:39', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(72, 'Juan Calderón More', '4_35_2017_(5)chiquita  35.xlsx', 'INSERTAR', '2017-09-06', '16:54:55', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(73, 'Juan Calderón More', '3_35_2017_(5)dole  35.xlsx', 'INSERTAR', '2017-09-06', '16:55:15', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(74, 'Juan Calderón More', '5_35_2017_(5)equifruit   35.xlsx', 'INSERTAR', '2017-09-06', '16:55:33', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(75, 'Juan Calderón More', '2_36_2017_(6)biodynamiska 36 bn.xlsx', 'INSERTAR', '2017-09-13', '12:46:27', '190.42.217.195', '190.42.217.195'),
(76, 'Juan Calderón More', '1_36_2017_(7)Packing Agrofair Sem 36.xls', 'INSERTAR', '2017-09-13', '12:47:29', '190.42.217.195', '190.42.217.195'),
(77, 'Juan Calderón More', '4_36_2017_(6)chiquita   36.xlsx', 'INSERTAR', '2017-09-13', '12:47:55', '190.42.217.195', '190.42.217.195'),
(78, 'Juan Calderón More', '3_36_2017_(6)dole    36.xlsx', 'INSERTAR', '2017-09-13', '12:48:17', '190.42.217.195', '190.42.217.195'),
(79, 'Juan Calderón More', '5_36_2017_(6)equifruit     36.xlsx', 'INSERTAR', '2017-09-13', '12:48:41', '190.42.217.195', '190.42.217.195'),
(80, 'APPBOSA SAMAN SAMAN', '2_37_2017_(7)diseño.xls', 'INSERTAR', '2017-10-02', '11:22:34', '::1', 'Luis'),
(81, 'APPBOSA SAMAN SAMAN', '3_27_2017_(7)Libro1.xlsx', 'INSERTAR', '2017-10-02', '11:36:01', '::1', 'Luis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_pais`
--

CREATE TABLE `audit_pais` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_parcela`
--

CREATE TABLE `audit_parcela` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_productor`
--

CREATE TABLE `audit_productor` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(255) NOT NULL,
  `usuario_afectado` varchar(255) NOT NULL,
  `operacion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_puerto_destino`
--

CREATE TABLE `audit_puerto_destino` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_puerto_destino`
--

INSERT INTO `audit_puerto_destino` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(21, 'Juan Calderón More', 'AMBERES', 'INSERTAR', '2017-05-31', '12:37:56', '181.67.4.220', '181.67.4.220');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_semana`
--

CREATE TABLE `audit_semana` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_semana`
--

INSERT INTO `audit_semana` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(14, 'Luis Guillermo Ramirez Coronado', '2', 'INSERTAR', '2017-05-22', '19:35:13', '190.236.31.16', '190.236.31.16'),
(15, 'Luis Guillermo Ramirez Coronado', '4', 'INSERTAR', '2017-05-22', '19:35:54', '190.236.31.16', '190.236.31.16'),
(16, 'Juan Calderón More', '2', 'ELIMINAR', '2017-05-23', '16:23:29', '179.7.139.187', '179.7.139.187'),
(17, 'Juan Calderón More', '4', 'ELIMINAR', '2017-05-23', '16:23:33', '179.7.139.187', '179.7.139.187'),
(18, 'Juan Calderón More', '20', 'INSERTAR', '2017-05-23', '16:24:33', '179.7.139.187', '179.7.139.187'),
(19, 'Juan Calderón More', '22', 'INSERTAR', '2017-05-29', '09:58:55', '179.7.132.187', '179.7.132.187'),
(20, 'Juan Calderón More', '21', 'INSERTAR', '2017-05-29', '10:49:28', '179.7.132.187', '179.7.132.187'),
(21, 'APPBOSA SAMAN SAMAN', '22', 'ELIMINAR', '2017-05-30', '07:16:04', '190.117.137.3', '190.117.137.3'),
(22, 'APPBOSA SAMAN SAMAN', '20', 'ELIMINAR', '2017-05-30', '07:16:09', '190.117.137.3', '190.117.137.3'),
(23, 'Juan Calderón More', '22', 'INSERTAR', '2017-05-30', '08:38:59', '179.7.132.187', '179.7.132.187'),
(24, 'Juan Calderón More', '20', 'INSERTAR', '2017-05-30', '10:46:57', '179.7.132.187', '179.7.132.187'),
(25, 'Juan Calderón More', '19', 'INSERTAR', '2017-05-30', '12:18:08', '179.7.132.187', '179.7.132.187'),
(26, 'Juan Calderón More', '18', 'INSERTAR', '2017-05-31', '08:23:16', '181.67.4.220', '181.67.4.220'),
(27, 'Juan Calderón More', '17', 'INSERTAR', '2017-05-31', '10:03:15', '181.67.4.220', '181.67.4.220'),
(28, 'Juan Calderón More', '16', 'INSERTAR', '2017-05-31', '11:33:14', '181.67.4.220', '181.67.4.220'),
(29, 'Juan Calderón More', '15', 'INSERTAR', '2017-06-01', '07:25:02', '190.237.186.168', '190.237.186.168'),
(30, 'Juan Calderón More', '14', 'INSERTAR', '2017-06-01', '08:47:37', '190.237.186.168', '190.237.186.168'),
(31, 'Juan Calderón More', '13', 'INSERTAR', '2017-06-01', '09:34:21', '190.237.186.168', '190.237.186.168'),
(32, 'Juan Calderón More', '12', 'INSERTAR', '2017-06-01', '10:55:06', '190.237.186.168', '190.237.186.168'),
(33, 'Juan Calderón More', '11', 'INSERTAR', '2017-06-02', '07:20:58', '190.237.186.168', '190.237.186.168'),
(34, 'Juan Calderón More', '10', 'INSERTAR', '2017-06-02', '09:53:18', '190.237.186.168', '190.237.186.168'),
(35, 'Juan Calderón More', '9', 'INSERTAR', '2017-06-02', '11:24:26', '190.237.186.168', '190.237.186.168'),
(36, 'Juan Calderón More', '9', 'ELIMINAR', '2017-06-02', '11:24:39', '190.237.186.168', '190.237.186.168'),
(37, 'Juan Calderón More', '9', 'INSERTAR', '2017-06-02', '11:24:51', '190.237.186.168', '190.237.186.168'),
(38, 'Juan Calderón More', '8', 'INSERTAR', '2017-06-05', '08:30:23', '190.237.186.168', '190.237.186.168'),
(39, 'Juan Calderón More', '7', 'INSERTAR', '2017-06-05', '10:41:01', '190.237.186.168', '190.237.186.168'),
(40, 'Juan Calderón More', '5', 'INSERTAR', '2017-06-05', '12:21:14', '190.237.186.168', '190.237.186.168'),
(41, 'Juan Calderón More', '4', 'INSERTAR', '2017-06-05', '12:21:20', '190.237.186.168', '190.237.186.168'),
(42, 'Juan Calderón More', '1', 'INSERTAR', '2017-06-05', '12:21:35', '190.237.186.168', '190.237.186.168'),
(43, 'Juan Calderón More', '2', 'INSERTAR', '2017-06-05', '12:22:21', '190.237.186.168', '190.237.186.168'),
(44, 'Juan Calderón More', '3', 'INSERTAR', '2017-06-05', '12:22:39', '190.237.186.168', '190.237.186.168'),
(45, 'Juan Calderón More', '6', 'INSERTAR', '2017-06-06', '07:16:08', '190.237.186.168', '190.237.186.168'),
(46, 'Juan Calderón More', '23', 'INSERTAR', '2017-06-06', '12:29:49', '190.237.186.168', '190.237.186.168'),
(47, 'Juan Calderón More', '24', 'INSERTAR', '2017-06-09', '11:31:07', '190.43.242.226', '190.43.242.226'),
(48, 'Juan Calderón More', '25', 'INSERTAR', '2017-06-15', '10:25:43', '201.240.113.132', 'client-201.240.113.132.speedy.net.pe'),
(49, 'Juan Calderón More', '26', 'INSERTAR', '2017-06-26', '07:32:51', '190.232.101.51', '190.232.101.51'),
(50, 'Juan Calderón More', '27', 'INSERTAR', '2017-06-30', '09:05:59', '190.232.101.51', '190.232.101.51'),
(51, 'Juan Calderón More', '28', 'INSERTAR', '2017-07-10', '07:25:03', '190.43.32.252', '190.43.32.252'),
(52, 'Juan Calderón More', '29', 'INSERTAR', '2017-07-14', '07:18:34', '190.237.128.174', '190.237.128.174'),
(53, 'Juan Calderón More', '30', 'INSERTAR', '2017-07-21', '07:40:18', '190.239.128.194', '190.239.128.194'),
(54, 'Juan Calderón More', '31', 'INSERTAR', '2017-07-27', '12:06:16', '190.42.206.197', '190.42.206.197'),
(55, 'Juan Calderón More', '32', 'INSERTAR', '2017-08-10', '08:05:36', '200.121.153.129', 'client-200.121.153.129.speedy.net.pe'),
(56, 'Juan Calderón More', '33', 'INSERTAR', '2017-08-21', '09:13:37', '190.238.17.74', '190.238.17.74'),
(57, 'Juan Calderón More', '34', 'INSERTAR', '2017-08-26', '09:02:37', '190.42.208.254', '190.42.208.254'),
(58, 'Juan Calderón More', '35', 'INSERTAR', '2017-08-26', '09:26:31', '190.42.208.254', '190.42.208.254'),
(59, 'APPBOSA SAMAN SAMAN', '36', 'INSERTAR', '2017-09-01', '11:43:46', '190.117.184.237', '190.117.184.237'),
(60, 'Juan Calderón More', '37', 'INSERTAR', '2017-09-16', '09:08:27', '190.43.117.238', '190.43.117.238');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_usuario`
--

CREATE TABLE `audit_usuario` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(255) NOT NULL,
  `usuario_afectado` varchar(255) NOT NULL,
  `operacion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_usuario`
--

INSERT INTO `audit_usuario` (`id`, `usuario_responsable`, `usuario_afectado`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(10, 'APPBOSA SAMAN SAMAN', 'Juan Calderón More', 'ACTUALIZAR', '2017-05-23', '15:41:08', '179.7.139.187', '179.7.139.187'),
(11, 'APPBOSA SAMAN SAMAN', 'Nilton Anterio Ancajima Castro', 'INSERTAR', '2017-05-23', '15:57:09', '179.7.139.187', '179.7.139.187'),
(12, 'APPBOSA SAMAN SAMAN', 'Luis Alberto Mejias Escobar', 'INSERTAR', '2017-05-23', '16:12:02', '179.7.139.187', '179.7.139.187'),
(13, 'APPBOSA SAMAN SAMAN', 'Juan Calderón More', 'ACTUALIZAR', '2017-05-23', '16:15:56', '179.7.139.187', '179.7.139.187'),
(14, 'APPBOSA SAMAN SAMAN', 'Juan Calderón More', 'ACTUALIZAR', '2017-05-29', '09:21:33', '179.7.132.187', '179.7.132.187'),
(15, 'APPBOSA SAMAN SAMAN', 'AGROFAIR  ', 'ACTUALIZAR', '2017-07-13', '13:48:39', '181.176.75.200', '181.176.75.200'),
(16, 'APPBOSA SAMAN SAMAN', 'BIODYNAMISKA  ', 'ACTUALIZAR', '2017-07-13', '13:48:52', '181.176.75.200', '181.176.75.200'),
(17, 'APPBOSA SAMAN SAMAN', 'DOLE  ', 'ACTUALIZAR', '2017-07-13', '13:49:02', '181.176.75.200', '181.176.75.200'),
(18, 'APPBOSA SAMAN SAMAN', 'TRANSASTRA  ', 'ACTUALIZAR', '2017-07-13', '13:49:11', '181.176.75.200', '181.176.75.200'),
(19, 'APPBOSA SAMAN SAMAN', 'EQUIFRUIT  ', 'ACTUALIZAR', '2017-07-13', '13:49:19', '181.176.75.200', '181.176.75.200'),
(20, 'APPBOSA SAMAN SAMAN', 'San Miguel  ', 'INSERTAR', '2017-07-13', '13:57:17', '181.176.75.200', '181.176.75.200'),
(21, 'APPBOSA SAMAN SAMAN', 'AGROFAIR  ', 'ACTUALIZAR', '2017-07-13', '13:59:58', '181.176.75.200', '181.176.75.200'),
(22, 'APPBOSA SAMAN SAMAN', 'Nilton Anterio Ancajima Castro', 'ACTUALIZAR', '2017-07-19', '11:45:41', '190.238.73.215', '190.238.73.215'),
(23, 'APPBOSA SAMAN SAMAN', 'BIODYNAMISKA  ', 'ACTUALIZAR', '2017-07-25', '10:57:20', '181.176.84.75', '181.176.84.75'),
(24, 'APPBOSA SAMAN SAMAN', 'DOLE  ', 'ACTUALIZAR', '2017-07-25', '10:58:07', '181.176.84.75', '181.176.84.75'),
(25, 'APPBOSA SAMAN SAMAN', 'TRANSASTRA  ', 'ACTUALIZAR', '2017-07-25', '11:09:05', '181.176.84.75', '181.176.84.75'),
(26, 'APPBOSA SAMAN SAMAN', 'EQUIFRUIT  ', 'ACTUALIZAR', '2017-07-25', '11:09:23', '181.176.84.75', '181.176.84.75'),
(27, 'APPBOSA SAMAN SAMAN', 'Raul Rodriguez Chero', 'INSERTAR', '2017-09-02', '08:12:11', '181.64.26.88', '181.64.26.88'),
(28, 'APPBOSA SAMAN SAMAN', 'Marcia Ninoska Herrera Reto', 'INSERTAR', '2017-09-02', '08:14:18', '181.64.26.88', '181.64.26.88'),
(29, 'APPBOSA SAMAN SAMAN', 'LA HANSEATICA  ', 'INSERTAR', '2017-09-02', '08:16:04', '181.64.26.88', '181.64.26.88'),
(30, 'APPBOSA SAMAN SAMAN', 'usuario usuario usuario', 'INSERTAR', '2017-09-02', '08:17:29', '181.64.26.88', '181.64.26.88'),
(31, 'APPBOSA SAMAN SAMAN', 'Raul Rodriguez Chero', 'ACTUALIZAR', '2017-09-06', '15:18:57', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(32, 'APPBOSA SAMAN SAMAN', 'Marcia Ninoska Herrera Reto', 'ACTUALIZAR', '2017-09-06', '15:28:35', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(33, 'APPBOSA SAMAN SAMAN', 'EQUIFRUIT  ', 'ACTUALIZAR', '2017-09-06', '15:40:44', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(34, 'APPBOSA SAMAN SAMAN', 'EQUIFRUIT  ', 'ACTUALIZAR', '2017-09-06', '15:43:50', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(35, 'APPBOSA SAMAN SAMAN', 'San Miguel  ', 'ACTUALIZAR', '2017-09-06', '15:44:16', '200.121.222.254', 'client-200.121.222.254.speedy.net.pe'),
(36, 'APPBOSA SAMAN SAMAN', 'LA HANSEATICA  ', 'ACTUALIZAR', '2017-09-21', '20:14:37', '::1', 'Luis'),
(37, 'APPBOSA SAMAN SAMAN', 'San Miguel  ', 'ACTUALIZAR', '2017-10-02', '10:20:46', '::1', 'Luis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_valija`
--

CREATE TABLE `audit_valija` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` text NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_valija`
--

INSERT INTO `audit_valija` (`id`, `usuario_responsable`, `registro`, `operacion`, `fecha`, `hora`, `ip`, `dispositivo`) VALUES
(11, 'APPBOSA SAMAN SAMAN', '1_26_2017_(1)1_24_2017_(1)1_24_2017_(2)AGROFAIR2.pdf', 'INSERTAR', '2017-07-24', '20:16:04', '190.117.166.176', '190.117.166.176'),
(12, 'APPBOSA SAMAN SAMAN', '1_26_2017_(1)1_24_2017_(1)1_24_2017_(2)AGROFAIR2.pdf', 'ELIMINAR', '2017-07-24', '20:16:47', '190.117.166.176', '190.117.166.176'),
(13, 'APPBOSA SAMAN SAMAN', '1_1_2017_(1)MVC.pdf', 'INSERTAR', '2017-08-22', '23:19:42', '181.176.78.33', '181.176.78.33'),
(14, 'APPBOSA SAMAN SAMAN', '1_1_2017_(1)MVC.pdf-20383', 'AGREGAR CONTENEDOR', '2017-08-22', '23:20:13', '181.176.78.33', '181.176.78.33'),
(15, 'APPBOSA SAMAN SAMAN', '1_1_2017_(1)MVC.pdf-20383', 'ELIMINAR CONTENEDOR', '2017-08-22', '23:25:35', '181.176.78.33', '181.176.78.33'),
(16, 'APPBOSA SAMAN SAMAN', '1_1_2017_(1)MVC.pdf', 'ELIMINAR', '2017-08-22', '23:26:23', '181.176.78.33', '181.176.78.33'),
(17, 'San Miguel  ', '2_27_2017_(1)MVC.pdf', 'INSERTAR', '2017-10-02', '12:29:47', '::1', 'Luis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_vl`
--

CREATE TABLE `audit_vl` (
  `id` int(11) NOT NULL,
  `usuario_responsable` varchar(50) NOT NULL,
  `registro` varchar(100) NOT NULL,
  `operacion` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dispositivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `booking`
--

INSERT INTO `booking` (`id`, `codigo`, `activo`) VALUES
(1, '572123443', 1),
(2, '087LIM227108 ', 1),
(3, '087LIM227108 ', 1),
(4, '98171199', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id` int(11) NOT NULL,
  `marca` varchar(50) NOT NULL,
  `peso` decimal(10,2) NOT NULL DEFAULT '18.14',
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`id`, `marca`, `peso`, `activo`) VALUES
(1, 'ALBERTH HEIJING BANDED', '18.14', 1),
(2, 'ALL SELET ALTOMERCATO (ITALIA)', '18.14', 1),
(3, 'EDEKA BIO BIO FT', '18.14', 1),
(4, 'CB CLUSTERBAGS BABG CARRES', '18.14', 1),
(5, 'EDEKA BIO FT', '18.14', 1),
(6, 'EDEKA BIO ORGANIC', '18.14', 1),
(7, 'EKOOKE FT', '18.14', 1),
(8, 'EQUIFRUIT', '18.14', 1),
(9, 'EKOOKE ORGANIC (VERDE)', '18.14', 1),
(10, 'SAMAN ORGANIC FT', '18.14', 1),
(11, 'SAMAN ORGANIC SPP', '18.14', 1),
(12, 'SPAR', '18.14', 1),
(13, 'YC. BIO GERMANY FLO', '18.14', 1),
(14, 'YC. SPAR  FLO', '18.14', 1),
(15, 'FT COREA CLOUSTER 13KG', '13.00', 1),
(16, 'JAPON 13KG - (Dole)', '13.00', 1),
(17, 'CHIQUITA ORGANIC', '18.14', 1),
(18, 'CHIQUITA ORGANIC 2LB', '18.14', 1),
(20, 'CHIQUITA EUROPA FT', '18.14', 1),
(21, 'WHOLE TRADE', '18.14', 1),
(22, 'YC ALBERTH HEIJING BANDED', '18.14', 1),
(23, 'MIGROS FT', '18.14', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_cliente`
--

CREATE TABLE `caja_cliente` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `caja_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_cliente`
--

INSERT INTO `caja_cliente` (`id`, `cliente_id`, `caja_id`) VALUES
(1, 1, 3),
(2, 1, 7),
(4, 2, 10),
(5, 2, 11),
(7, 3, 21),
(8, 4, 12),
(10, 1, 9),
(11, 1, 4),
(12, 1, 5),
(13, 1, 6),
(14, 1, 15),
(15, 1, 23),
(16, 1, 2),
(17, 3, 13),
(18, 3, 14),
(19, 3, 16),
(20, 3, 22),
(21, 4, 1),
(22, 4, 17),
(23, 4, 18),
(24, 4, 20),
(26, 5, 8),
(27, 4, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_contenedor`
--

CREATE TABLE `caja_contenedor` (
  `id` int(11) NOT NULL,
  `contenedor_id` int(11) NOT NULL,
  `caja_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_contenedor`
--

INSERT INTO `caja_contenedor` (`id`, `contenedor_id`, `caja_id`, `cantidad`) VALUES
(58, 26, 9, 864),
(59, 26, 7, 216),
(60, 27, 17, 1080),
(61, 28, 5, 1080),
(62, 29, 5, 1080),
(63, 30, 5, 1080),
(64, 31, 5, 1080),
(65, 32, 15, 1400),
(66, 33, 6, 1080),
(67, 34, 7, 1080),
(68, 35, 7, 1080),
(69, 36, 7, 1080),
(70, 37, 7, 1080),
(71, 38, 7, 1080),
(72, 39, 9, 864),
(73, 39, 7, 216),
(76, 42, 11, 540),
(77, 43, 1, 1080),
(78, 44, 12, 1080),
(79, 45, 13, 1080),
(80, 46, 8, 960),
(81, 47, 8, 960),
(82, 48, 13, 1080),
(83, 49, 5, 1080),
(84, 50, 5, 1080),
(85, 51, 5, 1080),
(86, 52, 15, 1400),
(88, 54, 7, 1080),
(89, 55, 7, 1080),
(90, 56, 7, 1080),
(91, 57, 7, 1080),
(95, 61, 13, 960),
(96, 62, 13, 960),
(98, 64, 13, 960),
(99, 65, 8, 960),
(100, 66, 5, 1080),
(101, 67, 5, 1080),
(102, 68, 15, 1400),
(103, 69, 6, 1080),
(104, 70, 7, 1080),
(105, 71, 7, 1080),
(106, 72, 7, 1080),
(107, 73, 7, 11080),
(108, 74, 7, 1080),
(109, 75, 7, 1080),
(110, 76, 7, 1080),
(111, 77, 9, 864),
(112, 77, 7, 216),
(113, 78, 10, 1080),
(114, 79, 10, 540),
(115, 79, 11, 540),
(116, 80, 1, 1080),
(117, 82, 12, 1080),
(118, 83, 8, 960),
(119, 84, 21, 960),
(120, 85, 21, 960),
(121, 86, 5, 1080),
(122, 87, 5, 1080),
(123, 88, 5, 1080),
(124, 89, 5, 1080),
(125, 90, 15, 1400),
(126, 91, 3, 1079),
(127, 92, 3, 1080),
(128, 93, 3, 1080),
(129, 94, 7, 1080),
(130, 95, 7, 1080),
(131, 96, 7, 216),
(132, 96, 9, 864),
(133, 97, 7, 1080),
(134, 98, 10, 1080),
(135, 99, 10, 540),
(136, 99, 11, 540),
(137, 100, 1, 1080),
(138, 101, 12, 1080),
(139, 102, 21, 960),
(140, 103, 21, 960),
(141, 104, 8, 960),
(142, 105, 8, 960),
(143, 106, 8, 960),
(144, 107, 5, 1080),
(145, 108, 5, 1080),
(146, 109, 5, 1080),
(147, 110, 15, 1400),
(148, 111, 5, 1080),
(149, 112, 5, 1080),
(150, 113, 7, 1080),
(151, 114, 7, 1080),
(152, 115, 7, 1080),
(153, 116, 3, 1080),
(154, 117, 7, 1080),
(155, 118, 7, 216),
(156, 118, 9, 864),
(157, 119, 10, 1080),
(158, 120, 10, 540),
(159, 120, 11, 540),
(160, 121, 1, 1080),
(161, 122, 12, 1080),
(162, 123, 8, 960),
(163, 124, 13, 1080),
(164, 125, 14, 1080),
(165, 126, 8, 960),
(166, 127, 5, 1080),
(167, 128, 5, 1080),
(168, 129, 5, 1080),
(169, 130, 15, 1400),
(170, 131, 7, 1080),
(171, 132, 7, 1080),
(172, 133, 7, 1080),
(173, 134, 7, 1080),
(174, 135, 7, 1080),
(175, 136, 7, 216),
(176, 136, 9, 864),
(177, 137, 2, 540),
(178, 137, 7, 540),
(179, 138, 10, 1080),
(180, 139, 10, 1080),
(181, 140, 10, 540),
(182, 140, 11, 540),
(183, 141, 12, 1080),
(184, 142, 1, 1080),
(185, 143, 13, 1080),
(186, 144, 8, 960),
(187, 145, 14, 1080),
(188, 146, 8, 960),
(189, 147, 8, 960),
(190, 148, 5, 1080),
(191, 149, 5, 1080),
(192, 150, 15, 1400),
(193, 151, 7, 1080),
(194, 152, 7, 1080),
(195, 153, 7, 1080),
(196, 154, 7, 1080),
(197, 155, 7, 1080),
(198, 156, 7, 1080),
(199, 157, 7, 216),
(200, 157, 9, 864),
(201, 158, 2, 1080),
(202, 159, 10, 1080),
(203, 160, 10, 1080),
(204, 161, 11, 540),
(205, 161, 10, 540),
(206, 162, 1, 1080),
(207, 163, 12, 1080),
(210, 166, 13, 1080),
(211, 167, 14, 1080),
(212, 168, 5, 1080),
(213, 169, 5, 1080),
(214, 170, 15, 1400),
(215, 171, 7, 1080),
(216, 172, 7, 1080),
(217, 173, 7, 1080),
(218, 174, 7, 1080),
(219, 175, 7, 1080),
(220, 176, 6, 1080),
(221, 177, 6, 1080),
(222, 178, 9, 864),
(223, 178, 7, 216),
(224, 179, 2, 1080),
(225, 180, 14, 1080),
(226, 181, 10, 1080),
(227, 182, 10, 1080),
(228, 183, 14, 1080),
(229, 184, 14, 1080),
(230, 185, 12, 1080),
(231, 186, 1, 1080),
(232, 187, 8, 960),
(233, 188, 21, 960),
(234, 189, 21, 960),
(235, 190, 5, 1080),
(236, 191, 5, 1080),
(237, 192, 15, 1400),
(238, 193, 7, 1080),
(239, 194, 7, 1080),
(240, 195, 7, 1080),
(241, 196, 7, 1080),
(242, 197, 7, 1080),
(243, 198, 7, 1080),
(244, 199, 9, 864),
(245, 199, 7, 216),
(246, 200, 7, 540),
(247, 200, 2, 540),
(248, 201, 10, 1080),
(249, 202, 10, 540),
(250, 202, 11, 540),
(251, 203, 12, 1080),
(252, 204, 1, 1080),
(253, 205, 13, 1080),
(255, 206, 21, 960),
(256, 207, 8, 960),
(257, 208, 3, 1080),
(259, 209, 7, 1080),
(260, 210, 7, 1080),
(261, 211, 5, 1080),
(264, 212, 5, 1080),
(265, 213, 5, 1080),
(266, 214, 10, 1080),
(267, 215, 10, 540),
(268, 215, 11, 540),
(269, 216, 7, 1080),
(270, 217, 7, 1080),
(271, 218, 9, 864),
(272, 218, 7, 216),
(273, 219, 12, 1080),
(274, 220, 12, 1080),
(277, 223, 13, 1080),
(278, 224, 14, 1080),
(279, 225, 21, 960),
(280, 226, 3, 1080),
(281, 228, 5, 1080),
(282, 229, 15, 1400),
(283, 230, 5, 1080),
(285, 232, 7, 1080),
(286, 233, 7, 1080),
(287, 234, 6, 1080),
(288, 235, 7, 1080),
(289, 236, 9, 1080),
(290, 237, 7, 540),
(291, 237, 2, 540),
(292, 238, 10, 1080),
(293, 239, 10, 540),
(294, 239, 11, 540),
(295, 240, 12, 1080),
(296, 241, 12, 1080),
(297, 242, 21, 960),
(298, 243, 21, 960),
(299, 244, 8, 960),
(300, 245, 8, 960),
(301, 246, 3, 1080),
(302, 247, 10, 1080),
(303, 248, 10, 1080),
(304, 249, 10, 540),
(305, 249, 11, 540),
(306, 250, 10, 1080),
(309, 252, 5, 1080),
(310, 251, 5, 1080),
(311, 253, 7, 216),
(312, 253, 9, 864),
(321, 257, 7, 1080),
(322, 256, 7, 1080),
(323, 255, 6, 1080),
(324, 254, 14, 1080),
(325, 258, 12, 1080),
(326, 259, 12, 1080),
(327, 260, 8, 960),
(328, 261, 8, 960),
(329, 262, 8, 960),
(330, 263, 3, 1080),
(332, 265, 3, 1080),
(333, 266, 5, 1080),
(334, 267, 5, 1080),
(335, 268, 5, 1080),
(336, 269, 7, 1080),
(337, 270, 7, 1080),
(339, 272, 7, 1080),
(340, 271, 6, 1080),
(341, 273, 9, 864),
(342, 273, 7, 216),
(343, 274, 13, 1080),
(345, 275, 14, 1080),
(346, 276, 10, 540),
(347, 277, 10, 1080),
(348, 276, 11, 540),
(349, 278, 12, 1080),
(350, 279, 12, 1080),
(351, 280, 8, 960),
(352, 281, 15, 1400),
(353, 282, 15, 1400),
(354, 283, 3, 1080),
(356, 285, 3, 1080),
(357, 286, 3, 1080),
(363, 291, 2, 1080),
(365, 290, 7, 1080),
(366, 289, 7, 1080),
(367, 292, 7, 1080),
(369, 293, 7, 432),
(370, 293, 9, 648),
(371, 294, 10, 1080),
(373, 296, 10, 1080),
(374, 297, 10, 1080),
(375, 295, 10, 540),
(376, 295, 11, 540),
(377, 298, 8, 960),
(378, 299, 8, 960),
(379, 300, 14, 1080),
(381, 301, 13, 1080),
(382, 302, 1, 1080),
(384, 303, 12, 1080),
(385, 304, 5, 1080),
(387, 306, 5, 1080),
(388, 305, 3, 1080),
(389, 307, 2, 1080),
(390, 308, 2, 1080),
(391, 309, 7, 1080),
(392, 310, 7, 1080),
(393, 311, 7, 1080),
(394, 312, 7, 1080),
(395, 313, 7, 1080),
(397, 314, 9, 1080),
(398, 315, 5, 1080),
(400, 317, 5, 1080),
(401, 316, 3, 1080),
(402, 318, 10, 1080),
(404, 319, 10, 540),
(405, 319, 11, 540),
(406, 320, 8, 960),
(407, 321, 21, 960),
(408, 322, 21, 960),
(409, 323, 17, 1080),
(410, 324, 17, 1080),
(411, 325, 17, 1080),
(412, 326, 17, 1080),
(419, 333, 2, 1080),
(420, 330, 7, 1080),
(421, 332, 7, 1080),
(422, 331, 7, 1080),
(423, 334, 9, 1080),
(425, 335, 6, 1080),
(426, 336, 7, 1080),
(427, 337, 7, 1080),
(428, 338, 7, 1080),
(429, 339, 7, 1080),
(430, 340, 10, 1080),
(432, 342, 10, 1080),
(433, 341, 10, 540),
(434, 341, 11, 540),
(435, 343, 2, 1080),
(436, 344, 2, 1080),
(437, 345, 8, 960),
(438, 346, 8, 960),
(439, 347, 21, 960),
(440, 348, 21, 960),
(443, 351, 3, 1080),
(444, 352, 5, 1080),
(445, 353, 5, 1080),
(446, 354, 5, 1080),
(447, 355, 2, 1080),
(448, 356, 2, 1080),
(452, 358, 7, 1080),
(453, 357, 7, 1080),
(454, 359, 7, 270),
(455, 359, 9, 810),
(456, 360, 7, 1080),
(457, 361, 7, 1080),
(458, 362, 7, 1080),
(459, 363, 7, 1080),
(460, 364, 7, 1080),
(461, 365, 7, 1080),
(462, 366, 7, 1080),
(463, 367, 10, 1080),
(465, 368, 10, 540),
(466, 368, 11, 540),
(467, 369, 8, 960),
(468, 63, 21, 960),
(469, 60, 1, 1080),
(470, 59, 10, 594),
(471, 59, 11, 486),
(472, 58, 7, 216),
(473, 58, 9, 864),
(474, 370, 12, 1080),
(475, 371, 7, 1080),
(476, 372, 3, 1080),
(477, 373, 6, 1080),
(478, 374, 6, 1080),
(479, 375, 6, 1080),
(480, 376, 2, 1080),
(481, 377, 7, 1080),
(482, 378, 7, 1080),
(483, 379, 7, 1080),
(484, 380, 7, 1080),
(485, 381, 7, 1080),
(486, 382, 7, 1080),
(487, 383, 7, 1080),
(488, 384, 7, 1080),
(489, 385, 6, 1080),
(490, 386, 10, 1080),
(491, 387, 10, 1080),
(492, 388, 10, 1080),
(493, 389, 10, 540),
(494, 389, 11, 540),
(495, 390, 8, 960),
(496, 391, 21, 960),
(499, 394, 3, 1080),
(503, 395, 5, 1080),
(504, 397, 2, 1080),
(505, 396, 5, 1080),
(508, 399, 5, 1080),
(509, 400, 7, 1080),
(510, 401, 7, 1080),
(511, 402, 7, 1080),
(512, 403, 7, 1080),
(513, 404, 7, 1080),
(515, 406, 7, 1080),
(516, 407, 7, 1080),
(517, 408, 7, 1080),
(518, 405, 7, 270),
(519, 405, 9, 810),
(520, 409, 2, 1080),
(521, 410, 8, 960),
(522, 411, 8, 960),
(526, 412, 1, 1080),
(528, 416, 6, 1080),
(530, 418, 6, 1080),
(531, 417, 2, 1080),
(532, 415, 7, 1080),
(533, 419, 3, 1080),
(537, 422, 5, 1080),
(538, 421, 5, 1080),
(539, 420, 7, 1080),
(540, 423, 7, 1080),
(541, 424, 7, 1080),
(542, 425, 7, 1080),
(543, 426, 7, 1080),
(544, 427, 7, 1080),
(547, 430, 7, 1080),
(548, 429, 2, 1080),
(549, 428, 7, 432),
(550, 428, 9, 648),
(553, 431, 11, 540),
(554, 431, 10, 540),
(555, 433, 8, 960),
(556, 434, 21, 960),
(557, 435, 21, 960),
(558, 436, 1, 1080),
(559, 437, 1, 1080),
(560, 438, 1, 1080),
(561, 439, 6, 1080),
(562, 440, 6, 1080),
(567, 444, 7, 1080),
(568, 443, 7, 1080),
(569, 442, 7, 1080),
(570, 441, 2, 1080),
(573, 447, 3, 1080),
(574, 446, 5, 1080),
(575, 445, 5, 1080),
(577, 449, 7, 1080),
(578, 450, 7, 1080),
(579, 451, 9, 864),
(580, 451, 7, 216),
(581, 452, 5, 1080),
(583, 454, 5, 1080),
(584, 455, 15, 1400),
(585, 456, 7, 1080),
(586, 457, 7, 1080),
(587, 458, 7, 1080),
(593, 462, 7, 1080),
(594, 461, 7, 1080),
(596, 459, 7, 216),
(597, 459, 9, 864),
(599, 465, 5, 1080),
(600, 466, 5, 1080),
(603, 467, 7, 1080),
(604, 468, 7, 1080),
(605, 469, 7, 1080),
(606, 470, 7, 1080),
(607, 471, 7, 1080),
(620, 476, 7, 378),
(621, 476, 9, 648),
(622, 476, 4, 54),
(623, 475, 2, 1080),
(624, 474, 10, 1080),
(625, 473, 10, 540),
(626, 473, 11, 540),
(627, 472, 8, 960),
(629, 478, 21, 960),
(636, 483, 6, 1080),
(640, 485, 7, 1080),
(641, 484, 7, 1080),
(642, 487, 7, 1080),
(643, 488, 5, 1080),
(644, 489, 5, 1080),
(647, 491, 7, 1080),
(648, 492, 7, 1080),
(650, 494, 7, 1080),
(651, 493, 2, 1080),
(652, 495, 7, 378),
(653, 495, 9, 648),
(654, 495, 4, 54),
(656, 497, 10, 1080),
(657, 498, 10, 1080),
(658, 499, 10, 1080),
(659, 496, 10, 540),
(660, 496, 11, 540),
(661, 500, 8, 960),
(662, 501, 21, 960),
(663, 502, 21, 960),
(664, 503, 1, 1080),
(665, 504, 1, 1080),
(668, 505, 10, 540),
(669, 505, 11, 540),
(670, 453, 12, 1080),
(671, 506, 1, 1080),
(672, 507, 12, 1080),
(673, 508, 13, 1080),
(675, 509, 21, 960),
(676, 510, 8, 960),
(677, 511, 8, 960),
(678, 512, 7, 1080),
(679, 513, 7, 1080),
(681, 514, 7, 1080),
(682, 515, 7, 1080),
(683, 516, 7, 1080),
(686, 518, 9, 864),
(687, 518, 7, 216),
(689, 520, 5, 1080),
(690, 521, 5, 1080),
(691, 519, 15, 1400),
(692, 464, 15, 1400),
(693, 463, 6, 1080),
(694, 460, 7, 1080),
(695, 522, 10, 1080),
(697, 523, 10, 540),
(698, 523, 11, 540),
(699, 524, 12, 1080),
(701, 525, 1, 1080),
(702, 526, 8, 960),
(703, 527, 8, 960),
(704, 528, 13, 1080),
(706, 529, 14, 1080),
(707, 530, 7, 1080),
(708, 531, 7, 1080),
(709, 532, 7, 1080),
(718, 537, 6, 1080),
(720, 536, 7, 1080),
(721, 535, 7, 1080),
(722, 534, 7, 1080),
(723, 533, 7, 216),
(724, 533, 9, 864),
(727, 540, 5, 1080),
(729, 538, 5, 1080),
(730, 539, 15, 1400),
(731, 517, 6, 1080),
(732, 541, 10, 1080),
(734, 542, 10, 540),
(735, 542, 11, 540),
(736, 543, 10, 540),
(737, 543, 11, 540),
(739, 545, 12, 1080),
(740, 544, 1, 1080),
(742, 547, 13, 1080),
(743, 546, 21, 960),
(744, 548, 8, 960),
(745, 549, 8, 960),
(746, 550, 3, 1080),
(749, 552, 7, 1080),
(750, 553, 6, 1080),
(751, 554, 7, 1080),
(752, 555, 7, 1080),
(754, 556, 9, 576),
(755, 556, 7, 504),
(756, 557, 3, 1080),
(758, 558, 5, 1080),
(759, 559, 5, 1080),
(761, 560, 15, 1400),
(762, 561, 10, 1080),
(763, 562, 10, 540),
(764, 562, 11, 540),
(766, 564, 12, 1080),
(767, 563, 1, 1080),
(769, 566, 13, 1080),
(770, 565, 21, 960),
(771, 567, 8, 960),
(772, 568, 8, 960),
(773, 569, 7, 1080),
(774, 570, 7, 1080),
(776, 572, 6, 1080),
(777, 571, 7, 1080),
(778, 573, 7, 1080),
(780, 574, 9, 576),
(781, 574, 7, 504),
(782, 575, 3, 1080),
(783, 576, 5, 1080),
(784, 577, 5, 1080),
(785, 578, 15, 1400),
(786, 551, 7, 1080),
(787, 579, 6, 1080),
(789, 581, 6, 1080),
(790, 580, 5, 1080),
(791, 582, 10, 1080),
(792, 583, 10, 1080),
(794, 584, 10, 540),
(795, 584, 11, 540),
(796, 585, 1, 1080),
(798, 586, 12, 1080),
(799, 587, 8, 960),
(800, 588, 13, 1080),
(802, 589, 8, 960),
(803, 590, 7, 1080),
(804, 591, 7, 1080),
(806, 593, 6, 1080),
(807, 592, 7, 1080),
(808, 594, 7, 1080),
(809, 595, 7, 1080),
(811, 596, 9, 576),
(812, 596, 7, 504),
(815, 599, 3, 1080),
(816, 598, 5, 1080),
(817, 597, 5, 1080),
(818, 600, 15, 1400),
(819, 601, 7, 1080),
(821, 603, 10, 1080),
(822, 602, 10, 540),
(823, 602, 11, 540),
(825, 605, 1, 1080),
(826, 604, 12, 1080),
(827, 606, 8, 960),
(829, 608, 21, 960),
(830, 607, 13, 1080),
(831, 609, 7, 1080),
(832, 610, 7, 1080),
(835, 613, 6, 1080),
(836, 611, 9, 576),
(837, 611, 7, 504),
(839, 614, 7, 1080),
(840, 615, 3, 1080),
(841, 616, 3, 1080),
(846, 621, 3, 1080),
(847, 620, 5, 1080),
(848, 619, 5, 1080),
(849, 618, 5, 1080),
(851, 622, 10, 1080),
(852, 623, 10, 1080),
(854, 624, 10, 540),
(855, 624, 11, 540),
(856, 625, 1, 1080),
(858, 626, 12, 1080),
(862, 629, 21, 960),
(863, 628, 13, 1080),
(864, 627, 8, 960),
(865, 630, 10, 1080),
(867, 631, 11, 540),
(868, 632, 13, 1080),
(869, 633, 1, 1080),
(871, 634, 12, 1080),
(872, 635, 8, 960),
(873, 636, 8, 960),
(874, 637, 6, 1080),
(878, 638, 7, 1080),
(879, 639, 7, 1080),
(880, 640, 9, 864),
(881, 640, 7, 216),
(882, 641, 3, 1080),
(885, 642, 5, 1080),
(887, 644, 7, 1080),
(889, 645, 15, 1400),
(891, 647, 3, 1080),
(892, 617, 15, 1400),
(893, 612, 7, 1080),
(895, 648, 8, 960),
(896, 649, 8, 960),
(897, 650, 13, 1080),
(903, 643, 5, 1080),
(904, 646, 7, 1080),
(906, 655, 12, 1080),
(908, 657, 7, 1080),
(909, 658, 6, 1080),
(910, 659, 7, 1080),
(911, 660, 7, 1080),
(912, 661, 9, 864),
(913, 661, 7, 216),
(914, 662, 7, 1080),
(916, 663, 7, 1080),
(917, 656, 7, 1080),
(918, 664, 3, 1080),
(919, 665, 5, 1080),
(920, 666, 5, 1080),
(921, 667, 15, 1400),
(922, 668, 8, 960),
(923, 669, 8, 960),
(924, 670, 13, 1080),
(925, 671, 12, 1080),
(926, 672, 1, 1080),
(929, 674, 11, 540),
(930, 674, 10, 540),
(931, 675, 5, 1080),
(932, 676, 5, 1080),
(934, 678, 3, 1080),
(935, 677, 3, 1080),
(936, 679, 6, 1080),
(937, 680, 15, 1400),
(938, 681, 7, 1080),
(939, 682, 7, 1080),
(940, 683, 7, 1080),
(941, 684, 7, 1080),
(944, 687, 1, 1080),
(945, 688, 12, 1080),
(946, 689, 9, 864),
(947, 689, 7, 216),
(948, 690, 21, 960),
(949, 691, 13, 1080),
(950, 692, 11, 540),
(951, 692, 10, 540),
(952, 693, 8, 960),
(953, 694, 8, 960),
(954, 695, 7, 1080),
(955, 696, 7, 1080),
(956, 697, 7, 1080),
(957, 698, 7, 1080),
(958, 699, 6, 1080),
(959, 700, 6, 1080),
(960, 701, 7, 1080),
(961, 702, 7, 1080),
(962, 703, 9, 864),
(963, 703, 7, 216),
(964, 704, 2, 1080),
(965, 705, 3, 1080),
(966, 706, 5, 1080),
(967, 707, 5, 1080),
(968, 708, 15, 1400),
(969, 654, 1, 1080),
(970, 653, 10, 540),
(971, 653, 11, 540),
(972, 709, 5, 1080),
(973, 710, 5, 1080),
(974, 711, 3, 1080),
(975, 712, 7, 1080),
(976, 713, 7, 1080),
(977, 714, 3, 1080),
(978, 715, 3, 1080),
(979, 716, 9, 648),
(980, 716, 7, 432),
(981, 717, 2, 1080),
(982, 718, 10, 1080),
(983, 719, 10, 540),
(984, 719, 11, 540),
(985, 720, 10, 1080),
(986, 721, 12, 1080),
(987, 722, 6, 1080),
(988, 723, 1, 1080),
(989, 724, 13, 1080),
(990, 725, 3, 1080),
(991, 726, 8, 960),
(992, 727, 8, 960),
(993, 728, 7, 1080),
(994, 729, 7, 1080),
(995, 730, 7, 1080),
(996, 731, 7, 1080),
(997, 732, 6, 1080),
(998, 733, 9, 864),
(999, 733, 7, 216),
(1000, 734, 2, 1080),
(1001, 735, 2, 1080),
(1002, 736, 3, 1080),
(1003, 737, 5, 1080),
(1004, 738, 5, 1080),
(1005, 739, 5, 1080),
(1006, 740, 15, 1400),
(1007, 741, 8, 960),
(1008, 742, 8, 960),
(1009, 743, 10, 1080),
(1010, 744, 10, 540),
(1011, 744, 11, 540),
(1012, 745, 1, 1080),
(1013, 746, 12, 1080),
(1014, 747, 13, 1080),
(1015, 748, 14, 1080),
(1016, 749, 13, 1080),
(1017, 750, 12, 1080),
(1018, 432, 10, 1080),
(1019, 398, 7, 1080),
(1020, 349, 1, 1080),
(1023, 288, 5, 1080),
(1024, 287, 5, 1080),
(1025, 284, 5, 1080),
(1026, 751, 21, 960),
(1027, 231, 5, 1080),
(1028, 221, 8, 960),
(1029, 222, 8, 960),
(1030, 164, 8, 960),
(1031, 165, 8, 960),
(1032, 41, 10, 1080),
(1033, 42, 10, 540),
(1034, 53, 7, 1080),
(1035, 448, 10, 1080),
(1036, 631, 10, 540),
(1037, 652, 10, 1080),
(1038, 651, 10, 1080),
(1039, 673, 10, 1080),
(1040, 686, 10, 1080),
(1041, 685, 10, 1080),
(1042, 752, 13, 1080),
(1043, 753, 14, 1080),
(1044, 486, 6, 1080),
(1045, 754, 12, 1080),
(1046, 755, 1, 1080),
(1047, 756, 10, 1080),
(1048, 757, 10, 540),
(1049, 757, 11, 540),
(1050, 758, 8, 960),
(1051, 759, 8, 960),
(1052, 490, 3, 1080),
(1053, 477, 13, 1080),
(1054, 480, 1, 1080),
(1055, 479, 1, 1080),
(1056, 481, 12, 1080),
(1057, 482, 12, 1080),
(1058, 760, 7, 1080),
(1059, 413, 12, 1080),
(1060, 414, 12, 1080),
(1061, 392, 12, 1080),
(1062, 393, 1, 1080),
(1063, 350, 12, 1080),
(1064, 329, 12, 1080),
(1065, 328, 12, 1080),
(1066, 327, 12, 1080),
(1067, 264, 3, 1080),
(1068, 761, 12, 1080),
(1069, 762, 7, 1080),
(1070, 763, 10, 1080),
(1072, 765, 5, 1080),
(1073, 764, 5, 1080),
(1074, 766, 5, 1080);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `nombre`, `activo`) VALUES
(1, 'Gerente', 1),
(2, 'Administrador', 1),
(8, 'Cliente', 1),
(9, 'Jefe Trazabilidad', 1),
(10, 'Auxiliar Trazabilidad', 1),
(11, 'Operador Logístico', 1),
(12, 'Ing. Agrónoma', 1),
(13, 'Otros', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificado`
--

CREATE TABLE `certificado` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `id_operador` int(11) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` text NOT NULL,
  `descripcion` text,
  `id_estado_archivo` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `certificado`
--

INSERT INTO `certificado` (`id`, `id_cliente`, `id_semana`, `id_operador`, `nombre`, `fecha`, `hora`, `url`, `descripcion`, `id_estado_archivo`) VALUES
(1, 1, 53, NULL, '1_30_2017_(1)PRIMA ABRIL 2017.pdf', '2017-08-07', '11:37:57', '1_30_2017_(1)PRIMA ABRIL 2017.pdf', 'nada', 2),
(5, 3, 57, NULL, '3_34_2017_(1)DOLE-34BEL.PDF', '2017-09-08', '07:51:58', '3_34_2017_(1)DOLE-34BEL.PDF', 'CERTIFICADO EMITIDO  CONFIRMADO', 2),
(6, 2, 57, NULL, '2_34_2017_(1)BD-34.pdf', '2017-09-08', '07:59:28', '2_34_2017_(1)BD-34.pdf', 'CERTIFICADO EMITIDO Y CONFIRMADO', 2),
(7, 1, 57, NULL, '1_34_2017_(2)AGF-34RTT.pdf', '2017-09-08', '08:01:50', '1_34_2017_(2)AGF-34RTT.pdf', 'CERTIFICADO EMITIDO Y CONFIRMADO', 2),
(8, 1, 57, NULL, '1_34_2017_(3)AGF-34HAM.pdf', '2017-09-08', '08:03:17', '1_34_2017_(3)AGF-34HAM.pdf', 'CERTIFICADO EMITIDO Y CONFIRMADO', 2),
(9, 2, 58, NULL, '2_35_2017_(2)W-35BD.pdf', '2017-09-08', '13:50:19', '2_35_2017_(2)W-35BD.pdf', 'CERTIFICADO TRAMITADO Y CONFIRMADO', 2),
(10, 1, 58, NULL, '1_35_2017_(4)W-35AGFRTT.pdf', '2017-09-08', '13:55:24', '1_35_2017_(4)W-35AGFRTT.pdf', 'CERTIFICADO EMITIDO Y CONFRIMADO / IMPRESION EN DESTINO', 2),
(11, 3, 58, NULL, '3_35_2017_(2)W-35DOLE.PDF', '2017-09-09', '07:48:33', '3_35_2017_(2)W-35DOLE.PDF', 'TC CONFIRMADO', 2),
(12, 4, 58, NULL, '4_35_2017_(1)W-35TA.PDF', '2017-09-09', '07:49:45', '4_35_2017_(1)W-35TA.PDF', 'TC CONFIRMADO', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificado_contenedor`
--

CREATE TABLE `certificado_contenedor` (
  `id` int(11) NOT NULL,
  `certificado_id` int(11) NOT NULL,
  `contenedor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `certificado_contenedor`
--

INSERT INTO `certificado_contenedor` (`id`, `certificado_id`, `contenedor_id`) VALUES
(6, 5, 724),
(7, 6, 718),
(8, 6, 719),
(9, 6, 720),
(10, 7, 695),
(11, 7, 712),
(12, 7, 713),
(13, 7, 717),
(14, 7, 716),
(15, 7, 715),
(16, 7, 714),
(17, 7, 722),
(18, 7, 725),
(19, 8, 709),
(20, 8, 710),
(21, 8, 711),
(22, 9, 756),
(23, 9, 757),
(24, 10, 696),
(25, 10, 698),
(26, 10, 697),
(27, 10, 701),
(28, 10, 703),
(29, 10, 704),
(30, 10, 702),
(31, 10, 699),
(32, 10, 700),
(33, 11, 752),
(34, 11, 753),
(35, 12, 754),
(36, 12, 755);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id`, `id_pais`, `nombre`, `activo`) VALUES
(1, 1, 'HOLANDA MERI...', 1),
(2, 2, 'HAMBURGO', 1),
(3, 3, 'CANADA', 1),
(4, 1, 'HOLANDA', 1),
(5, 1, 'DOVER', 1),
(6, 3, 'LOUSIANA', 1),
(7, 3, 'CALIFORNIA', 1),
(8, 3, 'RODMAN', 1),
(9, 4, 'PANAMA', 1),
(10, 4, 'Paita', 0),
(11, 3, 'DOVER USA', 1),
(12, 3, 'FLORIDA', 1),
(13, 5, 'BELGICA', 1),
(14, 3, 'NEW YOURK', 1),
(15, 6, 'JAPON', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `razon_social` varchar(50) NOT NULL,
  `slug` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `razon_social`, `slug`, `email`, `telefono`, `activo`) VALUES
(1, 'AGROFAIR', 'agrofair', '', '', 1),
(2, 'BIODYNAMISKA', 'biodynamiska', '', '', 1),
(3, 'DOLE', 'dole', '', '', 1),
(4, 'TRANSASTRA', 'transastra', '', '', 1),
(5, 'EQUIFRUIT', 'equifruit', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_operador`
--

CREATE TABLE `cliente_operador` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `operador_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente_operador`
--

INSERT INTO `cliente_operador` (`id`, `cliente_id`, `operador_id`) VALUES
(1, 1, 15),
(2, 2, 1),
(3, 3, 1),
(10, 5, 1),
(11, 4, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenedor`
--

CREATE TABLE `contenedor` (
  `id` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `referencia` varchar(20) NOT NULL,
  `booking` varchar(20) NOT NULL,
  `numero_contenedor` varchar(50) DEFAULT NULL,
  `id_lineanaviera` int(11) NOT NULL,
  `nave` varchar(50) NOT NULL,
  `viaje` varchar(20) NOT NULL,
  `id_puertodestino` int(11) NOT NULL,
  `id_operador` int(11) NOT NULL,
  `fecha_proceso_inicio` date NOT NULL,
  `fecha_proceso_fin` date NOT NULL,
  `fecha_zarpe` date DEFAULT NULL,
  `fecha_llegada` date DEFAULT NULL,
  `peso_bruto` double DEFAULT NULL,
  `peso_neto` double DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `factura` tinyint(1) NOT NULL DEFAULT '1',
  `certificado` tinyint(1) NOT NULL DEFAULT '1',
  `packing` tinyint(1) NOT NULL DEFAULT '1',
  `valija` tinyint(1) NOT NULL DEFAULT '1',
  `vl` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contenedor`
--

INSERT INTO `contenedor` (`id`, `id_semana`, `id_cliente`, `referencia`, `booking`, `numero_contenedor`, `id_lineanaviera`, `nave`, `viaje`, `id_puertodestino`, `id_operador`, `fecha_proceso_inicio`, `fecha_proceso_fin`, `fecha_zarpe`, `fecha_llegada`, `peso_bruto`, `peso_neto`, `activo`, `factura`, `certificado`, `packing`, `valija`, `vl`) VALUES
(26, 22, 1, '22513', '960685565', 'PONU 481624-0', 5, 'NORTHERN DEXTERITY 4', '1708', 1, 2, '2017-05-29', '2017-05-30', '2017-05-31', '2017-06-01', NULL, NULL, 0, 1, 1, 1, 1, 1),
(27, 22, 2, '1223', '1222', NULL, 2, 'rrrrrggg', '133', 1, 7, '2017-05-09', '2017-05-11', '2017-05-22', '2017-05-24', NULL, NULL, 0, 1, 1, 1, 1, 1),
(28, 23, 1, '22595', '960758731', 'PONU 497834-4', 5, 'MSK-SEALAND MANZANILLA', '1708', 1, 2, '2017-05-22', '2017-05-22', '2017-05-28', '2017-06-16', 22320, 19591.2, 1, 1, 1, 1, 1, 1),
(29, 23, 1, '22596', '960758731', 'MNBU 315895-0', 5, 'MSK-SEALAND MANZANILLA', '1708', 1, 2, '2017-05-22', '2017-05-22', '2017-05-28', '2017-06-16', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(30, 23, 1, '22597', '960758731', 'PONU 480767-6', 5, 'MSK-SEALAND MANZANILLA', '1708', 1, 2, '2017-05-22', '2017-05-22', '2017-05-28', '2017-06-16', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(31, 23, 1, '22598', '960758731', 'PONU 482052-8', 5, 'SEALAND MANZANILLA', '1708', 1, 2, '2017-05-23', '2017-05-23', '2017-05-28', '2017-06-16', 22410, 19591.2, 1, 1, 1, 1, 1, 1),
(32, 23, 1, '22599', '960758739', 'MMAU 107222-6', 5, 'SEALAND MANZANILLA', '1708', 7, 2, '2017-05-23', '2017-05-23', '2017-05-28', '2017-06-29', 21410, 18200, 1, 1, 1, 1, 1, 1),
(33, 23, 1, '22588', 'MBM140178149', 'BMOU 965137-1', 8, 'POLAR LIGHT', 'SR 17014EB', 3, 2, '2017-05-24', '2017-05-24', '2017-05-28', '2017-06-15', 22150, 19591.2, 1, 1, 1, 1, 1, 1),
(34, 23, 1, '22589', 'MBM140178149', 'TTNU 896246-2', 8, 'POLAR LIGHT', 'SR 17014EB', 3, 2, '2017-05-24', '2017-05-24', '2017-05-28', '2017-06-15', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(35, 23, 1, '22590', 'MBM 140178149', 'BMOU 961635-0', 8, 'POLAR LIGHT', 'SR 17014EB', 3, 2, '2017-05-24', '2017-05-24', '2017-05-28', '2017-06-15', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(36, 23, 1, '22591', '7LIMES2775', 'SUDU 605016-3', 3, 'HSD-MAGARI', '722', 3, 2, '2017-05-24', '2017-05-24', '2017-05-30', '2017-06-20', 22320, 19591.2, 1, 1, 1, 1, 1, 1),
(37, 23, 1, '22592', '7LIMES2775', 'SUDU 605417-4', 3, 'HSD-MAGARI', '722', 3, 2, '2017-05-25', '2017-05-25', '2017-05-30', '2017-06-20', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(38, 23, 1, '22593', '7LIMES2775', 'SUDU 604948-1', 3, 'HSD-MAGARI', '722', 3, 2, '2017-05-25', '2017-05-25', '2017-05-30', '2017-06-20', 22350, 19591.2, 1, 1, 1, 1, 1, 1),
(39, 23, 1, '22594', '7LIMES2775', 'SUDU 529208-0', 3, 'HSD-MAGARI', '722', 3, 2, '2017-05-25', '2017-05-25', '2017-05-30', '2017-06-20', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(41, 23, 2, '1', '12629836', 'HLXU 876009-0', 4, 'HLE- MAGARI', '722', 3, 1, '2017-05-26', '2017-05-26', '2017-05-30', '2017-06-20', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(42, 23, 2, '2', '12629836', 'HLBU 900451-0', 4, 'HLE- MAGARI', '722', 3, 1, '2017-05-26', '2017-05-26', '2017-05-30', '2017-06-20', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(43, 23, 4, '1', '98517939', 'TCLU 102844-3', 4, 'HLE- MAGARI', '722', 3, 2, '2017-05-27', '2017-05-27', '2017-05-30', '2017-06-20', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(44, 23, 4, '2', '98517939', 'CRLU 137750-9', 4, 'HLE- MAGARI', '722', 3, 2, '2017-05-27', '2017-05-27', '2017-05-30', '2017-06-20', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(45, 23, 3, '1', 'LMM0215232', 'TLLU 106897-4', 2, 'MAGARI', '016', 6, 1, '2017-05-27', '2017-05-27', '2017-05-29', '2017-06-20', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(46, 23, 5, '1', '7LIMAG1275', 'SUDU 823613-6', 3, 'HSD-MAGARI', '722', 2, 2, '2017-05-27', '2017-05-27', '2017-05-30', '2017-06-20', 19690, 17414.4, 1, 1, 1, 1, 1, 1),
(47, 23, 5, '2', '7LIMAG1275', 'SUDU  523335-0', 3, 'HSD-MAGARI', '722', 2, 2, '2017-05-28', '2017-05-28', '2017-05-30', '2017-06-20', 19680, 17414.4, 1, 1, 1, 1, 1, 1),
(48, 23, 3, '2', 'B6JBK024778', 'CXRU 161009-2', 9, 'ALIOTH', '171', 6, 1, '2017-05-28', '2017-05-28', '2017-05-31', '2017-06-20', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(49, 24, 1, '22694', '960851846', 'MWCU 657259-1', 5, 'SEALAND GUAYAQUIL', '1710', 1, 2, '2017-05-28', '2017-05-28', '2017-06-02', '2017-06-25', 22360, 19591.2, 1, 1, 1, 1, 1, 1),
(50, 24, 1, '22695', '960851846', 'DAYU 670586-6', 5, 'SEALAND GUAYAQUIL', '1710', 1, 2, '2017-05-29', '2017-05-29', '2017-06-02', '2017-06-25', 22370, 19591.2, 1, 1, 1, 1, 1, 1),
(51, 24, 1, '22696', '960851846', 'MSWU 102357-9', 5, 'SEALAND GUAYAQUIL', '1710', 1, 2, '2017-05-30', '2017-05-30', '2017-06-02', '2017-06-25', 22420, 19591.2, 1, 1, 1, 1, 1, 1),
(52, 24, 1, '22698', '960851855', 'MMAU 107723-3', 5, 'SEALAND GUAYAQUIL', '1710', 7, 2, '2017-05-30', '2017-05-30', '2017-06-02', '2017-07-02', 21330, 18200, 1, 1, 1, 1, 1, 1),
(53, 24, 1, '22687', 'MBM140178152', 'TCLU 138551-7', 8, 'NEDERLAND REEFER', 'RA17015EB', 3, 2, '2017-05-30', '2017-05-30', '2017-06-05', '2017-05-22', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(54, 24, 1, '22688', 'MBM140178152', 'SZLU 980331-0', 8, 'NEDERLAND REEFER', 'RA17015EB', 3, 2, '2017-06-02', '2017-06-02', '2017-06-05', '2017-06-22', 22420, 19591.2, 1, 1, 1, 1, 1, 1),
(55, 24, 1, '22689', 'MBM140178152', 'RRSU 100351-4', 8, 'NEDERLAND REEFER', 'RA17015EB', 3, 2, '2017-06-02', '2017-06-02', '2017-06-05', '2017-06-22', 22390, 19591.2, 1, 1, 1, 1, 1, 1),
(56, 24, 1, '22690', '7LIMES2945', 'CNIU 222198-8', 3, 'MAGARI', '723', 3, 2, '2017-05-30', '2017-05-30', '2017-06-06', '2017-06-27', 22140, 19591.2, 1, 1, 1, 1, 1, 1),
(57, 24, 1, '22691', '7LIMES2945', 'SUDU 600809-7', 3, 'MAGARI', '723', 3, 2, '2017-06-03', '2017-06-03', '2017-06-06', '2017-06-27', 22370, 19591.2, 1, 1, 1, 1, 1, 1),
(58, 24, 1, '22693', '7LIMES2945', 'TEMU 906017-7', 3, 'MAGARI', '723', 3, 2, '2017-06-03', '2017-06-03', '2017-06-06', '2017-06-27', 22450, 19591.2, 1, 1, 1, 1, 1, 1),
(59, 24, 2, '1', '98538041', 'HXLU 877232-1', 4, 'MAGARI', '723', 3, 1, '2017-06-03', '2017-06-03', '2017-06-08', '2017-07-08', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(60, 24, 4, '1', '087LIM234655', 'MSCU 7433652', 7, 'EANLAND LOS ANGELES', '1710', 3, 2, '2017-06-05', '2017-06-05', '2017-06-10', '2017-07-10', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(61, 24, 3, '1', '1', NULL, 2, 'MAGARI', '1', 6, 1, '2017-06-04', '2017-06-04', NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1),
(62, 24, 3, '1', '1', NULL, 2, 'MAGARI', '1', 6, 1, '2017-06-04', '2017-06-04', NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1),
(63, 24, 3, '1', 'B6JBK024823', 'DFIU 215125-2', 9, 'DOLE', '1', 4, 1, '2017-06-04', '2017-06-04', '2017-06-09', '2017-07-09', 19530, 17414.4, 1, 1, 1, 1, 1, 1),
(64, 24, 3, '1', '1', NULL, 2, 'MAGARI', '1', 6, 1, '2017-06-04', '2017-06-04', NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1),
(65, 24, 5, '1', '7LIMAG1334', 'SUDU 621885-3', 3, 'MAGARI', '723', 2, 2, '2017-06-04', '2017-06-04', '2017-06-09', '2017-07-09', 19680, 17414.4, 1, 1, 1, 1, 1, 1),
(66, 25, 1, '22513', '960685565', 'PONU 481624-0', 5, 'SEALAND PHILADELPHIA', '1708', 1, 2, '2017-05-13', '2017-05-13', '2017-05-17', '2017-06-03', 22360, 19591.2, 1, 1, 1, 1, 1, 1),
(67, 25, 1, '22514', '960685565', 'MNBU 040678-1', 5, 'SEALAND PHILADELPHIA', '1708', 1, 2, '2017-05-16', '2017-05-16', '2017-05-24', '2017-05-26', 22340, 19591.2, 1, 1, 1, 1, 1, 1),
(68, 25, 1, '22515', '960685575', 'MMAU 119589-5', 5, 'SEALAND PHILADELPHIA', '1708', 7, 2, '2017-05-16', '2017-05-16', '2017-05-24', '2017-06-15', 21300, 18200, 1, 1, 1, 1, 1, 1),
(69, 25, 1, '22505', 'MBM140178147', 'BMOU 961408-5', 8, 'SCHWEIZ REEFER', '17013', 3, 2, '2017-05-17', '2017-05-17', '2017-05-25', '2017-06-17', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(70, 25, 1, '22506', 'MBM140178147', 'BMOU 961479-0', 8, 'SCHWEIZ REEFER', '17013', 3, 2, '2017-05-17', '2017-05-17', '2017-05-25', '2017-05-25', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(71, 25, 1, '22507', 'MBM140178147', 'BMOU 961456-8', 7, 'SCHWEIZ REEFER', '17013', 3, 2, '2017-05-17', '2017-05-17', '2017-05-25', '2017-05-28', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(72, 25, 1, '22508', 'MBM140178147', 'CXRU 153116-2', 8, 'SCHWEIZ REEFER', '17013', 3, 2, '2017-05-18', '2017-05-18', '2017-05-25', '2017-06-14', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(73, 25, 1, '22509', '7LIMES2647', 'SUDU 603547-2', 3, 'MAGARI', '721', 3, 2, '2017-05-18', '2017-05-18', '2017-05-25', '2017-05-25', 22240, 200991.2, 1, 1, 1, 1, 1, 1),
(74, 25, 1, '22510', '7LIMES2647', 'SUDU 527211-9', 3, 'MAGARI', '721', 3, 2, '2017-05-18', '2017-05-18', '2017-05-28', '2017-06-21', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(75, 25, 1, '22511', '7LIMES2647', 'SUDU 622437-3', 3, 'MAGARI', '721', 3, 2, '2017-05-19', '2017-05-19', '2017-05-29', '2017-06-24', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(76, 25, 1, '22574', '7LIMES2647', 'SUDU 610493-2', 3, 'MAGARI', '721', 3, 2, '2017-05-19', '2017-05-19', '2017-05-27', '2017-06-22', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(77, 25, 1, '22512', '7LIMES2647', 'SUDU 514549-6', 3, 'MAGARI', '721', 3, 2, '2017-05-19', '2017-05-19', '2017-05-28', '2017-06-29', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(78, 25, 2, '1', '98481475', 'HLBU 900941-0', 4, 'MAGARI', '721', 3, 1, '2017-05-19', '2017-05-19', '2017-05-29', '2017-06-29', 22110, 19591.2, 1, 1, 1, 1, 1, 1),
(79, 25, 2, '2', '98481475', 'HLXU 876061-3', 4, 'MAGARI', '721', 3, 1, '2017-05-20', '2017-05-20', '2017-05-30', '2017-06-30', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(80, 25, 4, '1', '98488003', 'FSCU 565916-0', 4, 'MAGARI', '721', 3, 2, '2017-05-20', '2017-05-20', '2017-05-30', '2017-06-30', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(82, 25, 4, '2', '98488003', NULL, 4, 'MAGARI', '721', 3, 2, '2017-05-20', '2017-05-20', '2017-05-30', '2017-06-30', NULL, 19591.2, 0, 1, 1, 1, 1, 1),
(83, 25, 5, '1', '7LIMAG1194', 'SUDU 626452-4', 3, 'MAGARI', '721', 2, 2, '2017-05-21', '2017-05-21', '2017-06-03', '2017-06-03', 19610, 17414.4, 1, 1, 1, 1, 1, 1),
(84, 25, 3, '1', 'B6JBK024736', 'DFIU 722043-8', 9, 'JAMILA', '051', 4, 1, '2017-05-21', '2017-05-21', '2017-06-02', '2017-07-02', 17414.4, 17414.4, 1, 1, 1, 1, 1, 1),
(85, 25, 3, '2', 'B6JBK024735', 'DTPU 721041-5', 9, 'JAMILA', '051', 4, 1, '2017-05-21', '2017-05-21', '2017-06-01', '2017-07-01', 17414.4, 17414.4, 1, 1, 1, 1, 1, 1),
(86, 26, 1, '22383', '960593965', 'MSWU 100272-4', 5, 'MARINER', '1708', 1, 2, '2017-05-07', '2017-05-07', '2017-05-13', '2017-05-13', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(87, 26, 1, '22384', '960593965', 'MNBU 041511-9', 5, 'MARINER', '1708', 1, 2, '2017-05-07', '2017-05-07', '2017-05-13', '2017-05-13', 22080, 19591.2, 1, 1, 1, 1, 1, 1),
(88, 26, 1, '22385', '960593965', 'MNBU 018979-4', 5, 'MARINER', '1708', 1, 2, '2017-05-08', '2017-05-08', '2017-05-14', '2017-05-14', 22100, 19591.2, 1, 1, 1, 1, 1, 1),
(89, 26, 1, '22386', '960593965', 'TRIU 886892-8', 5, 'MARINER', '1708', 1, 2, '2017-05-08', '2017-05-08', '2017-05-15', '2017-05-15', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(90, 26, 1, '22387', '960593971', 'MMAU 108076-7', 5, 'MARINER', '1708', 7, 2, '2017-05-09', '2017-05-09', '2017-05-16', '2017-05-16', 21090, 18200, 1, 1, 1, 1, 1, 1),
(91, 26, 1, '22308', 'MBM140178145', NULL, 8, 'LOMBOK STRAIT', 'SR17012EB', 3, 2, '2017-05-09', '2017-05-09', '2017-05-16', '2017-06-16', NULL, NULL, 0, 1, 1, 1, 1, 1),
(92, 26, 1, '22378', 'MBM140178145', 'TCLU 138506-0', 8, 'LOMBOK STRAIT', 'SR17012EB', 3, 2, '2017-05-09', '2017-05-09', '2017-05-15', '2017-05-15', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(93, 26, 1, '22308', 'MBM140178145', 'CAIU 555044-4', 8, 'LOMBOK STRAIT', 'SR17012EB', 3, 2, '2017-05-09', '2017-05-09', '2017-05-15', '2017-05-15', 22150, 19591.2, 1, 1, 1, 1, 1, 1),
(94, 26, 1, '22379', 'MBM140178145', 'TEMU 921254-1', 8, 'LOMBOK STRAIT', 'SR17012EB', 3, 2, '2017-05-09', '2017-05-09', '2017-05-14', '2017-05-14', 22120, 19591.2, 1, 1, 1, 1, 1, 1),
(95, 26, 1, '22380', '7LIMES2508', 'SUDU 815048-4', 3, 'MAGARI', '720', 3, 2, '2017-05-10', '2017-05-10', '2017-05-06', '2017-05-06', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(96, 26, 1, '22382', '7LIMES2508', 'SUDU 603298-2', 3, 'MAGARI', '720', 3, 2, '2017-05-11', '2017-05-11', '2017-05-18', '2017-05-18', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(97, 26, 1, '22381', '7LIMES2508', 'SUDU 613373-5', 3, 'MAGARI', '720', 3, 2, '2017-05-10', '2017-05-10', '2017-05-17', '2017-05-17', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(98, 26, 2, '1', '98450104', 'CRLU 722620-1', 4, 'MAGARI', '720', 3, 1, '2017-05-11', '2017-05-11', '2017-05-19', '2017-05-19', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(99, 26, 2, '2', '98450104', 'HXLU 873973-0', 4, 'MAGARI', '720', 3, 1, '2017-05-11', '2017-05-11', '2017-05-18', '2017-05-18', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(100, 26, 4, '1', '98463454', 'HXLU 8725942-8', 4, 'MAGARI', '720', 3, 2, '2017-05-11', '2017-05-11', '2017-05-19', '2017-05-19', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(101, 26, 4, '2', '98463454', 'CPSU 515178-5', 4, 'MAGARI', '720', 3, 15, '2017-05-12', '2017-05-12', '2017-05-20', '2017-06-20', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(102, 26, 3, '1', 'B6JBK024710', 'DFIU 210486-2', 9, 'ALIOTH', '170', 4, 1, '2017-05-12', '2017-05-12', '2017-05-20', '2017-05-20', 19780, 17414.4, 1, 1, 1, 1, 1, 1),
(103, 26, 1, '2', 'B6JBK024709', 'DFIU 215285-5', 9, 'ALIOTH', '710', 4, 2, '2017-05-12', '2017-05-12', '2017-05-20', '2017-05-20', 19790, 17414.4, 1, 1, 1, 1, 1, 1),
(104, 26, 5, '1', '7LIMAG1117', 'SUDU 519096-2', 2, 'MAGARI', '720', 2, 1, '2017-05-13', '2017-05-13', '2017-05-21', '2017-05-21', 19620, 17414.4, 1, 1, 1, 1, 1, 1),
(105, 26, 5, '2', '7LIMAG1117', 'SUDU 608651-0', 3, 'MAGARI', '720', 2, 2, '2017-05-13', '2017-05-13', '2017-05-23', '2017-05-23', 19770, 17414.4, 1, 1, 1, 1, 1, 1),
(106, 26, 5, '3', '7LIMAG1117', 'SUDU 616035-0', 3, 'MAGARI', '721', 2, 2, '2017-05-13', '2017-05-13', '2017-05-14', '2017-05-14', 19700, 17414.4, 1, 1, 1, 1, 1, 1),
(107, 27, 1, '22312', '960508266', 'MWCU 531710-1', 5, 'SEALAND BALBOA', '1708', 1, 2, '2017-04-29', '2017-04-29', '2017-05-06', '2017-05-06', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(108, 27, 1, '22313', '960508266', 'MWCU 676726-9', 5, 'SEALAND BALBOA', '1708', 1, 2, '2017-04-29', '2017-05-29', '2017-05-06', '2017-05-06', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(109, 27, 1, '22314', '960508266', 'MNBU 315979-3', 5, 'SEALAND BALBOA', '1708', 1, 2, '2017-04-29', '2017-04-29', '2017-05-06', '2017-05-06', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(110, 27, 1, '22316', '960508294', 'MMAU 102717-1', 5, 'SEALAND BALBOA', '1708', 7, 2, '2017-04-30', '2017-04-30', '2017-05-07', '2017-05-07', 21130, 18200, 1, 1, 1, 1, 1, 1),
(111, 27, 1, '22315', '960508266', 'PONU 482405-6', 5, 'SEALAND BALBOA', '1708', 1, 2, '2017-04-30', '2017-04-30', '2017-05-07', '2017-05-23', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(112, 27, 1, '22315', '960508266', NULL, 5, 'SEALAND BALBOA', '1708', 1, 2, '2017-04-30', '2017-04-30', '2017-05-07', '2017-05-23', NULL, NULL, 0, 1, 1, 1, 1, 1),
(113, 27, 1, '22307', 'MBM140178142', 'CAIU 541972-7', 8, 'ATLANTIC KLIPPER', 'SR17011EB', 3, 2, '2017-04-30', '2017-04-30', '2017-05-07', '2017-05-07', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(114, 27, 1, '22309', '7LIMES2374', 'SUDU 620305-14', 3, 'MAGARI', '719', 3, 2, '2017-05-03', '2017-05-03', '2017-05-08', '2017-05-08', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(115, 27, 1, '22310', '7LIMES2374', 'SUDU 607630-0', 3, 'MAGARI', '719', 3, 2, '2017-05-03', '2017-05-03', '2017-05-08', '2017-05-24', 22140, 19591.2, 1, 1, 1, 1, 1, 1),
(116, 27, 1, '22355', 'MBM140178142', 'TEMU 936989-1', 8, 'ATLANTIC KLIPPER', 'SR17011EB', 3, 2, '2017-05-04', '2017-05-04', '2017-05-09', '2017-05-25', 22040, 19591.2, 1, 1, 1, 1, 1, 1),
(117, 27, 1, '22311', '7LIMES2374', 'SUDU 621605-9', 3, 'MAGARI', '719', 3, 2, '2017-05-04', '2017-05-04', '2017-05-09', '2017-05-29', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(118, 27, 1, '22317', '7LIMES2374', 'SUDU 527492-9', 3, 'MAGARI', '719', 3, 2, '2017-05-04', '2017-05-04', '2017-05-09', '2017-05-29', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(119, 27, 2, '1', '98443785', 'HLBU 900209-8', 4, 'MAGARI', '719', 3, 1, '2017-05-04', '2017-05-04', '2017-05-09', '2017-05-29', 22040, 19591.2, 1, 1, 1, 1, 1, 1),
(120, 27, 2, '2', '98450709', 'HLBU 902434-8', 4, 'MAGARI', '719', 3, 1, '2017-05-05', '2017-05-05', '2017-05-10', '2017-05-30', 22110, 19591.2, 1, 1, 1, 1, 1, 1),
(121, 27, 4, '1', '98436819', 'CPSU 511905-8', 4, 'MAGARI', '719', 3, 2, '2017-05-05', '2017-05-05', '2017-05-10', '2017-05-30', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(122, 27, 4, '2', '98436819', 'CPSU 511195-1', 4, 'MAGARI', '719', 3, 2, '2017-05-06', '2017-05-06', '2017-05-11', '2017-05-31', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(123, 27, 5, '1', '7LIMAG1025', 'SUDU 509979-1', 3, 'MAGARI', '719', 2, 2, '2017-05-06', '2017-05-06', '2017-05-11', '2017-05-31', 19670, 17414.4, 1, 1, 1, 1, 1, 1),
(124, 27, 3, '1', 'LMM0214243', 'CAIU 558886-1', 2, 'MAGARI', '013', 6, 1, '2017-05-06', '2017-05-06', '2017-05-11', '2017-05-31', 22110, 19591.2, 1, 1, 1, 1, 1, 1),
(125, 27, 3, '2', 'LMM0214243', 'CAIU 553414-5', 3, 'MAGARI', '013', 6, 1, '2017-05-06', '2017-05-06', '2017-05-11', '2017-05-31', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(126, 27, 5, '2', '7LIMAG1026', 'SUDU 522861-0', 3, 'MAGARI', '719', 2, 2, '2017-05-07', '2017-05-07', '2017-05-12', '2017-05-20', 19610, 17414.4, 1, 1, 1, 1, 1, 1),
(127, 28, 1, '22182', '960375921', 'MSWU 102450-7', 5, 'SEALAND LOS ANGELES', '1708', 1, 2, '2017-04-23', '2017-04-23', '2017-04-28', '2017-04-28', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(128, 28, 1, '22185', '960375921', 'MNBU 039856-2', 5, 'SEALAND LOS ANGELES', '1708', 1, 2, '2017-04-23', '2017-04-23', '2017-04-28', '2017-04-28', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(129, 28, 1, '22186', '960375921', 'TRIU 886482-0', 5, 'SEALAND LOS ANGELES', '1708', 1, 2, '2017-04-23', '2017-04-23', '2017-04-28', '2017-04-28', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(130, 28, 1, '22187', '960375916', 'MMAU 102304-7', 5, 'SEALAND LOS ANGELES', '1708', 7, 2, '2017-04-24', '2017-04-24', '2017-04-28', '2017-04-28', 21460, 18200, 1, 1, 1, 1, 1, 1),
(131, 28, 1, '22175', 'MBM140178139', 'CXRU 111249-0', 8, 'BALTIC KLIPPER', '17010', 3, 2, '2017-04-24', '2017-04-24', '2017-04-29', '2017-04-29', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(132, 28, 1, '22176', 'MBM140178139', 'TEMU 921320-8', 8, 'BALTIC KLIPPER', 'SR17010EB', 3, 2, '2017-04-24', '2017-04-24', '2017-04-29', '2017-04-29', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(133, 28, 1, '22177', 'MBM140178139', 'TTNU 895755-3', 8, 'BALTIC KLIPPER', 'SR17010EB', 3, 2, '2017-04-24', '2017-04-24', '2017-04-29', '2017-04-29', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(134, 28, 1, '22178', '7LIMES2192', 'SUDU 617838-6', 3, 'MAGARI', '718', 3, 2, '2017-04-25', '2017-05-25', '2017-05-30', '2017-05-30', 22420, 19591.2, 1, 1, 1, 1, 1, 1),
(135, 28, 1, '22179', '7LIMES2192', 'SUDU 513868-7', 3, 'MAGARI', '718', 3, 2, '2017-04-25', '2017-05-25', '2017-04-30', '2017-04-30', 22350, 19591.2, 1, 1, 1, 1, 1, 1),
(136, 28, 1, '22180', '7LIMES2192', 'SUDU 618302-1', 3, 'MAGARI', '718', 3, 2, '2017-04-26', '2017-04-26', '2017-05-01', '2017-05-01', 22300, 19591.2, 1, 1, 1, 1, 1, 1),
(137, 28, 1, '22181', '7LIMES2192', 'SUDU 629540-1', 3, 'MAGARI', '718', 3, 2, '2017-04-26', '2017-04-26', '2017-05-01', '2017-05-01', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(138, 28, 2, '1', '983909905', NULL, 4, 'MAGARI', '718', 3, 1, '2017-04-26', '2017-04-26', '2017-05-01', '2017-05-20', NULL, NULL, 0, 1, 1, 1, 1, 1),
(139, 28, 2, '1', '983909905', 'LNXU 755037-5', 4, 'MAGARI', '718', 3, 1, '2017-04-26', '2017-04-26', '2017-05-01', '2017-05-01', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(140, 28, 2, '2', '9839095', 'HLXU 875724-5', 4, 'MAGARI', '718', 3, 1, '2017-04-26', '2017-04-26', '2017-05-01', '2017-02-01', 22400, 19591.2, 1, 1, 1, 1, 1, 1),
(141, 28, 4, '1', '98390933', 'FSCU 565933-0', 4, 'MAGARI', '718', 3, 2, '2017-04-27', '2017-04-27', '2017-05-02', '2017-05-02', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(142, 28, 4, '2', '98390933', 'CRLU 7270950', 4, 'MAGARI', '718', 3, 2, '2017-04-27', '2017-04-27', '2017-05-02', '1920-02-02', 21920, 19591.2, 1, 1, 1, 1, 1, 1),
(143, 28, 3, '1', 'LMM0213991', 'LMM0213991', 2, 'MAGARI', '012', 6, 1, '2017-04-27', '2017-04-27', '2017-05-02', '2017-05-22', 22130, 19591.2, 1, 1, 1, 1, 1, 1),
(144, 28, 5, '1', '7LIMAG0937', 'SUDU 620537-3', 3, 'MAGARI', '718', 2, 2, '2017-04-27', '2017-04-27', '2017-05-02', '2017-05-02', 19650, 17414.4, 1, 1, 1, 1, 1, 1),
(145, 28, 3, '2', 'LMM0213991', 'IKSU 400955-4', 2, 'MAGARI', '012', 6, 1, '2017-04-28', '2017-04-28', '2017-05-03', '2017-05-03', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(146, 28, 5, '2', '7LIMAG0967', 'SUDU 816408-3', 3, 'MAGARI', '718', 2, 2, '2017-04-28', '2017-04-28', '2017-05-03', '2017-05-23', 19630, 17414.4, 1, 1, 1, 1, 1, 1),
(147, 28, 5, '3', '7LIMAG0938', 'SUDU 816408-4', 3, 'MAGARI', '718', 2, 2, '2017-04-28', '2017-04-28', '2017-05-03', '2017-05-03', 19630, 17414.4, 1, 1, 1, 1, 1, 1),
(148, 29, 1, '22079', '960279460', 'MWCU  530467-6', 5, 'SEALAND GUAYAQUIL', '1708', 1, 2, '2017-04-17', '2017-04-17', '2017-04-21', '2017-04-21', 22360, 19591.2, 1, 1, 1, 1, 1, 1),
(149, 29, 1, '22080', '960279460', 'MNBU 039353-4', 5, 'SEALAND GUAYAQUIL', '1708', 1, 2, '2017-04-18', '2017-04-18', '2017-04-22', '2017-04-22', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(150, 29, 1, '22081', '960279493', 'MMAU 105387-0', 5, 'SEALAND GUAYAQUIL', '1708', 7, 2, '2017-04-18', '2017-04-18', '2017-04-23', '2017-04-23', 21470, 18200, 1, 1, 1, 1, 1, 1),
(151, 29, 1, '22089', '960322352', 'MNBU 040856-8', 5, 'SEALAND GUAYAQUIL', '1708', 3, 2, '2017-04-18', '2017-04-18', '2017-04-23', '2017-04-23', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(152, 29, 1, '22082', 'MBM140093082', 'CAIU 552887-8', 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-04-18', '2017-04-18', '2017-04-23', '2017-04-23', 22340, 19591.2, 1, 1, 1, 1, 1, 1),
(153, 29, 1, '22083', 'MBM140178138', 'IRNU 751068-0', 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-04-19', '2017-04-19', '2017-04-24', '2017-04-24', 22460, 19591.2, 1, 1, 1, 1, 1, 1),
(154, 29, 1, '22084', 'MBM140178138', 'TCLU 113928-3', 8, 'NEDERLAND REEFER', '17009', 3, 2, '2017-04-19', '2017-04-19', '2017-04-24', '2017-04-24', 22420, 19591.2, 1, 1, 1, 1, 1, 1),
(155, 29, 1, '22085', '7LIMES2056', 'SUDU 522746-5', 3, 'MAGARI', '717', 3, 2, '2017-04-19', '2017-04-19', '2017-04-24', '2017-04-24', 22360, 19591.2, 1, 1, 1, 1, 1, 1),
(156, 29, 1, '22086', '7LIMES2056', 'SUDU  900272-3', 3, 'MAGARI', '717', 3, 2, '2017-04-19', '2017-04-19', '2017-04-24', '2017-04-24', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(157, 29, 1, '22088', '7LIMES2056', 'SUDU 616219-0', 3, 'MAGARI', '717', 3, 2, '2017-04-20', '2017-04-20', '2017-04-25', '2017-04-25', 22390, 19591.2, 1, 1, 1, 1, 1, 1),
(158, 29, 1, '22087', '7LIMES2056', 'SUDU 607649-2', 3, 'MAGARI', '717', 3, 2, '2017-04-20', '2017-04-20', '2017-04-25', '2017-04-25', 22450, 19591.2, 1, 1, 1, 1, 1, 1),
(159, 29, 2, '1', '98376171', 'CPSU 517118-5', 4, 'MAGARI', '717', 3, 1, '2017-04-20', '2017-04-20', '2017-04-25', '2017-04-25', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(160, 29, 2, '2', '98376171', 'CPSU 516485-9', 4, 'MAGARI', '717', 3, 1, '2017-04-20', '2017-04-20', '2017-05-25', '2017-05-25', 22140, 19591.2, 1, 1, 1, 1, 1, 1),
(161, 29, 2, '3', '98376171', 'LNXU 845797-1', 5, 'MAGARI', '717', 3, 1, '2017-04-21', '2017-04-21', '2017-04-26', '2017-04-26', 22130, 19591.2, 1, 1, 1, 1, 1, 1),
(162, 29, 4, '1', '98376965', 'LNXU 965912-8', 4, 'MAGARI', '717', 3, 2, '2017-04-21', '2017-04-21', '2017-04-26', '2017-04-26', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(163, 29, 4, '2', '98376965', 'HLBU 903450-0', 4, 'MAGARI', '717', 3, 2, '2017-04-21', '2017-04-21', '2017-04-26', '2017-04-26', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(164, 29, 5, '1', '7LIMAG0875', 'SUDU 620449-0', 3, 'MAGARI', '717', 2, 2, '2017-04-21', '2017-04-21', '2017-04-27', '2017-04-27', 19660, 17414.4, 1, 1, 1, 1, 1, 1),
(165, 29, 5, '2', '7LIMAG0875', 'SUDU 621297-9', 3, 'MAGARI', '717', 2, 2, '2017-04-22', '2017-04-22', '2016-04-27', '2017-04-27', 19670, 17414.4, 1, 1, 1, 1, 1, 1),
(166, 29, 3, '1', 'LMM0213636', 'TLLU 106936-9', 2, 'MAGARI', '011', 24, 2, '2017-04-22', '2017-04-22', '2017-04-27', '2017-05-17', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(167, 29, 3, '2', 'LMM0213636', 'TLLU 107086-3', 2, 'MAGARI', '011', 24, 2, '2017-04-22', '2017-04-22', '2017-04-27', '2017-04-27', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(168, 30, 1, '21943', '960206947', 'MSWU 908240-7', 5, 'SEALAND MANZANILLO', '1706', 1, 2, '2017-04-10', '2017-04-10', '2017-04-15', '2017-04-15', 22430, 19591.2, 1, 1, 1, 1, 1, 1),
(169, 30, 1, '21944', '960206947', 'MNBU 309103-4', 5, 'SEALAND MANZANILLO', '1706', 1, 2, '2017-04-10', '2017-04-10', '2017-04-15', '2017-04-15', 22420, 19591.2, 1, 1, 1, 1, 1, 1),
(170, 30, 1, '21945', '960206956', 'MMAU 114340-1', 5, 'SEALAND MANZANILLO', '1706', 7, 2, '2017-04-10', '2017-04-10', '2017-04-15', '2017-04-15', 21290, 18200, 1, 1, 1, 1, 1, 1),
(171, 30, 1, '21935', '087LIM229775', 'TEMU 952754-9', 7, 'SEALAND MANZANILLO', '1706', 3, 2, '2017-04-11', '2017-04-11', '2017-04-16', '2017-04-16', 22150, 19591.2, 1, 1, 1, 1, 1, 1),
(172, 30, 1, '21936', '087LIM229775', 'TEMU 940125-2', 7, 'SEALAND MANZANILLO', '1706', 3, 2, '2017-04-11', '2017-04-11', '2017-04-16', '2017-04-16', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(173, 30, 1, '21937', '7LIMES1944', 'SUDU 519611-1', 3, 'MAGARI', '716', 3, 2, '2017-04-11', '2017-04-11', '2017-04-16', '2017-04-16', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(174, 30, 1, '21938', '7LIMES1944', NULL, 3, 'MAGARI', '716', 3, 2, '2017-04-11', '2017-04-11', '2017-04-16', '2017-05-01', NULL, NULL, 0, 1, 1, 1, 1, 1),
(175, 30, 1, '21938', '7LIMES1944', 'SUDU 823262-9', 3, 'MAGARI', '716', 3, 2, '2017-04-11', '2017-04-11', '2017-04-16', '2017-04-16', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(176, 30, 1, '21939', 'MBM140093078', 'TTNU 896407-0', 8, 'NORDSERENA', '17006', 3, 2, '2017-04-12', '2017-04-12', '2017-04-17', '2017-04-17', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(177, 30, 1, '21940', 'MBM140093078', 'TTNU 895700-2', 8, 'NORDSERENA', '17006', 3, 2, '2017-04-12', '2017-04-12', '2017-04-17', '2016-04-17', 22310, 19591.2, 1, 1, 1, 1, 1, 1),
(178, 30, 1, '21941', '7LIMES1944', 'SUDU 519042-7', 3, 'MAGARI', '716', 3, 2, '2017-04-12', '2017-04-12', '2017-04-17', '2017-04-17', 2370, 19591.2, 1, 1, 1, 1, 1, 1),
(179, 30, 1, '21822', '7LIMES1944', 'SUDU 616311-2', 3, 'MAGARI', '716', 3, 2, '2017-04-15', '2017-04-15', '2017-04-20', '2017-05-10', NULL, 19591.2, 1, 1, 1, 1, 1, 1),
(180, 30, 2, '1', 'LMM0213386', NULL, 2, 'MAGARI', '716', 6, 1, '2017-04-15', '2017-04-15', '2017-04-20', '2017-05-10', NULL, NULL, 0, 1, 1, 1, 1, 1),
(181, 30, 2, '1', '98334557', 'CRLU 129625-9', 4, 'MAGARI', '716', 3, 1, '2017-04-15', '2017-04-15', '2017-04-20', '2017-04-20', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(182, 30, 2, '1', '98334557', NULL, 4, 'MAGARI', '716', 3, 1, '2017-04-15', '2017-04-15', '2017-04-20', '2017-05-10', NULL, 19591.2, 0, 1, 1, 1, 1, 1),
(183, 30, 3, '1', 'LMM0213386', 'CGMU 514892-4', 2, 'MAGARI', '716', 6, 1, '2017-04-15', '2017-04-15', '2017-04-20', '2017-04-20', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(184, 30, 3, '2', 'B6JBK024528', 'BMOU 982642-2', 2, 'ALIOTH', '168', 6, 1, '2017-04-16', '2017-04-16', '2017-04-20', '2017-04-20', 21950, 19591.2, 1, 1, 1, 1, 1, 1),
(185, 30, 4, '1', '98340958', 'HLXU 873258-7', 4, 'MAGARI', '716', 3, 2, '2017-04-16', '2017-04-16', '2017-04-21', '2017-04-21', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(186, 30, 4, '2', '98340958', 'HLBU 901498-8', 4, 'MAGARI', '716', 3, 2, '2017-04-16', '2017-04-16', '2017-04-21', '2017-04-21', 22130, 19591.2, 1, 1, 1, 1, 1, 1),
(187, 30, 5, '1', '7LIMAG0870', 'TEMU 905696-3', 3, 'MAGARI', '716', 2, 2, '2017-04-16', '2017-04-16', '2017-04-21', '2017-04-21', 19700, 17414.4, 1, 1, 1, 1, 1, 1),
(188, 30, 3, '3', 'B6JBK024543', 'DFIU 812402-2', 9, 'ALIOTH', '168', 5, 1, '2017-04-17', '2017-04-17', '2017-04-22', '2017-04-22', 19850, 17414.4, 1, 1, 1, 1, 1, 1),
(189, 30, 3, '3', 'B6JBK024543', NULL, 9, 'ALIOTH', '168', 5, 1, '2017-04-17', '2017-04-17', '2017-04-22', '2017-05-14', NULL, NULL, 0, 1, 1, 1, 1, 1),
(190, 31, 1, '21823', '960124055', 'NSWU 906402-3', 5, 'SEALAND PHILADELPHIA', '1706', 1, 2, '2017-04-04', '2017-04-04', '2017-06-09', '2017-06-09', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(191, 31, 1, '21824', '960124055', 'TRIU 803714-3', 5, 'SEALAND PHILADELPHIA', '1706', 1, 2, '2017-04-04', '2017-04-04', '2017-06-09', '2017-04-09', 22430, 19591.2, 1, 1, 1, 1, 1, 1),
(192, 31, 1, '21740', '960031000', 'MMAU 100216-8', 5, 'SEALAND PHILADELPHIA', '1706', 7, 2, '2017-04-04', '2017-04-04', '2017-04-09', '2017-04-09', 21310, 18200, 1, 1, 1, 1, 1, 1),
(193, 31, 1, '21815', 'MBM140160107', 'CXRU 162047-0', 8, 'SEATRADE RED', '170017', 3, 2, '2017-04-05', '2017-04-05', '2017-04-09', '2017-04-09', 22640, 19591.2, 1, 1, 1, 1, 1, 1),
(194, 31, 1, '21816', 'MBM140160107', 'CAIU 552897-0', 8, 'SEATRADE RED', '17005', 3, 2, '2017-04-05', '2017-04-05', '2017-04-10', '2017-04-10', 22450, 19591.2, 1, 1, 1, 1, 1, 1),
(195, 31, 1, '21817', '7LIMES1771', 'SUDU 626354-9', 3, 'MAGARI', '715', 3, 2, '2017-04-05', '2017-04-05', '2017-04-10', '2017-04-10', 22360, 19591.2, 1, 1, 1, 1, 1, 1),
(196, 31, 1, '21818', '7LIMES1771', 'SUDU 624794-9', 3, 'MAGARI', '715', 3, 2, '2017-04-06', '2017-04-06', '2017-04-10', '2017-04-10', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(197, 31, 1, '21819', '7LIMES1771', 'SUDU 820917-2', 3, 'MAGARI', '715', 3, 2, '2017-04-06', '2017-04-06', '2017-04-11', '2017-04-11', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(198, 31, 1, '21820', '7LIMES1771', 'SUDU 607748-3', 3, 'MAGARI', '715', 3, 2, '2017-04-06', '2017-04-06', '2017-04-11', '2017-04-11', 22310, 19591.2, 1, 1, 1, 1, 1, 1),
(199, 31, 1, '21821', '7LIMES1771', 'SUDU 623521-2', 3, 'MAGARI', '715', 3, 2, '2017-04-07', '2017-04-07', '2017-04-12', '2017-04-12', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(200, 31, 1, '21822', '7LIMES1771', 'SUDU 811312-6', 3, 'MAGARI', '715', 3, 2, '2017-04-07', '2017-04-07', '2017-04-12', '2017-04-12', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(201, 31, 2, '1', '98303394', 'HLBU 900241-5', 4, 'MAGARI', '715', 3, 1, '2017-04-07', '2017-04-07', '2017-04-12', '2017-04-12', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(202, 31, 2, '2', '98303394', 'HXLU 876200-4', 4, 'MAGARI', '715', 3, 1, '2017-04-07', '2017-04-07', '2017-04-12', '2017-04-12', 11165, 19591.2, 1, 1, 1, 1, 1, 1),
(203, 31, 4, '1', '98310300', 'TRLU 166726-9', 4, 'MAGARI', '715', 3, 2, '2017-04-09', '2017-04-09', '2017-04-14', '2017-04-14', 22380, 19591.2, 1, 1, 1, 1, 1, 1),
(204, 31, 4, '2', '98310300', 'CPSU 511506-8', 4, 'MAGARI', '715', 3, 2, '2017-04-09', '2017-04-09', '2017-04-14', '2017-04-14', 22490, 19591.2, 1, 1, 1, 1, 1, 1),
(205, 31, 3, '1', 'LMM0213101', 'CGMU 929863-0', 2, 'MAGARI', '715', 6, 1, '2017-04-09', '2017-04-09', '2017-04-14', '2017-04-14', 22350, 19591.2, 1, 1, 1, 1, 1, 1),
(206, 31, 3, '2', 'B6JBK024502', 'DFIU 426376-0', 2, 'JAMILA', '048', 6, 1, '2017-04-09', '2017-04-09', '2017-04-14', '2017-04-14', 19540, 17414.4, 1, 1, 1, 1, 1, 1),
(207, 31, 5, '1', '27803830725A', 'CXRU 113270-6', 6, 'JAMILA', '00048', 3, 2, '2017-04-09', '2017-04-09', '2017-04-14', '2017-04-14', 19780, 17414.4, 1, 1, 1, 1, 1, 1),
(208, 32, 1, '21735', '960030669', 'MNBU 039996-0', 5, 'MARINER', '1706', 1, 2, '2017-03-28', '2017-03-28', '2017-05-03', '2017-05-30', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(209, 32, 1, '21257', 'MBM140178133', 'CXRU 11095-6', 8, 'LOMBOK STRAIT', '17006', 3, 2, '2017-03-28', '2017-03-28', '2017-04-03', '2017-04-03', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(210, 32, 1, '21258', 'MBM140178133', 'TLLU 104334-3', 8, 'LOMBOK STRAIT', '17006', 3, 2, '2017-03-29', '2017-03-29', '2017-04-04', '2017-04-04', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(211, 32, 1, '21736', '960030669', 'TCLU 138407-0', 8, 'LOMBOK STRAIT', '17006', 1, 2, '2017-03-30', '2017-03-30', '2017-04-05', '2017-04-05', 22130, 19591.2, 1, 1, 1, 1, 1, 1),
(212, 32, 1, '21738', '960030669', 'BMOU 920417-8', 4, 'MAGARI', '714', 1, 2, '2017-04-01', '2017-04-01', '2017-04-05', '2017-04-05', 22150, 19591.2, 1, 1, 1, 1, 1, 1),
(213, 32, 1, '21737', '960030669', 'CAIU 557990-0', 8, 'LOMBOK STRAIT', '17006', 3, 2, '2017-03-31', '2017-03-31', '2017-04-05', '2017-04-05', 22150, 19591.2, 1, 1, 1, 1, 1, 1),
(214, 32, 2, '1', '98265992', 'HLXU 876491-7', 4, 'MAGARI', '714', 3, 1, '2017-04-01', '2017-04-01', '2017-04-06', '2017-04-06', 22120, 19591.2, 1, 1, 1, 1, 1, 1),
(215, 32, 2, '1', '98265992', 'HLBU 908649-0', 3, 'MAGARI', '714', 3, 1, '2017-04-01', '2017-04-01', '2017-04-06', '2017-04-06', 11060, 19591.2, 1, 1, 1, 1, 1, 1),
(216, 32, 1, '21731', '7LIMES1669', 'SUDU 625261-0', 3, 'MAGARI', '714', 3, 2, '2017-03-29', '2017-03-29', '2017-04-04', '2017-04-04', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(217, 32, 1, '21732', '7LIMES1669', 'SUDU 601706-2', 3, 'MAGARI', '714', 3, 2, '2017-03-29', '2017-03-29', '2017-04-05', '2017-04-05', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(218, 32, 1, '21733', '7LIMES1669', 'SUDU 803838-3', 3, 'MAGARI', '714', 3, 2, '2017-04-01', '2017-04-01', '2017-04-04', '2017-04-05', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(219, 32, 4, '1', '98290989', 'CRLU 724442-1', 4, 'MAGARI', '714', 3, 2, '2017-04-01', '2017-04-01', '2017-04-07', '2017-04-07', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(220, 32, 4, '2', '98290989', 'BMOU 921363-1', 4, 'MAGARI', '714', 3, 2, '2017-04-01', '2017-04-01', '2017-04-06', '2017-04-06', 22130, 19591.2, 1, 1, 1, 1, 1, 1),
(221, 32, 5, '1', '27803823109A', 'MORU 070911-7', 6, 'ALIOTH', '167', 2, 2, '2017-04-03', '2017-04-03', '2017-04-08', '2017-05-08', 19680, 17414.4, 1, 1, 1, 1, 1, 1),
(222, 32, 5, '2', '98303756', 'BMOU 975429-8', 6, 'ALIOTH', '167', 2, 2, '2017-04-03', '2017-04-03', '2017-04-08', '2017-04-08', 19720, 17414.4, 1, 1, 1, 1, 1, 1),
(223, 32, 3, '1', 'B6JBK024445', 'BMOU 982800-3', 2, 'ALIOTH', '167', 6, 1, '2017-04-02', '2017-04-02', '2017-04-06', '2017-04-06', 22150, 19591.2, 1, 1, 1, 1, 1, 1),
(224, 32, 3, '2', 'B6JBK024445', 'CRXU 161091-3', 2, 'ALIOTH', '167', 6, 2, '2017-04-02', '2017-04-02', '2017-04-07', '2017-04-07', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(225, 32, 3, '3', 'B6JBK024459', 'DFIU 812151-1', 9, 'ALIOTH', '167', 22, 1, '2017-04-03', '2017-04-03', '2017-04-07', '2017-04-07', 19860, 17414.4, 1, 1, 1, 1, 1, 1),
(226, 33, 1, '21606', '959974970', 'MWCU 691646-5', 5, 'SEALAND BALBOA', '1706', 1, 2, '2017-03-20', '2017-03-20', '2017-03-24', '2017-03-24', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(228, 33, 1, '21475', '959974970', 'MSWU 907745-8', 5, 'SEALAND BALBOA', '1706', 1, 2, '2017-03-20', '2017-03-20', '2017-03-25', '2017-03-25', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(229, 33, 1, '21676', '959976568', 'MSWU 900139-1', 5, 'SEALAND BALBOA', '1706', 7, 2, '2017-03-21', '2017-03-21', '2017-03-26', '2017-03-26', 21080, 18200, 1, 1, 1, 1, 1, 1),
(230, 33, 1, '21474', '959974970', 'MWCU 670039-4', 5, 'SEALAND BALBOA', '1706', 1, 2, '2017-03-20', '2017-03-20', '2017-03-25', '2017-03-25', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(231, 33, 1, '21598', 'MBM160065012', 'CAIU 557707-0', 8, 'ATLANTIC KLIPPER', '17005', 1, 2, '2017-03-21', '2017-03-21', '2017-03-26', '2017-03-26', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(232, 33, 1, '21257', 'MBM140178130', 'TEMU 921294-2', 8, 'ATLANTIC KLIPPER', '17005', 3, 2, '2017-03-21', '2017-03-21', '2017-03-26', '2017-03-26', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(233, 33, 1, '21258', 'MBM140178130', 'CXRU 16199-7', 8, 'ATLANTIC KLIPPER', '17005', 3, 2, '2017-03-23', '2017-03-23', '2017-03-26', '2017-03-26', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(234, 33, 1, '21601', '7LIMES1578', 'SUDU 520520-8', 3, 'MAGARI', '713', 3, 2, '2017-03-23', '2017-03-23', '2017-03-30', '2017-03-30', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(235, 33, 1, '21603', '7LIMES1578', 'SUDU 607032-3', 3, 'MAGARI', '713', 3, 2, '2017-03-23', '2017-03-23', '2017-03-29', '2017-03-30', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(236, 33, 1, '21604', '7LIMES1578', 'SUDU 603038-3', 3, 'MAGARI', '713', 3, 2, '2017-03-24', '2017-03-24', '2017-03-29', '2017-03-29', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(237, 33, 1, '21605', '7LIMES1578', 'SUDU 606248-3', 3, 'MAGARI', '713', 3, 2, '2017-03-24', '2017-03-24', '2017-03-30', '2017-03-30', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(238, 33, 2, '1', '98231039', 'CPSU 512140-9', 4, 'MAGARI', '713', 3, 1, '2017-03-24', '2017-03-24', '2017-03-30', '2017-03-30', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(239, 33, 2, '2', '98231039', 'BMOU 920790-0', 4, 'MAGARI', '713', 3, 1, '2017-03-25', '2017-03-25', '2017-03-31', '2017-03-30', 22070, 19591.2, 1, 1, 1, 1, 1, 1),
(240, 33, 4, '1', '98252089', 'LNXU 755914-0', 4, 'MAGARI', '713', 3, 2, '2017-03-25', '2017-03-25', '2017-03-30', '2017-03-30', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(241, 33, 4, '2', '087LIM228826', 'CXRU 116665-0', 7, 'MAGARI', '713', 3, 2, '2017-03-28', '2017-03-28', '2017-04-01', '2017-04-01', 22080, 19591.2, 1, 1, 1, 1, 1, 1),
(242, 33, 3, '3', 'B6JBK024411', 'DFIU 720139-8', 9, 'JAMILA', '047', 4, 1, '2017-03-27', '2017-03-27', '2017-04-01', '2017-04-02', 19800, 17414.4, 1, 1, 1, 1, 1, 1),
(243, 33, 3, '2', 'B6JBK024394', 'DTPU 429144-0', 9, 'JAMILA', '047', 22, 1, '2017-03-27', '2017-03-27', '2017-04-01', '2017-04-01', NULL, 17414.4, 1, 1, 1, 1, 1, 1),
(244, 33, 5, '2', '27803820368A', 'SEGU 9392940', 6, 'JAMILA', '00047', 2, 2, '2017-03-27', '2017-03-27', '2017-04-01', '2017-04-01', 19620, 17414.4, 1, 1, 1, 1, 1, 1),
(245, 33, 5, '1', '27803820352A', 'MORU 590143-6', 6, 'JAMILA', '00047', 2, 2, '2017-03-27', '2017-03-27', '2017-04-01', '2017-04-01', 19580, 17414.4, 1, 1, 1, 1, 1, 1),
(246, 34, 1, '21473', '959853379', 'MWCU 524508-5', 5, 'LOS  ANGELES', '712', 1, 2, '2017-03-14', '2017-03-14', '2017-03-17', '2017-04-19', 22150, 19591.2, 1, 1, 1, 1, 1, 1),
(247, 34, 2, '1', '98201974', 'CPSU 510279-6', 4, 'MAGARI', '712', 2, 1, '2017-03-15', '2017-03-15', '2017-03-19', '2017-03-19', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(248, 34, 2, '2', '98201974', 'HLXU 879182-5', 4, 'MAGARI', '712', 2, 1, '2017-03-14', '2017-03-14', '2017-03-19', '2017-03-19', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(249, 34, 2, '3', '98201974', 'CPSU 510299-1', 4, 'MAGARI', '702', 3, 1, '2017-03-15', '2017-03-15', '2017-03-20', '2017-03-20', 22310, 19591.2, 1, 1, 1, 1, 1, 1),
(250, 34, 2, '4', '7LIMES1569', 'CRSU 613956-1', 3, 'MAGARI', '712', 3, 1, '2017-03-15', '2017-03-15', '2017-03-20', '2017-03-20', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(251, 34, 1, '21466', 'MBM160065011', 'MBM 160065011', 8, 'BALTIC KLIPPER', '712', 1, 2, '2017-03-16', '2017-03-16', '2017-03-20', '2017-04-20', 22455, 19591.2, 1, 1, 1, 1, 1, 1),
(252, 34, 1, '21465', 'MBM160065011', 'SZLU 980398-4', 8, 'BALTIC KLIPPER', '712', 1, 2, '2017-03-16', '2017-03-16', '2017-03-19', '2017-03-20', 22390, 19591.2, 1, 1, 1, 1, 1, 1),
(253, 34, 1, '21472', '7LIMES1569', 'SUDU 817546-8', 3, 'MAGARI', '712', 3, 2, '2017-03-16', '2017-03-16', '2017-03-21', '2017-03-21', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(254, 34, 3, '1', 'LMM021215', 'TRIU 868746-8', 2, 'MAGARI', '006', 3, 1, '2017-03-18', '2017-03-18', '2017-03-23', '2017-03-23', 22680, 19591.2, 1, 1, 1, 1, 1, 1),
(255, 34, 1, '21471', '7LIMES1569', 'SUDU 627315-1', 3, 'MAGARI', '712', 3, 2, '2017-03-18', '2017-03-18', '2017-03-21', '2017-03-21', 22150, 19591.2, 1, 1, 1, 1, 1, 1),
(256, 34, 1, '21469', '7LIMES1569', 'SUDU 602313-1', 3, 'MAGARI', '712', 3, 2, '2017-03-17', '2017-03-17', '2017-03-21', '2017-03-21', 22370, 19591.2, 1, 1, 1, 1, 1, 1),
(257, 34, 1, '21468', '7LIMES1569', 'SUDU 600697-8', 3, 'MAGARI', '712', 3, 2, '2017-03-17', '2017-03-17', '2017-03-21', '2017-03-21', 22370, 19591.2, 1, 1, 1, 1, 1, 1),
(258, 34, 4, '1', '98219408', 'HLXU 676567-9', 4, 'MAGARI', '712', 3, 2, '2017-03-19', '2017-03-19', '2017-03-23', '2017-03-23', 22110, 19591.2, 1, 1, 1, 1, 1, 1),
(259, 34, 4, '2', '98219408', 'CPSU 514390-1', 4, 'MAGARI', '702', 3, 2, '2017-03-20', '2017-03-20', '2017-03-23', '2017-03-23', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(260, 34, 5, '2', '27803805527A', 'TCLU 124473--5', 6, 'ALIOTH', '00166', 2, 2, '2017-03-20', '2017-03-20', '2017-03-25', '2017-03-25', 19600, 17414.4, 1, 1, 1, 1, 1, 1),
(261, 34, 5, '3', '27803812640A', 'MORU 11618-9', 6, 'ALIOTH', '00166', 2, 2, '2017-03-20', '2017-03-20', '2017-03-22', '2017-03-22', NULL, 17414.4, 1, 1, 1, 1, 1, 1),
(262, 34, 5, '1', '27803805532A', 'TCLU 124783-7', 6, 'ALIOTH', '00166', 2, 2, '2017-03-20', '2017-03-20', '2017-03-26', '2017-03-26', 19630, 17414.4, 1, 1, 1, 1, 1, 1),
(263, 35, 1, '21354', '572123443', 'MECU 670019-9', 5, 'SEALAND GUAYAQUIL', '1706', 1, 2, '2017-03-07', '2017-03-07', '2017-03-10', '2017-03-10', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(264, 35, 1, '21356', '572123443', 'MNBU 335774-1', 5, 'SEALAND GUAYAQUIL', '1706', 1, 2, '2017-03-07', '2017-03-07', '2017-03-12', '2017-04-07', 22080, 19591.2, 1, 1, 1, 1, 1, 1),
(265, 35, 1, '21355', '572123443', 'PONU 487963-4', 5, 'SEALAND GUAYAQUIL', '1706', 1, 2, '2017-03-07', '2017-03-07', '2017-03-10', '2017-03-10', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(266, 35, 1, '21347', 'MBM160065010', 'TEMU 937082-4', 8, 'NEDERLAND REEFER', '17003', 1, 2, '2017-03-08', '2017-03-08', '2017-03-13', '2017-03-13', 22110, 19591.2, 1, 1, 1, 1, 1, 1),
(267, 35, 1, '21349', 'MBM160065010', 'TCLU 138380-7', 8, 'NEDERLAND REEFER', '17003', 1, 2, '2017-03-08', '2017-03-08', '2017-03-13', '2017-03-13', 22140, 19591.2, 1, 1, 1, 1, 1, 1),
(268, 35, 1, '21348', 'MBM160065010', 'ATKU 411084-4', 8, 'NEDERLAND REEFER', '17003', 1, 2, '2017-03-08', '2017-03-08', '2017-03-13', '2017-03-13', 2220, 19591.2, 1, 1, 1, 1, 1, 1),
(269, 35, 1, '21351', '087LIM227108', 'MSCU 732041-9', 7, 'ARUSHIV', 'NQ701R', 3, 2, '2017-03-09', '2017-03-09', '2017-03-14', '2017-03-14', 22265, 19591.2, 1, 1, 1, 1, 1, 1),
(270, 35, 1, '21352', '087LIM227108', 'TTNU 806981-9', 7, 'ARUSHIV', 'NQ701R', 3, 2, '2017-03-09', '2017-03-09', '2017-03-14', '2017-03-14', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(271, 35, 1, '21353', '087LIM227108', 'TRLU 164636-9', 7, 'ARUSHIV', 'NQ701R', 3, 2, '2017-03-09', '2017-03-09', '2017-03-14', '2017-03-14', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(272, 35, 1, '21350', '087LIM227108', 'CXRU 147615-2', 7, 'ARUSHIV', 'NQ701R', 3, 2, '2017-03-08', '2017-03-08', '2017-03-14', '2017-03-14', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(273, 35, 1, '21357', '087LIM227108', 'TRIU 815100-6', 7, 'ARUSHIV.', 'NQ701R', 3, 2, '2017-03-10', '2017-03-10', '2017-03-13', '2017-03-15', 22385, 19591.2, 1, 1, 1, 1, 1, 1),
(274, 35, 3, '1', 'LMM0211849', 'SZW901253-5', 2, 'MAGARI', '005', 6, 1, '2017-03-11', '2017-03-11', '2017-03-16', '2017-03-16', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(275, 35, 3, '2', 'LMM0211849', 'SZW 902443-3', 2, 'MAGARI', '005', 6, 1, '2017-03-11', '2017-03-11', '2017-03-16', '2017-04-16', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(276, 35, 2, '2', '98171199', 'CRLU 722638-8', 4, 'MAGARI', '005', 3, 1, '2017-03-13', '2017-03-13', '2017-03-17', '2017-03-17', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(277, 35, 2, '1', '087LIM227465', 'TRIU 89434-5', 7, 'ARUSHIV.', 'NQ701R', 3, 1, '2017-03-11', '2017-03-11', '2017-03-17', '2017-03-17', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(278, 35, 4, '1', '98183899', 'HLXU 872841-6', 4, 'MAGARI', '711', 3, 2, '2017-03-12', '2017-03-12', '2017-03-16', '2017-03-17', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(279, 35, 4, '2', '98183899', 'GESU 933551-9', 4, 'MAGARI', '711', 3, 2, '2017-03-12', '2017-03-12', '2017-03-16', '2017-03-16', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(280, 35, 5, '1', '27803800762A', 'MORU 113458-9', 6, 'JAMILA', '00046', 2, 2, '2017-03-13', '2017-03-13', '2017-03-15', '2017-03-15', 19680, 17414.4, 1, 1, 1, 1, 1, 1),
(281, 35, 1, '21491', 'APLU 902297338', 'CGMU 515225-1', 1, 'JAMILA', '046', 7, 2, '2017-03-13', '2017-03-13', '2017-03-15', '2017-03-15', 21180, 18200, 1, 1, 1, 1, 1, 1),
(282, 35, 1, '21492', 'APLU 902297338', 'CGMU 934441-6', 1, 'JAMILA', '046', 7, 2, '2017-03-13', '2017-03-13', '2017-03-15', '2017-04-15', 21210, 18200, 1, 1, 1, 1, 1, 1),
(283, 37, 1, '21262', '959681955', 'PUNU 496304-6', 5, 'SEALAND MANZANILLO', '1704', 1, 2, '2017-02-27', '2017-02-27', '2017-03-01', '2017-03-01', 22350, 19591.2, 1, 1, 1, 1, 1, 1),
(284, 37, 1, '21263', 'MBM140178128', 'TEMU 921177-7', 8, 'STR HELLAS  REEFER', '17018', 1, 2, '2017-03-01', '2017-03-01', '2017-03-05', '2017-03-05', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(285, 37, 1, '21322', '959681955', 'MSWU 905019-0', 5, 'SEALAND MANZANILLO', '1704', 1, 2, '2017-02-28', '2017-02-28', '2017-03-01', '2017-03-01', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(286, 37, 1, '21321', '959681955', 'MNBU 018684-0', 5, 'SEALAND MANZANILLO', '1704', 1, 2, '2017-02-28', '2017-02-28', '2017-03-01', '2017-03-01', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(287, 37, 1, '21264', 'MBM140178128', 'TEMU 921567-0', 8, 'STR HELLAS  REEFER', '17018', 1, 2, '2017-03-01', '2017-03-01', '2017-03-05', '2017-03-05', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(288, 37, 1, '21265', 'MBM140178128', 'CXRU 112030-4', 8, 'STR HELLAS  REEFER', '17018', 1, 2, '2017-03-01', '2017-03-01', '2017-03-05', '2017-03-05', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(289, 37, 1, '21259', '087LIME226348', 'TTNU 814495-4', 7, 'VIDISHA', '709', 3, 2, '2017-03-02', '2017-03-02', '2017-03-07', '2017-03-07', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(290, 37, 1, '21256', '087LIME226348', 'SEGU 929466-7', 7, 'VIDISHA', '709', 3, 2, '2017-03-02', '2017-03-02', '2017-03-07', '2017-03-07', 22340, 19591.2, 1, 1, 1, 1, 1, 1),
(291, 37, 1, '21266', '087LIME226348', 'TTNU 807064-0', 7, 'VIDISHA', '709', 3, 2, '2017-03-02', '2017-03-02', '2017-03-07', '2017-03-07', 22460, 19591.2, 1, 1, 1, 1, 1, 1),
(292, 37, 1, '21260', '087LIME226348', 'GESU 614410-4', 7, 'VIDISHA 709', '709', 3, 2, '2017-03-03', '2017-03-03', '2017-03-08', '2017-03-08', 22310, 19591.2, 1, 1, 1, 1, 1, 1),
(293, 37, 1, '21261', '087LIME226348', 'CRSU 614410-4', 7, 'VIDISHA 709', '709', 3, 2, '2017-03-03', '2017-03-03', '2017-03-08', '2017-03-08', 22310, 19591.2, 1, 1, 1, 1, 1, 1),
(294, 37, 2, '1', '98141286', 'BMOU 920720-1', 4, 'MAGARI', '710', 3, 1, '2017-03-03', '2017-03-03', '2017-03-04', '2017-03-04', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(295, 37, 2, '3', '98141286', 'CXRU 140098-5', 4, 'MAGARI', '710', 3, 1, '2017-03-06', '2017-03-06', '2017-03-11', '2017-03-11', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(296, 37, 2, '4', '087LIM227132', 'HLXU 873679-6', 4, 'MAGARI', '709', 3, 1, '2017-03-04', '2017-03-04', '2017-03-09', '2017-04-09', 22350, 19591.2, 1, 1, 1, 1, 1, 1),
(297, 37, 2, '2', '98141286', 'CRLU 126701-3', 4, 'MAGARI', '710', 3, 1, '2017-03-03', '2017-03-03', '2017-03-08', '2017-03-08', 22130, 19591.2, 1, 1, 1, 1, 1, 1),
(298, 37, 5, '1', '27803785159A', 'MORU 113727-4', 6, 'ALIOTH', '00165', 2, 2, '2017-03-16', '2017-03-16', '2017-03-21', '2017-03-21', 19860, 17414.4, 1, 1, 1, 1, 1, 1),
(299, 37, 5, '2', '278037898449A', 'BMOU 972253-6', 6, 'ALIOTH', '00165', 2, 2, '2017-03-16', '2017-03-16', '2017-03-21', '2017-03-21', 19500, 17414.4, 1, 1, 1, 1, 1, 1),
(300, 37, 3, '1', 'LMM0211529', 'CGMU 652702-8', 9, 'MAGARI', '004', 6, 1, '2017-03-04', '2017-03-04', '2017-03-09', '2017-03-09', 22450, 19591.2, 1, 1, 1, 1, 1, 1),
(301, 37, 3, '2', 'LMM0211529', 'AMWCU 929952-8', 9, 'MAGARI', '004', 6, 1, '2017-03-04', '2017-03-04', '2017-03-09', '2017-03-09', 22420, 19591.2, 1, 1, 1, 1, 1, 1),
(302, 37, 4, '1', '98152394', 'GESU 935288-2', 4, 'MAGARI', '710', 3, 2, '2017-03-05', '2017-04-05', '2017-03-10', '2017-04-10', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(303, 37, 4, '2', '98152394', 'GESU 950598-1', 4, 'MAGARI', '710', 3, 2, '2017-03-05', '2017-03-05', '2017-03-10', '2017-03-10', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(304, 38, 1, '21132', '959600274', 'MSWU 003600-0', 5, 'SEALAND PHILADELPHIA', '1704', 1, 2, '2017-02-20', '2017-02-20', '2017-02-25', '2017-02-25', 22320, 19591.2, 1, 1, 1, 1, 1, 1),
(305, 38, 1, '21131', '959600274', 'MNBU 311496-8', 5, 'SEALAND PHILADELPHIA', '1704', 1, 2, '2017-02-20', '2017-02-20', '2017-02-25', '2017-02-25', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(306, 38, 1, '21133', '959600274', 'MNBU 311496-8', 5, 'SEALAND PHILADELPHIA', '1704', 1, 2, '2017-02-20', '2017-02-20', '2017-02-25', '2017-02-25', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(307, 38, 1, '21134', '969500277', 'MWCU 690649-3', 5, 'SEALAND PHILADELPHIA', '1704', 3, 2, '2017-02-21', '2017-02-21', '2017-02-26', '2017-01-26', 22400, 19591.2, 1, 1, 1, 1, 1, 1),
(308, 38, 1, '21135', '969500277', 'MNBU 023386-0', 5, 'SEALAND PHILADELPHIA', '1704', 3, 2, '2017-02-21', '2017-02-21', '2017-02-26', '2017-11-26', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(309, 38, 1, '21125', 'MBM140178126', 'CAIU 556284-6', 8, 'LUZON STRAIT', '17001', 3, 2, '2017-02-22', '2017-02-22', '2017-02-26', '2017-02-26', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(310, 38, 1, '21127', 'MBM140178126', 'CAIU', 8, 'LUZON STRAIT', '17001', 3, 2, '2017-02-22', '2017-02-22', '2017-02-26', '2017-02-26', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(311, 38, 1, '21126', 'MBM140178126', 'CAIU 556541-8', 8, 'LUZON STRAIT', '17001', 3, 2, '2017-02-22', '2017-02-22', '2017-02-26', '2017-02-26', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(312, 38, 1, '21128', '087LIM225770', 'SZLU 915151-0', 7, 'VAISHNAVIR', '708', 3, 2, '2017-02-23', '2017-02-23', '2017-02-28', '2017-02-28', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(313, 38, 1, '21129', '087LIM225770', 'TRIU 815586-6', 7, 'VAISHNAVIR', '708', 3, 2, '2017-02-23', '2017-02-23', '2017-02-28', '2017-04-28', 22335, 19591.2, 1, 1, 1, 1, 1, 1),
(314, 38, 1, '21130', '087LIM225770', 'MEDU 916199-2', 7, 'VAISHNAVIR', '708', 3, 2, '2017-02-23', '2017-02-23', '2017-02-28', '2017-02-28', 22420, 19591.2, 1, 1, 1, 1, 1, 1),
(315, 38, 1, '21048', '7LIMES1152', 'SUDU 624722-9', 3, 'MAGARI', '709', 1, 2, '2017-02-23', '2017-02-23', '2017-02-27', '2017-02-27', 22310, 19591.2, 1, 1, 1, 1, 1, 1),
(316, 38, 1, '21047', '7LIMES1152', 'SUDU 605546-3', 3, 'MAGARI', '709', 1, 2, '2017-02-23', '2017-02-23', '2017-02-27', '2017-02-27', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(317, 38, 1, '21049', '7LIMES1152', 'SUDU 629158-2', 3, 'MAGARI', '709', 1, 2, '2017-02-23', '2017-02-23', '2017-02-27', '2017-02-27', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(318, 38, 2, '1', '98109596', 'CRLU 126171-4', 4, 'MAGARI', '709', 3, 1, '2017-02-24', '2017-02-24', NULL, NULL, 22090, 19591.2, 1, 1, 1, 1, 1, 1),
(319, 38, 2, '2', '98109596', 'CPSU 512134-8', 4, 'MAGARI', '709', 3, 1, '2017-02-24', '2017-02-24', NULL, NULL, 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(320, 38, 5, '1', '27803785117A', 'FSCU 568365-5', 6, 'JAMILA', '045', 2, 2, '2017-02-27', '2017-02-27', NULL, NULL, 19910, 17414.4, 1, 1, 1, 1, 1, 1),
(321, 38, 3, '1', 'B6JBK024175', 'DFIU 723027-2', 9, 'JAMILA', '045', 6, 1, '2017-02-25', '2017-02-25', '2017-03-01', '2017-03-01', 19680, 17414.4, 1, 1, 1, 1, 1, 1);
INSERT INTO `contenedor` (`id`, `id_semana`, `id_cliente`, `referencia`, `booking`, `numero_contenedor`, `id_lineanaviera`, `nave`, `viaje`, `id_puertodestino`, `id_operador`, `fecha_proceso_inicio`, `fecha_proceso_fin`, `fecha_zarpe`, `fecha_llegada`, `peso_bruto`, `peso_neto`, `activo`, `factura`, `certificado`, `packing`, `valija`, `vl`) VALUES
(322, 38, 3, '2', 'B6JBK024175', 'DTPU 721093-0', 9, 'JAMILA', '045', 6, 1, '2017-02-27', '2017-02-27', '2017-07-02', '2017-07-09', 19750, 17414.4, 1, 1, 1, 1, 1, 1),
(323, 38, 4, '1', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', NULL, '2017-03-30', NULL, NULL, 0, 1, 1, 1, 1, 1),
(324, 38, 4, '1', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', NULL, '2017-03-30', NULL, NULL, 0, 1, 1, 1, 1, 1),
(325, 38, 4, '1', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', NULL, '2017-03-30', NULL, NULL, 0, 1, 1, 1, 1, 1),
(326, 38, 4, '1', '98114996', NULL, 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', NULL, '2017-03-30', NULL, NULL, 0, 1, 1, 1, 1, 1),
(327, 38, 4, '1', '98114996', 'CRLU 727352-2', 4, 'MAGARI', '709', 2, 2, '2017-02-24', '2017-02-24', NULL, NULL, 22120, 19591.2, 1, 1, 1, 1, 1, 1),
(328, 38, 4, '3', '98114996', 'GESU 948741-9', 4, 'MAGARI', '709', 2, 2, '2017-02-27', '2017-02-27', NULL, NULL, 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(329, 38, 4, '2', '98114996', 'CRLU 727559-3', 4, 'MAGARI', '709', 2, 2, '2017-02-25', '2017-02-25', NULL, NULL, 22050, 19591.2, 1, 1, 1, 1, 1, 1),
(330, 39, 1, '20910', '087LIM225155', 'MEDU 913147-3', 7, 'JULIA', '707', 3, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-02-19', 22420, 19591.2, 1, 1, 1, 1, 1, 1),
(331, 39, 1, '20914', '087LIM225155', 'CRLU 141145-5', 7, 'JULIA', '707', 3, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-02-19', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(332, 39, 1, '20911', '087LIM225155', 'TEMU 900898-6', 7, 'JULIA', '707', 3, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-02-19', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(333, 39, 1, '20913', '087LIM225155', 'MSCU 745022-2', 7, 'JULIA', '707', 3, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-02-19', 22520, 19591.2, 1, 1, 1, 1, 1, 1),
(334, 39, 1, '21046', '087LIM225155', 'TTNU 814654-0', 7, 'JULIA', '707', 3, 2, '2017-02-15', '2017-02-15', '2017-02-20', '2017-02-20', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(335, 39, 1, '21045', '087LIM225155', 'MEDU 905506-0', 7, 'JULIA', '707', 3, 2, '2017-02-15', '2017-02-15', '2017-02-20', '2017-03-20', 22420, 19591.2, 1, 1, 1, 1, 1, 1),
(336, 39, 1, '21041', 'MBM140178124', 'CXRU 112006-9', 8, 'LOMBOK STRAIT', '17013', 3, 2, '2017-02-16', '2017-02-16', '2017-02-21', '2017-02-21', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(337, 39, 1, '21043', 'MBM140178124', 'CXRU 118701-5', 8, 'LOMBOK STRAIT', '17013', 3, 2, '2017-02-16', '2017-02-16', '2017-02-19', '2017-02-19', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(338, 39, 1, '21044', 'MBM140178124', 'CXRU 128234-7', 8, 'LOMBOK STRAIT', '17013', 3, 2, '2017-02-17', '2017-02-17', '2017-02-19', '2017-02-19', 22390, 19591.2, 1, 1, 1, 1, 1, 1),
(339, 39, 1, '21045', 'MBM140178124', 'TRIU 859164-3', 8, 'LOMBOK STRAIT', '17013', 3, 2, '2017-02-16', '2017-02-16', '2017-02-19', '2017-02-19', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(340, 39, 2, '1', '98079254', 'CPSU 512069-7', 4, 'MAGARI', '708', 3, 1, '2017-02-18', '2017-02-18', '2017-02-23', '2017-02-23', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(341, 39, 2, '2', '98079254', 'BMOU 922128-3', 4, 'MAGARI', '708', 3, 1, '2017-02-18', '2017-02-18', '2017-02-23', '2017-02-23', 22050, 19591.2, 1, 1, 1, 1, 1, 1),
(342, 39, 2, '3', '98079254', 'GESU 924539-6', 4, 'MAGARI', '708', 3, 1, '2017-02-20', '2017-02-20', '2017-02-25', '2017-02-25', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(343, 39, 1, '21050', '959532974', 'MNBU 361098-9', 5, 'MARINER', '1704', 2, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-02-19', 22400, 19591.2, 1, 1, 1, 1, 1, 1),
(344, 39, 1, '21051', '2', 'MWCU 664404-8', 5, 'MARINER', '1704', 2, 2, '2017-02-14', '2017-02-14', '2017-02-19', '2017-02-19', 22420, 19591.2, 1, 1, 1, 1, 1, 1),
(345, 39, 5, '2', '087LIM25927', 'TRIU 890632-5', 6, 'SEALAND PHILADELPHIA', '1704', 2, 2, '2017-02-21', '2017-02-21', '2017-02-25', '2017-02-25', 19770, 17414.4, 1, 1, 1, 1, 1, 1),
(346, 39, 5, '1', '087LIM25927', 'SEGU 946177-0', 6, 'SEALAND PHILADELPHIA', '1704', 2, 2, '2017-02-20', '2017-02-20', '2017-02-25', '2017-02-25', 19600, 17414.4, 1, 1, 1, 1, 1, 1),
(347, 39, 3, '1', 'B6JBK024107', 'DFIU 812371-0', 9, 'ALIOTH', '164', 22, 1, '2017-02-19', '2017-02-19', '2017-02-24', '2017-02-24', 19690, 17414.4, 1, 1, 1, 1, 1, 1),
(348, 39, 3, '2', 'B6JBK024107', 'DFIU 215252-0', 9, 'ALIOTH', '164', 22, 1, '2017-02-19', '2017-02-19', '2017-02-24', '2017-02-24', 19660, 17414.4, 1, 1, 1, 1, 1, 1),
(349, 39, 4, '1', '98103030', 'CRLU 126978-3', 4, 'MAGARI', '708', 3, 2, '2017-02-17', '2017-02-17', '2017-02-21', '2017-02-21', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(350, 39, 4, '1', '98103030', 'CRLU 723499-5', 4, 'MAGARI', '708', 3, 2, '2017-02-17', '2017-02-17', '2017-02-21', '2017-02-21', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(351, 45, 1, '20915', '959459191', 'MNBU 333660-4', 5, 'MARIA  KATHARINA', '1706', 1, 2, '2017-02-06', '2017-02-06', '2017-02-11', '2017-02-11', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(352, 45, 1, '20916', '959459191', 'MNBU 333335-4', 5, 'MARIA  KATHARINA', '1706', 1, 2, '2017-02-06', '2017-02-06', '2017-02-11', '2017-02-11', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(353, 45, 1, '20918', '959459191', 'MWCU 531764-7', 5, 'MARIA  KATHARINA', '1706', 1, 2, '2017-02-06', '2017-02-06', '2017-02-11', '2017-02-11', 22140, 19591.2, 1, 1, 1, 1, 1, 1),
(354, 45, 1, '20917', '959459191', 'MNBU 324198-3', 5, 'MARIA  KATHARINA', '1706', 1, 2, '2017-02-06', '2017-02-06', '2017-02-11', '2017-02-11', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(355, 45, 1, '20919', '959459207', 'PONU 497181-7', 5, 'MARIA  KATHARINA', '1706', 3, 2, '2017-02-08', '2017-02-08', '2017-02-12', '2017-02-12', 22440, 19591.2, 1, 1, 1, 1, 1, 1),
(356, 45, 1, '20805', '087LIM224347', 'CXRU 156856-2', 7, 'ZLATA', '706', 3, 2, '2017-02-07', '2017-02-07', '2017-02-12', '2017-02-12', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(357, 45, 1, '20803', '087LIM224347', 'TTNU 845381-9', 7, 'ZLATA', '707', 3, 2, '2017-02-07', '2017-02-07', '2017-02-12', '2017-02-12', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(358, 45, 1, '20804', '087LIM224347', 'TTNU 840858-0', 7, 'ZLATA', '706', 3, 2, '2017-02-07', '2017-02-07', '2017-02-12', '2017-02-12', 22370, 19591.2, 1, 1, 1, 1, 1, 1),
(359, 45, 1, '20806', '087LIM224347', 'GESU 934390-0', 7, 'ZLATA', '706', 3, 2, '2017-02-08', '2017-02-08', '2017-02-13', '2017-02-13', 22360, 19591.2, 1, 1, 1, 1, 1, 1),
(360, 45, 1, '20904', 'MBM140178122', NULL, 8, 'ATLANTIC REEFER', '17012', 3, 2, '2017-02-08', '2017-02-08', '2017-02-13', '2017-03-13', NULL, 19591.2, 0, 1, 1, 1, 1, 1),
(361, 45, 1, '20904', 'MBM140178122', 'TTNU 896156-9', 8, 'ATLANTIC KLIPPER', '17012', 3, 2, '2017-02-08', '2017-02-08', '2017-02-13', '2017-02-13', 22350, 19591.2, 1, 1, 1, 1, 1, 1),
(362, 45, 1, '20908', 'MBM140178122', 'TTNU 895868-9', 8, 'ATLANIC KLIPPER', '17012', 3, 2, '2017-02-10', '2017-02-10', '2017-02-12', '2017-02-12', 22410, 19591.2, 1, 1, 1, 1, 1, 1),
(363, 45, 1, '20907', 'MBM140178122', 'TTNU 896177-0', 8, 'ATLANTIC KIPPER', '17012', 3, 2, '2017-02-08', '2017-02-08', '2017-02-13', '2017-02-13', 22360, 19591.2, 1, 1, 1, 1, 1, 1),
(364, 45, 1, '20906', 'MBM140178122', 'TTNU 896181-0', 8, 'ATLANTIC KLIPPER', '17012', 3, 2, '2017-02-09', '2017-02-09', '2017-02-13', '2017-02-13', 22460, 19591.2, 1, 1, 1, 1, 1, 1),
(365, 45, 1, '20905', 'MBM140178122', 'TTNU 895804-0', 8, 'ATLANTIC KLIPPER', '17012', 3, 2, '2017-02-09', '2017-02-09', '2017-02-13', '2017-02-13', 22360, 19591.2, 1, 1, 1, 1, 1, 1),
(366, 45, 1, '20909', '087LIM225155', 'TRIU 839010-3', 7, 'JULIA R', '707', 3, 2, '2017-02-11', '2017-02-11', '2017-02-16', '2017-03-16', 22500, 19591.2, 1, 1, 1, 1, 1, 1),
(367, 45, 2, '1', '98062266', 'LNXU 755883-8', 4, 'MAGARI', '707', 3, 1, '2017-02-10', '2017-02-10', '2017-02-15', '2017-02-15', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(368, 45, 2, '2', '98062266', 'CPSU 517098-0', 4, 'MAGARI', '707', 3, 1, '2017-02-11', '2017-02-11', '2017-02-15', '2017-02-15', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(369, 45, 5, '1', '27803765949A', 'MORU 110221-5', 6, 'JAMILA', '044', 2, 2, '2017-02-12', '2017-02-12', '2017-02-17', '2017-02-17', 19710, 17414.4, 1, 1, 1, 1, 1, 1),
(370, 24, 4, '2', '98542528', 'CSVU 750614-3', 4, 'MAGARI', '723', 3, 2, '2017-06-03', '2017-06-03', '2017-06-08', '2017-07-08', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(371, 24, 1, '22692', '7LIMES2945', 'SUDU 616397-7', 3, 'MAGARI', '723', 3, 2, '2017-06-03', '2017-06-03', '2017-06-08', '2017-07-08', 22390, 19591.2, 1, 1, 1, 1, 1, 1),
(372, 40, 1, '20807', '959403367', 'PONU 496849-6', 5, 'MERIDIAN', '1704', 1, 2, '2017-01-29', '2017-01-29', '2017-01-30', '2017-01-30', 22130, 19591.2, 1, 1, 1, 1, 1, 1),
(373, 40, 1, '20808', '959403367', 'PONU 486364-3', 5, 'MERIDIAN', '1704', 1, 2, '2017-01-29', '2017-01-29', '2017-02-04', '2017-02-04', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(374, 40, 1, '20809', '959403367', 'MNBU 012042-1', 5, 'MERIDIAN', '1704', 1, 2, '2017-01-30', '2017-01-30', '2017-02-04', '2015-02-04', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(375, 40, 1, '20810', '959403367', 'MNBU 012042-1', 5, 'MERIDIAN', '1704', 1, 2, '2017-01-30', '2017-01-30', '2017-02-04', '2014-02-04', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(376, 40, 1, '20811', '959403356', 'PONU 486136-3', 5, 'MERIDIAN', 'V.1704', 3, 2, '2017-01-28', '2017-01-28', '2017-02-04', '2017-02-04', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(377, 40, 1, '20812', '959403356', 'PUNU 485432-2', 5, 'MERIDIAN', 'V.1704', 3, 2, '2017-01-31', '2017-01-31', '2017-02-06', '2017-02-06', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(378, 40, 1, '20797', 'MBM140178119', 'TTNU 895573-5', 8, 'BALTIC KLIPPER', '1701', 3, 2, '2017-02-01', '2017-02-01', '2017-02-06', '2017-03-06', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(379, 40, 1, '20798', 'MBM140178119', 'TTNU 895009-7', 8, 'BALTIC KLIPPER', '17011', 3, 2, '2017-02-01', '2017-02-01', '2017-02-06', '2017-03-06', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(380, 40, 1, '20800', 'MBM140178119', 'TTNU 895928-4', 8, 'BALTIC KLIPPER', '17011', 3, 2, '2017-02-01', '2017-02-01', '2017-02-06', '2017-03-06', 22150, 19591.2, 1, 1, 1, 1, 1, 1),
(381, 40, 1, '20799', 'MBM140178119', 'TTNU 895345-5', 8, 'BALTIC KLIPPER', '17011', 3, 2, '2017-02-01', '2017-02-01', '2017-02-06', '2017-03-06', 22120, 19591.2, 1, 1, 1, 1, 1, 1),
(382, 46, 1, '22787', 'MBM140178155', 'TRIU 835537-6', 8, 'BALTIC KLIPPER', '17016', 3, 2, '2017-06-06', '2017-06-06', '2017-06-13', '2017-07-13', 22415, 19591.2, 1, 1, 1, 1, 1, 1),
(383, 46, 1, '22788', 'MBM140178155', 'TRIU 811487-2', 8, 'BALTIC KLIPPER', '17016', 3, 2, '2017-06-07', '2017-06-07', '2017-06-29', '2017-07-29', 22360, 19591.2, 1, 1, 1, 1, 1, 1),
(384, 46, 1, '22789', 'MBM140178155', 'CAIU 554907-9', 8, 'BALTIC KLIPPER', '17016', 3, 2, '2017-06-07', '2017-06-04', '2017-06-29', '2017-07-29', 22370, 19591.2, 1, 1, 1, 1, 1, 1),
(385, 46, 1, '22790', '7LIMES3087', 'SUDU 625143-0', 3, 'MAGARI', '724', 3, 2, '2017-06-08', '2017-06-08', '2017-06-13', '2017-07-13', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(386, 40, 2, '1', '087LIM223577', 'SEGU 945919-7', 7, 'KATYA', '705', 3, 1, '2017-01-31', '2017-01-31', '2017-02-05', '2017-02-05', 22070, 19591.2, 1, 1, 1, 1, 1, 1),
(387, 40, 2, '2', '087LIM223577', 'MEDU 913599-3', 7, 'KATYA', '705', 3, 1, '2017-02-01', '2017-02-01', '2017-02-05', '2017-02-05', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(388, 40, 2, '3', '98029733', 'CPSU 514094-4', 4, 'NORASIA ALYA', '7202', 3, 1, '2017-02-03', '2017-02-03', '2017-02-05', '2017-02-05', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(389, 40, 2, '4', '98029733', 'CPSU 515779-9', 4, 'NORASIA ALYA', '7202', 3, 1, '2017-02-03', '2017-02-03', '2017-02-08', '2017-02-08', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(390, 40, 5, '1', '27803765933A', 'SEGU 925356-5', 6, 'HANSA MEERSBURG', '0008', 2, 2, '2017-02-05', '2017-02-05', '2017-02-10', '2017-03-10', 19650, 17414.4, 1, 1, 1, 1, 1, 1),
(391, 40, 3, '1', 'B6JBK023987', 'DIPU 429184-0', 9, 'HANSA MEERSBURG', '008', 4, 1, '2017-02-05', '2017-02-05', '2017-02-10', '2017-02-10', 19760, 17414.4, 1, 1, 1, 1, 1, 1),
(392, 40, 4, '1', '98038004', 'CPSU 510241-4', 4, 'NORASIA ALYA', '7202', 3, 2, '2017-02-04', '2017-02-04', '2017-02-09', '2017-03-09', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(393, 40, 4, '2', '98038004', 'CPSU 510241-4', 4, 'NORASIA ALYA', '7202', 3, 2, '2017-02-04', '2017-02-04', '2017-02-09', '2017-03-09', 22090, 19591.2, 1, 1, 1, 1, 1, 1),
(394, 41, 1, '20710', '959341518', 'PONU 483501-9', 5, 'KIEL TRADER', '1704', 1, 2, '2017-01-23', '2017-01-23', '2017-01-28', '2017-02-28', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(395, 41, 1, '20712', '959341518', 'MWMU 641236-1', 5, 'KIEL TRADER', '1704', 1, 2, '2017-01-23', '2017-01-23', '2017-01-28', '2017-02-28', 22150, 19591.2, 1, 1, 1, 1, 1, 1),
(396, 41, 1, '20711', '959341518', 'MWCU 663391-1', 7, 'KIEL TRADER', '1704', 1, 2, '2017-01-23', '2017-01-23', '2017-01-28', '2017-02-28', 22390, 19591.2, 1, 1, 1, 1, 1, 1),
(397, 41, 1, '20713', '959341542', 'PONU 450833-5', 5, 'KIEL TRADER', '1704', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', '2017-01-29', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(398, 41, 1, '20714', '959341542', 'PONU 499225-5', 5, 'KIEL TRADER', '1704', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', '2017-01-29', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(399, 41, 1, '20777', '959341542', 'MNBU 007605-7', 5, 'KIEL TRADER', '1704', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', '2017-01-29', 22130, 19591.2, 1, 1, 1, 1, 1, 1),
(400, 41, 1, '20704', 'MBM140178116', 'TTNU 896088-1', 8, 'SEATRADE WHITE', '201705', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', '2017-02-02', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(401, 41, 1, '20694', 'MBM140178116', 'TTNU 895553-0', 8, 'SEATRADE WHITE', '201705', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', '2017-02-02', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(402, 41, 1, '20703', 'MBM140178116', 'TTNU 895955-6', 8, 'SEATRADE WHITE', '201705', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', '2017-02-02', 22140, 19591.2, 1, 1, 1, 1, 1, 1),
(403, 41, 1, '20702', 'MBM140178116', 'TTNU 896073-1', 8, 'SEATRADE WHITE', '201705', 3, 2, '2017-01-25', '2017-01-25', '2017-01-29', '2017-02-02', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(404, 41, 1, '20701', 'MBM140178116', 'TTNU 895539-7', 8, 'SEATRADE WHITE', '201705', 3, 2, '2017-01-24', '2017-01-24', '2017-01-29', '2017-02-02', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(405, 41, 1, '20707', '7LIMES0443', 'SUDU 822379-8', 3, 'JACK LONDON', '651', 3, 2, '2017-01-27', '2017-01-27', '2017-01-30', '2017-01-31', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(406, 41, 1, '20705', '7LIMES0443', 'SUDU 822334-0', 3, 'JACK LONDON', '651', 3, 2, '2017-01-26', '2017-01-26', '2017-01-29', '2017-01-31', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(407, 41, 1, '20709', '7LIMES0443', 'SUDU  824006-0', 3, 'JACK LONDON', '651', 3, 2, '2017-01-26', '2017-01-26', '2017-01-30', '2017-01-31', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(408, 41, 1, '20708', '7LIMES0443', 'SUDU 607249-7', 3, 'JACK LONDON', '651', 3, 2, '2017-01-26', '2017-01-26', '2017-01-30', '2017-01-30', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(409, 41, 1, '20706', '7LIMES0443', 'SUDU 822883-0', 3, 'JACK LONDON', '651', 3, 2, '2017-01-27', '2017-01-27', '2017-01-28', '2017-01-29', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(410, 41, 5, '1', '27803759611A', 'TCLU 124255-8', 6, 'JAMILA', '043', 2, 2, '2017-01-28', '2017-01-28', '2017-02-01', '2017-02-02', 19570, 17414.4, 1, 1, 1, 1, 1, 1),
(411, 41, 5, '2', '27803759611A', 'CRLU 162668-5', 6, 'JAMILA', '043', 2, 2, '2017-01-28', '2017-01-28', '2017-02-01', '2017-02-02', 19630, 17414.4, 1, 1, 1, 1, 1, 1),
(412, 41, 4, '3', '98021416', 'GESU 954909-3', 4, 'HANSA EUROPE', '6252', 3, 2, '2017-01-28', '2017-01-28', '2017-01-30', '2017-01-31', 22060, 19591.2, 1, 1, 1, 1, 1, 1),
(413, 41, 4, '1', '97987759', 'CPSU 510153-1', 4, 'JACK LONDON', '651', 3, 2, '2017-01-25', '2017-01-25', '2017-01-30', '2017-01-31', 22320, 19591.2, 1, 1, 1, 1, 1, 1),
(414, 41, 4, '2', '98021416', 'CRLU 723221-0', 4, 'HANSA EUROPE', '6252', 3, 2, '2017-01-27', '2017-01-27', '2017-01-30', '2017-01-31', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(415, 44, 1, '20600', '959266652', 'PONU 48312-3', 5, 'MARIA  KATHARINA', '1704', 3, 2, '2017-01-16', '2017-01-16', '2017-01-19', '2017-02-19', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(416, 44, 1, '20597', '959266652', 'MNBU 304147-6', 5, 'MARIA  KATHARINA', '1704', 3, 2, '2017-01-14', '2017-01-14', '2017-01-19', '2017-02-19', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(417, 44, 1, '20599', '959266652', 'MSWU 959266652', 8, 'MARIA  KATHARINA', '1704', 3, 2, '2017-01-16', '2017-01-16', '2017-01-19', '2017-02-19', 22340, 19591.2, 1, 1, 1, 1, 1, 1),
(418, 44, 1, '20598', '959266652', 'MSWU 901933-8', 5, 'MARIA  KATHARINA', '1704', 3, 2, '2017-01-16', '2017-01-16', '2017-01-19', '2017-02-19', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(419, 44, 1, '20602', '959266659', 'MNBU 003905-3', 5, 'MARIA  KATHARINA', '1704', 1, 2, '2017-01-17', '2017-01-17', '2017-01-21', '2017-02-21', 22410, 19591.2, 1, 1, 1, 1, 1, 1),
(420, 44, 1, '20601', '959266659', 'MWSU 901692-0', 5, 'MARIA  KATHARINA', '1704', 3, 2, '2017-01-17', '2017-01-17', '2017-01-21', '2017-02-21', 22340, 19591.2, 1, 1, 1, 1, 1, 1),
(421, 44, 1, '20604', '959266659', 'MWCU 6677867-0', 5, 'MARIA  KATHARINA', '1704', 1, 2, '2017-01-17', '2017-01-17', '2017-01-21', '2017-02-21', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(422, 44, 1, '20603', '959266659', 'MNBU 003905-3', 5, 'MARIA  KATHARINA', '1704', 1, 2, '2017-01-17', '2017-01-17', '2017-01-21', '2017-02-21', 22410, 19591.2, 1, 1, 1, 1, 1, 1),
(423, 44, 1, '20593', 'MBM140178114', 'TTNU 895842-0', 8, 'ATLANTIC REEFER', '17008', 3, 2, '2017-01-20', '2017-01-20', '2017-01-24', '2017-02-24', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(424, 44, 1, '20592', 'MBM140178114', 'TTNU 896202-0', 3, 'ATLANTIC REEFER', '17008', 3, 2, '2017-01-19', '2017-01-19', '2017-01-23', '2017-02-23', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(425, 44, 1, '20591', 'MBM140178114', 'TTNU 895519-1', 3, 'ATLANTIC REEFER', '17008', 3, 2, '2017-01-19', '2017-01-19', '2017-01-23', '2017-02-23', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(426, 44, 1, '20590', 'MBM140178114', 'TTNU 895899-2', 8, 'ATLANTIC REEFER', '17008', 3, 2, '2017-01-18', '2017-01-18', '2017-01-23', '2017-02-23', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(427, 44, 1, '20589', 'MBM140178114', 'TTNU 895753-2', 8, 'ATLANTIC REEFER', '17008', 3, 2, '2017-01-18', '2017-01-18', '2017-01-23', '2017-02-23', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(428, 44, 1, '20596', '7LIMES0316', 'SUDU 818251-2', 3, 'VENETIA', '550', 1, 2, '2017-01-20', '2017-01-20', '2017-01-25', '2017-02-20', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(429, 44, 1, '20591', '7LIMES0316', 'SUDU 803220-9', 3, 'HAMMONIA VENETIA', '550', 3, 2, '2017-01-20', '2017-01-20', '2017-01-25', '2017-02-20', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(430, 44, 1, '20594', '7LIMES0316', 'SUDU 816039-1', 3, 'HAMMONIA VENETIA', '550', 3, 2, '2017-01-20', '2017-01-20', '2017-01-25', '2017-02-20', 22300, 19591.2, 1, 1, 1, 1, 1, 1),
(431, 44, 2, '2', '97975720', 'FSCU 56150-4', 4, 'CAP DOUKATO', '6251', 3, 1, '2017-01-21', '2017-01-21', '2017-01-26', '2017-02-26', 11150, 19591.2, 1, 1, 1, 1, 1, 1),
(432, 44, 2, '1', '97975720', 'CRLU 12743-2', 4, 'CAP DOUKATO', '6251', 3, 1, '2017-01-21', '2017-01-21', '2017-01-26', '2017-02-26', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(433, 44, 5, '1', '27803747380A', 'BMOU 975255-1', 6, 'HANSA MEERSBURG', '07', 2, 2, '2017-01-22', '2017-01-22', '2017-01-27', '2017-02-27', 19860, 17414.4, 1, 1, 1, 1, 1, 1),
(434, 44, 3, '1', 'B6JBK023876', 'DFIU 710073-0', 9, 'HANSA MEERSBURG', '07', 4, 1, '2017-01-22', '2017-01-22', '2017-01-27', '2017-02-27', 19750, 17414.4, 1, 1, 1, 1, 1, 1),
(435, 44, 3, '1', 'B6JBK023876', NULL, 9, 'HANSA MEERSBURG', '07', 4, 1, '2017-01-22', '2017-01-22', '2017-01-27', '2017-02-27', NULL, NULL, 0, 1, 1, 1, 1, 1),
(436, 44, 4, '2', '97987759', 'HLXU 871473-1', 4, 'HAPPAG LLOYD', '6251', 3, 2, '2017-01-19', '2017-01-19', '2017-01-23', '2017-02-25', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(437, 44, 4, '3', '97987759', 'CPSU 517265-9', 4, 'JACK LONDON', '6251', 3, 2, '2017-01-19', '2017-01-19', '2017-01-23', '2017-02-25', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(438, 44, 4, '1', '97987759', 'HLXU 871903-4', 4, 'HAPPAG LLOYD', '6251', 3, 2, '2017-01-18', '2017-01-18', '2017-01-23', '2017-02-25', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(439, 43, 1, '20466', '959182291', 'PONU 494233-6', 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(440, 43, 1, '20467', '959182291', 'MNBU 305438-6', 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(441, 43, 1, '20474', '959182291', 'MNBU 309499-0', 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-09', '2017-01-09', '2017-01-12', '2017-02-12', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(442, 43, 1, '20473', '959182291', 'PONE 499461-7', 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-08', '2017-01-08', '2017-01-12', '2017-02-12', 22320, 19591.2, 1, 1, 1, 1, 1, 1),
(443, 43, 1, '20472', '959182291', 'MNBU 345494-7', 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-08', '2017-01-08', '2017-01-12', '2017-02-12', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(444, 43, 1, '20471', '959182291', 'MWCU 629931-6', 5, 'SEALAND PHILADELPHIA', '1702', 3, 2, '2017-01-08', '2017-01-08', '2017-01-12', '2017-02-12', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(445, 43, 1, '20470', '959182308', 'MNBU 313510-6', 8, 'SEALAND PHILADELPHIA', '1702', 1, 2, '2017-01-09', '2017-01-09', '2017-01-14', '2017-02-09', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(446, 43, 1, '20469', '959182308', 'MNBU 337325-2', 5, 'SEALAND PHILADELPHIA', '1702', 1, 2, '2017-01-09', '2017-01-09', '2017-01-14', '2017-02-09', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(447, 43, 1, '20468', '959182308', 'MSWU 905159-8', 5, 'SEALAND PHILADELPHIA', '1702', 1, 2, '2017-01-09', '2017-01-09', '2017-01-14', '2017-02-09', 22310, 19591.2, 1, 1, 1, 1, 1, 1),
(448, 46, 2, '1', '98568834', 'HLXU 877275-9', 4, 'MAGARI', '724', 3, 1, '2017-06-09', '2017-06-09', '2017-06-16', '2017-07-16', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(449, 46, 1, '22791', '7LIMES3087', 'SUDU 800455-2', 3, 'MAGARI', '724', 3, 2, '2017-06-08', '2017-06-08', '2017-06-16', '2017-07-16', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(450, 46, 1, '22792', '7LIMES3087', 'SUDU 801296-4', 3, 'MAGARI', '724', 3, 2, '2017-06-11', '2017-06-11', '2017-06-16', '2017-07-16', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(451, 46, 1, '22794', '7LIMES3087', 'SUDU 823377-5', 3, 'MAGARI', '724', 3, 2, '2017-06-10', '2017-06-10', '2017-06-18', '2017-07-18', 22400, 19591.2, 1, 1, 1, 1, 1, 1),
(452, 46, 1, '22797', '960930886', 'MNBU 041418-0', 5, 'SEALAND LOS ANGELES', '1710', 1, 2, '2017-06-06', '2017-06-06', '2017-06-09', '2017-07-02', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(453, 46, 1, '1', '98586654', 'CPSU 515501-3', 4, 'MAGARI', '724', 3, 2, '2017-06-09', '2017-06-09', '2017-06-14', '2017-07-02', NULL, NULL, 0, 1, 1, 1, 1, 1),
(454, 46, 1, '22796', '960930886', 'MNBU 308760-4', 5, 'SEALAND LOS ANGELES', '1710', 1, 2, '2017-06-05', '2017-06-05', '2017-06-09', '2017-07-02', 22290, 19591.2, 1, 1, 1, 1, 1, 1),
(455, 46, 1, '22798', '960921067', 'MMAU 124524-0', 5, 'SEALAND LOS ANGELES', '1710', 7, 2, '2017-06-06', '2017-06-06', '2017-06-09', '2017-07-09', 21320, 18200, 1, 1, 1, 1, 1, 1),
(456, 47, 1, '22897', 'MBM140178156', 'SZLU 980178-6', 8, 'ATLANTIC KLIPPER', '17017', 3, 2, '2017-06-14', '2017-06-14', '2017-06-18', '2017-07-06', 22460, 19591.2, 1, 2, 1, 1, 1, 1),
(457, 47, 1, '22895', 'MBM140178156', 'CAIU 556259-5', 8, 'ATLANTIC KLIPPER', '17017', 3, 2, '2017-06-13', '2017-06-13', '2017-06-18', '2017-07-06', 22410, 19591.2, 1, 2, 1, 1, 1, 1),
(458, 47, 1, '22896', 'MBM140178156', 'CAIU 556413-4', 8, 'ATLANTIC KLIPPER', '17017', 3, 2, '2017-06-13', '2017-06-13', '2017-06-18', '2017-07-06', 22400, 19591.2, 1, 2, 1, 1, 1, 1),
(459, 47, 1, '22902', '7LIMES3276', 'SUDU 801981-9', 3, 'MAGARI', '725', 3, 2, '2017-06-17', '2017-06-17', '2017-06-20', '2017-07-11', 22470, 19591.2, 1, 2, 1, 1, 1, 1),
(460, 47, 1, '22901', '7LIMES3276', 'SUDU 629428-3', 3, 'MAGARI', '725', 3, 2, '2017-06-15', '2017-06-15', '2017-06-20', '2017-07-11', 22300, 19591.2, 1, 2, 1, 1, 1, 1),
(461, 47, 1, '22900', '7LIMES3276', 'SUDU 628780-7', 3, 'MAGARI', '725', 3, 2, '2017-06-14', '2017-06-14', '2017-06-20', '2017-07-11', 22340, 19591.2, 1, 2, 1, 1, 1, 1),
(462, 47, 1, '22899', '7LIMES3276', 'SUDU 801152-5', 3, 'MAGARI', '725', 3, 2, '2017-06-14', '2017-06-14', '2017-06-20', '2017-07-11', 22370, 19591.2, 1, 2, 1, 1, 1, 1),
(463, 47, 1, '22898', '7LIMES3276', 'SUDU 803505-0', 3, 'MAGARI', '725', 3, 2, '2017-06-15', '2017-06-15', '2017-06-20', '2017-07-11', 22280, 19591.2, 1, 2, 1, 1, 1, 1),
(464, 47, 1, '22905', '961020455', 'MMAU 111927-8', 5, 'SEALAND BALBOA', '1710', 7, 2, '2017-06-13', '2017-06-13', '2017-06-16', '2017-07-09', 21440, 18200, 1, 2, 1, 1, 1, 1),
(465, 47, 1, '22903', '961030470', 'MNBU 365704-0', 5, 'SEALAND BALBOA', '1710', 1, 2, '2017-06-12', '2017-06-12', '2017-06-16', '2017-07-09', 22300, 19591.2, 1, 2, 1, 1, 1, 1),
(466, 47, 1, '22904', '961030470', 'MWCU 678940-0', 5, 'SEALAND BALBOA', '1710', 1, 2, '2017-06-12', '2017-06-12', '2017-06-16', '2017-07-09', 22310, 19591.2, 1, 2, 1, 1, 1, 1),
(467, 43, 1, '20459', 'MBM140178112', 'TTNU 896082-9', 8, 'LUZON STRAIT', '17007', 2, 2, '2017-01-10', '2017-01-10', '2017-01-15', '2017-02-14', 22210, 19591.2, 1, 1, 1, 1, 1, 1),
(468, 43, 1, '20461', 'MBM140178112', 'CXRU 153754-0', 3, 'LUZON STRAIT', '17007', 3, 2, '2017-01-10', '2017-01-10', '2017-01-15', '2017-02-10', 22180, 19591.2, 1, 1, 1, 1, 1, 1),
(469, 43, 1, '20460', 'MBM140178112', 'TTNU 896066-5', 3, 'LUZON STRAIT', '17007', 3, 2, '2017-01-10', '2017-01-10', '2017-01-15', '2017-02-10', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(470, 43, 1, '20463', 'MBM140178112', 'TTNU 895541-6', 8, 'LUZON STRAIT', '17007', 3, 2, '2017-01-11', '2017-01-11', '2017-01-15', '2017-02-10', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(471, 43, 1, '20466', 'MBM140178112', 'TTNU 895542-1', 8, 'LUZON STRAIT', '17007', 3, 2, '2017-01-10', '2017-01-10', '2017-01-15', '2017-02-15', 22120, 19591.2, 1, 1, 1, 1, 1, 1),
(472, 43, 5, '20465', '27803738075A', 'MORU 580799-1', 6, 'JAMILA', '0042', 2, 2, '2017-01-14', '2017-01-14', '2017-01-17', '2017-02-15', 19760, 17414.4, 1, 1, 1, 1, 1, 1),
(473, 43, 2, '2', '97948060', 'CRLU 726811-0', 4, 'BALTASAR SCHULTE', '6250', 3, 1, '2017-01-12', '2017-01-12', '2017-01-17', '2017-02-15', 21950, 19591.2, 1, 1, 1, 1, 1, 1),
(474, 43, 2, '1', '97948060', 'CRLU 726872-1', 4, 'BALTASAR SCHULTE', '6250', 3, 1, '2017-01-12', '2017-01-12', '2017-01-17', '2017-02-15', 21950, 19591.2, 1, 1, 1, 1, 1, 1),
(475, 43, 1, '20464', '7LIMES0207', 'SUDU 505287-6', 3, 'GLASGOW EXPRESS', '649', 3, 2, '2017-01-11', '2017-01-11', '2017-01-17', '2017-02-15', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(476, 43, 1, '20465', '7LIMES0207', 'SUDU 506496-4', 3, 'GLASGOW EXPRESS', '649', 3, 2, '2017-01-11', '2017-01-11', '2017-01-17', '2017-02-15', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(477, 43, 3, '1', 'LMM0209565', 'CGMU 514920-0', 9, 'BALTASAR SCHULTE', '6250', 6, 1, '2017-01-13', '2017-01-13', '2017-01-14', '2017-02-14', 22380, 19591.2, 1, 1, 1, 1, 1, 1),
(478, 43, 3, '2', 'LMM0209565', 'DFIU 331303-9', 9, 'WILMINTON', '042', 3, 1, '2017-01-14', '2017-01-14', '2017-01-19', '2017-02-12', 19900, 17414.4, 1, 1, 1, 1, 1, 1),
(479, 43, 4, '2', '97963010', 'LNXU 755192-0', 4, 'BALTASAR SCHULTE', '6250', 1, 2, '2017-01-12', '2017-01-12', '2017-01-17', '2017-02-12', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(480, 43, 4, '1', '97963010', 'HLXU 870752-1', 4, 'BALTASAR SCHULTE', '6250', 3, 2, '2017-01-12', '2017-01-12', '2017-01-17', '2017-02-12', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(481, 43, 4, '3', '97963010', 'HLXU 872039-6', 4, 'BALTASAR SCHULTE', '6250', 3, 2, '2017-01-13', '2017-01-13', '2017-01-17', '2017-02-12', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(482, 43, 4, '4', '97963010', 'CPSU 511579-3', 4, 'BALTASAR SCHULTE', '6250', 3, 2, '2017-01-13', '2017-01-13', '2017-01-17', '2017-02-12', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(483, 42, 1, '20386', '959097108', 'MNBU 900997-5', 5, 'KIEL TRADER', '1702', 3, 2, '2016-12-31', '2016-12-31', '2017-01-04', '2017-02-01', 22090, 19591.2, 1, 1, 1, 1, 1, 1),
(484, 42, 1, '20381', '959097108', 'MNWU 005645-4', 5, 'KIEL TRADER', '1702', 3, 2, '2017-01-02', '2017-01-02', '2017-01-06', '2017-02-01', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(485, 42, 1, '20380', '959097108', 'MNBU 360791', 5, 'KIEL TRADER', '1702', 1, 2, '2017-01-02', '2017-01-02', '2017-01-04', '2017-02-01', 22310, 19591.2, 1, 1, 1, 1, 1, 1),
(486, 42, 1, '20387', '959097108', 'MNBU 903004-4', 5, 'KIEL TRADER', '1702', 3, 2, '2017-01-02', '2017-01-02', '2017-01-07', '2017-02-01', 22310, 19591.2, 1, 1, 1, 1, 1, 1),
(487, 42, 1, '20382', '959097108', 'MWCU 670330-4', 5, 'KIEL TRADER', '1702', 3, 2, '2017-01-03', '2017-01-03', '2017-01-08', '2017-02-08', 22400, 19591.2, 1, 1, 1, 1, 1, 1),
(488, 42, 1, '20384', '959097140', 'MWCU 682680-2', 5, 'KIEL TRADER', '1702', 1, 2, '2017-01-03', '2017-01-03', '2017-01-08', '2017-02-08', 22320, 19591.2, 1, 1, 1, 1, 1, 1),
(489, 42, 1, '20383', '959097140', 'MNBU 307581-4', 5, 'KIEL TRADER', '1702', 1, 2, '2017-01-03', '2017-01-03', '2017-01-08', '2017-02-08', 22320, 19591.2, 1, 1, 1, 1, 1, 1),
(490, 42, 1, '20385', '959097140', 'MWCU 694362-4', 5, 'KIEL TRADER', '1702', 1, 2, '2017-01-03', '2017-01-03', '2017-01-08', '2017-02-08', 22220, 19591.2, 1, 1, 1, 1, 1, 1),
(491, 42, 1, '20376', 'MBM140178110', 'CXRU 110968-7', 8, 'LOMBOK STRAIT', '160103', 3, 2, '2017-01-04', '2017-01-04', '2017-01-09', '2017-02-10', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(492, 42, 1, '20374', 'MBM140178110', 'TRIU 8591140-0', 8, 'LOMBOK STRAIT', '160103', 3, 2, '2017-01-04', '2017-01-04', '2017-01-09', '2017-02-10', 22280, 19591.2, 1, 1, 1, 1, 1, 1),
(493, 42, 1, '20378', '6LIMES8757', 'SUDU 615755-2', 3, 'SPIRIT OF HAMBURG', '648', 3, 2, '2017-01-06', '2017-01-06', '2017-01-08', '2017-02-08', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(494, 42, 1, '20377', '6LIMES8757', 'SIDU 625910-6', 3, 'SPIRIT OF HAMBURG', '648', 3, 2, '2017-01-05', '2017-01-05', '2017-01-08', '2017-02-08', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(495, 42, 1, '20379', '6LIMES8757', 'SUDU 808629-4', 3, 'SPIRIT OF HAMBURG', '648', 3, 2, '2017-01-06', '2017-01-06', '2017-01-11', '2017-02-11', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(496, 42, 2, '4', '97916134', 'SEGU 910535-7', 4, 'NORTHERN DEXTERITY', '6249', 3, 1, '2017-01-05', '2017-01-05', '2017-01-09', '2017-02-09', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(497, 42, 2, '3', '97916134', 'TRIU 834400-5', 4, 'NORTHERN DEXTERITY', '6249', 3, 1, '2017-01-05', '2017-01-01', '2017-01-09', '2017-02-09', 22085, 19591.2, 1, 1, 1, 1, 1, 1),
(498, 42, 2, '2', '97916134', 'CRLU 721799-8', 4, 'NORTHERN DEXTERITY', '6249', 3, 1, '2017-01-05', '2017-01-31', '2017-01-09', '2017-02-09', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(499, 42, 2, '1', '97916134', 'CRLU 725353-1', 4, 'NORTHERN DEXTERITY', '6249', 3, 1, '2017-01-04', '2017-01-04', '2017-01-09', '2017-02-09', 22110, 19591.2, 1, 1, 1, 1, 1, 1),
(500, 42, 5, '1', '27803720646A', 'TEMU 919858-8', 6, 'HANSA MEERS BURG', '06', 2, 2, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', 16690, 17414.4, 1, 1, 1, 1, 1, 1),
(501, 42, 3, '2', 'B6JBK023765', 'DTPU 213315-2', 9, 'HANSA MEERS BURG', '06', 5, 1, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', 19790, 17414.4, 1, 1, 1, 1, 1, 1),
(502, 42, 3, '1', 'B6JBK023765', 'DTPU 2115-2', 9, 'HANSA MEERS BURG', '06', 5, 1, '2017-01-07', '2017-01-07', '2017-01-12', '2017-02-12', 19790, 17414.4, 1, 1, 1, 1, 1, 1),
(503, 42, 4, '1', '97916337', 'SEGU 942085-2', 4, 'NORTHERN DEXTERITY', '6249', 3, 2, '2017-01-06', '2017-01-06', '2017-01-11', '2017-02-11', 22070, 19591.2, 1, 1, 1, 1, 1, 1),
(504, 42, 4, '2', '97916337', 'SEGU 935356-4', 4, 'NORTHERN DEXTERITY', '6249', 3, 2, '2017-01-07', '2017-01-07', '2017-01-11', '2017-02-11', 22060, 19591.2, 1, 1, 1, 1, 1, 1),
(505, 46, 2, '2', '98568834', 'HLXU 876189-9', 4, 'MAGARI', '724', 3, 1, '2017-06-09', '2017-06-09', '2017-06-14', '2017-07-14', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(506, 46, 4, '2', '98586654', 'HLXU 874770-9', 4, 'MAGARI', '724', 3, 2, '2017-06-10', '2017-06-10', '2017-06-15', '2017-07-15', 22250, 19591.2, 1, 1, 1, 1, 1, 1),
(507, 46, 4, '1', '98586654', 'CPSU 515501-3', 4, 'MAGARI', '724', 3, 2, '2017-06-09', '2017-06-09', '2017-06-14', '2017-07-14', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(508, 46, 3, '1', 'LMM0215762', 'TLLU 104789-0', 2, 'MAGARI', '018', 24, 1, '2017-06-10', '2017-06-10', '2017-06-15', '2017-07-15', 22300, 19591.2, 1, 1, 1, 1, 1, 1),
(509, 46, 3, '2', 'B6JBK024858', 'DFIU 811104-6', 9, 'ALIOTH', '712', 4, 1, '2017-06-12', '2017-06-12', '2017-06-17', '2017-07-17', 19840, 17414.4, 1, 1, 1, 1, 1, 1),
(510, 46, 5, '1', '7LIMAG1480', 'SUDU 623810-3', 3, 'MAGARI', '724', 2, 2, '2017-06-10', '2017-06-10', '2017-06-15', '2017-07-15', 19740, 17414.4, 1, 1, 1, 1, 1, 1),
(511, 46, 5, '2', '7LIMAG1501', 'SUDU 627365-5', 3, 'MAGARI', '724', 2, 2, '2017-06-10', '2017-06-10', '2017-06-15', '2017-07-15', 19620, 17414.4, 1, 1, 1, 1, 1, 1),
(512, 48, 1, '22997', 'MBM140178159', 'CXRU 111677-3', 8, 'LUZON STRAIT', '17019', 3, 2, '2017-06-20', '2017-07-20', '2017-06-25', '2017-07-13', 22320, 19591.2, 1, 1, 1, 1, 1, 1),
(513, 48, 1, '22999', 'MBM140178159', 'CXRU 152387-1', 8, 'LUZON STRAIT', '17019', 3, 2, '2017-06-21', '2017-07-16', '2017-06-25', '2017-07-13', 22240, 19591.2, 1, 1, 1, 1, 1, 1),
(514, 48, 1, '22998', 'MBM140178159', 'TRIU 859069-4', 8, 'LUZON STRAIT', '17019', 3, 2, '2017-06-20', '2017-07-20', '2017-06-25', '2017-07-13', 22370, 19591.2, 1, 1, 1, 1, 1, 1),
(515, 48, 1, '23002', '7LIMES3383', 'SUDU 805116-9', 3, 'LISZT', '726', 3, 2, '2017-06-21', '2017-06-21', '2017-06-27', '2017-07-18', 22310, 19591.2, 1, 1, 1, 1, 1, 1),
(516, 48, 1, '23001', '7LIMES3383', 'SUDU 821029-7', 3, 'LISZT', '726', 3, 2, '2017-06-17', '2017-06-21', '2017-06-27', '2017-07-18', 22300, 19591.2, 1, 1, 1, 1, 1, 1),
(517, 48, 1, '23000', '7LIMES3383', 'SUDU 811441-5', 3, 'LISZT', '726', 3, 2, '2017-06-22', '2017-06-22', '2017-06-27', '2017-06-18', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(518, 48, 1, '23003', '7LIMES3383', 'SUDU 817060-9', 3, 'LISZT', '726', 3, 2, '2017-06-24', '2017-06-24', '2017-06-27', '2017-06-18', 22360, 19591.2, 1, 1, 1, 1, 1, 1),
(519, 48, 1, '23006', '961097019', 'MMAU 101782-5', 5, 'MARINER', '1710', 7, 2, '2017-06-20', '2017-06-20', '2017-06-23', '2017-07-16', 21380, 18200, 1, 1, 1, 1, 1, 1),
(520, 48, 1, '23004', '961106881', 'PONU 483173-3', 5, 'MARINER', '1710', 1, 2, '2017-06-19', '2017-06-19', '2017-06-23', '2017-07-16', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(521, 48, 1, '23005', '961106881', 'PONU 484117-7', 5, 'MARINER', '1710', 1, 2, '2017-06-19', '2017-06-19', '2017-06-23', '2017-07-16', 22400, 19591.2, 1, 1, 1, 1, 1, 1),
(522, 47, 2, '1', '98598984', 'HLXU 875946-4', 4, 'MAGARI', '725', 3, 1, '2017-06-15', '2017-06-15', '2017-06-21', '2017-07-19', 22250, 19591.2, 1, 2, 1, 1, 1, 1),
(523, 47, 2, '2', '98598984', 'HLBU 900823-9', 4, 'MAGARI', '725', 3, 1, '2017-06-16', '2017-06-16', '2017-06-21', '2017-07-19', 22100, 19591.2, 1, 2, 1, 1, 1, 1),
(524, 47, 4, '1', '98622252', 'TCLU 117750-8', 4, 'MAGARI', '725', 3, 2, '2017-06-16', '2017-06-16', '2017-06-22', '2017-07-12', 22100, 19591.2, 1, 2, 1, 1, 1, 1),
(525, 47, 4, '2', '98622252', 'HLBU 902370-0', 4, 'MAGARI', '725', 3, 2, '2017-06-16', '2017-06-16', '2017-06-22', '2017-07-12', 22160, 19591.2, 1, 2, 1, 1, 1, 1),
(526, 47, 5, '2', '7LIMAG1548', 'SUDU 810704-1', 3, 'MAGARI', '725', 2, 2, '2017-06-17', '2017-06-17', '2017-06-24', '2017-07-14', 19600, 17414.4, 1, 2, 1, 1, 1, 1),
(527, 47, 5, '2', '7LIMAG1552', 'SUDU 803122-3', 3, 'MAGARI', '725', 2, 2, '2017-06-17', '2017-06-17', '2017-06-24', '2017-07-13', 19570, 17414.4, 1, 2, 1, 1, 1, 1),
(528, 47, 3, '1', 'LMM0216123', 'TLLU 105911-8', 2, 'MAGARI', '019', 6, 1, '2017-06-17', '2017-06-17', '2017-06-22', '2017-07-13', 22220, 19591.2, 1, 2, 1, 1, 1, 1),
(529, 47, 3, '2', 'LMM0216123', 'BMOU 964121-8', 2, 'MAGARI', '019', 6, 1, '2017-06-17', '2017-06-17', '2017-06-22', '2017-07-13', 22210, 19591.2, 1, 2, 1, 1, 1, 1),
(530, 49, 1, '23109', 'MBM140178160', 'RRSU 168117-3', 8, 'SCHWEIZ REEFER', 'RA17020EB', 3, 2, '2017-06-25', '2017-06-25', '2017-07-02', '2017-07-20', 22400, 19591.2, 1, 2, 1, 1, 1, 1),
(531, 49, 1, '23137', 'MBM140178160', 'TEMU 937220-0', 8, 'SCHWEIZ REEFER', 'RA17020EB', 3, 2, '2017-06-27', '2017-06-27', '2017-07-02', '2017-07-20', 22250, 19591.2, 1, 2, 1, 1, 1, 1),
(532, 49, 1, '23110', 'MBM140178160', 'CXRU 152647-0', 8, 'SCHWEIZ REEFER', 'RA17020EB', 3, 2, '2017-06-26', '2017-06-26', '2017-07-02', '2017-07-20', 22270, 19591.2, 1, 2, 1, 1, 1, 1),
(533, 49, 1, '23116', '7LIMES3561', 'SUDU 801096-1', 3, 'HS LISZT', '727N', 3, 2, '2017-06-28', '2017-06-28', '2017-07-04', '2017-07-25', 22410, 19591.2, 1, 2, 1, 1, 1, 1),
(534, 49, 1, '23115', '7LIMES3561', 'SUDU 801828-4', 3, 'HS LISZT', '727N', 3, 2, '2017-06-27', '2017-06-27', '2017-07-04', '2017-07-25', 22460, 19591.2, 1, 2, 1, 1, 1, 1),
(535, 49, 1, '23114', '7LIMES3561', 'SUDU 813105-3', 3, 'HS LISZT', '727N', 3, 2, '2017-06-26', '2017-06-26', '2017-07-04', '2017-07-25', 22530, 19591.2, 1, 2, 1, 1, 1, 1),
(536, 49, 1, '23113', '7LIMES3561', 'SUDU 620640-4', 3, 'HS LISZT', '727N', 3, 2, '2017-06-26', '2017-06-26', '2017-07-04', '2017-07-25', 22310, 19591.2, 1, 2, 1, 1, 1, 1),
(537, 49, 1, '23112', '7LIMES3561', 'SUDU 623352-3', 3, 'HS LISZT', '727N', 3, 2, '2017-06-27', '2017-06-27', '2017-07-04', '2017-07-25', 22290, 19591.2, 1, 2, 1, 1, 1, 1),
(538, 49, 1, '23117', '961208637', 'MSWU 001523-9', 5, 'SEALAND PHILADELPHIA', '1710', 1, 2, '2017-06-24', '2017-06-24', '2017-06-30', '2017-07-23', 22170, 19591.2, 1, 2, 1, 1, 1, 1),
(539, 49, 1, '23119', '961208642', 'MMAU 125925-9', 5, 'SEALAND PHILADELPHIA', '1710', 7, 2, '2017-06-25', '2017-06-25', '2017-06-30', '2017-08-03', 21190, 18900, 1, 2, 1, 1, 1, 1),
(540, 49, 1, '23118', '961208637', 'PONU 498186-2', 5, 'SEALAND PHILADELPHIA', '1710', 1, 2, '2017-06-24', '2017-06-24', '2017-06-30', '2017-07-23', 22320, 19591.2, 1, 2, 1, 1, 1, 1),
(541, 48, 2, '1', '98628647', 'HLBU 904294-8', 4, 'LISZT', '726', 3, 1, '2017-06-22', '2017-06-22', '2017-06-29', '2017-05-29', 22170, 19591.2, 1, 1, 1, 1, 1, 1),
(542, 48, 2, '2', '98628647', 'HLBU 904186-0', 4, 'LISZT', '729', 3, 1, '2017-06-22', '2017-06-22', '2017-06-29', '2017-05-29', NULL, 19591.2, 0, 1, 1, 1, 1, 1),
(543, 48, 2, '2', '98628647', 'HLBU 904186-0', 4, 'LISZT', '726', 3, 1, '2017-06-22', '2017-06-22', '2017-06-28', '2017-07-29', 22120, 19591.2, 1, 1, 1, 1, 1, 1),
(544, 48, 4, '2', '98653827', 'CPSU 511286-0', 4, 'LISZT', '726', 3, 2, '2017-06-23', '2017-06-20', '2017-06-28', '2017-07-28', 22270, 19591.2, 1, 1, 1, 1, 1, 1),
(545, 48, 4, '1', '98653827', 'LNXU 845859-8', 4, 'LISZT', '726', 3, 2, '2017-06-22', '2017-06-22', '2017-06-28', '2017-07-28', 22200, 19591.2, 1, 1, 1, 1, 1, 1),
(546, 48, 3, '2', 'B6JBK024927', 'DFIU 723082-1', 9, 'ALIOTH', '173', 4, 1, '2017-06-23', '2017-06-23', '2017-06-28', '2017-07-28', 19720, 17414.4, 1, 1, 1, 1, 1, 1),
(547, 48, 3, '1', 'LMM0216476', 'CAIU 558762-8', 2, 'LISZT', '020', 6, 1, '2017-06-23', '2017-06-23', '2017-06-28', '2017-07-28', 22160, 19591.2, 1, 1, 1, 1, 1, 1),
(548, 48, 5, '2', '7LIMAG1611', 'SUDU 810355-5', 3, 'LISZT', '726', 2, 2, '2017-06-25', '2017-06-25', '2017-06-28', '2017-07-28', 19730, 17414.4, 1, 1, 1, 1, 1, 1),
(549, 48, 5, '1', '7LIMAG1612', 'SUDU 803934-8', 3, 'LISZT', '726', 2, 2, '2017-06-24', '2017-06-24', '2017-06-28', '2017-07-28', 19690, 17414.4, 1, 1, 1, 1, 1, 1),
(550, 50, 1, '23147', '1', NULL, 8, 'ATLANTIC REEFER', '1', 3, 2, '2017-07-04', '2017-07-04', '2017-07-09', '2017-07-29', NULL, 19591.2, 0, 1, 1, 1, 1, 1),
(551, 50, 1, '23148', 'MBM140178162', 'CAIU541966-6', 3, 'ATLANTIC REEFER', 'RA17021EB', 3, 2, '2017-07-04', '2017-07-04', '2017-07-08', '2017-07-29', 22480, 19591.2, 1, 2, 1, 1, 1, 1),
(552, 50, 1, '23149', 'MBM140178162', 'CXRU 135069-4', 3, 'ATLANTIC REEFER', 'RA17021EB', 3, 2, '2017-07-04', '2017-07-04', '2017-07-08', '2017-07-27', 22330, 19591.2, 1, 2, 1, 1, 1, 1),
(553, 50, 1, '23150', '7LIMES3700', 'SUDU 804300-8', 3, 'HS LISZT', '728N', 3, 2, '2017-07-04', '2017-07-04', '2017-07-11', '2017-08-01', 22400, 19591.2, 1, 2, 1, 1, 1, 1),
(554, 50, 1, '23151', '7LIMES3700', 'SUDU 810435-6', 3, 'HS LISZT', '728N', 3, 2, '2017-07-05', '2017-07-05', '2017-07-11', '2017-08-01', 22330, 19591.2, 1, 2, 1, 1, 1, 1),
(555, 50, 1, '23152', '7LIMES3700', 'SUDU628863-4', 3, 'HS LISZT', '728N', 3, 2, '2017-07-06', '2017-07-06', '2017-07-11', '2017-08-01', 22250, 19591.2, 1, 2, 1, 1, 1, 1),
(556, 50, 1, '23153', '7LIMES3700', 'SUDU 818007-9', 3, 'HS LISZT', '728N', 3, 2, '2017-07-05', '2017-07-05', '2017-07-11', '2017-08-01', 22260, 19591.2, 1, 2, 1, 1, 1, 1),
(557, 50, 1, '23154', 'MSKPE0049', 'PONU481197-4', 8, 'SEALAND MANZANILLO', '1710', 1, 2, '2017-07-03', '2017-07-03', '2017-07-08', '2017-07-27', 22370, 19591.2, 1, 2, 1, 1, 1, 1),
(558, 50, 1, '23155', 'MSKPE0049', 'MWCU 685179-1', 5, 'SEALAND MANZANILLO', '1710', 1, 2, '2017-07-04', '2017-07-04', '2017-07-08', '2017-07-27', 22330, 19591.2, 1, 2, 1, 1, 1, 1),
(559, 50, 1, '23156', 'MSKPE0049', 'MWCU670752-6', 5, 'SEALAND MANZANILLO', 'RA1710EB', 1, 2, '2017-07-01', '2017-07-01', '2017-07-08', '2017-07-27', 22340, 19591.2, 1, 2, 1, 1, 1, 1),
(560, 50, 1, '23157', 'MSKPE 0044', 'MMAU 113307-0', 5, 'SEALAND MANZANILLO', '1710', 7, 2, '2017-07-01', '2017-07-01', '2017-07-08', '2017-07-27', 21510, 18900, 1, 2, 1, 1, 1, 1),
(561, 49, 2, '1', '98660535', 'HLBU 904194-1', 4, 'HS LISZT', '727N', 3, 1, '2017-06-27', '2017-06-27', '2017-07-04', '2017-07-28', 22180, 19591.2, 1, 2, 1, 1, 1, 1),
(562, 49, 2, '2', '98660535', 'HLBU 900822-3', 4, 'HS LISZT', '727N', 3, 1, '2017-06-28', '2017-06-28', '2017-07-04', '2017-07-28', 22260, 19591.2, 1, 2, 1, 1, 1, 1),
(563, 49, 4, '2', '98673354', 'BMOU 977317-4', 4, 'HS LISZT', '727N', 3, 2, '2017-06-30', '2017-06-30', '2017-07-06', '1901-07-31', 22030, 19591.2, 1, 2, 1, 1, 1, 1),
(564, 49, 4, '1', '98673354', 'GESU 950668-0', 4, 'HS LISZT', '727N', 3, 2, '2017-06-28', '2017-06-28', '2017-07-05', '1901-07-30', 22090, 19591.2, 1, 2, 1, 1, 1, 1),
(565, 49, 3, '2', 'B6JBK024982', 'DFIU 723001-4', 9, 'JAMILA', '054', 4, 1, '2017-06-30', '2017-06-30', '2017-07-07', '2017-07-31', 19710, 17414.4, 1, 2, 1, 1, 1, 1),
(566, 49, 3, '1', 'LMM0216749', 'TLLU 105619-2', 2, 'HS LISZT', '021CNR', 6, 1, '2017-06-30', '2017-06-30', '2017-07-07', '2017-07-31', 22450, 19591.2, 1, 2, 1, 1, 1, 1),
(567, 49, 5, '1', '7LIMNC2137', 'SUDU 627430-6', 3, 'HS LISZT', '727N', 2, 2, '2017-06-28', '2017-06-28', '2017-07-04', '2017-07-31', 19630, 17414.4, 1, 2, 1, 1, 1, 1),
(568, 49, 5, '1', '7LIMNC2137', 'SUDU 627430-6', 3, 'LISZT', '727', 2, 2, '2017-06-28', '2017-06-28', '2017-07-04', '2017-07-31', NULL, 17414.4, 0, 2, 1, 1, 1, 1),
(569, 51, 1, '23271', 'MBM140178164', 'CAIU 565696-6', 8, 'LOMBOK STRAIT', 'RA17022EB', 3, 2, '2017-07-08', '2017-07-08', '2017-07-16', '2017-08-03', 22290, 19591.2, 1, 2, 1, 1, 1, 1),
(570, 51, 1, '23272', 'MBM140178164', 'BMOU 965124-2', 8, 'LOMBOK STRAIT', 'RA17022EB', 3, 2, '2017-07-10', '2017-07-10', '2017-07-16', '2017-08-03', 22380, 19591.2, 1, 2, 1, 1, 1, 1),
(571, 51, 1, '23274', '7LIMES3924', 'SUDU 817614-5', 3, 'HS LISZT', '729N', 3, 2, '2017-07-10', '2017-07-10', '2017-07-18', '2017-08-08', 22410, 19591.2, 1, 2, 1, 1, 1, 1),
(572, 51, 1, '23273', '7LIMES3924', 'SUDU 803304-1', 3, 'HS LISZT', '729N', 3, 2, '2017-07-11', '2017-07-11', '2017-07-18', '2017-08-08', 22300, 19591.2, 1, 2, 1, 1, 1, 1),
(573, 51, 1, '23275', '7LIMES3924', 'SUDU 821453-8', 3, 'HS LISZT', '729N', 3, 2, '2017-07-10', '2017-07-10', '2017-07-18', '2017-08-08', 22300, 19591.2, 1, 2, 1, 1, 1, 1),
(574, 51, 1, '23276', '7LIMES3924', 'SUDU 806707-8', 3, 'HS LISZT', '729N', 3, 2, '2017-07-13', '2017-07-13', '2017-07-18', '2017-08-08', 22260, 19591.2, 1, 2, 1, 1, 1, 1),
(575, 51, 1, '23277', '961322284', 'MWCU 676530-6', 5, 'SEALAND GUAYAQUIL', '1712', 1, 2, '2017-07-08', '2017-07-08', '2017-07-14', '2017-08-03', 22370, 19591.2, 1, 2, 1, 1, 1, 1),
(576, 51, 1, '23278', '961322284', 'MSWU 900991-5', 5, 'SEALAND GUAYAQUIL', '1712', 1, 2, '2017-07-07', '2017-07-07', '2017-07-14', '2017-08-03', 22320, 19591.2, 1, 2, 1, 1, 1, 1),
(577, 51, 1, '23279', '961322284', 'MWCU 683473-1', 5, 'SEALAND GUAYAQUIL', '1712', 1, 2, '2017-07-08', '2017-07-08', '2017-07-14', '2017-08-03', 22220, 19591.2, 1, 2, 1, 1, 1, 1),
(578, 51, 1, '23280', '961312563', 'MMAU 106056-5', 5, 'SEALAND GUAYAQUIL', '1712', 7, 2, '2017-07-08', '2017-07-08', '2017-07-14', '2017-08-17', 21290, 18200, 1, 2, 1, 1, 1, 1),
(579, 50, 1, '23228', '7LIMES3700', 'SUDU 803239-0', 3, 'LISZT', '728', 3, 2, '2017-07-04', '2017-07-04', '2017-07-12', '2017-08-08', NULL, 19591.2, 0, 1, 1, 1, 1, 1),
(580, 50, 1, '23227', 'MSKPE0049', 'MWCU 524004-1', 8, 'SEALAND MANZANILLO', 'RA1710EB', 1, 2, '2017-07-03', '2017-07-03', '2017-07-08', '2017-08-08', 22330, 19591.2, 1, 2, 1, 1, 1, 1),
(581, 50, 1, '23228', '7LIMES3700', 'SUDU 803239-0', 3, 'HS LISZT', '728N', 3, 2, '2017-07-04', '2017-07-04', '2017-07-11', '2017-08-08', 22310, 19591.2, 1, 2, 1, 1, 1, 1),
(582, 50, 2, '1', '98685462', 'HLBU 900891-7', 4, 'HS LISZT', '728N', 3, 1, '2017-07-05', '2017-07-05', '2017-07-10', '2017-08-08', 22160, 19591.2, 1, 2, 1, 1, 3, 1),
(583, 50, 2, '2', '98685462', 'HLBU 904065-2', 4, 'HS LISZT', '728N', 3, 1, '2017-07-06', '2017-07-06', '2017-07-10', '2017-08-08', 22110, 19591.2, 1, 2, 1, 1, 1, 1),
(584, 50, 2, '3', '98685462', 'HLBU904522-7', 4, 'HS LISZT', '728N', 3, 1, '2017-07-06', '2017-07-06', '2017-07-10', '2017-08-08', 22190, 19591.2, 1, 2, 1, 1, 1, 1),
(585, 50, 4, '1', '98711299', 'LNXU 755981-3', 4, 'HS LISZT', '728N', 3, 2, '2017-07-06', '2017-07-06', '2017-07-11', '2017-08-07', 22270, 19591.2, 1, 2, 1, 1, 1, 1),
(586, 50, 4, '2', '98711299', 'BMOU 921454-0', 4, 'HS LISZT', '728N', 3, 2, '2017-07-06', '2017-07-06', '2017-07-11', '2017-08-07', 22100, 19591.2, 1, 2, 1, 1, 1, 1),
(587, 50, 5, '1', '7LIMAG1744', 'SUDU 804227-5', 3, 'HS LISZT', '728N', 2, 2, '2017-07-06', '2017-07-06', '2017-07-11', '2017-08-06', 19580, 17414.4, 1, 2, 1, 1, 1, 1),
(588, 50, 3, '1', 'LMM0217108', 'TLLU 106743-2', 2, 'HS LISZT', '022CNR', 6, 1, '2017-07-07', '2017-07-07', '2017-07-12', '2017-08-07', 22240, 19591.2, 1, 2, 1, 2, 1, 1),
(589, 50, 5, '2', '7LIMAG1745', 'SUDU 811934-0', 3, 'HS LISZT', '728N', 2, 1, '2017-07-07', '2017-07-07', '2017-07-11', '2017-08-07', 19700, 17414.4, 1, 2, 1, 1, 1, 1),
(590, 52, 1, '23351', 'MBM140178166', 'TTNU 845055-3', 8, 'BALTIC KLIPPER', 'RA17023EB', 3, 15, '2017-07-15', '2017-07-15', '2017-07-23', '2017-08-10', 22200, 19591.2, 1, 2, 1, 1, 1, 1),
(591, 52, 1, '23352', 'MBM140178166', 'CXRU 152421-9', 8, 'BALTIC KLIPPER', 'RA17023EB', 3, 15, '2017-07-15', '2017-07-15', '2017-07-23', '2017-08-10', 22170, 19591.2, 1, 2, 1, 1, 1, 1),
(592, 52, 1, '23355', '7LIMES4167', 'SUDU 624583-8', 3, 'HS LISZT', '730N', 3, 15, '2017-07-15', '2017-07-15', '2017-07-25', '2017-08-15', 22260, 19591.2, 1, 2, 1, 1, 1, 1),
(593, 52, 1, '23353', '7LIMES4167', 'SUDU 804642-9', 3, 'HS LISZT', '730N', 3, 15, '2017-07-17', '2017-07-17', '2017-07-25', '2017-08-15', 22260, 19591.2, 1, 2, 1, 1, 1, 1),
(594, 52, 1, '23356', '7LIMES4167', 'SUDU 628546-6', 3, 'LISZT', '730', 3, 15, '2017-07-17', '2017-07-17', '2017-07-25', '2017-08-15', NULL, 19591.2, 1, 2, 1, 1, 1, 1),
(595, 52, 1, '23357', '7LIMES4167', 'SUDU 803258-0', 3, 'HS LISZT', '730N', 3, 15, '2017-07-18', '2017-07-18', '2017-07-25', '2017-08-15', 22260, 19591.2, 1, 2, 1, 1, 1, 1),
(596, 52, 1, '23354', '7LIMES4167', 'SUDU 627879-1', 3, 'HS LISZT', '730N', 3, 15, '2017-07-18', '2017-07-18', '2017-07-25', '2017-08-15', 22230, 19591.2, 1, 2, 1, 1, 1, 1),
(597, 52, 1, '23350', '961415954', 'MWCU 530573-3', 5, 'SEALAND LOS ANGELES', '1712', 1, 15, '2017-07-18', '2017-07-18', '2017-07-21', '2017-08-13', 22210, 19591.2, 1, 2, 1, 1, 1, 1),
(598, 52, 1, '23349', '961415954', 'PONU 489353-0', 5, 'SEALAND LOS ANGELES', '1712', 1, 15, '2017-07-14', '2017-07-14', '2017-07-21', '2017-08-13', 22250, 19591.2, 1, 2, 1, 1, 1, 1),
(599, 52, 1, '23348', '961415954', 'PONU 489829-6', 5, 'SEALAND LOS ANGELES', '1712', 1, 15, '2017-07-14', '2017-07-14', '2017-07-21', '2017-08-13', 22280, 19591.2, 1, 2, 1, 1, 1, 1),
(600, 52, 1, '23347', '961406893', 'MMAU 110134-5', 5, 'SEALAND LOS ANGELES', '1712', 7, 15, '2017-07-15', '2017-07-15', '2017-07-21', '2017-08-24', 21170, 18200, 1, 2, 1, 1, 1, 1),
(601, 51, 1, '23317', '7LIMES3924', 'SUDU 628821-2', 3, 'HS LISZT', '729N', 3, 2, '2017-07-11', '2017-07-11', '2017-07-18', '2017-08-08', 22320, 19591.2, 1, 2, 1, 1, 1, 1),
(602, 51, 2, '2', '98724406', 'HLBU 907720-3', 4, 'HS LISZT', '729N', 3, 1, '2017-07-12', '2017-07-12', '2017-07-18', '2017-08-18', 22170, 19591.2, 1, 2, 1, 1, 1, 1),
(603, 51, 2, '1', '98724406', 'HLBU 907719-0', 4, 'HS LISZT', '729N', 3, 1, '2017-07-11', '2017-07-11', '2017-07-18', '2017-08-18', 22200, 19591.2, 1, 2, 1, 1, 1, 1),
(604, 51, 4, '2', '98744987', 'TCLU 102065-3', 4, 'HS LISZT', '729N', 3, 2, '2017-07-12', '2017-07-12', '2017-07-18', '2017-07-18', 22290, 19591.2, 1, 2, 1, 1, 1, 1),
(605, 51, 4, '1', '98744987', 'TCLU 118219-2', 4, 'HS LISZT', '729N', 3, 2, '2017-07-12', '2017-07-12', '2017-07-18', '2017-07-18', 22170, 19591.2, 1, 2, 1, 1, 1, 1),
(606, 51, 5, '1', '7LIMAG1816', 'SUDU 820004-6', 3, 'HS LISZT', '729N', 2, 2, '2017-07-13', '2017-07-13', '2017-07-18', '2017-08-17', 19710, 17414.4, 1, 2, 1, 1, 1, 1),
(607, 51, 3, '1', 'LMM0217538', 'TLLU 105065-6', 2, 'HS LISZT', '023CNR', 6, 1, '2017-07-13', '2017-07-13', '2017-07-17', '2017-08-17', 22290, 19591.2, 1, 2, 1, 1, 1, 1),
(608, 51, 3, '2', 'B6JBK025074', 'DFIU 811154-0', 9, 'JAMILA', '055', 4, 1, '2017-07-14', '2017-07-14', '2017-07-17', '2017-08-17', 19800, 17414.4, 1, 2, 1, 1, 1, 1),
(609, 53, 1, '23415', 'MBM140178168', 'BMOU 965241-8', 8, 'ATLANTIC KLIPPER', 'RA17024EB', 3, 15, '2017-07-25', '2017-07-25', '2017-07-30', '2017-08-17', 22270, 19591.2, 1, 2, 2, 2, 1, 1);
INSERT INTO `contenedor` (`id`, `id_semana`, `id_cliente`, `referencia`, `booking`, `numero_contenedor`, `id_lineanaviera`, `nave`, `viaje`, `id_puertodestino`, `id_operador`, `fecha_proceso_inicio`, `fecha_proceso_fin`, `fecha_zarpe`, `fecha_llegada`, `peso_bruto`, `peso_neto`, `activo`, `factura`, `certificado`, `packing`, `valija`, `vl`) VALUES
(610, 53, 1, '23414', 'BMB140178168', 'BMOU 961803-3', 8, 'ATLANTIC KLIPPER', 'RA17024EB', 3, 15, '2017-07-24', '2017-07-24', '2017-07-30', '2017-08-17', 22160, 19591.2, 1, 2, 2, 2, 1, 1),
(611, 53, 1, '23417', '7LIMES4465', 'SUDU 812206-7', 3, 'HS LISZT', '731N', 3, 15, '2017-07-26', '2017-07-26', '2017-08-01', '2017-08-22', 22300, 19591.2, 1, 2, 2, 2, 1, 1),
(612, 53, 1, '23418', '7LIMES4465', 'SUDU 813990-1', 3, 'HS LISZT', '731N', 3, 15, '2017-07-26', '2017-07-26', '2017-08-01', '2017-08-22', 22300, 19591.2, 1, 2, 2, 2, 1, 1),
(613, 53, 1, '23416', '7LIMES4465', 'SUDU 610980-5', 3, 'HS LISZT', '731N', 3, 15, '2017-07-26', '2017-07-26', '2017-08-01', '2017-08-22', 22270, 19591.2, 1, 2, 2, 2, 1, 1),
(614, 53, 1, '23419', '7LIMES 4465', 'SUDU 824675-1', 3, 'HSLISZT', '731N', 3, 15, '2017-07-25', '2017-07-26', '2017-08-01', '2017-08-22', 22250, 19591.2, 1, 2, 2, 2, 1, 1),
(615, 53, 1, '23420', '1', NULL, 5, 'SEALAND BALBOA', '1', 1, 15, '2017-07-23', '2017-07-23', '2017-07-28', '2017-08-20', NULL, 19591.2, 0, 1, 1, 2, 1, 1),
(616, 53, 1, '23420', '1', NULL, 5, 'SEALAND BALBOA', '1', 1, 15, '2017-07-23', '2017-07-23', '2017-07-28', '2017-08-20', NULL, 19591.2, 0, 1, 1, 2, 1, 1),
(617, 53, 1, '23424', '961501375', 'MMAU 115229-7', 5, 'SEALAND BALBOA', '1712', 7, 15, '2017-07-24', '2017-07-24', '2017-07-28', '2017-08-31', 21250, 18200, 1, 2, 2, 2, 1, 1),
(618, 53, 1, '23423', '961491863', 'PONU 450426-3', 5, 'SEALAND BALBOA', '1712', 1, 15, '2017-07-22', '2017-07-22', '2017-07-28', '2017-08-20', 22330, 19591.2, 1, 2, 2, 2, 1, 1),
(619, 53, 1, '23422', '961491863', 'MNBU 307774-0', 5, 'SEALAND BALBOA', '1712', 1, 15, '2017-07-22', '2017-07-22', '2017-07-28', '2017-08-20', 22240, 19591.2, 1, 2, 2, 2, 1, 1),
(620, 53, 1, '23421', '961491863', 'PONU 489371-4', 5, 'SEALAND BALBOA', '1712', 1, 15, '2017-07-22', '2017-07-22', '2017-07-28', '2017-08-20', 22270, 19591.2, 1, 2, 2, 2, 1, 1),
(621, 53, 1, '23420', '961491863', 'MNBU 339827-3', 5, 'SEALAND BALBOA', '1712', 1, 15, '2017-07-24', '2017-07-24', '2017-07-28', '2017-08-20', 22320, 19591.2, 1, 2, 2, 2, 1, 1),
(622, 52, 2, '1', '98757055', 'HLBU 904788-9', 4, 'HS LISZT', '730N', 3, 1, '2017-07-19', '2017-07-19', '2017-07-25', '2017-08-25', 22070, 19591.2, 1, 2, 1, 1, 1, 1),
(623, 52, 2, '2', '98757055', 'HLBU 907963-3', 4, 'HS LISZT', '730N', 3, 1, '2017-07-19', '2017-07-19', '2017-07-25', '2017-08-25', 22190, 19591.2, 1, 2, 1, 1, 1, 1),
(624, 52, 2, '3', '98757055', 'HLBU 907171-4', 4, 'HS LISZT', '730N', 3, 1, '2017-07-19', '2017-07-19', '2017-07-25', '2017-08-25', 22180, 19591.2, 1, 2, 1, 1, 1, 1),
(625, 52, 4, '1', '98780813', 'CRLU 183918-7', 4, 'HS LISZT', '730N', 3, 15, '2017-07-20', '2017-07-20', '2017-07-24', '2017-08-19', 22220, 19591.2, 1, 2, 1, 1, 1, 1),
(626, 52, 4, '2', '98780813', 'GESU 950621-0', 4, 'HS LISZT', '730N', 3, 15, '2017-07-20', '2017-07-20', '2017-07-24', '2017-08-19', 22190, 19591.2, 1, 2, 1, 1, 1, 1),
(627, 52, 5, '1', '7LIMNC2448', 'SUDU 807093-4', 3, 'HS LISZT', '730N', 2, 1, '2017-07-21', '2017-07-21', '2017-07-29', '2017-08-12', 19660, 17414.4, 1, 2, 1, 1, 1, 1),
(628, 52, 3, '1', 'LMM0217944', 'TLLU 104530-4', 2, 'HS LISZT', '024', 6, 1, '2017-07-20', '2017-07-20', '2017-07-25', '2017-08-19', 22150, 19591.2, 1, 2, 1, 1, 1, 1),
(629, 52, 3, '2', 'B6JBK025114', 'DFIU 722128-6', 9, 'ALIOTH', '175', 4, 1, '2017-07-21', '2017-07-21', '2017-07-25', '2017-08-19', 19770, 17414.4, 1, 2, 1, 1, 1, 1),
(630, 53, 2, '1', '98795687', 'HLBU 900879-5', 4, 'HS LISZT', '731N', 3, 1, '2017-07-27', '2017-07-27', '2017-08-31', '2017-08-12', 22220, 19591.2, 1, 2, 1, 2, 1, 1),
(631, 53, 2, '2', '98795687', 'HLXU 877281-0', 4, 'HS LISZT', '025', 3, 1, '2017-07-27', '2017-07-27', '2017-08-31', '2017-08-12', 22250, 19591.2, 1, 2, 1, 2, 1, 1),
(632, 53, 3, '1', 'LMM0218237', 'CAIU 565452-0', 2, 'CGM HS LISZT', '025', 6, 1, '2017-07-30', '2017-07-30', '2017-07-31', '2017-08-18', 22390, 19591.2, 1, 2, 1, 2, 1, 1),
(633, 53, 4, '1', '98813622', 'FSCU 567276-9', 4, 'HS LISZT', '731N', 3, 15, '2017-07-27', '2017-07-27', '2017-07-31', '2017-08-18', 22140, 19591.2, 1, 2, 1, 2, 1, 1),
(634, 53, 4, '1', '98813622', 'LNXU 965478-5', 4, 'HS LISZT', '731N', 3, 15, '2017-07-30', '2017-07-30', '2017-07-31', '2017-08-18', 22190, 19591.2, 1, 2, 1, 2, 1, 1),
(635, 53, 5, '1', '7LIMES2520', 'SUDU 813474-6', 3, 'HS LISZT', '731N', 2, 1, '2017-07-30', '2017-07-30', '2017-07-31', '2017-08-22', 19750, 17414.4, 1, 2, 1, 2, 1, 1),
(636, 53, 5, '1', '1', NULL, 3, '1', '1', 2, 1, '2017-07-26', '2017-07-26', '2017-07-31', '2017-08-22', NULL, 17414.4, 0, 1, 1, 2, 1, 1),
(637, 54, 1, '23510', '7LIMES46213', 'SUDU 808059-4', 3, 'HS LISZT', '732N', 3, 15, '2017-08-03', '2017-08-03', '2017-08-16', '2017-08-29', 22220, 19591.2, 1, 1, 1, 3, 1, 1),
(638, 54, 1, '23512', '7LIMES4613', 'SUDU 823497-7', 3, 'HSD HS LISZT', '732N', 3, 15, '2017-08-02', '2017-08-02', '2017-08-16', '2017-08-29', 22290, 19591.2, 1, 1, 1, 3, 1, 1),
(639, 54, 1, '23513', '7LIMES4613', 'SUDU 821297-8', 3, 'HSD HS LISZT', '732N', 3, 15, '2017-08-02', '2017-08-02', '2017-08-16', '2017-08-29', 22270, 19591.2, 1, 1, 1, 3, 1, 1),
(640, 54, 1, '23511', 'LIMES4613', 'SUDU 822107-5', 3, 'HS LISZT', '732N', 3, 15, '2017-08-03', '2017-08-03', '2017-08-16', '2017-08-29', 22350, 19591.2, 1, 1, 1, 3, 1, 1),
(641, 54, 1, '23517', '961586587', 'MWCU 663807-1', 5, 'MARINER', '1712', 1, 15, '2017-08-01', '2017-08-01', '2017-08-04', '2017-08-28', 22470, 19591.2, 1, 1, 1, 3, 1, 1),
(642, 54, 1, '23515', '961586587', 'PONU 497285-5', 5, 'MARINER', '1712', 1, 15, '2017-07-31', '2017-07-31', '2017-08-04', '2017-08-28', 22370, 19591.2, 1, 1, 1, 3, 1, 1),
(643, 54, 1, '23516', '961586587', 'MWMU 633834-6', 5, 'MARINER', '1712', 1, 15, '2017-07-31', '2017-07-31', '2017-08-04', '2017-08-28', 22110, 19591.2, 1, 1, 1, 3, 1, 1),
(644, 54, 1, '23508', '961586595', 'MNBU 309738-8', 5, 'MARINER', '1712', 3, 15, '2017-08-02', '2017-08-02', '2017-08-04', '2017-08-24', 22280, 19591.2, 1, 1, 1, 3, 1, 1),
(645, 54, 1, '23519', '961586595', 'MMAU 121545-6', 5, 'MARINER', '1712', 7, 15, '2017-08-01', '2017-08-01', '2017-08-04', '2017-09-07', 21310, 18200, 1, 1, 1, 3, 1, 1),
(646, 54, 1, '23509', 'MBM140178172', 'CXRU 111430-1', 8, 'LUZON STRAIT', 'RA17025EB', 3, 15, '2017-08-02', '2017-08-02', '2017-08-06', '2017-08-21', 22280, 19591.2, 1, 1, 1, 3, 1, 1),
(647, 54, 1, '23518', 'MBM140178173', 'CAIU 542117-5', 8, 'LUZON STRAIT', 'RA17025EB', 1, 15, '2017-08-01', '2017-08-01', '2017-08-06', '2017-08-21', 22420, 19591.2, 1, 1, 1, 3, 1, 1),
(648, 54, 5, '1', '7LIMES2594', 'SUDU 823041-5', 3, 'HS  LISZT', '732N', 2, 1, '2017-08-06', '2017-08-06', '2017-08-16', '2017-08-29', 19640, 17414.4, 1, 1, 1, 2, 1, 1),
(649, 54, 5, '1', '7LIMES2594', 'SUDU 808279-2', 3, 'LISZT', '732N', 2, 1, '2017-08-06', '2017-08-06', '2017-08-16', '2017-08-29', 19610, 17414.4, 1, 1, 1, 2, 1, 1),
(650, 54, 3, '1', 'LMM0218618', 'TLLU 105275-1', 2, 'HS LISZT', '026CNR', 6, 1, '2017-08-05', '2017-08-05', '2017-08-16', '2017-08-29', 22230, 19591.2, 1, 1, 1, 2, 1, 1),
(651, 54, 2, '1', '98828248', 'HLXU 876069-7', 4, 'HS LISZT', '732N', 3, 1, '2017-08-03', '2017-08-03', '2017-08-16', '2017-08-29', 22230, 19591.2, 1, 1, 1, 3, 1, 1),
(652, 54, 2, '1', '98828248', 'HLXU 875896-1', 4, 'HS LISZT', '732N', 3, 1, '2017-08-03', '2017-08-03', '2017-08-16', '2017-08-29', 22120, 19591.2, 1, 1, 1, 3, 1, 1),
(653, 54, 2, '1', '98828248', 'HLXU 876094-8', 4, 'HS LISZT', '732N', 3, 1, '2017-08-06', '2017-08-06', '2017-08-16', '2017-08-29', 22220, 19591.2, 1, 1, 1, 3, 1, 1),
(654, 54, 4, '1', '98862199', 'CRLU 127214-9', 4, 'HS  LISZT', '732N', 3, 15, '2017-08-05', '2017-08-05', '2017-08-16', '2017-08-29', 22110, 19591.2, 1, 1, 1, 3, 1, 1),
(655, 54, 4, '1', '98862199', 'HLXU 875124-7', 4, 'HS LISZT', '732N', 3, 15, '2017-08-05', '2017-08-05', '2017-08-16', '2017-08-29', 22170, 19591.2, 1, 1, 1, 3, 1, 1),
(656, 55, 1, '23683', 'MBM140178175', 'CXRU 111139-1', 8, 'SCHWEIZ REEFER', '1702EB', 3, 15, '2017-08-08', '2017-08-08', '2017-08-13', '2017-08-31', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(657, 55, 1, '23684', 'MBM140178175', 'TCLU 138554-3', 8, 'SCHWEIZ', '17036EB', 3, 15, '2017-08-08', '2017-08-08', '2017-08-13', '2017-08-31', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(658, 55, 1, '23685', '7LIMES4765', 'CNIU 223155-9', 3, 'HS LIZET', '733', 3, 15, '2017-08-09', '2017-08-09', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(659, 55, 1, '23686', '7LIMES4765', 'SUDU 6282717-4', 3, 'HS LIZET', '733', 3, 15, '2017-08-08', '2017-08-08', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(660, 55, 1, '23687', '7LIMES4765', 'SUDU 812231-8', 3, 'HS  LISET', '733', 3, 15, '2017-08-09', '2017-08-09', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(661, 55, 1, '23688', '7LIMES4765', 'SUDU 804382-0', 3, 'HS LIZET', '733', 3, 15, '2017-08-09', '2017-08-09', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(662, 55, 1, '23696', '7LIMES4765', 'SUDU 610246-2', 3, 'HS LIZET', '733', 3, 15, '2017-08-10', '2017-08-10', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(663, 55, 1, '23737', '7LIMES4765', 'SUDU 511341-5', 3, 'HS  LIZET', '733', 3, 15, '2017-08-12', '2017-08-12', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(664, 55, 1, '23689', '961685496', 'PONU 480451-1', 5, 'SEALAND PHILADELPHIA', '1712', 1, 15, '2017-08-10', '2017-08-10', '2017-08-11', '2017-09-03', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(665, 55, 1, '23690', '961685496', 'MWCU 530648-9', 5, 'SEALAND PHILADELPHIA', '1712', 1, 15, '2017-08-07', '2017-08-07', '2017-08-11', '2017-09-03', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(666, 55, 1, '23691', '961685496', 'MWCU 532834-3', 5, 'SEALAND PHILADELPHIA', '1712', 1, 15, '2017-08-07', '2017-08-07', '2017-08-11', '2017-09-03', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(667, 55, 1, '23692', '961694748', 'MMAU 112342-6', 5, 'SEALAND PHILADELPHIA', '1712', 7, 15, '2017-08-08', '2017-08-08', '2017-08-11', '2017-09-14', NULL, 18200, 1, 1, 1, 2, 1, 1),
(668, 55, 5, '1', '7LIMES2717', 'SUDU 310908-6', 3, 'HS  LIZET', '733', 2, 1, '2017-08-12', '2017-08-12', '2017-08-15', '2017-08-25', NULL, 17414.4, 1, 1, 1, 2, 1, 1),
(669, 55, 5, '1', '7LIMES2717', 'SUDU 602390-7', 3, 'HS LIZET', '733', 2, 1, '2017-08-12', '2017-08-12', '2017-08-15', '2017-08-26', NULL, 17414.4, 1, 1, 1, 2, 1, 1),
(670, 55, 3, '1', 'LMM0219002', 'APRU 614231-2', 2, 'HS  LIZET', '027', 24, 1, '2017-08-11', '2017-08-11', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(671, 55, 4, '1', '98895432', 'LNXU 755955-7', 4, 'HS LIZET', '733', 3, 15, '2017-08-11', '2017-08-11', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(672, 55, 4, '1', '98895432', 'SEGU 937696-0', 4, 'HS LIZET', '733', 3, 15, '2017-08-10', '2017-08-10', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(673, 55, 2, '1', '98877086', 'HLBU 900930-1', 4, 'HS LIZET', '733', 3, 1, '2017-08-10', '2017-08-10', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(674, 55, 2, '1', '98877086', 'HLBU 900318-1', 4, 'HS  LIZET', '733', 3, 1, '2017-08-10', '2017-08-10', '2017-08-15', '2017-09-05', NULL, 19591.2, 1, 1, 1, 2, 1, 1),
(675, 56, 1, '23752', '961757441', 'MNBU 355141-7', 5, 'SEALAND MANZANILLO', '1712', 1, 15, '2017-08-14', '2017-08-14', '2017-08-18', '2017-09-10', 22320, 19591.2, 1, 2, 1, 2, 1, 1),
(676, 56, 1, '23753', '961757441', 'MNBU 901425-1', 5, 'SEALAND MANZANILLO', '1712', 1, 15, '2017-08-14', '2017-08-14', '2017-08-18', '2017-09-09', 22150, 19591.2, 1, 2, 1, 2, 1, 1),
(677, 56, 1, '23750', '461757441', 'PONU 484667-2', 5, 'SEALAND MANZANILLO', '1712', 1, 15, '2017-08-14', '2017-08-14', '2017-08-18', '2017-09-10', 22260, 19591.2, 1, 2, 1, 2, 1, 1),
(678, 56, 1, '23751', '961757441', 'PONU 451898-7', 5, 'SEALAND MANZANILLO', '1712', 1, 15, '2017-08-15', '2017-08-15', '2017-08-18', '2017-09-10', 22330, 19591.2, 1, 2, 1, 2, 1, 1),
(679, 56, 1, '23746', '7LIMES4914', 'SUDU 601834-6', 3, 'HS LISZT', '734N', 3, 15, '2017-08-15', '2017-08-15', '2017-08-22', '2017-09-12', 22270, 19591.2, 1, 2, 1, 2, 1, 1),
(680, 56, 1, '23754', '961748563', 'MMAU 110979-4', 5, 'SEALAND MANZANILLO', '1712', 7, 15, '2017-08-15', '2017-08-15', '2017-08-18', '2017-09-10', 21290, 18200, 1, 2, 1, 2, 1, 1),
(681, 56, 1, '23744', 'MBM140178176', 'CXRU 127908-7', 8, 'ATLANTIC REEFER', 'RA17027EB', 3, 15, '2017-08-16', '2017-08-16', '2017-08-20', '2017-09-07', 22300, 19591.2, 1, 2, 1, 2, 1, 1),
(682, 56, 1, '23745', 'MBM140178176', 'RRSU 170259-5', 8, 'ATLANTIC REEFER', 'RA17027EB', 3, 15, '2017-08-16', '2017-08-16', '2017-08-20', '2017-09-07', 22320, 19591.2, 1, 2, 1, 2, 1, 1),
(683, 56, 1, '23717', '7LIMES 4914', 'SUDU 514081-1', 3, 'HS LISZT', '734N', 3, 15, '2017-08-16', '2017-08-16', '2017-08-22', '2017-09-12', 22340, 19591.2, 1, 2, 1, 2, 1, 1),
(684, 56, 1, '23748', '7LIMES4914', 'SUDU 809810-3', 3, 'HS  LISZT', '734N', 3, 15, '2017-08-16', '2017-09-12', '2017-08-22', '2017-08-22', 22330, 19591.2, 1, 2, 1, 2, 1, 1),
(685, 56, 2, '1', '98917987', 'HLBU 900576-0', 4, 'HS LISZT', '734N', 3, 1, '2017-08-17', '2017-08-17', '2017-08-22', '2017-09-12', 22180, 19591.2, 1, 2, 1, 1, 1, 1),
(686, 56, 2, '1', '98917987', 'HLXU 876210-7', 4, 'HS LISZT', '734N', 3, 1, '2017-08-17', '2017-08-17', '2017-08-22', '2017-09-12', 22210, 19591.2, 1, 2, 1, 1, 1, 1),
(687, 56, 4, '1', '98933870', 'TRIU 881644-1', 4, 'HS LISZT', '734N', 3, 15, '2017-08-17', '2017-08-17', '2017-08-22', '2017-09-12', 22210, 19591.2, 1, 2, 1, 1, 1, 1),
(688, 56, 4, '1', '98933870', 'TRLU 166544-0', 4, 'HS LISZT', '734N', 3, 15, '2017-08-17', '2017-08-17', '2017-08-22', '2017-09-12', 22210, 19591.2, 1, 2, 1, 1, 1, 1),
(689, 56, 1, '23749', '7LIMES4914', 'SUDU 801041-0', 3, 'HS LISZT', '734N', 3, 15, '2017-08-18', '2017-08-18', '2017-08-22', '2017-09-12', 22140, 19591.2, 1, 2, 1, 2, 1, 1),
(690, 56, 3, '1', 'B6JBK025257', 'DFIU 812177-0', 9, 'ALIOTH', '177', 4, 1, '2017-08-18', '2017-08-22', '2017-08-23', '2017-09-04', 19700, 17414.4, 1, 2, 1, 1, 1, 1),
(691, 56, 3, '1', 'LMM0219369', 'TRIU 865904-4', 2, 'HS LISZT', '028CNR', 24, 1, '2017-08-18', '2017-08-18', '2017-08-22', '2017-09-12', 22430, 19591.2, 1, 2, 1, 1, 1, 1),
(692, 56, 2, '1', '98917987', 'HLBU 900704-2', 4, 'HS LISZT', '734N', 3, 1, '2017-08-19', '2017-08-19', '2017-08-22', '2017-09-12', 22260, 19591.2, 1, 2, 1, 1, 1, 1),
(693, 56, 5, '1', '7LIMES2834', 'SUDU 602126-8', 3, 'HS LISZT', '734N', 2, 1, '2017-08-19', '2017-08-19', '2017-08-22', '2017-09-05', 19750, 17414.4, 1, 2, 1, 1, 1, 1),
(694, 56, 5, '1', '7LIMES2834', 'SUDU 605321-8', 3, 'HS  LISZT', '734N', 2, 1, '2017-08-19', '2017-08-19', '2017-08-22', '2017-09-05', 19740, 17414.4, 1, 2, 1, 1, 1, 1),
(695, 57, 1, '23839', 'MBM140178178', 'TGHU991684-0', 8, 'LOMBOT STRAIT', 'RA17028EB', 3, 15, '2017-08-22', '2017-08-22', '2017-09-27', '2017-09-14', 22290, 19591.2, 1, 2, 2, 2, 1, 1),
(696, 58, 1, '23963', 'MBM140178179', 'CXRU 111338-9', 8, 'BALTIC KLIPPER', '17029', 3, 15, '2017-08-29', '2017-08-29', '2017-09-03', '2017-09-21', 22300, 19591.2, 1, 2, 2, 2, 1, 1),
(697, 58, 1, '23966', 'MBM140178179', 'TRIU 859121-6', 8, 'BALTIC KLIPPER', '17029', 3, 15, '2017-08-29', '2017-08-29', '2017-09-03', '2017-09-21', 22320, 19591.2, 1, 2, 2, 2, 1, 1),
(698, 58, 1, '23967', 'MBM140178179', 'CAIU 542161-6', 8, 'BALTIC KLIPPER', '17029', 3, 15, '2017-08-29', '2017-08-29', '2017-09-03', '2017-09-21', 22380, 19591.2, 1, 2, 2, 2, 1, 1),
(699, 58, 1, '23968', '7LIMES5236', 'SUDU 627006-5', 3, 'HS LISZT', '736', 3, 15, '2017-09-01', '2017-09-01', '2017-09-05', '2017-09-26', 22260, 19591.2, 1, 2, 2, 2, 1, 1),
(700, 58, 1, '23969', '7LIMES5236', 'SUDU 628288-9', 3, 'HS LISZT', '736', 3, 15, '2017-09-02', '2017-09-02', '2017-09-05', '2017-09-26', 22290, 19591.2, 1, 2, 2, 2, 1, 1),
(701, 58, 1, '23970', '7LIMES5236', 'SUDU 615306-9', 3, 'HS LISZT', '736', 3, 15, '2017-08-31', '2017-08-31', '2017-09-05', '2017-09-26', 22360, 19591.2, 1, 2, 2, 2, 1, 1),
(702, 58, 1, '23971', '7LIMES5236', 'SUDU 621344-5', 3, 'HS LISZT', '736', 3, 15, '2017-08-31', '2017-08-31', '2017-09-05', '2017-09-26', 22410, 19591.2, 1, 2, 2, 2, 1, 1),
(703, 58, 1, '23972', '7LIMES5236', 'SUDU 608150-2', 3, 'HS LISZT', '736', 3, 15, '2017-08-31', '2017-08-31', '2017-09-05', '2017-09-26', 22430, 19591.2, 1, 2, 2, 2, 1, 1),
(704, 58, 1, '23980', '7LIMES5236', 'SUDU 608761-9', 3, 'HS LISZT', '736', 3, 15, '2017-08-31', '2017-08-31', '2017-09-05', '2017-09-26', 22290, 19591.2, 1, 2, 2, 2, 1, 1),
(705, 58, 1, '23973', '961975322', 'PONU 483751-5', 5, 'SEALAND  LOS  ANGELES', '1714', 1, 15, '2017-08-28', '2017-08-28', '2017-09-01', '2017-09-24', 22400, 19591.2, 1, 2, 1, 2, 1, 1),
(706, 58, 1, '23974', '961975322', 'MNBU 365450-2', 5, 'SEALAND  LOS  ANGELES', '1714', 1, 15, '2017-08-26', '2017-08-26', '2017-09-01', '2017-09-24', 22260, 19591.2, 1, 2, 1, 2, 1, 1),
(707, 58, 1, '23975', '961975322', 'MWCU 523233-9', 5, 'SEALAND LOS  ANGELES', '1714', 1, 15, '2017-08-28', '2017-08-28', '2017-09-01', '2017-09-24', 22330, 19591.2, 1, 2, 1, 2, 1, 1),
(708, 58, 1, '23976', '961975333', 'MMAU 111331-0', 5, 'SEALAND  LOS  ANGELES', '1714', 7, 15, '2017-08-28', '2017-08-28', '2017-09-01', '2017-09-24', 21440, 18200, 1, 2, 1, 2, 1, 1),
(709, 57, 1, '23849', '961828979', 'PONU 494585-0', 5, 'SEALAND GUAYAQUIL', '1714', 1, 15, '2017-08-21', '2017-08-21', '2017-08-25', '2017-09-17', 22450, 19591.2, 1, 2, 2, 2, 1, 1),
(710, 57, 1, '23850', '961828979', 'MNBU 331586-0', 5, 'SEALAND GUAYAQUIL', '1714', 1, 15, '2017-08-21', '2017-08-21', '2017-08-25', '2017-09-17', 22210, 19591.2, 1, 2, 2, 2, 1, 1),
(711, 57, 1, '23848', '961828979', 'MSWU 006006-9', 5, 'SEALAND GUAYAQUIL', '1714', 1, 15, '2017-08-21', '2017-08-21', '2017-08-25', '2017-09-17', 22260, 19591.2, 1, 2, 2, 2, 1, 1),
(712, 57, 1, '23840', 'MBM140178178', 'ATKU 411267-8', 8, 'LOMBOT STRAIT', 'RA17028EB', 3, 15, '2017-08-22', '2017-08-22', '2017-08-27', '2017-09-14', 22330, 19591.2, 1, 2, 2, 2, 1, 1),
(713, 57, 1, '23841', 'MBM140178178', 'SZLU 913378-0', 8, 'LOMBOT STRAIT', 'RA17028EB', 3, 15, '2017-08-22', '2017-08-22', '2017-08-27', '2017-09-14', 22160, 19591.2, 1, 2, 2, 2, 1, 1),
(714, 57, 1, '23842', '7LIMES5076', 'SUDU 612457-0', 3, 'HS LISZT', '735N', 3, 15, '2017-08-23', '2017-08-23', '2017-08-29', '2017-09-19', 22210, 19591.2, 1, 2, 2, 2, 1, 1),
(715, 57, 1, '23843', '7LIMES5076', 'SUDU 605195-6', 3, 'HS LISZT', '735N', 3, 15, '2017-08-23', '2017-08-23', '2017-08-29', '2017-09-19', 22280, 19591.2, 1, 2, 2, 2, 1, 1),
(716, 57, 1, '23847', '7LIMES5076', 'SUDU 819760-0', 3, 'HS LISZT', '735N', 3, 15, '2017-08-23', '2017-08-23', '2017-08-29', '2017-09-14', 22420, 19591.2, 1, 2, 2, 2, 1, 1),
(717, 57, 1, '23846', '7LIMES5076', 'SUDU 817353-1', 3, 'HS LISZT', '735N', 3, 15, '2017-08-23', '2017-08-23', '2017-08-29', '2017-09-14', 22380, 19591.2, 1, 2, 2, 2, 1, 1),
(718, 57, 2, '1', '98953711', 'CPSU 516640-3', 4, 'HS LISZT', '735N', 3, 1, '2017-08-24', '2017-08-24', '2017-08-29', '2017-09-14', 22290, 19591.2, 1, 2, 2, 3, 1, 1),
(719, 57, 2, '1', '98953711', 'HLXU 673459-6', 4, 'HS LISZT', '735N', 3, 1, '2017-08-24', '2017-08-24', '2017-08-29', '2017-09-14', 22190, 19591.2, 1, 2, 2, 3, 1, 1),
(720, 57, 2, '1', '98953711', 'CRLU 721320-4', 4, 'HS LISZT', '735N', 3, 1, '2017-08-24', '2017-08-24', '2017-08-29', '2017-09-19', 22100, 19591.2, 1, 2, 2, 3, 1, 1),
(721, 57, 4, '1', '98982081', 'TCLU 118605-3', 4, 'HS LISZT', '735N', 3, 15, '2017-08-24', '2017-08-24', '2017-08-29', '2017-09-19', 22090, 19591.2, 1, 2, 1, 2, 1, 1),
(722, 57, 1, '23845', '7LIMES5076', 'SUDU 515015-2', 3, 'HS LISZT', '735N', 3, 15, '2017-08-25', '2017-08-25', '2017-08-29', '2017-09-14', 22390, 19591.2, 1, 2, 2, 2, 1, 1),
(723, 57, 4, '1', '98982081', 'CPSU 512120-3', 4, 'HS  LISZT', '735N', 3, 15, '2017-08-25', '2017-08-25', '2017-08-29', '2017-09-19', 22240, 19591.2, 1, 2, 1, 2, 1, 1),
(724, 57, 3, '1', 'LMM0219738', 'TRIU 897756-0', 2, 'HS LISZT', '029CNR', 24, 1, '2017-08-25', '2017-08-25', '2017-08-29', '2017-09-19', 22360, 19591.2, 1, 2, 2, 2, 1, 1),
(725, 57, 1, '23844', '7LIMES5076', 'SUDU 519274-9', 3, 'HS  LISZT', '735N', 2, 15, '2017-08-25', '2017-08-25', '2017-08-29', '2017-09-19', 22250, 19591.2, 1, 2, 2, 2, 1, 1),
(726, 57, 5, '1', '7LIMES2933', 'SUDU 620400-0', 3, 'HS LISZT', '735N', 2, 1, '2017-08-26', '2017-08-26', '2017-08-29', '2017-09-11', 19670, 17414.4, 1, 2, 1, 2, 1, 1),
(727, 57, 5, '1', '7LIMES2934', 'SUDU 623091-0', 3, 'HS  LISZT', '735N', 2, 1, '2017-08-26', '2017-08-26', '2017-08-29', '2017-09-11', 19670, 17414.4, 1, 2, 1, 2, 1, 1),
(728, 59, 1, '24037', 'MBM140178180', 'CXRU 128422-6', 8, 'ATLANTIC KLIPPER', 'RA17030EB', 3, 15, '2017-09-05', '2017-09-04', '2017-09-10', '2017-09-28', 22340, 19591.2, 1, 2, 1, 2, 1, 1),
(729, 59, 1, '24038', 'MBM140178180', 'CAIU 542053-8', 8, 'ATLANTIC KLIPPER', 'RA17030EB', 3, 15, '2017-09-05', '2017-09-05', '2017-09-10', '2017-09-28', 22490, 19591.2, 1, 2, 1, 2, 1, 1),
(730, 59, 1, '24039', 'MBM140178180', 'CXRU 152742-9', 8, 'ATLANTIC KLIPPER', 'RA17030EB', 3, 15, '2017-09-08', '2017-09-08', '2017-09-10', '2017-09-28', 22240, 19591.2, 1, 2, 1, 2, 1, 1),
(731, 59, 1, '24040', 'MBM140178180', 'TTNU 896364-3', 8, 'ATLANTIC KLIPPER', 'RA17030EB', 3, 15, '2017-09-06', '2017-09-06', '2017-09-10', '2017-09-28', 22300, 19591.2, 1, 2, 1, 2, 1, 1),
(732, 59, 1, '24041', '7LIMES5301', 'SUDU 816768-9', 3, 'HS LISZT', '737N', 3, 15, '2017-09-08', '2017-09-08', '2017-09-12', '2017-10-03', 22410, 19591.2, 1, 2, 1, 2, 1, 1),
(733, 59, 1, '24042', '7LIMES5301', 'SUDU 824044-0', 3, 'HS LISZT', '737N', 3, 15, '2017-09-08', '2017-09-08', '2017-09-12', '2017-10-03', 22370, 19591.2, 1, 2, 1, 2, 1, 1),
(734, 59, 1, '24102', '7LIMES5301', 'SUDU 800702-1', 3, 'HS LISZT', '737N', 3, 15, '2017-09-06', '2017-09-06', '2017-09-12', '2017-10-03', 22370, 19591.2, 1, 2, 1, 2, 1, 1),
(735, 59, 1, '24103', '7LIMES5301', 'SUDU 800714-5', 3, 'HS LISZT', '737N', 3, 15, '2017-09-06', '2017-09-06', '2017-09-12', '2017-10-03', 22410, 19591.2, 1, 2, 1, 2, 1, 1),
(736, 59, 1, '24043', '962044717', 'MMBU 325216-5', 5, 'SEALAND BALBOA', '1714', 1, 15, '2017-09-04', '2017-09-04', '2017-09-08', '2017-10-01', 22220, 19591.2, 1, 2, 1, 2, 1, 1),
(737, 59, 1, '24044', '962044717', 'MWCU 692721-7', 5, 'SEALAND BALBOA', '1714', 1, 15, '2017-09-04', '2017-09-04', '2017-09-08', '2017-10-01', 22350, 19591.2, 1, 2, 1, 2, 1, 1),
(738, 59, 1, '24045', '962044717', 'MWCU 695324-2', 5, 'SEALAND BALBOA', '1714', 1, 15, '2017-09-04', '2017-09-04', '2017-09-08', '2017-10-01', 22480, 19591.2, 1, 2, 1, 2, 1, 1),
(739, 59, 1, '24046', '962044717', 'MSWU 901014-0', 5, 'SEALAND BALBOA', '1714', 1, 15, '2017-09-04', '2017-09-04', '2017-09-08', '2017-10-01', 22340, 19591.2, 1, 2, 1, 2, 1, 1),
(740, 59, 1, '24047', '962044722', 'MMAU 101604-8', 3, 'SEALAND BALBOA', '1714', 7, 15, '2017-09-05', '2017-09-05', '2017-09-01', '2017-10-12', 21600, 18200, 1, 2, 1, 2, 1, 1),
(741, 59, 5, '1', '7LIMAG2632', 'SUDU 822627-2', 3, 'HS LISZT', '737N', 2, 1, '2017-09-09', '2017-09-09', '2017-09-12', '2017-09-24', 19750, 17414.4, 1, 3, 1, 2, 1, 1),
(742, 59, 5, '1', '7LIMAG2633', 'SUDU 814923-7', 3, 'HS LISZT', '737N', 2, 1, '2017-09-09', '2017-09-09', '2017-09-12', '2017-09-24', 19750, 17414.4, 1, 3, 1, 2, 1, 1),
(743, 59, 2, '1', '80028346', 'HLXU 673488-9', 4, 'HS LISZT', '737N', 3, 1, '2017-09-06', '2017-09-06', '2017-09-12', '2017-10-03', 22270, 19591.2, 1, 3, 1, 2, 1, 1),
(744, 59, 2, '1', '80028346', 'CPSU 516498-8', 3, 'HS LISZT', '737N', 3, 1, '2017-09-07', '2017-09-07', '2017-09-12', '2017-10-03', 22210, 19591.2, 1, 3, 1, 2, 1, 1),
(745, 59, 4, '1', '80033391', 'TCLU 120483-5', 3, 'HS LISZT', '737N', 3, 15, '2017-09-07', '2017-09-07', '2017-09-12', '2017-10-03', 22200, 19591.2, 1, 3, 1, 2, 1, 1),
(746, 59, 4, '1', '80033391', 'TCLU 102664-6', 4, 'HS LISZT', '737N', 3, 15, '2017-09-07', '2017-09-07', '2017-09-12', '2017-10-03', 22200, 19591.2, 1, 3, 1, 2, 1, 1),
(747, 59, 3, '1', 'LMM0220370', 'TLLU 106984-1', 2, 'HS LISZT', '031CNR', 6, 1, '2017-09-08', '2017-09-08', '2017-09-12', '2017-10-03', 22270, 19591.2, 1, 3, 1, 2, 1, 1),
(748, 59, 3, '1', 'LMM0220370', 'TLLU 106960-4', 2, 'HS LISZT', '031CNR', 6, 1, '2017-09-08', '2017-09-08', '2017-09-12', '2017-10-03', 22300, 19591.2, 1, 3, 1, 2, 1, 1),
(749, 45, 3, '1', 'B6JBK024038', 'TEMU 943002-9', 9, 'JAMILA', '044', 6, 1, '2017-02-11', '2017-02-11', '2017-02-15', '2017-02-15', 22260, 19591.2, 1, 1, 1, 1, 1, 1),
(750, 45, 4, '1', '98068225', 'GESU 938471-9', 4, 'MAGARI', '707', 3, 15, '2017-02-13', '2017-02-13', '2017-02-15', '2017-02-15', 22230, 19591.2, 1, 1, 1, 1, 1, 1),
(751, 33, 3, '1', 'B6JBK024412', 'DFIU 260060-4', 9, 'JAMILA 047', '047', 22, 1, '2017-03-25', '2017-03-25', '2017-03-27', '2017-03-30', 19740, 17414.4, 1, 1, 1, 1, 1, 1),
(752, 58, 3, '1', 'LMM0220119', 'TLLU 105927-3', 2, 'HS LISZT', '030', 6, 1, '2017-09-01', '2017-09-01', '2017-09-05', '2017-09-26', 22270, 19591.2, 1, 2, 2, 2, 1, 1),
(753, 58, 3, '1', 'LMM0220119', 'TLLU 105716-2', 2, 'HS LISZT', '030', 6, 1, '2017-09-02', '2017-09-02', '2017-09-05', '2017-09-20', 22130, 19591.2, 1, 2, 2, 2, 1, 1),
(754, 58, 4, '1', '80016015', 'CRLU 130221-7', 4, 'HS LISZT', '736', 3, 15, '2017-09-01', '2017-09-02', '2017-09-05', '2017-09-26', 22090, 19591.2, 1, 2, 2, 2, 1, 1),
(755, 58, 4, '1', '80016015', 'CRLU 150303-7', 4, 'HS LISZT', '736', 3, 15, '2017-09-02', '2017-09-02', '2017-09-05', '2017-09-26', 22250, 19591.2, 1, 2, 2, 2, 1, 1),
(756, 58, 2, '1', '98990069', 'CRLU 126164-8', 4, 'HS LISZT', '736', 3, 1, '2017-09-02', '2017-09-02', '2017-09-05', '2017-09-26', 22220, 19591.2, 1, 2, 2, 2, 1, 1),
(757, 58, 2, '1', '98990069', 'HLXU 875617-2', 4, 'HS LISZT', '736', 3, 1, '2017-09-03', '2017-09-03', '2017-09-05', '2017-09-26', 22250, 19591.2, 1, 2, 2, 2, 1, 1),
(758, 58, 5, '1', '7LIMES3031', 'SUDU 629572-0', 3, 'HS LISZT', '736', 2, 1, '2017-09-03', '2017-09-03', '2017-09-05', '2017-09-18', 19660, 17414.4, 1, 2, 1, 3, 1, 1),
(759, 58, 5, '1', '7LIMES3030', 'SUDU 620492-6', 3, 'HS LISZT', '736', 2, 1, '2017-09-03', '2017-09-03', '2017-09-05', '2017-09-18', 19690, 17414.4, 1, 2, 1, 3, 1, 1),
(760, 40, 1, '20801', 'MBM140178124', 'TTNU 895408-7', 8, 'BALTIC KLIPPER', '17011', 3, 2, '2017-02-03', '2017-02-03', '2017-02-05', '2017-02-05', 22190, 19591.2, 1, 1, 1, 1, 1, 1),
(761, 25, 4, '2', '98488003', 'CPSU 510199-5', 4, 'MAGARI', '721', 3, 2, '2017-05-20', '2017-05-20', '2017-05-23', '2017-05-30', 22330, 19591.2, 1, 1, 1, 1, 1, 1),
(762, 59, 1, '24135', '80028346', 'SUDU 502329-2', 3, 'HS LISZT', '737N', 3, 15, '2017-09-08', '2017-09-08', '2017-09-12', '2017-10-03', 22380, 19591.2, 1, 2, 1, 2, 1, 1),
(763, 59, 2, '1', '80028346', 'UACU 472996-0', 4, 'HS  LISZT', '737N', 3, 1, '2017-09-07', '2017-09-07', '2017-09-12', '2017-10-03', 22160, 19591.2, 1, 3, 1, 2, 1, 1),
(764, 60, 1, '24179', '962111270', 'PONU 480756-8', 5, 'MARINER', '1715', 1, 15, '2017-09-09', '2017-09-09', '2017-10-15', '2017-10-08', NULL, 19591.2, 1, 1, 1, 1, 1, 1),
(765, 60, 1, '24180', '962111270', 'MWCU 524826-8', 5, 'MARINER', '1714', 1, 15, '2017-09-09', '2017-09-09', '2017-09-15', '2017-10-08', NULL, 19591.2, 1, 1, 1, 1, 1, 1),
(766, 60, 1, '24181', '962111270', 'MWMU 635700-6', 5, 'MARINER', '1714', 1, 15, '2017-09-10', '2017-09-10', '2017-09-15', '2017-10-08', NULL, 19591.2, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenedor_valija`
--

CREATE TABLE `contenedor_valija` (
  `id` int(11) NOT NULL,
  `valija_id` int(11) NOT NULL,
  `contenedor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contenedor_valija`
--

INSERT INTO `contenedor_valija` (`id`, `valija_id`, `contenedor_id`) VALUES
(1, 1, 582);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `emails`
--

INSERT INTO `emails` (`id`, `id_cliente`, `email`, `activo`) VALUES
(7, 1, 'producerinvoices@agrofair.nl', 1),
(9, 2, 'tobias.zeiher@biodynamiskaprodukter.se', 1),
(10, 2, 'info@biodynamiskaprodukter.nl', 1),
(12, 4, 'fiorella.melendrez@transastra.net', 1),
(13, 3, 'marile.noriega@dole.com', 1),
(14, 3, 'Daniela.Chica@dole.com', 1),
(15, 5, 'info@equifruit.com', 1),
(16, 1, 'appbosa@yahoo.com', 1),
(17, 1, 'rianne.bruinsma@agrofair.nl', 1),
(18, 1, 'certappbosa@hotmail.com', 1),
(19, 1, 'jucamo78appbosa@hotmail.com', 1),
(20, 1, 'appbosa_contab@hotmail.com', 1),
(21, 1, 'contableappbosa@yahoo.com', 1),
(22, 2, 'john.gerhard@biodynamiskaprodukter.se', 1),
(23, 2, 'appbosa@yahoo.com', 1),
(24, 2, 'certappbosa@hotmail.com', 1),
(25, 2, 'jucamo78appbosa@hotmail.com', 1),
(26, 2, 'contableappbosa@yahoo.com', 1),
(27, 3, 'appbosa@yahoo.com', 1),
(28, 3, 'certappbosa@hotmail.com', 1),
(29, 3, 'jucamo78appbosa@hotmail.com', 1),
(30, 3, 'contableappbosa@yahoo.com', 1),
(31, 4, 'appbosa@yahoo.com', 1),
(32, 4, 'certappbosa@hotmail.com', 1),
(33, 4, 'jucamo78appbosa@hotmail.com', 1),
(34, 4, 'contableappbosa@yahoo.com', 1),
(35, 5, 'appbosa@yahoo.com', 1),
(36, 5, 'certappbosa@hotmail.com', 1),
(37, 5, 'jucamo78appbosa@hotmail.com', 1),
(38, 5, 'contableappbosa@yahoo.com', 1),
(39, 5, 'appbosa_conatb@hotmail.com', 1),
(40, 4, 'appbosa_conatb@hotmail.com', 1),
(41, 3, 'appbosa_contab@hotmail.com', 1),
(42, 2, 'appbosa_contab@hotmail.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empacadora`
--

CREATE TABLE `empacadora` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `ubicacion` varchar(255) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empacadora`
--

INSERT INTO `empacadora` (`id`, `nombre`, `ubicacion`, `activo`) VALUES
(1, 'ORDINOLA', 'LA NORIA', 1),
(2, 'YACILA', 'SAMÁN', 1),
(3, 'EMP 05', 'SAMAN', 1),
(4, 'SANDOVAL', 'LA NORIA', 1),
(5, 'LA LENGUA', 'LA NORIA', 1),
(6, 'VILLAREYES', 'SAMAN', 1),
(7, 'VIUDA', 'SAMAN CHICO', 1),
(8, 'MONTERON', 'MONTERON', 1),
(9, 'ZEGARRA', 'SAMAN', 1),
(10, 'INVERNA ZETA', 'SEGUNDA', 1),
(11, 'CARRILLO', 'SAMAN', 1),
(12, 'TERCERA', 'MALLARES', 1),
(13, 'GOLONDRINA', 'GOLONDRINA', 1),
(14, 'NORIA', 'LA NORIA', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `color` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id`, `nombre`, `color`, `activo`) VALUES
(1, 'Pendiente', '#ff0013', 1),
(2, 'En Proceso', '#D35400', 1),
(3, 'Enviado', '#27AE60', 1),
(4, 'En Puerto Paita', '#2980B9', 1),
(5, 'En la nave', '#004040', 1),
(6, 'Llegó a destino', '#b516e9', 1),
(7, 'Cancelado', '#008040', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_archivo`
--

CREATE TABLE `estado_archivo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado_archivo`
--

INSERT INTO `estado_archivo` (`id`, `nombre`, `color`, `activo`) VALUES
(1, 'Pendiente', '#D9534F', 1),
(2, 'Tramitado', '#2062C8', 1),
(3, 'Enviado', '#1EBD2F', 1),
(4, 'Confirmado', '#229954', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `id_operador` int(11) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` text NOT NULL,
  `id_estado_archivo` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id`, `id_cliente`, `id_semana`, `id_operador`, `nombre`, `fecha`, `hora`, `url`, `id_estado_archivo`) VALUES
(2, 1, 47, NULL, '1_24_2017_(1)AGROFAIR01.pdf', '2017-06-22', '15:15:07', '1_24_2017_(1)AGROFAIR01.pdf', 2),
(3, 1, 47, NULL, '1_24_2017_(2)AGROFAIR2.pdf', '2017-06-22', '15:19:18', '1_24_2017_(2)AGROFAIR2.pdf', 2),
(4, 3, 47, NULL, '3_24_2017_(1)DOLE.pdf', '2017-06-22', '15:24:23', '3_24_2017_(1)DOLE.pdf', 2),
(5, 2, 47, NULL, '2_24_2017_(1)BIODYNAMISKA.pdf', '2017-06-22', '15:25:36', '2_24_2017_(1)BIODYNAMISKA.pdf', 2),
(6, 5, 47, NULL, '5_24_2017_(1)EQUIFRUIT.pdf', '2017-06-22', '15:26:28', '5_24_2017_(1)EQUIFRUIT.pdf', 2),
(7, 4, 47, NULL, '4_24_2017_(1)TRANSASTRA.pdf', '2017-06-22', '15:28:13', '4_24_2017_(1)TRANSASTRA.pdf', 2),
(8, 1, 49, NULL, '1_26_2017_(3)AGROFAIR1.pdf', '2017-07-05', '10:17:23', '1_26_2017_(3)AGROFAIR1.pdf', 2),
(9, 1, 49, NULL, '1_26_2017_(4)AGROFAIR2.pdf', '2017-07-05', '10:17:23', '1_26_2017_(4)AGROFAIR2.pdf', 2),
(10, 2, 49, NULL, '2_26_2017_(2)BIODYNAMISKA.pdf', '2017-07-05', '10:18:24', '2_26_2017_(2)BIODYNAMISKA.pdf', 2),
(11, 3, 49, NULL, '3_26_2017_(2)DOLE D.pdf', '2017-07-05', '10:20:29', '3_26_2017_(2)DOLE D.pdf', 2),
(12, 5, 49, NULL, '5_26_2017_(2)EQUIFRUIT.pdf', '2017-07-05', '10:21:00', '5_26_2017_(2)EQUIFRUIT.pdf', 2),
(13, 4, 49, NULL, '4_26_2017_(2)TRANSASTRA.pdf', '2017-07-05', '10:21:45', '4_26_2017_(2)TRANSASTRA.pdf', 2),
(14, 1, 50, NULL, '1_27_2017_(5)AGROFAIR01.pdf', '2017-07-17', '15:04:18', '1_27_2017_(5)AGROFAIR01.pdf', 2),
(15, 1, 50, NULL, '1_27_2017_(6)AGROFAIR02.pdf', '2017-07-17', '15:04:18', '1_27_2017_(6)AGROFAIR02.pdf', 2),
(16, 2, 50, NULL, '2_27_2017_(3)BIODYNAMISKA.pdf', '2017-07-17', '15:05:24', '2_27_2017_(3)BIODYNAMISKA.pdf', 2),
(17, 2, 50, NULL, '2_27_2017_(4)BIODYNAMISKA02.pdf', '2017-07-17', '15:05:24', '2_27_2017_(4)BIODYNAMISKA02.pdf', 2),
(18, 3, 50, NULL, '3_27_2017_(3)DOLE.pdf', '2017-07-17', '15:05:54', '3_27_2017_(3)DOLE.pdf', 2),
(19, 5, 50, NULL, '5_27_2017_(3)EQUIFRUIT.pdf', '2017-07-17', '15:06:46', '5_27_2017_(3)EQUIFRUIT.pdf', 2),
(20, 5, 50, NULL, '5_27_2017_(4)EQUIFRUIT2.pdf', '2017-07-17', '15:06:46', '5_27_2017_(4)EQUIFRUIT2.pdf', 2),
(21, 4, 50, NULL, '4_27_2017_(3)TRANSASTRA.pdf', '2017-07-17', '15:07:34', '4_27_2017_(3)TRANSASTRA.pdf', 2),
(22, 1, 51, 2, '1_28_2017_(7)AGROFAIR01.pdf', '2017-08-18', '16:16:06', '1_28_2017_(7)AGROFAIR01.pdf', 2),
(23, 1, 51, 2, '1_28_2017_(8)AGROFAIR02.pdf', '2017-08-18', '16:16:06', '1_28_2017_(8)AGROFAIR02.pdf', 2),
(24, 2, 51, 1, '2_28_2017_(5)BIODYNAMISKA.pdf', '2017-08-18', '16:18:03', '2_28_2017_(5)BIODYNAMISKA.pdf', 2),
(25, 3, 51, 1, '3_28_2017_(4)DOLE D.pdf', '2017-08-18', '16:19:09', '3_28_2017_(4)DOLE D.pdf', 2),
(26, 5, 51, 2, '5_28_2017_(5)EQUIFRUIT.pdf', '2017-08-18', '16:22:51', '5_28_2017_(5)EQUIFRUIT.pdf', 2),
(27, 4, 51, 2, '4_28_2017_(4)TRANSASTRA.pdf', '2017-08-18', '16:25:16', '4_28_2017_(4)TRANSASTRA.pdf', 2),
(28, 1, 52, 15, '1_29_2017_(9)AGROFAIR01.pdf', '2017-08-21', '08:38:24', '1_29_2017_(9)AGROFAIR01.pdf', 2),
(29, 1, 52, 15, '1_29_2017_(10)AGROFAIR02.pdf', '2017-08-21', '08:38:24', '1_29_2017_(10)AGROFAIR02.pdf', 2),
(30, 2, 52, 1, '2_29_2017_(6)BIODYNAMISKA.pdf', '2017-08-21', '08:41:52', '2_29_2017_(6)BIODYNAMISKA.pdf', 2),
(31, 3, 52, 1, '3_29_2017_(5)DOLE.pdf', '2017-08-21', '08:42:59', '3_29_2017_(5)DOLE.pdf', 2),
(32, 5, 52, 1, '5_29_2017_(6)EQUIFRUIT.pdf', '2017-08-21', '08:44:11', '5_29_2017_(6)EQUIFRUIT.pdf', 2),
(33, 4, 52, 15, '4_29_2017_(5)TRANSASTRA.pdf', '2017-08-21', '08:51:16', '4_29_2017_(5)TRANSASTRA.pdf', 2),
(34, 1, 53, 15, '1_30_2017_(11)AGROFAIR 1.pdf', '2017-08-21', '09:26:22', '1_30_2017_(11)AGROFAIR 1.pdf', 2),
(35, 1, 53, 15, '1_30_2017_(12)AGROFAIR 2.pdf', '2017-08-21', '09:26:22', '1_30_2017_(12)AGROFAIR 2.pdf', 2),
(37, 1, 56, 15, '1_33_2017_(13)AGROFAIR1.pdf', '2017-08-24', '12:32:48', '1_33_2017_(13)AGROFAIR1.pdf', 2),
(38, 1, 56, 15, '1_33_2017_(14)AGROFAIR2.pdf', '2017-08-24', '12:32:48', '1_33_2017_(14)AGROFAIR2.pdf', 2),
(39, 2, 56, 1, '2_33_2017_(7)BIODYNAMISKA.pdf', '2017-08-24', '12:33:56', '2_33_2017_(7)BIODYNAMISKA.pdf', 2),
(40, 3, 56, 1, '3_33_2017_(6)DOLE D.pdf', '2017-08-24', '12:34:46', '3_33_2017_(6)DOLE D.pdf', 2),
(41, 5, 56, 1, '5_33_2017_(7)EQUIFRUIT.pdf', '2017-08-24', '12:35:41', '5_33_2017_(7)EQUIFRUIT.pdf', 2),
(42, 4, 56, 15, '4_33_2017_(6)TRANSASTRA.pdf', '2017-08-24', '12:36:29', '4_33_2017_(6)TRANSASTRA.pdf', 2),
(43, 3, 53, 1, '3_30_2017_(7)DOLE GERMANY.pdf', '2017-08-26', '10:39:54', '3_30_2017_(7)DOLE GERMANY.pdf', 2),
(44, 5, 53, 1, '5_30_2017_(8)EQUIFRUIT.pdf', '2017-08-26', '10:40:57', '5_30_2017_(8)EQUIFRUIT.pdf', 2),
(45, 2, 53, 1, '2_30_2017_(8)STIFTELSEN BIODYNAMISKA.pdf', '2017-08-26', '10:43:06', '2_30_2017_(8)STIFTELSEN BIODYNAMISKA.pdf', 2),
(46, 4, 53, 15, '4_30_2017_(7)TRANSASTRA.pdf', '2017-08-26', '10:44:08', '4_30_2017_(7)TRANSASTRA.pdf', 2),
(47, 1, 57, 15, '1_34_2017_(15)AGROFAIR.pdf', '2017-08-29', '14:57:29', '1_34_2017_(15)AGROFAIR.pdf', 2),
(48, 2, 57, 1, '2_34_2017_(9)BIODYNAMISKA.pdf', '2017-08-29', '14:58:42', '2_34_2017_(9)BIODYNAMISKA.pdf', 2),
(49, 3, 57, 1, '3_34_2017_(8)DOLE.pdf', '2017-08-29', '14:59:16', '3_34_2017_(8)DOLE.pdf', 2),
(50, 5, 57, 1, '5_34_2017_(9)EQUIFRUIT.pdf', '2017-08-29', '15:00:22', '5_34_2017_(9)EQUIFRUIT.pdf', 2),
(51, 4, 57, 15, '4_34_2017_(8)TRANSASTRA.pdf', '2017-08-29', '15:01:16', '4_34_2017_(8)TRANSASTRA.pdf', 2),
(52, 1, 58, 15, '1_35_2017_(16)AGROFAIR1.pdf', '2017-09-12', '10:53:44', '1_35_2017_(16)AGROFAIR1.pdf', 2),
(53, 1, 58, 15, '1_35_2017_(17)AGROFAIR2.pdf', '2017-09-12', '10:53:44', '1_35_2017_(17)AGROFAIR2.pdf', 2),
(54, 2, 58, 1, '2_35_2017_(10)BIODYNAMISKA.pdf', '2017-09-12', '10:54:51', '2_35_2017_(10)BIODYNAMISKA.pdf', 2),
(55, 3, 58, 1, '3_35_2017_(9)DOLE.pdf', '2017-09-12', '10:55:38', '3_35_2017_(9)DOLE.pdf', 2),
(56, 5, 58, 1, '5_35_2017_(10)EQUIFRUIT.pdf', '2017-09-12', '10:56:26', '5_35_2017_(10)EQUIFRUIT.pdf', 2),
(57, 4, 58, 15, '4_35_2017_(9)TRANSASTRA.pdf', '2017-09-12', '10:57:51', '4_35_2017_(9)TRANSASTRA.pdf', 2),
(58, 2, 59, 1, '2_36_2017_(11)BIODYNAMISKA.pdf', '2017-09-12', '16:16:31', '2_36_2017_(11)BIODYNAMISKA.pdf', 3),
(59, 3, 59, 1, '3_36_2017_(10)DOLE.pdf', '2017-09-12', '16:17:42', '3_36_2017_(10)DOLE.pdf', 3),
(60, 5, 59, 1, '5_36_2017_(11)EQUIFRUIT.pdf', '2017-09-12', '16:18:45', '5_36_2017_(11)EQUIFRUIT.pdf', 3),
(61, 4, 59, 15, '4_36_2017_(10)TRANSASTRA.pdf', '2017-09-12', '16:19:29', '4_36_2017_(10)TRANSASTRA.pdf', 3),
(62, 1, 59, 15, '1_36_2017_(18)AGROFAIR1.pdf', '2017-09-12', '16:26:57', '1_36_2017_(18)AGROFAIR1.pdf', 2),
(63, 1, 59, 15, '1_36_2017_(19)AGROFAIR2.pdf', '2017-09-12', '16:26:57', '1_36_2017_(19)AGROFAIR2.pdf', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `icono`
--

CREATE TABLE `icono` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linea_naviera`
--

CREATE TABLE `linea_naviera` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `sigla` varchar(5) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `linea_naviera`
--

INSERT INTO `linea_naviera` (`id`, `nombre`, `sigla`, `activo`) VALUES
(1, 'APL', 'APL', 1),
(2, 'CMA - CGM', 'CMA', 1),
(3, 'HAMBURG SUD - HSD', 'HSD', 1),
(4, 'HAPAG LLOYD - HLE', 'HLE', 1),
(5, 'MAERSK MSK', 'MSK', 1),
(6, 'MOL', 'MOL', 1),
(7, 'MSC', 'MSC', 1),
(8, 'SEATRADE - STR', 'STR', 1),
(9, 'DOLE', 'DOLE', 1),
(18, 'YOKOHAMA-JAPON', 'JAPON', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `nombre`, `activo`) VALUES
(14, 'Administrador', 1),
(15, 'Jefe Trazabilidad', 1),
(16, 'Cliente', 1),
(20, 'Gerencia', 1),
(21, 'Auxiliar Trazabilidad', 1),
(22, 'Operador', 1),
(23, 'Control Interno', 1),
(24, 'Facturador', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_opcion`
--

CREATE TABLE `menu_opcion` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `opcion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_opcion`
--

INSERT INTO `menu_opcion` (`id`, `menu_id`, `opcion_id`) VALUES
(1, 14, 7),
(2, 14, 8),
(3, 14, 9),
(4, 14, 10),
(6, 14, 12),
(7, 15, 7),
(8, 15, 8),
(9, 15, 9),
(10, 15, 10),
(11, 15, 11),
(19, 14, 11),
(24, 20, 7),
(25, 20, 8),
(26, 20, 9),
(27, 20, 10),
(28, 20, 11),
(29, 16, 13),
(30, 21, 8),
(31, 21, 7),
(32, 21, 10),
(33, 21, 11),
(34, 22, 14),
(35, 23, 8),
(36, 23, 10),
(37, 23, 11),
(38, 14, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `motivo`
--

CREATE TABLE `motivo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `motivo`
--

INSERT INTO `motivo` (`id`, `nombre`, `activo`) VALUES
(1, 'Fallecimiento', 1),
(2, 'Suspensión', 1),
(3, 'Sancionado', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `id` int(11) NOT NULL,
  `id_contenedor` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `id_estado` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`id`, `id_contenedor`, `id_usuario`, `fecha`, `hora`, `id_estado`, `activo`) VALUES
(73, 26, 4, '2017-05-29', '10:15:54', 1, 0),
(74, 26, 4, '2017-05-29', '10:18:43', 2, 0),
(75, 26, 4, '2017-05-29', '10:19:24', 3, 1),
(76, 27, 4, '2017-05-29', '12:18:59', 1, 1),
(77, 28, 4, '2017-05-29', '12:33:50', 1, 0),
(78, 28, 4, '2017-05-29', '12:34:12', 5, 0),
(79, 29, 4, '2017-05-29', '12:42:51', 1, 0),
(80, 29, 4, '2017-05-29', '12:43:12', 5, 0),
(81, 30, 4, '2017-05-29', '12:49:33', 1, 0),
(82, 30, 4, '2017-05-29', '12:49:36', 5, 0),
(83, 31, 4, '2017-05-29', '12:52:50', 1, 0),
(84, 32, 4, '2017-05-29', '12:56:27', 1, 0),
(85, 32, 4, '2017-05-29', '12:56:36', 5, 0),
(86, 31, 4, '2017-05-29', '12:56:39', 5, 0),
(87, 33, 4, '2017-05-29', '15:11:35', 1, 0),
(88, 33, 4, '2017-05-29', '15:11:49', 5, 0),
(89, 34, 4, '2017-05-29', '15:17:08', 1, 0),
(90, 35, 4, '2017-05-29', '15:19:48', 1, 0),
(91, 35, 4, '2017-05-29', '15:20:59', 5, 0),
(92, 34, 4, '2017-05-29', '15:21:03', 5, 1),
(93, 36, 4, '2017-05-29', '16:13:40', 1, 0),
(94, 37, 4, '2017-05-29', '16:15:15', 1, 0),
(95, 38, 4, '2017-05-29', '16:17:18', 1, 0),
(96, 39, 4, '2017-05-29', '16:20:48', 1, 0),
(97, 41, 4, '2017-05-29', '16:24:37', 1, 0),
(98, 36, 4, '2017-05-29', '16:24:55', 4, 0),
(99, 37, 4, '2017-05-29', '16:24:57', 4, 0),
(100, 38, 4, '2017-05-29', '16:25:00', 4, 0),
(101, 39, 4, '2017-05-29', '16:25:03', 4, 0),
(102, 41, 4, '2017-05-29', '16:25:06', 4, 0),
(103, 42, 4, '2017-05-29', '16:27:25', 1, 0),
(104, 42, 4, '2017-05-29', '16:27:31', 4, 0),
(105, 43, 4, '2017-05-29', '16:29:35', 1, 0),
(106, 43, 4, '2017-05-29', '16:29:39', 4, 0),
(107, 44, 4, '2017-05-29', '16:31:34', 1, 0),
(108, 45, 4, '2017-05-29', '16:40:33', 1, 0),
(109, 46, 4, '2017-05-29', '16:42:35', 1, 0),
(110, 47, 4, '2017-05-29', '16:44:06', 1, 0),
(111, 48, 4, '2017-05-29', '16:48:31', 1, 0),
(112, 43, 2, '2017-05-29', '21:28:08', 2, 0),
(113, 43, 2, '2017-05-29', '21:28:21', 3, 0),
(114, 49, 4, '2017-05-30', '08:41:26', 1, 0),
(115, 50, 4, '2017-05-30', '08:45:52', 1, 0),
(116, 51, 4, '2017-05-30', '08:49:30', 1, 0),
(117, 52, 4, '2017-05-30', '08:52:21', 1, 0),
(118, 53, 4, '2017-05-30', '09:05:37', 1, 0),
(119, 54, 4, '2017-05-30', '09:08:10', 1, 0),
(120, 55, 4, '2017-05-30', '09:11:01', 1, 0),
(121, 56, 4, '2017-05-30', '09:14:17', 1, 0),
(122, 57, 4, '2017-05-30', '09:20:06', 1, 0),
(123, 58, 4, '2017-05-30', '09:23:09', 1, 0),
(124, 59, 4, '2017-05-30', '10:26:21', 1, 0),
(125, 60, 4, '2017-05-30', '10:29:37', 1, 0),
(126, 61, 4, '2017-05-30', '10:34:36', 1, 1),
(127, 62, 4, '2017-05-30', '10:34:36', 1, 1),
(128, 63, 4, '2017-05-30', '10:34:36', 1, 0),
(129, 64, 4, '2017-05-30', '10:34:36', 1, 1),
(130, 65, 4, '2017-05-30', '10:41:36', 1, 0),
(131, 66, 4, '2017-05-30', '10:52:17', 1, 0),
(132, 66, 4, '2017-05-30', '10:53:02', 5, 0),
(133, 67, 4, '2017-05-30', '11:18:40', 1, 0),
(134, 67, 4, '2017-05-30', '11:18:47', 5, 0),
(135, 68, 4, '2017-05-30', '11:21:45', 1, 0),
(136, 68, 4, '2017-05-30', '11:21:53', 5, 0),
(137, 69, 4, '2017-05-30', '11:25:41', 1, 0),
(138, 69, 4, '2017-05-30', '11:25:44', 5, 0),
(139, 70, 4, '2017-05-30', '11:28:18', 1, 0),
(140, 70, 4, '2017-05-30', '11:28:21', 5, 0),
(141, 71, 4, '2017-05-30', '11:30:56', 1, 0),
(142, 71, 4, '2017-05-30', '11:31:00', 6, 0),
(143, 71, 4, '2017-05-30', '11:31:02', 5, 1),
(144, 72, 4, '2017-05-30', '11:33:42', 1, 0),
(145, 72, 4, '2017-05-30', '11:33:45', 5, 0),
(146, 73, 4, '2017-05-30', '11:40:12', 1, 0),
(147, 73, 4, '2017-05-30', '11:40:15', 5, 0),
(148, 74, 4, '2017-05-30', '11:42:30', 1, 0),
(149, 74, 4, '2017-05-30', '11:42:33', 5, 0),
(150, 75, 4, '2017-05-30', '11:45:39', 1, 0),
(151, 75, 4, '2017-05-30', '11:45:42', 5, 0),
(152, 76, 4, '2017-05-30', '11:48:11', 1, 0),
(153, 76, 4, '2017-05-30', '11:48:15', 5, 0),
(154, 77, 4, '2017-05-30', '11:51:08', 1, 0),
(155, 77, 4, '2017-05-30', '11:51:12', 5, 0),
(156, 78, 4, '2017-05-30', '11:54:13', 1, 0),
(157, 78, 4, '2017-05-30', '11:54:17', 5, 0),
(158, 79, 4, '2017-05-30', '11:56:22', 1, 0),
(159, 79, 4, '2017-05-30', '11:56:25', 5, 0),
(160, 80, 4, '2017-05-30', '11:59:41', 1, 0),
(161, 80, 4, '2017-05-30', '11:59:45', 5, 0),
(162, 82, 4, '2017-05-30', '12:03:27', 1, 0),
(163, 82, 4, '2017-05-30', '12:03:30', 5, 0),
(164, 83, 4, '2017-05-30', '12:07:06', 1, 0),
(165, 83, 4, '2017-05-30', '12:07:09', 6, 0),
(166, 83, 4, '2017-05-30', '12:07:13', 5, 1),
(167, 84, 4, '2017-05-30', '12:11:27', 1, 0),
(168, 84, 4, '2017-05-30', '12:11:30', 5, 0),
(169, 85, 4, '2017-05-30', '12:15:02', 1, 0),
(170, 85, 4, '2017-05-30', '12:15:05', 5, 0),
(171, 86, 4, '2017-05-30', '12:22:31', 1, 0),
(172, 86, 4, '2017-05-30', '12:22:39', 5, 0),
(173, 87, 4, '2017-05-30', '12:25:50', 1, 0),
(174, 87, 4, '2017-05-30', '12:25:53', 5, 0),
(175, 88, 4, '2017-05-30', '12:27:40', 1, 0),
(176, 88, 4, '2017-05-30', '12:33:07', 5, 0),
(177, 89, 4, '2017-05-30', '12:36:13', 1, 0),
(178, 89, 4, '2017-05-30', '12:36:17', 5, 0),
(179, 90, 4, '2017-05-30', '12:40:49', 1, 0),
(180, 90, 4, '2017-05-30', '12:40:54', 5, 0),
(181, 91, 4, '2017-05-31', '07:21:05', 1, 0),
(182, 92, 4, '2017-05-31', '07:23:14', 1, 0),
(183, 93, 4, '2017-05-31', '07:23:26', 1, 0),
(184, 93, 4, '2017-05-31', '07:27:22', 5, 0),
(185, 92, 4, '2017-05-31', '07:27:26', 5, 0),
(186, 91, 4, '2017-05-31', '07:27:29', 5, 1),
(187, 94, 4, '2017-05-31', '07:30:45', 1, 0),
(188, 94, 4, '2017-05-31', '07:30:49', 5, 0),
(189, 95, 4, '2017-05-31', '07:35:05', 1, 0),
(190, 95, 4, '2017-05-31', '07:35:09', 5, 0),
(191, 96, 4, '2017-05-31', '07:38:46', 1, 0),
(192, 96, 4, '2017-05-31', '07:38:50', 5, 0),
(193, 97, 4, '2017-05-31', '07:40:36', 1, 0),
(194, 97, 4, '2017-05-31', '07:40:48', 5, 0),
(195, 98, 4, '2017-05-31', '07:43:34', 1, 0),
(196, 98, 4, '2017-05-31', '07:43:37', 5, 0),
(197, 99, 4, '2017-05-31', '07:46:13', 1, 0),
(198, 100, 4, '2017-05-31', '07:52:45', 1, 0),
(199, 100, 4, '2017-05-31', '07:52:52', 5, 0),
(200, 99, 4, '2017-05-31', '07:52:54', 5, 0),
(201, 101, 4, '2017-05-31', '07:59:23', 1, 0),
(202, 101, 4, '2017-05-31', '08:03:58', 5, 0),
(203, 102, 4, '2017-05-31', '08:07:02', 1, 0),
(204, 102, 4, '2017-05-31', '08:07:06', 5, 0),
(205, 103, 4, '2017-05-31', '08:08:47', 1, 0),
(206, 103, 4, '2017-05-31', '08:08:58', 6, 0),
(207, 103, 4, '2017-05-31', '08:08:59', 5, 1),
(208, 104, 4, '2017-05-31', '08:12:56', 1, 0),
(209, 105, 4, '2017-05-31', '08:15:36', 1, 0),
(210, 106, 4, '2017-05-31', '08:17:15', 1, 0),
(211, 104, 4, '2017-05-31', '08:17:19', 5, 0),
(212, 105, 4, '2017-05-31', '08:17:22', 5, 0),
(213, 106, 4, '2017-05-31', '08:17:24', 5, 0),
(214, 107, 4, '2017-05-31', '08:29:44', 1, 0),
(215, 107, 4, '2017-05-31', '08:34:54', 6, 1),
(216, 108, 4, '2017-05-31', '08:37:11', 1, 0),
(217, 108, 4, '2017-05-31', '08:37:30', 6, 1),
(218, 109, 4, '2017-05-31', '08:40:06', 1, 0),
(219, 109, 4, '2017-05-31', '08:40:10', 5, 0),
(220, 109, 4, '2017-05-31', '08:41:08', 6, 1),
(221, 110, 4, '2017-05-31', '08:44:18', 1, 0),
(222, 110, 4, '2017-05-31', '08:44:41', 5, 0),
(223, 110, 4, '2017-05-31', '08:44:42', 6, 1),
(224, 111, 4, '2017-05-31', '08:47:25', 1, 0),
(225, 112, 4, '2017-05-31', '08:47:43', 1, 0),
(226, 111, 4, '2017-05-31', '08:48:08', 6, 1),
(227, 112, 4, '2017-05-31', '08:48:10', 6, 1),
(228, 113, 4, '2017-05-31', '08:53:29', 1, 0),
(229, 113, 4, '2017-05-31', '08:53:32', 6, 1),
(230, 114, 4, '2017-05-31', '08:57:09', 1, 0),
(231, 114, 4, '2017-05-31', '08:57:12', 6, 1),
(232, 115, 4, '2017-05-31', '09:00:28', 1, 0),
(233, 116, 4, '2017-05-31', '09:04:00', 1, 0),
(234, 116, 4, '2017-05-31', '09:04:29', 6, 1),
(235, 115, 4, '2017-05-31', '09:04:31', 6, 1),
(236, 117, 4, '2017-05-31', '09:07:43', 1, 0),
(237, 117, 4, '2017-05-31', '09:07:47', 6, 1),
(238, 118, 4, '2017-05-31', '09:10:13', 1, 0),
(239, 118, 4, '2017-05-31', '09:10:17', 6, 1),
(240, 119, 4, '2017-05-31', '09:12:45', 1, 0),
(241, 119, 4, '2017-05-31', '09:12:52', 6, 1),
(242, 120, 4, '2017-05-31', '09:17:36', 1, 0),
(243, 120, 4, '2017-05-31', '09:17:43', 6, 1),
(244, 121, 4, '2017-05-31', '09:20:03', 1, 0),
(245, 121, 4, '2017-05-31', '09:20:07', 6, 1),
(246, 122, 4, '2017-05-31', '09:21:47', 1, 0),
(247, 122, 4, '2017-05-31', '09:21:51', 6, 1),
(248, 123, 4, '2017-05-31', '09:24:13', 1, 0),
(249, 123, 4, '2017-05-31', '09:24:25', 6, 1),
(250, 124, 4, '2017-05-31', '09:37:03', 1, 0),
(251, 124, 4, '2017-05-31', '09:37:52', 6, 1),
(252, 125, 4, '2017-05-31', '09:40:10', 1, 0),
(253, 125, 4, '2017-05-31', '09:40:17', 6, 1),
(254, 126, 4, '2017-05-31', '09:42:00', 1, 0),
(255, 127, 4, '2017-05-31', '10:11:58', 1, 0),
(256, 127, 4, '2017-05-31', '10:12:03', 6, 1),
(257, 128, 4, '2017-05-31', '10:14:11', 1, 0),
(258, 128, 4, '2017-05-31', '10:14:17', 6, 1),
(259, 129, 4, '2017-05-31', '10:16:45', 1, 0),
(260, 129, 4, '2017-05-31', '10:16:52', 6, 1),
(261, 130, 4, '2017-05-31', '10:18:53', 1, 0),
(262, 130, 4, '2017-05-31', '10:18:57', 6, 1),
(263, 131, 4, '2017-05-31', '10:23:04', 1, 0),
(264, 131, 4, '2017-05-31', '10:23:07', 6, 1),
(265, 132, 4, '2017-05-31', '10:47:00', 1, 0),
(266, 132, 4, '2017-05-31', '10:47:13', 6, 1),
(267, 133, 4, '2017-05-31', '10:49:54', 1, 0),
(268, 133, 4, '2017-05-31', '10:50:00', 6, 1),
(269, 134, 4, '2017-05-31', '10:52:54', 1, 0),
(270, 134, 4, '2017-05-31', '10:52:57', 6, 1),
(271, 135, 4, '2017-05-31', '10:58:46', 1, 0),
(272, 135, 4, '2017-05-31', '10:58:49', 6, 1),
(273, 136, 4, '2017-05-31', '11:02:55', 1, 0),
(274, 136, 4, '2017-05-31', '11:03:02', 5, 0),
(275, 136, 4, '2017-05-31', '11:03:05', 6, 1),
(276, 137, 4, '2017-05-31', '11:04:54', 1, 0),
(277, 137, 4, '2017-05-31', '11:04:58', 6, 1),
(278, 138, 4, '2017-05-31', '11:07:42', 1, 1),
(279, 139, 4, '2017-05-31', '11:07:42', 1, 0),
(280, 139, 4, '2017-05-31', '11:08:01', 6, 1),
(281, 140, 4, '2017-05-31', '11:09:29', 1, 0),
(282, 140, 4, '2017-05-31', '11:09:32', 6, 1),
(283, 141, 4, '2017-05-31', '11:11:19', 1, 0),
(284, 141, 4, '2017-05-31', '11:11:22', 5, 0),
(285, 141, 4, '2017-05-31', '11:11:24', 6, 1),
(286, 142, 4, '2017-05-31', '11:12:36', 1, 0),
(287, 142, 4, '2017-05-31', '11:12:45', 6, 1),
(288, 143, 4, '2017-05-31', '11:14:47', 1, 0),
(289, 143, 4, '2017-05-31', '11:14:50', 6, 1),
(290, 144, 4, '2017-05-31', '11:17:39', 1, 0),
(291, 144, 4, '2017-05-31', '11:17:48', 6, 1),
(292, 145, 4, '2017-05-31', '11:19:54', 1, 0),
(293, 145, 4, '2017-05-31', '11:19:58', 6, 1),
(294, 146, 4, '2017-05-31', '11:21:41', 1, 0),
(295, 146, 4, '2017-05-31', '11:21:49', 6, 1),
(296, 147, 4, '2017-05-31', '11:23:58', 1, 0),
(297, 147, 4, '2017-05-31', '11:24:07', 5, 0),
(298, 147, 4, '2017-05-31', '11:24:11', 6, 1),
(299, 148, 4, '2017-05-31', '11:40:50', 1, 0),
(300, 148, 4, '2017-05-31', '11:40:53', 6, 1),
(301, 149, 4, '2017-05-31', '11:43:54', 1, 0),
(302, 149, 4, '2017-05-31', '11:43:57', 6, 1),
(303, 150, 4, '2017-05-31', '11:46:05', 1, 0),
(304, 150, 4, '2017-05-31', '11:46:08', 6, 1),
(305, 151, 4, '2017-05-31', '11:51:48', 1, 0),
(306, 151, 4, '2017-05-31', '11:51:51', 6, 1),
(307, 152, 4, '2017-05-31', '12:01:02', 1, 0),
(308, 152, 4, '2017-05-31', '12:01:07', 6, 1),
(309, 153, 4, '2017-05-31', '12:03:40', 1, 0),
(310, 153, 4, '2017-05-31', '12:03:45', 6, 1),
(311, 154, 4, '2017-05-31', '12:06:33', 1, 0),
(312, 154, 4, '2017-05-31', '12:06:37', 6, 1),
(313, 155, 4, '2017-05-31', '12:08:51', 1, 0),
(314, 155, 4, '2017-05-31', '12:08:56', 6, 1),
(315, 156, 4, '2017-05-31', '12:11:04', 1, 0),
(316, 156, 4, '2017-05-31', '12:11:10', 6, 1),
(317, 157, 4, '2017-05-31', '12:12:52', 1, 0),
(318, 157, 4, '2017-05-31', '12:12:56', 6, 1),
(319, 158, 4, '2017-05-31', '12:15:26', 1, 0),
(320, 158, 4, '2017-05-31', '12:15:41', 6, 1),
(321, 159, 4, '2017-05-31', '12:23:27', 1, 0),
(322, 159, 4, '2017-05-31', '12:23:34', 5, 0),
(323, 159, 4, '2017-05-31', '12:23:35', 6, 1),
(324, 160, 4, '2017-05-31', '12:25:09', 1, 0),
(325, 160, 4, '2017-05-31', '12:25:17', 6, 1),
(326, 161, 4, '2017-05-31', '12:27:09', 1, 0),
(327, 161, 4, '2017-05-31', '12:27:14', 6, 1),
(328, 162, 4, '2017-05-31', '12:28:51', 1, 0),
(329, 162, 4, '2017-05-31', '12:28:54', 6, 1),
(330, 163, 4, '2017-05-31', '12:30:12', 1, 0),
(331, 163, 4, '2017-05-31', '12:30:16', 6, 1),
(332, 164, 4, '2017-05-31', '12:32:17', 1, 0),
(333, 164, 4, '2017-05-31', '12:32:20', 6, 1),
(334, 165, 4, '2017-05-31', '12:33:55', 1, 0),
(335, 165, 4, '2017-05-31', '12:33:57', 6, 1),
(336, 166, 4, '2017-05-31', '12:40:02', 1, 0),
(337, 166, 4, '2017-05-31', '12:40:04', 6, 1),
(338, 167, 4, '2017-05-31', '12:41:14', 1, 0),
(339, 167, 4, '2017-05-31', '12:41:22', 6, 1),
(340, 168, 4, '2017-06-01', '07:31:12', 1, 0),
(341, 168, 4, '2017-06-01', '07:32:21', 6, 1),
(342, 169, 4, '2017-06-01', '07:34:06', 1, 0),
(343, 170, 4, '2017-06-01', '07:36:09', 1, 0),
(344, 169, 4, '2017-06-01', '07:36:13', 6, 1),
(345, 171, 4, '2017-06-01', '07:38:16', 1, 0),
(346, 170, 4, '2017-06-01', '07:38:25', 6, 1),
(347, 171, 4, '2017-06-01', '07:38:28', 6, 1),
(348, 172, 4, '2017-06-01', '07:40:08', 1, 0),
(349, 172, 4, '2017-06-01', '07:40:27', 6, 1),
(350, 173, 4, '2017-06-01', '07:41:56', 1, 0),
(351, 173, 4, '2017-06-01', '07:41:59', 6, 1),
(352, 174, 4, '2017-06-01', '07:43:43', 1, 1),
(353, 175, 4, '2017-06-01', '07:43:51', 1, 0),
(354, 175, 4, '2017-06-01', '07:45:00', 6, 1),
(355, 176, 4, '2017-06-01', '07:49:08', 1, 0),
(356, 176, 4, '2017-06-01', '07:51:07', 6, 1),
(357, 177, 4, '2017-06-01', '07:54:31', 1, 0),
(358, 177, 4, '2017-06-01', '07:55:10', 6, 1),
(359, 178, 4, '2017-06-01', '07:57:14', 1, 0),
(360, 178, 4, '2017-06-01', '07:58:38', 6, 1),
(361, 179, 4, '2017-06-01', '08:00:20', 1, 0),
(362, 179, 4, '2017-06-01', '08:00:55', 6, 1),
(363, 180, 4, '2017-06-01', '08:09:46', 1, 1),
(364, 181, 4, '2017-06-01', '08:12:51', 1, 0),
(365, 181, 4, '2017-06-01', '08:13:10', 6, 1),
(366, 182, 4, '2017-06-01', '08:13:17', 1, 0),
(367, 183, 4, '2017-06-01', '08:15:20', 1, 0),
(368, 184, 4, '2017-06-01', '08:16:33', 1, 0),
(369, 182, 4, '2017-06-01', '08:19:32', 6, 1),
(370, 184, 4, '2017-06-01', '08:19:58', 6, 1),
(371, 183, 4, '2017-06-01', '08:20:03', 6, 1),
(372, 185, 4, '2017-06-01', '08:27:07', 1, 0),
(373, 185, 4, '2017-06-01', '08:27:19', 6, 1),
(374, 186, 4, '2017-06-01', '08:29:17', 1, 0),
(375, 186, 4, '2017-06-01', '08:29:31', 6, 1),
(376, 187, 4, '2017-06-01', '08:31:21', 1, 0),
(377, 187, 4, '2017-06-01', '08:31:42', 6, 1),
(378, 188, 4, '2017-06-01', '08:34:36', 1, 0),
(379, 189, 4, '2017-06-01', '08:34:51', 1, 1),
(380, 188, 4, '2017-06-01', '08:35:27', 6, 1),
(381, 190, 4, '2017-06-01', '08:51:19', 1, 0),
(382, 190, 4, '2017-06-01', '08:51:23', 6, 1),
(383, 191, 4, '2017-06-01', '08:53:23', 1, 0),
(384, 191, 4, '2017-06-01', '08:53:40', 6, 1),
(385, 192, 4, '2017-06-01', '08:55:08', 1, 0),
(386, 192, 4, '2017-06-01', '08:55:13', 6, 1),
(387, 193, 4, '2017-06-01', '08:59:29', 1, 0),
(388, 193, 4, '2017-06-01', '08:59:45', 6, 1),
(389, 194, 4, '2017-06-01', '09:01:32', 1, 0),
(390, 194, 4, '2017-06-01', '09:01:37', 6, 1),
(391, 195, 4, '2017-06-01', '09:04:13', 1, 0),
(392, 196, 4, '2017-06-01', '09:04:33', 1, 0),
(393, 195, 4, '2017-06-01', '09:04:55', 6, 1),
(394, 196, 4, '2017-06-01', '09:06:37', 6, 1),
(395, 197, 4, '2017-06-01', '09:10:29', 1, 0),
(396, 197, 4, '2017-06-01', '09:11:49', 6, 1),
(397, 198, 4, '2017-06-01', '09:13:03', 1, 0),
(398, 198, 4, '2017-06-01', '09:13:48', 6, 1),
(399, 199, 4, '2017-06-01', '09:16:52', 1, 0),
(400, 199, 4, '2017-06-01', '09:17:08', 6, 1),
(401, 200, 4, '2017-06-01', '09:18:41', 1, 0),
(402, 200, 4, '2017-06-01', '09:18:48', 6, 1),
(403, 201, 4, '2017-06-01', '09:20:57', 1, 0),
(404, 202, 4, '2017-06-01', '09:22:09', 1, 0),
(405, 201, 4, '2017-06-01', '09:22:14', 6, 1),
(406, 203, 4, '2017-06-01', '09:24:00', 1, 0),
(407, 202, 4, '2017-06-01', '09:24:09', 6, 1),
(408, 203, 4, '2017-06-01', '09:24:13', 6, 1),
(409, 204, 4, '2017-06-01', '09:25:36', 1, 0),
(410, 205, 4, '2017-06-01', '09:27:54', 1, 0),
(411, 206, 4, '2017-06-01', '09:27:55', 1, 0),
(412, 204, 4, '2017-06-01', '09:28:03', 6, 1),
(413, 205, 4, '2017-06-01', '09:28:16', 6, 1),
(414, 206, 4, '2017-06-01', '09:30:40', 6, 1),
(415, 207, 4, '2017-06-01', '09:33:09', 1, 0),
(416, 207, 4, '2017-06-01', '09:33:20', 6, 1),
(417, 208, 4, '2017-06-01', '09:36:51', 1, 0),
(418, 209, 4, '2017-06-01', '09:36:54', 1, 0),
(419, 208, 4, '2017-06-01', '09:38:30', 6, 1),
(420, 209, 4, '2017-06-01', '09:38:36', 6, 1),
(421, 210, 4, '2017-06-01', '09:46:43', 1, 0),
(422, 210, 4, '2017-06-01', '09:47:01', 6, 1),
(423, 211, 4, '2017-06-01', '09:53:28', 1, 0),
(424, 211, 4, '2017-06-01', '09:53:49', 6, 1),
(425, 212, 4, '2017-06-01', '09:55:19', 1, 0),
(426, 213, 4, '2017-06-01', '10:03:32', 1, 0),
(427, 212, 4, '2017-06-01', '10:03:51', 6, 1),
(428, 213, 4, '2017-06-01', '10:07:28', 6, 1),
(429, 214, 4, '2017-06-01', '10:10:28', 1, 0),
(430, 214, 4, '2017-06-01', '10:10:32', 6, 1),
(431, 215, 4, '2017-06-01', '10:13:19', 1, 0),
(432, 215, 4, '2017-06-01', '10:13:31', 6, 1),
(433, 216, 4, '2017-06-01', '10:16:00', 1, 0),
(434, 216, 4, '2017-06-01', '10:18:48', 6, 1),
(435, 217, 4, '2017-06-01', '10:23:26', 1, 0),
(436, 217, 4, '2017-06-01', '10:23:30', 6, 1),
(437, 218, 4, '2017-06-01', '10:28:27', 1, 0),
(438, 218, 4, '2017-06-01', '10:28:34', 6, 1),
(439, 219, 4, '2017-06-01', '10:39:09', 1, 0),
(440, 220, 4, '2017-06-01', '10:40:16', 1, 0),
(441, 220, 4, '2017-06-01', '10:40:26', 6, 1),
(442, 219, 4, '2017-06-01', '10:40:29', 6, 1),
(443, 221, 4, '2017-06-01', '10:43:15', 1, 0),
(444, 222, 4, '2017-06-01', '10:44:52', 1, 0),
(445, 222, 4, '2017-06-01', '10:45:17', 6, 1),
(446, 221, 4, '2017-06-01', '10:45:20', 6, 1),
(447, 223, 4, '2017-06-01', '10:48:59', 1, 0),
(448, 224, 4, '2017-06-01', '10:50:51', 1, 0),
(449, 223, 4, '2017-06-01', '10:50:55', 6, 1),
(450, 225, 4, '2017-06-01', '10:52:32', 1, 0),
(451, 225, 4, '2017-06-01', '10:52:52', 6, 1),
(452, 224, 4, '2017-06-01', '10:52:53', 6, 1),
(453, 226, 4, '2017-06-01', '11:06:27', 1, 0),
(454, 226, 4, '2017-06-01', '11:16:51', 6, 1),
(455, 228, 4, '2017-06-01', '11:20:01', 1, 0),
(456, 228, 4, '2017-06-01', '11:20:13', 5, 0),
(457, 228, 4, '2017-06-01', '11:27:45', 6, 1),
(458, 229, 4, '2017-06-01', '11:30:21', 1, 0),
(459, 229, 4, '2017-06-01', '11:30:43', 6, 1),
(460, 230, 4, '2017-06-01', '11:33:22', 1, 0),
(461, 231, 4, '2017-06-01', '11:35:39', 1, 0),
(462, 230, 4, '2017-06-01', '11:36:17', 6, 1),
(463, 231, 4, '2017-06-01', '11:36:23', 6, 1),
(464, 232, 4, '2017-06-01', '11:38:45', 1, 0),
(465, 232, 4, '2017-06-01', '11:39:10', 6, 1),
(466, 233, 4, '2017-06-01', '11:50:02', 1, 0),
(467, 234, 4, '2017-06-01', '11:52:12', 1, 0),
(468, 233, 4, '2017-06-01', '11:52:20', 6, 1),
(469, 234, 4, '2017-06-01', '11:52:22', 6, 1),
(470, 235, 4, '2017-06-01', '11:58:10', 1, 0),
(471, 235, 4, '2017-06-01', '11:58:17', 6, 1),
(472, 236, 4, '2017-06-01', '12:04:14', 1, 0),
(473, 236, 4, '2017-06-01', '12:04:24', 6, 1),
(474, 237, 4, '2017-06-01', '12:06:21', 1, 0),
(475, 237, 4, '2017-06-01', '12:06:29', 6, 1),
(476, 238, 4, '2017-06-01', '12:09:36', 1, 0),
(477, 238, 4, '2017-06-01', '12:09:43', 6, 1),
(478, 239, 4, '2017-06-01', '12:13:26', 1, 0),
(479, 239, 4, '2017-06-01', '12:13:32', 6, 1),
(480, 240, 4, '2017-06-01', '12:16:28', 1, 0),
(481, 241, 4, '2017-06-01', '12:18:34', 1, 0),
(482, 241, 4, '2017-06-01', '12:18:58', 6, 1),
(483, 240, 4, '2017-06-01', '12:19:00', 6, 1),
(484, 242, 4, '2017-06-01', '12:22:59', 1, 0),
(485, 243, 4, '2017-06-01', '12:23:01', 1, 0),
(486, 242, 4, '2017-06-01', '12:23:53', 6, 1),
(487, 243, 4, '2017-06-01', '12:23:56', 6, 1),
(488, 244, 4, '2017-06-01', '12:26:02', 1, 0),
(489, 245, 4, '2017-06-01', '12:26:05', 1, 0),
(490, 245, 4, '2017-06-01', '12:26:39', 6, 1),
(491, 244, 4, '2017-06-01', '12:26:41', 6, 1),
(492, 246, 4, '2017-06-02', '08:20:54', 1, 0),
(493, 246, 4, '2017-06-02', '08:21:58', 6, 1),
(494, 247, 4, '2017-06-02', '08:31:13', 1, 0),
(495, 248, 4, '2017-06-02', '08:31:13', 1, 0),
(496, 248, 4, '2017-06-02', '08:32:18', 6, 1),
(497, 247, 4, '2017-06-02', '08:32:20', 6, 1),
(498, 249, 4, '2017-06-02', '08:33:50', 1, 0),
(499, 249, 4, '2017-06-02', '08:34:15', 6, 1),
(500, 250, 4, '2017-06-02', '08:35:54', 1, 0),
(501, 251, 4, '2017-06-02', '08:35:55', 1, 0),
(502, 252, 4, '2017-06-02', '08:35:57', 1, 0),
(503, 252, 4, '2017-06-02', '08:42:20', 6, 1),
(504, 251, 4, '2017-06-02', '08:44:31', 6, 1),
(505, 250, 4, '2017-06-02', '08:44:33', 6, 1),
(506, 253, 4, '2017-06-02', '08:51:02', 1, 0),
(507, 254, 4, '2017-06-02', '08:51:02', 1, 0),
(508, 255, 4, '2017-06-02', '08:51:02', 1, 0),
(509, 256, 4, '2017-06-02', '08:51:02', 1, 0),
(510, 257, 4, '2017-06-02', '08:51:03', 1, 0),
(511, 257, 4, '2017-06-02', '09:07:24', 6, 1),
(512, 256, 4, '2017-06-02', '09:07:26', 6, 1),
(513, 255, 4, '2017-06-02', '09:08:24', 6, 1),
(514, 254, 4, '2017-06-02', '09:20:37', 6, 1),
(515, 253, 4, '2017-06-02', '09:20:48', 6, 1),
(516, 258, 4, '2017-06-02', '09:24:46', 1, 0),
(517, 259, 4, '2017-06-02', '09:24:48', 1, 0),
(518, 260, 4, '2017-06-02', '09:34:13', 1, 0),
(519, 260, 4, '2017-06-02', '09:48:29', 6, 1),
(520, 259, 4, '2017-06-02', '09:48:59', 6, 1),
(521, 258, 4, '2017-06-02', '09:49:01', 6, 1),
(522, 261, 4, '2017-06-02', '09:50:38', 1, 0),
(523, 262, 4, '2017-06-02', '09:50:38', 1, 0),
(524, 262, 4, '2017-06-02', '09:51:31', 6, 1),
(525, 261, 4, '2017-06-02', '09:52:35', 6, 1),
(526, 263, 4, '2017-06-02', '10:01:55', 1, 0),
(527, 264, 4, '2017-06-02', '10:01:55', 1, 0),
(528, 265, 4, '2017-06-02', '10:01:55', 1, 0),
(529, 265, 4, '2017-06-02', '10:02:49', 6, 1),
(530, 263, 4, '2017-06-02', '10:02:55', 6, 1),
(531, 264, 4, '2017-06-02', '10:03:11', 6, 1),
(532, 266, 4, '2017-06-02', '10:05:08', 1, 0),
(533, 267, 4, '2017-06-02', '10:05:15', 1, 0),
(534, 268, 4, '2017-06-02', '10:05:15', 1, 0),
(535, 268, 4, '2017-06-02', '10:06:06', 6, 1),
(536, 267, 4, '2017-06-02', '10:07:12', 6, 1),
(537, 266, 4, '2017-06-02', '10:07:14', 6, 1),
(538, 269, 4, '2017-06-02', '10:12:19', 1, 0),
(539, 270, 4, '2017-06-02', '10:12:19', 1, 0),
(540, 271, 4, '2017-06-02', '10:12:20', 1, 0),
(541, 272, 4, '2017-06-02', '10:12:20', 1, 0),
(542, 269, 4, '2017-06-02', '10:13:12', 6, 1),
(543, 270, 4, '2017-06-02', '10:14:44', 6, 1),
(544, 271, 4, '2017-06-02', '10:16:46', 6, 1),
(545, 272, 4, '2017-06-02', '10:16:48', 6, 1),
(546, 273, 4, '2017-06-02', '10:20:09', 1, 0),
(547, 273, 4, '2017-06-02', '10:20:24', 6, 1),
(548, 274, 4, '2017-06-02', '10:22:30', 1, 0),
(549, 275, 4, '2017-06-02', '10:22:30', 1, 0),
(550, 275, 4, '2017-06-02', '10:26:54', 6, 1),
(551, 274, 4, '2017-06-02', '10:26:56', 6, 1),
(552, 276, 4, '2017-06-02', '10:29:05', 1, 0),
(553, 277, 4, '2017-06-02', '10:29:05', 1, 0),
(554, 277, 4, '2017-06-02', '10:33:03', 6, 1),
(555, 276, 4, '2017-06-02', '10:33:08', 6, 1),
(556, 278, 4, '2017-06-02', '10:38:28', 1, 0),
(557, 279, 4, '2017-06-02', '10:38:29', 1, 0),
(558, 278, 4, '2017-06-02', '10:39:51', 6, 1),
(559, 279, 4, '2017-06-02', '10:40:41', 6, 1),
(560, 280, 4, '2017-06-02', '10:42:46', 1, 0),
(561, 281, 4, '2017-06-02', '10:44:33', 1, 0),
(562, 282, 4, '2017-06-02', '10:44:34', 1, 0),
(563, 280, 4, '2017-06-02', '10:44:50', 6, 1),
(564, 282, 4, '2017-06-02', '10:46:58', 6, 1),
(565, 281, 4, '2017-06-02', '11:09:44', 6, 1),
(566, 283, 4, '2017-06-02', '11:44:33', 1, 0),
(567, 284, 4, '2017-06-02', '11:44:36', 1, 0),
(568, 285, 4, '2017-06-02', '11:44:37', 1, 0),
(569, 286, 4, '2017-06-02', '11:44:37', 1, 0),
(570, 286, 4, '2017-06-02', '11:50:07', 6, 1),
(571, 285, 4, '2017-06-02', '11:50:07', 6, 1),
(572, 284, 4, '2017-06-02', '11:50:10', 6, 1),
(573, 283, 4, '2017-06-02', '11:50:20', 5, 0),
(574, 283, 4, '2017-06-02', '11:50:24', 6, 1),
(575, 287, 4, '2017-06-02', '11:57:29', 1, 0),
(576, 288, 4, '2017-06-02', '11:57:29', 1, 0),
(577, 288, 4, '2017-06-02', '11:57:56', 6, 1),
(578, 287, 4, '2017-06-02', '11:57:58', 6, 1),
(579, 289, 4, '2017-06-02', '12:04:47', 1, 0),
(580, 290, 4, '2017-06-02', '12:04:48', 1, 0),
(581, 291, 4, '2017-06-02', '12:04:48', 1, 0),
(582, 291, 4, '2017-06-02', '12:08:26', 6, 1),
(583, 290, 4, '2017-06-02', '12:08:26', 6, 1),
(584, 289, 4, '2017-06-02', '12:08:26', 6, 1),
(585, 292, 4, '2017-06-02', '12:10:24', 1, 0),
(586, 293, 4, '2017-06-02', '12:10:26', 1, 0),
(587, 292, 4, '2017-06-02', '12:12:40', 6, 1),
(588, 293, 4, '2017-06-02', '12:12:42', 6, 1),
(589, 294, 4, '2017-06-05', '07:28:26', 1, 0),
(590, 295, 4, '2017-06-05', '07:28:26', 1, 0),
(591, 296, 4, '2017-06-05', '07:28:26', 1, 0),
(592, 297, 4, '2017-06-05', '07:28:27', 1, 0),
(593, 297, 4, '2017-06-05', '07:31:29', 6, 1),
(594, 296, 4, '2017-06-05', '07:31:32', 6, 1),
(595, 294, 4, '2017-06-05', '07:31:36', 6, 1),
(596, 295, 4, '2017-06-05', '07:33:25', 6, 1),
(597, 298, 4, '2017-06-05', '07:59:51', 1, 0),
(598, 298, 4, '2017-06-05', '08:00:45', 6, 1),
(599, 299, 4, '2017-06-05', '08:02:28', 1, 0),
(600, 299, 4, '2017-06-05', '08:06:51', 6, 1),
(601, 300, 4, '2017-06-05', '08:12:20', 1, 0),
(602, 301, 4, '2017-06-05', '08:12:21', 1, 0),
(603, 300, 4, '2017-06-05', '08:17:05', 6, 1),
(604, 301, 4, '2017-06-05', '08:17:07', 6, 1),
(605, 302, 4, '2017-06-05', '08:19:44', 1, 0),
(606, 303, 4, '2017-06-05', '08:19:44', 1, 0),
(607, 302, 4, '2017-06-05', '08:26:23', 6, 1),
(608, 303, 4, '2017-06-05', '08:26:25', 6, 1),
(609, 304, 4, '2017-06-05', '08:33:20', 1, 0),
(610, 305, 4, '2017-06-05', '08:33:23', 1, 0),
(611, 306, 4, '2017-06-05', '08:33:24', 1, 0),
(612, 306, 4, '2017-06-05', '08:36:05', 6, 1),
(613, 305, 4, '2017-06-05', '08:36:06', 6, 1),
(614, 304, 4, '2017-06-05', '08:42:18', 6, 1),
(615, 307, 4, '2017-06-05', '08:50:03', 1, 0),
(616, 308, 4, '2017-06-05', '08:50:03', 1, 0),
(617, 307, 4, '2017-06-05', '08:51:15', 6, 1),
(618, 308, 4, '2017-06-05', '08:51:18', 6, 1),
(619, 309, 4, '2017-06-05', '08:56:03', 1, 0),
(620, 310, 4, '2017-06-05', '08:56:03', 1, 0),
(621, 311, 4, '2017-06-05', '08:56:04', 1, 0),
(622, 311, 4, '2017-06-05', '08:57:08', 6, 1),
(623, 310, 4, '2017-06-05', '08:57:09', 6, 1),
(624, 309, 4, '2017-06-05', '08:57:11', 6, 1),
(625, 312, 4, '2017-06-05', '08:59:32', 1, 0),
(626, 313, 4, '2017-06-05', '08:59:33', 1, 0),
(627, 314, 4, '2017-06-05', '08:59:33', 1, 0),
(628, 314, 4, '2017-06-05', '09:00:36', 6, 1),
(629, 313, 4, '2017-06-05', '09:00:38', 6, 1),
(630, 312, 4, '2017-06-05', '09:00:39', 6, 1),
(631, 315, 4, '2017-06-05', '09:11:01', 1, 0),
(632, 316, 4, '2017-06-05', '09:11:03', 1, 0),
(633, 317, 4, '2017-06-05', '09:11:04', 1, 0),
(634, 317, 4, '2017-06-05', '09:13:07', 6, 1),
(635, 316, 4, '2017-06-05', '09:13:08', 6, 1),
(636, 315, 4, '2017-06-05', '09:13:10', 6, 1),
(637, 318, 4, '2017-06-05', '09:26:02', 1, 0),
(638, 319, 4, '2017-06-05', '09:27:26', 1, 0),
(639, 319, 4, '2017-06-05', '10:11:26', 6, 1),
(640, 318, 4, '2017-06-05', '10:11:27', 6, 1),
(641, 320, 4, '2017-06-05', '10:19:45', 1, 0),
(642, 320, 4, '2017-06-05', '10:19:59', 6, 1),
(643, 321, 4, '2017-06-05', '10:22:11', 1, 0),
(644, 322, 4, '2017-06-05', '10:22:12', 1, 0),
(645, 322, 4, '2017-06-05', '10:22:19', 6, 1),
(646, 321, 4, '2017-06-05', '10:22:51', 6, 1),
(647, 323, 4, '2017-06-05', '10:31:21', 1, 1),
(648, 324, 4, '2017-06-05', '10:31:22', 1, 1),
(649, 325, 4, '2017-06-05', '10:31:22', 1, 1),
(650, 326, 4, '2017-06-05', '10:31:22', 1, 1),
(651, 327, 4, '2017-06-05', '10:31:22', 1, 0),
(652, 328, 4, '2017-06-05', '10:31:22', 1, 0),
(653, 329, 4, '2017-06-05', '10:31:24', 1, 0),
(654, 329, 4, '2017-06-05', '10:39:39', 6, 1),
(655, 328, 4, '2017-06-05', '10:39:40', 6, 1),
(656, 327, 4, '2017-06-05', '10:39:41', 6, 1),
(657, 330, 4, '2017-06-05', '11:04:32', 1, 0),
(658, 331, 4, '2017-06-05', '11:04:32', 1, 0),
(659, 332, 4, '2017-06-05', '11:04:32', 1, 0),
(660, 333, 4, '2017-06-05', '11:04:33', 1, 0),
(661, 333, 4, '2017-06-05', '11:08:01', 6, 1),
(662, 332, 4, '2017-06-05', '11:08:44', 6, 1),
(663, 331, 4, '2017-06-05', '11:09:08', 6, 1),
(664, 330, 4, '2017-06-05', '11:09:15', 6, 1),
(665, 334, 4, '2017-06-05', '11:25:12', 1, 0),
(666, 335, 4, '2017-06-05', '11:25:13', 1, 0),
(667, 335, 4, '2017-06-05', '11:28:43', 6, 1),
(668, 334, 4, '2017-06-05', '11:28:45', 5, 0),
(669, 336, 4, '2017-06-05', '11:31:13', 1, 0),
(670, 334, 4, '2017-06-05', '11:31:53', 6, 1),
(671, 336, 4, '2017-06-05', '11:31:56', 6, 1),
(672, 337, 4, '2017-06-05', '11:36:15', 1, 0),
(673, 338, 4, '2017-06-05', '11:36:16', 1, 0),
(674, 339, 4, '2017-06-05', '11:36:16', 1, 0),
(675, 339, 4, '2017-06-05', '11:47:10', 6, 1),
(676, 338, 4, '2017-06-05', '11:47:12', 6, 1),
(677, 337, 4, '2017-06-05', '11:47:15', 6, 1),
(678, 340, 4, '2017-06-05', '11:49:21', 1, 0),
(679, 341, 4, '2017-06-05', '11:49:21', 1, 0),
(680, 342, 4, '2017-06-05', '11:49:21', 1, 0),
(681, 342, 4, '2017-06-05', '11:53:21', 6, 1),
(682, 341, 4, '2017-06-05', '11:53:56', 6, 1),
(683, 340, 4, '2017-06-05', '11:53:57', 6, 1),
(684, 343, 4, '2017-06-05', '11:56:28', 1, 0),
(685, 344, 4, '2017-06-05', '11:56:28', 1, 0),
(686, 344, 4, '2017-06-05', '11:57:16', 6, 1),
(687, 343, 4, '2017-06-05', '11:57:18', 6, 1),
(688, 345, 4, '2017-06-05', '11:59:40', 1, 0),
(689, 346, 4, '2017-06-05', '11:59:40', 1, 0),
(690, 346, 4, '2017-06-05', '12:00:37', 6, 1),
(691, 345, 4, '2017-06-05', '12:01:02', 6, 1),
(692, 347, 4, '2017-06-05', '12:03:10', 1, 0),
(693, 348, 4, '2017-06-05', '12:03:11', 1, 0),
(694, 348, 4, '2017-06-05', '12:06:55', 6, 1),
(695, 347, 4, '2017-06-05', '12:06:58', 6, 1),
(696, 349, 4, '2017-06-05', '12:09:31', 1, 0),
(697, 350, 4, '2017-06-05', '12:09:31', 1, 0),
(698, 350, 4, '2017-06-05', '12:10:36', 6, 1),
(699, 349, 4, '2017-06-05', '12:10:38', 6, 1),
(700, 351, 4, '2017-06-06', '07:19:04', 1, 0),
(701, 351, 4, '2017-06-06', '07:19:16', 6, 1),
(702, 352, 4, '2017-06-06', '07:21:13', 1, 0),
(703, 353, 4, '2017-06-06', '07:21:14', 1, 0),
(704, 354, 4, '2017-06-06', '07:21:14', 1, 0),
(705, 354, 4, '2017-06-06', '07:22:16', 6, 1),
(706, 353, 4, '2017-06-06', '07:22:18', 6, 1),
(707, 352, 4, '2017-06-06', '07:22:20', 6, 1),
(708, 355, 4, '2017-06-06', '07:27:41', 1, 0),
(709, 356, 4, '2017-06-06', '07:27:42', 1, 0),
(710, 357, 4, '2017-06-06', '07:27:42', 1, 0),
(711, 358, 4, '2017-06-06', '07:27:43', 1, 0),
(712, 358, 4, '2017-06-06', '07:39:19', 6, 1),
(713, 357, 4, '2017-06-06', '07:39:21', 6, 1),
(714, 356, 4, '2017-06-06', '07:39:22', 6, 1),
(715, 355, 4, '2017-06-06', '07:42:14', 6, 1),
(716, 359, 4, '2017-06-06', '07:45:54', 1, 0),
(717, 360, 4, '2017-06-06', '07:50:10', 1, 0),
(718, 361, 4, '2017-06-06', '07:50:11', 1, 0),
(719, 362, 4, '2017-06-06', '07:50:13', 1, 0),
(720, 363, 4, '2017-06-06', '07:50:14', 1, 0),
(721, 364, 4, '2017-06-06', '07:50:14', 1, 0),
(722, 365, 4, '2017-06-06', '07:50:14', 1, 0),
(723, 364, 4, '2017-06-06', '07:54:42', 6, 1),
(724, 363, 4, '2017-06-06', '07:54:43', 6, 1),
(725, 362, 4, '2017-06-06', '07:54:44', 6, 1),
(726, 361, 4, '2017-06-06', '07:54:45', 5, 0),
(727, 361, 4, '2017-06-06', '07:54:48', 6, 1),
(728, 365, 4, '2017-06-06', '07:54:56', 6, 1),
(729, 359, 4, '2017-06-06', '08:02:05', 6, 1),
(730, 360, 4, '2017-06-06', '08:02:07', 5, 0),
(731, 360, 4, '2017-06-06', '08:02:08', 6, 1),
(732, 366, 4, '2017-06-06', '08:04:43', 1, 0),
(733, 366, 4, '2017-06-06', '08:06:42', 6, 1),
(734, 367, 4, '2017-06-06', '08:08:39', 1, 0),
(735, 368, 4, '2017-06-06', '08:08:39', 1, 0),
(736, 368, 4, '2017-06-06', '08:14:23', 6, 1),
(737, 367, 4, '2017-06-06', '08:14:24', 6, 1),
(738, 369, 4, '2017-06-06', '08:17:18', 1, 0),
(739, 369, 4, '2017-06-06', '08:17:29', 6, 1),
(740, 63, 4, '2017-06-06', '08:28:02', 5, 0),
(741, 63, 4, '2017-06-06', '08:28:06', 2, 0),
(742, 63, 4, '2017-06-06', '08:28:09', 3, 1),
(743, 65, 4, '2017-06-06', '08:29:49', 3, 1),
(744, 60, 4, '2017-06-06', '08:32:18', 3, 1),
(745, 59, 4, '2017-06-06', '08:34:53', 3, 1),
(746, 58, 4, '2017-06-06', '08:36:35', 3, 1),
(747, 57, 4, '2017-06-06', '08:37:56', 3, 1),
(748, 56, 4, '2017-06-06', '08:39:35', 3, 1),
(749, 55, 4, '2017-06-06', '08:47:34', 3, 1),
(750, 54, 4, '2017-06-06', '08:48:56', 3, 1),
(751, 53, 4, '2017-06-06', '08:50:18', 3, 1),
(752, 52, 4, '2017-06-06', '08:59:57', 3, 1),
(753, 51, 4, '2017-06-06', '09:01:14', 3, 1),
(754, 50, 4, '2017-06-06', '09:01:58', 3, 1),
(755, 49, 4, '2017-06-06', '09:02:37', 3, 1),
(756, 370, 4, '2017-06-06', '09:07:48', 1, 0),
(757, 370, 4, '2017-06-06', '09:07:54', 3, 1),
(758, 371, 4, '2017-06-06', '09:13:28', 1, 0),
(759, 371, 4, '2017-06-06', '09:13:35', 3, 1),
(760, 372, 4, '2017-06-06', '09:34:35', 1, 0),
(761, 372, 4, '2017-06-06', '09:34:48', 6, 1),
(762, 373, 4, '2017-06-06', '09:39:05', 1, 0),
(763, 374, 4, '2017-06-06', '09:39:06', 1, 0),
(764, 375, 4, '2017-06-06', '09:39:06', 1, 0),
(765, 375, 4, '2017-06-06', '09:43:30', 6, 1),
(766, 374, 4, '2017-06-06', '09:44:08', 6, 1),
(767, 373, 4, '2017-06-06', '09:44:11', 6, 1),
(768, 376, 4, '2017-06-06', '10:12:56', 1, 0),
(769, 376, 4, '2017-06-06', '10:13:27', 6, 1),
(770, 377, 4, '2017-06-06', '10:16:10', 1, 0),
(771, 377, 4, '2017-06-06', '10:19:25', 6, 1),
(772, 378, 4, '2017-06-06', '10:27:36', 1, 0),
(773, 378, 4, '2017-06-06', '10:27:41', 5, 0),
(774, 378, 4, '2017-06-06', '10:28:28', 6, 1),
(775, 379, 4, '2017-06-06', '10:30:49', 1, 0),
(776, 380, 4, '2017-06-06', '10:30:49', 1, 0),
(777, 381, 4, '2017-06-06', '10:30:49', 1, 0),
(778, 382, 4, '2017-06-06', '12:34:45', 1, 0),
(779, 383, 4, '2017-06-06', '12:37:56', 1, 0),
(780, 384, 4, '2017-06-06', '12:41:01', 1, 0),
(781, 385, 4, '2017-06-06', '12:45:25', 1, 0),
(782, 385, 4, '2017-06-06', '12:51:40', 3, 0),
(783, 384, 4, '2017-06-06', '12:51:44', 3, 0),
(784, 383, 4, '2017-06-06', '12:51:47', 2, 0),
(785, 382, 4, '2017-06-06', '12:51:50', 3, 0),
(786, 384, 4, '2017-06-06', '12:52:02', 2, 0),
(787, 382, 4, '2017-06-06', '12:52:04', 2, 0),
(788, 385, 4, '2017-06-06', '12:52:06', 2, 0),
(789, 380, 4, '2017-06-07', '08:51:36', 6, 1),
(790, 379, 4, '2017-06-07', '08:51:38', 6, 1),
(791, 381, 4, '2017-06-07', '08:51:39', 6, 1),
(792, 386, 4, '2017-06-07', '08:54:25', 1, 0),
(793, 386, 4, '2017-06-07', '08:54:39', 6, 1),
(794, 387, 4, '2017-06-07', '08:56:29', 1, 0),
(795, 388, 4, '2017-06-07', '08:56:29', 1, 0),
(796, 387, 4, '2017-06-07', '09:01:07', 5, 0),
(797, 388, 4, '2017-06-07', '09:01:09', 6, 1),
(798, 389, 4, '2017-06-07', '09:03:17', 1, 0),
(799, 387, 4, '2017-06-07', '09:05:09', 6, 1),
(800, 389, 4, '2017-06-07', '09:05:09', 6, 1),
(801, 390, 4, '2017-06-07', '09:07:06', 1, 0),
(802, 390, 4, '2017-06-07', '09:07:58', 6, 1),
(803, 391, 4, '2017-06-07', '09:09:34', 1, 0),
(804, 392, 4, '2017-06-07', '09:19:50', 1, 0),
(805, 393, 4, '2017-06-07', '09:19:51', 1, 0),
(806, 391, 4, '2017-06-07', '09:20:02', 6, 1),
(807, 392, 4, '2017-06-07', '09:20:07', 6, 1),
(808, 393, 4, '2017-06-07', '09:21:59', 6, 1),
(809, 394, 4, '2017-06-09', '07:41:52', 1, 0),
(810, 395, 4, '2017-06-09', '07:41:52', 1, 0),
(811, 396, 4, '2017-06-09', '07:41:52', 1, 0),
(812, 396, 4, '2017-06-09', '07:46:41', 6, 1),
(813, 395, 4, '2017-06-09', '07:46:43', 6, 1),
(814, 394, 4, '2017-06-09', '07:46:44', 6, 1),
(815, 397, 4, '2017-06-09', '07:56:46', 1, 0),
(816, 397, 4, '2017-06-09', '07:57:17', 6, 1),
(817, 398, 4, '2017-06-09', '08:06:21', 1, 0),
(818, 399, 4, '2017-06-09', '08:06:21', 1, 0),
(819, 398, 4, '2017-06-09', '08:09:45', 6, 1),
(820, 399, 4, '2017-06-09', '08:09:47', 6, 1),
(821, 400, 4, '2017-06-09', '08:31:03', 1, 0),
(822, 401, 4, '2017-06-09', '08:31:03', 1, 0),
(823, 402, 4, '2017-06-09', '08:31:04', 1, 0),
(824, 403, 4, '2017-06-09', '08:31:04', 1, 0),
(825, 404, 4, '2017-06-09', '08:31:04', 1, 0),
(826, 404, 4, '2017-06-09', '08:35:13', 6, 1),
(827, 401, 4, '2017-06-09', '08:35:45', 6, 1),
(828, 402, 4, '2017-06-09', '08:36:40', 6, 1),
(829, 403, 4, '2017-06-09', '08:36:42', 6, 1),
(830, 400, 4, '2017-06-09', '08:37:20', 6, 1),
(831, 405, 4, '2017-06-09', '08:40:40', 1, 0),
(832, 406, 4, '2017-06-09', '08:40:40', 1, 0),
(833, 407, 4, '2017-06-09', '08:40:40', 1, 0),
(834, 408, 4, '2017-06-09', '08:40:41', 1, 0),
(835, 408, 4, '2017-06-09', '08:41:39', 6, 1),
(836, 407, 4, '2017-06-09', '08:42:32', 6, 1),
(837, 406, 4, '2017-06-09', '08:42:35', 6, 1),
(838, 405, 4, '2017-06-09', '08:43:49', 6, 1),
(839, 409, 4, '2017-06-09', '08:45:44', 1, 0),
(840, 409, 4, '2017-06-09', '08:46:03', 6, 1),
(841, 410, 4, '2017-06-09', '08:48:47', 1, 0),
(842, 411, 4, '2017-06-09', '08:48:47', 1, 0),
(843, 411, 4, '2017-06-09', '08:50:56', 6, 1),
(844, 410, 4, '2017-06-09', '08:50:58', 6, 1),
(845, 412, 4, '2017-06-09', '08:53:22', 1, 0),
(846, 413, 4, '2017-06-09', '08:53:22', 1, 0),
(847, 414, 4, '2017-06-09', '08:53:22', 1, 0),
(848, 414, 4, '2017-06-09', '08:54:45', 6, 1),
(849, 413, 4, '2017-06-09', '08:54:47', 6, 1),
(850, 412, 4, '2017-06-09', '08:56:22', 6, 1),
(851, 415, 4, '2017-06-09', '09:08:49', 1, 0),
(852, 416, 4, '2017-06-09', '09:08:49', 1, 0),
(853, 417, 4, '2017-06-09', '09:08:50', 1, 0),
(854, 418, 4, '2017-06-09', '09:08:50', 1, 0),
(855, 419, 4, '2017-06-09', '09:18:21', 1, 0),
(856, 420, 4, '2017-06-09', '09:18:21', 1, 0),
(857, 421, 4, '2017-06-09', '09:18:21', 1, 0),
(858, 422, 4, '2017-06-09', '09:18:21', 1, 0),
(859, 418, 4, '2017-06-09', '09:18:42', 6, 1),
(860, 417, 4, '2017-06-09', '09:18:45', 6, 1),
(861, 416, 4, '2017-06-09', '09:18:46', 6, 1),
(862, 415, 4, '2017-06-09', '09:18:47', 6, 1),
(863, 419, 4, '2017-06-09', '09:18:56', 6, 1),
(864, 422, 4, '2017-06-09', '09:21:56', 6, 1),
(865, 421, 4, '2017-06-09', '09:22:45', 6, 1),
(866, 420, 4, '2017-06-09', '09:23:47', 6, 1),
(867, 423, 4, '2017-06-09', '09:34:03', 1, 0),
(868, 424, 4, '2017-06-09', '09:34:03', 1, 0),
(869, 425, 4, '2017-06-09', '09:34:03', 1, 0),
(870, 426, 4, '2017-06-09', '09:34:05', 1, 0),
(871, 427, 4, '2017-06-09', '09:35:54', 1, 0),
(872, 427, 4, '2017-06-09', '09:44:06', 6, 1),
(873, 426, 4, '2017-06-09', '09:44:41', 6, 1),
(874, 425, 4, '2017-06-09', '09:49:20', 6, 1),
(875, 424, 4, '2017-06-09', '09:50:23', 6, 1),
(876, 423, 4, '2017-06-09', '09:51:55', 6, 1),
(877, 428, 4, '2017-06-09', '09:58:34', 1, 0),
(878, 429, 4, '2017-06-09', '09:58:35', 1, 0),
(879, 430, 4, '2017-06-09', '09:58:35', 1, 0),
(880, 430, 4, '2017-06-09', '10:01:08', 6, 1),
(881, 429, 4, '2017-06-09', '10:01:10', 6, 1),
(882, 428, 4, '2017-06-09', '10:01:10', 6, 1),
(883, 431, 4, '2017-06-09', '10:03:29', 1, 0),
(884, 432, 4, '2017-06-09', '10:03:29', 1, 0),
(885, 432, 4, '2017-06-09', '10:04:22', 6, 1),
(886, 431, 4, '2017-06-09', '10:04:24', 6, 1),
(887, 433, 4, '2017-06-09', '10:09:58', 1, 0),
(888, 434, 4, '2017-06-09', '10:12:39', 1, 0),
(889, 435, 4, '2017-06-09', '10:12:45', 1, 1),
(890, 433, 4, '2017-06-09', '10:13:03', 6, 1),
(891, 436, 4, '2017-06-09', '10:15:03', 1, 0),
(892, 437, 4, '2017-06-09', '10:15:03', 1, 0),
(893, 438, 4, '2017-06-09', '10:15:07', 1, 1),
(894, 434, 4, '2017-06-09', '10:16:19', 6, 1),
(895, 436, 4, '2017-06-09', '10:16:47', 6, 1),
(896, 437, 4, '2017-06-09', '10:17:42', 6, 1),
(897, 439, 4, '2017-06-09', '10:22:48', 1, 0),
(898, 440, 4, '2017-06-09', '10:26:08', 1, 0),
(899, 441, 4, '2017-06-09', '10:26:09', 1, 0),
(900, 442, 4, '2017-06-09', '10:26:09', 1, 0),
(901, 443, 4, '2017-06-09', '10:26:09', 1, 0),
(902, 444, 4, '2017-06-09', '10:26:09', 1, 0),
(903, 439, 4, '2017-06-09', '10:29:27', 6, 1),
(904, 444, 4, '2017-06-09', '10:32:20', 6, 1),
(905, 443, 4, '2017-06-09', '10:49:47', 6, 1),
(906, 442, 4, '2017-06-09', '10:51:29', 6, 1),
(907, 441, 4, '2017-06-09', '10:52:47', 6, 1),
(908, 440, 4, '2017-06-09', '10:52:58', 6, 1),
(909, 445, 4, '2017-06-09', '10:57:20', 1, 0),
(910, 446, 4, '2017-06-09', '10:57:23', 1, 0),
(911, 447, 4, '2017-06-09', '10:57:23', 1, 0),
(912, 447, 4, '2017-06-09', '10:57:59', 6, 1),
(913, 446, 4, '2017-06-09', '10:58:56', 6, 1),
(914, 445, 4, '2017-06-09', '10:59:55', 6, 1),
(915, 448, 4, '2017-06-09', '11:03:33', 1, 0),
(916, 449, 4, '2017-06-09', '11:03:40', 1, 0),
(917, 450, 4, '2017-06-09', '11:03:41', 1, 0),
(918, 449, 4, '2017-06-09', '11:04:40', 6, 0),
(919, 449, 4, '2017-06-09', '11:07:28', 2, 0),
(920, 450, 4, '2017-06-09', '11:07:32', 2, 0),
(921, 448, 4, '2017-06-09', '11:07:35', 2, 0),
(922, 451, 4, '2017-06-09', '11:15:23', 1, 0),
(923, 451, 4, '2017-06-09', '11:15:38', 2, 0),
(924, 452, 4, '2017-06-09', '11:19:56', 1, 0),
(925, 453, 4, '2017-06-09', '11:19:57', 1, 0),
(926, 454, 4, '2017-06-09', '11:19:58', 1, 0),
(927, 454, 4, '2017-06-09', '11:23:13', 6, 0),
(928, 453, 4, '2017-06-09', '11:23:14', 6, 0),
(929, 454, 4, '2017-06-09', '11:24:21', 2, 0),
(930, 453, 4, '2017-06-09', '11:24:24', 2, 0),
(931, 452, 4, '2017-06-09', '11:24:25', 2, 0),
(932, 455, 4, '2017-06-09', '11:27:05', 1, 0),
(933, 456, 4, '2017-06-09', '11:33:06', 1, 0),
(934, 457, 4, '2017-06-09', '11:33:06', 1, 0),
(935, 458, 4, '2017-06-09', '11:33:06', 1, 0),
(936, 458, 4, '2017-06-09', '11:33:55', 6, 0),
(937, 457, 4, '2017-06-09', '11:33:56', 6, 0),
(938, 456, 4, '2017-06-09', '11:34:14', 2, 0),
(939, 457, 4, '2017-06-09', '11:34:18', 2, 0),
(940, 459, 4, '2017-06-09', '11:36:47', 1, 0),
(941, 460, 4, '2017-06-09', '11:36:47', 1, 0),
(942, 461, 4, '2017-06-09', '11:36:48', 1, 0),
(943, 462, 4, '2017-06-09', '11:36:48', 1, 0),
(944, 463, 4, '2017-06-09', '11:36:48', 1, 0),
(945, 458, 4, '2017-06-09', '11:37:00', 2, 0),
(946, 463, 4, '2017-06-09', '11:38:26', 2, 0),
(947, 462, 4, '2017-06-09', '11:38:28', 2, 0),
(948, 461, 4, '2017-06-09', '11:39:07', 2, 0),
(949, 460, 4, '2017-06-09', '11:39:52', 2, 0),
(950, 459, 4, '2017-06-09', '11:42:39', 2, 0),
(951, 464, 4, '2017-06-09', '11:46:12', 1, 0),
(952, 465, 4, '2017-06-09', '11:46:12', 1, 0),
(953, 466, 4, '2017-06-09', '11:46:12', 1, 0),
(954, 466, 4, '2017-06-09', '11:46:49', 2, 0),
(955, 465, 4, '2017-06-09', '11:46:50', 2, 0),
(956, 464, 4, '2017-06-09', '11:46:52', 2, 0),
(957, 467, 4, '2017-06-12', '07:33:37', 1, 0),
(958, 468, 4, '2017-06-12', '07:37:55', 1, 0),
(959, 469, 4, '2017-06-12', '07:37:55', 1, 0),
(960, 470, 4, '2017-06-12', '07:37:55', 1, 0),
(961, 468, 4, '2017-06-12', '08:08:25', 6, 1),
(962, 467, 4, '2017-06-12', '08:08:27', 6, 1),
(963, 469, 4, '2017-06-12', '08:08:31', 6, 1),
(964, 470, 4, '2017-06-12', '08:09:13', 6, 1),
(965, 471, 4, '2017-06-12', '08:12:55', 1, 0),
(966, 471, 4, '2017-06-12', '08:20:13', 6, 1),
(967, 472, 4, '2017-06-12', '08:37:37', 1, 0),
(968, 473, 4, '2017-06-12', '08:37:37', 1, 0),
(969, 474, 4, '2017-06-12', '08:37:38', 1, 0),
(970, 475, 4, '2017-06-12', '08:37:38', 1, 0),
(971, 476, 4, '2017-06-12', '08:37:38', 1, 0),
(972, 476, 4, '2017-06-12', '08:38:23', 6, 1),
(973, 475, 4, '2017-06-12', '08:39:36', 6, 1),
(974, 474, 4, '2017-06-12', '08:44:38', 6, 1),
(975, 473, 4, '2017-06-12', '08:50:45', 6, 1),
(976, 472, 4, '2017-06-12', '08:59:38', 6, 1),
(977, 477, 4, '2017-06-12', '09:06:46', 1, 0),
(978, 477, 4, '2017-06-12', '09:08:59', 6, 1),
(979, 478, 4, '2017-06-12', '09:10:32', 1, 0),
(980, 478, 4, '2017-06-12', '09:12:38', 6, 1),
(981, 479, 4, '2017-06-12', '09:16:21', 1, 0),
(982, 480, 4, '2017-06-12', '09:16:21', 1, 0),
(983, 481, 4, '2017-06-12', '09:16:22', 1, 0),
(984, 482, 4, '2017-06-12', '09:17:31', 1, 0),
(985, 479, 4, '2017-06-12', '09:17:52', 6, 1),
(986, 481, 4, '2017-06-12', '09:23:21', 6, 1),
(987, 480, 4, '2017-06-12', '09:23:23', 6, 1),
(988, 482, 4, '2017-06-12', '09:24:46', 6, 1),
(989, 483, 4, '2017-06-12', '09:30:09', 1, 0),
(990, 484, 4, '2017-06-12', '09:30:09', 1, 0),
(991, 485, 4, '2017-06-12', '09:30:09', 1, 0),
(992, 486, 4, '2017-06-12', '09:30:09', 1, 0),
(993, 486, 4, '2017-06-12', '09:31:28', 6, 1),
(994, 485, 4, '2017-06-12', '09:34:38', 6, 1),
(995, 484, 4, '2017-06-12', '09:34:42', 6, 0),
(996, 483, 4, '2017-06-12', '09:34:44', 6, 1),
(997, 487, 4, '2017-06-12', '09:39:52', 1, 0),
(998, 487, 4, '2017-06-12', '09:40:16', 6, 1),
(999, 488, 4, '2017-06-12', '09:42:22', 1, 0),
(1000, 489, 4, '2017-06-12', '09:42:23', 1, 0),
(1001, 490, 4, '2017-06-12', '09:43:13', 1, 0),
(1002, 488, 4, '2017-06-12', '09:45:51', 6, 1),
(1003, 489, 4, '2017-06-12', '09:46:54', 6, 1),
(1004, 490, 4, '2017-06-12', '09:48:40', 6, 1),
(1005, 491, 4, '2017-06-12', '09:50:38', 1, 0),
(1006, 492, 4, '2017-06-12', '09:50:39', 1, 0),
(1007, 492, 4, '2017-06-12', '09:53:10', 6, 1),
(1008, 491, 4, '2017-06-12', '09:53:12', 6, 1),
(1009, 493, 4, '2017-06-12', '09:55:05', 1, 0),
(1010, 494, 4, '2017-06-12', '09:55:05', 1, 0),
(1011, 494, 4, '2017-06-12', '09:56:15', 6, 1),
(1012, 493, 4, '2017-06-12', '09:57:53', 6, 1),
(1013, 455, 4, '2017-06-12', '10:00:23', 2, 0),
(1014, 495, 4, '2017-06-12', '10:07:14', 1, 0),
(1015, 495, 4, '2017-06-12', '10:13:29', 6, 1),
(1016, 496, 4, '2017-06-12', '10:15:04', 1, 0),
(1017, 497, 4, '2017-06-12', '10:15:05', 1, 0),
(1018, 498, 4, '2017-06-12', '10:15:07', 1, 0),
(1019, 499, 4, '2017-06-12', '10:15:07', 1, 0),
(1020, 499, 4, '2017-06-12', '10:18:40', 5, 0),
(1021, 499, 4, '2017-06-12', '10:18:41', 6, 1),
(1022, 498, 4, '2017-06-12', '10:18:42', 5, 0),
(1023, 498, 4, '2017-06-12', '10:18:44', 6, 1),
(1024, 497, 4, '2017-06-12', '10:18:47', 6, 1),
(1025, 496, 4, '2017-06-12', '10:19:29', 6, 1),
(1026, 500, 4, '2017-06-12', '10:26:38', 1, 0),
(1027, 500, 4, '2017-06-12', '10:26:43', 6, 1),
(1028, 501, 4, '2017-06-12', '10:29:19', 1, 0),
(1029, 502, 4, '2017-06-12', '10:29:19', 1, 0),
(1030, 502, 4, '2017-06-12', '10:30:28', 6, 1),
(1031, 501, 4, '2017-06-12', '10:30:30', 6, 0),
(1032, 503, 4, '2017-06-12', '10:33:15', 1, 0),
(1033, 504, 4, '2017-06-12', '10:33:15', 1, 0),
(1034, 504, 4, '2017-06-12', '10:50:57', 6, 1),
(1035, 503, 4, '2017-06-12', '10:50:59', 6, 1),
(1036, 454, 4, '2017-06-13', '07:19:17', 3, 0),
(1037, 455, 4, '2017-06-13', '07:46:59', 3, 0),
(1038, 452, 4, '2017-06-13', '07:48:55', 3, 0),
(1039, 453, 4, '2017-06-13', '07:49:10', 4, 1),
(1040, 454, 4, '2017-06-13', '07:49:12', 4, 1),
(1041, 455, 4, '2017-06-13', '07:49:15', 4, 1),
(1042, 452, 4, '2017-06-13', '07:49:18', 4, 1),
(1043, 451, 4, '2017-06-13', '07:52:04', 4, 1),
(1044, 450, 4, '2017-06-13', '07:53:37', 4, 1),
(1045, 449, 4, '2017-06-13', '07:55:32', 4, 1),
(1046, 385, 4, '2017-06-13', '07:58:18', 4, 1),
(1047, 383, 4, '2017-06-13', '08:03:58', 4, 1),
(1048, 382, 4, '2017-06-13', '08:04:01', 4, 1),
(1049, 384, 4, '2017-06-13', '08:04:25', 4, 1),
(1050, 448, 4, '2017-06-13', '08:07:54', 4, 1),
(1051, 505, 4, '2017-06-13', '08:10:42', 1, 0),
(1052, 505, 4, '2017-06-13', '08:12:39', 4, 1),
(1053, 506, 4, '2017-06-13', '08:19:20', 1, 0),
(1054, 506, 4, '2017-06-13', '08:19:25', 4, 1),
(1055, 507, 4, '2017-06-13', '08:24:20', 1, 0),
(1056, 507, 4, '2017-06-13', '08:24:32', 4, 1),
(1057, 508, 4, '2017-06-13', '08:27:25', 1, 0),
(1058, 509, 4, '2017-06-13', '08:27:25', 1, 0),
(1059, 508, 4, '2017-06-13', '08:27:33', 4, 1),
(1060, 509, 4, '2017-06-13', '08:30:21', 4, 1),
(1061, 510, 4, '2017-06-13', '08:32:49', 1, 0),
(1062, 511, 4, '2017-06-13', '08:32:49', 1, 0),
(1063, 511, 4, '2017-06-13', '08:34:20', 6, 0),
(1064, 510, 4, '2017-06-13', '08:34:21', 4, 1),
(1065, 511, 4, '2017-06-13', '08:34:23', 4, 1),
(1066, 512, 4, '2017-06-15', '10:28:25', 1, 0),
(1067, 513, 4, '2017-06-15', '10:28:25', 1, 0),
(1068, 514, 4, '2017-06-15', '10:28:25', 1, 0),
(1069, 515, 4, '2017-06-15', '10:31:57', 1, 0),
(1070, 516, 4, '2017-06-15', '10:31:57', 1, 0),
(1071, 517, 4, '2017-06-15', '10:33:45', 1, 0),
(1072, 518, 4, '2017-06-15', '10:33:45', 1, 0),
(1073, 519, 4, '2017-06-15', '10:36:55', 1, 0),
(1074, 520, 4, '2017-06-15', '10:36:55', 1, 0),
(1075, 521, 4, '2017-06-15', '10:36:55', 1, 0),
(1076, 466, 4, '2017-06-19', '11:09:42', 3, 1),
(1077, 465, 4, '2017-06-19', '11:11:18', 3, 1),
(1078, 464, 4, '2017-06-19', '11:15:33', 3, 1),
(1079, 463, 4, '2017-06-19', '11:17:25', 5, 0),
(1080, 463, 4, '2017-06-19', '11:17:33', 3, 1),
(1081, 462, 4, '2017-06-19', '11:19:17', 3, 1),
(1082, 461, 4, '2017-06-19', '11:20:43', 3, 1),
(1083, 460, 4, '2017-06-19', '11:22:22', 3, 1),
(1084, 459, 4, '2017-06-19', '11:25:05', 3, 1),
(1085, 458, 4, '2017-06-19', '11:26:47', 3, 1),
(1086, 457, 4, '2017-06-19', '11:28:31', 3, 1),
(1087, 456, 4, '2017-06-19', '11:29:55', 3, 1),
(1088, 522, 4, '2017-06-19', '11:32:44', 1, 0),
(1089, 523, 4, '2017-06-19', '11:32:44', 1, 0),
(1090, 522, 4, '2017-06-19', '11:33:42', 3, 1),
(1091, 523, 4, '2017-06-19', '11:33:44', 3, 1),
(1092, 524, 4, '2017-06-19', '11:35:56', 1, 0),
(1093, 525, 4, '2017-06-19', '11:35:57', 1, 0),
(1094, 525, 4, '2017-06-19', '11:37:22', 3, 1),
(1095, 524, 4, '2017-06-19', '11:37:25', 3, 1),
(1096, 526, 4, '2017-06-19', '11:39:47', 1, 0),
(1097, 526, 4, '2017-06-19', '11:39:51', 3, 1),
(1098, 527, 4, '2017-06-19', '11:41:29', 1, 0),
(1099, 527, 4, '2017-06-19', '11:41:33', 3, 1),
(1100, 528, 4, '2017-06-19', '11:44:01', 1, 0),
(1101, 529, 4, '2017-06-19', '11:44:02', 1, 0),
(1102, 528, 4, '2017-06-19', '11:44:53', 3, 1),
(1103, 529, 4, '2017-06-19', '11:44:58', 5, 0),
(1104, 529, 4, '2017-06-19', '11:45:10', 4, 0),
(1105, 529, 4, '2017-06-19', '11:45:25', 3, 1),
(1106, 106, 4, '2017-06-19', '12:03:07', 6, 1),
(1107, 105, 4, '2017-06-19', '12:03:08', 6, 1),
(1108, 102, 4, '2017-06-19', '12:03:12', 6, 1),
(1109, 104, 4, '2017-06-19', '12:03:16', 6, 1),
(1110, 101, 4, '2017-06-19', '12:03:19', 6, 1),
(1111, 100, 4, '2017-06-19', '12:03:26', 6, 1),
(1112, 99, 4, '2017-06-19', '12:03:27', 6, 1),
(1113, 97, 4, '2017-06-19', '12:03:31', 6, 1),
(1114, 98, 4, '2017-06-19', '12:03:33', 6, 1),
(1115, 96, 4, '2017-06-19', '12:03:40', 6, 1),
(1116, 95, 4, '2017-06-19', '12:03:42', 6, 1),
(1117, 94, 4, '2017-06-19', '12:03:44', 6, 1),
(1118, 93, 4, '2017-06-19', '12:03:45', 6, 1),
(1119, 92, 4, '2017-06-19', '12:03:48', 6, 1),
(1120, 89, 4, '2017-06-19', '12:03:55', 6, 1),
(1121, 88, 4, '2017-06-19', '12:03:57', 6, 1),
(1122, 87, 4, '2017-06-19', '12:03:59', 6, 1),
(1123, 86, 4, '2017-06-19', '12:04:00', 6, 1),
(1124, 90, 4, '2017-06-19', '12:04:10', 6, 1),
(1125, 126, 4, '2017-06-19', '12:05:34', 6, 1),
(1126, 85, 4, '2017-06-19', '12:05:51', 6, 1),
(1127, 84, 4, '2017-06-19', '12:05:52', 6, 1),
(1128, 82, 4, '2017-06-19', '12:05:57', 6, 1),
(1129, 80, 4, '2017-06-19', '12:06:01', 6, 1),
(1130, 79, 4, '2017-06-19', '12:06:03', 6, 1),
(1131, 78, 4, '2017-06-19', '12:06:03', 6, 1),
(1132, 75, 4, '2017-06-19', '12:06:07', 6, 1),
(1133, 77, 4, '2017-06-19', '12:06:16', 6, 1),
(1134, 76, 4, '2017-06-19', '12:06:17', 6, 1),
(1135, 74, 4, '2017-06-19', '12:06:27', 6, 1),
(1136, 73, 4, '2017-06-19', '12:06:27', 6, 1),
(1137, 72, 4, '2017-06-19', '12:06:48', 6, 1),
(1138, 70, 4, '2017-06-19', '12:06:51', 6, 1),
(1139, 69, 4, '2017-06-19', '12:06:58', 6, 1),
(1140, 68, 4, '2017-06-19', '12:07:01', 6, 1),
(1141, 66, 4, '2017-06-19', '12:07:04', 6, 1),
(1142, 67, 4, '2017-06-19', '12:07:08', 6, 1),
(1143, 48, 4, '2017-06-19', '12:07:45', 6, 1),
(1144, 46, 4, '2017-06-19', '12:07:48', 6, 1),
(1145, 47, 4, '2017-06-19', '12:07:51', 6, 1),
(1146, 44, 4, '2017-06-19', '12:07:53', 6, 1),
(1147, 45, 4, '2017-06-19', '12:07:56', 6, 1),
(1148, 43, 4, '2017-06-19', '12:08:00', 6, 1),
(1149, 42, 4, '2017-06-19', '12:08:03', 6, 1),
(1150, 39, 4, '2017-06-19', '12:08:06', 6, 1),
(1151, 38, 4, '2017-06-19', '12:08:09', 6, 1),
(1152, 41, 4, '2017-06-19', '12:08:12', 6, 1),
(1153, 37, 4, '2017-06-19', '12:08:15', 6, 1),
(1154, 36, 4, '2017-06-19', '12:08:19', 6, 1),
(1155, 35, 4, '2017-06-19', '12:08:30', 6, 1),
(1156, 33, 4, '2017-06-19', '12:08:33', 6, 1),
(1157, 32, 4, '2017-06-19', '12:08:41', 6, 1),
(1158, 29, 4, '2017-06-19', '12:08:47', 6, 1),
(1159, 28, 4, '2017-06-19', '12:08:48', 6, 1),
(1160, 30, 4, '2017-06-19', '12:08:50', 6, 1),
(1161, 31, 4, '2017-06-19', '12:08:52', 6, 1),
(1162, 530, 4, '2017-06-26', '07:54:46', 1, 0),
(1163, 531, 4, '2017-06-26', '07:54:46', 1, 0),
(1164, 532, 4, '2017-06-26', '07:54:46', 1, 0),
(1165, 533, 4, '2017-06-26', '08:01:44', 1, 0),
(1166, 534, 4, '2017-06-26', '08:01:45', 1, 0),
(1167, 535, 4, '2017-06-26', '08:01:45', 1, 0),
(1168, 536, 4, '2017-06-26', '08:01:47', 1, 0),
(1169, 537, 4, '2017-06-26', '08:01:47', 1, 0),
(1170, 538, 4, '2017-06-26', '08:11:39', 1, 0),
(1171, 539, 4, '2017-06-26', '08:11:48', 1, 0),
(1172, 540, 4, '2017-06-26', '08:11:49', 1, 0),
(1173, 520, 4, '2017-06-26', '10:10:11', 3, 1),
(1174, 521, 4, '2017-06-26', '10:12:14', 3, 1),
(1175, 519, 4, '2017-06-26', '10:13:32', 3, 1),
(1176, 518, 4, '2017-06-26', '10:18:11', 3, 1),
(1177, 517, 4, '2017-06-26', '10:18:14', 3, 1),
(1178, 516, 4, '2017-06-26', '10:23:31', 3, 1),
(1179, 515, 4, '2017-06-26', '10:25:22', 3, 1),
(1180, 514, 4, '2017-06-26', '10:30:08', 3, 1),
(1181, 513, 4, '2017-06-26', '10:30:11', 3, 1);
INSERT INTO `movimientos` (`id`, `id_contenedor`, `id_usuario`, `fecha`, `hora`, `id_estado`, `activo`) VALUES
(1182, 512, 4, '2017-06-26', '10:31:19', 3, 1),
(1183, 541, 4, '2017-06-26', '10:33:31', 1, 0),
(1184, 542, 4, '2017-06-26', '10:33:32', 1, 0),
(1185, 542, 4, '2017-06-26', '10:35:36', 5, 1),
(1186, 542, 4, '2017-06-26', '10:35:36', 3, 1),
(1187, 541, 4, '2017-06-26', '10:36:46', 3, 1),
(1188, 543, 4, '2017-06-26', '10:38:43', 1, 0),
(1189, 543, 4, '2017-06-26', '10:38:47', 3, 1),
(1190, 544, 4, '2017-06-26', '11:02:02', 1, 0),
(1191, 545, 4, '2017-06-26', '11:02:02', 1, 0),
(1192, 545, 4, '2017-06-26', '11:03:10', 3, 1),
(1193, 544, 4, '2017-06-26', '11:03:13', 3, 1),
(1194, 546, 4, '2017-06-26', '11:05:59', 1, 0),
(1195, 547, 4, '2017-06-26', '11:05:59', 1, 0),
(1196, 546, 4, '2017-06-26', '11:08:42', 3, 1),
(1197, 547, 4, '2017-06-26', '11:08:44', 3, 1),
(1198, 548, 4, '2017-06-26', '11:10:53', 1, 0),
(1199, 549, 4, '2017-06-26', '11:10:53', 1, 0),
(1200, 549, 4, '2017-06-26', '11:12:08', 3, 1),
(1201, 548, 4, '2017-06-26', '11:12:11', 3, 1),
(1202, 550, 4, '2017-06-30', '09:10:44', 1, 1),
(1203, 551, 4, '2017-06-30', '09:10:45', 1, 0),
(1204, 552, 4, '2017-06-30', '09:13:44', 1, 0),
(1205, 553, 4, '2017-06-30', '09:16:13', 1, 0),
(1206, 554, 4, '2017-06-30', '09:18:20', 1, 0),
(1207, 555, 4, '2017-06-30', '09:18:21', 1, 0),
(1208, 556, 4, '2017-06-30', '09:18:21', 1, 0),
(1209, 557, 4, '2017-06-30', '09:24:20', 1, 0),
(1210, 558, 4, '2017-06-30', '09:24:20', 1, 0),
(1211, 559, 4, '2017-06-30', '09:27:19', 1, 0),
(1212, 560, 4, '2017-06-30', '09:27:19', 1, 0),
(1213, 530, 4, '2017-07-03', '10:35:08', 3, 1),
(1214, 539, 4, '2017-07-03', '10:36:50', 3, 1),
(1215, 538, 4, '2017-07-03', '10:38:23', 3, 1),
(1216, 536, 4, '2017-07-03', '10:42:12', 3, 1),
(1217, 535, 4, '2017-07-03', '10:47:58', 3, 1),
(1218, 533, 4, '2017-07-03', '11:05:10', 3, 1),
(1219, 532, 4, '2017-07-03', '11:08:23', 3, 1),
(1220, 534, 4, '2017-07-03', '11:10:36', 3, 1),
(1221, 537, 4, '2017-07-03', '11:10:39', 3, 1),
(1222, 531, 4, '2017-07-03', '11:10:42', 3, 1),
(1223, 540, 4, '2017-07-03', '11:13:15', 3, 1),
(1224, 561, 4, '2017-07-03', '11:18:06', 1, 0),
(1225, 562, 4, '2017-07-03', '11:22:43', 1, 0),
(1226, 561, 4, '2017-07-03', '11:24:09', 3, 1),
(1227, 562, 4, '2017-07-03', '11:24:11', 3, 1),
(1228, 563, 4, '2017-07-03', '11:27:00', 1, 0),
(1229, 564, 4, '2017-07-03', '11:27:01', 1, 0),
(1230, 564, 4, '2017-07-03', '11:28:30', 3, 1),
(1231, 563, 4, '2017-07-03', '11:28:33', 3, 1),
(1232, 565, 4, '2017-07-03', '11:31:51', 1, 0),
(1233, 566, 4, '2017-07-03', '11:31:51', 1, 0),
(1234, 565, 4, '2017-07-03', '11:33:47', 3, 1),
(1235, 566, 4, '2017-07-03', '11:33:50', 3, 1),
(1236, 567, 4, '2017-07-03', '11:36:01', 1, 0),
(1237, 568, 4, '2017-07-03', '11:36:10', 1, 1),
(1238, 567, 4, '2017-07-03', '11:36:27', 3, 1),
(1239, 569, 4, '2017-07-10', '07:27:52', 1, 0),
(1240, 570, 4, '2017-07-10', '07:29:01', 1, 0),
(1241, 571, 4, '2017-07-10', '07:34:37', 1, 0),
(1242, 572, 4, '2017-07-10', '07:34:37', 1, 0),
(1243, 573, 4, '2017-07-10', '07:37:15', 1, 0),
(1244, 574, 4, '2017-07-10', '07:37:15', 1, 0),
(1245, 575, 4, '2017-07-10', '07:42:38', 1, 0),
(1246, 576, 4, '2017-07-10', '07:44:13', 1, 0),
(1247, 577, 4, '2017-07-10', '07:44:13', 1, 0),
(1248, 578, 4, '2017-07-10', '07:47:38', 1, 0),
(1249, 577, 4, '2017-07-10', '07:56:26', 2, 0),
(1250, 576, 4, '2017-07-10', '07:56:29', 2, 0),
(1251, 575, 4, '2017-07-10', '07:56:31', 2, 0),
(1252, 578, 4, '2017-07-10', '07:56:34', 2, 0),
(1253, 569, 4, '2017-07-10', '07:56:37', 2, 0),
(1254, 570, 4, '2017-07-10', '07:56:41', 2, 0),
(1255, 573, 4, '2017-07-10', '07:56:43', 2, 0),
(1256, 574, 4, '2017-07-10', '07:56:47', 2, 0),
(1257, 572, 4, '2017-07-10', '07:56:50', 2, 0),
(1258, 571, 4, '2017-07-10', '07:56:53', 2, 0),
(1259, 560, 4, '2017-07-10', '08:29:25', 3, 1),
(1260, 558, 4, '2017-07-10', '09:03:46', 3, 1),
(1261, 557, 4, '2017-07-10', '09:05:07', 4, 0),
(1262, 557, 4, '2017-07-10', '09:05:09', 3, 1),
(1263, 552, 4, '2017-07-10', '09:07:47', 3, 1),
(1264, 551, 4, '2017-07-10', '09:13:41', 3, 1),
(1265, 559, 4, '2017-07-10', '09:19:12', 3, 1),
(1266, 556, 4, '2017-07-10', '09:19:47', 3, 1),
(1267, 555, 4, '2017-07-10', '09:25:41', 3, 1),
(1268, 554, 4, '2017-07-10', '09:28:51', 3, 1),
(1269, 553, 4, '2017-07-10', '09:31:49', 3, 1),
(1270, 579, 4, '2017-07-10', '09:44:27', 1, 0),
(1271, 580, 4, '2017-07-10', '09:44:27', 1, 0),
(1272, 581, 4, '2017-07-10', '09:44:27', 1, 0),
(1273, 580, 4, '2017-07-10', '09:47:04', 3, 1),
(1274, 579, 4, '2017-07-10', '09:47:08', 3, 1),
(1275, 581, 4, '2017-07-10', '09:47:56', 3, 1),
(1276, 582, 4, '2017-07-10', '09:53:21', 1, 0),
(1277, 583, 4, '2017-07-10', '09:53:22', 1, 0),
(1278, 584, 4, '2017-07-10', '09:53:22', 1, 0),
(1279, 583, 4, '2017-07-10', '09:55:49', 3, 1),
(1280, 582, 4, '2017-07-10', '09:55:53', 3, 1),
(1281, 584, 4, '2017-07-10', '09:56:26', 3, 1),
(1282, 585, 4, '2017-07-10', '09:59:05', 1, 0),
(1283, 586, 4, '2017-07-10', '09:59:05', 1, 0),
(1284, 585, 4, '2017-07-10', '10:00:34', 3, 1),
(1285, 586, 4, '2017-07-10', '10:00:38', 3, 1),
(1286, 587, 4, '2017-07-10', '10:02:49', 1, 0),
(1287, 587, 4, '2017-07-10', '10:02:52', 3, 1),
(1288, 588, 4, '2017-07-10', '10:05:40', 1, 0),
(1289, 589, 4, '2017-07-10', '10:05:40', 1, 0),
(1290, 589, 4, '2017-07-10', '10:08:54', 3, 1),
(1291, 588, 4, '2017-07-10', '10:09:13', 3, 1),
(1292, 590, 4, '2017-07-14', '07:21:16', 1, 0),
(1293, 591, 4, '2017-07-14', '07:21:17', 1, 0),
(1294, 590, 4, '2017-07-14', '07:21:44', 2, 0),
(1295, 591, 4, '2017-07-14', '07:21:48', 2, 0),
(1296, 592, 4, '2017-07-14', '07:30:08', 1, 0),
(1297, 593, 4, '2017-07-14', '07:30:08', 1, 0),
(1298, 594, 4, '2017-07-14', '07:32:30', 1, 0),
(1299, 595, 4, '2017-07-14', '07:32:30', 1, 0),
(1300, 596, 4, '2017-07-14', '07:32:30', 1, 0),
(1301, 594, 4, '2017-07-14', '07:33:09', 2, 0),
(1302, 592, 4, '2017-07-14', '07:33:12', 2, 0),
(1303, 593, 4, '2017-07-14', '07:33:14', 2, 0),
(1304, 595, 4, '2017-07-14', '07:35:53', 2, 0),
(1305, 596, 4, '2017-07-14', '07:35:55', 2, 0),
(1306, 597, 4, '2017-07-14', '07:39:10', 1, 0),
(1307, 598, 4, '2017-07-14', '07:39:10', 1, 0),
(1308, 599, 4, '2017-07-14', '07:39:10', 1, 0),
(1309, 599, 4, '2017-07-14', '07:40:45', 2, 0),
(1310, 598, 4, '2017-07-14', '07:40:49', 2, 0),
(1311, 597, 4, '2017-07-14', '07:40:51', 2, 0),
(1312, 600, 4, '2017-07-14', '07:42:39', 1, 0),
(1313, 600, 4, '2017-07-14', '07:42:54', 2, 0),
(1314, 571, 4, '2017-07-17', '12:11:51', 3, 1),
(1315, 570, 4, '2017-07-17', '12:13:58', 3, 1),
(1316, 573, 4, '2017-07-17', '12:15:56', 3, 1),
(1317, 574, 4, '2017-07-17', '12:17:47', 3, 1),
(1318, 572, 4, '2017-07-17', '12:18:31', 3, 1),
(1319, 577, 4, '2017-07-17', '12:29:29', 3, 1),
(1320, 569, 4, '2017-07-17', '12:30:51', 3, 1),
(1321, 576, 4, '2017-07-17', '12:31:56', 3, 1),
(1322, 578, 4, '2017-07-17', '12:31:58', 3, 1),
(1323, 575, 4, '2017-07-17', '12:33:20', 3, 1),
(1324, 601, 4, '2017-07-17', '12:39:59', 1, 0),
(1325, 601, 4, '2017-07-17', '12:40:12', 3, 1),
(1326, 602, 4, '2017-07-17', '12:48:29', 1, 0),
(1327, 603, 4, '2017-07-17', '12:48:29', 1, 0),
(1328, 603, 4, '2017-07-17', '12:49:55', 3, 1),
(1329, 604, 4, '2017-07-17', '12:52:06', 1, 0),
(1330, 605, 4, '2017-07-17', '12:52:06', 1, 0),
(1331, 602, 4, '2017-07-17', '12:52:14', 3, 1),
(1332, 605, 4, '2017-07-17', '12:53:25', 3, 1),
(1333, 604, 4, '2017-07-17', '12:53:28', 3, 1),
(1334, 606, 4, '2017-07-17', '12:55:26', 1, 0),
(1335, 607, 4, '2017-07-17', '12:57:30', 1, 0),
(1336, 608, 4, '2017-07-17', '12:57:30', 1, 0),
(1337, 606, 4, '2017-07-17', '12:59:26', 3, 1),
(1338, 608, 4, '2017-07-17', '12:59:28', 3, 1),
(1339, 607, 4, '2017-07-17', '12:59:56', 3, 1),
(1340, 609, 4, '2017-07-21', '08:50:12', 1, 0),
(1341, 610, 4, '2017-07-21', '08:52:03', 1, 0),
(1342, 611, 4, '2017-07-21', '08:57:29', 1, 0),
(1343, 612, 4, '2017-07-21', '08:57:30', 1, 0),
(1344, 613, 4, '2017-07-21', '08:57:30', 1, 0),
(1345, 614, 4, '2017-07-21', '09:00:51', 1, 0),
(1346, 615, 4, '2017-07-21', '09:03:36', 1, 1),
(1347, 616, 4, '2017-07-21', '09:03:36', 1, 1),
(1348, 617, 4, '2017-07-21', '09:03:36', 1, 0),
(1349, 618, 4, '2017-07-21', '09:03:38', 1, 0),
(1350, 619, 4, '2017-07-21', '09:03:38', 1, 0),
(1351, 620, 4, '2017-07-21', '09:03:39', 1, 0),
(1352, 621, 4, '2017-07-21', '09:03:39', 1, 0),
(1353, 599, 4, '2017-07-24', '07:55:39', 3, 0),
(1354, 598, 4, '2017-07-24', '07:56:45', 3, 1),
(1355, 597, 4, '2017-07-24', '08:02:11', 3, 1),
(1356, 590, 4, '2017-07-24', '08:03:52', 3, 1),
(1357, 596, 4, '2017-07-24', '08:07:39', 3, 1),
(1358, 595, 4, '2017-07-24', '08:08:43', 3, 1),
(1359, 591, 4, '2017-07-24', '08:09:48', 3, 1),
(1360, 592, 4, '2017-07-24', '08:10:46', 3, 1),
(1361, 593, 4, '2017-07-24', '08:11:43', 3, 1),
(1362, 594, 4, '2017-07-24', '08:12:28', 3, 1),
(1363, 622, 4, '2017-07-24', '08:17:20', 1, 0),
(1364, 623, 4, '2017-07-24', '08:17:20', 1, 0),
(1365, 624, 4, '2017-07-24', '08:17:21', 1, 0),
(1366, 622, 4, '2017-07-24', '08:17:28', 3, 1),
(1367, 623, 4, '2017-07-24', '08:18:15', 3, 1),
(1368, 624, 4, '2017-07-24', '08:18:51', 3, 1),
(1369, 625, 4, '2017-07-24', '08:21:26', 1, 0),
(1370, 626, 4, '2017-07-24', '08:21:27', 1, 0),
(1371, 626, 4, '2017-07-24', '08:22:12', 3, 1),
(1372, 625, 4, '2017-07-24', '08:22:16', 3, 1),
(1373, 627, 4, '2017-07-24', '08:24:39', 1, 0),
(1374, 627, 4, '2017-07-24', '08:24:44', 3, 1),
(1375, 628, 4, '2017-07-24', '08:26:49', 1, 0),
(1376, 629, 4, '2017-07-24', '08:26:49', 1, 0),
(1377, 628, 4, '2017-07-24', '08:26:54', 3, 1),
(1378, 629, 4, '2017-07-24', '08:28:32', 3, 1),
(1379, 600, 4, '2017-07-24', '08:28:37', 3, 1),
(1380, 630, 4, '2017-07-26', '08:34:23', 1, 0),
(1381, 631, 4, '2017-07-26', '08:34:23', 1, 0),
(1382, 632, 4, '2017-07-26', '08:37:02', 1, 0),
(1383, 633, 4, '2017-07-26', '08:38:32', 1, 0),
(1384, 634, 4, '2017-07-26', '08:38:32', 1, 0),
(1385, 635, 4, '2017-07-26', '08:41:44', 1, 0),
(1386, 636, 4, '2017-07-26', '08:41:44', 1, 1),
(1387, 599, 2, '2017-07-27', '08:36:10', 7, 0),
(1388, 599, 2, '2017-07-27', '08:36:46', 5, 1),
(1389, 637, 4, '2017-07-27', '12:23:52', 1, 0),
(1390, 638, 4, '2017-07-27', '12:23:52', 1, 0),
(1391, 639, 4, '2017-07-27', '12:23:53', 1, 0),
(1392, 640, 4, '2017-07-27', '12:23:53', 1, 0),
(1393, 641, 4, '2017-07-27', '12:41:57', 1, 0),
(1394, 642, 4, '2017-07-27', '12:41:58', 1, 0),
(1395, 643, 4, '2017-07-27', '12:41:58', 1, 0),
(1396, 645, 4, '2017-07-31', '07:42:08', 1, 0),
(1397, 644, 4, '2017-07-31', '07:42:08', 1, 0),
(1398, 646, 4, '2017-07-31', '08:02:09', 1, 0),
(1399, 647, 4, '2017-07-31', '08:02:09', 1, 0),
(1400, 648, 4, '2017-08-01', '10:55:43', 1, 0),
(1401, 649, 4, '2017-08-01', '10:56:47', 1, 0),
(1402, 650, 4, '2017-08-01', '10:57:37', 1, 0),
(1403, 651, 4, '2017-08-01', '10:58:59', 1, 0),
(1404, 652, 4, '2017-08-01', '10:59:36', 1, 0),
(1405, 653, 4, '2017-08-01', '11:00:48', 1, 0),
(1406, 654, 4, '2017-08-07', '09:08:10', 1, 0),
(1407, 655, 4, '2017-08-07', '09:10:59', 1, 0),
(1408, 656, 4, '2017-08-10', '08:27:18', 1, 1),
(1409, 657, 4, '2017-08-10', '08:28:46', 1, 1),
(1410, 658, 4, '2017-08-10', '08:44:35', 1, 1),
(1411, 659, 4, '2017-08-10', '09:33:58', 1, 1),
(1412, 660, 4, '2017-08-10', '09:35:18', 1, 1),
(1413, 661, 4, '2017-08-10', '09:37:17', 1, 1),
(1414, 662, 4, '2017-08-10', '09:38:28', 1, 1),
(1415, 663, 4, '2017-08-10', '09:39:37', 1, 1),
(1416, 664, 4, '2017-08-10', '09:42:35', 1, 1),
(1417, 665, 4, '2017-08-10', '09:43:37', 1, 1),
(1418, 666, 4, '2017-08-10', '09:44:43', 1, 1),
(1419, 667, 4, '2017-08-10', '09:46:52', 1, 1),
(1420, 668, 4, '2017-08-10', '10:03:16', 1, 1),
(1421, 669, 4, '2017-08-10', '10:04:25', 1, 1),
(1422, 670, 4, '2017-08-10', '10:06:01', 1, 1),
(1423, 671, 4, '2017-08-10', '10:07:14', 1, 1),
(1424, 672, 4, '2017-08-10', '10:08:38', 1, 1),
(1425, 673, 4, '2017-08-10', '10:10:31', 1, 1),
(1426, 674, 4, '2017-08-10', '10:11:22', 1, 1),
(1427, 644, 2, '2017-08-18', '16:57:46', 2, 0),
(1428, 619, 2, '2017-08-21', '09:01:57', 3, 1),
(1429, 620, 2, '2017-08-21', '09:02:25', 3, 1),
(1430, 618, 2, '2017-08-21', '09:02:49', 3, 1),
(1431, 610, 2, '2017-08-21', '09:03:32', 3, 1),
(1432, 621, 2, '2017-08-21', '09:04:02', 3, 1),
(1433, 617, 2, '2017-08-21', '09:04:44', 3, 1),
(1434, 609, 2, '2017-08-21', '09:05:26', 3, 1),
(1435, 614, 2, '2017-08-21', '09:06:15', 3, 1),
(1436, 613, 2, '2017-08-21', '09:07:10', 3, 1),
(1437, 612, 2, '2017-08-21', '09:07:43', 3, 1),
(1438, 611, 2, '2017-08-21', '09:08:33', 3, 1),
(1439, 633, 2, '2017-08-21', '09:10:36', 3, 1),
(1440, 631, 2, '2017-08-21', '09:14:07', 3, 1),
(1441, 630, 2, '2017-08-21', '09:14:47', 3, 1),
(1442, 632, 2, '2017-08-21', '09:15:28', 3, 1),
(1443, 675, 4, '2017-08-21', '09:17:45', 1, 0),
(1444, 676, 4, '2017-08-21', '09:19:55', 1, 0),
(1445, 634, 2, '2017-08-21', '09:21:48', 3, 1),
(1446, 635, 2, '2017-08-21', '09:22:30', 3, 1),
(1447, 677, 4, '2017-08-21', '09:23:07', 1, 0),
(1448, 678, 4, '2017-08-21', '09:25:03', 1, 0),
(1449, 679, 4, '2017-08-21', '09:28:45', 1, 0),
(1450, 680, 4, '2017-08-21', '09:31:40', 1, 0),
(1451, 681, 4, '2017-08-21', '09:34:54', 1, 0),
(1452, 682, 4, '2017-08-21', '09:37:21', 1, 0),
(1453, 683, 4, '2017-08-21', '09:39:30', 1, 0),
(1454, 684, 4, '2017-08-21', '10:10:04', 1, 0),
(1455, 685, 4, '2017-08-21', '10:23:02', 1, 0),
(1456, 686, 4, '2017-08-21', '10:26:03', 1, 0),
(1457, 687, 4, '2017-08-21', '10:30:30', 1, 0),
(1458, 688, 4, '2017-08-21', '10:32:41', 1, 0),
(1459, 689, 4, '2017-08-21', '10:35:23', 1, 0),
(1460, 690, 4, '2017-08-21', '10:46:51', 1, 0),
(1461, 691, 4, '2017-08-21', '10:49:10', 1, 0),
(1462, 692, 4, '2017-08-21', '10:53:43', 1, 0),
(1463, 693, 4, '2017-08-21', '10:56:37', 1, 0),
(1464, 694, 4, '2017-08-21', '10:58:11', 1, 1),
(1465, 675, 2, '2017-08-24', '11:54:35', 3, 1),
(1466, 676, 2, '2017-08-24', '11:55:08', 3, 1),
(1467, 677, 2, '2017-08-24', '11:57:16', 3, 1),
(1468, 678, 2, '2017-08-24', '11:58:05', 3, 1),
(1469, 679, 2, '2017-08-24', '11:58:49', 3, 1),
(1470, 680, 2, '2017-08-24', '11:59:10', 3, 1),
(1471, 684, 2, '2017-08-24', '12:15:51', 3, 1),
(1472, 683, 2, '2017-08-24', '12:16:25', 3, 1),
(1473, 682, 2, '2017-08-24', '12:16:55', 3, 1),
(1474, 681, 2, '2017-08-24', '12:17:48', 3, 1),
(1475, 688, 2, '2017-08-24', '12:18:37', 3, 1),
(1476, 687, 2, '2017-08-24', '12:19:11', 3, 1),
(1477, 686, 2, '2017-08-24', '12:19:53', 3, 1),
(1478, 685, 2, '2017-08-24', '12:22:19', 3, 1),
(1479, 689, 2, '2017-08-24', '12:22:45', 3, 1),
(1480, 690, 2, '2017-08-24', '12:23:15', 3, 1),
(1481, 691, 2, '2017-08-24', '12:24:53', 3, 1),
(1482, 692, 2, '2017-08-24', '12:25:17', 3, 1),
(1483, 693, 2, '2017-08-24', '12:26:00', 3, 1),
(1484, 695, 4, '2017-08-26', '09:26:15', 1, 0),
(1485, 696, 4, '2017-08-26', '09:29:30', 1, 0),
(1486, 697, 4, '2017-08-26', '09:30:48', 1, 0),
(1487, 698, 4, '2017-08-26', '09:32:07', 1, 0),
(1488, 699, 4, '2017-08-26', '09:35:32', 1, 0),
(1489, 700, 4, '2017-08-26', '09:39:26', 1, 0),
(1490, 701, 4, '2017-08-26', '10:00:19', 1, 0),
(1491, 702, 4, '2017-08-26', '10:01:32', 1, 0),
(1492, 703, 4, '2017-08-26', '10:03:47', 1, 0),
(1493, 704, 4, '2017-08-26', '10:05:49', 1, 0),
(1494, 705, 4, '2017-08-26', '10:09:50', 1, 0),
(1495, 706, 4, '2017-08-26', '10:11:27', 1, 0),
(1496, 707, 4, '2017-08-26', '10:12:25', 1, 0),
(1497, 708, 4, '2017-08-26', '10:13:46', 1, 0),
(1498, 642, 2, '2017-08-26', '10:48:28', 3, 1),
(1499, 643, 2, '2017-08-26', '10:48:55', 3, 1),
(1500, 645, 2, '2017-08-26', '10:58:58', 3, 1),
(1501, 647, 2, '2017-08-26', '11:01:05', 3, 1),
(1502, 641, 2, '2017-08-26', '11:01:30', 3, 1),
(1503, 639, 2, '2017-08-26', '11:03:30', 3, 1),
(1504, 644, 2, '2017-08-26', '11:04:41', 3, 1),
(1505, 638, 2, '2017-08-26', '11:05:40', 3, 1),
(1506, 646, 2, '2017-08-26', '11:06:13', 3, 1),
(1507, 652, 2, '2017-08-26', '11:06:59', 3, 1),
(1508, 651, 2, '2017-08-26', '11:07:49', 3, 1),
(1509, 637, 2, '2017-08-26', '11:08:19', 3, 1),
(1510, 640, 2, '2017-08-26', '11:08:46', 3, 1),
(1511, 650, 2, '2017-08-26', '11:09:24', 3, 1),
(1512, 654, 2, '2017-08-26', '11:13:25', 3, 1),
(1513, 655, 2, '2017-08-26', '11:47:28', 3, 1),
(1514, 648, 2, '2017-08-26', '11:48:09', 3, 1),
(1515, 649, 2, '2017-08-26', '11:48:47', 3, 1),
(1516, 653, 2, '2017-08-26', '12:16:21', 3, 1),
(1517, 709, 4, '2017-08-28', '09:50:18', 1, 0),
(1518, 710, 4, '2017-08-28', '10:55:38', 1, 0),
(1519, 711, 4, '2017-08-28', '11:18:32', 1, 0),
(1520, 712, 4, '2017-08-28', '11:43:20', 1, 0),
(1521, 713, 4, '2017-08-28', '11:50:33', 1, 0),
(1522, 714, 4, '2017-08-28', '11:53:36', 1, 0),
(1523, 715, 4, '2017-08-28', '11:57:21', 1, 0),
(1524, 716, 4, '2017-08-28', '12:00:27', 1, 0),
(1525, 717, 4, '2017-08-28', '12:03:40', 1, 0),
(1526, 718, 4, '2017-08-28', '12:06:48', 1, 0),
(1527, 719, 4, '2017-08-28', '12:08:51', 1, 0),
(1528, 720, 4, '2017-08-28', '12:20:08', 1, 0),
(1529, 721, 4, '2017-08-28', '12:22:59', 1, 0),
(1530, 722, 4, '2017-08-28', '12:26:48', 1, 0),
(1531, 723, 4, '2017-08-28', '12:28:24', 1, 0),
(1532, 724, 4, '2017-08-28', '12:40:43', 1, 0),
(1533, 725, 4, '2017-08-28', '12:43:22', 1, 0),
(1534, 726, 4, '2017-08-28', '12:45:59', 1, 0),
(1535, 727, 4, '2017-08-28', '12:47:03', 1, 0),
(1536, 709, 2, '2017-08-29', '14:47:23', 3, 1),
(1537, 710, 2, '2017-08-29', '14:47:58', 3, 1),
(1538, 711, 2, '2017-08-29', '14:48:30', 3, 1),
(1539, 695, 2, '2017-08-29', '14:48:59', 3, 1),
(1540, 712, 2, '2017-08-29', '14:49:21', 3, 1),
(1541, 713, 2, '2017-08-29', '14:49:40', 3, 1),
(1542, 717, 2, '2017-08-29', '14:50:05', 3, 1),
(1543, 716, 2, '2017-08-29', '14:50:29', 3, 1),
(1544, 715, 2, '2017-08-29', '14:50:48', 3, 1),
(1545, 714, 2, '2017-08-29', '14:51:09', 3, 1),
(1546, 721, 2, '2017-08-29', '14:51:37', 3, 1),
(1547, 720, 2, '2017-08-29', '14:52:00', 3, 1),
(1548, 719, 2, '2017-08-29', '14:52:29', 3, 1),
(1549, 718, 2, '2017-08-29', '14:53:00', 3, 1),
(1550, 722, 2, '2017-08-29', '14:53:37', 3, 1),
(1551, 723, 2, '2017-08-29', '14:53:55', 3, 1),
(1552, 724, 2, '2017-08-29', '14:54:38', 3, 1),
(1553, 725, 2, '2017-08-29', '14:54:57', 3, 1),
(1554, 726, 2, '2017-08-29', '14:55:19', 3, 1),
(1555, 727, 2, '2017-08-29', '14:55:59', 3, 1),
(1556, 728, 4, '2017-09-01', '12:44:33', 1, 0),
(1557, 729, 4, '2017-09-01', '12:46:12', 1, 0),
(1558, 730, 4, '2017-09-01', '12:47:08', 1, 0),
(1559, 731, 4, '2017-09-01', '12:48:12', 1, 0),
(1560, 732, 4, '2017-09-01', '12:50:22', 1, 0),
(1561, 733, 4, '2017-09-01', '12:52:27', 1, 0),
(1562, 734, 4, '2017-09-01', '12:55:04', 1, 0),
(1563, 735, 4, '2017-09-01', '12:57:09', 1, 0),
(1564, 736, 4, '2017-09-01', '13:02:48', 1, 0),
(1565, 737, 4, '2017-09-01', '13:04:10', 1, 0),
(1566, 738, 4, '2017-09-01', '13:05:25', 1, 0),
(1567, 739, 4, '2017-09-01', '13:06:13', 1, 0),
(1568, 740, 4, '2017-09-01', '13:07:27', 1, 0),
(1569, 741, 4, '2017-09-01', '14:46:17', 1, 0),
(1570, 742, 4, '2017-09-01', '14:47:26', 1, 0),
(1571, 743, 4, '2017-09-01', '14:49:21', 1, 0),
(1572, 744, 4, '2017-09-01', '14:50:12', 1, 0),
(1573, 745, 4, '2017-09-01', '14:51:20', 1, 0),
(1574, 746, 4, '2017-09-01', '14:52:01', 1, 0),
(1575, 747, 4, '2017-09-01', '14:53:16', 1, 0),
(1576, 748, 4, '2017-09-01', '14:54:04', 1, 0),
(1577, 484, 2, '2017-09-02', '10:35:20', 7, 1),
(1578, 501, 2, '2017-09-02', '10:35:48', 7, 1),
(1579, 749, 2, '2017-09-02', '11:54:33', 1, 1),
(1580, 750, 2, '2017-09-02', '11:57:33', 1, 1),
(1581, 751, 2, '2017-09-03', '08:35:12', 1, 1),
(1582, 696, 4, '2017-09-04', '09:02:10', 4, 1),
(1583, 697, 4, '2017-09-04', '09:04:19', 4, 1),
(1584, 698, 4, '2017-09-04', '09:06:20', 4, 1),
(1585, 699, 4, '2017-09-04', '09:12:08', 4, 1),
(1586, 700, 4, '2017-09-04', '09:14:03', 4, 1),
(1587, 708, 4, '2017-09-04', '09:16:38', 4, 1),
(1588, 707, 4, '2017-09-04', '09:27:31', 4, 1),
(1589, 706, 4, '2017-09-04', '09:35:05', 4, 1),
(1590, 705, 4, '2017-09-04', '09:36:40', 4, 1),
(1591, 704, 4, '2017-09-04', '09:39:04', 4, 1),
(1592, 703, 4, '2017-09-04', '09:40:56', 4, 1),
(1593, 702, 4, '2017-09-04', '09:42:23', 4, 1),
(1594, 701, 4, '2017-09-04', '09:44:19', 4, 1),
(1595, 752, 4, '2017-09-04', '09:49:55', 1, 0),
(1596, 752, 4, '2017-09-04', '09:50:02', 4, 1),
(1597, 753, 4, '2017-09-04', '09:52:35', 1, 0),
(1598, 753, 4, '2017-09-04', '09:52:41', 4, 1),
(1599, 754, 4, '2017-09-04', '09:56:32', 1, 0),
(1600, 754, 4, '2017-09-04', '09:56:59', 4, 1),
(1601, 755, 4, '2017-09-04', '09:58:43', 1, 0),
(1602, 755, 4, '2017-09-04', '09:58:58', 4, 1),
(1603, 756, 4, '2017-09-04', '10:02:26', 1, 0),
(1604, 756, 4, '2017-09-04', '10:02:33', 4, 1),
(1605, 757, 4, '2017-09-04', '10:04:28', 1, 0),
(1606, 757, 4, '2017-09-04', '10:04:35', 4, 1),
(1607, 758, 4, '2017-09-04', '10:06:39', 1, 0),
(1608, 758, 4, '2017-09-04', '10:06:55', 4, 1),
(1609, 759, 4, '2017-09-04', '10:09:08', 1, 0),
(1610, 759, 4, '2017-09-04', '10:09:19', 4, 1),
(1611, 760, 2, '2017-09-05', '09:49:47', 1, 1),
(1612, 761, 2, '2017-09-07', '09:38:55', 1, 1),
(1613, 728, 4, '2017-09-12', '08:03:18', 4, 1),
(1614, 736, 4, '2017-09-12', '08:06:02', 4, 1),
(1615, 735, 4, '2017-09-12', '08:08:52', 4, 1),
(1616, 734, 4, '2017-09-12', '08:10:17', 4, 1),
(1617, 733, 4, '2017-09-12', '08:12:41', 4, 1),
(1618, 732, 4, '2017-09-12', '08:12:54', 4, 1),
(1619, 731, 4, '2017-09-12', '08:14:26', 4, 1),
(1620, 730, 4, '2017-09-12', '08:16:54', 4, 1),
(1621, 729, 4, '2017-09-12', '08:40:17', 4, 1),
(1622, 737, 4, '2017-09-12', '08:41:45', 4, 1),
(1623, 738, 4, '2017-09-12', '08:43:36', 4, 1),
(1624, 739, 4, '2017-09-12', '08:45:04', 4, 1),
(1625, 746, 4, '2017-09-12', '08:49:06', 4, 1),
(1626, 740, 4, '2017-09-12', '08:50:42', 4, 1),
(1627, 741, 4, '2017-09-12', '08:53:19', 4, 1),
(1628, 742, 4, '2017-09-12', '08:56:58', 4, 1),
(1629, 743, 4, '2017-09-12', '08:58:13', 4, 1),
(1630, 744, 4, '2017-09-12', '09:00:03', 4, 1),
(1631, 747, 4, '2017-09-12', '09:03:18', 4, 1),
(1632, 745, 4, '2017-09-12', '09:03:42', 4, 1),
(1633, 748, 4, '2017-09-12', '09:05:43', 4, 1),
(1634, 762, 4, '2017-09-12', '09:10:51', 1, 1),
(1635, 763, 4, '2017-09-12', '09:12:37', 1, 1),
(1636, 764, 4, '2017-09-16', '09:14:39', 1, 0),
(1637, 764, 4, '2017-09-16', '09:22:47', 4, 1),
(1638, 765, 4, '2017-09-16', '09:24:46', 1, 0),
(1639, 765, 4, '2017-09-16', '09:24:52', 4, 1),
(1640, 766, 4, '2017-09-16', '09:31:03', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nave`
--

CREATE TABLE `nave` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nave`
--

INSERT INTO `nave` (`id`, `nombre`, `activo`) VALUES
(1, 'MARINER', 1),
(3, 'LOMBOK STRAIT', 1),
(21, 'NORTHERN DEXTERITY', 1),
(22, 'HANSA MEERS BURG', 1),
(24, 'AEREO', 1),
(25, 'ALGOL', 1),
(26, 'ALIOTH', 1),
(27, 'ALM ZURICH', 1),
(28, 'AOTEA MAERSK', 1),
(29, 'ARKADIA', 1),
(30, 'AS FIORELLA', 1),
(31, 'ATLANTIC KLIPPER', 1),
(32, 'ATLANTIC REEFER', 1),
(33, 'AVELONA STAR', 1),
(34, 'AVILA STAR', 1),
(35, 'BALTHASAR SHULTE', 1),
(36, 'BALTIC KLIPPER', 1),
(37, 'BARBARA', 1),
(38, 'BELLA SHULTE', 1),
(39, 'BIRK', 1),
(40, 'BODO SHULTE', 1),
(41, 'BOMAR RESOLUTE', 1),
(42, 'CALLAO EXPRESS', 1),
(43, 'CAP DOUKATO', 1),
(44, 'CAP INES', 1),
(45, 'CAP ISABEL', 1),
(46, 'CAP PALLISER', 1),
(47, 'CAP PALMESTOR', 1),
(48, 'CAP PASLEY', 1),
(49, 'CAP PATTON', 1),
(50, 'CAP PORTLAND', 1),
(51, 'CAP SAN SOUNIO', 1),
(52, 'CAP SAN TAINARO', 1),
(53, 'CAP TALBOT', 1),
(54, 'CAPA', 1),
(55, 'CARDIFF TRADER', 1),
(56, 'CAROLINA STAR', 1),
(57, 'CARSTEN MAERSK', 1),
(58, 'CATHARINA SHULTER', 1),
(59, 'CERINTHUS', 1),
(60, 'CHOAPA TRADER', 1),
(61, 'CMA CGM AMERICA', 1),
(62, 'CMA CGM MISSISSIPPI', 1),
(63, 'CMA CGM SAMBHAR', 1),
(64, 'CNP ILO', 1),
(65, 'CNP PAITA', 1),
(66, 'CONTI ARABELLA', 1),
(67, 'CONTI SOLOME', 1),
(68, 'COSCO KOREA', 1),
(69, 'CREDO', 1),
(70, 'CSCL LONG BEACH', 1),
(71, 'DOLE CALIFORNIA', 1),
(72, 'DOLE COSTA RICA', 1),
(73, 'DOLE ECUADOR', 1),
(74, 'DOLE PACIFIC', 1),
(75, 'DOMINGO', 1),
(76, 'DUBLIAN EXPRESS', 1),
(77, 'DUNCAN ISLAND', 1),
(78, 'ELISABETH-S', 1),
(79, 'EM HYDRA', 1),
(80, 'EVER EXCEL', 1),
(81, 'EVER SAFETY', 1),
(82, 'EVER UBERTY', 1),
(83, 'EVER UNION', 1),
(84, 'EVER URANUS', 1),
(85, 'EVERGREEN LINE', 1),
(86, 'FIONA', 1),
(87, 'FRISIA ROTTERDAM', 1),
(88, 'GLASGLOW EXPRESS', 1),
(89, 'GLORIA', 1),
(90, 'HAMBURG SUDD', 1),
(91, 'HAMMONIA AMERICA', 1),
(92, 'HANNAH SHULTE', 1),
(93, 'HANSA ASIA', 1),
(94, 'HANSA AUSTRALIA', 1),
(95, 'HANSA BREMEN', 1),
(96, 'HANSA CLOPPENBURG', 1),
(97, 'HANSA EUROPE', 1),
(98, 'HANSA FLENSBURG', 1),
(99, 'HANSA FREYBURG', 1),
(100, 'HANSA MEERSBURG', 1),
(101, 'HANSA OLDERBUNG', 1),
(102, 'HELENE 5', 1),
(103, 'HELLAS REEFER', 1),
(104, 'HYUNDAI SPLENDOR', 1),
(105, 'ITAL LIRICA', 1),
(106, 'ITALIA REEFER', 1),
(107, 'JACK LONDON', 1),
(108, 'JAMILA', 1),
(109, 'JPO VELA', 1),
(110, 'JULES VERNE', 1),
(111, 'JULIANA', 1),
(112, 'KIEL TRADER', 1),
(113, 'KMARIN AQUA', 1),
(114, 'KMARIN AZUR', 1),
(115, 'KOTA LAHIR', 1),
(116, 'KOTA LATIF', 1),
(117, 'KOTA LAYANG', 1),
(118, 'KOTA LEGIT', 1),
(119, 'KOTA LUKIS', 1),
(120, 'LADY KORCULA', 1),
(121, 'LADY REEFER', 1),
(122, 'LIVERPOOL EXPRESS', 1),
(123, 'LUNA MAERSK', 1),
(124, 'LUZON STRAIT', 1),
(125, 'MAERSK ALFIRK', 1),
(126, 'MAERSK ANTARES', 1),
(127, 'MAERSK BINTAN', 1),
(128, 'MAERSK BOGOR', 1),
(129, 'MAERSK GAIRLOCH', 1),
(130, 'MAERSK GIRONDE', 1),
(131, 'MAERSK LA PAZ', 1),
(132, 'MAERSK LAUNCESTON', 1),
(133, 'MAERSK LOME', 1),
(134, 'MAERSK NEWHAVEN', 1),
(135, 'MAGARI', 1),
(136, 'MARIA - KATHARINE S', 1),
(137, 'MAX WONDER', 1),
(138, 'MEDOCEAN', 1),
(139, 'MERIDIAN', 1),
(140, 'MOL GRANDEUR', 1),
(141, 'MOL PARADISE', 1),
(142, 'MOL PRESTIGE', 1),
(143, 'MSC AGRIGENTO', 1),
(144, 'MSC ALGECIRAS', 1),
(145, 'MSC ANISHA R', 1),
(146, 'MSC ANZU', 1),
(147, 'MSC ARUSHI R', 1),
(148, 'MSC ATHENS', 1),
(149, 'MSC AZOV', 1),
(150, 'MSC BRANKA', 1),
(151, 'MSC BRUNELLA', 1),
(152, 'MSC CARMEN', 1),
(153, 'MSC CHLOE', 1),
(154, 'MSC DOMITELLE', 1),
(155, 'MSC DONATA', 1),
(156, 'MSC FLAVIA', 1),
(157, 'MSC JULIA R', 1),
(158, 'MSC KATRINA', 1),
(159, 'MSC KATYA R', 1),
(160, 'MSC KIM', 1),
(161, 'MSC LEANNE', 1),
(162, 'MSC LEIGHT', 1),
(163, 'MSC LORENA', 1),
(164, 'MSC LOS ANGELES', 1),
(165, 'MSC MANU', 1),
(166, 'MSC PALAK', 1),
(167, 'MSC ROSARIA', 1),
(168, 'MSC SARAH', 1),
(169, 'MSC SARISKA', 1),
(170, 'MSC SASHA', 1),
(171, 'MSC SILVA', 1),
(172, 'MSC SOFIA CELESTE', 1),
(173, 'MSC VAISHNAVI R', 1),
(174, 'MSC VIDISHA R.', 1),
(175, 'MSC ZLATA R', 1),
(176, 'NEDERLAND REFFER', 1),
(177, 'NORASIA ALYA', 1),
(178, 'NORDAMELIA', 1),
(179, 'NORDSERENA', 1),
(180, 'NYK LIBRA', 1),
(181, 'NYL LYRA', 1),
(182, 'PACIFIC REEFER', 1),
(183, 'PIL', 1),
(184, 'POLARLIGHT', 1),
(185, 'RT ODIN', 1),
(186, 'SAFMARINE BENGUELA', 1),
(187, 'SAFMARINE MULANJE', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opcion`
--

CREATE TABLE `opcion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT 'home.',
  `icono` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `opcion`
--

INSERT INTO `opcion` (`id`, `nombre`, `url`, `icono`, `activo`) VALUES
(7, 'Container', 'home.container', 'truck', 1),
(8, 'Semana', 'home.semana', 'calendar', 1),
(9, 'Mantenimientos', 'home.mantenimiento', 'cog', 1),
(10, 'Reportes', 'home.reporte', 'file-excel-o', 1),
(11, 'Datos Estadisticos', 'home.grafico', 'area-chart', 1),
(12, 'Gestión de Usuarios', 'home.usuario', 'users', 1),
(13, 'Cliente', 'home.container', 'users', 1),
(14, 'Operador', 'home.container', 'users', 1),
(15, 'Auditorías', 'home.auditoria', 'search', 1),
(16, 'Productores', 'home.productor', 'check', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opcion_subopcion`
--

CREATE TABLE `opcion_subopcion` (
  `id` int(11) NOT NULL,
  `opcion_id` int(11) NOT NULL,
  `subopcion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operador`
--

CREATE TABLE `operador` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `operador`
--

INSERT INTO `operador` (`id`, `nombre`, `activo`) VALUES
(1, 'SAN MIGUEL', 1),
(2, 'TRANSLOGISTCS SAC', 1),
(3, 'NEPTUNIA S.A.', 1),
(5, 'TPSAC', 0),
(6, 'AFE TRANSPORTATION S.A.C.', 1),
(7, 'GRUPO ADUAN', 1),
(8, 'YACZ CARGO', 1),
(9, 'TAN CARGO PERU SAC', 1),
(10, 'BLUE EXPRESS', 1),
(11, 'TRANSPORTES GIRASOLES', 1),
(12, 'RANSA', 1),
(13, 'SMP SERVICIOS POSTALES Y LOGÍSTICOS', 1),
(14, 'SEATRADE', 1),
(15, 'LA HANSEATICA', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packing`
--

CREATE TABLE `packing` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` text NOT NULL,
  `id_estado_archivo` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `packing`
--

INSERT INTO `packing` (`id`, `id_cliente`, `id_semana`, `nombre`, `fecha`, `hora`, `url`, `id_estado_archivo`) VALUES
(11, 1, 53, '1_30_2017_(2)agrofair 01.xlsx', '2017-08-01', '16:14:17', '1_30_2017_(2)agrofair 01.xlsx', 2),
(13, 5, 53, '5_30_2017_(1)equifruit    30.xlsx', '2017-08-02', '16:14:44', '5_30_2017_(1)equifruit    30.xlsx', 2),
(14, 3, 53, '3_30_2017_(1)dole    30.xlsx', '2017-08-02', '16:17:15', '3_30_2017_(1)dole    30.xlsx', 2),
(15, 2, 53, '2_30_2017_(1)biodynamiska   30.xlsx', '2017-08-02', '16:18:00', '2_30_2017_(1)biodynamiska   30.xlsx', 2),
(16, 4, 53, '4_30_2017_(1)chiquita  30.xlsx', '2017-08-02', '16:18:24', '4_30_2017_(1)chiquita  30.xlsx', 2),
(17, 1, 54, '1_31_2017_(2)agrofair  31.xlsx', '2017-08-09', '15:44:21', '1_31_2017_(2)agrofair  31.xlsx', 3),
(18, 2, 54, '2_31_2017_(2)biodynamiska   31.xlsx', '2017-08-09', '15:46:05', '2_31_2017_(2)biodynamiska   31.xlsx', 3),
(19, 4, 54, '4_31_2017_(2)chiquita  31.xlsx', '2017-08-09', '15:46:55', '4_31_2017_(2)chiquita  31.xlsx', 3),
(20, 3, 54, '3_31_2017_(2)dole   31.xlsx', '2017-08-09', '15:48:00', '3_31_2017_(2)dole   31.xlsx', 2),
(21, 5, 54, '5_31_2017_(2)equifruit    31.xlsx', '2017-08-09', '15:48:34', '5_31_2017_(2)equifruit    31.xlsx', 2),
(32, 1, 55, '1_32_2017_(3)Packing Agrofair Sem 32.xls', '2017-08-21', '08:10:07', '1_32_2017_(3)Packing Agrofair Sem 32.xls', 2),
(33, 2, 55, '2_32_2017_(3)bidynamiska   32.xlsx', '2017-08-21', '08:10:33', '2_32_2017_(3)bidynamiska   32.xlsx', 2),
(34, 4, 55, '4_32_2017_(3)chiquita  32.xlsx', '2017-08-21', '08:11:17', '4_32_2017_(3)chiquita  32.xlsx', 2),
(35, 3, 55, '3_32_2017_(3)dole   32.xlsx', '2017-08-21', '08:11:40', '3_32_2017_(3)dole   32.xlsx', 2),
(36, 5, 55, '5_32_2017_(3)equifruit   32.xlsx', '2017-08-21', '08:12:02', '5_32_2017_(3)equifruit   32.xlsx', 2),
(39, 1, 56, '1_33_2017_(4)Packing Agrofair Sem 33.xls', '2017-08-24', '10:58:53', '1_33_2017_(4)Packing Agrofair Sem 33.xls', 2),
(40, 1, 57, '1_34_2017_(5)Packing Agrofair Sem 34.xls', '2017-08-31', '12:21:12', '1_34_2017_(5)Packing Agrofair Sem 34.xls', 2),
(41, 2, 57, '2_34_2017_(4)biodynamiska   34.xlsx', '2017-08-31', '12:27:02', '2_34_2017_(4)biodynamiska   34.xlsx', 3),
(42, 4, 57, '4_34_2017_(4)chiquita    34.xlsx', '2017-08-31', '12:27:29', '4_34_2017_(4)chiquita    34.xlsx', 2),
(43, 3, 57, '3_34_2017_(4)dole  34.xlsx', '2017-08-31', '12:29:11', '3_34_2017_(4)dole  34.xlsx', 2),
(44, 5, 57, '5_34_2017_(4)equifruit  34.xlsx', '2017-08-31', '12:30:56', '5_34_2017_(4)equifruit  34.xlsx', 2),
(45, 1, 58, '1_35_2017_(6)Packing Agrofair Sem 35.xls', '2017-09-06', '16:48:41', '1_35_2017_(6)Packing Agrofair Sem 35.xls', 2),
(46, 2, 58, '2_35_2017_(5)biodynamiska  35.xlsx', '2017-09-06', '16:54:39', '2_35_2017_(5)biodynamiska  35.xlsx', 2),
(47, 4, 58, '4_35_2017_(5)chiquita  35.xlsx', '2017-09-06', '16:54:55', '4_35_2017_(5)chiquita  35.xlsx', 2),
(48, 3, 58, '3_35_2017_(5)dole  35.xlsx', '2017-09-06', '16:55:15', '3_35_2017_(5)dole  35.xlsx', 2),
(49, 5, 58, '5_35_2017_(5)equifruit   35.xlsx', '2017-09-06', '16:55:33', '5_35_2017_(5)equifruit   35.xlsx', 3),
(50, 2, 59, '2_36_2017_(6)biodynamiska 36 bn.xlsx', '2017-09-13', '12:46:27', '2_36_2017_(6)biodynamiska 36 bn.xlsx', 2),
(51, 1, 59, '1_36_2017_(7)Packing Agrofair Sem 36.xls', '2017-09-13', '12:47:29', '1_36_2017_(7)Packing Agrofair Sem 36.xls', 2),
(52, 4, 59, '4_36_2017_(6)chiquita   36.xlsx', '2017-09-13', '12:47:55', '4_36_2017_(6)chiquita   36.xlsx', 2),
(53, 3, 59, '3_36_2017_(6)dole    36.xlsx', '2017-09-13', '12:48:17', '3_36_2017_(6)dole    36.xlsx', 2),
(54, 5, 59, '5_36_2017_(6)equifruit     36.xlsx', '2017-09-13', '12:48:41', '5_36_2017_(6)equifruit     36.xlsx', 2),
(55, 2, 60, '2_37_2017_(7)diseño.xls', '2017-10-02', '11:22:33', '2_37_2017_(7)diseño.xls', 2),
(56, 3, 50, '3_27_2017_(7)Libro1.xlsx', '2017-10-02', '11:36:01', '3_27_2017_(7)Libro1.xlsx', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `nombre`, `activo`) VALUES
(1, 'HOLANDA', 1),
(2, 'ALEMANIA', 1),
(3, 'ESTADOS UNIDOS', 1),
(4, 'PANAMA', 1),
(5, 'BELGICA', 1),
(6, 'JAPON', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parcela`
--

CREATE TABLE `parcela` (
  `id` int(11) NOT NULL,
  `id_productor` int(11) NOT NULL,
  `id_empacadora` int(11) NOT NULL,
  `id_sector` int(11) NOT NULL,
  `ha_cultivada` double(10,2) DEFAULT NULL,
  `ha_sembrada` double(10,2) NOT NULL,
  `numero_parcela` int(11) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parcela`
--

INSERT INTO `parcela` (`id`, `id_productor`, `id_empacadora`, `id_sector`, `ha_cultivada`, `ha_sembrada`, `numero_parcela`, `activo`) VALUES
(1, 5747, 6, 1, 1.25, 1.25, 1, 1),
(2, 5748, 6, 1, 1.25, 1.25, 1, 1),
(3, 5749, 8, 1, 0.50, 0.50, 1, 1),
(4, 5750, 2, 1, 2.47, 1.25, 1, 1),
(5, 5751, 9, 1, 0.50, 0.50, 1, 1),
(6, 5751, 9, 1, 0.50, 0.50, 2, 1),
(7, 5751, 9, 1, 1.00, 1.00, 3, 1),
(8, 5752, 9, 1, 0.50, 0.50, 1, 1),
(9, 5752, 3, 1, 2.00, 2.00, 2, 1),
(10, 5753, 3, 1, 1.25, 1.25, 1, 1),
(11, 5754, 3, 1, 1.00, 1.00, 1, 1),
(12, 5755, 3, 1, 2.00, 2.00, 1, 1),
(13, 5755, 4, 1, 3.00, 3.00, 2, 1),
(14, 5756, 4, 1, 1.50, 1.50, 1, 1),
(15, 5757, 2, 1, 2.30, 2.30, 1, 1),
(16, 5758, 4, 1, 1.00, 1.00, 1, 1),
(17, 5759, 4, 1, 0.50, 0.50, 1, 1),
(18, 5759, 4, 1, 0.50, 0.50, 2, 1),
(19, 5760, 4, 1, 0.95, 0.95, 1, 1),
(20, 5761, 3, 1, 0.25, 0.25, 1, 1),
(21, 5761, 4, 1, 0.50, 0.50, 2, 1),
(22, 5762, 4, 1, 0.50, 0.00, 1, 1),
(23, 5763, 4, 1, 0.50, 0.50, 1, 1),
(24, 5764, 2, 1, 0.85, 0.85, 1, 1),
(25, 5765, 3, 1, 0.88, 0.88, 1, 1),
(26, 5765, 2, 1, 0.50, 0.50, 2, 1),
(27, 5766, 5, 1, 2.50, 1.50, 1, 1),
(28, 5767, 4, 1, 1.00, 1.00, 2, 1),
(29, 5768, 8, 1, 1.50, 1.50, 1, 1),
(30, 5769, 10, 1, 1.00, 1.00, 1, 1),
(31, 5770, 4, 1, 0.25, 0.25, 1, 1),
(32, 5770, 1, 1, 0.25, 0.25, 2, 1),
(33, 5770, 4, 1, 0.50, 0.50, 3, 1),
(34, 5771, 11, 1, 0.36, 0.36, 1, 1),
(35, 5772, 3, 1, 0.50, 0.50, 1, 1),
(36, 5772, 3, 1, 0.25, 0.25, 2, 1),
(37, 5772, 2, 1, 1.00, 1.00, 3, 1),
(38, 5773, 2, 1, 1.00, 1.00, 1, 1),
(39, 5774, 7, 1, 0.37, 0.37, 1, 1),
(40, 5775, 6, 1, 0.88, 0.88, 1, 1),
(41, 5776, 6, 1, 0.25, 0.25, 1, 1),
(42, 5777, 5, 1, 0.30, 0.30, 2, 1),
(43, 5778, 6, 1, 1.00, 1.00, 1, 1),
(44, 5779, 8, 1, 0.63, 0.63, 1, 1),
(45, 5780, 7, 1, 0.50, 0.50, 1, 1),
(46, 5780, 7, 1, 1.25, 1.25, 2, 1),
(47, 5781, 8, 1, 1.39, 1.39, 1, 1),
(48, 5782, 8, 1, 1.40, 1.00, 1, 1),
(49, 5783, 2, 1, 0.25, 0.25, 1, 1),
(50, 5784, 7, 1, 0.50, 0.50, 1, 1),
(51, 5785, 2, 1, 0.25, 0.25, 1, 1),
(52, 5786, 6, 1, 0.25, 0.25, 1, 1),
(53, 5787, 10, 1, 2.25, 2.25, 1, 1),
(54, 5788, 7, 1, 0.50, 0.50, 1, 1),
(55, 5789, 2, 1, 0.25, 0.25, 1, 1),
(56, 5789, 2, 1, 0.50, 0.50, 2, 1),
(57, 5789, 2, 1, 0.37, 0.37, 3, 1),
(58, 5789, 6, 1, 0.20, 0.20, 4, 1),
(59, 5789, 7, 1, 0.37, 0.37, 5, 1),
(60, 5790, 2, 1, 0.37, 0.37, 1, 1),
(61, 5790, 7, 1, 0.37, 0.37, 2, 1),
(62, 5791, 7, 1, 0.50, 0.50, 1, 1),
(63, 5792, 2, 1, 0.37, 0.37, 1, 1),
(64, 5792, 7, 1, 0.53, 0.53, 2, 1),
(65, 5793, 7, 1, 0.50, 0.50, 1, 1),
(66, 5794, 7, 1, 0.50, 0.50, 1, 1),
(67, 5795, 2, 1, 1.06, 1.06, 1, 1),
(68, 5796, 7, 1, 0.50, 0.50, 1, 1),
(69, 5797, 2, 1, 0.50, 0.50, 1, 1),
(70, 5797, 2, 1, 0.25, 0.25, 2, 1),
(71, 5798, 2, 1, 0.25, 0.25, 1, 1),
(72, 5799, 2, 1, 2.50, 2.50, 1, 1),
(73, 5800, 7, 1, 0.77, 0.77, 2, 1),
(74, 5800, 7, 1, 0.79, 0.79, 1, 1),
(75, 5801, 2, 1, 0.50, 0.50, 1, 1),
(76, 5802, 2, 1, 0.35, 0.35, 1, 1),
(77, 5803, 3, 1, 0.50, 0.50, 1, 1),
(78, 5803, 7, 1, 0.59, 0.59, 2, 1),
(79, 5804, 2, 1, 1.39, 1.39, 1, 1),
(80, 5804, 7, 1, 1.00, 1.00, 1, 1),
(81, 5805, 7, 1, 0.50, 0.50, 1, 1),
(82, 5806, 6, 1, 2.50, 2.50, 1, 1),
(83, 5807, 2, 1, 2.00, 2.00, 1, 1),
(84, 5807, 7, 1, 0.35, 0.35, 2, 1),
(85, 5808, 2, 1, 0.50, 0.50, 1, 1),
(86, 5808, 7, 1, 0.50, 0.50, 2, 1),
(87, 5809, 2, 1, 2.00, 2.00, 1, 1),
(88, 5810, 2, 1, 0.75, 0.75, 1, 1),
(89, 5811, 2, 1, 1.00, 1.00, 1, 1),
(90, 5811, 7, 1, 0.50, 0.50, 3, 1),
(91, 5812, 7, 1, 1.02, 1.02, 1, 1),
(92, 5813, 12, 1, 1.18, 1.18, 1, 1),
(93, 5814, 2, 1, 2.50, 2.50, 1, 1),
(94, 5815, 6, 1, 0.75, 0.75, 1, 1),
(95, 5815, 6, 1, 0.12, 0.12, 2, 1),
(96, 5816, 6, 1, 0.75, 0.75, 1, 1),
(97, 5816, 6, 1, 0.25, 0.25, 2, 1),
(98, 5817, 6, 1, 1.50, 1.50, 1, 1),
(99, 5818, 12, 1, 1.50, 1.50, 1, 1),
(100, 5819, 3, 1, 0.50, 0.50, 1, 1),
(101, 5819, 2, 1, 0.50, 0.15, 2, 1),
(102, 5819, 7, 1, 3.25, 0.50, 3, 1),
(103, 5820, 6, 1, 0.25, 0.25, 1, 1),
(104, 5821, 6, 1, 1.00, 1.00, 1, 1),
(105, 5822, 7, 1, 0.50, 0.50, 1, 1),
(106, 5823, 2, 1, 0.50, 0.50, 1, 1),
(107, 5823, 7, 1, 0.50, 0.50, 2, 1),
(108, 5824, 2, 1, 1.50, 1.50, 1, 1),
(109, 5825, 2, 1, 2.00, 2.00, 1, 1),
(110, 5826, 12, 1, 0.50, 0.50, 1, 1),
(111, 5827, 6, 1, 1.00, 1.00, 1, 1),
(112, 5827, 7, 1, 0.25, 0.25, 2, 1),
(113, 5828, 2, 1, 1.25, 1.25, 1, 1),
(114, 5829, 6, 1, 0.25, 0.25, 1, 1),
(115, 5829, 6, 1, 0.25, 0.25, 2, 1),
(116, 5829, 11, 1, 0.25, 0.25, 3, 1),
(117, 5830, 6, 1, 0.25, 0.25, 1, 1),
(118, 5831, 3, 1, 0.25, 0.25, 1, 1),
(119, 5831, 6, 1, 0.25, 0.25, 2, 1),
(120, 5832, 2, 1, 0.25, 0.25, 1, 1),
(121, 5832, 6, 1, 0.25, 0.25, 2, 1),
(122, 5833, 2, 1, 0.25, 0.25, 1, 1),
(123, 5833, 7, 1, 0.50, 0.50, 2, 1),
(124, 5834, 2, 1, 0.50, 0.50, 1, 1),
(125, 5834, 7, 1, 1.25, 1.25, 2, 1),
(126, 5834, 7, 1, 0.50, 0.50, 1, 1),
(127, 5835, 2, 1, 0.50, 0.50, 1, 1),
(128, 5835, 7, 1, 1.50, 1.50, 2, 1),
(129, 5836, 2, 1, 0.25, 0.25, 1, 1),
(130, 5836, 11, 1, 0.25, 0.25, 2, 1),
(131, 5836, 7, 1, 1.50, 1.50, 3, 1),
(132, 5837, 7, 1, 0.25, 0.25, 1, 1),
(133, 5838, 2, 1, 0.25, 0.25, 1, 1),
(134, 5839, 6, 1, 0.50, 0.50, 1, 1),
(135, 5840, 3, 1, 0.25, 0.25, 2, 1),
(136, 5840, 2, 1, 0.25, 0.25, 1, 1),
(137, 5841, 3, 1, 0.25, 0.25, 2, 1),
(138, 5841, 2, 1, 0.25, 0.25, 1, 1),
(139, 5842, 3, 1, 0.50, 0.50, 1, 1),
(140, 5842, 2, 1, 0.50, 0.50, 2, 1),
(141, 5842, 2, 1, 2.50, 2.50, 3, 1),
(142, 5843, 11, 1, 0.25, 0.25, 1, 1),
(143, 5844, 4, 1, 0.50, 0.50, 1, 1),
(144, 5844, 7, 1, 0.50, 0.50, 2, 1),
(145, 5845, 4, 1, 0.50, 0.50, 1, 1),
(146, 5846, 6, 1, 1.00, 1.00, 1, 1),
(147, 5846, 6, 1, 0.76, 0.76, 2, 1),
(148, 5847, 6, 1, 0.50, 0.50, 1, 1),
(149, 5848, 6, 1, 0.50, 0.50, 1, 1),
(150, 5849, 6, 1, 2.25, 2.25, 1, 1),
(151, 5850, 4, 1, 0.50, 0.50, 1, 1),
(152, 5851, 7, 1, 0.25, 0.25, 1, 1),
(153, 5851, 7, 1, 0.50, 0.50, 2, 1),
(154, 5852, 7, 1, 0.50, 0.50, 1, 1),
(155, 5853, 7, 1, 0.50, 0.50, 1, 1),
(156, 5854, 2, 1, 2.25, 2.25, 1, 1),
(157, 5855, 3, 1, 1.25, 1.25, 1, 1),
(158, 5855, 13, 1, 0.50, 0.50, 2, 1),
(159, 5856, 3, 1, 0.50, 0.50, 1, 1),
(160, 5856, 2, 1, 1.00, 1.00, 2, 1),
(161, 5856, 2, 1, 1.00, 0.50, 3, 1),
(162, 5857, 2, 1, 0.75, 0.72, 1, 1),
(163, 5858, 6, 1, 0.50, 0.50, 1, 1),
(164, 5858, 6, 1, 0.50, 0.50, 2, 1),
(165, 5859, 4, 1, 0.50, 0.50, 1, 1),
(166, 5860, 1, 1, 0.75, 0.75, 1, 1),
(167, 5861, 7, 1, 1.00, 1.00, 1, 1),
(168, 5862, 7, 1, 1.21, 1.21, 1, 1),
(169, 5863, 6, 1, 1.13, 1.13, 1, 1),
(170, 5864, 6, 1, 2.00, 2.00, 1, 1),
(171, 5865, 5, 1, 2.19, 1.25, 1, 1),
(172, 5866, 11, 1, 0.25, 0.25, 1, 1),
(173, 5866, 11, 1, 1.93, 0.50, 2, 1),
(174, 5867, 13, 1, 0.50, 0.50, 1, 1),
(175, 5868, 13, 1, 0.50, 0.50, 1, 1),
(176, 5869, 13, 1, 0.50, 0.50, 1, 1),
(177, 5870, 6, 1, 0.50, 0.50, 1, 1),
(178, 5871, 6, 1, 0.88, 0.88, 1, 1),
(179, 5872, 6, 1, 0.50, 0.50, 1, 1),
(180, 5873, 6, 1, 1.00, 1.00, 1, 1),
(181, 5873, 10, 1, 0.50, 0.50, 2, 1),
(182, 5874, 10, 1, 0.50, 0.50, 1, 1),
(183, 5874, 10, 1, 2.26, 2.26, 2, 1),
(184, 5875, 2, 1, 1.00, 1.00, 1, 1),
(185, 5875, 7, 1, 1.25, 1.25, 2, 1),
(186, 5876, 2, 1, 0.50, 0.50, 1, 1),
(187, 5877, 6, 1, 1.00, 1.00, 1, 1),
(188, 5878, 6, 1, 1.50, 1.50, 1, 1),
(189, 5879, 9, 1, 0.50, 0.50, 1, 1),
(190, 5879, 9, 1, 1.00, 1.00, 2, 1),
(191, 5879, 7, 1, 0.50, 0.50, 3, 1),
(192, 5879, 7, 1, 0.50, 0.50, 4, 1),
(193, 5880, 13, 1, 0.75, 0.75, 1, 1),
(194, 5881, 13, 1, 0.50, 0.50, 1, 1),
(195, 5882, 13, 1, 0.50, 0.50, 1, 1),
(196, 5883, 13, 1, 0.50, 0.50, 1, 1),
(197, 5884, 6, 1, 0.50, 0.50, 1, 1),
(198, 5884, 6, 1, 0.50, 0.50, 2, 1),
(199, 5884, 6, 1, 0.50, 0.50, 3, 1),
(200, 5885, 6, 1, 2.40, 2.40, 1, 1),
(201, 5886, 6, 1, 0.75, 0.75, 1, 1),
(202, 5886, 6, 1, 0.25, 0.25, 2, 1),
(203, 5886, 7, 1, 3.00, 3.00, 3, 1),
(204, 5887, 6, 1, 0.50, 0.50, 1, 1),
(205, 5888, 7, 1, 1.25, 1.25, 1, 1),
(206, 5889, 2, 1, 1.00, 1.00, 1, 1),
(207, 5890, 2, 1, 2.50, 2.50, 1, 1),
(208, 5890, 7, 1, 1.25, 0.50, 2, 1),
(209, 5891, 6, 1, 0.50, 0.50, 1, 1),
(210, 5892, 3, 1, 2.20, 1.50, 1, 1),
(211, 5893, 6, 1, 0.80, 0.80, 1, 1),
(212, 5894, 6, 1, 0.50, 0.50, 1, 1),
(213, 5894, 7, 1, 0.50, 0.50, 2, 1),
(214, 5895, 6, 1, 0.50, 0.50, 1, 1),
(215, 5896, 6, 1, 1.60, 1.60, 1, 1),
(216, 5897, 2, 1, 0.50, 0.50, 1, 1),
(217, 5898, 6, 1, 0.50, 0.50, 1, 1),
(218, 5899, 7, 1, 1.14, 1.14, 1, 1),
(219, 5900, 6, 1, 0.25, 0.25, 1, 1),
(220, 5900, 7, 1, 0.25, 0.25, 2, 1),
(221, 5901, 6, 1, 1.50, 1.50, 1, 1),
(222, 5902, 2, 1, 0.75, 0.75, 1, 1),
(223, 5903, 7, 1, 1.25, 1.25, 1, 1),
(224, 5904, 6, 1, 0.50, 0.50, 1, 1),
(225, 5905, 2, 1, 0.30, 0.30, 1, 1),
(226, 5905, 7, 1, 0.76, 0.76, 2, 1),
(227, 5906, 2, 1, 0.25, 0.25, 1, 1),
(228, 5906, 7, 1, 0.50, 0.50, 2, 1),
(229, 5907, 6, 1, 0.07, 0.07, 1, 1),
(230, 5907, 11, 1, 0.25, 0.25, 2, 1),
(231, 5907, 7, 1, 1.93, 1.93, 3, 1),
(232, 5908, 2, 1, 2.00, 2.00, 2, 1),
(233, 5908, 2, 1, 0.40, 0.40, 1, 1),
(234, 5909, 5, 1, 0.47, 0.47, 1, 1),
(235, 5910, 6, 1, 0.50, 0.50, 1, 1),
(236, 5911, 9, 1, 0.25, 0.25, 1, 1),
(237, 5911, 4, 1, 0.19, 0.19, 2, 1),
(238, 5911, 5, 1, 1.25, 0.75, 3, 1),
(239, 5912, 2, 1, 2.50, 2.50, 1, 1),
(240, 5912, 6, 1, 0.25, 0.25, 2, 1),
(241, 5913, 6, 1, 0.30, 0.30, 1, 1),
(242, 5914, 7, 1, 1.65, 1.65, 1, 1),
(243, 5915, 7, 1, 2.59, 2.59, 1, 1),
(244, 5916, 7, 1, 1.50, 1.50, 1, 1),
(245, 5917, 7, 1, 1.50, 1.50, 1, 1),
(246, 5918, 7, 1, 1.00, 1.00, 1, 1),
(247, 5919, 2, 1, 0.75, 0.75, 1, 1),
(248, 5920, 2, 1, 1.50, 1.50, 1, 1),
(249, 5921, 2, 1, 0.25, 0.25, 1, 1),
(250, 5921, 2, 1, 0.25, 0.25, 2, 1),
(251, 5922, 2, 1, 0.50, 0.50, 1, 1),
(252, 5923, 3, 1, 1.25, 1.25, 1, 1),
(253, 5924, 2, 1, 0.25, 0.25, 1, 1),
(254, 5925, 2, 1, 0.25, 0.25, 3, 1),
(255, 5925, 2, 1, 0.50, 0.50, 2, 1),
(256, 5925, 2, 1, 0.75, 0.75, 1, 1),
(257, 5926, 2, 1, 0.25, 0.25, 1, 1),
(258, 5926, 2, 1, 0.25, 0.25, 3, 1),
(259, 5926, 2, 1, 0.82, 0.82, 2, 1),
(260, 5926, 11, 1, 0.25, 0.25, 4, 1),
(261, 5927, 2, 1, 0.75, 0.75, 1, 1),
(262, 5928, 2, 1, 0.25, 0.25, 1, 1),
(263, 5929, 3, 1, 0.25, 0.25, 2, 1),
(264, 5930, 2, 1, 1.25, 1.25, 1, 1),
(265, 5931, 2, 1, 2.25, 2.25, 1, 1),
(266, 5931, 2, 1, 1.00, 1.00, 2, 1),
(267, 5932, 3, 1, 0.40, 0.40, 1, 1),
(268, 5933, 3, 1, 1.00, 1.00, 1, 1),
(269, 5934, 3, 1, 1.00, 1.00, 1, 1),
(270, 5935, 3, 1, 0.25, 0.25, 1, 1),
(271, 5935, 7, 1, 1.72, 1.72, 2, 1),
(272, 5936, 3, 1, 0.50, 0.50, 1, 1),
(273, 5936, 7, 1, 1.75, 1.75, 2, 1),
(274, 5937, 3, 1, 0.50, 0.50, 1, 1),
(275, 5938, 3, 1, 0.25, 0.25, 1, 1),
(276, 5939, 3, 1, 0.25, 0.25, 1, 1),
(277, 5940, 3, 1, 0.25, 0.25, 1, 1),
(278, 5941, 3, 1, 0.25, 0.25, 1, 1),
(279, 5942, 13, 1, 5.75, 5.75, 1, 1),
(280, 5943, 13, 1, 0.50, 0.50, 1, 1),
(281, 5944, 13, 1, 0.62, 0.62, 1, 1),
(282, 5945, 13, 1, 2.00, 2.00, 1, 1),
(283, 5946, 8, 1, 2.50, 2.50, 1, 1),
(284, 5947, 6, 1, 0.50, 0.50, 1, 1),
(285, 5948, 13, 1, 0.75, 0.75, 1, 1),
(286, 5949, 1, 1, 1.00, 1.00, 1, 1),
(287, 5949, 5, 1, 0.50, 0.50, 2, 1),
(288, 5949, 7, 1, 1.00, 1.00, 3, 1),
(289, 5949, 7, 1, 0.50, 0.50, 4, 1),
(290, 5950, 3, 1, 0.30, 0.30, 1, 1),
(291, 5951, 7, 1, 0.75, 0.75, 1, 1),
(292, 5952, 10, 1, 0.64, 0.64, 1, 1),
(293, 5953, 6, 1, 2.00, 2.00, 1, 1),
(294, 5954, 6, 1, 0.31, 0.31, 1, 1),
(295, 5955, 3, 1, 0.75, 0.75, 1, 1),
(296, 5956, 6, 1, 0.69, 0.69, 1, 1),
(297, 5957, 6, 1, 0.75, 0.75, 1, 1),
(298, 5958, 3, 1, 0.50, 0.50, 1, 1),
(299, 5959, 7, 1, 1.00, 1.00, 1, 1),
(300, 5960, 9, 1, 0.25, 0.25, 1, 1),
(301, 5960, 3, 1, 0.25, 0.25, 2, 1),
(302, 5961, 7, 1, 1.50, 1.50, 1, 1),
(303, 5962, 7, 1, 1.25, 1.25, 1, 1),
(304, 5963, 2, 1, 1.75, 1.75, 1, 1),
(305, 5964, 2, 1, 0.50, 0.50, 1, 1),
(306, 5964, 2, 1, 0.50, 0.50, 2, 1),
(307, 5964, 6, 1, 0.25, 0.25, 3, 1),
(308, 5965, 9, 1, 0.37, 0.37, 1, 1),
(309, 5965, 7, 1, 1.00, 1.00, 2, 1),
(310, 5965, 7, 1, 0.50, 0.50, 3, 1),
(311, 5966, 8, 1, 2.50, 2.50, 1, 1),
(312, 5967, 9, 1, 1.63, 1.63, 1, 1),
(313, 5967, 4, 1, 0.25, 0.25, 2, 1),
(314, 5967, 4, 1, 0.50, 0.50, 3, 1),
(315, 5968, 10, 1, 3.07, 0.70, 1, 1),
(316, 5969, 13, 1, 0.50, 0.50, 1, 1),
(317, 5970, 3, 1, 1.50, 1.50, 1, 1),
(318, 5970, 4, 1, 0.25, 0.25, 2, 1),
(319, 5971, 3, 1, 2.00, 2.00, 1, 1),
(320, 5972, 5, 1, 1.00, 1.00, 1, 1),
(321, 5973, 2, 1, 2.50, 2.50, 1, 1),
(322, 5974, 7, 1, 0.50, 0.50, 1, 1),
(323, 5975, 3, 1, 1.50, 1.50, 2, 1),
(324, 5975, 3, 1, 0.44, 0.44, 1, 1),
(325, 5975, 3, 1, 0.75, 0.75, 4, 1),
(326, 5975, 2, 1, 0.25, 0.25, 3, 1),
(327, 5976, 12, 1, 0.50, 0.50, 1, 1),
(328, 5977, 12, 1, 1.18, 1.18, 1, 1),
(329, 5977, 12, 1, 1.25, 1.25, 2, 1),
(330, 5978, 7, 1, 1.50, 1.50, 1, 1),
(331, 5979, 3, 1, 0.33, 0.33, 1, 1),
(332, 5980, 3, 1, 2.00, 2.00, 1, 1),
(333, 5981, 8, 1, 0.50, 0.50, 1, 1),
(334, 5982, 8, 1, 0.25, 0.25, 1, 1),
(335, 5982, 8, 1, 0.28, 0.28, 2, 1),
(336, 5983, 8, 1, 2.50, 2.50, 1, 1),
(337, 5984, 6, 1, 0.75, 0.75, 1, 1),
(338, 5985, 6, 1, 0.50, 0.50, 1, 1),
(339, 5986, 4, 1, 0.50, 0.50, 1, 1),
(340, 5987, 4, 1, 0.50, 0.50, 1, 1),
(341, 5987, 4, 1, 0.50, 0.00, 2, 1),
(342, 5988, 12, 1, 0.50, 0.50, 1, 1),
(343, 5988, 12, 1, 0.50, 0.50, 2, 1),
(344, 5989, 3, 1, 0.25, 0.25, 1, 1),
(345, 5989, 3, 1, 0.25, 0.25, 1, 1),
(346, 5990, 3, 1, 0.80, 0.80, 3, 1),
(347, 5990, 3, 1, 0.75, 0.75, 4, 1),
(348, 5990, 3, 1, 0.50, 0.50, 1, 1),
(349, 5990, 3, 1, 0.94, 0.94, 2, 1),
(350, 5991, 3, 1, 0.25, 0.25, 1, 1),
(351, 5991, 2, 1, 1.00, 1.00, 2, 1),
(352, 5991, 2, 1, 1.25, 1.25, 3, 1),
(353, 5992, 13, 1, 0.75, 0.75, 1, 1),
(354, 5993, 2, 1, 0.50, 0.50, 1, 1),
(355, 5993, 7, 1, 2.00, 1.00, 2, 1),
(356, 5994, 7, 1, 0.86, 0.86, 1, 1),
(357, 5995, 10, 1, 1.00, 1.00, 1, 1),
(358, 5996, 10, 1, 2.10, 2.10, 1, 1),
(359, 5997, 2, 1, 0.25, 0.25, 1, 1),
(360, 5997, 7, 1, 0.50, 0.50, 2, 1),
(361, 5998, 2, 1, 0.50, 0.50, 1, 1),
(362, 5999, 9, 1, 2.25, 2.25, 1, 1),
(363, 6000, 2, 1, 0.75, 0.75, 1, 1),
(364, 6001, 4, 1, 1.50, 1.50, 1, 1),
(365, 6001, 4, 1, 0.50, 0.50, 2, 1),
(366, 6002, 8, 1, 2.50, 2.50, 1, 1),
(367, 6003, 12, 1, 0.50, 0.50, 1, 1),
(368, 6004, 6, 1, 0.25, 0.25, 1, 1),
(369, 6004, 6, 1, 0.37, 0.37, 2, 1),
(370, 6005, 6, 1, 1.75, 1.75, 1, 1),
(371, 6005, 7, 1, 0.75, 0.75, 1, 1),
(372, 6006, 4, 1, 0.50, 0.50, 1, 1),
(373, 6007, 3, 1, 2.29, 2.29, 1, 1),
(374, 6007, 3, 1, 0.21, 0.21, 2, 1),
(375, 6008, 2, 1, 0.25, 0.25, 1, 1),
(376, 6008, 2, 1, 2.25, 2.25, 2, 1),
(377, 6009, 14, 1, 1.00, 0.50, 1, 1),
(378, 6010, 3, 1, 1.00, 1.00, 1, 1),
(379, 6010, 14, 1, 10.00, 4.00, 2, 1),
(380, 6011, 5, 1, 0.50, 0.50, 1, 1),
(381, 6011, 7, 1, 0.13, 0.13, 1, 1),
(382, 6012, 3, 1, 1.21, 1.21, 1, 1),
(383, 6012, 14, 1, 10.00, 8.00, 2, 1),
(384, 6013, 7, 1, 1.00, 0.75, 1, 1),
(385, 6014, 2, 1, 0.25, 0.25, 1, 1),
(386, 6014, 2, 1, 0.25, 0.25, 2, 1),
(387, 6014, 7, 1, 1.00, 0.75, 3, 1),
(388, 6015, 5, 1, 0.62, 0.62, 1, 1),
(389, 6015, 4, 1, 0.68, 0.23, 2, 1),
(390, 6015, 7, 1, 0.50, 0.50, 3, 1),
(391, 6016, 11, 1, 0.25, 0.25, 1, 1),
(392, 6016, 11, 1, 0.22, 0.22, 2, 1),
(393, 6017, 6, 1, 0.25, 0.25, 1, 1),
(394, 6017, 6, 1, 0.25, 0.25, 2, 1),
(395, 6017, 7, 1, 1.75, 1.75, 3, 1),
(396, 6018, 3, 1, 0.25, 0.25, 1, 1),
(397, 6018, 3, 1, 0.32, 0.32, 1, 1),
(398, 6019, 3, 1, 0.20, 0.00, 1, 1),
(399, 6019, 2, 1, 0.20, 0.00, 2, 1),
(400, 6019, 7, 1, 0.20, 0.20, 3, 1),
(401, 6019, 7, 1, 0.75, 0.75, 4, 1),
(402, 6019, 7, 1, 0.38, 0.38, 5, 1),
(403, 6020, 7, 1, 0.50, 0.50, 1, 1),
(404, 6021, 13, 1, 0.75, 0.75, 1, 1),
(405, 6022, 3, 1, 0.45, 0.45, 1, 1),
(406, 6022, 3, 1, 0.75, 0.75, 4, 1),
(407, 6022, 3, 1, 0.22, 0.22, 2, 1),
(408, 6022, 6, 1, 0.50, 0.50, 3, 1),
(409, 6023, 7, 1, 0.50, 0.50, 1, 1),
(410, 6024, 2, 1, 2.50, 2.50, 1, 1),
(411, 6025, 3, 1, 1.00, 1.00, 3, 1),
(412, 6025, 6, 1, 0.31, 0.31, 1, 1),
(413, 6025, 6, 1, 0.50, 0.50, 2, 1),
(414, 6026, 3, 1, 1.00, 1.00, 2, 1),
(415, 6026, 2, 1, 2.12, 2.12, 3, 1),
(416, 6026, 2, 1, 1.00, 1.00, 4, 1),
(417, 6026, 2, 1, 0.50, 0.50, 1, 1),
(418, 6026, 7, 1, 1.00, 1.00, 5, 1),
(419, 6027, 7, 1, 0.50, 0.50, 1, 1),
(420, 6028, 6, 1, 0.25, 0.25, 1, 1),
(421, 6028, 11, 1, 1.00, 1.00, 2, 1),
(422, 6028, 11, 1, 0.25, 0.25, 3, 1),
(423, 6028, 7, 1, 0.50, 0.50, 4, 1),
(424, 6029, 3, 1, 0.50, 0.50, 1, 1),
(425, 6030, 7, 1, 2.00, 2.00, 1, 1),
(426, 6031, 13, 1, 0.50, 0.50, 1, 1),
(427, 6032, 12, 1, 0.50, 0.50, 1, 1),
(428, 6033, 2, 1, 1.00, 1.00, 1, 1),
(429, 6033, 10, 1, 1.00, 1.00, 2, 1),
(430, 6033, 10, 1, 0.40, 0.40, 3, 1),
(431, 6034, 7, 1, 1.75, 1.75, 1, 1),
(432, 6035, 2, 1, 0.90, 0.90, 1, 1),
(433, 6036, 2, 1, 1.25, 1.25, 1, 1),
(434, 6037, 6, 1, 2.50, 2.50, 1, 1),
(435, 6037, 8, 1, 3.00, 3.00, 2, 1),
(436, 6037, 8, 1, 0.60, 0.60, 3, 1),
(437, 6038, 2, 1, 0.25, 0.25, 1, 1),
(438, 6038, 7, 1, 0.25, 0.25, 2, 1),
(439, 6038, 7, 1, 1.00, 1.00, 3, 1),
(440, 6039, 7, 1, 1.25, 1.25, 1, 1),
(441, 6040, 7, 1, 1.25, 1.25, 1, 1),
(442, 6041, 6, 1, 0.75, 0.75, 1, 1),
(443, 6042, 10, 1, 0.66, 0.66, 1, 1),
(444, 6043, 5, 1, 0.50, 0.50, 1, 1),
(445, 6044, 4, 1, 0.60, 0.60, 1, 1),
(446, 6044, 4, 1, 0.62, 0.62, 2, 1),
(447, 6045, 7, 1, 0.25, 0.25, 1, 1),
(448, 6045, 7, 1, 1.20, 1.20, 2, 1),
(449, 6046, 14, 1, 2.25, 1.25, 1, 1),
(450, 6047, 14, 1, 2.25, 1.25, 1, 1),
(451, 6048, 5, 1, 1.50, 1.50, 1, 1),
(452, 6048, 7, 1, 0.50, 0.50, 2, 1),
(453, 6049, 5, 1, 2.50, 2.00, 1, 1),
(454, 6050, 1, 1, 0.75, 0.75, 1, 1),
(455, 6051, 4, 1, 1.34, 1.34, 1, 1),
(456, 6051, 7, 1, 1.34, 1.34, 1, 1),
(457, 6052, 9, 1, 0.75, 0.75, 1, 1),
(458, 6052, 1, 1, 4.49, 1.50, 2, 1),
(459, 6053, 5, 1, 2.50, 2.00, 1, 1),
(460, 6053, 1, 1, 0.50, 0.50, 2, 1),
(461, 6054, 4, 1, 1.00, 1.00, 1, 1),
(462, 6055, 2, 1, 0.25, 0.25, 1, 1),
(463, 6055, 2, 1, 0.50, 0.50, 2, 1),
(464, 6055, 5, 1, 2.50, 2.00, 3, 1),
(465, 6056, 4, 1, 1.00, 1.00, 1, 1),
(466, 6056, 7, 1, 1.00, 1.00, 2, 1),
(467, 6057, 9, 1, 0.37, 0.37, 1, 1),
(468, 6057, 9, 1, 0.25, 0.25, 2, 1),
(469, 6057, 4, 1, 0.50, 0.50, 3, 1),
(470, 6057, 7, 1, 1.00, 1.00, 4, 1),
(471, 6057, 7, 1, 0.25, 0.25, 5, 1),
(472, 6058, 4, 1, 2.34, 1.00, 1, 1),
(473, 6059, 2, 1, 0.50, 0.50, 1, 1),
(474, 6059, 4, 1, 0.87, 0.87, 2, 1),
(475, 6060, 4, 1, 0.50, 0.50, 3, 1),
(476, 6061, 4, 1, 3.34, 3.34, 1, 1),
(477, 6062, 1, 1, 0.84, 0.84, 1, 1),
(478, 6063, 4, 1, 1.00, 1.00, 1, 1),
(479, 6064, 1, 1, 0.50, 0.50, 1, 1),
(480, 6065, 2, 1, 1.00, 1.00, 1, 1),
(481, 6065, 2, 1, 0.31, 0.31, 2, 1),
(482, 6065, 2, 1, 0.62, 0.62, 3, 1),
(483, 6066, 1, 1, 0.50, 0.50, 1, 1),
(484, 6066, 4, 1, 1.00, 1.00, 2, 1),
(485, 6067, 13, 1, 1.00, 1.00, 1, 1),
(486, 6068, 4, 1, 1.50, 1.50, 1, 1),
(487, 6068, 4, 1, 0.50, 0.50, 2, 1),
(488, 6069, 4, 1, 1.00, 1.00, 1, 1),
(489, 6070, 4, 1, 1.81, 1.81, 1, 1),
(490, 6070, 5, 1, 1.50, 1.50, 2, 1),
(491, 6071, 5, 1, 1.38, 1.38, 1, 1),
(492, 6072, 4, 1, 0.50, 0.50, 1, 1),
(493, 6072, 4, 1, 2.00, 2.00, 1, 1),
(494, 6073, 3, 1, 0.50, 0.50, 1, 1),
(495, 6074, 6, 1, 0.25, 0.25, 1, 1),
(496, 6074, 6, 1, 0.75, 0.75, 2, 1),
(497, 6074, 6, 1, 0.50, 0.50, 3, 1),
(498, 6075, 4, 1, 0.63, 0.63, 1, 1),
(499, 6075, 4, 1, 0.25, 0.25, 2, 1),
(500, 6076, 9, 1, 0.15, 0.15, 1, 1),
(501, 6076, 4, 1, 0.50, 0.50, 2, 1),
(502, 6076, 4, 1, 0.12, 0.12, 3, 1),
(503, 6077, 3, 1, 0.50, 0.50, 1, 1),
(504, 6078, 5, 1, 0.50, 0.50, 1, 1),
(505, 6078, 1, 1, 0.50, 0.50, 2, 1),
(506, 6079, 9, 1, 0.25, 0.25, 1, 1),
(507, 6079, 5, 1, 0.50, 0.50, 3, 1),
(508, 6080, 9, 1, 0.25, 0.25, 1, 1),
(509, 6080, 4, 1, 1.37, 1.37, 2, 1),
(510, 6080, 5, 1, 0.50, 0.50, 3, 1),
(511, 6080, 5, 1, 0.25, 0.25, 4, 1),
(512, 6081, 5, 1, 4.16, 4.16, 1, 1),
(513, 6082, 9, 1, 0.25, 0.25, 1, 1),
(514, 6082, 4, 1, 1.29, 1.29, 2, 1),
(515, 6083, 12, 1, 0.44, 0.44, 1, 1),
(516, 6084, 12, 1, 0.50, 0.50, 1, 1),
(517, 6085, 9, 1, 0.30, 0.30, 1, 1),
(518, 6085, 3, 1, 0.30, 0.30, 2, 1),
(519, 6085, 9, 1, 0.32, 0.32, 3, 1),
(520, 6085, 4, 1, 1.50, 1.50, 4, 1),
(521, 6086, 9, 1, 0.50, 0.50, 1, 1),
(522, 6086, 3, 1, 0.61, 0.61, 2, 1),
(523, 6087, 9, 1, 1.00, 1.00, 1, 1),
(524, 6088, 12, 1, 0.73, 0.73, 1, 1),
(525, 6089, 6, 1, 0.17, 0.17, 1, 1),
(526, 6090, 6, 1, 2.50, 2.50, 1, 1),
(527, 6091, 2, 1, 0.50, 0.50, 1, 1),
(528, 6091, 2, 1, 0.25, 0.25, 2, 1),
(529, 6092, 8, 1, 1.25, 1.25, 1, 1),
(530, 6093, 8, 1, 1.00, 1.00, 1, 1),
(531, 6094, 8, 1, 0.50, 0.50, 1, 1),
(532, 6095, 13, 1, 3.00, 3.00, 1, 1),
(533, 6095, 13, 1, 1.03, 0.50, 2, 1),
(534, 6096, 5, 1, 0.91, 0.91, 1, 1),
(535, 6097, 5, 1, 0.50, 0.50, 1, 1),
(536, 6098, 2, 1, 1.50, 1.50, 1, 1),
(537, 6098, 12, 1, 1.00, 1.00, 2, 1),
(538, 6099, 2, 1, 0.25, 0.25, 1, 1),
(539, 6100, 6, 1, 1.50, 1.00, 1, 1),
(540, 6101, 4, 1, 1.68, 1.68, 1, 1),
(541, 6102, 4, 1, 0.50, 0.50, 1, 1),
(542, 6103, 13, 1, 0.50, 0.50, 1, 1),
(543, 6103, 13, 1, 1.00, 1.00, 2, 1),
(544, 6103, 13, 1, 0.50, 0.50, 3, 1),
(545, 6104, 13, 1, 0.50, 0.50, 1, 1),
(546, 6104, 13, 1, 0.50, 0.50, 2, 1),
(547, 6104, 13, 1, 0.50, 0.50, 3, 1),
(548, 6105, 13, 1, 0.50, 0.50, 1, 1),
(549, 6105, 13, 1, 1.00, 1.00, 2, 1),
(550, 6105, 13, 1, 1.00, 1.00, 3, 1),
(551, 6106, 10, 1, 2.50, 2.50, 1, 1),
(552, 6106, 10, 1, 1.00, 1.00, 2, 1),
(553, 6107, 5, 1, 5.94, 5.94, 1, 1),
(554, 6108, 5, 1, 0.75, 0.75, 1, 1),
(555, 6109, 8, 1, 0.75, 0.75, 1, 1),
(556, 6110, 7, 1, 2.05, 1.75, 1, 1),
(557, 6111, 2, 1, 2.50, 2.50, 1, 1),
(558, 6112, 8, 1, 0.50, 0.50, 1, 1),
(559, 6113, 6, 1, 1.25, 1.25, 1, 1),
(560, 6113, 6, 1, 0.50, 0.50, 2, 1),
(561, 6113, 4, 1, 0.50, 0.50, 3, 1),
(562, 6113, 12, 1, 1.25, 1.25, 4, 1),
(563, 6113, 13, 1, 0.51, 0.51, 5, 1),
(564, 6113, 13, 1, 0.50, 0.50, 6, 1),
(565, 6113, 13, 1, 0.50, 0.50, 7, 1),
(566, 6113, 13, 1, 1.00, 1.00, 8, 1),
(567, 6114, 6, 1, 0.88, 0.88, 1, 1),
(568, 6115, 9, 1, 0.75, 0.75, 1, 1),
(569, 6116, 9, 1, 0.75, 0.75, 1, 1),
(570, 6117, 9, 1, 2.00, 1.00, 1, 1),
(571, 6118, 6, 1, 0.67, 0.67, 1, 1),
(572, 6119, 1, 1, 0.50, 0.50, 1, 1),
(573, 6119, 4, 1, 3.50, 3.50, 2, 1),
(574, 6119, 5, 1, 0.25, 0.25, 3, 1),
(575, 6120, 3, 1, 0.32, 0.32, 1, 1),
(576, 6121, 7, 1, 0.50, 0.50, 1, 1),
(577, 6122, 7, 1, 0.50, 0.50, 1, 1),
(578, 6123, 4, 1, 1.00, 1.00, 1, 1),
(579, 6123, 4, 1, 0.50, 0.50, 2, 1),
(580, 6123, 7, 1, 1.00, 1.00, 3, 1),
(581, 6124, 12, 1, 0.68, 0.68, 1, 1),
(582, 6125, 3, 1, 1.50, 1.50, 2, 1),
(583, 6125, 6, 1, 2.50, 2.50, 1, 1),
(584, 6126, 6, 1, 2.50, 2.50, 1, 1),
(585, 6127, 9, 1, 0.25, 0.25, 1, 1),
(586, 6127, 5, 1, 1.00, 1.00, 2, 1),
(587, 6128, 7, 1, 1.50, 1.50, 1, 1),
(588, 6129, 6, 1, 0.50, 0.50, 1, 1),
(589, 6130, 3, 1, 0.88, 0.88, 1, 1),
(590, 6131, 3, 1, 0.50, 0.50, 1, 1),
(591, 6132, 8, 1, 1.50, 1.50, 1, 1),
(592, 6133, 8, 1, 0.77, 0.77, 1, 1),
(593, 6134, 7, 1, 0.43, 0.43, 1, 1),
(594, 6134, 7, 1, 1.07, 1.07, 2, 1),
(595, 6135, 7, 1, 1.25, 1.25, 1, 1),
(596, 6136, 8, 1, 2.50, 2.50, 1, 1),
(597, 6137, 7, 1, 0.50, 0.50, 1, 1),
(598, 6138, 7, 1, 0.37, 0.37, 1, 1),
(599, 6138, 7, 1, 0.50, 0.50, 2, 1),
(600, 6138, 7, 1, 0.50, 0.50, 3, 1),
(601, 6139, 8, 1, 1.00, 1.00, 1, 1),
(602, 6140, 3, 1, 1.50, 1.50, 1, 1),
(603, 6141, 9, 1, 0.50, 0.50, 1, 1),
(604, 6142, 7, 1, 1.75, 0.75, 1, 1),
(605, 6143, 7, 1, 0.75, 0.75, 1, 1),
(606, 6144, 4, 1, 1.94, 1.00, 1, 1),
(607, 6145, 2, 1, 0.50, 0.50, 3, 1),
(608, 6145, 6, 1, 0.50, 0.50, 1, 1),
(609, 6145, 6, 1, 0.50, 0.50, 2, 1),
(610, 6146, 6, 1, 2.44, 2.44, 1, 1),
(611, 6147, 13, 1, 0.75, 0.75, 1, 1),
(612, 6148, 13, 1, 0.25, 0.25, 1, 1),
(613, 6149, 8, 1, 1.00, 1.00, 1, 1),
(614, 6150, 8, 1, 2.50, 2.50, 1, 1),
(615, 6151, 8, 1, 1.75, 1.75, 1, 1),
(616, 6152, 8, 1, 3.00, 3.00, 1, 1),
(617, 6153, 8, 1, 2.50, 2.50, 1, 1),
(618, 6154, 8, 1, 2.50, 2.50, 1, 1),
(619, 6155, 3, 1, 1.75, 1.75, 1, 1),
(620, 6156, 3, 1, 0.25, 0.25, 1, 1),
(621, 6156, 5, 1, 0.52, 0.52, 2, 1),
(622, 6157, 5, 1, 0.57, 0.57, 1, 1),
(623, 6158, 3, 1, 0.50, 0.50, 1, 1),
(624, 6158, 2, 1, 0.37, 0.37, 2, 1),
(625, 6159, 13, 1, 0.50, 0.50, 1, 1),
(626, 6160, 13, 1, 0.50, 0.50, 1, 1),
(627, 6161, 2, 1, 0.75, 0.75, 1, 1),
(628, 6161, 11, 1, 0.25, 0.25, 2, 1),
(629, 6161, 7, 1, 1.00, 1.00, 3, 1),
(630, 6161, 7, 1, 0.75, 0.75, 4, 1),
(631, 6161, 7, 1, 0.25, 0.25, 5, 1),
(632, 6162, 8, 1, 1.79, 1.79, 1, 1),
(633, 6163, 9, 1, 0.50, 0.50, 1, 1),
(634, 6164, 3, 1, 0.25, 0.25, 1, 1),
(635, 6164, 7, 1, 0.75, 0.75, 2, 1),
(636, 6165, 7, 1, 0.76, 0.76, 1, 1),
(637, 6166, 6, 1, 2.50, 2.50, 1, 1),
(638, 6167, 2, 1, 1.32, 1.32, 1, 1),
(639, 6168, 6, 1, 2.50, 2.50, 1, 1),
(640, 6169, 13, 1, 1.00, 0.50, 1, 1),
(641, 6170, 2, 1, 1.50, 1.50, 1, 1),
(642, 6171, 12, 1, 1.83, 1.83, 1, 1),
(643, 6172, 2, 1, 0.50, 0.50, 1, 1),
(644, 6173, 2, 1, 0.50, 0.50, 1, 1),
(645, 6174, 2, 1, 0.50, 0.50, 1, 1),
(646, 6175, 2, 1, 1.00, 1.00, 1, 1),
(647, 6176, 2, 1, 0.40, 0.40, 1, 1),
(648, 6177, 3, 1, 0.50, 0.50, 1, 1),
(649, 6177, 11, 1, 0.50, 0.50, 2, 1),
(650, 6177, 13, 1, 1.00, 1.00, 3, 1),
(651, 6178, 3, 1, 0.25, 0.25, 1, 1),
(652, 6178, 7, 1, 0.75, 0.75, 2, 1),
(653, 6179, 2, 1, 0.25, 0.25, 1, 1),
(654, 6179, 7, 1, 0.50, 0.50, 2, 1),
(655, 6179, 7, 1, 0.50, 0.50, 3, 1),
(656, 6179, 13, 1, 0.25, 0.25, 4, 1),
(657, 6179, 13, 1, 0.25, 0.25, 5, 1),
(658, 6179, 13, 1, 1.00, 1.00, 6, 1),
(659, 6180, 3, 1, 1.50, 1.50, 1, 1),
(660, 6181, 3, 1, 0.75, 0.75, 2, 1),
(661, 6181, 7, 1, 0.50, 0.50, 3, 1),
(662, 6182, 3, 1, 0.25, 0.25, 1, 1),
(663, 6183, 3, 1, 0.25, 0.25, 1, 1),
(664, 6184, 7, 1, 1.75, 1.75, 1, 1),
(665, 6185, 6, 1, 2.50, 2.50, 1, 1),
(666, 6186, 5, 1, 0.50, 0.50, 1, 1),
(667, 6187, 4, 1, 1.50, 1.00, 1, 1),
(668, 6188, 4, 1, 0.50, 0.50, 1, 1),
(669, 6189, 4, 1, 1.25, 1.25, 1, 1),
(670, 6190, 3, 1, 0.50, 0.50, 1, 1),
(671, 6191, 6, 1, 0.75, 0.75, 1, 1),
(672, 6192, 14, 1, 1.00, 0.50, 1, 1),
(673, 6193, 1, 1, 2.50, 2.50, 1, 1),
(674, 6194, 12, 1, 1.00, 1.00, 1, 1),
(675, 6195, 12, 1, 1.00, 1.00, 1, 1),
(676, 6195, 12, 1, 0.50, 0.50, 2, 1),
(677, 6196, 8, 1, 1.00, 1.00, 1, 1),
(678, 6196, 8, 1, 1.00, 1.00, 2, 1),
(679, 6196, 12, 1, 0.62, 0.62, 3, 1),
(680, 6196, 12, 1, 8.04, 8.04, 4, 1),
(681, 6197, 12, 1, 0.50, 0.50, 1, 1),
(682, 6198, 12, 1, 0.50, 0.50, 1, 1),
(683, 6199, 12, 1, 0.50, 0.50, 1, 1),
(684, 6199, 12, 1, 0.50, 0.50, 1, 1),
(685, 6200, 12, 1, 1.00, 1.00, 1, 1),
(686, 6200, 12, 1, 0.55, 0.55, 2, 1),
(687, 6201, 6, 1, 1.75, 1.75, 1, 1),
(688, 6202, 6, 1, 0.75, 0.75, 1, 1),
(689, 6203, 12, 1, 0.50, 0.50, 4, 1),
(690, 6203, 10, 1, 1.50, 1.50, 1, 1),
(691, 6203, 10, 1, 2.25, 2.25, 2, 1),
(692, 6203, 10, 1, 0.50, 0.50, 3, 1),
(693, 6204, 10, 1, 1.18, 1.18, 1, 1),
(694, 6204, 10, 1, 1.36, 1.36, 2, 1),
(695, 6205, 6, 1, 0.75, 0.75, 1, 1),
(696, 6205, 6, 1, 1.50, 1.50, 2, 1),
(697, 6206, 2, 1, 2.00, 2.00, 1, 1),
(698, 6207, 6, 1, 0.50, 0.50, 1, 1),
(699, 6208, 6, 1, 0.80, 0.80, 1, 1),
(700, 6209, 6, 1, 1.00, 1.00, 1, 1),
(701, 6210, 3, 1, 0.50, 0.50, 1, 1),
(702, 6210, 11, 1, 0.25, 0.25, 2, 1),
(703, 6211, 2, 1, 0.25, 0.25, 1, 1),
(704, 6212, 2, 1, 0.50, 0.50, 1, 1),
(705, 6212, 2, 1, 0.25, 0.25, 2, 1),
(706, 6213, 2, 1, 0.25, 0.25, 1, 1),
(707, 6213, 11, 1, 0.25, 0.25, 2, 1),
(708, 6214, 2, 1, 0.25, 0.25, 1, 1),
(709, 6214, 7, 1, 0.37, 0.37, 2, 1),
(710, 6215, 2, 1, 0.25, 0.25, 1, 1),
(711, 6216, 2, 1, 1.00, 1.00, 1, 1),
(712, 6217, 6, 1, 0.50, 0.50, 1, 1),
(713, 6218, 2, 1, 0.25, 0.25, 1, 1),
(714, 6219, 2, 1, 0.25, 0.25, 1, 1),
(715, 6220, 3, 1, 0.56, 0.56, 1, 1),
(716, 6220, 2, 1, 0.25, 0.25, 2, 1),
(717, 6220, 7, 1, 0.50, 0.50, 3, 1),
(718, 6221, 5, 1, 0.46, 0.46, 1, 1),
(719, 6222, 5, 1, 0.75, 0.75, 1, 1),
(720, 6223, 5, 1, 0.50, 0.50, 1, 1),
(721, 6224, 3, 1, 0.50, 0.50, 1, 1),
(722, 6224, 3, 1, 0.25, 0.25, 2, 1),
(723, 6224, 3, 1, 0.50, 0.50, 3, 1),
(724, 6225, 3, 1, 0.25, 0.25, 2, 1),
(725, 6225, 3, 1, 0.75, 0.75, 1, 1),
(726, 6226, 13, 1, 0.50, 0.50, 1, 1),
(727, 6227, 13, 1, 0.50, 0.50, 1, 1),
(728, 6228, 6, 1, 0.50, 0.50, 1, 1),
(729, 6229, 8, 1, 1.00, 0.50, 1, 1),
(730, 6230, 2, 1, 1.00, 1.00, 1, 1),
(731, 6230, 6, 1, 1.00, 1.00, 2, 1),
(732, 6230, 6, 1, 0.50, 0.50, 3, 1),
(733, 6231, 12, 1, 0.50, 0.50, 1, 1),
(734, 6232, 2, 1, 3.25, 3.25, 1, 1),
(735, 6232, 2, 1, 1.05, 1.05, 2, 1),
(736, 6233, 9, 1, 1.25, 1.25, 1, 1),
(737, 6233, 5, 1, 0.25, 0.25, 2, 1),
(738, 6234, 9, 1, 0.25, 0.25, 1, 1),
(739, 6234, 4, 1, 0.25, 0.25, 2, 1),
(740, 6234, 7, 1, 0.25, 0.25, 3, 1),
(741, 6235, 9, 1, 0.50, 0.50, 1, 1),
(742, 6235, 7, 1, 0.50, 0.50, 2, 1),
(743, 6236, 9, 1, 0.50, 0.50, 1, 1),
(744, 6236, 7, 1, 0.50, 0.50, 2, 1),
(745, 6237, 9, 1, 1.54, 1.54, 1, 1),
(746, 6238, 9, 1, 2.00, 2.00, 1, 1),
(747, 6239, 3, 1, 0.25, 0.25, 1, 1),
(748, 6239, 3, 1, 0.25, 0.25, 2, 1),
(749, 6240, 13, 1, 0.50, 0.50, 1, 1),
(750, 6240, 13, 1, 0.75, 0.75, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`id`, `nombre`, `slug`, `activo`) VALUES
(1, 'Contenedor - Registrar', 'contenedor.registrar', 1),
(2, 'Facturas - Registrar', 'facturas.registrar', 1),
(3, 'Packing - Registrar', 'packings.registrar', 1),
(4, 'Certificados - Registrar', 'certificados.registrar', 1),
(8, 'Semana Aperturar', 'semana.aperturar', 1),
(9, 'Semana Eliminar', 'semana.eliminar', 1),
(10, 'Contenedor Actualizar', 'contenedor.actualizar', 1),
(11, 'Contenedor Eliminar', 'contenedor.eliminar', 1),
(13, 'Mantenimientos Registrar', 'mantenimiento.registrar', 1),
(14, 'Mantenimientos Eliminar', 'mantenimiento.eliminar', 1),
(15, 'Contenedor - Cambiar Estado', 'contenedor.cambiar_estado', 1),
(16, 'Mantenimientos - Actualizar', 'mantenimiento.actualizar', 1),
(17, 'Certificados - Eliminar', 'certificados.eliminar', 1),
(18, 'Facturas - Eliminar', 'facturas.eliminar', 1),
(19, 'Packings List - Eliminar', 'packings.eliminar', 1),
(20, 'Actualizar Password', 'usuario.actualizar_password', 1),
(21, 'Actualizar Nombre Usuario', 'usuario.actualizar_usuario', 1),
(22, 'Valija - Registrar', 'valijas.registrar', 1),
(23, 'Valija - Eliminar', 'valijas.eliminar', 1),
(24, 'Enviar Emails Con Archivos', 'emails.enviar', 1),
(25, 'Gestión de Correos', 'correos.gestionar', 1),
(26, 'Asignación de Cajas', 'cajas.asignar', 1),
(27, 'Asignación de Operadores', 'operador.asignar', 1),
(28, 'Usuarios - Registrar', 'usuario.registrar', 1),
(29, 'Usuarios - Actualizar', 'usuario.actualizar', 1),
(30, 'Usuarios - Eliminar', 'usuario.eliminar', 1),
(31, 'Usuarios - Configuración', 'usuario.configuracion', 1),
(32, 'VLs - Registrar', 'vls.registrar', 1),
(33, 'VLs - Eliminar', 'vls.eliminar', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso_rol`
--

CREATE TABLE `permiso_rol` (
  `id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `permiso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso_rol`
--

INSERT INTO `permiso_rol` (`id`, `rol_id`, `permiso_id`) VALUES
(23, 1, 8),
(24, 1, 9),
(26, 1, 11),
(27, 1, 13),
(28, 1, 14),
(30, 2, 1),
(32, 2, 3),
(35, 2, 9),
(36, 2, 10),
(37, 2, 11),
(39, 2, 13),
(40, 2, 14),
(42, 1, 10),
(43, 1, 15),
(44, 1, 16),
(45, 2, 15),
(46, 2, 16),
(47, 1, 20),
(48, 2, 20),
(49, 6, 20),
(50, 1, 17),
(51, 1, 18),
(52, 1, 19),
(55, 1, 4),
(57, 2, 8),
(58, 7, 3),
(59, 7, 10),
(60, 7, 11),
(61, 7, 15),
(62, 7, 19),
(63, 1, 21),
(64, 1, 1),
(65, 1, 2),
(66, 1, 3),
(67, 1, 23),
(68, 1, 22),
(69, 3, 20),
(70, 8, 20),
(71, 8, 10),
(72, 9, 4),
(73, 9, 17),
(74, 8, 22),
(75, 8, 23),
(76, 2, 19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso_usuario`
--

CREATE TABLE `permiso_usuario` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `permiso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso_usuario`
--

INSERT INTO `permiso_usuario` (`id`, `usuario_id`, `permiso_id`) VALUES
(1, 2, 31),
(2, 2, 1),
(3, 2, 2),
(4, 2, 3),
(5, 2, 4),
(6, 2, 8),
(7, 2, 9),
(8, 2, 10),
(9, 2, 11),
(10, 2, 13),
(11, 2, 14),
(12, 2, 15),
(13, 2, 16),
(14, 2, 17),
(15, 2, 18),
(16, 2, 19),
(17, 2, 20),
(18, 2, 21),
(19, 2, 22),
(20, 2, 23),
(21, 2, 24),
(22, 2, 25),
(23, 2, 26),
(24, 2, 27),
(25, 2, 28),
(26, 2, 29),
(27, 2, 30),
(28, 4, 1),
(29, 4, 3),
(30, 4, 8),
(31, 4, 9),
(32, 4, 10),
(33, 4, 11),
(34, 4, 13),
(35, 4, 14),
(36, 4, 15),
(37, 4, 16),
(38, 4, 19),
(39, 4, 20),
(40, 4, 21),
(41, 4, 24),
(42, 21, 22),
(43, 21, 23),
(44, 20, 4),
(45, 20, 17),
(46, 19, 3),
(47, 19, 19),
(48, 17, 15),
(49, 18, 22),
(50, 18, 23),
(51, 18, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `dni` char(8) DEFAULT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_paterno` varchar(50) DEFAULT NULL,
  `apellido_materno` varchar(50) DEFAULT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `id_cargo` int(11) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `dni`, `nombre`, `apellido_paterno`, `apellido_materno`, `fullname`, `email`, `telefono`, `direccion`, `id_cargo`, `activo`) VALUES
(6, '00000000', 'APPBOSA', 'SAMAN', 'SAMAN', 'APPBOSA SAMAN SAMAN', 'appbosa@yahoo.com', '969412141', NULL, 2, 1),
(8, '40021803', 'Juan', 'Calderón', 'More', 'Juan Calderón More', 'jucamo78appbosa@hotmail.com', '969051503', NULL, 9, 1),
(21, '90879876', 'AGROFAIR', '', '', 'AGROFAIR  ', 'agrofair@hotmail.com', '987634589', NULL, 8, 1),
(22, '45645678', 'BIODYNAMISKA', '', '', 'BIODYNAMISKA  ', 'BIODYNAMISKA@hotmail.com', '986798907', NULL, 8, 1),
(23, '89897867', 'DOLE', '', '', 'DOLE  ', 'dole@hotmail.com', '890767345', NULL, 8, 1),
(24, '98779845', 'TRANSASTRA', '', '', 'TRANSASTRA  ', 'transastra@hotmail.com', '908745345', NULL, 8, 1),
(25, '98767678', 'EQUIFRUIT', '', '', 'EQUIFRUIT  ', 'equifruit@hotmail.com', '987876789', NULL, 8, 1),
(27, '42262238', 'Nilton Anterio', 'Ancajima', 'Castro', 'Nilton Anterio Ancajima Castro', 'nilton_ancajima@hotmail.com', '938228070', NULL, 10, 1),
(28, '42661236', 'Luis Alberto', 'Mejias', 'Escobar', 'Luis Alberto Mejias Escobar', 'luisalberto@hotmail.com', '111111111', NULL, 10, 1),
(29, NULL, 'San Miguel', NULL, NULL, 'San Miguel  ', 'sanmiguel@hotmail.com', NULL, NULL, 11, 1),
(30, '03498873', 'Raul', 'Rodriguez', 'Chero', 'Raul Rodriguez Chero', 'raul@hotmail.com', NULL, NULL, 10, 1),
(31, '02822149', 'Marcia Ninoska', 'Herrera', 'Reto', 'Marcia Ninoska Herrera Reto', 'marcia@hotmail.com', NULL, NULL, 12, 1),
(32, NULL, 'LA HANSEATICA', NULL, NULL, 'LA HANSEATICA  ', NULL, NULL, NULL, 11, 1),
(33, NULL, 'usuario', 'usuario', 'usuario', 'usuario usuario usuario', NULL, NULL, NULL, 13, 1),
(526, '25838134', 'MARTHA DEL PILAR', 'ABAD', 'ALBURQUEQUE', 'ABAD ALBURQUEQUE MARTHA DEL PILAR', NULL, NULL, NULL, NULL, 1),
(527, '25794121', 'ALEX MIGUEL', 'ABAD', 'ALBURQUEQUE', 'ABAD ALBURQUEQUE ALEX MIGUEL', NULL, NULL, NULL, NULL, 1),
(528, '44945398', 'FREDY ROLAND', 'ACARO', 'CASTRO', 'ACARO CASTRO FREDY ROLAND', NULL, NULL, NULL, NULL, 1),
(529, '18131653', 'JUAN ORLANDO', 'AGUIRRE', 'PELAEZ', 'AGUIRRE PELAEZ JUAN ORLANDO', NULL, NULL, NULL, NULL, 1),
(530, '03624300', 'DANIEL', 'AGURTO', 'CASTILLO', 'AGURTO CASTILLO  DANIEL', NULL, NULL, NULL, NULL, 1),
(531, '03624347', 'JACINTO', 'AGURTO', 'CASTILLO', 'AGURTO CASTILLO  JACINTO', NULL, NULL, NULL, NULL, 1),
(532, '03626033', 'RICARDO', 'AGURTO', 'CASTILLO', 'AGURTO CASTILLO  RICARDO', NULL, NULL, NULL, NULL, 1),
(533, '03627135', 'SANTIAGO', 'AGURTO', 'CASTILLO', 'AGURTO CASTILLO  SANTIAGO', NULL, NULL, NULL, NULL, 1),
(534, '03622208', 'FRANCISCO', 'AGURTO', 'CASTILLO', 'AGURTO CASTILLO FRANCISCO', NULL, NULL, NULL, NULL, 1),
(535, '03676729', 'MIGUEL', 'AGURTO', 'CASTILLO', 'AGURTO CASTILLO MIGUEL', NULL, NULL, NULL, NULL, 1),
(536, '03625758', 'LUIS', 'AGURTO', 'CESPEDES', 'AGURTO CESPEDES  LUIS ', NULL, NULL, NULL, NULL, 1),
(537, '40273124', 'ALICIA MABEL', 'AGURTO', 'GIRON', 'AGURTO GIRON  ALICIA MABEL', NULL, NULL, NULL, NULL, 1),
(538, '03669289', 'ARMIDA', 'AGURTO', 'GIRON', 'AGURTO GIRON ARMIDA', NULL, NULL, NULL, NULL, 1),
(539, '03496580', 'FRANCISCO', 'AGURTO', 'GIRON', 'AGURTO GIRON FRANCISCO', NULL, NULL, NULL, NULL, 1),
(540, '41136850', 'JUAN CARLOS', 'AGURTO', 'ORDINOLA', 'AGURTO ORDINOLA JUAN CARLOS', NULL, NULL, NULL, NULL, 1),
(541, '42016626', 'SANTIAGO', 'AGURTO', 'RODRIGUEZ', 'AGURTO RODRIGUEZ SANTIAGO', NULL, NULL, NULL, NULL, 1),
(542, '03660338', 'SANTOS ANDREA', 'AGURTO', 'RODRIGUEZ', 'AGURTO RODRIGUEZ SANTOS ANDREA', NULL, NULL, NULL, NULL, 1),
(543, '41126920', 'GEIDI JAQUELINE', 'AGURTO', 'ROMERO', 'AGURTO ROMERO GEIDI JAQUELINE', NULL, NULL, NULL, NULL, 1),
(544, '03686858', 'MIGUEL ALEJANDRO', 'AGURTO', 'RUIZ', 'AGURTO RUIZ  MIGUEL ALEJANDRO', NULL, NULL, NULL, NULL, 1),
(545, '41799367', 'CARLOS', 'AGURTO', 'ZEGARRA', 'AGURTO ZEGARRA CARLOS', NULL, NULL, NULL, NULL, 1),
(546, '02839195', 'RICARDO', 'AGURTO', 'ZEGARRA', 'AGURTO ZEGARRA RICARDO', NULL, NULL, NULL, NULL, 1),
(547, '40239268', 'LUIS ALFREDO', 'ALBURQUEQUE', 'SALDARRIAGA', 'ALBURQUEQUE SALDARRIAGA LUIS ALFREDO', NULL, NULL, NULL, NULL, 1),
(548, '', 'JUAN DIEGO', 'ALEJABO', 'ZAPATA', 'ALEJABO ZAPATA JUAN DIEGO', NULL, NULL, NULL, NULL, 1),
(549, '03687821', 'FLOR DE MARIA', 'ALVARADO', 'RAMOS', 'ALVARADO RAMOS  FLOR DE MARIA', NULL, NULL, NULL, NULL, 1),
(550, '03594392', 'ANTERIO', 'ANCAJIMA', 'SILVA', 'ANCAJIMA SILVA ANTERIO ', NULL, NULL, NULL, NULL, 1),
(551, '03625743', 'CARMEN', 'ANCAJIMA', 'SILVA', 'ANCAJIMA SILVA CARMEN', NULL, NULL, NULL, NULL, 1),
(552, '03594392', 'SANTOS INOCENCIO', 'ANCAJIMA', 'SILVA', 'ANCAJIMA SILVA SANTOS INOCENCIO', NULL, NULL, NULL, NULL, 1),
(553, '41775114', 'ORLANDO', 'ANDRADE', 'ATOCHE', 'ANDRADE ATOCHE ORLANDO', NULL, NULL, NULL, NULL, 1),
(554, '41498118', 'JESUS MANUEL', 'ANDRADE', 'CHERO', 'ANDRADE CHERO  JESUS MANUEL', NULL, NULL, NULL, NULL, 1),
(555, '42776564', 'OLGA', 'ANDRADE', 'CHERO', 'ANDRADE CHERO  OLGA', NULL, NULL, NULL, NULL, 1),
(556, '42957386', 'JORGE ISMAEL', 'ANDRADE', 'HERRERA', 'ANDRADE HERRERA JORGE ISMAEL', NULL, NULL, NULL, NULL, 1),
(557, '03677717', 'MELQUIADES', 'ARAMBULO', 'GOMEZ', 'ARAMBULO GOMEZ MELQUIADES', NULL, NULL, NULL, NULL, 1),
(558, '45839416', 'MARVIN', 'ARICA', 'SAAVEDRA', 'ARICA SAAVEDRA MARVIN', NULL, NULL, NULL, NULL, 1),
(559, '03671555', 'FRANCISCO FABIAN', 'ATO', 'NOLE', 'ATO NOLE FRANCISCO FABIAN', NULL, NULL, NULL, NULL, 1),
(560, '03629880', 'EVERARDO', 'ATOCHA', 'CHORRAS', 'ATOCHA CHORRAS EVERARDO', NULL, NULL, NULL, NULL, 1),
(561, '03629807', 'SANTOS ANTONIO', 'ATOCHA', 'CHORRAS', 'ATOCHA CHORRAS SANTOS ANTONIO', NULL, NULL, NULL, NULL, 1),
(562, '03627375', 'CLEMENTINO', 'ATOCHE', 'CALDERON', 'ATOCHE CALDERON CLEMENTINO', NULL, NULL, NULL, NULL, 1),
(563, '03627903', 'ESTANISLAO', 'ATOCHE', 'CALDERON', 'ATOCHE CALDERON ESTANISLAO', NULL, NULL, NULL, NULL, 1),
(564, '03671654', 'SANTOS', 'ATOCHE', 'CALDERON', 'ATOCHE CALDERON SANTOS', NULL, NULL, NULL, NULL, 1),
(565, '03623543', 'TEODORO', 'ATOCHE', 'CRUZ', 'ATOCHE CRUZ  TEODORO', NULL, NULL, NULL, NULL, 1),
(566, '03578838', 'MARIO', 'ATOCHE', 'DIOSES', 'ATOCHE DIOSES MARIO', NULL, NULL, NULL, NULL, 1),
(567, '03666651', 'AGUSTINA', 'ATOCHE', 'ESCOBAR', 'ATOCHE ESCOBAR AGUSTINA', NULL, NULL, NULL, NULL, 1),
(568, '40454281', 'ALEJANDRO', 'ATOCHE', 'ESCOBAR', 'ATOCHE ESCOBAR ALEJANDRO', NULL, NULL, NULL, NULL, 1),
(569, '40138518', 'GERONIMO', 'ATOCHE', 'ESCOBAR', 'ATOCHE ESCOBAR GERONIMO', NULL, NULL, NULL, NULL, 1),
(570, '43351435', 'ISABEL', 'ATOCHE', 'ESCOBAR', 'ATOCHE ESCOBAR ISABEL', NULL, NULL, NULL, NULL, 1),
(571, '41478943', 'JULIO CESAR', 'ATOCHE', 'ESCOBAR', 'ATOCHE ESCOBAR JULIO CESAR', NULL, NULL, NULL, NULL, 1),
(572, '03669287', 'MARIA DEL PILAR', 'ATOCHE', 'ESCOBAR', 'ATOCHE ESCOBAR MARIA DEL PILAR', NULL, NULL, NULL, NULL, 1),
(573, '41649879', 'ROSA', 'ATOCHE', 'ESCOBAR', 'ATOCHE ESCOBAR ROSA', NULL, NULL, NULL, NULL, 1),
(574, '03623950', 'JOSE SANTOS', 'ATOCHE', 'GONZALES', 'ATOCHE GONZALES JOSE SANTOS', NULL, NULL, NULL, NULL, 1),
(575, '40190454', 'POLICARPIO', 'ATOCHE', 'GONZALES', 'ATOCHE GONZALES POLICARPIO', NULL, NULL, NULL, NULL, 1),
(576, '43625327', 'SANTOS GABRIEL', 'ATOCHE', 'GONZALES', 'ATOCHE GONZALES SANTOS GABRIEL', NULL, NULL, NULL, NULL, 1),
(577, '40283656', 'VICTOR', 'ATOCHE', 'GONZALES', 'ATOCHE GONZALES VICTOR', NULL, NULL, NULL, NULL, 1),
(578, '03622040', 'SANTOS ELAUTERIO', 'ATOCHE', 'HIDALGO', 'ATOCHE HIDALGO  SANTOS ELAUTERIO', NULL, NULL, NULL, NULL, 1),
(579, '03623896', 'BARBARA', 'ATOCHE', 'IPANAQUE', 'ATOCHE IPANAQUE BARBARA', NULL, NULL, NULL, NULL, 1),
(580, '03623503', 'FELICITA', 'ATOCHE', 'IPANAQUE', 'ATOCHE IPANAQUE FELICITA', NULL, NULL, NULL, NULL, 1),
(581, '03590960', 'MARTINA', 'ATOCHE', 'IPANAQUE', 'ATOCHE IPANAQUE MARTINA', NULL, NULL, NULL, NULL, 1),
(582, '03496442', 'NICOLAS', 'ATOCHE', 'IPANAQUE', 'ATOCHE IPANAQUE NICOLAS', NULL, NULL, NULL, NULL, 1),
(583, '03623946', 'RUFINO', 'ATOCHE', 'IPANAQUE', 'ATOCHE IPANAQUE RUFINO', NULL, NULL, NULL, NULL, 1),
(584, '03628089', 'SANTOS', 'ATOCHE', 'IPANAQUE', 'ATOCHE IPANAQUE SANTOS', NULL, NULL, NULL, NULL, 1),
(585, '03627392', 'GODOFREDO', 'ATOCHE', 'JIMENEZ', 'ATOCHE JIMENEZ GODOFREDO', NULL, NULL, NULL, NULL, 1),
(586, '03627197', 'EMILIANO', 'ATOCHE', 'MARIÑAS', 'ATOCHE MARIÑAS EMILIANO', NULL, NULL, NULL, NULL, 1),
(587, '02839019', 'FLORO', 'ATOCHE', 'MARIÑAS', 'ATOCHE MARIÑAS FLORO', NULL, NULL, NULL, NULL, 1),
(588, '03627570', 'HEMENEGILDO', 'ATOCHE', 'PALOMINO', 'ATOCHE PALOMINO HEMENEGILDO', NULL, NULL, NULL, NULL, 1),
(589, '3626042', 'JULIO', 'ATOCHE', 'RIOFRIO', 'ATOCHE RIOFRIO JULIO', NULL, NULL, NULL, NULL, 1),
(590, '03627294', 'JOSE LUIS', 'ATOCHE', 'ROJAS', 'ATOCHE ROJAS JOSE LUIS', NULL, NULL, NULL, NULL, 1),
(591, '03880157', 'SANTOS RAFAEL', 'ATOCHE', 'SAAVEDRA', 'ATOCHE SAAVEDRA SANTOS RAFAEL', NULL, NULL, NULL, NULL, 1),
(592, '02839134', 'JAVIER ENRIQUE', 'ATOCHE', 'TALLEDO', 'ATOCHE TALLEDO  JAVIER ENRIQUE', NULL, NULL, NULL, NULL, 1),
(593, '03627273', 'FRANCISCO', 'ATOCHE', 'VARGAS', 'ATOCHE VARGAS FRANCISCO', NULL, NULL, NULL, NULL, 1),
(594, '40532258', 'JACINTO', 'ATOCHE', 'VILLARREAL', 'ATOCHE VILLARREAL JACINTO', NULL, NULL, NULL, NULL, 1),
(595, '03623583', 'LUCIANO', 'ATOCHE', 'VILLARREAL', 'ATOCHE VILLARREAL LUCIANO', NULL, NULL, NULL, NULL, 1),
(596, '03622978', 'MARIO', 'ATOCHE', 'VILLARREAL', 'ATOCHE VILLARREAL MARIO', NULL, NULL, NULL, NULL, 1),
(597, '03625113', 'ERESMIL', 'ATOCHE', 'VILLEGAS', 'ATOCHE VILLEGAS ERESMIL', NULL, NULL, NULL, NULL, 1),
(598, '03647676', 'MARIA', 'ATOCHE', 'YPANAQUE', 'ATOCHE YPANAQUE MARIA', NULL, NULL, NULL, NULL, 1),
(599, '03666706', 'JOSE', 'BURGOS', 'ROJAS', 'BURGOS ROJAS  JOSE', NULL, NULL, NULL, NULL, 1),
(600, '03623300', 'TOMAS', 'CALDERON', 'ALMESTAR', 'CALDERON ALMESTAR TOMAS', NULL, NULL, NULL, NULL, 1),
(601, '40671580', 'JACOBO', 'CALDERON', 'ATOCHE', 'CALDERON ATOCHE JACOBO', NULL, NULL, NULL, NULL, 1),
(602, '03689014', 'ROBERTO', 'CALDERON', 'ATOCHE', 'CALDERON ATOCHE ROBERTO', NULL, NULL, NULL, NULL, 1),
(603, '03626834', 'SEVERIANO', 'CALDERON', 'BALLADARES', 'CALDERON BALLADARES SEVERIANO', NULL, NULL, NULL, NULL, 1),
(604, '03627351', 'DOLORES', 'CALDERON', 'CASTILLO', 'CALDERON CASTILLO DOLORES', NULL, NULL, NULL, NULL, 1),
(605, '03625141', 'JUAN SANTOS', 'CALDERON', 'CRUZ', 'CALDERON CRUZ JUAN SANTOS', NULL, NULL, NULL, NULL, 1),
(606, '03660234', 'EDUARDO', 'CALDERON', 'GONZALES', 'CALDERON GONZALES EDUARDO', NULL, NULL, NULL, NULL, 1),
(607, '03660234', 'MARINO', 'CALDERON', 'GONZALES', 'CALDERON GONZALES MARINO', NULL, NULL, NULL, NULL, 1),
(608, '03657307', 'CESAR', 'CALDERON', 'MEJIA', 'CALDERON MEJIA CESAR', NULL, NULL, NULL, NULL, 1),
(609, '03686998', 'FRANCISCO', 'CALDERON', 'MEJIAS', 'CALDERON MEJIAS FRANCISCO', NULL, NULL, NULL, NULL, 1),
(610, '03623922', 'JUAN', 'CALDERON', 'MEJIAS', 'CALDERON MEJIAS JUAN', NULL, NULL, NULL, NULL, 1),
(611, '03678419', 'LUIS', 'CALDERON', 'MEJIAS', 'CALDERON MEJIAS LUIS', NULL, NULL, NULL, NULL, 1),
(612, '43782789', 'DIONICIO', 'CALDERON', 'MORE', 'CALDERON MORE DIONICIO', NULL, NULL, NULL, NULL, 1),
(613, '40021803', 'JUAN', 'CALDERON', 'MORE', 'CALDERON MORE JUAN', NULL, NULL, NULL, NULL, 1),
(614, '03663941', 'LAUREANO', 'CALDERON', 'MORE', 'CALDERON MORE LAUREANO', NULL, NULL, NULL, NULL, 1),
(615, '40772288', 'VICENTE', 'CALDERON', 'MORE', 'CALDERON MORE VICENTE', NULL, NULL, NULL, NULL, 1),
(616, '03657220', 'CEFERINO', 'CALDERON', 'NOLE', 'CALDERON NOLE CEFERINO', NULL, NULL, NULL, NULL, 1),
(617, '03623968', 'FRANCISCO', 'CALDERON', 'PALOMINO', 'CALDERON PALOMINO FRANCISCO', NULL, NULL, NULL, NULL, 1),
(618, '03623436', 'JOSE SANTOS', 'CALDERON', 'SALDARRIAGA', 'CALDERON SALDARRIAGA JOSE SANTOS', NULL, NULL, NULL, NULL, 1),
(619, '03624255', 'MARIO', 'CALDERON', 'VALLADARES', 'CALDERON VALLADARES MARIO', NULL, NULL, NULL, NULL, 1),
(620, '03622853', 'TOMAS', 'CALDERON', 'VALLADARES', 'CALDERON VALLADARES TOMAS', NULL, NULL, NULL, NULL, 1),
(621, '03625536', 'MARTIN', 'CARREÑO', 'OJEDA', 'CARREÑO OJEDA MARTIN', NULL, NULL, NULL, NULL, 1),
(622, '03624937', 'JUAN ARNALDO', 'CARRILLO', 'REYES', 'CARRILLO REYES  JUAN ARNALDO', NULL, NULL, NULL, NULL, 1),
(623, '41480184', 'WILDER', 'CARRILLO', 'VALLADARES', 'CARRILLO VALLADARES WILDER', NULL, NULL, NULL, NULL, 1),
(624, '03624509', 'DIONICIO', 'CARRILLO', 'VIERA', 'CARRILLO VIERA DIONICIO', NULL, NULL, NULL, NULL, 1),
(625, '03582048', 'BASILIA', 'CASTILLO', 'CARLIN', 'CASTILLO CARLIN BASILIA', NULL, NULL, NULL, NULL, 1),
(626, '03579103', 'JESUS MARIA', 'CASTILLO', 'CARLIN', 'CASTILLO CARLIN JESUS MARIA', NULL, NULL, NULL, NULL, 1),
(627, '03622388', 'JUANA', 'CASTILLO', 'CARLIN', 'CASTILLO CARLIN JUANA', NULL, NULL, NULL, NULL, 1),
(628, '03572271', 'FELICIANO', 'CASTILLO', 'CARRILLO', 'CASTILLO CARRILLO FELICIANO', NULL, NULL, NULL, NULL, 1),
(629, '43307331', 'ARSENIA', 'CASTILLO', 'RUIZ', 'CASTILLO RUIZ ARSENIA', NULL, NULL, NULL, NULL, 1),
(630, '44554240', 'DANIEL', 'CASTILLO', 'RUIZ', 'CASTILLO RUIZ DANIEL', NULL, NULL, NULL, NULL, 1),
(631, '71870578', 'JOSE', 'CASTILLO', 'RUIZ', 'CASTILLO RUIZ JOSE', NULL, NULL, NULL, NULL, 1),
(632, '71870577', 'ROLANDO', 'CASTILLO', 'RUIZ', 'CASTILLO RUIZ ROLANDO', NULL, NULL, NULL, NULL, 1),
(633, '03627598', 'SANTOS', 'CASTRO', 'ATOCHE', 'CASTRO ATOCHE  SANTOS', NULL, NULL, NULL, NULL, 1),
(634, '03628250', 'RAQUEL', 'CASTRO', 'ATOCHE', 'CASTRO ATOCHE DE URBINA RAQUEL', NULL, NULL, NULL, NULL, 1),
(635, '03624867', 'FRANCISCO', 'CASTRO', 'ATOCHE', 'CASTRO ATOCHE FRANCISCO', NULL, NULL, NULL, NULL, 1),
(636, '03623923', 'PEDRO PASCUAL', 'CASTRO', 'ATOCHE', 'CASTRO ATOCHE PEDRO PASCUAL', NULL, NULL, NULL, NULL, 1),
(637, '03623348', 'MARIA ESPERANZA', 'CASTRO', 'DE JIMENEZ', 'CASTRO DE JIMENEZ MARIA ESPERANZA', NULL, NULL, NULL, NULL, 1),
(638, '03627876', 'ESTERFILIA', 'CASTRO', 'DE ORDINOLA', 'CASTRO DE ORDINOLA ERTERFILIA', NULL, NULL, NULL, NULL, 1),
(639, '03573683', 'DANTE RODOLFO', 'CASTRO', 'PONCE', 'CASTRO PONCE DANTE RODOLFO', NULL, NULL, NULL, NULL, 1),
(640, '03671777', 'JAVIER', 'CASTRO', 'RIOFRIO', 'CASTRO RIOFRIO JAVIER', NULL, NULL, NULL, NULL, 1),
(641, '03689071', 'JOSE', 'CASTRO', 'RIOFRIO', 'CASTRO RIOFRIO JOSE', NULL, NULL, NULL, NULL, 1),
(642, '03893297', 'JOSE AGUSTIN', 'CASTRO', 'SALDARRIAGA', 'CASTRO SALDARRIAGA JOSE AGUSTIN', NULL, NULL, NULL, NULL, 1),
(643, '03628629', 'JULIO ANTENOR', 'CASTRO', 'SALDARRIAGA', 'CASTRO SALDARRIAGA JULIO ANTENOR', NULL, NULL, NULL, NULL, 1),
(644, '03627877', 'EUSEBIO', 'CASTRO', 'VALLADARES', 'CASTRO VALLADARES EUSEBIO', NULL, NULL, NULL, NULL, 1),
(645, '03623516', 'FLORESMINDA', 'CHAPILLIQUEN', 'DE MORE', 'CHAPILLIQUEN DE MORE FLORESMILDA', NULL, NULL, NULL, NULL, 1),
(646, '42955201', 'DORIS DEL PILAR', 'CHERO', 'DURAND', 'CHERO DURAND DORIS DEL PILAR', NULL, NULL, NULL, NULL, 1),
(647, '42207904', 'ISAAC', 'CHERO', 'DURAND', 'CHERO DURAND ISAAC', NULL, NULL, NULL, NULL, 1),
(648, '42207867', 'JOSE ORLANDO', 'CHERO', 'DURAND', 'CHERO DURAND JOSE ORLANDO', NULL, NULL, NULL, NULL, 1),
(649, '03474585', 'LUIS MARTIN', 'CHERO', 'GALECIO', 'CHERO GALECIO LUIS MARTIN', NULL, NULL, NULL, NULL, 1),
(650, '03623948', 'EUSEBIA MARIA', 'CHERO', 'PANTA', 'CHERO PANTA  EUSEBIA MARIA', NULL, NULL, NULL, NULL, 1),
(651, '03623938', 'PETRONILA', 'CHERO', 'PANTA', 'CHERO PANTA  PETRONILA', NULL, NULL, NULL, NULL, 1),
(652, '03623131', 'PEDRO', 'CHORRES', 'MONTERO', 'CHORRES MONTERO PEDRO', NULL, NULL, NULL, NULL, 1),
(653, '03624893', 'PABLO ALBERTO', 'COLMENARES', 'TINOCO', 'COLMENARES TINOCO PABLO ALBERTO ', NULL, NULL, NULL, NULL, 1),
(654, '03676745', 'ANGEL OVIDIO', 'CORDOVA', 'ANDRADE', 'CORDOVA ANDRADE ANGEL OVIDIO', NULL, NULL, NULL, NULL, 1),
(655, '03654667', 'LAURA ISABEL', 'CORDOVA', 'ANDRADE', 'CORDOVA ANDRADE LAURA ISABEL', NULL, NULL, NULL, NULL, 1),
(656, '40305993', 'EBER HERNAN', 'CORDOVA', 'ATOCHE', 'CORDOVA ATOCHE EBER HERNAN', NULL, NULL, NULL, NULL, 1),
(657, '03625711', 'JUVENTINO', 'CORDOVA', 'ROMERO', 'CORDOVA ROMERO  JUVENTINO', NULL, NULL, NULL, NULL, 1),
(658, '02854586', 'FRANCISCO', 'CORREA', 'HERRERA', 'CORREA HERRERA FRANCISCO', NULL, NULL, NULL, NULL, 1),
(659, '41563472', 'ANA MARIA', 'COSTI', 'ATOCHE', 'COSTI ATOCHE ANA MARIA', NULL, NULL, NULL, NULL, 1),
(660, '', 'ISAAC', 'COSTI', 'FLOREANO', 'COSTI FLOREANO ISAAC', NULL, NULL, NULL, NULL, 1),
(661, '42964474', 'CARLOS', 'CRISANTO', 'MARCELO', 'CRISANTO MARCELO CARLOS', NULL, NULL, NULL, NULL, 1),
(662, '45034791', 'JOSE ALEJANDRO', 'CRISANTO', 'MARCELO', 'CRISANTO MARCELO JOSE ALEJANDRO', NULL, NULL, NULL, NULL, 1),
(663, '03627313', 'HILARIO', 'DEL ROSARIO', 'ROSARIO', 'DEL ROSARIO ATOCHE  HILARIO', NULL, NULL, NULL, NULL, 1),
(664, '03625833', 'JUAN', 'DEL ROSARIO', 'ROSARIO', 'DEL ROSARIO ATOCHE JUAN', NULL, NULL, NULL, NULL, 1),
(665, '03880666', 'JULIO', 'DEL ROSARIO', 'ROSARIO', 'DEL ROSARIO ATOCHE JULIO', NULL, NULL, NULL, NULL, 1),
(666, '03624795', 'ZOILO', 'DEL ROSARIO', 'ROSARIO', 'DEL ROSARIO ATOCHE ZOILO', NULL, NULL, NULL, NULL, 1),
(667, '40409971', 'JAVIER', 'DEL ROSARIO', 'ROSARIO', 'DEL ROSARIO CASTRO JAVIER', NULL, NULL, NULL, NULL, 1),
(668, '03880666', 'JHINO PAOLO', 'DEL ROSARIO', 'ROSARIO', 'DEL ROSARIO CASTRO JHINO PAOLO', NULL, NULL, NULL, NULL, 1),
(669, '03627343', 'ZOILA', 'DEL ROSARIO', 'ROSARIO', 'DEL ROSARIO DE ANCAJIMA ZOILA', NULL, NULL, NULL, NULL, 1),
(670, '42075534', 'ROBERTO CARLOS', 'DEL ROSARIO', 'ROSARIO', 'DEL ROSARIO DELGADO ROBERTO CARLOS', NULL, NULL, NULL, NULL, 1),
(671, '03627434', 'MARESLA', 'DELGADO', 'DE DEL ROSARIO', 'DELGADO DE DEL ROSARIO MARESLA', NULL, NULL, NULL, NULL, 1),
(672, '03627347', 'MARLENI', 'DELGADO', 'MEJIA', 'DELGADO MEJIA DE ATOCHE MARLENI', NULL, NULL, NULL, NULL, 1),
(673, '03657313', 'JAIME', 'DELGADO', 'MEJIA', 'DELGADO MEJIA JAIME', NULL, NULL, NULL, NULL, 1),
(674, '03628628', 'LUIS JORGE', 'DELGADO', 'MEJIAS', 'DELGADO MEJIAS LUIS JORGE', NULL, NULL, NULL, NULL, 1),
(675, '03623941', 'LUIS JULIO', 'DELGADO', 'SALDARRIAGA', 'DELGADO SALDARRIAGA LUIS JULIO', NULL, NULL, NULL, NULL, 1),
(676, '15856857', 'VICTOR ALEJANDRO', 'DIONICIO', 'GRANADOS', 'DIONICIO GRANADOS VICTOR ALEJANDRO', NULL, NULL, NULL, NULL, 1),
(677, '03626454', 'FEDERICO', 'DIOSES', 'JIMENEZ', 'DIOSES JIMENEZ  FEDERICO', NULL, NULL, NULL, NULL, 1),
(678, '03624303', 'JUAN', 'ESCOBAR', 'AGURTO', 'ESCOBAR AGURTO  JUAN', NULL, NULL, NULL, NULL, 1),
(679, '41476638', 'RONALD', 'ESCOBAR', 'ATOCHE', 'ESCOBAR ATOCHE  RONALD', NULL, NULL, NULL, NULL, 1),
(680, '10190733', 'OMAR', 'ESCOBAR', 'CARRILLO', 'ESCOBAR CARRILLO OMAR', NULL, NULL, NULL, NULL, 1),
(681, '03854726', 'HERMINIA', 'ESCOBAR', 'DE VILLARREYES', 'ESCOBAR DE VILLARREYES HERMINIA', NULL, NULL, NULL, NULL, 1),
(682, '03686991', 'TEODORO', 'ESCOBAR', 'ESTRADA', 'ESCOBAR ESTRADA  TEODORO', NULL, NULL, NULL, NULL, 1),
(683, '03626406', 'LEONARDA', 'ESCOBAR', 'MENA', 'ESCOBAR MENA DE VARGAS LEONARDA', NULL, NULL, NULL, NULL, 1),
(684, '03623955', 'ISABEL', 'ESCOBAR', 'MENA', 'ESCOBAR MENA ISABEL', NULL, NULL, NULL, NULL, 1),
(685, '03623955', 'JUAN', 'ESCOBAR', 'MENA', 'ESCOBAR MENA JUAN', NULL, NULL, NULL, NULL, 1),
(686, '03627142', 'LEONARDO', 'ESCOBAR', 'MENA', 'ESCOBAR MENA LEONARDO', NULL, NULL, NULL, NULL, 1),
(687, '03626328', 'SANTOS', 'ESCOBAR', 'MENA', 'ESCOBAR MENA SANTOS', NULL, NULL, NULL, NULL, 1),
(688, '45018361', 'GABY TATIANA', 'ESCOBAR', 'ORDINOLA', 'ESCOBAR ORDINOLA GABY TATIANA', NULL, NULL, NULL, NULL, 1),
(689, '80313804', 'BERTHA AMELIA', 'ESPINOZA', 'ATOCHA', 'ESPINOZA ATOCHA BERTHA AMELIA', NULL, NULL, NULL, NULL, 1),
(690, '03653392', 'JULIO ENRIQUE', 'ESPINOZA', 'ATOCHE', 'ESPINOZA ATOCHE JULIO ENRIQUE', NULL, NULL, NULL, NULL, 1),
(691, '03624125', 'ANDRES', 'ESPINOZA', 'SANDOVAL', 'ESPINOZA SANDOVAL ANDRES', NULL, NULL, NULL, NULL, 1),
(692, '03624882', 'CRISTOBAL', 'ESPINOZA', 'SANDOVAL', 'ESPINOZA SANDOVAL CRISTOBAL', NULL, NULL, NULL, NULL, 1),
(693, '03654409', 'ALEJANDRO', 'ESTRADA', 'GARCIA', 'ESTRADA GARCIA ALEJANDRO', NULL, NULL, NULL, NULL, 1),
(694, '03625989', 'ANDRES', 'ESTRADA', 'GARCIA', 'ESTRADA GARCIA ANDRES', NULL, NULL, NULL, NULL, 1),
(695, '03673650', 'JUAN', 'ESTRADA', 'GARCIA', 'ESTRADA GARCIA JUAN', NULL, NULL, NULL, NULL, 1),
(696, '03660326', 'PEDRO', 'ESTRADA', 'GARCIA', 'ESTRADA GARCIA PEDRO', NULL, NULL, NULL, NULL, 1),
(697, '03623789', 'SANTIAGO', 'ESTRADA', 'GARCIA', 'ESTRADA GARCIA SANTIAGO', NULL, NULL, NULL, NULL, 1),
(698, '03624436', 'PEDRO', 'ESTRADA', 'IPANAQUE', 'ESTRADA IPANAQUE  PEDRO', NULL, NULL, NULL, NULL, 1),
(699, '03689055', 'HENRY', 'ESTRADA', 'IPANAQUE', 'ESTRADA IPANAQUE HENRY', NULL, NULL, NULL, NULL, 1),
(700, '03654524', 'JOSE NICOLAS', 'ESTRADA', 'IPANAQUE', 'ESTRADA IPANAQUE JOSE NICOLAS', NULL, NULL, NULL, NULL, 1),
(701, '80299055', 'ASUNCIONA', 'ESTRADA', 'URBINA', 'ESTRADA URBINA ASUNCIONA', NULL, NULL, NULL, NULL, 1),
(702, '02839141', 'GILBERTO', 'FARIAS', 'ANCAJIMA', 'FARIAS ANCAJIMA GILBERTO', NULL, NULL, NULL, NULL, 1),
(703, '03880005', 'ANGELICA', 'FARIAS', 'CALDERON', 'FARIAS CALDERON  ANGELICA', NULL, NULL, NULL, NULL, 1),
(704, '03669211', 'JOSE', 'FARIAS', 'CALDERON', 'FARIAS CALDERON  JOSE', NULL, NULL, NULL, NULL, 1),
(705, '03647628', 'ALEJANDRO', 'FARIAS', 'CALDERON', 'FARIAS CALDERON ALEJANDRO', NULL, NULL, NULL, NULL, 1),
(706, '03657312', 'JOSE LUIS', 'FARIAS', 'DELGADO', 'FARIAS DELGADO JOSE LUIS', NULL, NULL, NULL, NULL, 1),
(707, '03628395', 'RUPERTO', 'FARIAS', 'FARIAS', 'FARIAS FARIAS RUPERTO', NULL, NULL, NULL, NULL, 1),
(708, '03880052', 'CONCEPCION', 'FARIAS', 'MENA', 'FARIAS MENA CONCEPCION', NULL, NULL, NULL, NULL, 1),
(709, '03624493', 'NICOLAS', 'FARIAS', 'MENA', 'FARIAS MENA NICOLAS', NULL, NULL, NULL, NULL, 1),
(710, '03623967', 'WILFREDO', 'FARIAS', 'MENA', 'FARIAS MENA WILFREDO', NULL, NULL, NULL, NULL, 1),
(711, '40551601', 'ELMER', 'FARIAS', 'ROSALES', 'FARIAS ROSALES  ELMER', NULL, NULL, NULL, NULL, 1),
(712, '03627341', 'VICTOR', 'FARIAS', 'ROSALES', 'FARIAS ROSALES  VICTOR', NULL, NULL, NULL, NULL, 1),
(713, '03676771', 'JORGE', 'FARIAS', 'ROSALES', 'FARIAS ROSALES JORGE', NULL, NULL, NULL, NULL, 1),
(714, '03627312', 'JOSE', 'FARIAS', 'ROSALES', 'FARIAS ROSALES JOSE', NULL, NULL, NULL, NULL, 1),
(715, '15758945', 'JUAN', 'FARIAS', 'ROSALES', 'FARIAS ROSALES JUAN', NULL, NULL, NULL, NULL, 1),
(716, '03669335', 'JULIO', 'FARIAS', 'ROSALES', 'FARIAS ROSALES JULIO', NULL, NULL, NULL, NULL, 1),
(717, '03627436', 'SANTOS', 'FARIAS', 'ROSALES', 'FARIAS ROSALES SANTOS', NULL, NULL, NULL, NULL, 1),
(718, '40483093', 'MARITZA', 'FARIAS', 'VILLARREYES', 'FARIAS VILLARREYES MARITZA', NULL, NULL, NULL, NULL, 1),
(719, '43595356', 'RUDY ANGELO', 'FARIAS', 'VILLARREYES', 'FARIAS VILLARREYES RUDY ANGELO', NULL, NULL, NULL, NULL, 1),
(720, '41421104', 'VICTOR', 'FARIAS', 'VILLARREYES', 'FARIAS VILLARREYES VICTOR', NULL, NULL, NULL, NULL, 1),
(721, '42600066', 'DIÑER', 'FERNANDEZ', 'FERNANDEZ', 'FERNANDEZ FERNANDEZ DIÑER', NULL, NULL, NULL, NULL, 1),
(722, '', 'ELBER PAUL', 'FLOREANO', 'ORDINOLA', 'FLOREANO ORDINOLA ELBER PAUL', NULL, NULL, NULL, NULL, 1),
(723, '', 'JOSE LUIS', 'FLOREANO', 'ORDINOLA', 'FLOREANO ORDINOLA JOSE LUIS', NULL, NULL, NULL, NULL, 1),
(724, '43049791', 'SEGUNDO', 'FLOREANO', 'ORDINOLA', 'FLOREANO ORDINOLA SEGUNDO', NULL, NULL, NULL, NULL, 1),
(725, '03565242', 'PEDRO', 'FLOREANO', 'PALOMINO', 'FLOREANO PALOMINO PEDRO', NULL, NULL, NULL, NULL, 1),
(726, '03624044', 'FLORENCIO', 'FLOREANO', 'RUIZ', 'FLOREANO RUIZ FLORENCIO', NULL, NULL, NULL, NULL, 1),
(727, '42151790', 'WILMER HELARD', 'FLORES', 'SALINAS', 'FLORES SALINAS WILMER HELARD', NULL, NULL, NULL, NULL, 1),
(728, '03661958', 'MARIA ELENA', 'GALLO', 'CASTRO', 'GALLO CASTRO MARIA ELENA', NULL, NULL, NULL, NULL, 1),
(729, '03671750', 'ROBERTO', 'GALLO', 'CASTRO', 'GALLO CASTRO ROBERTO', NULL, NULL, NULL, NULL, 1),
(730, '80298263', 'PETRONILA DAEMA', 'GARAY', 'ARIAS', 'GARAY ARIAS PETRONILA DAEMA', NULL, NULL, NULL, NULL, 1),
(731, '03625265', 'ROJAS ELEOVINA', 'GARAY', 'DE ROJAS', 'GARAY DE ROJAS ELEOVINA', NULL, NULL, NULL, NULL, 1),
(732, '03626403', 'WILFREDO', 'GARCES', 'VILLASECA', 'GARCES VILLASECA WILFREDO', NULL, NULL, NULL, NULL, 1),
(733, '80584089', 'SANTOS EDILBERTO', 'GARCIA', 'CASTILLO', 'GARCIA CASTILLO  SANTOS EDILBERTO', NULL, NULL, NULL, NULL, 1),
(734, '03623960', 'ANTONIO', 'GARCIA', 'CISNEROS', 'GARCIA CISNEROS ANTONIO', NULL, NULL, NULL, NULL, 1),
(735, '03657340', 'SANTOS', 'GARCIA', 'CISNEROS', 'GARCIA CISNEROS SANTOS', NULL, NULL, NULL, NULL, 1),
(736, '45079029', 'ROSARIO GENESIS', 'GARCIA', 'DEL ROSARIO', 'GARCIA DEL ROSARIO GENESIS', NULL, NULL, NULL, NULL, 1),
(737, '42178099', 'AGUSTIN', 'GARCIA', 'IPANAQUE', 'GARCIA IPANAQUE AGUSTIN', NULL, NULL, NULL, NULL, 1),
(738, '42262234', 'LIDER RIGOBERTO', 'GARCIA', 'IPANAQUE', 'GARCIA IPANAQUE LIDER RIGOBERTO', NULL, NULL, NULL, NULL, 1),
(739, '43584853', 'MARIA CLAUDIA', 'GARCIA', 'IPANAQUE', 'GARCIA IPANAQUE MARIA CLAUDIA', NULL, NULL, NULL, NULL, 1),
(740, '42252157', 'NILTON LIZARDO', 'GARCIA', 'IPANAQUE', 'GARCIA IPANAQUE NILTON LIZARDO', NULL, NULL, NULL, NULL, 1),
(741, '80298314', 'SANTOS PABLO', 'GARCIA', 'IPANAQUE', 'GARCIA IPANAQUE SANTOS PABLO', NULL, NULL, NULL, NULL, 1),
(742, '03647608', 'TRANQUILINO', 'GONZALES', 'CALDERON', 'GONZALES CALDERON  TRANQUILINO', NULL, NULL, NULL, NULL, 1),
(743, '03623399', 'PASCUAL', 'GONZALES', 'CALDERON', 'GONZALES CALDERON PASCUAL', NULL, NULL, NULL, NULL, 1),
(744, '03686854', 'JOSE ELEAZAR', 'GONZALES', 'ORDINOLA', 'GONZALES ORDINOLA JOSE ELEAZAR', NULL, NULL, NULL, NULL, 1),
(745, '03622556', 'SANTOS', 'GONZALES', 'ZAPATA', 'GONZALES ZAPATA SANTOS', NULL, NULL, NULL, NULL, 1),
(746, '03625155', 'VICTORIO', 'GONZALES', 'ZEGARRA', 'GONZALES ZEGARRA VICTORIO', NULL, NULL, NULL, NULL, 1),
(747, '03626701', 'SANTOS', 'GUTIERREZ', 'CHERO', 'GUTIERREZ CHERO SANTOS', NULL, NULL, NULL, NULL, 1),
(748, '41797550', 'DANY DANIEL', 'HERRERA', 'DURAN', 'HERRERA DURAN DANY DANIEL', NULL, NULL, NULL, NULL, 1),
(749, '03660260', 'JESUS', 'HERRERA', 'PALOMINO', 'HERRERA PALOMINO JESUS', NULL, NULL, NULL, NULL, 1),
(750, '03626439', 'MADRID GREGORIO', 'INFANTE', 'LA MADRID', 'INFANTE LA MADRID GREGORIO', NULL, NULL, NULL, NULL, 1),
(751, '42591104', 'EULALIO', 'INFANTE', 'VALLADARRES', 'INFANTE VALLADARRES EULALIO', NULL, NULL, NULL, NULL, 1),
(752, '03624219', 'ELADIO', 'INGA', 'CASTILLO', 'INGA CASTILLO ELADIO', NULL, NULL, NULL, NULL, 1),
(753, '42956230', 'DALILA NOEMI', 'INGA', 'RUIZ', 'INGA RUIZ DALILA NOEMI', NULL, NULL, NULL, NULL, 1),
(754, '80313577', 'MARIA CRUZ', 'IPANAQUE', 'CHERO', 'IPANAQUE CHERO MARIA CRUZ', NULL, NULL, NULL, NULL, 1),
(755, '02854218', 'JUAN', 'IPANAQUE', 'QUEVEDO', 'IPANAQUE QUEVEDO JUAN', NULL, NULL, NULL, NULL, 1),
(756, '03620648', 'JESUS', 'IPANAQUE', 'RAMOS', 'IPANAQUE RAMOS JESUS', NULL, NULL, NULL, NULL, 1),
(757, '40862902', 'JAIME', 'IPANAQUE', 'SUAREZ', 'IPANAQUE SUAREZ JAIME', NULL, NULL, NULL, NULL, 1),
(758, '03627349', 'CRISOSTOMO', 'IPANAQUE', 'VILLEGAS', 'IPANAQUE VILLEGAS  CRISOSTOMO', NULL, NULL, NULL, NULL, 1),
(759, '03625835', 'GENARO', 'IPANAQUE', 'VILLEGAS', 'IPANAQUE VILLEGAS  GENARO', NULL, NULL, NULL, NULL, 1),
(760, '44351401', 'ADRIANO', 'JARAMILLO', 'ATOCHE', 'JARAMILLO ATOCHE ADRIANO', NULL, NULL, NULL, NULL, 1),
(761, '03647698', 'VICENTE', 'JARAMILLO', 'CASTILLO', 'JARAMILLO CASTILLO VICENTE', NULL, NULL, NULL, NULL, 1),
(762, '03623183', 'ELIO', 'JARAMILLO', 'SOCOLA', 'JARAMILLO SOCOLA ELIO', NULL, NULL, NULL, NULL, 1),
(763, '03623606', 'ELBERTH', 'JIMENEZ', 'ATOCHE', 'JIMENEZ ATOCHE  ELBERTH', NULL, NULL, NULL, NULL, 1),
(764, '43848365', 'FELIPE SANTIAGO', 'LA MADRID', 'INFANTE', 'LA MADRID INFANTE FELIPE SANTIAGO', NULL, NULL, NULL, NULL, 1),
(765, '', 'PEDRO', 'LOPEZ', 'FLORES', 'LOPEZ FLORES PEDRO', NULL, NULL, NULL, NULL, 1),
(766, '03661271', 'DENNIS', 'LOZANO', 'JUAREZ', 'LOZANO JUAREZ DENNIS', NULL, NULL, NULL, NULL, 1),
(767, '03627566', 'FELIPE', 'LY', 'ORTIZ', 'LY ORTIZ FELIPE', NULL, NULL, NULL, NULL, 1),
(768, '03670968', 'AMALIA', 'MACALUPU', 'CASTRO', 'MACALUPU CASTRO AMALIA', NULL, NULL, NULL, NULL, 1),
(769, '03650767', 'HORACIO', 'MACALUPU', 'CASTRO', 'MACALUPU CASTRO HORACIO', NULL, NULL, NULL, NULL, 1),
(770, '03628065', 'JOSE', 'MACALUPU', 'CASTRO', 'MACALUPU CASTRO JOSE ', NULL, NULL, NULL, NULL, 1),
(771, '3654527', 'CARMEN ROSA', 'MACALUPU', 'FLOREANO', 'MACALUPU FLOREANO DE CHANDOVI CARMEN ROSA', NULL, NULL, NULL, NULL, 1),
(772, '03671663', 'SANTOS GEORGINA', 'MACALUPU', 'OBLEA', 'MACALUPU OBLEA SANTOS GEORGINA', NULL, NULL, NULL, NULL, 1),
(773, '03628041', 'JOSE HUMBERTO', 'MACALUPU', 'SILUPU', 'MACALUPU SILUPU  JOSE HUMBERTO ', NULL, NULL, NULL, NULL, 1),
(774, '', 'TEODOMIRO', 'MADRID', 'GONZALES', 'MADRID GONZALES TEODOMIRO', NULL, NULL, NULL, NULL, 1),
(775, '03624121', 'MERCEDES', 'MADRID', 'SANCHEZ', 'MADRID SANCHEZ MERCEDES', NULL, NULL, NULL, NULL, 1),
(776, '03671771', 'WILFREDO', 'MARCHENA', 'CORDOVA', 'MARCHENA CORDOVA WILFREDO', NULL, NULL, NULL, NULL, 1),
(777, '03622299', 'BACILIO', 'MARIÑAS', 'ATOCHE', 'MARIÑAS ATOCHE BACILIO', NULL, NULL, NULL, NULL, 1),
(778, '03626379', 'MARÍA TRINIDAD', 'MARIÑAS', 'ATOCHE', 'MARIÑAS ATOCHE MARÍA TRINIDAD', NULL, NULL, NULL, NULL, 1),
(779, '44681855', 'JUAN CARLOS', 'MARTINEZ', 'GUERRERO', 'MARTINEZ GUERRERO JUAN CARLOS', NULL, NULL, NULL, NULL, 1),
(780, '03626379', 'JULIA', 'MARTINEZ', 'HERRERA', 'MARTINEZ HERRERA JULIA', NULL, NULL, NULL, NULL, 1),
(781, '03622616', 'LEONCIO', 'MAURICIO', 'ATOCHE', 'MAURICIO ATOCHE LEONCIO', NULL, NULL, NULL, NULL, 1),
(782, '03625300', 'MARIA ISABEL', 'MEDINA', 'JUAREZ', 'MEDINA JUAREZ DE ORDINOLA MARIA ISABEL', NULL, NULL, NULL, NULL, 1),
(783, '03843372', 'SANTOS JULIO', 'MEJIA', 'CHERO', 'MEJIA CHERO  SANTOS JULIO', NULL, NULL, NULL, NULL, 1),
(784, '10691586', 'JUAN', 'MEJIA', 'CHERO', 'MEJIA CHERO JUAN', NULL, NULL, NULL, NULL, 1),
(785, '48175127', 'ANYI CLEYDICRUZ', 'MEJIA', 'ORDINOLA', 'MEJIA ORDINOLA ANYI CLEYDICRUZ', NULL, NULL, NULL, NULL, 1),
(786, '03625967', 'SANTOS', 'MEJIAS', 'AGURTO', 'MEJIAS AGURTO SANTOS', NULL, NULL, NULL, NULL, 1),
(787, '03623791', 'GENARO', 'MEJIAS', 'ATOCHE', 'MEJIAS ATOCHE GENARO', NULL, NULL, NULL, NULL, 1),
(788, '40973633', 'ANTONIO EDGAR', 'MEJIAS', 'ESCOBAR', 'MEJIAS ESCOBAR ANTONIO EDGAR', NULL, NULL, NULL, NULL, 1),
(789, '03682565', 'FERMIN', 'MEJIAS', 'ESCOBAR', 'MEJIAS ESCOBAR FERMIN', NULL, NULL, NULL, NULL, 1),
(790, '47657859', 'KEVIN ANTONY', 'MEJIAS', 'ESCOBAR', 'MEJIAS ESCOBAR KEVIN ANTONY', NULL, NULL, NULL, NULL, 1),
(791, '3474599', 'SANTOS NERI', 'MEJIAS', 'ESCOBAR', 'MEJIAS ESCOBAR SANTOS NERI', NULL, NULL, NULL, NULL, 1),
(792, '41482579', 'WILMER', 'MEJIAS', 'GONZALES', 'MEJIAS GONZALES  WILMER', NULL, NULL, NULL, NULL, 1),
(793, '40111067', 'JOSE LUIS', 'MEJIAS', 'GONZALES', 'MEJIAS GONZALES JOSE LUIS', NULL, NULL, NULL, NULL, 1),
(794, '46019901', 'SEGUNDO LUIS', 'MEJIAS', 'MALDONADO', 'MEJIAS MALDONADO SEGUNDO LUIS', NULL, NULL, NULL, NULL, 1),
(795, '03627348', 'ELAUTERIO', 'MENA', 'ANCAJIMA', 'MENA ANCAJIMA  ELAUTERIO', NULL, NULL, NULL, NULL, 1),
(796, '03569893', 'FRANCISCO', 'MENA', 'ANCAJIMA', 'MENA ANCAJIMA FRANCISCO', NULL, NULL, NULL, NULL, 1),
(797, '03669358', 'VICTOR', 'MENA', 'ANCAJIMA', 'MENA ANCAJIMA VICTOR', NULL, NULL, NULL, NULL, 1),
(798, '41476640', 'JAIME', 'MENA', 'ATOCHE', 'MENA ATOCHE JAIME', NULL, NULL, NULL, NULL, 1),
(799, '43872111', 'JULIO CESAR', 'MENA', 'ATOCHE', 'MENA ATOCHE JULIO CESAR', NULL, NULL, NULL, NULL, 1),
(800, '40478505', 'LUZ MARIA', 'MENA', 'CALDERON', 'MENA CALDERON LUZ MARIA', NULL, NULL, NULL, NULL, 1),
(801, '42105807', 'ELMER', 'MENA', 'CASTRO', 'MENA CASTRO ELMER', NULL, NULL, NULL, NULL, 1),
(802, '03508163', 'JIMENEZ JUANA', 'MENA', 'JIMENEZ', 'MENA DE JIMENEZ JUANA', NULL, NULL, NULL, NULL, 1),
(803, '03624857', 'JULIO', 'MENA', 'FARIAS', 'MENA FARIAS JULIO', NULL, NULL, NULL, NULL, 1),
(804, '03625606', 'CLEMENTINO', 'MENA', 'RODRIGUEZ', 'MENA RODRIGUEZ  CLEMENTINO', NULL, NULL, NULL, NULL, 1),
(805, '03624568', 'CONFELINO', 'MENA', 'RODRIGUEZ', 'MENA RODRIGUEZ  CONFELINO', NULL, NULL, NULL, NULL, 1),
(806, '40241810', 'JULIA', 'MENA', 'RUIZ', 'MENA RUIZ JULIA', NULL, NULL, NULL, NULL, 1),
(807, '03654521', 'JULIAN', 'MENA', 'RUIZ', 'MENA RUIZ JULIAN', NULL, NULL, NULL, NULL, 1),
(808, '42419066', 'MARIANO', 'MENA', 'RUIZ', 'MENA RUIZ MARIANO', NULL, NULL, NULL, NULL, 1),
(809, '41789638', 'BENIGNO', 'MENA', 'URBINA', 'MENA URBINA BENIGNO', NULL, NULL, NULL, NULL, 1),
(810, '02799004', 'MATILDE', 'MENDOZA', 'FLORES', 'MENDOZA FLORES MATILDE', NULL, NULL, NULL, NULL, 1),
(811, '02626048', 'MARCELINO', 'MENDOZA', 'YUCRA', 'MENDOZA YUCRA MARCELINO', NULL, NULL, NULL, NULL, 1),
(812, '40557264', 'JESUS LUCIANO', 'MOGOLLON', 'DURAND', 'MOGOLLON  DURAND JESUS LUCIANO', NULL, NULL, NULL, NULL, 1),
(813, '03603702', 'TEODORO', 'MORE', 'APONTE', 'MORE APONTE TEODORO', NULL, NULL, NULL, NULL, 1),
(814, '03624490', 'JUAN BAUTISTA', 'MORE', 'YOVERA', 'MORE YOVERA JUAN BAUTISTA', NULL, NULL, NULL, NULL, 1),
(815, '03624235', 'JUSTA', 'MORE', 'YOVERA', 'MORE YOVERA JUSTA', NULL, NULL, NULL, NULL, 1),
(816, '19246144', 'PEDRO HEMENEGILDO', 'NAMO', 'VERASTEGUI', 'NAMO VERASTEGUI PEDRO HEMENEGILDO', NULL, NULL, NULL, NULL, 1),
(817, '03626055', 'JUAN MANUEL', 'NOLE', 'ROMERO', 'NOLE ROMERO JUAN MANUEL', NULL, NULL, NULL, NULL, 1),
(818, '03624437', 'LUIS MARINO', 'NOLE', 'ROMERO', 'NOLE ROMERO LUIS MARINO', NULL, NULL, NULL, NULL, 1),
(819, '03496426', 'SEBASTIANA', 'NOLE', 'ROMERO', 'NOLE ROMERO SEBASTIANA', NULL, NULL, NULL, NULL, 1),
(820, '03625303', 'MANUEL', 'OBLEA', 'RUIZ', 'OBLEA RUIZ  MANUEL', NULL, NULL, NULL, NULL, 1),
(821, '03622918', 'FRANCISCO', 'OLIVARES', 'HERRERA', 'OLIVARES HERRERA FRANCISCO', NULL, NULL, NULL, NULL, 1),
(822, '42766683', 'WILLIAM', 'ORDINOLA', 'AGURTO', 'ORDINOLA AGURTO WILLIAM', NULL, NULL, NULL, NULL, 1),
(823, '03496584', 'ELIOPOLDO', 'ORDINOLA', 'ALVARADO', 'ORDINOLA ALVARADO  ELIOPOLDO', NULL, NULL, NULL, NULL, 1),
(824, '03628784', 'ANGEL', 'ORDINOLA', 'ALVARADO', 'ORDINOLA ALVARADO ANGEL', NULL, NULL, NULL, NULL, 1),
(825, '03686830', 'EUSEBIO', 'ORDINOLA', 'ALVARADO', 'ORDINOLA ALVARADO EUSEBIO', NULL, NULL, NULL, NULL, 1),
(826, '41298468', 'FREDDY', 'ORDINOLA', 'ALVARADO', 'ORDINOLA ALVARADO FREDDY', NULL, NULL, NULL, NULL, 1),
(827, '03695669', 'SANTOS LUIS', 'ORDINOLA', 'ALVARADO', 'ORDINOLA ALVARADO SANTOS LUIS', NULL, NULL, NULL, NULL, 1),
(828, '03624251', 'ADRIANO', 'ORDINOLA', 'ATOCHE', 'ORDINOLA ATOCHE  ADRIANO', NULL, NULL, NULL, NULL, 1),
(829, '03625495', 'ALBERTO', 'ORDINOLA', 'ATOCHE', 'ORDINOLA ATOCHE  ALBERTO', NULL, NULL, NULL, NULL, 1),
(830, '41488263', 'GROVER RUFINO', 'ORDINOLA', 'ATOCHE', 'ORDINOLA ATOCHE GROVER RUFINO', NULL, NULL, NULL, NULL, 1),
(831, '03624353', 'JUAN', 'ORDINOLA', 'ATOCHE', 'ORDINOLA ATOCHE JUAN', NULL, NULL, NULL, NULL, 1),
(832, '03624336', 'NICOLAS', 'ORDINOLA', 'ATOCHE', 'ORDINOLA ATOCHE NICOLAS', NULL, NULL, NULL, NULL, 1),
(833, '42144343', 'PERCY ARNALDO', 'ORDINOLA', 'ATOCHE', 'ORDINOLA ATOCHE PERCY ARNALDO', NULL, NULL, NULL, NULL, 1),
(834, '03624316', 'VICTOR', 'ORDINOLA', 'ATOCHE', 'ORDINOLA ATOCHE VICTOR', NULL, NULL, NULL, NULL, 1),
(835, '45052672', 'YARI NOELIA', 'ORDINOLA', 'ATOCHE', 'ORDINOLA ATOCHE YARI NOELIA', NULL, NULL, NULL, NULL, 1),
(836, '03627875', 'EDUARDO', 'ORDINOLA', 'CASTILLO', 'ORDINOLA CASTILLO EDUARDO', NULL, NULL, NULL, NULL, 1),
(837, '03627959', 'INOCENTE', 'ORDINOLA', 'CASTILLO', 'ORDINOLA CASTILLO INOCENTE', NULL, NULL, NULL, NULL, 1),
(838, '03621093', 'JOSEFA', 'ORDINOLA', 'CASTILLO', 'ORDINOLA CASTILLO JOSEFA', NULL, NULL, NULL, NULL, 1),
(839, '03595302', 'JUANA', 'ORDINOLA', 'CASTILLO', 'ORDINOLA CASTILLO JUANA', NULL, NULL, NULL, NULL, 1),
(840, '03647621', 'RICARDO', 'ORDINOLA', 'CASTILLO', 'ORDINOLA CASTILLO RICARDO', NULL, NULL, NULL, NULL, 1),
(841, '40689233', 'ROSNEDO', 'ORDINOLA', 'CASTILLO', 'ORDINOLA CASTILLO ROSNEDO', NULL, NULL, NULL, NULL, 1),
(842, '03625629', 'RUFINO', 'ORDINOLA', 'CASTILLO', 'ORDINOLA CASTILLO RUFINO', NULL, NULL, NULL, NULL, 1),
(843, '41200929', 'MARIA ELENA', 'ORDINOLA', 'CASTRO', 'ORDINOLA CASTRO MARIA ELENA', NULL, NULL, NULL, NULL, 1),
(844, '03691492', 'MARTIN', 'ORDINOLA', 'CLAVIJO', 'ORDINOLA CLAVIJO MARTIN', NULL, NULL, NULL, NULL, 1),
(845, '03625381', 'PEDRO', 'ORDINOLA', 'CRUZ', 'ORDINOLA CRUZ PEDRO', NULL, NULL, NULL, NULL, 1),
(846, '03627086', 'AMANDA', 'ORDINOLA', 'FLOREANO', 'ORDINOLA DE FLOREANO AMANDA', NULL, NULL, NULL, NULL, 1),
(847, '03625156', 'PAULA', 'ORDINOLA', 'GONZALES', 'ORDINOLA DE GONZALES PAULA', NULL, NULL, NULL, NULL, 1),
(848, '03627032', 'JULIA', 'ORDINOLA', 'SAAVEDRA', 'ORDINOLA DE SAAVEDRA JULIA', NULL, NULL, NULL, NULL, 1),
(849, '43338125', 'WILSON', 'ORDINOLA', 'ESPINOZA', 'ORDINOLA ESPINOZA  WILSON', NULL, NULL, NULL, NULL, 1),
(850, '46437901', 'EDINSON PAUL', 'ORDINOLA', 'ESPINOZA', 'ORDINOLA ESPINOZA EDINSON PAUL', NULL, NULL, NULL, NULL, 1),
(851, '47568871', 'ESTEBAN', 'ORDINOLA', 'MARTINEZ', 'ORDINOLA MARTINEZ ESTEBAN', NULL, NULL, NULL, NULL, 1),
(852, '03626121', 'BENITO', 'ORDINOLA', 'PANTA', 'ORDINOLA PANTA BENITO', NULL, NULL, NULL, NULL, 1),
(853, '42307127', 'WILLIAM ALBERTO', 'ORDINOLA', 'RUIZ', 'ORDINOLA RUIZ WILLIAM ALBERTO', NULL, NULL, NULL, NULL, 1),
(854, '40842259', 'FELIX', 'ORDINOLA', 'VALLADARES', 'ORDINOLA VALLADARES FELIX', NULL, NULL, NULL, NULL, 1),
(855, '03676733', 'JAVIER', 'ORDINOLA', 'VALLADARES', 'ORDINOLA VALLADARES JAVIER', NULL, NULL, NULL, NULL, 1),
(856, '03626691', 'JUSTO', 'ORDINOLA', 'VALLADARES', 'ORDINOLA VALLADARES JUSTO', NULL, NULL, NULL, NULL, 1),
(857, '40698095', 'BELTRAN', 'ORDINOLA', 'ZEGARRA', 'ORDINOLA ZEGARRA BELTRAN', NULL, NULL, NULL, NULL, 1),
(858, '80579975', 'BENJAMIN', 'ORDINOLA', 'ZEGARRA', 'ORDINOLA ZEGARRA BENJAMIN', NULL, NULL, NULL, NULL, 1),
(859, '40689232', 'DOMINGO', 'ORDINOLA', 'ZEGARRA', 'ORDINOLA ZEGARRA DOMINGO', NULL, NULL, NULL, NULL, 1),
(860, '03624310', 'PEDRO', 'ORDINOLA', 'ZEGARRA', 'ORDINOLA ZEGARRA PEDRO', NULL, NULL, NULL, NULL, 1),
(861, '03676721', 'SANTOS', 'ORDINOLA', 'ZEGARRA', 'ORDINOLA ZEGARRA SANTOS', NULL, NULL, NULL, NULL, 1),
(862, '40845747', 'DANIEL', 'ORDOÑEZ', 'YOVERA', 'ORDOÑEZ YOVERA DANIEL', NULL, NULL, NULL, NULL, 1),
(863, '45649781', 'VICTOR RAUL', 'ORTIZ', 'ORDOÑEZ', 'ORTIZ ORDOÑEZ VICTOR RAUL', NULL, NULL, NULL, NULL, 1),
(864, '03474583', 'GILBERTO', 'PALOMINO', 'ORDINOLA', 'PALOMINO ORDINOLA GILBERTO', NULL, NULL, NULL, NULL, 1),
(865, '3666761', 'JOSE SANTOS', 'PALOMINO', 'ORDINOLA', 'PALOMINO ORDINOLA JOSE SANTOS', NULL, NULL, NULL, NULL, 1),
(866, '03627963', 'JUAN BAUTISTA', 'PALOMINO', 'ORDINOLA', 'PALOMINO ORDINOLA JUAN BAUTISTA', NULL, NULL, NULL, NULL, 1),
(867, '03880149', 'JOSE ANTONIO', 'PALOMINO', 'SILVA', 'PALOMINO SILVA JOSE ANTONIO', NULL, NULL, NULL, NULL, 1),
(868, '03626848', 'YOJANA', 'PANTA', 'LA MADRID', 'PANTA LA MADRID YOJANA', NULL, NULL, NULL, NULL, 1),
(869, '3653763', 'ISABEL ARCIRA', 'PANTA', 'SOBRINO', 'PANTA SOBRINO ISABEL ARCIRA', NULL, NULL, NULL, NULL, 1),
(870, '', 'APPBOSA', 'PARCELA', 'DEMOSTRATIVA', 'PARCELA DEMOSTRATIVA APPBOSA ', NULL, NULL, NULL, NULL, 1),
(871, '03630353', 'EUSEBIO', 'PEÑA', 'CARRANZA', 'PEÑA CARRANZA EUSEBIO', NULL, NULL, NULL, NULL, 1),
(872, '03630499', 'FAUSTINO', 'PEÑA', 'CARRANZA', 'PEÑA CARRANZA FAUSTINO', NULL, NULL, NULL, NULL, 1),
(873, '03630343', 'REYNALDO', 'PEÑA', 'CARRANZA', 'PEÑA CARRANZA REYNALDO', NULL, NULL, NULL, NULL, 1),
(874, '', 'ESPERANZA NOLBERTA', 'PORRAS', 'ORIHUELA', 'PORRAS ORIHUELA ESPERANZA NOLBERTA', NULL, NULL, NULL, NULL, 1),
(875, '42843574', 'MOISES', 'PUCCIO', 'CASTRO', 'PUCCIO CASTRO MOISES', NULL, NULL, NULL, NULL, 1),
(876, '40614537', 'ROBERTO', 'PUCCIO', 'CASTRO', 'PUCCIO CASTRO ROBERTO', NULL, NULL, NULL, NULL, 1),
(877, '03623770', 'MANUEL GILBERTO', 'QUEVEDO', 'ALBAN', 'QUEVEDO ALBAN MANUEL GILBERTO', NULL, NULL, NULL, NULL, 1),
(878, '03861955', 'CARMEN ROSA', 'QUEVEDO', 'INGA', 'QUEVEDO INGA CARMEN ROSA', NULL, NULL, NULL, NULL, 1),
(879, '40234354', 'BORIS ARTURO', 'RAMIREZ', 'CASTILLO', 'RAMIREZ CASTILLO  BORIS ARTURO', NULL, NULL, NULL, NULL, 1),
(880, '03625956', 'DEYVI', 'RAMOS', 'AGURTO', 'RAMOS AGURTO DEYVI', NULL, NULL, NULL, NULL, 1),
(881, '72845655', 'JOSE MANUEL', 'RAMOS', 'RIVERA', 'RAMOS RIVERA JOSE MANUEL', NULL, NULL, NULL, NULL, 1),
(882, '03695683', 'FROILAN GERARDO', 'RAMOS', 'SERNAQUE', 'RAMOS SERNAQUE FROILAN GERARDO', NULL, NULL, NULL, NULL, 1),
(883, '03671673', 'JOSE DANIEL', 'RAMOS', 'SERNAQUE', 'RAMOS SERNAQUE JOSE DANIEL', NULL, NULL, NULL, NULL, 1),
(884, '03677657', 'SANTOS CRISOSTOMO', 'RAMOS', 'SERNAQUE', 'RAMOS SERNAQUE SANTOS CRISOSTOMO', NULL, NULL, NULL, NULL, 1),
(885, '03623748', 'EPIFANIO', 'REYES', 'CASTRO', 'REYES CASTRO EPIFANIO', NULL, NULL, NULL, NULL, 1),
(886, '03591855', 'JOSE ANTONIO', 'REYES', 'TAVARA', 'REYES TAVARA JOSE ANTONIO', NULL, NULL, NULL, NULL, 1),
(887, '47757514', 'EVELIN FIORELA', 'RIVERA', 'SILVA', 'RIVERA SILVA EVELIN FIORELA', NULL, NULL, NULL, NULL, 1),
(888, '03624643', 'JUAN', 'RODRIGUEZ', 'BENITES', 'RODRIGUEZ BENITES JUAN', NULL, NULL, NULL, NULL, 1),
(889, '03595987', 'JESUS', 'RODRIGUEZ', 'CASTILLO', 'RODRIGUEZ CASTILLO JESUS', NULL, NULL, NULL, NULL, 1),
(890, '03626931', 'MANUEL', 'RODRIGUEZ', 'SOCOLA', 'RODRIGUEZ SOCOLA MANUEL', NULL, NULL, NULL, NULL, 1),
(891, '03628472', 'TOMAS', 'RODRIGUEZ', 'YACILA', 'RODRIGUEZ YACILA TOMAS', NULL, NULL, NULL, NULL, 1),
(892, '03623931', 'SANTOS FRANCISCO', 'ROJAS', 'MORALES', 'ROJAS MORALES SANTOS FRANCISCO', NULL, NULL, NULL, NULL, 1),
(893, '03644396', 'MANUEL', 'ROJAS', 'VILLARREYES', 'ROJAS VILLARREYES  MANUEL', NULL, NULL, NULL, NULL, 1),
(894, '03594202', 'ANITA', 'ROMERO', 'MEJIAS', 'ROMERO MEJIAS  ANITA', NULL, NULL, NULL, NULL, 1),
(895, '03594202', 'MARIBEL', 'ROMERO', 'MEJIAS', 'ROMERO MEJIAS  MARIBEL', NULL, NULL, NULL, NULL, 1),
(896, '03624327', 'TEMISTOCLES', 'ROMERO', 'MEJIAS', 'ROMERO MEJIAS TEMISTOCLES', NULL, NULL, NULL, NULL, 1),
(897, '03627902', 'SERGIO ADAN', 'ROMERO', 'NOLE', 'ROMERO NOLE  SERGIO ADAN', NULL, NULL, NULL, NULL, 1),
(898, '03624304', 'MODESTO', 'ROMERO', 'ORDINOLA', 'ROMERO ORDINOLA MODESTO', NULL, NULL, NULL, NULL, 1),
(899, '80313579', 'LUISA', 'ROSALES', 'PEÑA', 'ROSALES PEÑA LUISA', NULL, NULL, NULL, NULL, 1),
(900, '03628207', 'FLORESMINDA', 'RUIZ', 'AÑAZCO', 'RUIZ AÑAZCO FLORESMINDA', NULL, NULL, NULL, NULL, 1),
(901, '03841531', 'MODESTO', 'RUIZ', 'AÑAZCO', 'RUIZ AÑAZCO MODESTO', NULL, NULL, NULL, NULL, 1),
(902, '03626167', 'ANDREA', 'RUIZ', 'AYALA', 'RUIZ AYALA ANDREA', NULL, NULL, NULL, NULL, 1),
(903, '03622671', 'JUAN FRANCISCO', 'RUIZ', 'CASTILLO', 'RUIZ CASTILLO JUAN FRANCISCO', NULL, NULL, NULL, NULL, 1),
(904, '03623080', 'VALENTIN', 'RUIZ', 'DELGADO', 'RUIZ DELGADO VALENTIN', NULL, NULL, NULL, NULL, 1),
(905, '03626618', 'PABLO', 'RUIZ', 'ESCOBAR', 'RUIZ ESCOBAR PABLO', NULL, NULL, NULL, NULL, 1),
(906, '03666707', 'PAULINA', 'RUIZ', 'ESTRADA', 'RUIZ ESTRADA PAULINA', NULL, NULL, NULL, NULL, 1),
(907, '03626618', 'SEGUNDO GONZALO', 'RUIZ', 'GARCIA', 'RUIZ GARCIA SEGUNDO GONZALO', NULL, NULL, NULL, NULL, 1),
(908, '80618349', 'MARTIN', 'RUIZ', 'PANTA', 'RUIZ PANTA MARTIN', NULL, NULL, NULL, NULL, 1),
(909, '03671704', 'PASCUALA', 'RUIZ', 'RIVERA', 'RUIZ RIVERA  PASCUALA', NULL, NULL, NULL, NULL, 1),
(910, '', 'DOLORES', 'RUIZ', 'RIVERA', 'RUIZ RIVERA DOLORES', NULL, NULL, NULL, NULL, 1),
(911, '43087752', 'EDDY', 'RUIZ', 'RUFINO', 'RUIZ RUFINO EDDY', NULL, NULL, NULL, NULL, 1),
(912, '03624676', 'SEGUNDO', 'SAAVEDRA', 'ALBURQUEQUE', 'SAAVEDRA ALBURQUEQUE SEGUNDO', NULL, NULL, NULL, NULL, 1),
(913, '40629655', 'LEANDRO', 'SAAVEDRA', 'ATOCHE', 'SAAVEDRA ATOCHE  LEANDRO', NULL, NULL, NULL, NULL, 1),
(914, '40639497', 'BRIGIDO', 'SAAVEDRA', 'ATOCHE', 'SAAVEDRA ATOCHE BRIGIDO', NULL, NULL, NULL, NULL, 1),
(915, '03623192', 'VICTOR', 'SAAVEDRA', 'CHERO', 'SAAVEDRA CHERO VICTOR', NULL, NULL, NULL, NULL, 1),
(916, '42534941', 'DARWIN YONAD', 'SAAVEDRA', 'Saman', 'DARWIN YONAD SAAVEDRA ESCOBAR', NULL, NULL, NULL, NULL, 1),
(917, '03684841', 'NEPTALI', 'SAAVEDRA', 'HERRERA', 'SAAVEDRA HERRERA NEPTALI', NULL, NULL, NULL, NULL, 1),
(918, '03593525', 'CRISTOBAL', 'SAAVEDRA', 'LOPEZ', 'SAAVEDRA LOPEZ CRISTOBAL', NULL, NULL, NULL, NULL, 1),
(919, '03627498', 'ALFREDO', 'SAAVEDRA', 'MEJIAS', 'SAAVEDRA MEJIAS  ALFREDO', NULL, NULL, NULL, NULL, 1),
(920, '03624341', 'SANTOS HUMBERTO', 'SAAVEDRA', 'MEJIAS', 'SAAVEDRA MEJIAS  SANTOS HUMBERTO', NULL, NULL, NULL, NULL, 1),
(921, '03623504', 'FRANCISCO', 'SAAVEDRA', 'ROJAS', 'SAAVEDRA ROJAS  FRANCISCO', NULL, NULL, NULL, NULL, 1),
(922, '03627295', 'SEBASTIAN', 'SAAVEDRA', 'ROJAS', 'SAAVEDRA ROJAS SEBASTIAN', NULL, NULL, NULL, NULL, 1),
(923, '03626875', 'MARTIN', 'SAAVEDRA', 'ROSALES', 'SAAVEDRA ROSALES MARTIN', NULL, NULL, NULL, NULL, 1),
(924, '03868359', 'AMABILIA', 'SALDARRIAGA', 'ATOCHE', 'SALDARRIAGA ATOCHE AMABILIA', NULL, NULL, NULL, NULL, 1),
(925, '03691581', 'SEGUNDO', 'SALDARRIAGA', 'MEDINA', 'SALDARRIAGA MEDINA  SEGUNDO', NULL, NULL, NULL, NULL, 1),
(926, '29350212', 'RUDECINDA HILDA', 'SALINAS', 'DE FLORES', 'SALINAS DE FLORES RUDECINDA HILDA', NULL, NULL, NULL, NULL, 1),
(927, '', 'MILTON', 'SANCHEZ', 'GONZALES', 'SANCHEZ GONZALES MILTON', NULL, NULL, NULL, NULL, 1),
(928, '03624639', 'LUCIANO', 'SANDOVAL', 'ALEJOS', 'SANDOVAL ALEJOS LUCIANO', NULL, NULL, NULL, NULL, 1),
(929, '03624654', 'ELIAS', 'SANDOVAL', 'BECERRA', 'SANDOVAL BECERRA ELIAS', NULL, NULL, NULL, NULL, 1),
(930, '03622635', 'JUAN', 'SANDOVAL', 'BECERRA', 'SANDOVAL BECERRA JUAN', NULL, NULL, NULL, NULL, 1),
(931, '03622617', 'DIONICIO', 'SANDOVAL', 'SAAVEDRA', 'SANDOVAL SAAVEDRA DIONICIO', NULL, NULL, NULL, NULL, 1),
(932, '43055998', 'PEDRO', 'SANDOVAL', 'SUAREZ', 'SANDOVAL SUAREZ PEDRO', NULL, NULL, NULL, NULL, 1),
(933, '03626151', 'JULIO', 'SANJINEZ', 'AREAVALO', 'SANJINEZ AREAVALO JULIO', NULL, NULL, NULL, NULL, 1),
(934, '03657392', 'EDILBERTO', 'SILVA', 'ALBAN', 'SILVA ALBAN  EDILBERTO', NULL, NULL, NULL, NULL, 1),
(935, '03671749', 'VICTOR', 'SILVA', 'ALBAN', 'SILVA ALBAN VICTOR', NULL, NULL, NULL, NULL, 1),
(936, '03671782', 'SANTOS GERARDO', 'SILVA', 'CASTRO', 'SILVA CASTRO SANTOS GERARDO', NULL, NULL, NULL, NULL, 1),
(937, '03624355', 'JOSE ANGEL', 'SILVA', 'FARIAS', 'SILVA FARIAS JOSE ANGEL', NULL, NULL, NULL, NULL, 1),
(938, '03682577', 'MARCOS', 'SILVA', 'MORE', 'SILVA MORE MARCOS', NULL, NULL, NULL, NULL, 1),
(939, '03624195', 'JOSE ANTONIO', 'SILVA', 'NIZAMA', 'SILVA NIZAMA JOSE ANTONIO', NULL, NULL, NULL, NULL, 1),
(940, '80663886', 'SANTOS WILLIAM', 'SILVA', 'ORDINOLA', 'SILVA ORDINOLA SANTOS WILLIAM', NULL, NULL, NULL, NULL, 1),
(941, '03676780', 'DAVID', 'SILVA', 'SILVA', 'SILVA SILVA DAVID', NULL, NULL, NULL, NULL, 1),
(942, '17552798', 'WILMER', 'SIME', 'SANTAMARIA', 'SIME SANTAMARIA WILMER', NULL, NULL, NULL, NULL, 1),
(943, '43041460', 'LUIS OSWALDO', 'SIME', 'ZEGARRA', 'SIME ZEGARRA LUIS OSWALDO', NULL, NULL, NULL, NULL, 1),
(944, '03655313', 'ESTEBAN', 'SOCOLA', 'CORREA', 'SOCOLA CORREA ESTEBAN', NULL, NULL, NULL, NULL, 1),
(945, '03623458', 'VICTOR MARINO', 'SOCOLA', 'MEDINA', 'SOCOLA MEDINA  VICTOR MARINO', NULL, NULL, NULL, NULL, 1),
(946, '03623550', 'ERMITAÑO', 'SOCOLA', 'MEDINA', 'SOCOLA MEDINA ERMITAÑO', NULL, NULL, NULL, NULL, 1),
(947, '41424218', 'CESAR VALENTIN', 'SOCOLA', 'PANTA', 'SOCOLA PANTA CESAR VALENTIN', NULL, NULL, NULL, NULL, 1),
(948, '42740367', 'WILMER', 'SUAREZ', 'RAMOS', 'SUAREZ RAMOS WILMER', NULL, NULL, NULL, NULL, 1),
(949, '03627036', 'SANTOS', 'SUAREZ', 'SILVA', 'SUAREZ SILVA SANTOS ', NULL, NULL, NULL, NULL, 1),
(950, '03593361', 'ELIO', 'TALLEDO', 'FLORES', 'TALLEDO FLORES ELIO', NULL, NULL, NULL, NULL, 1),
(951, '03689035', 'MARTIN GUMERCINDO', 'TAVARA', 'MOSCOL', 'TAVARA MOSCOL MARTIN GUMERCINDO', NULL, NULL, NULL, NULL, 1),
(952, '40202327', 'NAYDA GABRIELA', 'TAVARA', 'MOSCOL', 'TAVARA MOSCOL NAYDA GABRIELA', NULL, NULL, NULL, NULL, 1),
(953, '03666725', 'ROMEL JACKSON', 'TAVARA', 'MOSCOL', 'TAVARA MOSCOL ROMEL JACKSON', NULL, NULL, NULL, NULL, 1),
(954, '03624931', 'GUMERCINDO', 'TAVARA', 'ROSALES', 'TAVARA ROSALES GUMERCINDO', NULL, NULL, NULL, NULL, 1),
(955, '02610185', 'SEGUNDO', 'URBINA', 'ANDRADE', 'URBINA ANDRADE  SEGUNDO', NULL, NULL, NULL, NULL, 1),
(956, '03880007', 'ERNESTO', 'URBINA', 'CASTRO', 'URBINA CASTRO ERNESTO', NULL, NULL, NULL, NULL, 1),
(957, '03671661', 'MANUEL', 'URBINA', 'CASTRO', 'URBINA CASTRO MANUEL', NULL, NULL, NULL, NULL, 1),
(958, '03666678', 'MARIO', 'URBINA', 'CASTRO', 'URBINA CASTRO MARIO', NULL, NULL, NULL, NULL, 1),
(959, '03650762', 'MARCOS', 'URBINA', 'ESTRADA', 'URBINA ESTRADA  MARCOS', NULL, NULL, NULL, NULL, 1),
(960, '80313574', 'BENITO', 'URBINA', 'ESTRADA', 'URBINA ESTRADA BENITO', NULL, NULL, NULL, NULL, 1),
(961, '80298644', 'PASCUAL', 'URBINA', 'ESTRADA', 'URBINA ESTRADA PASCUAL', NULL, NULL, NULL, NULL, 1),
(962, '03623956', 'TEODORO', 'URBINA', 'ORDINOLA', 'URBINA ORDINOLA TEODORO', NULL, NULL, NULL, NULL, 1),
(963, '03650762', 'ANGEL MARTIN', 'URBINA', 'RUIZ', 'URBINA RUIZ ANGEL MARTIN', NULL, NULL, NULL, NULL, 1),
(964, '03623467', 'JOSE RICARDO', 'VALDIVIEZO', 'MASA', 'VALDIVIEZO MASA JOSE RICARDO', NULL, NULL, NULL, NULL, 1),
(965, '03624323', 'ELSA', 'VALLADARES', 'AGURTO', 'VALLADARES AGURTO DE ORDINOLA ELSA', NULL, NULL, NULL, NULL, 1),
(966, '03669309', 'ELIZANDRE', 'VALLADARES', 'AGURTO', 'VALLADARES AGURTO ELIZANDRE', NULL, NULL, NULL, NULL, 1),
(967, '44405196', 'PILAR', 'VALLADARES', 'AGURTO', 'VALLADARES AGURTO PILAR', NULL, NULL, NULL, NULL, 1),
(968, '03669312', 'SANTOS', 'VALLADARES', 'AGURTO', 'VALLADARES AGURTO SANTOS', NULL, NULL, NULL, NULL, 1),
(969, '80313051', 'JOSE', 'VALLADARES', 'ATOCHE', 'VALLADARES ATOCHE JOSE', NULL, NULL, NULL, NULL, 1),
(970, '03625603', 'GUMERCINDO', 'VALLADARES', 'CALDERON', 'VALLADARES CALDERON  GUMERCINDO', NULL, NULL, NULL, NULL, 1),
(971, '03689027', 'MARCELINO', 'VALLADARES', 'CASTRO', 'VALLADARES CASTRO MARCELINO', NULL, NULL, NULL, NULL, 1),
(972, '42669433', 'EDER', 'VALLADARES', 'ROMERO', 'VALLADARES ROMERO EDER', NULL, NULL, NULL, NULL, 1),
(973, '03626313', 'EUGENIO', 'VALLADOLID', 'MORE', 'VALLADOLID MORE EUGENIO', NULL, NULL, NULL, NULL, 1),
(974, '03626625', 'SANTOS', 'VALLADOLID', 'MORE', 'VALLADOLID MORE SANTOS', NULL, NULL, NULL, NULL, 1),
(975, '40870135', 'CESAR', 'VALLADOLID', 'SAMPERTEGUI', 'VALLADOLID SAMPERTEGUI CESAR', NULL, NULL, NULL, NULL, 1),
(976, '03496003', 'FAUSTO', 'VALLADOLID', 'SANDOVAL', 'VALLADOLID SANDOVAL FAUSTO', NULL, NULL, NULL, NULL, 1),
(977, '03686913', 'ROMAN', 'VALLADOLID', 'SANDOVAL', 'VALLADOLID SANDOVAL ROMAN', NULL, NULL, NULL, NULL, 1),
(978, '03627807', 'SANTOS TORIBIO', 'VALLADOLID', 'SANDOVAL', 'VALLADOLID SANDOVAL SANTOS TORIBIO', NULL, NULL, NULL, NULL, 1),
(979, '03627834', 'MAXIMINO', 'VALLADOLID', 'VILLEGAS', 'VALLADOLID VILLEGAS MAXIMINO', NULL, NULL, NULL, NULL, 1),
(980, '03626408', 'JUAN', 'VARGAS', 'BENITES', 'VARGAS BENITES  JUAN', NULL, NULL, NULL, NULL, 1);
INSERT INTO `persona` (`id`, `dni`, `nombre`, `apellido_paterno`, `apellido_materno`, `fullname`, `email`, `telefono`, `direccion`, `id_cargo`, `activo`) VALUES
(981, '03671653', 'JUAN AGUSTIN', 'VARGAS', 'ESCOBAR', 'VARGAS ESCOBAR JUAN AGUSTIN', NULL, NULL, NULL, NULL, 1),
(982, '03682421', 'JUAN IGNACIO', 'VASQUEZ', 'ALVARADO', 'VASQUEZ ALVARADO JUAN IGNACIO', NULL, NULL, NULL, NULL, 1),
(983, '03622908', 'MARINO', 'VELASQUEZ', 'HERRERA', 'VELASQUEZ HERRERA MARINO', NULL, NULL, NULL, NULL, 1),
(984, '03689178', 'RAYMUNDO', 'VILLARREAL', 'PURIZACA', 'VILLARREAL PURIZACA  RAYMUNDO ', NULL, NULL, NULL, NULL, 1),
(985, '03833659', 'GENARO WILBERTO', 'VILLARREYES', 'CASTRO', 'VILLARREYES CASTRO  GENARO WILBERTO', NULL, NULL, NULL, NULL, 1),
(986, '03671695', 'RODOLFO', 'VILLARREYES', 'PEÑA', 'VILLARREYES PEÑA RODOLFO', NULL, NULL, NULL, NULL, 1),
(987, '03671694', 'SANTOS GREGORIO', 'VILLARREYES', 'ROJAS', 'VILLARREYES ROJAS SANTOS GREGORIO', NULL, NULL, NULL, NULL, 1),
(988, '03638941', 'SERGIO', 'VILLEGAS', 'RAMOS', 'VILLEGAS RAMOS  SERGIO', NULL, NULL, NULL, NULL, 1),
(989, '03623924', 'MARIA MARTINA', 'VILLEGAS', 'YOVERA', 'VILLEGAS YOVERA MARIA MARTINA', NULL, NULL, NULL, NULL, 1),
(990, '03660240', 'ALEJANDRO', 'YACILA', 'CALDERON', 'YACILA CALDERON  ALEJANDRO', NULL, NULL, NULL, NULL, 1),
(991, '03623973', 'CONCEPCION', 'YACILA', 'CALDERON', 'YACILA CALDERON  CONCEPCION', NULL, NULL, NULL, NULL, 1),
(992, '03628572', 'LUIS', 'YACILA', 'CALDERON', 'YACILA CALDERON  LUIS', NULL, NULL, NULL, NULL, 1),
(993, '3691431', 'CATALINO', 'YACILA', 'CALDERON', 'YACILA CALDERON CATALINO', NULL, NULL, NULL, NULL, 1),
(994, '03623920', 'JESUS', 'YACILA', 'CALDERON', 'YACILA CALDERON JESUS', NULL, NULL, NULL, NULL, 1),
(995, '', 'OLIBORIO', 'YACILA', 'CALDERON', 'YACILA CALDERON OLIBORIO', NULL, NULL, NULL, NULL, 1),
(996, '03627541', 'SEGUNDO', 'YACILA', 'CHIRA', 'YACILA CHIRA  SEGUNDO', NULL, NULL, NULL, NULL, 1),
(997, '80287767', 'FAUSTINA', 'YACILA', 'CHIRA', 'YACILA CHIRA FAUSTINA', NULL, NULL, NULL, NULL, 1),
(998, '80289047', 'JOSEFA', 'YACILA', 'CHIRA', 'YACILA CHIRA JOSEFA', NULL, NULL, NULL, NULL, 1),
(999, '03689012', 'LORENZO', 'YACILA', 'CHIRA', 'YACILA CHIRA LORENZO', NULL, NULL, NULL, NULL, 1),
(1000, '03657306', 'VICTOR', 'YACILA', 'MEJIA', 'YACILA MEJIA VICTOR', NULL, NULL, NULL, NULL, 1),
(1001, '03624344', 'CANDELARIO', 'YACILA', 'MEJIAS', 'YACILA MEJIAS CANDELARIO', NULL, NULL, NULL, NULL, 1),
(1002, '41516676', 'SEBASTIAN', 'YACILA', 'MEJIAS', 'YACILA MEJIAS SEBASTIAN', NULL, NULL, NULL, NULL, 1),
(1003, '03625332', 'ANGEL', 'YACILA', 'PEÑA', 'YACILA PEÑA ANGEL', NULL, NULL, NULL, NULL, 1),
(1004, '03624858', 'SABINO', 'YACILA', 'PEÑA', 'YACILA PEÑA SABINO', NULL, NULL, NULL, NULL, 1),
(1005, '03624391', 'PAULINO', 'YARLEQUE', 'MARCELO', 'YARLEQUE MARCELO PAULINO', NULL, NULL, NULL, NULL, 1),
(1006, '03691412', 'JUAN', 'YARLEQUE', 'SILVA', 'YARLEQUE SILVA JUAN', NULL, NULL, NULL, NULL, 1),
(1007, '02875371', 'VICTOR', 'YOVERA', 'GALVEZ', 'YOVERA GALVEZ  VICTOR', NULL, NULL, NULL, NULL, 1),
(1008, '03628848', 'JULIO', 'YOVERA', 'MORE', 'YOVERA MORE JULIO', NULL, NULL, NULL, NULL, 1),
(1009, '03627354', 'SANTOS SEGUNDO', 'YPANAQUE', 'CHERO', 'YPANAQUE CHERO  SANTOS SEGUNDO', NULL, NULL, NULL, NULL, 1),
(1010, '03628092', 'SANTOS CARMEN', 'YPANAQUE', 'RAMOS', 'YPANAQUE RAMOS SANTOS CARMEN', NULL, NULL, NULL, NULL, 1),
(1011, '03623442', 'AGUSTIN', 'ZAPATA', 'SILVA', 'ZAPATA SILVA AGUSTIN', NULL, NULL, NULL, NULL, 1),
(1012, '03624799', 'JULIO', 'ZEGARRA', 'ABAD', 'ZEGARRA ABAD JULIO', NULL, NULL, NULL, NULL, 1),
(1013, '03669332', 'DANTE', 'ZEGARRA', 'VALDIVIEZO', 'ZEGARRA VALDIVIEZO DANTE', NULL, NULL, NULL, NULL, 1),
(1014, '80313586', 'JORGE LUIS', 'ZEGARRA', 'VALDIVIEZO', 'ZEGARRA VALDIVIEZO JORGE LUIS', NULL, NULL, NULL, NULL, 1),
(1015, '40920120', 'SANTOS WILBERTO', 'ZEGARRA', 'VALDIVIEZO', 'ZEGARRA VALDIVIEZO SANTOS WILBERTO', NULL, NULL, NULL, NULL, 1),
(1016, '03657347', 'MILTON CESAR', 'ZEGARRA', 'VALLADARES', 'ZEGARRA VALLADARES MILTON CESAR', NULL, NULL, NULL, NULL, 1),
(1017, '41981325', 'JOSE ALEXANDER', 'ZEGARRA', 'VILLARREAL', 'ZEGARRA VILLARREAL  JOSE ALEXANDER', NULL, NULL, NULL, NULL, 1),
(1018, '03678454', 'JUAN CARLOS', 'ZEGARRA', 'VILLARREAL', 'ZEGARRA VILLARREAL  JUAN CARLOS', NULL, NULL, NULL, NULL, 1),
(1019, '03669227', 'AURELIO', 'ZEGARRA', 'ZAPATA', 'ZEGARRA ZAPATA AURELIO', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productor`
--

CREATE TABLE `productor` (
  `id` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `socio` tinyint(4) NOT NULL DEFAULT '0',
  `codigo_productor` varchar(255) DEFAULT NULL,
  `codigo_caja` varchar(255) DEFAULT NULL,
  `ggn` double(20,0) DEFAULT NULL,
  `fecha_ingreso` date NOT NULL,
  `fecha_cese` date DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `id_motivo` tinyint(1) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productor`
--

INSERT INTO `productor` (`id`, `id_persona`, `socio`, `codigo_productor`, `codigo_caja`, `ggn`, `fecha_ingreso`, `fecha_cese`, `estado`, `id_motivo`, `activo`) VALUES
(5747, 526, 1, 'APP-SAM-209', '3102', 4050373162000, '0000-00-00', NULL, 1, NULL, 1),
(5748, 527, 1, 'APP-SAM-002', '3101', 4052852620300, '0000-00-00', NULL, 1, NULL, 1),
(5749, 528, 1, 'APP-MON-021', '7111', 4056186425000, '0000-00-00', NULL, 1, NULL, 1),
(5750, 529, 1, 'APP-SAM-451', '3725', 4052852330100, '0000-00-00', NULL, 1, NULL, 1),
(5751, 530, 1, 'APP-SAM-003', '4401', 4050373162000, '0000-00-00', NULL, 1, NULL, 1),
(5752, 531, 1, 'APP-SAM-004', '2802', 4050373162000, '0000-00-00', NULL, 1, NULL, 1),
(5753, 532, 1, 'APP-SAM-007', '403 ', 4050373162100, '0000-00-00', NULL, 1, NULL, 1),
(5754, 533, 1, 'APP-SAM-008', '3404', 4050373162100, '0000-00-00', NULL, 1, NULL, 1),
(5755, 534, 1, 'APP-SAM-006', '3402', 4050373162000, '0000-00-00', NULL, 1, NULL, 1),
(5756, 535, 1, 'APP-SAM-005', '2803', 4050373162100, '0000-00-00', NULL, 1, NULL, 1),
(5757, 536, 1, 'APP-SAM-010', '1706', 4050373162100, '0000-00-00', NULL, 1, NULL, 1),
(5758, 537, 1, 'APP-SAM-324', '2826', 4050373613000, '0000-00-00', NULL, 1, NULL, 1),
(5759, 538, 1, 'APP-SAM-398', '2867', 4052852302200, '0000-00-00', NULL, 1, NULL, 1),
(5760, 539, 1, 'APP-SAM-482', '2903', 4056186862600, '0000-00-00', NULL, 1, NULL, 1),
(5761, 540, 1, 'APP-SAM-434', '3728', 4052852617500, '0000-00-00', NULL, 1, NULL, 1),
(5762, 541, 1, 'APP-SAM-433', '2898', 4056186425100, '0000-00-00', NULL, 1, NULL, 1),
(5763, 542, 1, 'APP-SAM-413', '2882', 4052852618100, '0000-00-00', NULL, 1, NULL, 1),
(5764, 543, 1, 'APP-SAM-462', '2902', 4056186425100, '0000-00-00', NULL, 1, NULL, 1),
(5765, 544, 1, 'APP-SAM-011', '2225', 4052852843600, '0000-00-00', NULL, 1, NULL, 1),
(5766, 545, 1, 'APP-SAM-394', '3808', 4050373872500, '0000-00-00', NULL, 1, NULL, 1),
(5767, 546, 1, 'APP-SAM-012', '2804', 4050373162100, '0000-00-00', NULL, 1, NULL, 1),
(5768, 547, 1, 'APP-MON-027', '7118', 4056186862700, '0000-00-00', NULL, 1, NULL, 1),
(5769, 548, 1, 'APP-INV-Z-019', '0927', 4052852301900, '0000-00-00', NULL, 1, NULL, 1),
(5770, 549, 1, 'APP-SAM-016', '2805', 4050373162100, '0000-00-00', NULL, 1, NULL, 1),
(5771, 550, 1, 'APP-SAM-013', '1201', NULL, '0000-00-00', NULL, 1, NULL, 1),
(5772, 551, 1, 'APP-SAM-123', '2510', 4049928712300, '0000-00-00', NULL, 1, NULL, 1),
(5773, 552, 1, 'APP-SAM-150', '2515', 4056186425100, '0000-00-00', NULL, 1, NULL, 1),
(5774, 553, 1, 'APP-SAM-404', '5330', 4052852302200, '0000-00-00', NULL, 1, NULL, 1),
(5775, 554, 1, 'APP-SAM-014', '3103', 4050373162200, '0000-00-00', NULL, 1, NULL, 1),
(5776, 555, 1, 'APP-SAM-015', '3104', 4050373162200, '0000-00-00', NULL, 1, NULL, 1),
(5777, 556, 1, 'APP-SAM-210', '3844', 4050373162200, '0000-00-00', NULL, 1, NULL, 1),
(5778, 557, 1, 'APP-SAM-017', '3201', 4050373162200, '0000-00-00', NULL, 1, NULL, 1),
(5779, 558, 1, 'APP-MON-028', '7119', 4056186862700, '0000-00-00', NULL, 1, NULL, 1),
(5780, 559, 1, 'APP-VIU-047', '5128', 4052852618200, '0000-00-00', NULL, 1, NULL, 1),
(5781, 560, 1, 'APP-MON-023', '7113', 4056186425000, '0000-00-00', NULL, 1, NULL, 1),
(5782, 561, 1, 'APP-MON-022', '7112', 4056186425000, '0000-00-00', NULL, 1, NULL, 1),
(5783, 562, 1, 'APP-SAM-235', '1601', 4050373162200, '0000-00-00', NULL, 1, NULL, 1),
(5784, 563, 1, 'APP-VIU-037', '5320', 4052852302200, '0000-00-00', NULL, 1, NULL, 1),
(5785, 564, 1, 'APP-SAM-236', '1602', 4050373162200, '0000-00-00', NULL, 1, NULL, 1),
(5786, 565, 1, 'APP-SAM-020', '1302', 4050373162200, '0000-00-00', NULL, 1, NULL, 1),
(5787, 566, 1, 'APP-INV-Z-001', '0910', 4050373613000, '0000-00-00', NULL, 1, NULL, 1),
(5788, 567, 1, 'APP-SAM-479', '2872', 4052852843200, '0000-00-00', NULL, 1, NULL, 1),
(5789, 568, 1, 'APP-SAM-022', '2220', 4050373162300, '0000-00-00', NULL, 1, NULL, 1),
(5790, 569, 1, 'APP-SAM-212', '1604', 4050373162300, '0000-00-00', NULL, 1, NULL, 1),
(5791, 570, 1, 'APP-SAM-446', '2888', 4052852843300, '0000-00-00', NULL, 1, NULL, 1),
(5792, 571, 1, 'APP-SAM-213', '1605', 4050373162300, '0000-00-00', NULL, 1, NULL, 1),
(5793, 572, 1, 'APP-SAM-445', '2887', 4052852843300, '0000-00-00', NULL, 1, NULL, 1),
(5794, 573, 1, 'APP-SAM-444', '2886', 4052852843300, '0000-00-00', NULL, 1, NULL, 1),
(5795, 574, 1, 'APP-SAM-021', '1701', 4056186862600, '0000-00-00', NULL, 1, NULL, 1),
(5796, 575, 1, 'APP-SAM-392', '5321', 4052852302200, '0000-00-00', NULL, 1, NULL, 1),
(5797, 576, 1, 'APP-SAM-023', '2101', 4050373162300, '0000-00-00', NULL, 1, NULL, 1),
(5798, 577, 1, 'APP-SAM-024', '2103', 4050373162300, '0000-00-00', NULL, 1, NULL, 1),
(5799, 578, 1, 'APP-SAM-025', '1606', 4050373162300, '0000-00-00', NULL, 1, NULL, 1),
(5800, 579, 1, 'APP-VIU-056', '5210', 4052852618200, '0000-00-00', NULL, 1, NULL, 1),
(5801, 580, 1, 'APP-SAM-273', '2517', 4050373162300, '0000-00-00', NULL, 1, NULL, 1),
(5802, 581, 1, 'APP-SAM-274', '2518', 4050373162300, '0000-00-00', NULL, 1, NULL, 1),
(5803, 582, 1, 'APP-SAM-256', '2201', 4050373162300, '0000-00-00', NULL, 1, NULL, 1),
(5804, 583, 1, 'APP-SAM-027', '1607', 4050373162400, '0000-00-00', NULL, 1, NULL, 1),
(5805, 584, 1, 'APP-SAM-391', '2863', 4052852302100, '0000-00-00', NULL, 1, NULL, 1),
(5806, 585, 1, 'APP-SAM-026', '1304', 4050373162400, '0000-00-00', NULL, 1, NULL, 1),
(5807, 586, 1, 'APP-SAM-028', '1702', 4050373162400, '0000-00-00', NULL, 1, NULL, 1),
(5808, 587, 1, 'APP-VIU-038', '1821', 4052852302200, '0000-00-00', NULL, 1, NULL, 1),
(5809, 588, 1, 'APP-SAM-205', '1608', 4050373162400, '0000-00-00', NULL, 1, NULL, 1),
(5810, 589, 1, 'APP-SAM-346', '1909', 4050373613000, '0000-00-00', NULL, 1, NULL, 1),
(5811, 590, 1, 'APP-SAM-247', '1902', 4050373162400, '0000-00-00', NULL, 1, NULL, 1),
(5812, 591, 1, 'APP-SAM-483', '5338', 4056186862600, '0000-00-00', NULL, 1, NULL, 1),
(5813, 592, 1, 'APP-TER-031', '7526', 4049929995300, '0000-00-00', NULL, 1, NULL, 1),
(5814, 593, 1, 'APP-SAM-206', '4201', 4050373162400, '0000-00-00', NULL, 1, NULL, 1),
(5815, 594, 1, 'APP-SAM-214', '3203', 4050373163200, '0000-00-00', NULL, 1, NULL, 1),
(5816, 595, 1, 'APP-SAM-033', '3205', 4050373163200, '0000-00-00', NULL, 1, NULL, 1),
(5817, 596, 1, 'APP-SAM-032', '3204', 4050373163200, '0000-00-00', NULL, 1, NULL, 1),
(5818, 597, 1, 'APP-TER-011', '7506', 4052852302300, '0000-00-00', NULL, 1, NULL, 1),
(5819, 598, 1, 'APP-SAM-029', '2202', 4052852843300, '0000-00-00', NULL, 1, NULL, 1),
(5820, 599, 1, 'APP-SAM-034', '3206', 4050373163200, '0000-00-00', NULL, 1, NULL, 1),
(5821, 600, 1, 'APP-SAM-035', '3207', 4050373163200, '0000-00-00', NULL, 1, NULL, 1),
(5822, 601, 1, 'APP-SAM-484', '2905', 4056186862600, '0000-00-00', NULL, 1, NULL, 1),
(5823, 602, 1, 'APP-SAM-358', '1714', 4052852300700, '0000-00-00', NULL, 1, NULL, 1),
(5824, 603, 1, 'APP-SAM-036', '1801', 4050373163600, '0000-00-00', NULL, 1, NULL, 1),
(5825, 604, 1, 'APP-SAM-215', '1703', 4050373163300, '0000-00-00', NULL, 1, NULL, 1),
(5826, 605, 1, 'APP-TER-014', '7511', 4052852843300, '0000-00-00', NULL, 1, NULL, 1),
(5827, 606, 1, 'APP-SAM-038', '1305', 4050373163300, '0000-00-00', NULL, 1, NULL, 1),
(5828, 607, 1, 'APP-SAM-037', '2104', 4050373163300, '0000-00-00', NULL, 1, NULL, 1),
(5829, 608, 1, 'APP-SAM-216', '3209', 4050373163300, '0000-00-00', NULL, 1, NULL, 1),
(5830, 609, 1, 'APP-SAM-041', '3212', 4050373163400, '0000-00-00', NULL, 1, NULL, 1),
(5831, 610, 1, 'APP-SAM-039', '3604', 4050373163300, '0000-00-00', NULL, 1, NULL, 1),
(5832, 611, 1, 'APP-SAM-040', '3405', 4050373163400, '0000-00-00', NULL, 1, NULL, 1),
(5833, 612, 1, 'APP-VIU-035', '1711', 4050373872600, '0000-00-00', NULL, 1, NULL, 1),
(5834, 613, 1, 'APP-SAM-044', '1803', 4050373163500, '0000-00-00', NULL, 1, NULL, 1),
(5835, 614, 1, 'APP-SAM-045', '1804', 4050373163500, '0000-00-00', NULL, 1, NULL, 1),
(5836, 615, 1, 'APP-VIU-001', '1712', 4050373872600, '0000-00-00', NULL, 1, NULL, 1),
(5837, 616, 1, 'APP-VIU-002', '5301', 4050373872600, '0000-00-00', NULL, 1, NULL, 1),
(5838, 617, 1, 'APP-SAM-046', '1805', 4050373163500, '0000-00-00', NULL, 1, NULL, 1),
(5839, 618, 1, 'APP-SAM-237', '1003', 4050373163600, '0000-00-00', NULL, 1, NULL, 1),
(5840, 619, 1, 'APP-SAM-047', '2208', 4050373163600, '0000-00-00', NULL, 1, NULL, 1),
(5841, 620, 1, 'APP-SAM-048', '2219', 4050373163600, '0000-00-00', NULL, 1, NULL, 1),
(5842, 621, 1, 'APP-SAM-049', '3626', 4050373163600, '0000-00-00', NULL, 1, NULL, 1),
(5843, 622, 1, 'APP-SAM-239', '1202', 4056186862900, '0000-00-00', NULL, 1, NULL, 1),
(5844, 623, 1, 'APP-SAM-371', '2857', 4052852302000, '0000-00-00', NULL, 1, NULL, 1),
(5845, 624, 1, 'APP-SAM-373', '2859', 4052852302000, '0000-00-00', NULL, 1, NULL, 1),
(5846, 625, 1, 'APP-SAM-050', '3105', 4050373163600, '0000-00-00', NULL, 1, NULL, 1),
(5847, 626, 1, 'APP-SAM-051', '1102', 4050373163700, '0000-00-00', NULL, 1, NULL, 1),
(5848, 627, 1, 'APP-SAM-248', '3106', 4050373163700, '0000-00-00', NULL, 1, NULL, 1),
(5849, 628, 1, 'APP-SAM-357', '3110', 4050373444400, '0000-00-00', NULL, 1, NULL, 1),
(5850, 629, 1, 'APP-SAM-432', '2881', 4052852302100, '0000-00-00', NULL, 1, NULL, 1),
(5851, 630, 1, 'APP-VIU-027', '5215', 4052852302300, '0000-00-00', NULL, 1, NULL, 1),
(5852, 631, 1, 'APP-SAM-380', '5323', 4052852302100, '0000-00-00', NULL, 1, NULL, 1),
(5853, 632, 1, 'APP-VIU-048', '5332', 4052852618200, '0000-00-00', NULL, 1, NULL, 1),
(5854, 633, 1, 'APP-SAM-056', '3702', 4050373163800, '0000-00-00', NULL, 1, NULL, 1),
(5855, 634, 1, 'APP-SAM-182', '622 ', 4052852620300, '0000-00-00', NULL, 1, NULL, 1),
(5856, 635, 1, 'APP-SAM-054', '2203', 4050373163700, '0000-00-00', NULL, 1, NULL, 1),
(5857, 636, 1, 'APP-SAM-055', '2503', 4050373163800, '0000-00-00', NULL, 1, NULL, 1),
(5858, 637, 1, 'APP-SAM-104', '1405', 4050373167500, '0000-00-00', NULL, 1, NULL, 1),
(5859, 638, 1, 'APP-SAM-470', '2816', 4056186425100, '0000-00-00', NULL, 1, NULL, 1),
(5860, 639, 1, 'APP-SAM-217', '3901', 4050373613000, '0000-00-00', NULL, 1, NULL, 1),
(5861, 640, 1, 'APP-VIU-003', '5104', 4050373613000, '0000-00-00', NULL, 1, NULL, 1),
(5862, 641, 1, 'APP-VIU-004', '5105', 4050373613000, '0000-00-00', NULL, 1, NULL, 1),
(5863, 642, 1, 'APP-SAM-455', '2307', 4052852330100, '0000-00-00', NULL, 1, NULL, 1),
(5864, 643, 1, 'APP-SAM-057', '2301', 4050373163800, '0000-00-00', NULL, 1, NULL, 1),
(5865, 644, 1, 'APP-SAM-436', '3821', 4052852618200, '0000-00-00', NULL, 1, NULL, 1),
(5866, 645, 1, 'APP-SAM-243', '1207', 4056186862900, '0000-00-00', NULL, 1, NULL, 1),
(5867, 646, 1, 'APP-GOL-003', '6004', 4052852618200, '0000-00-00', NULL, 1, NULL, 1),
(5868, 647, 1, 'APP-GOL-004', '6005', 4052852618300, '0000-00-00', NULL, 1, NULL, 1),
(5869, 648, 1, 'APP-GOL-005', '6006', 4052852618300, '0000-00-00', NULL, 1, NULL, 1),
(5870, 649, 1, 'APP-SAM-240', '1006', 4050373163900, '0000-00-00', NULL, 1, NULL, 1),
(5871, 650, 1, 'APP-SAM-061', '3108', 4050373163900, '0000-00-00', NULL, 1, NULL, 1),
(5872, 651, 1, 'APP-SAM-257', '3109', 4050373163900, '0000-00-00', NULL, 1, NULL, 1),
(5873, 652, 1, 'APP-SAM-219', '1306', 4050373163900, '0000-00-00', NULL, 1, NULL, 1),
(5874, 653, 1, 'APP-INV-Z-002', '0919', 4050373613100, '0000-00-00', NULL, 1, NULL, 1),
(5875, 654, 1, 'APP-SAM-058', '1104', 4050373164000, '0000-00-00', NULL, 1, NULL, 1),
(5876, 655, 1, 'APP-SAM-220', '1105', 4050373164000, '0000-00-00', NULL, 1, NULL, 1),
(5877, 656, 1, 'APP-SAM-221', '3214', 4050373164000, '0000-00-00', NULL, 1, NULL, 1),
(5878, 657, 1, 'APP-SAM-059', '3215', 4050373164000, '0000-00-00', NULL, 1, NULL, 1),
(5879, 658, 1, 'APP-SAM-060', '4104', 4050373444400, '0000-00-00', NULL, 1, NULL, 1),
(5880, 659, 1, 'APP-GOL-067', '6058', 4056186862700, '0000-00-00', NULL, 1, NULL, 1),
(5881, 660, 1, 'S/C', 's/c', 4056186424900, '0000-00-00', NULL, 1, NULL, 1),
(5882, 661, 1, 'APP-GOL-007', '6008', 4052852618300, '0000-00-00', NULL, 1, NULL, 1),
(5883, 662, 1, 'APP-GOL-008', '6009', 4052852618300, '0000-00-00', NULL, 1, NULL, 1),
(5884, 663, 1, 'APP-SAM-063', '1309', 4050373164000, '0000-00-00', NULL, 1, NULL, 1),
(5885, 664, 1, 'APP-SAM-207', '1310', 4050373164100, '0000-00-00', NULL, 1, NULL, 1),
(5886, 665, 1, 'APP-SAM-064', '1312', 4052852843400, '0000-00-00', NULL, 1, NULL, 1),
(5887, 666, 1, 'APP-SAM-065', '1313', 4050373164100, '0000-00-00', NULL, 1, NULL, 1),
(5888, 667, 1, 'APP-VIU-019', '5114', 4050373613100, '0000-00-00', NULL, 1, NULL, 1),
(5889, 668, 1, 'APP-SAM-218', '1103', 4052852618300, '0000-00-00', NULL, 1, NULL, 1),
(5890, 669, 1, 'APP-SAM-062', '1106', 4050373164100, '0000-00-00', NULL, 1, NULL, 1),
(5891, 670, 1, 'APP-SAM-222', '1307', 4050373164100, '0000-00-00', NULL, 1, NULL, 1),
(5892, 671, 1, 'APP-SAM-207', '2222', 4052852618300, '0000-00-00', NULL, 1, NULL, 1),
(5893, 672, 1, 'APP-SAM-223', '1403', 4050373164200, '0000-00-00', NULL, 1, NULL, 1),
(5894, 673, 1, 'APP-SAM-287', '1410', 4050373164100, '0000-00-00', NULL, 1, NULL, 1),
(5895, 674, 1, 'APP-SAM-288', '2305', 4050373164200, '0000-00-00', NULL, 1, NULL, 1),
(5896, 675, 1, 'APP-SAM-066', '1402', 4050373164200, '0000-00-00', NULL, 1, NULL, 1),
(5897, 676, 1, 'APP-SAM-361', '2221', 4052852302000, '0000-00-00', NULL, 1, NULL, 1),
(5898, 677, 1, 'APP-SAM-241', '1007', 4050373164200, '0000-00-00', NULL, 1, NULL, 1),
(5899, 678, 1, 'APP-SAM-330', '2828', 4050373613100, '0000-00-00', NULL, 1, NULL, 1),
(5900, 679, 1, 'APP-SAM-242', '1314', 4050373164200, '0000-00-00', NULL, 1, NULL, 1),
(5901, 680, 1, 'APP-SAM-067', '2107', 4050373164000, '0000-00-00', NULL, 1, NULL, 1),
(5902, 681, 1, 'APP-SAM-249', '1704', 4050373167700, '0000-00-00', NULL, 1, NULL, 1),
(5903, 682, 1, 'APP-VIU-006', '5107', 4050373613100, '0000-00-00', NULL, 1, NULL, 1),
(5904, 683, 1, 'APP-SAM-263', '1413', 4050373164200, '0000-00-00', NULL, 1, NULL, 1),
(5905, 684, 1, 'APP-SAM-068', '2505', 4050373163900, '0000-00-00', NULL, 1, NULL, 1),
(5906, 685, 1, 'APP-SAM-363', '1819', 4050373872600, '0000-00-00', NULL, 1, NULL, 1),
(5907, 686, 1, 'APP-VIU-007', '1324', 4050373163800, '0000-00-00', NULL, 1, NULL, 1),
(5908, 687, 1, 'APP-SAM-069', '3605', 4050373167000, '0000-00-00', NULL, 1, NULL, 1),
(5909, 688, 1, 'APP-SAM-442', '3822', 4056186425100, '0000-00-00', NULL, 1, NULL, 1),
(5910, 689, 1, 'APP-SAM-367', '1325', 4052852302000, '0000-00-00', NULL, 1, NULL, 1),
(5911, 690, 1, 'APP-SAM-364', '4419', 4050373872600, '0000-00-00', NULL, 1, NULL, 1),
(5912, 691, 1, 'APP-SAM-070', '1903', 4050373167000, '0000-00-00', NULL, 1, NULL, 1),
(5913, 692, 1, 'APP-SAM-071', '2302', 4050373167000, '0000-00-00', NULL, 1, NULL, 1),
(5914, 693, 1, 'APP-VIU-008', '5201', 4050373613100, '0000-00-00', NULL, 1, NULL, 1),
(5915, 694, 1, 'APP-VIU-009', '5204', 4050373613100, '0000-00-00', NULL, 1, NULL, 1),
(5916, 695, 1, 'APP-VIU-020', '5115', 4050373613100, '0000-00-00', NULL, 1, NULL, 1),
(5917, 696, 1, 'APP-VIU-010', '5109', 4050373613100, '0000-00-00', NULL, 1, NULL, 1),
(5918, 697, 1, 'APP-VIU-021', '5207', 4050373613100, '0000-00-00', NULL, 1, NULL, 1),
(5919, 698, 1, 'APP-SAM-072', '1910', 4050373167000, '0000-00-00', NULL, 1, NULL, 1),
(5920, 699, 1, 'APP-SAM-073', '1911', 4052852957100, '0000-00-00', NULL, 1, NULL, 1),
(5921, 700, 1, 'APP-SAM-224', '1904', 4050373167000, '0000-00-00', NULL, 1, NULL, 1),
(5922, 701, 1, 'APP-SAM-228', '2504', 4056186862900, '0000-00-00', NULL, 1, NULL, 1),
(5923, 702, 1, 'APP-SAM-370', '2226', 4052852618400, '0000-00-00', NULL, 1, NULL, 1),
(5924, 703, 1, 'APP-SAM-076', '1808', 4050373167100, '0000-00-00', NULL, 1, NULL, 1),
(5925, 704, 1, 'APP-SAM-077', '2506', 4050373167100, '0000-00-00', NULL, 1, NULL, 1),
(5926, 705, 1, 'APP-SAM-075', '4222', 4050373167100, '0000-00-00', NULL, 1, NULL, 1),
(5927, 706, 1, 'APP-SAM-078', '2108', 4050373167100, '0000-00-00', NULL, 1, NULL, 1),
(5928, 707, 1, 'APP-SAM-354', '4215', 4050373444400, '0000-00-00', NULL, 1, NULL, 1),
(5929, 708, 1, 'APP-SAM-079', '2235', 4050373167100, '0000-00-00', NULL, 1, NULL, 1),
(5930, 709, 1, 'APP-SAM-080', '2109', 4050373167100, '0000-00-00', NULL, 1, NULL, 1),
(5931, 710, 1, 'APP-SAM-225', '4507', 4050373613100, '0000-00-00', NULL, 1, NULL, 1),
(5932, 711, 1, 'APP-SAM-081', '3606', 4050373167100, '0000-00-00', NULL, 1, NULL, 1),
(5933, 712, 1, 'APP-SAM-087', '3008', 4050373167200, '0000-00-00', NULL, 1, NULL, 1),
(5934, 713, 1, 'APP-SAM-082', '4413', 4050373167200, '0000-00-00', NULL, 1, NULL, 1),
(5935, 714, 1, 'APP-SAM-083', '3607', 4050373167200, '0000-00-00', NULL, 1, NULL, 1),
(5936, 715, 1, 'APP-SAM-084', '3608', 4050373167200, '0000-00-00', NULL, 1, NULL, 1),
(5937, 716, 1, 'APP-SAM-085', '4414', 4050373167200, '0000-00-00', NULL, 1, NULL, 1),
(5938, 717, 1, 'APP-SAM-086', '3609', 4050373167200, '0000-00-00', NULL, 1, NULL, 1),
(5939, 718, 1, 'APP-SAM-088', '3009', 4050373167200, '0000-00-00', NULL, 1, NULL, 1),
(5940, 719, 1, 'APP-SAM-226', '3703', 4050373167200, '0000-00-00', NULL, 1, NULL, 1),
(5941, 720, 1, 'APP-SAM-089', '3010', 4050373167200, '0000-00-00', NULL, 1, NULL, 1),
(5942, 721, 1, 'APP-GOL-068', '6051', 4056186862700, '0000-00-00', NULL, 1, NULL, 1),
(5943, 722, 1, 'APP-GOL-05', 's/c', 4056186424900, '0000-00-00', NULL, 1, NULL, 1),
(5944, 723, 1, 'APP-GOL-061', 's/c', 4056186424900, '0000-00-00', NULL, 1, NULL, 1),
(5945, 724, 1, 'APP-GOL-012', '6015', 4052852618400, '0000-00-00', NULL, 1, NULL, 1),
(5946, 725, 1, 'APP-MON-004', '7001', 4050373167200, '0000-00-00', NULL, 1, NULL, 1),
(5947, 726, 1, 'APP-SAM-481', '1016', 4056186862800, '0000-00-00', NULL, 1, NULL, 1),
(5948, 727, 1, 'APP-GOL-072', '6054', 4056186862700, '0000-00-00', NULL, 1, NULL, 1),
(5949, 728, 1, 'APP-SAM-090', '4006', 4050373168700, '0000-00-00', NULL, 1, NULL, 1),
(5950, 729, 1, 'APP-SAM-294', '2216', 4050373167300, '0000-00-00', NULL, 1, NULL, 1),
(5951, 730, 1, 'APP-VIU-055', '5216', 4052852618400, '0000-00-00', NULL, 1, NULL, 1),
(5952, 731, 1, 'APP-INV-Z-017', '0926', 4052852618400, '0000-00-00', NULL, 1, NULL, 1),
(5953, 732, 1, 'APP-SAM-397', '3234', 4052852618900, '0000-00-00', NULL, 1, NULL, 1),
(5954, 733, 1, 'APP-SAM-091', '1315', 4050373168700, '0000-00-00', NULL, 1, NULL, 1),
(5955, 734, 1, 'APP-SAM-092', '3704', 4050373168700, '0000-00-00', NULL, 1, NULL, 1),
(5956, 735, 1, 'APP-SAM-093', '1316', 4050373168700, '0000-00-00', NULL, 1, NULL, 1),
(5957, 736, 1, 'APP-SAM-211', '1301', 4050373162200, '0000-00-00', NULL, 1, NULL, 1),
(5958, 737, 1, 'APP-SAM-258', '3705', 4050373168700, '0000-00-00', NULL, 1, NULL, 1),
(5959, 738, 1, 'APP-VIU-022', '5116', 4050373613200, '0000-00-00', NULL, 1, NULL, 1),
(5960, 739, 1, 'APP-SAM-296', '4411', 4050373168700, '0000-00-00', NULL, 1, NULL, 1),
(5961, 740, 1, 'APP-VIU-028', '5120', 4052852302300, '0000-00-00', NULL, 1, NULL, 1),
(5962, 741, 1, 'APP-VIU-016', '5110', 4050373613200, '0000-00-00', NULL, 1, NULL, 1),
(5963, 742, 1, 'APP-SAM-095', '1811', 4050373168700, '0000-00-00', NULL, 1, NULL, 1),
(5964, 743, 1, 'APP-SAM-094', '3610', 4050373168700, '0000-00-00', NULL, 1, NULL, 1),
(5965, 744, 1, 'APP-SAM-097', '4106', 4050373444400, '0000-00-00', NULL, 1, NULL, 1),
(5966, 745, 1, 'APP-MON-005', '7301', 4050373168800, '0000-00-00', NULL, 1, NULL, 1),
(5967, 746, 1, 'APP-SAM-098', '4107', 4050373168800, '0000-00-00', NULL, 1, NULL, 1),
(5968, 747, 1, 'APP-INV-Z-015', '0922', 4052852301900, '0000-00-00', NULL, 1, NULL, 1),
(5969, 748, 1, 'APP-GOL-045', '6040', 4052852843400, '0000-00-00', NULL, 1, NULL, 1),
(5970, 749, 1, 'APP-SAM-099', '3706', 4050373168800, '0000-00-00', NULL, 1, NULL, 1),
(5971, 750, 1, 'APP-SAM-100', '3611', 4050373168800, '0000-00-00', NULL, 1, NULL, 1),
(5972, 751, 1, 'APP-SAM-466', '3841', 4056186425100, '0000-00-00', NULL, 1, NULL, 1),
(5973, 752, 1, 'APP-SAM-208', '1912', 4050373168900, '0000-00-00', NULL, 1, NULL, 1),
(5974, 753, 1, 'APP-VIU-060', '5218', 4052852952400, '0000-00-00', NULL, 1, NULL, 1),
(5975, 754, 1, 'APP-SAM-147', '2218', 4056186863000, '0000-00-00', NULL, 1, NULL, 1),
(5976, 755, 1, 'APP-TER-015', '7531', 4052852843400, '0000-00-00', NULL, 1, NULL, 1),
(5977, 756, 1, 'APP-TER-013', '7529', 4052852618500, '0000-00-00', NULL, 1, NULL, 1),
(5978, 757, 1, 'APP-VIU-011', '5136', 4050373613200, '0000-00-00', NULL, 1, NULL, 1),
(5979, 758, 1, 'APP-SAM-101', '3707', 4050373168900, '0000-00-00', NULL, 1, NULL, 1),
(5980, 759, 1, 'APP-SAM-102', '4108', 4050373168900, '0000-00-00', NULL, 1, NULL, 1),
(5981, 760, 1, 'APP-MON-019', '108 ', 4052852952500, '0000-00-00', NULL, 1, NULL, 1),
(5982, 761, 1, 'APP-MON-030', '7123', 4056186862800, '0000-00-00', NULL, 1, NULL, 1),
(5983, 762, 1, 'APP-MON-008', '7302', 4050373168900, '0000-00-00', NULL, 1, NULL, 1),
(5984, 763, 1, 'APP-SAM-103', '1404', 4050373173100, '0000-00-00', NULL, 1, NULL, 1),
(5985, 764, 1, 'APP-SAM-227', '1009', 4050373167500, '0000-00-00', NULL, 1, NULL, 1),
(5986, 765, 1, 'APP-SAM-298', '2895', 4052852957100, '0000-00-00', NULL, 1, NULL, 1),
(5987, 766, 1, 'APP-SAM-105', '2807', 4050373167500, '0000-00-00', NULL, 1, NULL, 1),
(5988, 767, 1, 'APP-TER-016', '7513', 4052852843400, '0000-00-00', NULL, 1, NULL, 1),
(5989, 768, 1, 'APP-SAM-009', '3730', 4050373167500, '0000-00-00', NULL, 1, NULL, 1),
(5990, 769, 1, 'APP-SAM-106', '2236', 4050373167500, '0000-00-00', NULL, 1, NULL, 1),
(5991, 770, 1, 'APP-SAM-107', '3708', 4050373167500, '0000-00-00', NULL, 1, NULL, 1),
(5992, 771, 1, 'APP-GOL-070', '6052', 4056186862700, '0000-00-00', NULL, 1, NULL, 1),
(5993, 772, 1, 'APP-VIU-063', '2522', 4052852618500, '0000-00-00', NULL, 1, NULL, 1),
(5994, 773, 1, 'APP-SAM-108', '5324', 4050373167500, '0000-00-00', NULL, 1, NULL, 1),
(5995, 774, 1, 'APP-INV-Z-016', '0929', 4052852957100, '0000-00-00', NULL, 1, NULL, 1),
(5996, 775, 1, 'APP-INV-Z-003', '0912', 4050373613200, '0000-00-00', NULL, 1, NULL, 1),
(5997, 776, 1, 'APP-VIU-049', '4404', 4052852618500, '0000-00-00', NULL, 1, NULL, 1),
(5998, 777, 1, 'APP-SAM-109', '3709', 4050373167600, '0000-00-00', NULL, 1, NULL, 1),
(5999, 778, 1, 'APP-SAM-204', '4407', 4052852843500, '0000-00-00', NULL, 1, NULL, 1),
(6000, 779, 1, 'APP-SAM-491', '4221', 4056186862800, '0000-00-00', NULL, 1, NULL, 1),
(6001, 780, 1, 'APP-SAM-110', '2808', 4050373167600, '0000-00-00', NULL, 1, NULL, 1),
(6002, 781, 1, 'APP-MON-002', '7202', 4050373167600, '0000-00-00', NULL, 1, NULL, 1),
(6003, 782, 1, 'APP-TER-018', '7516', 4052852843500, '0000-00-00', NULL, 1, NULL, 1),
(6004, 783, 1, 'APP-SAM-115', '1318', 4050373167600, '0000-00-00', NULL, 1, NULL, 1),
(6005, 784, 1, 'APP-SAM-114', '1317', 4056186425200, '0000-00-00', NULL, 1, NULL, 1),
(6006, 785, 1, 'APP-SAM-472', '2825', 4056186425200, '0000-00-00', NULL, 1, NULL, 1),
(6007, 786, 1, 'APP-SAM-111', '3406', 4050373167600, '0000-00-00', NULL, 1, NULL, 1),
(6008, 787, 1, 'APP-SAM-112', '3724', 4050373167600, '0000-00-00', NULL, 1, NULL, 1),
(6009, 788, 1, 'APP-NOR-012', '3836', 4056186425100, '0000-00-00', NULL, 1, NULL, 1),
(6010, 789, 1, 'APP-SAM-116', '2210', 4050373167600, '0000-00-00', NULL, 1, NULL, 1),
(6011, 790, 1, 'APP-SAM-471', '3845', 4056186425200, '0000-00-00', NULL, 1, NULL, 1),
(6012, 791, 1, 'APP-SAM-117', '2211', 4050373167700, '0000-00-00', NULL, 1, NULL, 1),
(6013, 792, 1, 'APP-VIU-029', '5303', 4050373613200, '0000-00-00', NULL, 1, NULL, 1),
(6014, 793, 1, 'APP-SAM-118', '3710', 4050373167700, '0000-00-00', NULL, 1, NULL, 1),
(6015, 794, 1, 'APP-SAM-382', '3838', 4052852302100, '0000-00-00', NULL, 1, NULL, 1),
(6016, 795, 1, 'APP-SAM-120', '1205', 4056186862900, '0000-00-00', NULL, 1, NULL, 1),
(6017, 796, 1, 'APP-SAM-119', '1320', 4050373167700, '0000-00-00', NULL, 1, NULL, 1),
(6018, 797, 1, 'APP-SAM-121', '2217', 4050373167700, '0000-00-00', NULL, 1, NULL, 1),
(6019, 798, 1, 'APP-SAM-369', '2232', 4052852295700, '0000-00-00', NULL, 1, NULL, 1),
(6020, 799, 1, 'APP-SAM-412', '5333', 4052852302300, '0000-00-00', NULL, 1, NULL, 1),
(6021, 800, 1, 'APP-GOL-062', '6048', 4056186424900, '0000-00-00', NULL, 1, NULL, 1),
(6022, 801, 1, 'APP-SAM-424', '2227', 4052852618500, '0000-00-00', NULL, 1, NULL, 1),
(6023, 802, 1, 'APP-SAM-485', '5407', 4056186862600, '0000-00-00', NULL, 1, NULL, 1),
(6024, 803, 1, 'APP-SAM-122', '1905', 4050373167700, '0000-00-00', NULL, 1, NULL, 1),
(6025, 804, 1, 'APP-SAM-124', '2233', 4050373167700, '0000-00-00', NULL, 1, NULL, 1),
(6026, 805, 1, 'APP-SAM-125', '2212', 4050373167700, '0000-00-00', NULL, 1, NULL, 1),
(6027, 806, 1, 'APP-SAM-486', '2408', 4056186862600, '0000-00-00', NULL, 1, NULL, 1),
(6028, 807, 1, 'APP-SAM-127', '2111', 4050373167400, '0000-00-00', NULL, 1, NULL, 1),
(6029, 808, 1, 'APP-SAM-492', '2238', 4056186862900, '0000-00-00', NULL, 1, NULL, 1),
(6030, 809, 1, 'APP-VIU-013', '5217', 4052852302300, '0000-00-00', NULL, 1, NULL, 1),
(6031, 810, 1, 'APP-GOL-019', '6026', 4056186862900, '0000-00-00', NULL, 1, NULL, 1),
(6032, 811, 1, 'APP-TER-019', '7532', 4052852843500, '0000-00-00', NULL, 1, NULL, 1),
(6033, 812, 1, 'APP-INV-Z-004', '2507', 4050373613200, '0000-00-00', NULL, 1, NULL, 1),
(6034, 813, 1, 'APP-VIU-051', '5406', 4052852618900, '0000-00-00', NULL, 1, NULL, 1),
(6035, 814, 1, 'APP-SAM-353', '1218', 4050373613200, '0000-00-00', NULL, 1, NULL, 1),
(6036, 815, 1, 'APP-SAM-043', '1802', 4050373163500, '0000-00-00', NULL, 1, NULL, 1),
(6037, 816, 1, 'APP-SAM-128', '3219', 4050373167400, '0000-00-00', NULL, 1, NULL, 1),
(6038, 817, 1, 'APP-VIU-045', '1914', 4052852302300, '0000-00-00', NULL, 1, NULL, 1),
(6039, 818, 1, 'APP-VIU-059', '5132', 4052852302200, '0000-00-00', NULL, 1, NULL, 1),
(6040, 819, 1, 'APP-VIU-024', '5118', 4050373613200, '0000-00-00', NULL, 1, NULL, 1),
(6041, 820, 1, 'APP-SAM-129', '3112', 4050373167400, '0000-00-00', NULL, 1, NULL, 1),
(6042, 821, 1, 'APP-INV-Z-005', '0916', 4050373613200, '0000-00-00', NULL, 1, NULL, 1),
(6043, 822, 1, 'APP-SAM-435', '3819', 4052852618600, '0000-00-00', NULL, 1, NULL, 1),
(6044, 823, 1, 'APP-SAM-334', '2831', 4050373613500, '0000-00-00', NULL, 1, NULL, 1),
(6045, 824, 1, 'APP-SAM-245', '2908', 4050373167400, '0000-00-00', NULL, 1, NULL, 1),
(6046, 825, 1, 'APP-NOR-008', '3832', 4052852674000, '0000-00-00', NULL, 1, NULL, 1),
(6047, 826, 1, 'APP-NOR-009', '3833', 4056186425000, '0000-00-00', NULL, 1, NULL, 1),
(6048, 827, 1, 'APP-SAM-408', '3846', 4052852302200, '0000-00-00', NULL, 1, NULL, 1),
(6049, 828, 1, 'APP-SAM-246', '3801', 4050373613200, '0000-00-00', NULL, 1, NULL, 1),
(6050, 829, 1, 'APP-SAM-130', '3902', 4050373613300, '0000-00-00', NULL, 1, NULL, 1),
(6051, 830, 1, 'APP-SAM-458', '2873', 4052852300700, '0000-00-00', NULL, 1, NULL, 1),
(6052, 831, 1, 'APP-SAM-131', '4402', 4050373167400, '0000-00-00', NULL, 1, NULL, 1),
(6053, 832, 1, 'APP-SAM-133', '3803', 4050373613300, '0000-00-00', NULL, 1, NULL, 1),
(6054, 833, 1, 'APP-SAM-459', '2874', 4052852300700, '0000-00-00', NULL, 1, NULL, 1),
(6055, 834, 1, 'APP-SAM-132', '3729', 4050373167400, '0000-00-00', NULL, 1, NULL, 1),
(6056, 835, 1, 'APP-SAM-460', '2875', 4052852302100, '0000-00-00', NULL, 1, NULL, 1),
(6057, 836, 1, 'APP-SAM-134', '2811', 4050373613300, '0000-00-00', NULL, 1, NULL, 1),
(6058, 837, 1, 'APP-SAM-389', '2871', 4052852302100, '0000-00-00', NULL, 1, NULL, 1),
(6059, 838, 1, 'APP-SAM-135', '3711', 4050373167400, '0000-00-00', NULL, 1, NULL, 1),
(6060, 839, 1, 'APP-SAM-113', '2845', 4050373167600, '0000-00-00', NULL, 1, NULL, 1),
(6061, 840, 1, 'APP-SAM-387', '2868', 4052852302100, '0000-00-00', NULL, 1, NULL, 1),
(6062, 841, 1, 'APP-SAM-374', '3905', 4052852618600, '0000-00-00', NULL, 1, NULL, 1),
(6063, 842, 1, 'APP-SAM-390', '2879', 4052852843600, '0000-00-00', NULL, 1, NULL, 1),
(6064, 843, 1, 'APP-SAM-492', '4007', 4056186862600, '0000-00-00', NULL, 1, NULL, 1),
(6065, 844, 1, 'APP-SAM-137', '3712', 4050373167500, '0000-00-00', NULL, 1, NULL, 1),
(6066, 845, 1, 'APP-SAM-138', '4002', 4050373167500, '0000-00-00', NULL, 1, NULL, 1),
(6067, 846, 1, 'APP-GOL-063', '6049', 4056186425000, '0000-00-00', NULL, 1, NULL, 1),
(6068, 847, 1, 'APP-SAM-266', '2822', 4050373167500, '0000-00-00', NULL, 1, NULL, 1),
(6069, 848, 1, 'APP-SAM-351', '2848', 4050373613300, '0000-00-00', NULL, 1, NULL, 1),
(6070, 849, 1, 'APP-SAM-139', '2813', 4050373168800, '0000-00-00', NULL, 1, NULL, 1),
(6071, 850, 1, 'APP-SAM-465', '3840', 4056186425100, '0000-00-00', NULL, 1, NULL, 1),
(6072, 851, 1, 'APP-SAM-476', '2841', 4056186425200, '0000-00-00', NULL, 1, NULL, 1),
(6073, 852, 1, 'APP-SAM-140', '3614', 4050373168800, '0000-00-00', NULL, 1, NULL, 1),
(6074, 853, 1, 'APP-SAM-480', '3238', 4056186862600, '0000-00-00', NULL, 1, NULL, 1),
(6075, 854, 1, 'APP-SAM-143', '2817', 4050373168800, '0000-00-00', NULL, 1, NULL, 1),
(6076, 855, 1, 'APP-SAM-142', '2815', 4050373168900, '0000-00-00', NULL, 1, NULL, 1),
(6077, 856, 1, 'APP-SAM-488', '4103', 4056186862700, '0000-00-00', NULL, 1, NULL, 1),
(6078, 857, 1, 'APP-SAM-478', '3827', 4052852300700, '0000-00-00', NULL, 1, NULL, 1),
(6079, 858, 1, 'APP-SAM-365', '4418', 4050373872600, '0000-00-00', NULL, 1, NULL, 1),
(6080, 859, 1, 'APP-SAM-338', '4417', 4050373444500, '0000-00-00', NULL, 1, NULL, 1),
(6081, 860, 1, 'APP-SAM-414', '3815', 4052852618600, '0000-00-00', NULL, 1, NULL, 1),
(6082, 861, 1, 'APP-SAM-337', '4416', 4050373444500, '0000-00-00', NULL, 1, NULL, 1),
(6083, 862, 1, 'APP-TER-022', '7518', 4052852843500, '0000-00-00', NULL, 1, NULL, 1),
(6084, 863, 1, 'APP-TER-023', '7533', 4052852843500, '0000-00-00', NULL, 1, NULL, 1),
(6085, 864, 1, 'APP-SAM-251', '4422', 4050373168900, '0000-00-00', NULL, 1, NULL, 1),
(6086, 865, 1, 'APP-SAM-144', '4421', 4050373169000, '0000-00-00', NULL, 1, NULL, 1),
(6087, 866, 1, 'APP-SAM-145', '4109', 4056186862900, '0000-00-00', NULL, 1, NULL, 1),
(6088, 867, 1, 'APP-TER-024', '7519', 4052852843600, '0000-00-00', NULL, 1, NULL, 1),
(6089, 868, 1, 'APP-SAM-146', '3221', 4050373169000, '0000-00-00', NULL, 1, NULL, 1),
(6090, 869, 1, 'APP-SAM-052', '3107', 4052852620300, '0000-00-00', NULL, 1, NULL, 1),
(6091, 870, 1, 'S/C', '', NULL, '0000-00-00', NULL, 1, NULL, 1),
(6092, 871, 1, 'APP-MON-031', '7120', 4056186862800, '0000-00-00', NULL, 1, NULL, 1),
(6093, 872, 1, 'APP-MON-032', '7121', 4056186862800, '0000-00-00', NULL, 1, NULL, 1),
(6094, 873, 1, 'APP-MON-033', '7122', 4056186862800, '0000-00-00', NULL, 1, NULL, 1),
(6095, 874, 1, 'APP-GOL-040', '6038', 4056186862800, '0000-00-00', NULL, 1, NULL, 1),
(6096, 875, 1, 'APP-SAM-418', '3818', 4052852618600, '0000-00-00', NULL, 1, NULL, 1),
(6097, 876, 1, 'APP-SAM-437', '3820', 4052852618600, '0000-00-00', NULL, 1, NULL, 1),
(6098, 877, 1, 'APP-SAM-148', '4203', 4050373169000, '0000-00-00', NULL, 1, NULL, 1),
(6099, 878, 1, 'APP-SAM-160', '4204', 4050373169000, '0000-00-00', NULL, 1, NULL, 1),
(6100, 879, 1, 'APP-SAM-149', '1010', 4050373169100, '0000-00-00', NULL, 1, NULL, 1),
(6101, 880, 1, 'APP-SAM-252', '2819', 4050373169200, '0000-00-00', NULL, 1, NULL, 1),
(6102, 881, 1, 'APP-SAM-477', '2812', 4056186425200, '0000-00-00', NULL, 1, NULL, 1),
(6103, 882, 1, 'APP-GOL-017', '6024', 4052852618700, '0000-00-00', NULL, 1, NULL, 1),
(6104, 883, 1, 'APP-GOL-018', '6056', 4052852618700, '0000-00-00', NULL, 1, NULL, 1),
(6105, 884, 1, 'APP-GOL-020', '6027', 4052852618700, '0000-00-00', NULL, 1, NULL, 1),
(6106, 885, 1, 'APP-INV-Z-006', '0913', 4050373613300, '0000-00-00', NULL, 1, NULL, 1),
(6107, 886, 1, 'APP-SAM-415', '3814', 4052852618700, '0000-00-00', NULL, 1, NULL, 1),
(6108, 887, 1, 'APP-SAM-457', '3829', 4052852952500, '0000-00-00', NULL, 1, NULL, 1),
(6109, 888, 1, 'APP-MON-018', '7107', 4056186425000, '0000-00-00', NULL, 1, NULL, 1),
(6110, 889, 1, 'APP-VIU-015', '5304', 4050373613300, '0000-00-00', NULL, 1, NULL, 1),
(6111, 890, 1, 'APP-SAM-151', '1907', 4050373169200, '0000-00-00', NULL, 1, NULL, 1),
(6112, 891, 1, 'APP-MON-024', '7114', 4056186425000, '0000-00-00', NULL, 1, NULL, 1),
(6113, 892, 1, 'APP-SAM-152', '1107', 4050373169100, '0000-00-00', NULL, 1, NULL, 1),
(6114, 893, 1, 'APP-SAM-153', '1011', 4050373169100, '0000-00-00', NULL, 1, NULL, 1),
(6115, 894, 1, 'APP-SAM-352', '2837', 4050373613300, '0000-00-00', NULL, 1, NULL, 1),
(6116, 895, 1, 'APP-SAM-340', '2838', 4050373613300, '0000-00-00', NULL, 1, NULL, 1),
(6117, 896, 1, 'APP-SAM-155', '2820', 4050373169100, '0000-00-00', NULL, 1, NULL, 1),
(6118, 897, 1, 'APP-SAM-341', '3232', 4050373444400, '0000-00-00', NULL, 1, NULL, 1),
(6119, 898, 1, 'APP-SAM-156', '4004', 4050373169200, '0000-00-00', NULL, 1, NULL, 1),
(6120, 899, 1, 'APP-SAM-074', '4412', 4056186862900, '0000-00-00', NULL, 1, NULL, 1),
(6121, 900, 1, 'VDA 5211', '5227', 4052852618100, '0000-00-00', NULL, 1, NULL, 1),
(6122, 901, 1, 'APP-VIU-050', '5211', 4052852618900, '0000-00-00', NULL, 1, NULL, 1),
(6123, 902, 1, 'APP-SAM-157', '2821', 4050373168900, '0000-00-00', NULL, 1, NULL, 1),
(6124, 903, 1, 'APP-TER-029', '7527', 4049929311000, '0000-00-00', NULL, 1, NULL, 1),
(6125, 904, 1, 'APP-SAM-159', '2223', 4050373169100, '0000-00-00', NULL, 1, NULL, 1),
(6126, 905, 1, 'APP-SAM-186', '3223', 4050373164100, '0000-00-00', NULL, 1, NULL, 1),
(6127, 906, 1, 'APP-SAM-250', '4403', 4056186425100, '0000-00-00', NULL, 1, NULL, 1),
(6128, 907, 1, 'APP-SAM-411', '5137', 4052852302300, '0000-00-00', NULL, 1, NULL, 1),
(6129, 908, 1, 'APP-SAM-490', '3235', 4056186862800, '0000-00-00', NULL, 1, NULL, 1),
(6130, 909, 1, 'APP-SAM-161', '3713', 4050373169000, '0000-00-00', NULL, 1, NULL, 1),
(6131, 910, 1, 'APP-SAM-158', '4110', 4050373169000, '0000-00-00', NULL, 1, NULL, 1),
(6132, 911, 1, 'APP-MON-020', '7109', 4056186863000, '0000-00-00', NULL, 1, NULL, 1),
(6133, 912, 1, 'APP-MON-016', '7103', 4052852301900, '0000-00-00', NULL, 1, NULL, 1),
(6134, 913, 1, 'APP-VIU-017', '2894', 4052852620300, '0000-00-00', NULL, 1, NULL, 1),
(6135, 914, 1, 'APP-VIU-025', '5334', 4050373613300, '0000-00-00', NULL, 1, NULL, 1),
(6136, 915, 1, 'APP-MON-009', '7101', 4050373169000, '0000-00-00', NULL, 1, NULL, 1),
(6137, 916, 1, 'APP-SAM-377', '5326', 4052852302100, '2017-01-11', NULL, 1, NULL, 1),
(6138, 917, 1, 'APP-VIU-033', '2893', 4050373613400, '0000-00-00', NULL, 1, NULL, 1),
(6139, 918, 1, 'APP-MON-010', '7002', 4050373168900, '0000-00-00', NULL, 1, NULL, 1),
(6140, 919, 1, 'APP-SAM-162', '3407', 4050373169000, '0000-00-00', NULL, 1, NULL, 1),
(6141, 920, 1, 'APP-SAM-385', '2860', 4052852302100, '0000-00-00', NULL, 1, NULL, 1),
(6142, 921, 1, 'APP-VIU-030', '5310', 4050373613400, '0000-00-00', NULL, 1, NULL, 1),
(6143, 922, 1, 'APP-VIU-052', '5212', 4052852618800, '0000-00-00', NULL, 1, NULL, 1),
(6144, 923, 1, 'APP-SAM-393', '2876', 4052852302200, '0000-00-00', NULL, 1, NULL, 1),
(6145, 924, 1, 'APP-SAM-163', '1813', 4050373169200, '0000-00-00', NULL, 1, NULL, 1),
(6146, 925, 1, 'APP-SAM-164', '2304', 4050373169200, '0000-00-00', NULL, 1, NULL, 1),
(6147, 926, 1, 'APP-GOL-071', '6053', 4056186862700, '0000-00-00', NULL, 1, NULL, 1),
(6148, 927, 1, '', '', NULL, '0000-00-00', NULL, 1, NULL, 1),
(6149, 928, 1, 'APP-MON-017', '7106', 4056186425000, '0000-00-00', NULL, 1, NULL, 1),
(6150, 929, 1, 'APP-MON-011', '7403', 4050373169200, '0000-00-00', NULL, 1, NULL, 1),
(6151, 930, 1, 'APP-MON-025', '7110', 4056186425000, '0000-00-00', NULL, 1, NULL, 1),
(6152, 931, 1, 'APP-MON-012', '7003', 4050373169200, '0000-00-00', NULL, 1, NULL, 1),
(6153, 932, 1, 'APP-MON-014', '7104', 4056186862800, '0000-00-00', NULL, 1, NULL, 1),
(6154, 933, 1, 'APP-MON-013', '7102', 4050373169200, '0000-00-00', NULL, 1, NULL, 1),
(6155, 934, 1, 'APP-SAM-165', '3408', 4050373169200, '0000-00-00', NULL, 1, NULL, 1),
(6156, 935, 1, 'APP-SAM-468', '3727', 4056186425200, '0000-00-00', NULL, 1, NULL, 1),
(6157, 936, 1, 'APP-SAM-467', '3842', 4050373169300, '0000-00-00', NULL, 1, NULL, 1),
(6158, 937, 1, 'APP-SAM-166', '3715', 4050373169300, '0000-00-00', NULL, 1, NULL, 1),
(6159, 938, 1, 'APP-GOL-025', '6032', 4052852618800, '0000-00-00', NULL, 1, NULL, 1),
(6160, 939, 1, 'APP-GOL-056', '6043', 4052852952500, '0000-00-00', NULL, 1, NULL, 1),
(6161, 940, 1, 'APP-SAM-167', '1818', 4050373169300, '0000-00-00', NULL, 1, NULL, 1),
(6162, 941, 1, 'APP-MON-015', '7105', 4052852301900, '0000-00-00', NULL, 1, NULL, 1),
(6163, 942, 1, 'APP-VIU-032', '2880', 4050373872600, '0000-00-00', NULL, 1, NULL, 1),
(6164, 943, 1, 'APP-VIU-031', '2234', 4050373872600, '0000-00-00', NULL, 1, NULL, 1),
(6165, 944, 1, 'APP-VIU-058', '5213', 4052852618800, '0000-00-00', NULL, 1, NULL, 1),
(6166, 945, 1, 'APP-SAM-170', '3116', 4050373169300, '0000-00-00', NULL, 1, NULL, 1),
(6167, 946, 1, 'APP-SAM-168', '1908', 4050373169300, '0000-00-00', NULL, 1, NULL, 1),
(6168, 947, 1, 'APP-SAM-169', '3115', 4050373169300, '0000-00-00', NULL, 1, NULL, 1),
(6169, 948, 1, 'APP-GOL-035', '6033', 4052852618800, '0000-00-00', NULL, 1, NULL, 1),
(6170, 949, 1, 'APP-SAM-171', '1108', 4050373169300, '0000-00-00', NULL, 1, NULL, 1),
(6171, 950, 1, 'APP-TER-008', '7509', 4052852302300, '0000-00-00', NULL, 1, NULL, 1),
(6172, 951, 1, 'APP-SAM-172', '4205', 4050373169300, '0000-00-00', NULL, 1, NULL, 1),
(6173, 952, 1, 'APP-SAM-173', '4206', 4050373169300, '0000-00-00', NULL, 1, NULL, 1),
(6174, 953, 1, 'APP-SAM-174', '4207', 4050373164100, '0000-00-00', NULL, 1, NULL, 1),
(6175, 954, 1, 'APP-SAM-175', '4208', 4050373169300, '0000-00-00', NULL, 1, NULL, 1),
(6176, 955, 1, 'APP-SAM-176', '2516', 4050373167300, '0000-00-00', NULL, 1, NULL, 1),
(6177, 956, 1, 'APP-SAM-177', '3617', 4050373167300, '0000-00-00', NULL, 1, NULL, 1),
(6178, 957, 1, 'APP-SAM-178', '3618', 4050373613300, '0000-00-00', NULL, 1, NULL, 1),
(6179, 958, 1, 'APP-VIU-043', '1822', 4052852302300, '0000-00-00', NULL, 1, NULL, 1),
(6180, 959, 1, 'APP-SAM-180', '3620', 4050373167300, '0000-00-00', NULL, 1, NULL, 1),
(6181, 960, 1, 'APP-SAM-179', '3619', 4050373167300, '0000-00-00', NULL, 1, NULL, 1),
(6182, 961, 1, 'APP-SAM-181', '3621', 4050373167300, '0000-00-00', NULL, 1, NULL, 1),
(6183, 962, 1, 'APP-SAM-183', '3623', 4050373167300, '0000-00-00', NULL, 1, NULL, 1),
(6184, 963, 1, 'APP-VIU-053', '5209', 4052852618800, '0000-00-00', NULL, 1, NULL, 1),
(6185, 964, 1, 'APP-SAM-343', '1109', 4050373444700, '0000-00-00', NULL, 1, NULL, 1),
(6186, 965, 1, 'APP-SAM-417', '3817', 4052852618800, '0000-00-00', NULL, 1, NULL, 1),
(6187, 966, 1, 'APP-SAM-372', '2858', 4052852302000, '0000-00-00', NULL, 1, NULL, 1),
(6188, 967, 1, 'APP-SAM-461', '2842', 4056186430000, '0000-00-00', NULL, 1, NULL, 1),
(6189, 968, 1, 'APP-SAM-386', '2856', 4052852302100, '0000-00-00', NULL, 1, NULL, 1),
(6190, 969, 1, 'APP-SAM-487', '3629', 4056186862600, '0000-00-00', NULL, 1, NULL, 1),
(6191, 970, 1, 'APP-SAM-184', '3222', 4050373164100, '0000-00-00', NULL, 1, NULL, 1),
(6192, 971, 1, 'APP-NOR-010', '3835', 4050373163700, '0000-00-00', NULL, 1, NULL, 1),
(6193, 972, 1, 'APP-SAM-154', '4003', 4056186863000, '0000-00-00', NULL, 1, NULL, 1),
(6194, 973, 1, 'APP-TER-001', '7501', 4050373613400, '0000-00-00', NULL, 1, NULL, 1),
(6195, 974, 1, 'APP-TER-003', '7503', 4050373613400, '0000-00-00', NULL, 1, NULL, 1),
(6196, 975, 1, 'APP-TER-002', '7005', 4050373613400, '0000-00-00', NULL, 1, NULL, 1),
(6197, 976, 1, 'APP-TER-026', '7521', 4052852843600, '0000-00-00', NULL, 1, NULL, 1),
(6198, 977, 1, 'APP-TER-027', '7522', 4052852843600, '0000-00-00', NULL, 1, NULL, 1),
(6199, 978, 1, 'APP-TER-007', '7528', 4050373613400, '0000-00-00', NULL, 1, NULL, 1),
(6200, 979, 1, 'APP-TER-004', '7504', 4050373613400, '0000-00-00', NULL, 1, NULL, 1),
(6201, 980, 1, 'APP-SAM-185', '1407', 4050373164100, '0000-00-00', NULL, 1, NULL, 1),
(6202, 981, 1, 'APP-SAM-229', '1408', 4050373164200, '0000-00-00', NULL, 1, NULL, 1),
(6203, 982, 1, 'APP-INV-Z-013', '7515', 4050373613400, '0000-00-00', NULL, 1, NULL, 1),
(6204, 983, 1, 'APP-INV-Z-007', '0914', 4050373613400, '0000-00-00', NULL, 1, NULL, 1),
(6205, 984, 1, 'APP-SAM-186', '2303', 4050373164200, '0000-00-00', NULL, 1, NULL, 1),
(6206, 985, 1, 'APP-SAM-188', '1705', 4050373164200, '0000-00-00', NULL, 1, NULL, 1),
(6207, 986, 1, 'APP-SAM-189', '3225', 4050373164000, '0000-00-00', NULL, 1, NULL, 1),
(6208, 987, 1, 'APP-SAM-190', '3226', 4050373164000, '0000-00-00', NULL, 1, NULL, 1),
(6209, 988, 1, 'APP-SAM-191', '3227', 4050373164000, '0000-00-00', NULL, 1, NULL, 1),
(6210, 989, 1, 'APP-SAM-254', '3624', 4050373163900, '0000-00-00', NULL, 1, NULL, 1),
(6211, 990, 1, 'APP-SAM-230', '3717', 4050373163900, '0000-00-00', NULL, 1, NULL, 1),
(6212, 991, 1, 'APP-SAM-192', '3718', 4050373163900, '0000-00-00', NULL, 1, NULL, 1),
(6213, 992, 1, 'APP-SAM-194', '4210', 4050373163800, '0000-00-00', NULL, 1, NULL, 1),
(6214, 993, 1, 'APP-SAM-403', '4219', 4052852330100, '0000-00-00', NULL, 1, NULL, 1),
(6215, 994, 1, 'APP-SAM-193', '4209', 4050373163800, '0000-00-00', NULL, 1, NULL, 1),
(6216, 995, 1, 'APP-SAM-196', '4211', 4056186430000, '0000-00-00', NULL, 1, NULL, 1),
(6217, 996, 1, 'APP-SAM-232', '3228', 4050373163800, '0000-00-00', NULL, 1, NULL, 1),
(6218, 997, 1, 'APP-SAM-356', '4217', 4052852302000, '0000-00-00', NULL, 1, NULL, 1),
(6219, 998, 1, 'APP-SAM-355', '4216', 4050373444700, '0000-00-00', NULL, 1, NULL, 1),
(6220, 999, 1, 'APP-SAM-231', '3625', 4050373163800, '0000-00-00', NULL, 1, NULL, 1),
(6221, 1000, 1, 'APP-SAM-456', '3828', 4052852952500, '0000-00-00', NULL, 1, NULL, 1),
(6222, 1001, 1, 'APP-SAM-439', '3824', 4052852618800, '0000-00-00', NULL, 1, NULL, 1),
(6223, 1002, 1, 'APP-SAM-440', '3825', 4052852618800, '0000-00-00', NULL, 1, NULL, 1),
(6224, 1003, 1, 'APP-SAM-195', '2520', 4050373163700, '0000-00-00', NULL, 1, NULL, 1),
(6225, 1004, 1, 'APP-SAM-197', '2228', 4050373163700, '0000-00-00', NULL, 1, NULL, 1),
(6226, 1005, 1, 'APP-GOL-031', '6034', 4052852618900, '0000-00-00', NULL, 1, NULL, 1),
(6227, 1006, 1, 'APP-GOL-026', '6035', 4052852618900, '0000-00-00', NULL, 1, NULL, 1),
(6228, 1007, 1, 'APP-SAM-198', '1323', 4050373163700, '0000-00-00', NULL, 1, NULL, 1),
(6229, 1008, 1, 'APP-MON-026', '7115', 4056186425000, '0000-00-00', NULL, 1, NULL, 1),
(6230, 1009, 1, 'APP-SAM-199', '1815', 4050373168900, '0000-00-00', NULL, 1, NULL, 1),
(6231, 1010, 1, 'APP-TER-028', '7524', 4052852620300, '0000-00-00', NULL, 1, NULL, 1),
(6232, 1011, 1, 'APP-SAM-200', '1708', 4050373163600, '0000-00-00', NULL, 1, NULL, 1),
(6233, 1012, 1, 'APP-SAM-203', '4406', 4050373163500, '0000-00-00', NULL, 1, NULL, 1),
(6234, 1013, 1, 'APP-SAM-261', '4409', 4050373163500, '0000-00-00', NULL, 1, NULL, 1),
(6235, 1014, 1, 'APP-SAM-262', '4410', 4050373163500, '0000-00-00', NULL, 1, NULL, 1),
(6236, 1015, 1, 'APP-SAM-233', '4408', 4050373163400, '0000-00-00', NULL, 1, NULL, 1),
(6237, 1016, 1, 'APP-SAM-201', '4405', 4050373163600, '0000-00-00', NULL, 1, NULL, 1),
(6238, 1017, 1, 'APP-SAM-202', '3721', 4050373163500, '0000-00-00', NULL, 1, NULL, 1),
(6239, 1018, 1, 'APP-SAM-234', '3722', 4050373163400, '0000-00-00', NULL, 1, NULL, 1),
(6240, 1019, 1, 'APP-GOL-029', '6037', 4052852618900, '0000-00-00', NULL, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerto_destino`
--

CREATE TABLE `puerto_destino` (
  `id` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puerto_destino`
--

INSERT INTO `puerto_destino` (`id`, `id_ciudad`, `nombre`, `activo`) VALUES
(1, 2, 'HAMBURGO\r\n', 1),
(2, 3, 'MONTREAL', 1),
(3, 1, 'ROTTERDAM', 1),
(4, 7, 'SAN DIEGO', 1),
(5, 1, 'WILMINGTON', 1),
(6, 1, 'ANTWERP', 1),
(7, 1, 'KOREA', 1),
(11, 4, 'VLISSINGEN', 1),
(12, 5, 'DOVER', 1),
(13, 6, 'NEW ORLEANS', 1),
(14, 7, 'PORT HUENEME', 1),
(15, 8, 'RODMAN', 1),
(16, 9, 'PORT FREEPORT', 1),
(17, 11, 'DELAWARE', 1),
(18, 12, 'EVERGALDES', 1),
(19, 13, 'PORT ANTWERP', 1),
(20, 14, 'PHILADELPHIA', 1),
(21, 12, 'MIAMI', 1),
(22, 7, 'WILMINGTON, US', 1),
(23, 15, 'ALIOTH', 1),
(24, 13, 'AMBERES', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `id_menu`, `activo`) VALUES
(1, 'Administrador', 14, 1),
(2, 'Jefe Trazabilidad', 15, 1),
(3, 'Clientes', 16, 1),
(5, 'paletizador', 16, 0),
(6, 'Gerencia', 20, 1),
(7, 'Auxiliar Trazabilidad', 21, 1),
(8, 'Operador', 22, 1),
(9, 'Control Interno', 23, 1),
(10, 'Facturador', 24, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sector`
--

CREATE TABLE `sector` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `ubicacion` varchar(255) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sector`
--

INSERT INTO `sector` (`id`, `nombre`, `ubicacion`, `activo`) VALUES
(1, 'BOMBA 01', '', 1),
(2, 'BOMBA 02', '', 1),
(3, 'BOMBA 03', '', 1),
(4, 'BOMBA PROVISIONAL', '', 1),
(5, 'CARRILLO BOMBA', '', 1),
(6, 'CUCHILLA LA BOMBA', '', 1),
(7, 'EL ALGORROBO', '', 1),
(8, 'EL ALGORROBO III', '', 1),
(9, 'EL DERECHO', '', 1),
(10, 'INVERNA ZAPATA 01', '', 1),
(11, 'INVERNA ZAPATA 02', '', 1),
(12, 'INVERNA ZAPATA 03', '', 1),
(13, 'LAS 7 CUADRAS', '', 1),
(14, 'LENGUA DE FUEGO I', '', 1),
(15, 'LENGUA DE FUEGO II', '', 1),
(16, 'LENGUA DE FUEGO III', '', 1),
(17, 'LENGUA DE FUEGO IV', '', 1),
(18, 'MACALO', '', 1),
(19, 'MONTECITO BAJO', '', 1),
(20, 'MONTERON', '', 1),
(21, 'ORDINOLA', '', 1),
(22, 'PAMPAS DE LA VIUDA', '', 1),
(23, 'S. EL ANIMA', '', 1),
(24, 'S. EL CAMOTE', '', 1),
(25, 'S. LAS 20', '', 1),
(26, 'S. LAS 24', '', 1),
(27, 'S. LAS 31', '', 1),
(28, 'S. LAS 35', '', 1),
(29, 'S. LAS 45', '', 1),
(30, 'S. LAS 50', '', 1),
(31, 'S. LAS 58', '', 1),
(32, 'S. LAS 64', '', 1),
(33, 'S/C', '', 1),
(34, 'SAMAN CHICO', '', 1),
(35, 'SAN MIGUEL', '', 1),
(36, 'SANDOVAL', '', 1),
(37, 'SANDOVAL 02', '', 1),
(38, 'SANTA SOFÍA', '', 1),
(39, 'SECTOR CARRILLO', '', 1),
(40, 'TERCERA', '', 1),
(41, 'VDA 5211', '', 1),
(42, 'VIUDA ALTA 01', '', 1),
(43, 'VIUDA ALTA ALTA', '', 1),
(44, 'VIUDA BAJA 01', '', 1),
(45, 'VIUDA BAJA 02', '', 1),
(46, 'VIUDA BAJA BADEN ', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semana`
--

CREATE TABLE `semana` (
  `id` int(11) NOT NULL,
  `nombre` int(11) NOT NULL,
  `id_anio` int(4) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `semana`
--

INSERT INTO `semana` (`id`, `nombre`, `id_anio`, `activo`) VALUES
(19, 2, 1, 0),
(20, 4, 1, 0),
(21, 20, 1, 0),
(22, 22, 1, 0),
(23, 21, 1, 1),
(24, 22, 1, 1),
(25, 20, 1, 1),
(26, 19, 1, 1),
(27, 18, 1, 1),
(28, 17, 1, 1),
(29, 16, 1, 1),
(30, 15, 1, 1),
(31, 14, 1, 1),
(32, 13, 1, 1),
(33, 12, 1, 1),
(34, 11, 1, 1),
(35, 10, 1, 1),
(36, 9, 1, 0),
(37, 9, 1, 1),
(38, 8, 1, 1),
(39, 7, 1, 1),
(40, 5, 1, 1),
(41, 4, 1, 1),
(42, 1, 1, 1),
(43, 2, 1, 1),
(44, 3, 1, 1),
(45, 6, 1, 1),
(46, 23, 1, 1),
(47, 24, 1, 1),
(48, 25, 1, 1),
(49, 26, 1, 1),
(50, 27, 1, 1),
(51, 28, 1, 1),
(52, 29, 1, 1),
(53, 30, 1, 1),
(54, 31, 1, 1),
(55, 32, 1, 1),
(56, 33, 1, 1),
(57, 34, 1, 1),
(58, 35, 1, 1),
(59, 36, 1, 1),
(60, 37, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subopcion`
--

CREATE TABLE `subopcion` (
  `id` int(11) NOT NULL,
  `opcion_id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `icono` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subopcion`
--

INSERT INTO `subopcion` (`id`, `opcion_id`, `nombre`, `icono`, `url`, `activo`) VALUES
(9, 7, 'Lista', 'cog', 'home.container.lista', 1),
(10, 8, 'Semanas', 'calendar', 'home.semana.lista_semana', 1),
(11, 9, 'Clientes', 'users', 'home.mantenimiento.cliente', 1),
(13, 9, 'Linea Naviera', 'ship', 'home.mantenimiento.linea_naviera', 1),
(14, 9, 'Puerto Destino', 'anchor', 'home.mantenimiento.puerto_destino', 1),
(15, 9, 'Operadores', 'user', 'home.mantenimiento.operador', 1),
(16, 12, 'Usuarios', 'user', 'home.usuario.usuario', 1),
(19, 9, 'Cargo', 'vcard-o', 'home.mantenimiento.cargo', 1),
(20, 9, 'Nave', 'train', 'home.mantenimiento.nave', 1),
(21, 9, 'Estados', 'tags', 'home.mantenimiento.estado', 1),
(22, 10, 'Contenedores Por Semana', 'file-excel-o', 'home.reporte.reporte_semana', 1),
(23, 10, 'Contenedores Por Cliente', 'file-excel-o', 'home.reporte.contenedores_cliente', 1),
(24, 10, 'Movimiento de Contenedores', 'file-excel-o', 'home.reporte.movimiento_contenedores', 1),
(25, 11, 'Contenedores Por Cliente', 'bar-chart', 'home.grafico.grafico_cliente', 1),
(26, 11, 'Contenedores Por Semana', 'bar-chart', 'home.grafico.grafico_semana', 1),
(27, 11, 'Contenedores Por Año', 'bar-chart', 'home.grafico.grafico_anio', 1),
(28, 9, 'Caja', 'inbox', 'home.mantenimiento.caja', 1),
(29, 13, 'Mis Contenedores', 'list', 'home.container.cliente', 1),
(30, 9, 'Pais', 'flag', 'home.mantenimiento.pais', 1),
(31, 9, 'Ciudad', 'institution', 'home.mantenimiento.ciudad', 1),
(33, 14, 'Mis Containers', 'truck', 'home.container.operador', 1),
(35, 15, 'Ver Auditorías', 'search', 'home.auditoria.auditoria_reporte', 1),
(36, 10, 'Cajas Exportadas', 'inbox', 'home.reporte.cajas_exportadas', 1),
(39, 10, 'Contenedores Por Operador', 'file-excel-o', 'home.reporte.contenedores_operador', 1),
(41, 10, 'Buscar Contenedores', 'search', 'home.reporte.buscar_contenedor', 1),
(42, 9, 'Empacadora', 'check', 'home.mantenimiento.empacadora', 1),
(43, 9, 'Sector', 'check', 'home.mantenimiento.sector', 1),
(44, 16, 'Productor', 'check', 'home.productor.productor', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subopcion_usuario`
--

CREATE TABLE `subopcion_usuario` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `subopcion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subopcion_usuario`
--

INSERT INTO `subopcion_usuario` (`id`, `usuario_id`, `subopcion_id`) VALUES
(2, 2, 9),
(3, 2, 10),
(4, 2, 11),
(5, 2, 13),
(6, 2, 14),
(7, 2, 15),
(8, 2, 19),
(9, 2, 20),
(10, 2, 21),
(11, 2, 28),
(12, 2, 30),
(13, 2, 31),
(14, 2, 22),
(15, 2, 23),
(16, 2, 24),
(17, 2, 36),
(18, 2, 25),
(19, 2, 26),
(20, 2, 27),
(21, 2, 35),
(22, 4, 9),
(23, 4, 10),
(24, 4, 11),
(25, 4, 13),
(26, 4, 14),
(27, 4, 15),
(28, 4, 19),
(29, 4, 20),
(30, 4, 21),
(31, 4, 28),
(32, 4, 30),
(33, 4, 31),
(34, 4, 22),
(35, 4, 23),
(36, 4, 24),
(37, 4, 36),
(38, 4, 25),
(39, 4, 26),
(40, 4, 27),
(41, 10, 29),
(42, 11, 29),
(43, 12, 29),
(44, 13, 29),
(45, 14, 29),
(46, 18, 33),
(47, 16, 9),
(48, 16, 10),
(49, 17, 9),
(50, 17, 10),
(51, 22, 10),
(52, 21, 33),
(53, 20, 10),
(54, 20, 22),
(55, 20, 23),
(56, 20, 24),
(57, 20, 36),
(58, 20, 25),
(59, 20, 26),
(60, 20, 27),
(61, 19, 10),
(62, 19, 22),
(63, 19, 23),
(64, 19, 24),
(65, 19, 36),
(66, 19, 25),
(67, 19, 26),
(68, 19, 27),
(73, 20, 9),
(74, 19, 9),
(75, 2, 16),
(76, 2, 39),
(77, 2, 41),
(78, 2, 42),
(79, 2, 43),
(80, 2, 44);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_cliente_operador` int(11) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nick`, `password`, `id_persona`, `id_cliente_operador`, `remember_token`, `activo`) VALUES
(2, '20484062031', '$2y$10$QLduX8C9SFiE.QyCObyMGet8y8dwP2eibs8oe2ZJpA3TT49Z6gtsq', 6, NULL, 'Y8kOpUoA9UI6Xqy2KWwKRYWSRkpSZCowRtEta9dblUUONchagrVaV0NqQeiB', 1),
(4, '40021803', '$2y$10$wHmbrT0.uU1m/c5KSOtGl.ABkwLjMRPtmiJzgXP4WI7c0wP.iOmzS', 8, NULL, 'uJu3pjx6Fzt14UbMhjqvx4cMnnGOp0akIE0RWbSKeCV4tU9FKweKfwbemSFe', 1),
(10, 'agrofair', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 21, 1, 'zKqKu6hH3lmz3IZ2cgmURgg3vY6ICtiOf2464vQELwoAIQth0BkVscBYBS0f', 1),
(11, 'biodynamiska', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 22, 2, 'MOsOg5bAZAwG4LesiqaFRBV3I3jHfHxRVfRV1RzxCdLCKvdj0hX04DoAoypC', 1),
(12, 'dole', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 23, 3, 'm5VO3SL0FRqyMerisUvGgpV97eCEixhITC9fZHnCFH5dQ1x6si3PoyxBT228', 1),
(13, 'transastra', '$2y$10$ScOu.jH7zJ/TdRX5tOTBeOU2YYiyLrFA4PLrYa9cSRvT2YJr/R31O', 24, 4, NULL, 1),
(14, 'equifruit', '$2y$10$oMAqZHdJYgIDfBKooa0jXuIIQBTrWt2yoxa4364pAP0uzVTVc/G8O', 25, 5, NULL, 1),
(16, '42262238', '$2y$10$/nPupH85dkWWDhYlho79UusR5CgZeZaBVB/UbHRrWXmaQlR7nVOuC', 27, NULL, 'AdhY5DB6YhBVpxX5CS65lVABZo0LnTGz4LbHmBxd8fwUttDqU9in0Hig8djf', 1),
(17, '42661236', '$2y$10$Vpah7zygI9rECUQm94vOuudUrfieTYJuUT5xyz/OnlJR2ZoCwrQTO', 28, NULL, NULL, 1),
(18, 'sanmiguel', '$2y$10$ByBUGWD4MMPBNAAgeRbJpOPxUY6X1W6WQdFHnfJ3RZk2y3YP2GkY.', 29, 1, 'MpcqpKw5X5DLrknisj95JY8wg9rcWj1PpcoGzAWnhsXlslsPMS4TCN8VWG19', 1),
(19, 'raul', '$2y$10$8AsYY0jiFGMlQb2uvlQKYe0t8YWgalpa6wmVmrwI1Ya19cJTxeP12', 30, NULL, 'EYxVUPSLqZAsqBZcCn3zU5Ji8lUKWNXpqCUIzojaeU1ZkY9NQ6kzgejxqOgy', 1),
(20, 'marcia', '$2y$10$JI0q31slaabPZlftm7r1lOwz2gUynYcqe/2AQE/vnO7Y9GwxL0iNy', 31, NULL, 'gB2pEFtsfXNcmpJeI5bBusCgcewukTpCwvech6bepLjeTXF7QN68uvXmA2vw', 1),
(21, 'lahanseatica', '$2y$10$gi9QnciJaofEHQ5NL.D7yeHRZp6l9czrZbyt12RItftlsfs/P7ezm', 32, 15, 'HoosU1cDbsK8zMw1fKtYHFtT1R6inSpHWfWhYaZegFIGJ5Y1bRqSN0hEXTxn', 1),
(22, 'usuario', '$2y$10$0.DcRizjDJou3qEBURzrWu1e5bmsxObkaUgbafG8vM1HaJrBMrzra', 33, NULL, '9h9KrbDKSDvfXsi35bHl3guFMUe6t3d3Fr2Dy8MfhXe4Dd4T2UJYTkJszWAo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valija`
--

CREATE TABLE `valija` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` text NOT NULL,
  `id_estado_archivo` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `valija`
--

INSERT INTO `valija` (`id`, `id_cliente`, `id_semana`, `nombre`, `fecha`, `hora`, `url`, `id_estado_archivo`) VALUES
(1, 2, 50, '2_27_2017_(1)MVC.pdf', '2017-10-02', '12:29:46', '2_27_2017_(1)MVC.pdf', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vl`
--

CREATE TABLE `vl` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_semana` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `url` text NOT NULL,
  `id_estado_archivo` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anio`
--
ALTER TABLE `anio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_caja`
--
ALTER TABLE `audit_caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_cargo`
--
ALTER TABLE `audit_cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_certificado`
--
ALTER TABLE `audit_certificado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_ciudad`
--
ALTER TABLE `audit_ciudad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_cliente`
--
ALTER TABLE `audit_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_contenedor`
--
ALTER TABLE `audit_contenedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_estado`
--
ALTER TABLE `audit_estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_factura`
--
ALTER TABLE `audit_factura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_linea_naviera`
--
ALTER TABLE `audit_linea_naviera`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_nave`
--
ALTER TABLE `audit_nave`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_operador`
--
ALTER TABLE `audit_operador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_packing`
--
ALTER TABLE `audit_packing`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_pais`
--
ALTER TABLE `audit_pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_parcela`
--
ALTER TABLE `audit_parcela`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_productor`
--
ALTER TABLE `audit_productor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_puerto_destino`
--
ALTER TABLE `audit_puerto_destino`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_semana`
--
ALTER TABLE `audit_semana`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_usuario`
--
ALTER TABLE `audit_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_valija`
--
ALTER TABLE `audit_valija`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_vl`
--
ALTER TABLE `audit_vl`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caja_cliente`
--
ALTER TABLE `caja_cliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_caja_cliente` (`caja_id`),
  ADD KEY `id_cliente_caja` (`cliente_id`);

--
-- Indices de la tabla `caja_contenedor`
--
ALTER TABLE `caja_contenedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contendor_id` (`contenedor_id`),
  ADD KEY `caja_id` (`caja_id`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `certificado`
--
ALTER TABLE `certificado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente_certificado` (`id_cliente`),
  ADD KEY `id_semana_certificado` (`id_semana`),
  ADD KEY `id_estado_archivo_certificado` (`id_estado_archivo`);

--
-- Indices de la tabla `certificado_contenedor`
--
ALTER TABLE `certificado_contenedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `certificado_id` (`certificado_id`),
  ADD KEY `contenedor_id_certificado` (`contenedor_id`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pais_id` (`id_pais`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente_operador`
--
ALTER TABLE `cliente_operador`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `operador_id` (`operador_id`);

--
-- Indices de la tabla `contenedor`
--
ALTER TABLE `contenedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_booking` (`booking`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_lineanaviera` (`id_lineanaviera`),
  ADD KEY `id_puertodestino` (`id_puertodestino`),
  ADD KEY `id_operador` (`id_operador`),
  ADD KEY `id_semana` (`id_semana`),
  ADD KEY `id_nave` (`nave`);

--
-- Indices de la tabla `contenedor_valija`
--
ALTER TABLE `contenedor_valija`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contenedor_id` (`contenedor_id`),
  ADD KEY `valija_id` (`valija_id`);

--
-- Indices de la tabla `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id_email` (`id_cliente`);

--
-- Indices de la tabla `empacadora`
--
ALTER TABLE `empacadora`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_archivo`
--
ALTER TABLE `estado_archivo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente_factura` (`id_cliente`),
  ADD KEY `id_semana_factura` (`id_semana`),
  ADD KEY `id_estado_archivo_factura` (`id_estado_archivo`);

--
-- Indices de la tabla `icono`
--
ALTER TABLE `icono`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `linea_naviera`
--
ALTER TABLE `linea_naviera`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu_opcion`
--
ALTER TABLE `menu_opcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `opcionm_id` (`opcion_id`);

--
-- Indices de la tabla `motivo`
--
ALTER TABLE `motivo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_contenedor` (`id_contenedor`),
  ADD KEY `id_estado` (`id_estado`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `nave`
--
ALTER TABLE `nave`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `opcion`
--
ALTER TABLE `opcion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `opcion_subopcion`
--
ALTER TABLE `opcion_subopcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `opcion_id` (`opcion_id`),
  ADD KEY `subopcion_id` (`subopcion_id`);

--
-- Indices de la tabla `operador`
--
ALTER TABLE `operador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `packing`
--
ALTER TABLE `packing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_contenedor_packing` (`id_cliente`),
  ADD KEY `id_semana_packing` (`id_semana`),
  ADD KEY `id_estado_archivo_packing` (`id_estado_archivo`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `parcela`
--
ALTER TABLE `parcela`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parcela_id_productor` (`id_productor`),
  ADD KEY `parcela_id_empacadora` (`id_empacadora`),
  ADD KEY `parcela_id_sector` (`id_sector`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permiso_rol`
--
ALTER TABLE `permiso_rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rol_id` (`rol_id`),
  ADD KEY `permiso_id` (`permiso_id`);

--
-- Indices de la tabla `permiso_usuario`
--
ALTER TABLE `permiso_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pu_usuario_id` (`usuario_id`),
  ADD KEY `pu_permiso_id` (`permiso_id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cargo` (`id_cargo`);

--
-- Indices de la tabla `productor`
--
ALTER TABLE `productor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `productor_id_persona` (`id_persona`) USING BTREE;

--
-- Indices de la tabla `puerto_destino`
--
ALTER TABLE `puerto_destino`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ciudad_id` (`id_ciudad`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_menu` (`id_menu`);

--
-- Indices de la tabla `sector`
--
ALTER TABLE `sector`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `semana`
--
ALTER TABLE `semana`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_anio` (`id_anio`);

--
-- Indices de la tabla `subopcion`
--
ALTER TABLE `subopcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `opcion_id` (`opcion_id`);

--
-- Indices de la tabla `subopcion_usuario`
--
ALTER TABLE `subopcion_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id_new` (`usuario_id`),
  ADD KEY `subopcion_id_new` (`subopcion_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_persona` (`id_persona`);

--
-- Indices de la tabla `valija`
--
ALTER TABLE `valija`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente_valija` (`id_cliente`),
  ADD KEY `id_semana_valija` (`id_semana`),
  ADD KEY `id_estado_archivo_valija` (`id_estado_archivo`);

--
-- Indices de la tabla `vl`
--
ALTER TABLE `vl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_contenedor_vl` (`id_cliente`) USING BTREE,
  ADD KEY `id_semana_vl` (`id_semana`) USING BTREE,
  ADD KEY `id_estado_archivo_vl` (`id_estado_archivo`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anio`
--
ALTER TABLE `anio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `audit_caja`
--
ALTER TABLE `audit_caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `audit_cargo`
--
ALTER TABLE `audit_cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `audit_certificado`
--
ALTER TABLE `audit_certificado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `audit_ciudad`
--
ALTER TABLE `audit_ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_cliente`
--
ALTER TABLE `audit_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_contenedor`
--
ALTER TABLE `audit_contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1350;
--
-- AUTO_INCREMENT de la tabla `audit_estado`
--
ALTER TABLE `audit_estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `audit_factura`
--
ALTER TABLE `audit_factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT de la tabla `audit_linea_naviera`
--
ALTER TABLE `audit_linea_naviera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_nave`
--
ALTER TABLE `audit_nave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_operador`
--
ALTER TABLE `audit_operador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `audit_packing`
--
ALTER TABLE `audit_packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT de la tabla `audit_pais`
--
ALTER TABLE `audit_pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_parcela`
--
ALTER TABLE `audit_parcela`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_productor`
--
ALTER TABLE `audit_productor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audit_puerto_destino`
--
ALTER TABLE `audit_puerto_destino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `audit_semana`
--
ALTER TABLE `audit_semana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT de la tabla `audit_usuario`
--
ALTER TABLE `audit_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT de la tabla `audit_valija`
--
ALTER TABLE `audit_valija`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `audit_vl`
--
ALTER TABLE `audit_vl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `caja_cliente`
--
ALTER TABLE `caja_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `caja_contenedor`
--
ALTER TABLE `caja_contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1075;
--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `certificado`
--
ALTER TABLE `certificado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `certificado_contenedor`
--
ALTER TABLE `certificado_contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `cliente_operador`
--
ALTER TABLE `cliente_operador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `contenedor`
--
ALTER TABLE `contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=767;
--
-- AUTO_INCREMENT de la tabla `contenedor_valija`
--
ALTER TABLE `contenedor_valija`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `empacadora`
--
ALTER TABLE `empacadora`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `estado_archivo`
--
ALTER TABLE `estado_archivo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT de la tabla `linea_naviera`
--
ALTER TABLE `linea_naviera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `menu_opcion`
--
ALTER TABLE `menu_opcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT de la tabla `motivo`
--
ALTER TABLE `motivo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1641;
--
-- AUTO_INCREMENT de la tabla `nave`
--
ALTER TABLE `nave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT de la tabla `opcion`
--
ALTER TABLE `opcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `opcion_subopcion`
--
ALTER TABLE `opcion_subopcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `operador`
--
ALTER TABLE `operador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `packing`
--
ALTER TABLE `packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `parcela`
--
ALTER TABLE `parcela`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=751;
--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `permiso_rol`
--
ALTER TABLE `permiso_rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT de la tabla `permiso_usuario`
--
ALTER TABLE `permiso_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1020;
--
-- AUTO_INCREMENT de la tabla `productor`
--
ALTER TABLE `productor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6241;
--
-- AUTO_INCREMENT de la tabla `puerto_destino`
--
ALTER TABLE `puerto_destino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `sector`
--
ALTER TABLE `sector`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT de la tabla `semana`
--
ALTER TABLE `semana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT de la tabla `subopcion`
--
ALTER TABLE `subopcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT de la tabla `subopcion_usuario`
--
ALTER TABLE `subopcion_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `valija`
--
ALTER TABLE `valija`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `vl`
--
ALTER TABLE `vl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `caja_cliente`
--
ALTER TABLE `caja_cliente`
  ADD CONSTRAINT `id_caja_cliente` FOREIGN KEY (`caja_id`) REFERENCES `caja` (`id`),
  ADD CONSTRAINT `id_cliente_caja` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`);

--
-- Filtros para la tabla `caja_contenedor`
--
ALTER TABLE `caja_contenedor`
  ADD CONSTRAINT `caja_id` FOREIGN KEY (`caja_id`) REFERENCES `caja` (`id`),
  ADD CONSTRAINT `contendor_id` FOREIGN KEY (`contenedor_id`) REFERENCES `contenedor` (`id`);

--
-- Filtros para la tabla `certificado`
--
ALTER TABLE `certificado`
  ADD CONSTRAINT `id_cliente_certificado` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_estado_archivo_certificado` FOREIGN KEY (`id_estado_archivo`) REFERENCES `estado_archivo` (`id`),
  ADD CONSTRAINT `id_semana_certificado` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

--
-- Filtros para la tabla `certificado_contenedor`
--
ALTER TABLE `certificado_contenedor`
  ADD CONSTRAINT `certificado_id` FOREIGN KEY (`certificado_id`) REFERENCES `certificado` (`id`),
  ADD CONSTRAINT `contenedor_id_certificado` FOREIGN KEY (`contenedor_id`) REFERENCES `contenedor` (`id`);

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `pais_id` FOREIGN KEY (`id_pais`) REFERENCES `pais` (`id`);

--
-- Filtros para la tabla `cliente_operador`
--
ALTER TABLE `cliente_operador`
  ADD CONSTRAINT `cliente_id` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `operador_id` FOREIGN KEY (`operador_id`) REFERENCES `operador` (`id`);

--
-- Filtros para la tabla `contenedor`
--
ALTER TABLE `contenedor`
  ADD CONSTRAINT `id_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_lineanaviera` FOREIGN KEY (`id_lineanaviera`) REFERENCES `linea_naviera` (`id`),
  ADD CONSTRAINT `id_operador` FOREIGN KEY (`id_operador`) REFERENCES `operador` (`id`),
  ADD CONSTRAINT `id_puertodestino` FOREIGN KEY (`id_puertodestino`) REFERENCES `puerto_destino` (`id`),
  ADD CONSTRAINT `id_semana` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

--
-- Filtros para la tabla `contenedor_valija`
--
ALTER TABLE `contenedor_valija`
  ADD CONSTRAINT `contenedor_id` FOREIGN KEY (`contenedor_id`) REFERENCES `contenedor` (`id`),
  ADD CONSTRAINT `valija_id` FOREIGN KEY (`valija_id`) REFERENCES `valija` (`id`);

--
-- Filtros para la tabla `emails`
--
ALTER TABLE `emails`
  ADD CONSTRAINT `cliente_id_email` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `id_cliente_factura` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_estado_archivo_factura` FOREIGN KEY (`id_estado_archivo`) REFERENCES `estado_archivo` (`id`),
  ADD CONSTRAINT `id_semana_factura` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

--
-- Filtros para la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD CONSTRAINT `id_contenedor` FOREIGN KEY (`id_contenedor`) REFERENCES `contenedor` (`id`),
  ADD CONSTRAINT `id_estado` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id`),
  ADD CONSTRAINT `id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `packing`
--
ALTER TABLE `packing`
  ADD CONSTRAINT `id_cliente_packing` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_estado_archivo_packing` FOREIGN KEY (`id_estado_archivo`) REFERENCES `estado_archivo` (`id`),
  ADD CONSTRAINT `id_semana_packing` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

--
-- Filtros para la tabla `parcela`
--
ALTER TABLE `parcela`
  ADD CONSTRAINT `parcela_id_empacadora` FOREIGN KEY (`id_empacadora`) REFERENCES `empacadora` (`id`),
  ADD CONSTRAINT `parcela_id_productor` FOREIGN KEY (`id_productor`) REFERENCES `productor` (`id`),
  ADD CONSTRAINT `parcela_id_sector` FOREIGN KEY (`id_sector`) REFERENCES `sector` (`id`);

--
-- Filtros para la tabla `permiso_rol`
--
ALTER TABLE `permiso_rol`
  ADD CONSTRAINT `rol_id` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `permiso_usuario`
--
ALTER TABLE `permiso_usuario`
  ADD CONSTRAINT `pu_permiso_id` FOREIGN KEY (`permiso_id`) REFERENCES `permiso` (`id`),
  ADD CONSTRAINT `pu_usuario_id` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `id_cargo` FOREIGN KEY (`id_cargo`) REFERENCES `cargo` (`id`);

--
-- Filtros para la tabla `productor`
--
ALTER TABLE `productor`
  ADD CONSTRAINT `productor_id_persona` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`);

--
-- Filtros para la tabla `puerto_destino`
--
ALTER TABLE `puerto_destino`
  ADD CONSTRAINT `ciudad_id` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudad` (`id`);

--
-- Filtros para la tabla `rol`
--
ALTER TABLE `rol`
  ADD CONSTRAINT `id_menu` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `semana`
--
ALTER TABLE `semana`
  ADD CONSTRAINT `id_anio` FOREIGN KEY (`id_anio`) REFERENCES `anio` (`id`);

--
-- Filtros para la tabla `subopcion`
--
ALTER TABLE `subopcion`
  ADD CONSTRAINT `opcion_id` FOREIGN KEY (`opcion_id`) REFERENCES `opcion` (`id`);

--
-- Filtros para la tabla `subopcion_usuario`
--
ALTER TABLE `subopcion_usuario`
  ADD CONSTRAINT `subopcion_id_new` FOREIGN KEY (`subopcion_id`) REFERENCES `subopcion` (`id`),
  ADD CONSTRAINT `usuario_id_new` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `id_persona` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `valija`
--
ALTER TABLE `valija`
  ADD CONSTRAINT `id_cliente_valija` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_estado_archivo_valija` FOREIGN KEY (`id_estado_archivo`) REFERENCES `estado_archivo` (`id`),
  ADD CONSTRAINT `id_semana_valija` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);

--
-- Filtros para la tabla `vl`
--
ALTER TABLE `vl`
  ADD CONSTRAINT `id_cliente_vl` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `id_estado_archivo_vl` FOREIGN KEY (`id_estado_archivo`) REFERENCES `estado_archivo` (`id`),
  ADD CONSTRAINT `id_semana_vl` FOREIGN KEY (`id_semana`) REFERENCES `semana` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
